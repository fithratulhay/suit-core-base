<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail . str_random(10),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
		'username' => $faker->unique()->userName . str_random(100),
		'role' => 'user',
		'status' => 'active',
		'registration_date' => Carbon::now(),
		'last_visit' => Carbon::now(),
    ];
});
