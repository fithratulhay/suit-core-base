<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('challenge_id');
            $table->unsignedInteger('user_group_id');
            $table->unsignedTinyInteger('chapter_complete')->default(0);
            $table->unsignedTinyInteger('progress')->default(0); // percentage 1 - 100
            $table->timestamps();

            $table->unique(['user_id', 'challenge_id']);

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('challenge_id')
                    ->references('id')->on('challenges')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('user_group_id')
                    ->references('id')->on('user_challenge_groups')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_challenges');
    }
}
