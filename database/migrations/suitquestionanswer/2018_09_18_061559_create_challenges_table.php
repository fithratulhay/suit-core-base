<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Chellange cannot be edited after its published, can only be deleted
         */

        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id');
            $table->string('title', 50);
            $table->string('slug', 100)->unique();
            $table->string('highlight', 100)->nullable();
            $table->text('description')->nullable();
            $table->unsignedMediumInteger('point_sum')->default(0); // PUBLISH DENORM
            $table->unsignedTinyInteger('chapter_count')->default(0); // PUBLISH DENORM 
            $table->boolean('is_double_point')->default(0);
            $table->dateTime('start_datetime')->nullable(); // it still need to be published
            $table->dateTime('end_datetime')->nullable();
            $table->unsignedTinyInteger('position_order');
            $table->string('status', 15)->default('drafted')->index();
            $table->timestamp('published_at')->nullable(); // if its posible to be edited after publish
            $table->timestamps();

            $table->foreign('group_id')
                    ->references('id')->on('challenge_groups')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}
