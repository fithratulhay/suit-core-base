<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('chapter_id');
            $table->unsignedInteger('user_challenge_id');
            $table->unsignedTinyInteger('question_complete')->default(0);
            $table->unsignedTinyInteger('material_complete')->default(0);
            $table->unsignedTinyInteger('progress')->default(0); // 1 - 100
            $table->timestamps();

            $table->unique(['user_id', 'chapter_id']);            

            $table->foreign('chapter_id')
                    ->references('id')->on('chapters')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('user_challenge_id')
                    ->references('id')->on('user_challenges')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_chapters');
    }
}
