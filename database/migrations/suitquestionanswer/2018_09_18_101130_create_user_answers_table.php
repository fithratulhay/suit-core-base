<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('answer_id');
            $table->unsignedInteger('user_chapter_id');
            $table->timestamps();

            $table->unique(['user_id', 'question_id']);

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('question_id')
                    ->references('id')->on('questions')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('answer_id')
                    ->references('id')->on('answers')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('user_chapter_id')
                    ->references('id')->on('user_chapters')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_answers');
    }
}
