<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('challenge_id');
            $table->unsignedMediumInteger('point_sum')->default(0); // DENORM
            $table->unsignedTinyInteger('question_count')->default(0); // DENORM
            $table->unsignedTinyInteger('material_count')->default(0); // DENORM
            $table->dateTime('start_datetime')->nullable(); // it still need to be published
            $table->dateTime('end_datetime')->nullable();
            $table->string('status', 15)->default('drafted')->index();
            $table->unsignedTinyInteger('position_order');
            $table->timestamps();

            $table->foreign('challenge_id')
                    ->references('id')->on('challenges')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapters');
    }
}
