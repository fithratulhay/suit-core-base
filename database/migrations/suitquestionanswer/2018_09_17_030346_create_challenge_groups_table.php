<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->unsignedTinyInteger('challenge_count')->default(0);
            $table->string('status', 15)->default('drafted')->index(); // drafted, published
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_groups');
    }
}
