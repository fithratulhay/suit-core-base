<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chapter_id');
            $table->string('question');
            $table->unsignedMediumInteger('point');
            $table->boolean('is_double_point')->default(0); // PUBLISH DENORM
            $table->dateTime('start_datetime')->nullable(); // PUBLISH DENORM
            $table->dateTime('end_datetime')->nullable(); // PUBLISH DENORM
            $table->string('status', 15)->default('drafted')->index(); // PUBLISH DENORM
            $table->unsignedTinyInteger('position_order');
            $table->timestamps();
            
            $table->foreign('chapter_id')
                    ->references('id')->on('chapters')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
