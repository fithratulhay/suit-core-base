<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_id');
            $table->string('answer', 100);
            $table->unsignedTinyInteger('position_order');
            $table->timestamps();

            $table->foreign('question_id')
                    ->references('id')->on('questions')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->unsignedInteger('answer_id')->nullable()->after('chapter_id'); // correct answer

            $table->foreign('answer_id')
                    ->references('id')->on('answers')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropForeign(['answer_id']);
            $table->dropColumn('answer_id');
        });

        Schema::dropIfExists('answers');
    }
}
