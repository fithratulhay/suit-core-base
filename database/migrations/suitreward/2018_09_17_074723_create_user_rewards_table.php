<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* 1. Where rewards $id get max_per_user
* 2. Where user_rewards $user_id, $reward_id order by redeem_counter desc get redeem_counter
* 
* if exist
* 3. Create user_rewards with redeem_counter = 1
* 
* if not exist
* 3. Create user_rewards with latest redeem_counter
* 
* 4. user_id, reward_id, redeem_counter is unique pairs.
* It can be safely used as generated_code parameter.
* It may act as safeguard from paralel requests.
* 
* 5. If user somehow can still create a user_rewards that exceed max_per_user value.
* It still checked in the end of script, if exceeded then it get deleted.
* max_per_user value also act as limiter when selecting user_rewards.
 */
class CreateUserRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('reward_id');
            $table->unsignedSmallInteger('redeem_counter'); // redeem counter to prevent fast execution
            $table->string('generated_code')->nullable();
            $table->timestamps();

            $table->unique(['user_id', 'reward_id', 'redeem_counter']);            

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('reward_id')
                    ->references('id')->on('rewards')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rewards');
    }
}
