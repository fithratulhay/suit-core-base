<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->nullable();
            $table->string('title', 100);
            $table->string('slug', 150);
            $table->string('highlight')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->text('term_condition')->nullable();
            $table->integer('point_cost'); // point cost to redeem
            $table->string('redeem_code')->nullable(); // used for global redeeming, not "code" as reward
            $table->unsignedInteger('max_redeem')->nullable(); // null = unlimited, server wide limit
            $table->unsignedInteger('max_redeem_per_user')->nullable(); // per user limit
            $table->unsignedInteger('redeemed_count'); // server wide redeemed stats
            $table->dateTime('start_datetime')->nullable(); // it still need to be active
            $table->dateTime('end_datetime')->nullable();
            $table->string('status', 15)->default('active')->index(); // active, inactive
            $table->timestamps();

            $table->unique(['slug', 'category_id']);

            $table->foreign('category_id')
                    ->references('id')->on('reward_categories')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewards');
    }
}
