<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_points', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id'); // owned by user_id
            $table->unsignedInteger('rewardable_id');
            $table->string('rewardable_type', 50);
            $table->unsignedMediumInteger('point_before');
            $table->integer('point_change');
            $table->unsignedMediumInteger('point_after');
            $table->string('type', 20); // reward / redeem
            $table->string('note', 150)->nullable();
            $table->string('status', 15)->default('confirmed')->index(); // requested / confirmed / rejected
            $table->timestamps();

            $table->unique(['rewardable_id', 'rewardable_type']);            

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_points');
    }
}
