<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners', function(Blueprint $table) {
			$table->increments('id');
			$table->string('type', 48);
            $table->integer('position_order')->nullable();
			$table->text('title', 128);
			$table->text('subtitle', 255)->nullable();
			$table->text('highlight', 1024)->nullable();
			$table->string('url', 1024)->nullable();
			$table->string('image', 255);
			$table->string('status', 48);
			$table->datetime('active_start_date')->nullable();
			$table->datetime('active_end_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners');
	}

}
