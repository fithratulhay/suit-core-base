<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 16)->index();
            $table->integer('position_order')->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('label', 64);
            $table->string('url', 1024);
            $table->boolean('is_active')->default(0);
            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')->on('menus')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
