<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id')->nullable(); // for event
            $table->unsignedInteger('city_id')->nullable(); // register from city
            $table->unsignedInteger('user_id')->nullable(); // if registered user participated (optional)
            $table->unsignedInteger('registered_by_user_id')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('phone_number');
            $table->text('address')->nullable();
            $table->text('activation_code')->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('event_id')
                    ->references('id')->on('events')
                    ->onDelete('set null');
            $table->foreign('city_id')
                    ->references('id')->on('event_cities')
                    ->onDelete('set null');
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('set null');
            $table->foreign('registered_by_user_id')
                    ->references('id')->on('users')
                    ->onDelete('set null');

            $table->unique(['event_id', 'email']);
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_registrations');
    }
}
