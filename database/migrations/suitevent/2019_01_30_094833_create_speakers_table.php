<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_speakers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('picture', 255)->nullable();
            $table->string('job_position')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('fb_profile_url')->nullable();
            $table->string('google_profile_url')->nullable();
            $table->string('linkedin_profile_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_speakers');
    }
}
