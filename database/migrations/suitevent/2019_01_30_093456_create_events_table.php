<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('city_id')->nullable();
            $table->string('name');
            $table->datetime('datetime_start')->nullable();
            $table->datetime('datetime_end')->nullable();
            $table->string('venue_detail')->nullable();
            $table->string('venue_name')->nullable();
            $table->double('venue_latitude', 18, 8)->nullable();
            $table->double('venue_longitude', 18, 8)->nullable();
            $table->text('summary')->nullable();
            $table->string('picture', 255)->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('city_id')
                    ->references('id')->on('event_cities')
                    ->onDelete('set null');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });
        Schema::dropIfExists('events');
    }
}
