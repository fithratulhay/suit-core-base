<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('program_type_id')->unsigned()->nullable();
            $table->integer('program_category_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('title', 128);
            $table->string('slug', 128)->unique();
            $table->text('highlight')->nullable();
            $table->longText('description')->nullable();
            // $table->string('image', 255)->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dateTime('published_date')->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('province_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('optional_location_name', 128)->nullable();
            $table->double('base_amount', 16, 2)->nullable();
            $table->double('target', 16, 2)->default(0);
            $table->double('collected', 16, 2)->default(0);
            $table->string('status', 12)->index();
            $table->integer('receiver')->default(0);
            $table->integer('position_order')->default(0);
            $table->integer('transaction_count')->default(0);
            $table->integer('transaction_avg')->default(0);
            $table->tinyInteger('is_show_target')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('program_type_id')->references('id')->on('program_types')->onDelete('set null');
            $table->foreign('program_category_id')->references('id')->on('program_categories')->onDelete('set null');
            $table->foreign('parent_id')->references('id')->on('programs')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');
            $table->foreign('province_id')->references('id')->on('provinces')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
