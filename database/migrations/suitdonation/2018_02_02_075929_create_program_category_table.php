<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_type_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('title', 128);
            $table->string('slug', 128)->unique();
            $table->mediumText('highlight')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('icon', 255)->nullable();
            $table->text('description')->nullable();
            $table->integer('position_order')->default(0);
            $table->timestamps();

            $table->foreign('program_type_id')->references('id')->on('program_types')->onDelete('set null');
            $table->foreign('parent_id')->references('id')->on('program_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_categories');
    }
}
