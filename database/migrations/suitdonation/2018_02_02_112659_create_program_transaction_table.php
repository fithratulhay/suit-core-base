<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36)->nullable();
            $table->integer('program_category_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('program_id')->unsigned()->nullable();
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->string('custom_user_name', 255)->nullable();
            $table->string('transaction_number', 48)->index();
            $table->string('transid_corez', 15)->nullable();
            $table->integer('quantity')->default(0);
            $table->double('base_amount', 16, 2)->default(0);
            $table->double('amount', 16, 2)->default(0);
            $table->dateTime('transaction_date');
            $table->dateTime('expiry_date')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->dateTime('verified_date')->nullable();
            $table->string('status', 12)->index();
            $table->text('message')->nullable();
            $table->string('transfer_file', 255)->nullable();
            $table->integer('unique_transaction_code')->length(10)->nullable();
            $table->timestamps();

            $table->foreign('program_category_id')->references('id')->on('program_categories')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('set null');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_transactions');
    }
}
