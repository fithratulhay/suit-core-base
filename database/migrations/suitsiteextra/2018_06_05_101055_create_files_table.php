<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_category_id')->unsigned()->nullable();
            $table->string('title', 128);
            $table->string('slug', 128)->unique();
            $table->text('highlight')->nullable();
            $table->mediumText('description')->nullable();
            $table->dateTime('published_date')->nullable();
            $table->string('optional_page_url', 255)->nullable();
            $table->string('source', 255)->nullable();
            $table->integer('file_size')->nullable();
            $table->integer('position_order')->nullable();
            $table->string('status', 50)->index();
            $table->timestamps();

            $table->foreign('file_category_id')->references('id')->on('file_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
