<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 64)->unique(); // as authentication username
            $table->string('email', 128)->unique(); // as authentication username
            $table->string('password'); // as authentication password
            $table->string('name');
            $table->date('birthdate')->nullable(); // optional
            $table->string('picture', 255)->nullable(); // optional
            $table->string('phone_number', 20)->nullable(); // optional
            $table->string('fb_id', 255)->nullable()->comment('facebook id');
            $table->string('fb_access_token', 255)->nullable()->comment('facebook access token');
            $table->string('tw_id')->nullable()->comment('twitter id');
            $table->string('tw_access_token', 255)->nullable()->comment('twitter access token');
            $table->string('gp_id')->nullable()->comment('google plus id');
            $table->string('gp_access_token', 255)->nullable()->comment('google plus access token');
            $table->string('referral_code', 32)->nullable();
            $table->integer('referral_user_id')->unsigned()->nullable();
            $table->string('role', 32)->index(); // basic : admin, operator, user
            $table->string('status', 32)->index(); // basic : inactive, active, unregistered, closed, banned
            $table->datetime('registration_date');
            $table->datetime('last_visit');
            $table->string('forget_password_token')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('referral_user_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['referral_user_id']);
        });
        Schema::drop('users');
    }
}
