<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubpathThemeBasecolorToRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->string('subpath', 64)->nullable()->after('name');
            $table->string('theme', 64)->nullable()->after('subpath');
            $table->string('base_color', 64)->nullable()->after('theme');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('subpath');
            $table->dropColumn('theme');
            $table->dropColumn('base_color');
        });
    }
}
