<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeritransInstallmentTenorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veritrans_installment_tenors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('veritrans_bank_id')->unsigned();
            $table->integer('term');
            $table->double('interest');
            $table->string('description')->nullable();
            $table->timestamps();

            $table->foreign('veritrans_bank_id')
                  ->references('id')->on('veritrans_banks')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veritrans_installment_tenors');
    }
}
