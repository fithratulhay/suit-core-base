<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokuInstallmentTenorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doku_installment_tenors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doku_bank_id')->unsigned();
            $table->integer('term');
            $table->double('interest');
            $table->string('description')->nullable();
            $table->timestamps();

            $table->foreign('doku_bank_id')
                  ->references('id')->on('doku_banks')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doku_installment_tenors');
    }
}
