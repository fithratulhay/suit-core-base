<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentMethodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_methods', function(Blueprint $table) {
			$table->increments('id');
			$table->string('bank_name', 45)->nullable();
			$table->string('bank_branch', 45)->nullable();
			$table->string('account_number', 45)->nullable();
			$table->string('account_name', 100)->nullable();
			$table->string('title', 45);
			$table->string('type', 45)->nullable();
			$table->double('admin_fee', 15, 2)->nullable();
            $table->double('optional_fixed_fee', 15, 2)->default(0)->nullable();
            $table->double('optional_percentage_fee', 15, 2)->default(0)->nullable();
            $table->double('tax', 15, 2)->default(0)->nullable();
			$table->string('status', 45);
			$table->string('image', 255)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('thirdparty_module_code')->nullable();
            $table->string('thirdparty_module_enabled_payment')->nullable();
            $table->string('thirdparty_module_installment_setting')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_methods');
	}

}
