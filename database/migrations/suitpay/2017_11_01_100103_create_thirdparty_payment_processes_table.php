<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThirdpartyPaymentProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thirdparty_payment_processes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_id');
            $table->integer('thirdparty_module_id')->unsigned()->nullable();
            $table->string('order_code')->nullable();
            $table->string('status', 32);
            $table->text('json_result')->nullable();
            $table->timestamps();

            //Constraints
            $table->foreign('thirdparty_module_id')
                ->references('id')->on('thirdparty_modules')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('thirdparty_payment_processes', function (Blueprint $table) {
            //Drop Constraint
            $table->dropForeign('thirdparty_payment_processes_thirdparty_module_id_foreign');
        });
        Schema::drop('thirdparty_payment_processes');
    }
}
