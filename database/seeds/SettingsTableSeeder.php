<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->delete();

        $data = [
            [
                'key'   => 'address',
                'value' => 'Jl. Pejaten Barat II Nomor 3A Jakarta Selatan - DKI Jakarta'
            ],
            [
                'key'   => 'brandname',
                'value' => 'Suitcore'
            ],
            [
                'key'   => 'facebook',
                'value' => 'https://www.facebook.com/suitmedia'
            ],
            [
                'key'   => 'googleplus',
                'value' => 'https://plus.google.com/+suitmedia'
            ],
            [
                'key'   => 'twitter',
                'value' => 'https://twitter.com/suitmedia'
            ],
            [
                'key'   => 'latitude',
                'value' => '-6.27636'
            ],
            [
                'key'   => 'legalname',
                'value' => 'SuitCore'
            ],
            [
                'key'   => 'longitude',
                'value' => '106.82429'
            ],
            [
                'key'   => 'phone',
                'value' => '021-123456789'
            ]
        ];

        \DB::table('settings')->insert($data);
    }
}
