<?php

use Illuminate\Database\Seeder;

class WholeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 200000)->create();
    }
}
