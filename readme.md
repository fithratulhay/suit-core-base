# Suitcore-Base #


## Description ##
Suitcore-Base is web base-application that support rapid application development based on Laravel 5.5 Framework. Suitcore is standard application core to build an application based on client requirement. This PHP version build on top of Laravel Framework with PDO database interfaces.

Version : 1.0_php


### Folder Structure(s) ###
```
CLIENT_APPS extends SUITCORE
|- .git : Folder repository
|- _frontend : Frontend team placed their codes here (another naming may occured)
|- app
|  |- Console : Standard Laravel for CLI
|  |- Events : Standard Laravel for Event
|  |- Exceptions : Standard Laravel for Exception Handler
|  |- Helpers : Suitcore additional helper
|  |- Http
|  |   |- Controllers
|  |   |   |- Api : All class should extends Suitcore\Controllers\ApiController,
|  |   |   |        can contain BaseController for additional behaviour layer
|  |   |   |- Auth : Related to authentication
|  |   |   |- Backend : All class should extends Suitcore\Controllers\BackendController,
|  |   |   |            can contain base BaseController for additional behaviour layer
|  |   |   |- Frontend : All class should extends Suitcore\Controllers\FrontendController,
|  |   |   |             can contain base BaseController for additional behaviour layer
|  |   |   |- Mobile : All class should extends Suitcore\Controllers\FrontendController,
|  |   |               can contain base BaseController for additional behaviour layer
|  |   |- Middleware : Standard Laravel for Middleware Path, add more based on your implementation
|  |   |- Requests : Standard Laravel for Request Path, add more based on your implementation
|  |   |- Kernel.php : Middleware stack initialization, invoked from Laravel Application Boot
|  |- Jobs : Standard Laravel path for Jobs definition
|  |- Listeners : Standard Laravel path for Listeners definition
|  |- Models : New model placed here and should extends Suitcore\Models\SuitModel 
|  |           (for eloquent-based model)
|  |- Notifications : New notification implementation (for notifiable object) placed here
|  |- Policies : Standard Laravel path for Policies definition, add more based on your implementation
|  |- Providers : Any service provider definition, add more based on your implementation
|  |- Repositories : New repository related to new Model on App\Model should be placed here and
|  |  |              implements Suitcore\Repositories\Contract\SuitRepositoryContract or its childs
|  |  |              in App\Repositories\Contract, use Suitcore\Repositories\Traits\SuitRepositoryTrait 
|  |  |              for fast implementation
|  |  |- Contract : New repository contract / interface definition if needed, should extends
|  |                Suitcore\Repositories\Contract\SuitRepositoryContract or its childs
|  |- Suitcore : Suitcore "SHIP" module, handling basic model/controller/repository/serviceprovider and 
|  |             any addon like datatable representation, thumbnailer, image thumbnail handler, excel and 
|  |             other common feature as a trait implementation.
|  |- Suitsite : Suitcore-based container / additional module, handling model/controller/repository and  
|  |             serviceprovider that related to common website implementation
|  |- Suitcommerce : Suitcore-based container / additional module, handling model/controller/repository   
|  |                 and serviceprovider that related to common web e-commerce implementation
|  |- Suitdonation : Suitcore-based container / additional module, handling model/controller/repository   
|  |                 and serviceprovider that related to common donation web implementation
|  |- Suitpay : Suitcore-based container / additional module, handling model/controller/repository and  
|               serviceprovider that related to common payment method / payment gateway
|- bootstrap
|- config
|- database
|  |- factories
|  |- migrations : Base database migration of current application / suitcore instances placed here
|  |  |- suitcore : Base database migration of suitcore "SHIP" module
|  |  |- suitsite : Base database migration of suitsite container module
|  |  |- suitcommerce : Base database migration of suitcommerce container module
|  |  |- suitdonation : Base database migration of suitdonation container module
|  |  |- suitpay : Base database migration of suitpay container module
|  |- seeds : Base database data seeding placed here
|- deploy : Capistrano deploying config, changes based on staging and production server settings,
|                ignored if using deployer
|- log : Capistrano log path, ignored if using deployer
|- public
|  |- metronics : This folder contain basic themes for metronics based admin panel
|  |  |- css
|  |  |- fonts
|  |  |- img
|  |  |- js
|  |- files : Uploaded file on runtime placed here, in capistrano / deployer this folder is shared area
|  |          for every deployed apps in server, excluded from versioning (git)
|  |- assets : This folder contain themes for frontend website. This folder should be the same content 
|  |  |          with _frontend\assets, you can make it as softlink (ln -s command in unix based system)
|  |  |- css
|  |  |- fonts
|  |  |- img
|  |  |- js
|  |- mobile : This folder contain themes for mobile website. This folder should be the same content with
|     |        _frontend\mobile\assets, you can make it as softlink (ln -s command in unix based system)
|     |- css
|     |- fonts
|     |- img
|     |- js
|- resources
|  |- lang : For label translation used in backend/frontend/mobile UI/UX, based on languages (en, id, etc)
|  |- views
|     |- backend : Backend admin portal views, for basic CRUD can extends suitcore/*/partials/create,
|     |            suitcore/*/partials/edit, suitcore/*/partials/form, suitcore/*/partials/view and
|     |            suitcore/*/partials/list, otherwise can extends suitcore/*/layouts/base or others
|     |- emails : Email templated used in system
|     |- errors : Error page layout (Error 404, 50x, etc. Change / adapt if needed)
|     |- frontend : Frontend views
|     |  |- layouts : Base layout of frontend view, every frontend view will extends these layout
|     |  |- partials : Partial layout of frontend view element, related to model if the context is 
|     |                adapter design pattern where will be showed in grid, list, etc
|     |- suitcore : Suitcore backend admin portal base view
|     |  |- metronics1
|     |  |  |- layouts : Base layout of metronics-based admin panel view, every view with metronic1 themes |     |  |  |            will extends these layout
|     |  |  |- partials : Partial implementation of basic CRUD, datatable views and form-input
|     |- mobile
|        |- layouts : Base layout of mobile view, every mobile view will extends these layout
|        |- partials : Partial layout of mobile view element, related to model if the context is 
|                      adapter design pattern where will be showed in grid, list, etc
|- routes : 
|  |- web_backend.php : Backend / admin route
|  |- api.php : API route
|  |- web_frontend.php : Frontend desktop web route (responsive / not responsive layout)
|  |- web_mobile.php : Frontend mobile web route
|- storage : Used by laravel runtime for storages
|- tests : Unit Test Playground
|- vendor : Vendor folder that be used by packages that downloaded from library repository using composer,
|                excluded from versioning (git)
|- .env : Environmet variable that used as global main settings (environment type, database, api key, etc),
|            excluded from versioning (git)
|- .env.example : Example of .env, included in versioning (git)
|- deploy.php : Deployer script (for deployment)
|- Capfile : Capistrano config
|- readme.md : Project readme / brief
|- .gitignore : File or folder path that must excluded from versioning (git)
```


## Short Initialization ##
* initialize .env based on .env.example
* composer install
* php artisan key:generate
* php artisan optimize
* php artisan migrate --path /database/migrations/suitcore
* php artisan migrate --path /database/migrations/suitsite (for sample apps instances works)
* php artisan migrate
* php artisan db:seed


## Official Documentation ##
Documentation for the framework can be found on the [Google docs](https://docs.google.com).


## Database Table / Model ##
...


## Basic Standard Operation Procedure (SOP) ##
* Don't change Suitcore "SHIP" module folder/files or related 'container', any changes to "ship" core or container should be placed as issue and discussed later, or implement in a new branches.
* All controller must use related/needed injected repository contract (supplied by related service provider) instead of make direct query builder to model's object instances.
* Override related repository and/or base model can be done by extends related class and change default
class in config with the new one (config/suitcore, config/suitsite, config/suitpay, etc).
* BackendController already have standard action (CRUD) : datatable list, create, update, delete and detail. You can gain it by duplicate backend view from views/backend/default, extend and adapt base BackendController config.
* If client have new module, make new related model (extends SuitModel, don't forget to define attributeSettings) on app/Models, related repository and contracts that use related model in app/Repository, related backend controller (extends BackendController) in app/Http/Controllers/Backend and related basic CRUD view that extends suitcore/*/partials/create, suitcore/*/partials/edit, suitcore/*/partials/form, suitcore/*/partials/view and suitcore/*/partials/list
* Any interesting module in point 5 that could be refactored/included to base suitcore will be discussed later for integration/refactoring.


## Implementation & Folder Structure ##
### Step(s) ###
* Clone Suitcore if you don't have this repository on your local web root folder (example /var/www/suitcore)
* Clone newly created SuitcoreApplicationInstances empty repository to your local web root folder (example /var/www/suitcoreinstances)
* Copy Suitcore content (include file .gitignore, exclude folder .git) to SuitcoreApplicationInstances
* Create database needed
* Copy .env.example to .env and update needed base configuration (database config, etc)
* Run composer install
* Run php artisan key:generate
* Run php artisan optimize
* Run php artisan migrate --path /database/migrations/suitcore
* Run php artisan migrate --path /database/migrations/suitsite (for sample apps instances works)
* Run php artisan migrate
* Run php -d memory_limit=2G artisan db:seed
* Edit readme.md to make clear summary of project descripiton
* Push all code to by run : git commit -m "initial instances code" & git push origin master
* Setting web server and access new suitcore apps-instances from configured path/domain
* Frontend developer should use different branches (example, branch 'frontend') from backend developer
* Backend developer can pull frontend code from frontend branch to current branch (example, git pull origin frontend)
* Backend developer make softlink of _frontend/assets to public/assets
* Backend developer start to integrating frontend layout/views, happy coding :)


## Security Vulnerabilities ##
If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

If you discover a security vulnerability within SuitCore, please send an e-mail to Suitcore Developer at suitcore.developer@gmail.com. All security vulnerabilities will be promptly addressed.


## License ##
The Laravel framework and Suitcore Base Application is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
