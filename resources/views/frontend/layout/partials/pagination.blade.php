@if($paginator && method_exists($paginator, 'appends'))
    @if($paginator->lastPage() > 1)
        <?php 
            $pagePadding = isset($pagePadding) ? $pagePadding : 3;
            $previousPage = ($paginator->currentPage() > 1 ? $paginator->currentPage() - 1 : 1); 
            $nextPage = ($paginator->currentPage() < $paginator->lastPage() ? $paginator->currentPage() + 1 : $paginator->lastPage()); 
        ?>
        <ul class="pagination text-center">
    		<li><a href="{{ $paginator->currentPage() == 1 ? '' : $paginator->appends(request()->except(['_token', 'filters']))->url($previousPage) }}" class="{{ $paginator->currentPage() == 1 ? 'disabled' : '' }}"><span class="fa fa-angle-left"></span></a></li>
            @if($paginator->currentPage()-$pagePadding > 1)
                <li>
                    <a href="{{ $paginator->appends(request()->except(['_token', 'filters']))->url(1) }}">
                        <span>1</span>
                    </a>
                </li>
                @if($paginator->currentPage()-$pagePadding > 2)
                    <li>
                        ...
                    </li>
                @endif
            @endif
            @for ($i = max(1, $paginator->currentPage()-$pagePadding); $i <= min($paginator->lastPage(), $paginator->currentPage()+$pagePadding); $i++)
                <li>
                    <a href="{{ $paginator->appends(request()->except(['_token', 'filters']))->url($i) }}" class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
                        <span>{{ $i }}</span>
                    </a>
                </li>
            @endfor
            @if($paginator->currentPage()+$pagePadding < $paginator->lastPage())
                @if($paginator->currentPage()+$pagePadding < ($paginator->lastPage()-1))
                    <li>
                        ...
                    </li>
                @endif
                <li>
                    <a href="{{ $paginator->appends(request()->except(['_token', 'filters']))->url($paginator->lastPage()) }}">
                        <span>{{ $paginator->lastPage() }}</span>
                    </a>
                </li>
            @endif
    		<li><a href="{{ $paginator->currentPage() == $paginator->lastPage() ? '' : $paginator->appends(request()->except(['_token', 'filters']))->url($paginator->currentPage()+1) }}" class="{{ $paginator->currentPage() == $paginator->lastPage() ? 'disabled' : '' }}"><span class="fa fa-angle-right"></span></a></li>
            <!-- pagination -->
        </ul>
    @else
        <ul class="pagination text-center">
            <li><a href="#" class="disabled"><span class="fa fa-caret-left"></span></a></li>
            <li class="active"><a href="#" class="disabled"><span>1</span></a></li>
            <li><a href="#" class="disabled"><span class="fa fa-caret-right"></span></a></li>
            <!-- default-pagination -->
        </ul>
    @endif
@endif
