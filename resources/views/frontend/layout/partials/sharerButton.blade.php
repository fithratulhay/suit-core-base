<div class="text-center" style="padding-top: 48px; padding-bottom: 48px;">
	<strong>Share To</strong>
    <div class="share-socials" data-url="{{ request()->url() }}" data-text="{{ $pageTitle or settings('legalname', 'Site') }}"  data-button='["facebook", "twitter", "googleplus", "pinterest", "whatsapp", "line", "email"]'></div>
</div>
