<!doctype html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head id="Head1">
    @if(config('suitcore.bust_iframe'))
    <style id="antiClickjack">body{display:none !important;}</style>
    <script type="text/javascript" src="/js/owasp-framebuster.min.js"></script>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="SuitApplications is the upcoming megapolitan of Pekanbaru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    
    <link href="{{ Theme::url('img/apple-icon.png') }}" rel="apple-touch-icon" type="image/png"/>
    <link href="{{ Theme::url('img/favicon.png') }}" rel="shortcut icon" type="image/png"/>

    <title>{{ $title or 'Site' }} | Home</title>
    
    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/main.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/vendor/jquery.tiltShift.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/vendor/jquery.growl.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/vendor/js-socials/jssocials.css') }}">
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/modernizr.min.js') }}"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    @yield('style-head')
    @yield('script-head')
</head>
<body>
    <!--[if lt IE 9]>
        <p class="browsehappy" style="position: absolute; z-index: 1000; padding: 5px; width: 100%;">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <script>alert("You are using Internet Explorer lower than version 9. Please upgrade your browser to improve your experience.");</script>
    <![endif]-->

    <header class="site-header" role="banner">
        <div class="container cf">
            <a class="anchor-logo" href="/">
                <img class="logo" src="{{ asset('img/logo-landscape-white.png') }}" alt="">
            </a>

            <button class="nav-trigger" type="button">
                <span>Menu</span>
                <span class="nav-trigger__icon fa fa-fw"></span>
            </button>
            
            <nav class="site-nav">
                <h2 class="sr-only">Site's main navigation</h2>
                <ul class="nav-list nav-list--main cf">
                    <li class="nav-list__item">
                        <a class="nav-anchor" href="/">
                            <span class="sr-only">Beranda</span>
                            <span class="fa fa-fw fa-lg fa-home text-orange"></span>
                        </a>
                    </li>
                    <?php
                        $current = url()->current();
                        $mainMenus = menu_items_by_type('header');
                    ?>
                    @foreach ($mainMenus as $mainMenu)
                        <li class="nav-list__item">
                            <a class="nav-anchor {{ str_contains($current, $mainMenu->url) ? 'is-active' : '' }}" href="{{ count($mainMenu->children) > 0 ? '#' : $mainMenu->url }}" {{ str_contains($mainMenu->url, 'http') ? 'target="_blank"' : '' }}>{{ $mainMenu->label }}</a>
                            @if (count($mainMenu->children) > 0)
                                <?php $subMainMenus = submenu_by_type('header', false, $mainMenu->id); ?>
                                <ul class="nav-list nav-list--sub">
                                @foreach ($subMainMenus as $subMainMenu)
                                    <li class="nav-list__item">
                                        <a class="nav-anchor" href="{{ $subMainMenu->url }}" {{ str_contains($subMainMenu->url, 'http') ? 'target="_blank"' : '' }}>{{ $subMainMenu->label }}</a>
                                    </li>
                                @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                    <li class="nav-list__item nav-list__item--inline">
                        <a class="nav-anchor" href="#" data-toggle-form-search>
                            <span class="sr-only">Search</span>
                            <span class="fa fa-fw fa-lg fa-search"></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <!-- site-header -->

    <div class="form-search" role="search">
        <div class="form-search-container">
            <label class="sr-only" for="inputSearch">Cari</label>
            <input class="form-input form-input--block form-search-input" id="inputSearch" onkeypress="handle(event)" type="search" placeholder="Cari..." />
            <button class="form-search-cancel-btn btn-plain" type="button" data-toggle-form-search>
                <span class="sr-only">Batal</span>
                <span class="fa fa-fw fa-close"></span>
                <span>Tutup</span>
            </button>
        </div>
    </div>
    <!-- search-bar -->

    @yield('featured-content')

    <div class="container">
        <main class="{{ ((isset($layoutType) && $layoutType == "home") ? "home-main" : "common-main") }}" id="main">
            @yield('content')
        </main>
    </div>
    <!-- site-main -->

    <div class="container-footer">
        <div class="container bzg">
            <?php
                $footerMenus = menu_items_by_type('footer_nav');
            ?>
            <div class="bzg_c" data-col="l9">
            @foreach($footerMenus as $menuSection)
                <div class="sub-container-footer">
                    <div class="menu-footer-heading"><a href="{{ $menuSection->url }}">{{ $menuSection->label }}</a></div>
                    <ul class="ui-list-footer">
                    @foreach(submenu_by_type('footer_nav', false, $menuSection->id) as $menu)
                        <li class="ui-list-footer__item"><a href="{{ $menu->url }}">{{ $menu->label }}</a></li>
                    @endforeach
                    </ul>
                </div>
            @endforeach
            </div>
            <div class="bzg_c" data-col="l3">
                @if(!empty(settings('facebook', '')))
                    <a href="{{ settings('facebook', '') }}" class="block" target="_blank">
                        <span class="fa fa-fw fa-lg fa-facebook">&nbsp;</span>
                    </a>
                @endif
                @if(!empty(settings('twitter', '')))
                    <a href="{{ settings('twitter', '') }}" class="block" target="_blank">
                        <span class="fa fa-fw fa-lg fa-twitter">&nbsp;</span>
                    </a>
                @endif
                @if(!empty(settings('youtube', '')))
                    <a href="{{ settings('youtube', '') }}" class="block" target="_blank">
                        <span class="fa fa-fw fa-lg fa-youtube">&nbsp;</span>
                    </a>
                @endif
                @if(!empty(settings('instagram', '')))
                    <a href="{{ settings('instagram', '') }}" class="block" target="_blank">
                        <span class="fa fa-fw fa-lg fa-instagram">&nbsp;</span>
                    </a>
                @endif
                <br>&nbsp;
                <hr>
                <form method="post" action="{{ route('frontend.newsletter.subscribe') }}">
                    <div class="form-row">
                        <div class="bzg">
                            <div class="bzg_c" data-col="l12">
                                <label class="label-inline" for="email">Subscribe for Newsletter</label>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="text" class="form-input form-input--block" id="email" name="email" placeholder="type your email address ..." required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="bzg">
                            <div class="bzg_c" data-col="l12">
                                <input type="submit" class="btn btn--block btn--red" value="Subscribe">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container bzg">
            <footer class="site-footer">
                <small class="text-muted">Copyright &copy; 2017 SuitApplications, Hak Cipta Dilindungi oleh Undang-Undang</small>
            </footer>
        </div>
    </div>
    <!-- site-footer -->
    
    <script type="text/javascript">        window.myPrefix = '';</script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/jquery.growl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/fastclick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/highcharts.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/highcharts.3d.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/jquery.tooltip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/jquery.datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/pikaday.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/lightbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/jssocials.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/vendor/jquery.growl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/helper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/main.min.js') }}"></script>

    <script type="text/javascript">
        @if(session()->has('danger'))
            $.growl.error({ title: "", message: "{{ session()->pull('danger') }}" });
        @elseif(session()->has('success'))
            $.growl.notice({ title: "", message: "{{ session()->pull('success') }}" });
        @elseif(session()->has('info'))
            $.growl.warning({ title: "", message: "{{ session()->pull('info') }}" });
        @elseif(session()->has('message'))
            $.growl({ title: "", message: "{{ session()->pull('message') }}" });
        @endif
    </script>

    <script type="text/javascript">
        function handle(e) {
            if (e.keyCode === 13) {
                searching();
                e.preventDefault();
                return false;
            }

            return false;
        }

        function searching() {
            var keyword = $('#inputSearch').val();

            if (keyword != '') {
                var route = '{{route('frontend.home.content.search')}}?keyword=' + encodeURI(keyword);
                window.location = route;
            }
        }
    </script>
    @if(!empty(env('ONESIGNAL_APP_ID', '')))
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "{{ env('ONESIGNAL_APP_ID', '') }}",
                });
            @if(auth()->check())
                OneSignal.getUserId(function(registrationId) { 
                    if (registrationId) {
                        $.ajax({
                            url: "{{ route('frontend.user.pushregistration') }}",
                            type: 'post',
                            dataType: 'json',
                            // contentType: 'application/json',
                            data: 'user_id={{ auth()->user()->id }}&registration_id=' + registrationId + '&device_type=browser&_token={{ csrf_token() }}',
                            success: function success(data) {
                                if (data.result == 'success') {
                                    // change further state if any
                                } else {
                                    // change further state if any
                                }
                            }
                        });  
                    }
                });
            @endif
            });
        </script>
    @endif
    @yield('style-footer')
    @yield('script-footer')
</body>
</html>
