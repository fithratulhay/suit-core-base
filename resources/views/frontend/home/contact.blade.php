@extends('frontend.layout.base')

@section('content')
    <div class="container">
        <main class="common-main">
            <ul class="breadcrumb">
                <li>Kontak Kami</li>
            </ul>

            <article class="cf">
                <div class="bzg">
                    <div class="bzg_c" data-col="l9">
                        <h1>Kontak Kami</h1>
                    </div>
                </div>

                <div class="bzg">
                    <div class="bzg_c" data-col="l9">
                        {!!Form::open(['data-validate-form'])!!}
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l3">
                                        <label class="label-inline" for="name">Nama</label>
                                    </div>
                                    <div class="bzg_c" data-col="l7">
                                        <input class="form-input" style="width: 100%;" id="name" placeholder="Nama" name="sender_name" type="text" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l3">
                                        <label class="label-inline" for="email">Email</label>
                                    </div>
                                    <div class="bzg_c" data-col="l7">
                                        <input class="form-input" style="width: 100%;" id="email" placeholder="Email" name="sender_email" type="email" required />
                                    </div>
                                </div>
                            </div>
                            <div class="bzg">
                                <div class="bzg_c" data-col="l3">
                                    <label class="label-inline" for="about">Perihal</label>
                                </div>
                                <div class="bzg_c" data-col="l7">
                                    <select class="form-input" style="width: 100%;" id="about" name="category">
                                        <option value="">-- perihal --</option>
                                    @foreach($contactcategory as $key=>$label)
                                        <option value="{{ $key }}">{{ $label }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l10">
                                        <label class="label-inline" for="message">Pesan</label>
                                    </div>
                                    <div class="bzg_c" data-col="l10">
                                        <textarea class="form-input" style="width: 100%;" id="message" cols="8" rows="10" placeholder="Pesan Anda" name="content"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l10" data-offset="l9">
                                        <button class="btn btn--md btn--red">Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="bzg_c" data-col="l3">
                        
                    </div>
                </div>
            </article>
            
        </main>
    </div>
@stop
