@extends('frontend.layout.base')

@section('style-head')
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <style>
        .ucontainer {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .ucontent {
            text-align: center;
            display: inline-block;
        }

        .utitle {
            font-size: 96px;
            font-weight: 100;
            font-family: 'Lato';
        }

        .site-banner-slider-item {
            background-color: #444;
        }

        .site-banner-slider-item__img {
            opacity: .6;
        }

        .site-banner-slider-item__caption__title {
            background: none;
            text-shadow: 1px 1px 2px rgb(72, 67, 67);
            text-transform: none;
        }
        
        @include('frontend.partials.event_css')

    </style>
@stop

@section('content')
<div class="container">
    <main class="home-main" id="main">
        <p class="welcome-text text-center">Default landing page ...</p>
        <hr>
    </main>
</div>

@stop
