@extends('frontend.layout.base')

@section('style-head')
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <style>
        .ucontainer {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .ucontent {
            text-align: center;
            display: inline-block;
        }

        .utitle {
            font-size: 96px;
            font-weight: 100;
            font-family: 'Lato';
        }

        .site-banner-slider-item {
            background-color: #444;
        }

        .site-banner-slider-item__img {
            opacity: .6;
        }

        .site-banner-slider-item__caption__title {
            background: none;
            text-shadow: 1px 1px 2px rgb(72, 67, 67);
            text-transform: none;
        }

    </style>
@stop

@section('featured-content')
    <div class="site-banner">
        <div class="site-banner-slider" id="mainSlider">
        @if($mainBanners && count($mainBanners) > 0)
            @foreach($mainBanners as $banner)
                <div>
                    <figure class="site-banner-slider-item">
                        <img class="site-banner-slider-item__img" src="{{ $banner->image_large_cover }}" alt="{{ $banner->title }}">
                        <figcaption class="site-banner-slider-item__caption">
                            <span class="label label--blue">{{ $banner->subtitle }}</span>
                            <h1 class="site-banner-slider-item__caption__title">{{ $banner->title }}</h1>
                        </figcaption>
                    </figure>
                </div>
            @endforeach
        @else
            <div>
                <figure class="site-banner-slider-item">
                    <img class="site-banner-slider-item__img" src="{{ asset('frontend/img/defaultbanner.jpg') }}" alt="">
                    <figcaption class="site-banner-slider-item__caption">
                        <span class="label label--blue">SuitCore</span>
                        <h1 class="site-banner-slider-item__caption__title">Alternative Way in Rapid Application Development</h1>
                    </figcaption>
                </figure>
            </div>
        @endif
        </div>
        <div class="main-slider-mask sr-only"></div>
    </div>
    <a class="home-arrow-down" href="#main" id="main-page">
        <span class="fa fa-fw fa-2x fa-chevron-down"></span>
    </a>
@stop

@section('content')
<div class="container">
    <main class="home-main" id="main">
        <div class="bzg">
            <div class="bzg_c" data-col="l9">
            @if($contentShowcase && is_array($contentShowcase) && count($contentShowcase) > 0)
            @foreach($contentShowcase as $showcase)
                @if(isset($showcase['type']) && $showcase['type'] && $showcase['type']->code != 'static' && isset($showcase['contents']) && $showcase['contents'] && count($showcase['contents']) > 0)
                    <section>
                        <h2 class="text-uppercase h3">Recent {{ $showcase['type']->name }}</h2>

                        <ul class="bzg list-nostyle">
                            @foreach($showcase['contents'] as $content)
                            <li class="bzg_c" data-col="s6,l4">
                                <a class="thumbnail-anchor" href="{{ route('frontend.home.content.dynamic', ['type' => $showcase['type']->code, 'slug'=>$content->slug]) }}">
                                    <figure class="thumbnail">
                                        <img class="thumbnail__img" src="{{ $content->thumbnail ? $content->thumbnail_medium_square : asset('frontend/img/defaultbanner.jpg') }}" alt="{{ $content->title }}">
                                        <figcaption class="thumbnail__caption">
                                            <strong>{{ $content->title }}</strong>
                                            <p class="thumbnail__caption__desc no-margin summary">{{ $content->highlight }}</p>
                                            <small class="text-muted"><span class="text-blue">{{ $content->created_at->format('d F Y') }}</span></small>
                                        </figcaption>
                                    </figure>
                                </a>
                                <!--
                                <figure class="thumbnail thumbnail--cover">
                                    <img class="thumbnail__img" src="{{ $content->thumbnail ? $content->thumbnail_medium_square : asset('frontend/img/defaultbanner.jpg') }}" alt="{{ $content->title }}">
                                    <figcaption class="thumbnail__caption thumbnail__caption--cover">
                                        <h3 class="text-uppercase h2">{{ $content->title }}</h3>
                                        <a class="btn btn--sm btn--outline btn--white" href="{{ route('frontend.home.content.dynamic', ['type'=> $showcase['type']->code, 'slug'=>$content->slug]) }}">Read more</a>
                                    </figcaption>
                                </figure>
                                -->
                            </li>
                            @endforeach
                        </ul>

                        <div class="block text-center">
                            <a class="btn btn--outline btn--red" href="{{ route('frontend.home.content.dynamic', ['type' => $showcase['type']->code]) }}">Lihat Semua</a>
                        </div>
                    </section>
                @endif
            @endforeach
            @endif
            </div>
            <div class="bzg_c" data-col="l3">
            @if($sideLinks = menu_items_by_type('side_nav'))
            @if(count($sideLinks) > 0)
                <section>
                    <h2 class="text-uppercase h3">Tautan</h2>

                    <ul class="ui-list">
                    @foreach($sideLinks as $menu)
                        <li class="ui-list__item">
                            <a href="{{ $menu->url}}" data-tooltip="{{ $menu->label }}">{{ $menu->label }}</a>
                        </li>
                    @endforeach
                    </ul>

                    <!-- <div class="block">
                        <a class="btn btn--block btn--outline btn--red" href="#">Lihat Semua</a>
                    </div> -->
                </section>
            @endif
            @endif
            </div>
        </div>
    </main>
</div>

@stop
