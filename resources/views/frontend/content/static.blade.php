@extends('frontend.layout.base')

@section('content')
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li>{{ $content->title__trans }}</li>
</ul>

<article class="cf">
    <div class="bzg">
        <div class="bzg_c" data-col="l8">
            <h1>{{ $content->title__trans }}</h1>
        </div>
    </div>

    <div class="bzg">
        <div class="bzg_c" data-col="l12">
        @if($content->image)
            <figure>
                <img src="{{ $content->image_large_cover }}" alt="{{ $content->title__trans }}">
            </figure>
        @endif
        
            {!! htmlspecialchars_decode($content->content__trans) !!}

            @include('frontend.layout.partials.sharerButton')
        </div>
    </div>
</article>
@endsection
