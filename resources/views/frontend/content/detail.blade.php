@extends('frontend.layout.base')

@section('content')
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode]) }}">{{ $title }}</a></li>
    <li>{{ $content->title__trans }}</li>
</ul>

<article class="cf">
    <time class="text-muted"><small>{{ $content->created_at->format('d F Y') }}</small></time>
    <div class="bzg">
        <div class="bzg_c" data-col="l8">
            <h1>{{ $content->title__trans }}</h1>
        </div>
    </div>

    <div class="bzg">
        <div class="bzg_c" data-col="l8">
            <figure>
                <img src="{{ $content->image_large_cover }}" alt="{{ $content->title__trans }}">
            </figure>

            {!! htmlspecialchars_decode($content->content__trans) !!}

        	@include('frontend.layout.partials.sharerButton')
        </div>
        <div class="bzg_c" data-col="l4">
        @if(isset($recentList) && $recentList && count($recentList) > 0)
            <aside>
                <h2 class="text-uppercase">Recent {{ ucwords($title) }}</h2>

                <ul class="ui-list list-nostyle">
                	<?php
                		$firstRecent = false;
                	?>
                	@foreach($recentList as $recentContent)
                    <li class="ui-list__item">
                        <a href="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode, 'slug'=>$recentContent->slug]) }}">
                        @if(!$firstRecent)
                            <figure class="no-margin">
                                <img src="{{ $recentContent->thumbnail_medium_cover }}" alt="{{ $recentContent->title__trans }}">
                                <figcaption>{{ $recentContent->title__trans }}</figcaption>
                            </figure>
                            <?php
                				$firstRecent = true;
                            ?>
                        @else
                            {{ $recentContent->title__trans }}
                      	@endif
                        </a>
                    </li>
                    @endforeach
                </ul>

                <div class="block">
                    <a class="btn btn--block btn--outline btn--red" href="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode]) }}">View all</a>
                </div>
            </aside>
       	@endif

            <section>
                <h2 class="text-uppercase h3">Tautan</h2>

                <ul class="ui-list">
                    @foreach(menu_items_by_type('side_nav') as $menu)
                        <li class="ui-list__item">
                            <a href="{{ $menu->url}}" data-tooltip="{{ $menu->label }}">{{ $menu->label }}</a>
                        </li>
                    @endforeach
                </ul>
            </section>
        </div>
    </div>
</article>
@endsection
