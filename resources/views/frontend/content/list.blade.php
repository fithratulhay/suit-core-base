@extends('frontend.layout.base')

@section('content')
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li>
    @if($contentCategory)
        <li><a href="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode]) }}">{{ $title }}</a></li>
        <li>{{ $contentCategory->name }}</li>
    @else
        <li>{{ $title }}</li>
    @endif
    </ul>
    
    <article class="cf">
        <h1 class="main-heading">{!! $title . ($contentCategory ? " <span style='color: black;'>|</span> " . $contentCategory->name : "") !!}</h1>
        <hr>

        @if(isset($contentCategories) && $contentCategories && count($contentCategories) > 0)
        <div class="bzg">
            <div class="bzg_c" data-col="m3" data-offset="m9">
                <form class="block" method="get">
                    <label class="sr-only" for="filterCategory">Category</label>
                    <select class="form-input form-input--block" id="filterCategory" onChange="window.location=this.value">
                        <option value="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode]) }}">All {{ $title }}</option>
                    @foreach($contentCategories as $category)
                        <option value="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode]) . '?cat=' . $category->slug }}" @if($contentCategory && $contentCategory->id == $category->id) selected @endif>{{ $category->name }}</option>
                    @endforeach
                    </select>
                </form>
            </div>
        </div>
        @endif

        @if (count($contents))
            <ul class="bzg list-nostyle">
            @foreach ($contents as $content)
                <li class="bzg_c" data-col="s6,m3">
                    <a class="thumbnail-anchor" href="{{ route('frontend.home.content.dynamic', ['type'=>$routeNode, 'slug'=>$content->slug]) }}">
                        <figure class="thumbnail">
                            <img class="thumbnail__img" src="{{ $content->image_medium_cover }}" alt="{{ $content->title__trans }}">
                            <figcaption class="thumbnail__caption">
                                <time class="text-muted"><small>{{ $content->created_at->format('d F Y') }}</small></time> <br>
                                <b>{{ $content->title__trans }}</b>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            @endforeach
            </ul>
        @else
            <div class="bzg">
                <div class="block text-center" style="padding-top: 48px; padding-bottom: 48px;">
                    <h3 class="product-not-found">No {{ $title }} Yet !</h3>                    
                </div>
            </div>
        @endif
            <div class="bzg">
                <div class="block text-center">
                    @include('frontend.layout.partials.pagination', ['paginator' => $contents])
                </div>
            </div>
    </article>
@endsection
