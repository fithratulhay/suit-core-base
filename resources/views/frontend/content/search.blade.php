@extends('frontend.layout.base')

@section('content')
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li>Search Result</li>
    </ul>
    
    <article class="cf">
        <h1 class="main-heading">Search Result for "{{ $keyword }}" ...</h1>
        <hr>

        @if (count($contents))
            <ul class="bzg list-nostyle">
            @foreach ($contents as $content)
                <li class="bzg_c" data-col="s6,m3">
                    <a class="thumbnail-anchor" href="{{ route('frontend.home.content.dynamic', ['type'=>($content->type ? $content->type->code : 'unknown'), 'slug'=>$content->slug]) }}">
                        <figure class="thumbnail">
                            <img class="thumbnail__img" src="{{ $content->image_medium_cover }}" alt="{{ $content->title__trans }}">
                            <figcaption class="thumbnail__caption">
                                <time class="text-muted"><small>{{ $content->created_at->format('d F Y') }}</small></time> <br>
                                <b>{{ $content->title__trans }}</b>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            @endforeach
            </ul>
        @else
            <div class="bzg">
                <div class="block text-center" style="padding-top: 48px; padding-bottom: 48px;">
                    <h3 class="product-not-found">Content not found!</h3>                    
                </div>
            </div>
        @endif
            <div class="bzg">
                <div class="block text-center">
                    @include('frontend.layout.partials.pagination', ['paginator' => $contents])
                </div>
            </div>
    </article>
@endsection
