<!-- Input Time Range -->
@if($groupFormSetting['type'] == \Suitcore\Models\SuitModel::GROUPTYPE_TIMERANGE)
<div class="form-group {{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['errors'] || $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['errors'] ? 'has-error' : '' }}" id="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}">
    <label class="col-md-3 control-label" for="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['id'] }}">{{ isset($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['label']) ? $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['label'] : ucwords(str_replace("_", " ", $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'])) }}</label>
    <div class="col-md-9">
        <div class="range-input text-center clearfix">
        @foreach($groupFormSetting['elmt'] as $role=>$formSetting)
            @if(in_array($role, [\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START, \Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]))
                <div class="input-group input-medium col-md-5 pull-left">
                    <span class="input-group-addon">{{ $role == \Suitcore\Models\SuitModel::GROUPROLE_RANGE_START ? 'From' : 'To' }}</span>
                    <input class='form-control' id='{{ $formSetting['id'] }}' type='time' name='{{ $formSetting['name'] }}' value='{{ $formSetting['value'] }}' {{ $formSetting['required'] ? 'required' : '' }}>
                </div>
                @if($role == \Suitcore\Models\SuitModel::GROUPROLE_RANGE_START)
                <div class="col-md-1 pull-left text-center">
                    <i class="control-label text-orange fa-2x fa fa-long-arrow-right"></i>
                </div>
                @endif
            @endif
        @endforeach
        </div>
        @foreach($groupFormSetting['elmt'] as $role=>$formSetting)
            @if(in_array($role, [\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START, \Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]))
                @if($formSetting['errors'])
                    <div class="form-control-focus">{{ $formSetting['errors'] ? $formSetting['errors'] : "" }}</div>
                @endif
            @endif
        @endforeach
    </div>
</div>
@endif
