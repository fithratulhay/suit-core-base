@extends('suitcore.metronics6.layouts.base')

@section ('style-head')
    <link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
@stop

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="row">
      <div class="col-md-12">
          <!-- Begin: life time stats -->
          <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                  <div class="caption">
                      <i class="{{ $pageIcon or 'icon-badge'}} font-dark"></i>&nbsp;
                      <span class="caption-subject font-dark sbold uppercase">Import {{ $baseObject->_label }} Data From XLS</span>
                  </div>
            </div>
            <div class="portlet-body">
            <div class="table-container">
                <div id="import-progress" style="display: none; padding: 16px; border: 1px solid #AAAAAA; background-color: #FAFAFA; margin: 8px 8px 16px 8px;">
                    <div class="progress progress-striped active">
                        <div id="import-progress-bar" class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        </div>
                    </div>
                    <strong>Current Import Process</strong> : <span id="import-progress-bar-imported">0</span> of <span id="import-progress-bar-sources">0</span> (<span id="import-progress-bar-percentage">0</span>%), <span id="import-progress-bar-successfull">0</span> imported in <i><span id="progres-time" class>00:00:00</span></i> (<i><span id="progres-estimated-remaining-time">00:00:00</span></i> estimated remaining time)<br>
                    <i>( process will still continue, even if you close this window/tab or leave this page )</i>

                    @if( Route::has($routeBaseName . '.importstop') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.importstop'])
                        {!! Form::model($baseObject, ['id'=>class_basename($baseObject) . '_import_stop', 'route' => $routeBaseName . '.importstop', 'class' => 'form-horizontal', 'style' => 'text-align: right; margin-top: 8px;']) !!}
                            <button class="btn btn-sm red active" type="submit" onClick="return confirm('Are you sure to stop this process?');">
                                <i class="fa fa-times">&nbsp;</i> Stop Current Import Process
                            </button>
                        {!! Form::close() !!}
                    @endcan
                    @endif
                </div>

                <div id="import-message" style="padding: 16px; border: 1px solid #AAAAAA; background-color: #EAEAEA; margin: 8px 8px 16px 8px;">
                    <strong>Last Import Result</strong> : <span id="import-product-message-result">-</span>
                </div>

                <ol>
                    <li>
                        <p>Download and fill the {{ strtolower( $baseObject->_label ) }} data in the following template.</p>
                        <a class="btn btn-sm btn-primary btn-outline" href="{{route($routeBaseName . '.downloadtemplate')}}">
                            <i class="fa fa-download">&nbsp;</i> Download template
                        </a>
                        <br><br>
                        <p>Sample fill in :</p>
                        <div style="width: 100%; overflow-x: scroll;">
                            <table class="table table--zebra">
                                <thead>
                                @foreach($baseObject->attribute_settings as $attr=>$_setting)
                                @if(isset($_setting['formdisplay']) && $_setting['formdisplay'] && ( (isset($_setting['initiated']) && !$_setting['initiated']) || !isset($_setting['initiated'])))
                                    <th align="center"><b>{{ $_setting['label'] }}</b></th> 
                                @endif
                                @endforeach
                                </thead>
                                <tbody>
                                    <tr>
                                    @foreach($baseObject->attribute_settings as $attr=>$_setting)
                                    @if(isset($_setting['formdisplay']) && $_setting['formdisplay'] && ( (isset($_setting['initiated']) && !$_setting['initiated']) || !isset($_setting['initiated'])))
                                        <td><small>[{{ $_setting['type'] }}]</small></td>
                                    @endif
                                    @endforeach
                                    </tr>
                                    <tr>
                                    @foreach($baseObject->attribute_settings as $attr=>$_setting)
                                    @if(isset($_setting['formdisplay']) && $_setting['formdisplay'] && ( (isset($_setting['initiated']) && !$_setting['initiated']) || !isset($_setting['initiated'])))
                                        @if($_setting['type'] == \Suitcore\Models\SuitModel::TYPE_NUMERIC)
                                            <td>1</td>
                                        @elseif($_setting['type'] == \Suitcore\Models\SuitModel::TYPE_FLOAT)
                                            <td>1.0</td>
                                        @elseif($_setting['type'] == \Suitcore\Models\SuitModel::TYPE_BOOLEAN)
                                            <td>1</td>
                                        @elseif($_setting['type'] == \Suitcore\Models\SuitModel::TYPE_DATETIME)
                                            <td>{{ \Carbon\Carbon::now()->toDateTimeString() }}</td>
                                        @elseif($_setting['type'] == \Suitcore\Models\SuitModel::TYPE_DATE)
                                            <td>{{ \Carbon\Carbon::now()->toDateString() }}</td>
                                        @elseif($_setting['type'] == \Suitcore\Models\SuitModel::TYPE_TIME)
                                            <td>{{ \Carbon\Carbon::now()->toTimeString() }}</td>
                                        @else
                                            <td>some-text</td>
                                        @endif
                                    @endif
                                    @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                    <li>
                        <p>Upload {{ strtolower( $baseObject->_label ) }} data.</p>
                        {!! Form::model($baseObject, ['files'=> true, 'id'=>class_basename($baseObject) . '_import_form', 'route' => $routeBaseName . '.importfromtemplate', 'class' => 'form-horizontal']) !!}
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="file_url">File (*.xls)</label>
                                    <div class="col-md-6">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="hidden" value="" name="..."><input type="file" id="file_url" name="file_url" required="required"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Option</label>
                                    <div class="col-md-10">
                                        <?php
                                            $keyBaseName = "";
                                            $objAttrSettings = $baseObject->getBufferedAttributeSettings();
                                        ?>
                                        @if(is_array($baseObject->getImportExcelKeyBaseName()))
                                            @foreach($baseObject->getImportExcelKeyBaseName() as $value)
                                                <?php
                                                    $keyBaseName .= $objAttrSettings[$value]['label'] . ",&nbsp;";
                                                ?>
                                            @endforeach
                                        @else
                                            <?php
                                                $keyBaseName = $objAttrSettings[$baseObject->getImportExcelKeyBaseName()]['label'];
                                            ?>
                                        @endif
                                        <div class="md-radio-list">
                                            <div class="md-radio">
                                                <input type="radio" class="md-radiobtn" name="method" value="ignore" id="method-ignore" checked="checked">
                                                <label for="method-ignore">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Do not replace existing product that have same <b>{{ $keyBaseName }}</b> in *.xls file.</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" class="md-radiobtn" name="method" value="replace" id="method-replace">
                                                <label for="method-replace">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Replace existing product that have same <b>{{ $keyBaseName }}</b> in *.xls file.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button id="import-product-submit-button" class="btn btn-sm green-jungle active" type="submit">
                                            <i class="fa fa-upload">&nbsp;</i> Import
                                        </button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </li>
                </ol>

                <h3>Last Import Log</h3>
                <div id="import-log" style="padding: 16px; border: 1px solid #AAAAAA; background-color: #FAFAFA; margin: 16px 8px 16px 8px;"></div>
          </div>
      </div>
    </div>
    <!-- End: life time stats -->
    </div>
    </div>
    <!-- END CONTENT -->
@stop

@section('page_script')

@if( Route::has($routeBaseName . '.importprogress') )
@can('suitcorepermission', [$baseObject, $routeBaseName . '.importprogress'])
    <script>
        var progressBar = $('#import-progress-bar');
        var progressImported = $('#import-progress-bar-imported');
        var progressSources = $('#import-progress-bar-sources');
        var progressSuccesfull = $('#import-progress-bar-successfull');
        var progressPercentage = $('#import-progress-bar-percentage');
        var progressTime = $('#progres-time');
        var progressEstimatedRemainingTime = $('#progres-estimated-remaining-time');
        var lastResultMessage = $('#import-product-message-result');
        var importLogArea = $('#import-log');
        var progressSeconds = 0, progressMinutes = 0, progressHours = 0, tProgress;
        var remainingSeconds = 60, remainingMinutes = 59, remainingHours = 1, tRemaining;

        function progressTimer() {
            progressSeconds++;
            if (progressSeconds >= 60) {
                progressSeconds = 0;
                progressMinutes++;
                if (progressMinutes >= 60) {
                    progressMinutes = 0;
                    progressHours++;
                }
            }
            progressTime.html( (progressHours ? (progressHours > 9 ? progressHours : "0" + progressHours) : "00") + ":" + (progressMinutes ? (progressMinutes > 9 ? progressMinutes : "0" + progressMinutes) : "00") + ":" + (progressSeconds > 9 ? progressSeconds : "0" + progressSeconds) );
            progressTimerRefresh();
        }

        function progressTimerRefresh() {
            tProgress = setTimeout(progressTimer, 1000);
        }

        function remainingTimer() {
            remainingSeconds--;
            if (remainingSeconds < 0) {
                remainingSeconds = 0;
                remainingMinutes--;
                if (remainingMinutes < 0) {
                    remainingMinutes = 0;
                    remainingHours--;
                    if (remainingHours < 0) {
                        remainingHours = 0;
                    }
                }
            }
            progressEstimatedRemainingTime.html( (remainingHours ? (remainingHours > 9 ? remainingHours : "0" + remainingHours) : "00") + ":" + (remainingMinutes ? (remainingMinutes > 9 ? remainingMinutes : "0" + remainingMinutes) : "00") + ":" + (remainingSeconds > 9 ? remainingSeconds : "0" + remainingSeconds) );
            remainingTimeRefresh();
        }

        function remainingTimeRefresh() {
            tRemaining = setTimeout(remainingTimer, 1000);
        }

        function importProgress() {
            $.ajax({
                url: "{{ route($routeBaseName . '.importprogress') }}",
                type: 'get',
                dataType: 'json',
                contentType: 'application/json',
                success: function success(data) {
                    progressBar.attr('style', 'width: ' + data.import_product_progress_percentage + '%');
                    progressImported.html(data.import_product_imported_count);
                    progressSources.html(data.import_product_sources_count);
                    progressSuccesfull.html(data.import_product_imported_success_count);
                    progressPercentage.html(data.import_product_progress_percentage);
                    lastResultMessage.html(data.import_product_message_result);
                    importLogArea.html(data.import_product_imported_log);
                    if (data.import_product_sources_count > 0) {
                        $('#import-product-submit-button').prop('disabled', true);
                        $('#import-progress').show();

                        // progress-time
                        clearTimeout(tProgress);
                        timeComponents = data.import_product_progress_time;
                        progressSeconds = timeComponents[2], progressMinutes = timeComponents[1], progressHours = timeComponents[0];
                        progressTimerRefresh();

                        // remaining time
                        clearTimeout(tRemaining);
                        timeComponents = data.import_product_estimated_remaining_time;
                        remainingSeconds = timeComponents[2], remainingMinutes = timeComponents[1], remainingHours = timeComponents[0];
                        remainingTimeRefresh();
                    } else {
                        $('#import-progress').hide();
                        $('#import-product-submit-button').prop('disabled', false);
                    }
                }
            });
        }
        setInterval(importProgress, 10000); // refresh every 10 second(s)
        importProgress();
    </script>
@endcan
@endif

@stop