@extends('suitcore.metronics1.layouts.base')

@inject('baseConfig', '\Suitcore\Config\DefaultConfig')

@section('content')
<?php 

$heading = isset($pageId) && isset($baseConfig::getConfig()['backendNavigation'][$pageId[0]]['submenu'][$pageId]) ? $baseConfig::getConfig()['backendNavigation'][$pageId[0]]['submenu'][$pageId]['label'] : $baseObject->getLabel();

$pageTitle = trans($heading);

$anyFilterable = false;
foreach($baseObject->getBufferedAttributeSettings() as $key=>$val) {
  if ( isset($val['visible']) && $val['visible'] && isset($val['filterable']) && $val['filterable'] ) {
    $anyFilterable = true;
    break;
  }
}
$yadcfContainer = [];
$yadcfIdx = (Route::has($routeBaseName . '.select') ? 1 : 0);
?>
<!-- BEGIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- Begin: life time stats -->
      <div class="portlet light portlet-fit portlet-datatable bordered">
          <div class="portlet-title">
              @section('list.caption')
              <div class="caption">
                  <i class="{{$pageIcon or ''}} font-dark"></i>&nbsp;
                  <span class="caption-subject font-dark sbold uppercase">{{ $pageTitle or '' }}</span>
              </div>
              <div class="actions">
              </div>
              @endsection
              @yield('list.caption')
          </div>
          <div class="portlet-body table-manages">
            @if(isset($pageInfo) && !empty(trim($pageInfo)))
              <div class="note note-info">
                  <p>{{ trim($pageInfo) }}</p>
              </div>
            @endif
            @section('list.body')
            <div class="table-toolbar">
              @if($anyFilterable)
                <div class="row">
                  <div class="col-md-12">
                    <div class="toggles">
                      <div class="block">
                          <a class="toggles__trigger btn btn-primary text-left" data-target="#filterColumn">
                              Filter
                              <span class="fa fa-fw fa-caret-down"></span>
                          </a>
                      </div>
                      <div class="toggles__content hide-element filter-custom-area" id="filterColumn">
                        @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
                          @if( isset($val['visible']) && $val['visible'] )
                            @if( isset($val['filterable']) && $val['filterable'] )
                              <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label">Filter by {{ $val['label'] }}</label>
                                <div class="col-md-8">
                                    <span id="{{$key}}Filter"
                                      data-filter-column="{{ $yadcfIdx }}"
                                      data-filter-type="{{ in_array($val['type'], ['datetime', 'date']) ? $val['type'] : 'select' }}"
                                      @if(in_array($val['type'], ['select', 'numeric']) && isset($val['options_url']) && !empty($val['options_url']) && $val['options_url'] != 'static')
                                        data-filter-url-ajax="{{ $val['options_url'] }}"
                                      @endif
                                      data-filter-default-label="-- select {{ strtolower($val['label']) }} --"></span>
                                </div>
                              </div>
                              <?php 
                                $yadcfContainer[] = $key . "Filter"; 
                              ?>
                            @endif
                            <?php 
                              $yadcfIdx++;
                            ?>
                          @endif
                        @endforeach
                        <div class="clearfix">&nbsp;</div>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
                <div class="row">
                  <div class="col-md-6">
                    <!-- <div class="btn-group"> -->
                      @if( Route::has($routeBaseName . '.create') )
                        @can('suitcorepermission', [$baseObject, $routeBaseName . '.create'])
                          {!! nav_menu(route($routeBaseName . ".create"), trans('backendnav.create'), 'fa fa-plus', 'btn sbold btn-primary') !!}
                        @endcan
                      @endif
                      @if( Route::has($routeBaseName . '.template') )
                        @can('suitcorepermission', [$baseObject, $routeBaseName . '.template'])
                          {!! nav_menu(route($routeBaseName . ".template"), trans('backendnav.importxls'), 'fa fa-download', 'btn sbold btn-primary') !!}
                        @endcan
                      @endif
                    <!-- </div> -->
                    <div class="table-actions @if(!empty(session(class_basename($baseObject) . '_selected_ids', []))) active @endif" style="background-color: #eeeeee;">
                      &nbsp;&nbsp;Action for Selected Item : &nbsp;&nbsp;
                      <!--
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                      -->
                      @if( Route::has($routeBaseName . '.destroyall') )
                        @can('suitcorepermission', [$baseObject, $routeBaseName . '.destroyall'])
                          {!! post_nav_menu(route($routeBaseName . ".destroyall"), '', csrf_token(), 'Are you sure to delete selected item?', 'icon-trash', "btn btn-circle btn-icon-only btn-default", 'Delete selected item') !!}
                        @endcan
                      @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    @if( Route::has($routeBaseName . '.exportxls') )
                      @can('suitcorepermission', [$baseObject, $routeBaseName . '.exportxls'])
                        <div class="btn-group pull-right">
                          <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Tools
                              <i class="fa fa-angle-down"></i>
                          </button>
                          <ul class="dropdown-menu pull-right">
                            @if( Route::has($routeBaseName . '.exportxls') )
                              <li>
                                  <a href='{{ route($routeBaseName . ".exportxls") }}' id="export-to-xls">
                                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                              </li>
                            @endif
                            <!--
                              <li>
                                  <a href="javascript:;">
                                      <i class="fa fa-print"></i> Print </a>
                              </li>
                              <li>
                                  <a href="javascript:;">
                                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                              </li>
                            -->
                          </ul>
                        </div>
                      @endcan
                    @endif
                    <div class="btn-group pull-right">
                      @if(request()->get('responsive', 1))
                        <a class="btn btn-secondary" href="{{ route($routeBaseName . '.index') }}?responsive=0">Switch to Full Table <i class="fa fa-angle-double-right"></i></a>
                      @else
                        <a class="btn btn-secondary" href="{{ route($routeBaseName . '.index') }}?responsive=1">Switch to Responsive <i class="fa fa-angle-double-down"></i></a>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-12">
                    <?php
                      function getFileName($file) {
                        $filePath = explode("/", $file);
                        return $filePath[count($filePath) - 1];
                      }
                    ?>
                    @if ($files != [] || $files != null)
                    <div class="col-md-12">
                      <h3>Previous export file</h3>
                      <ul id="file-list">
                        @if (is_array($files))
                        @foreach ($files as $file)
                          <li><a href="{{ url($file) }}">{{ getFileName($file) }}</a></li>
                        @endforeach
                        @elseif (!is_array($files) && $files)
                          <li><a href="{{ url($files) }}">{{ getFileName($files) }}</a></li>
                        @endif
                      </ul>
                    </div>
                    @endif
                    <div class="col-md-12">
                      <div id="export-progress" style="display: none; padding: 16px; border: 1px solid #AAAAAA; background-color: #FAFAFA; margin: 8px 8px 16px 8px;">
                        <div class="progress progress-striped active">
                            <div id="export-progress-bar" class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            </div>
                        </div>
                        <p class="export-start-notification"></p>

                        <strong>Current Export Progress</strong> : (<span id="export-progress-bar-percentage">0</span>%)
                        <a href="#" id="download-latest-xls">Download File</a>
                        @if( Route::has($routeBaseName . '.exportstop') )
                        @can('suitcorepermission', [$baseObject, $routeBaseName . '.exportstop'])
                            {!! Form::model($baseObject, ['id'=>class_basename($baseObject) . '_export_stop', 'route' => $routeBaseName . '.exportstop', 'class' => 'form-horizontal', 'style' => 'text-align: right; margin-top: 8px;']) !!}
                                <button class="btn btn-sm red active" type="submit" onClick="return confirm('Are you sure to stop this process?');"  id="stop-process">
                                    <i class="fa fa-times">&nbsp;</i> Stop Current Export Process
                                </button>
                            {!! Form::close() !!}
                        @endcan
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div style="background-color: #f7f7f7; text-align: center; padding: 4px; color: #cccccc; font-size: 9pt;">
                  @if(request()->get('responsive', 1))
                    <strong>responsive table</strong> - klik <i class="fa fa-plus-circle"></i> untuk <i>expand row</i> dan melihat data kolom lainnya
                  @else
                    <i class="fa fa-angle-double-left"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>full table</strong> - <i>scroll</i> ke kiri / kanan untuk melihat kolom lainnya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-double-right"></i>
                  @endif
                </div>
              </div>
            </div>

            <table id="{{ class_basename($baseObject) }}" class="table table-striped table-bordered table-hover table-checkable"
            data-datatable-yadcf="{{ route($routeBaseName . '.index.json') . "?_token=" . csrf_token() . (isset($paramSerialized) && !empty($paramSerialized) ? "&".$paramSerialized : "") }}"
              data-datatable-yadcf-container="{{ implode(',', $yadcfContainer) }}"
              @if(Route::has($routeBaseName . '.select'))
                data-datatable-yadcf-nb-checked="{{ count(session(class_basename($baseObject) . '_selected_ids', [])) }}"
                data-datatable-yadcf-order-disabled="0"
              @endif
              @if(request()->get('responsive', 1) != 1)
                data-datatable-responsive="0"
                data-datatable-fixed-left-column="1"
                data-datatable-fixed-right-column="1"
              @endif
              data-start="{{ session()->get('datatable['.$pageId.'][iDisplayStart]', 0) }}"
              data-length="{{ session()->get('datatable['.$pageId.'][iDisplayLength]', 10) }}">
              <thead>
                  <tr>
                    @if(Route::has($routeBaseName . '.select'))
                      <th>
                          #
                      </th>
                    @endif
                    @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
                      @if( isset($val['visible']) && $val['visible'] )
                        <th>{{ $val['label'] }}</th>
                      @endif
                    @endforeach
                    <td><b>{{ trans('backendnav.menu') }}</b></td>
                </tr>
              </thead>
            </table>
            @endsection
            @yield('list.body')
          </div>
      </div>
      <!-- End: life time stats -->
  </div>
</div>
<!-- END CONTENT -->
@stop

@push('end_script')
<script>
</script>
@endpush

@section('page_script')

@if( Route::has($routeBaseName . '.exportprogress') )
@can('suitcorepermission', [$baseObject, $routeBaseName . '.exportprogress'])
    <script>
        var progressBarContainer = $('#export-progress');
        var status = false
        var downloadLink = $('#download-latest-xls')
        var runProcess;

        downloadLink.hide()
        progressBarContainer.hide()

        $("#export-to-xls").click( function(e) {
          e.preventDefault()
          status = true
          var action = $(this).attr('href')
          progressBarContainer.show()
          exportProgress();
          runProcess = setInterval(exportProgress, 10000);
          $.ajax({
            url: action,
            type: 'get',
            dataType: 'json',
            contentType: 'application/json',
            success: function success(data) {
              $('.export-start-notification').html(data.message)
            }
          })
        })

        $('#stop-process').click( function(e) {
          progressBarContainer.hide()
          status = false
          return true
        })

        var progressBar = $('#export-progress-bar');
        var progressImported = $('#export-progress-bar-exported');
        var progressSources = $('#export-progress-bar-sources');
        var progressPercentage = $('#export-progress-bar-percentage');

        function exportProgress() {
          $.ajax({
              url: "{{ route($routeBaseName . '.exportprogress') }}",
              type: 'get',
              dataType: 'json',
              contentType: 'application/json',
              success: function success(data) {
                console.log(data)
                if (data.exported_precentage == 100 || status == true) {                   
                  progressBar.hide()
                  downloadLink.attr('href', data.lastfile)
                  downloadLink.show()
                  progressPercentage.html(data.exported_precentage);
                  $('.export-start-notification').html('export completed')
                  $('#stop-process').hide()
                  clearInterval(runProcess)
                  status = false
                } else {
                  progressBar.attr('style', 'width: ' + data.exported_precentage + '%');
                }
                progressImported.html(data.exported_success_count);
                progressSources.html(data.sources_count);
                progressPercentage.html(data.exported_precentage);
              }
          });
        }
    </script>
@endcan
@endif

@stop