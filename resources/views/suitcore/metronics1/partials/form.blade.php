<div class="form-body">
    <!-- Default Entry Form -->
    <?php
    	$baseObject->resetGroupRendering();
    ?>
    @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
    @if( $val['formdisplay'] && (!isset($hiddenInputs) || !in_array($key, $hiddenInputs)) )
    {!! $baseObject->renderFormView($key, route('admin.uploadfile', ['_token' => csrf_token()]), $errors) !!}
    @endif
    @endforeach
</div>
