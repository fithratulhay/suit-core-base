<!-- (13) Standard Input Text -->
@if($groupFormSetting['type'] == \Suitcore\Models\SuitModel::GROUPTYPE_DATETIMERANGE)
@foreach($groupFormSetting['elmt'] as $role=>$formSetting)
    @if($role == \Suitcore\Models\SuitModel::GROUPROLE_RANGE_START)
<div class='form-row' id='{{ $formSetting['container_id'] }}'>
    <div class='bzg'>
        <div class='bzg_c' data-col='l4'>
            <label class='label-inline' for='{{ $formSetting['id'] }}'>{{ $formSetting['label'] }}</label>
        </div>
    @endif
@endforeach
@foreach($groupFormSetting['elmt'] as $role=>$formSetting)
    @if(in_array($role, [\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START, \Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]))
        <div class='bzg_c' data-col='l3'>
            <input data-datetime-input class='form-input' id='{{ $formSetting['id'] }}' type='text' name='{{ $formSetting['name'] }}' value='{{ $formSetting['value'] }}' {{ $formSetting['required'] ? 'required' : '' }}>
            @if($formSetting['errors'])
                <br><label class='label-inline' style='color:red;''>{{ $formSetting['errors'] ? $formSetting['errors'] : "" }}</label>
            @endif
        </div>
        @if($role == \Suitcore\Models\SuitModel::GROUPROLE_RANGE_START)
            <div class='bzg_c text-center' data-col='l2'> &nbsp; - &nbsp; </div>
        @endif
    @endif
@endforeach
    </div>
</div>
@endif
