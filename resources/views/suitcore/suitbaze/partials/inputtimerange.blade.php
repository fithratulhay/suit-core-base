<!-- (13) Standard Input Text -->
@if($groupFormSetting['type'] == \Suitcore\Models\SuitModel::GROUPTYPE_TIMERANGE)
@foreach($groupFormSetting['elmt'] as $role=>$formSetting)
    @if($role == \Suitcore\Models\SuitModel::GROUPROLE_RANGE_START)
<div class="form-group {{ $formSetting['errors'] ? 'has-error' : '' }}" id="{{ $formSetting['container_id'] }}">
    <label class="col-md-3 control-label" for="{{ $formSetting['id'] }}">{{ $formSetting['label'] }}</label>
    @endif
@endforeach
@foreach($groupFormSetting['elmt'] as $role=>$formSetting)
    @if(in_array($role, [\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START, \Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]))
        <div class="col-md-4">
            <input class='form-control' id='{{ $formSetting['id'] }}' type='time' name='{{ $formSetting['name'] }}' value='{{ $formSetting['value'] }}' {{ $formSetting['required'] ? 'required' : '' }}>
            @if($formSetting['errors'])
                <div class="form-control-focus">{{ $formSetting['errors'] ? $formSetting['errors'] : "" }}</div>
            @endif
        </div>
        @if($role == \Suitcore\Models\SuitModel::GROUPROLE_RANGE_START)
            <div class="col-md-1 text-center"> &nbsp; - &nbsp; </div>
        @endif
    @endif
@endforeach
</div>
@endif
