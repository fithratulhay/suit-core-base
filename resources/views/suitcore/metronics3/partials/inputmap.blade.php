<!-- (13) Standard Input Text -->
@if($groupFormSetting['type'] == \Suitcore\Models\SuitModel::GROUPTYPE_MAP)
@foreach($groupFormSetting['elmt'] as $role=>$formSetting)
    @if($role == \Suitcore\Models\SuitModel::GROUPROLE_MAP_LOCATIONNAME)
        @include('suitcore.metronics1.inputtext', compact('formSetting'))
        <div class="form-group" id="container-{{ $formSetting['elmt_group']['name'] }}">
            <label class="col-md-3 control-label" for="id-{{ $formSetting['elmt_group']['name'] }}">&nbsp;</label>
            <div class="col-md-9">
                <div id="map-{{ $formSetting['elmt_group']['name'] }}" style="width: 100%; height: 400px;"></div>
            </div>
        </div>
    @else
        @include('suitcore.metronics1.inputfloat', compact('formSetting'))
    @endif
@endforeach

@push('pre_end_script_1')
<script>
    function initMap() {
        // Render Map with Location Picker 
        $("#map-{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LOCATIONNAME]['elmt_group']['name'] }}").locationpicker({
            location: {latitude: {{ empty($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LATITUDE]['value']) ? "-6.194288191779069" : $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LATITUDE]['value'] }}, longitude: {{ empty($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LONGITUDE]['value']) ? "106.92647360610965" : $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LONGITUDE]['value']  }} },
            radius: 200,
            inputBinding: {
                latitudeInput: $('#{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LATITUDE]['id'] }}'),
                longitudeInput: $('#{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LONGITUDE]['id'] }}'),
                locationNameInput: $('#{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LOCATIONNAME]['id'] }}')
            },
            enableAutocomplete: true
        });
        $('#{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_MAP_LOCATIONNAME]['id'] }}').on('keypress', function(e) {
            var code = e.keyCode || e.which;
            if(code == 13) return false;
        });
    }
</script>
@endpush

@push('end_script')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key={{ env('GOOGLE_WEB_API_KEY','AIzaSyDzbTbiPooayXM1WieJaKWJHKXKm0k_aLM') }}&callback=initMap" async defer></script>
@endpush

@endif
