@extends('suitcore.metronics2.layouts.base')

@inject('baseConfig', 'Suitcore\Config\DefaultConfig')

@section('content')
<?php 

$heading = isset($pageId) && isset($baseConfig::getConfig()['backendNavigation'][$pageId[0]]['submenu'][$pageId]) ? $baseConfig::getConfig()['backendNavigation'][$pageId[0]]['submenu'][$pageId]['label'] : $baseObject->getLabel();

$pageTitle = trans($heading);

?>
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="{{$pageIcon or ''}} font-dark"></i>&nbsp;
                    <span class="caption-subject sbold uppercase font-dark">{{ $pageTitle or '' }}</span>
                </div>
                <div class="actions">
                    @if( Route::has($routeBaseName . '.create') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.create'])
                        {!! nav_menu(route($routeBaseName . ".create"), '', 'icon-plus', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.create')) !!}
                    @endcan
                    @endif
                    @if( Route::has($routeBaseName . '.show') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.show'])
                        {!! nav_menu(route($routeBaseName . ".show", ['id'=>$baseObject->id]), '', 'icon-eye', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.show')) !!}
                    @endcan
                    @endif
                    @if( Route::has($routeBaseName . '.destroy') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.destroy'])
                        {!! post_nav_menu(route($routeBaseName . '.destroy', ['id' => $baseObject->id]), '', csrf_token(), 'Are you sure?', 'icon-trash', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.delete')) !!}
                    @endcan
                    @endif
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" title="Fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">
                {!! Form::model($baseObject, ['files'=> true, 'id'=>class_basename($baseObject) . '_form', 'class' => 'form-horizontal']) !!}
                
                @if(view()->exists($viewBaseClosure . '.form'))
                    @include($viewBaseClosure . '.form')
                @else
                    @include(config('suitcore.admin_panel_default_base_view', '')['metronics2'] . '.form')
                @endif

                @section('form_actions')
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            @if( Route::has($routeBaseName . '.index') )
                            <a onClick="return confirm('{{ trans('backendnav.confirmdelete') }}');" href="{{ route($routeBaseName . '.index') }}" class="btn default">{{ trans('backendnav.cancel') }}</a>
                            @endif
                            <input onClick="return confirm('Current change will be reset! Are you sure?');" type="reset" class="btn secondary" value="Reset">
                            <input class="btn btn-primary" onClick="if ($('#inputPassword').val() != $('#inputPasswordConfirm').val()) { alert('{{ trans('backendnav.passwordnotsame') }}'); return false;  } else { return true; }" type="submit" value="{{ trans('backendnav.save') }}"/>
                        </div>
                    </div>
                </div>
                @endsection
                @yield('form_actions')
                {!! Form::close() !!}
            </div>
        </div>
        <!-- END FORM PORTLET-->
    </div>
</div>
<!-- END CONTENT -->
@stop

@push('pre-main-js-script')
@endpush

@section('page_script')
<script>
</script>
@stop
