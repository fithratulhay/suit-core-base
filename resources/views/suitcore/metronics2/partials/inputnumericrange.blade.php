<!-- (13) Standard Input Text -->
@if($groupFormSetting['type'] == \Suitcore\Models\SuitModel::GROUPTYPE_NUMERICRANGE)
<div class="form-group {{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['errors'] || $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['errors'] ? 'has-error' : '' }}" id="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}">
    <label class="col-md-3 control-label" for="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['id'] }}">{{ isset($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['label']) ? $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['label'] : ucwords(str_replace("_", " ", $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'])) }}</label>
    <div class="col-md-9">
        @foreach($groupFormSetting['elmt'] as $role=>$formSetting)
            @if(in_array($role, [\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START, \Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]))
                <input type="hidden" name="{{ $formSetting['name'] }}" value="{{ $formSetting['value'] }}">
            @endif
        @endforeach
        <div class="rangeSlider">
            <div id="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}-slider" class="noUi-danger range-container" data-start='[{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['value'] ?: (isset($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['peak_value']) ? $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['peak_value'] : 0) }}, {{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['value'] ?: (isset($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['elmt_group']['peak_value']) ? $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['elmt_group']['peak_value'] : 1000000) }}]' data-min="{{ min( ($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['value'] ?: 0), (isset($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['peak_value']) ? $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['peak_value'] : 0) ) }}" data-max="{{ max( ($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['value'] ?: 0), (isset($groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['elmt_group']['peak_value']) ? $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['elmt_group']['peak_value'] : 1000000) ) }}"></div>
            <div class="range-input text-center clearfix">
                <div class="input-group input-medium col-md-5 pull-left">
                    <span class="input-group-addon">From</span>
                    <input id="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}-range_select" class="form-control need-autoupdate">
                </div>
                <i class="control-label text-orange fa-2x fa fa-long-arrow-right" style="font-size: 2.0em;"></i>
                <div class="input-group input-medium col-md-5 pull-right">
                    <span class="input-group-addon">To</span>
                    <input id="{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['elmt_group']['name'] }}-range_input" class="form-control need-autoupdate">
                </div>
            </div>
        </div>
    </div>
</div>

@push('end_script')
<script type="text/javascript">
    function {{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}Slider() {
        //** init the select
        var $rangeSlider = $('.rangeSlider');
        if(!$rangeSlider.length) return;
        var select = document.getElementById('{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}-range_select');
        var dataStart = $('#{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}-slider').data('start');
        var $input      = $rangeSlider.find('input');

        $input.keypress(function(event){
            var $this   = $(this);
            if (event.keyCode === 10 || event.keyCode === 13) {
                event.preventDefault();

                $this.blur();
            } 
        });

        // console.log(dataStart);

        //** init the slider
        var html5Slider = document.getElementById('{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}-slider');
        var minRange = Number(html5Slider.getAttribute('data-min'));
        var maxRange = Number(html5Slider.getAttribute('data-max'));

        // console.log(minRange);

        noUiSlider.create(html5Slider, {
            start: dataStart,
            connect: true,
            margin: 500000.0,
            padding: 500000.0,
            step: 1.0,
            range: {
                'min': minRange,
                'max': maxRange
            },
            format: wNumb({
                decimals: 3,
                thousand: '.',
                postfix: '',
            })
        });

        //** init the input
        var inputNumber = document.getElementById('{{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['elmt_group']['name'] }}-range_input');

        html5Slider.noUiSlider.on('update', function( values, handle ) {

            var value = values[handle];

            if ( handle ) {
                inputNumber.value = value;

                var updatedValue = Number(value.replace(/[^0-9\,]+/g,""));
                $('[name={{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_END]['name'] }}]').val(updatedValue);
            } else {
                select.value = value;

                var updatedValue = Number(value.replace(/[^0-9\,]+/g,"")); 
                $('[name={{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['name'] }}]').val(updatedValue);
            }
        });

        select.addEventListener('change', function(){
            html5Slider.noUiSlider.set([this.value, null]);
        });

        inputNumber.addEventListener('change', function(){
            html5Slider.noUiSlider.set([null, this.value]);
        });
    }

    $(function () {
        {{ $groupFormSetting['elmt'][\Suitcore\Models\SuitModel::GROUPROLE_RANGE_START]['elmt_group']['name'] }}Slider();
    });
</script>
@endpush

@endif
