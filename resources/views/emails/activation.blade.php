@extends('emails.layout')

@section('content')
<center>
	<h2>WELCOME TO SUITCORE</h2>
</center>

<p>Hello {{ $name }},</p>

<p>Welcome and thank you for joining Suitcore!</p>

<p>To activate your account and make sure that your email registration is valid, please click the button below :</p>

<center>
	<a href="{{ $activationLink or '' }}" style="display: inline-block; color: #fff; background-color: #6FB6B8; line-height: 34px; padding: 0 20px; text-decoration: none;">Click here to verify</a>	
</center>
<br>

<p>If you need some help, please send it to Contact Us page on our website.</p>

<center>
	<a href="{{ route('frontend.home.contactus') }}" style="display: inline-block; color: #fff; background-color: #6FB6B8; line-height: 34px; padding: 0 20px; text-decoration: none;">Contact Us Link</a>	
</center>
<br>

<p>
Cheers,
<br><br>
Suitcore Team
</p>
@endsection
