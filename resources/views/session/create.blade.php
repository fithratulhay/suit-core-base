@extends('frontend.layout.base')

@section('content')
    <div class="container">
        <main class="common-main">
            <article class="cf">
                <div class="bzg">
                    <div class="bzg_c text-center" data-col="l8" data-offset="l2">
                        <h1>Login</h1>
                    </div>
                </div>

                <div class="bzg">
                    <div class="bzg_c" data-col="l4" data-offset="l4">
                        {!!Form::open(['route' => 'sessions.store', 'data-validate-form'])!!}
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l12">
                                        <label class="label-inline" for="inputUsername">Email</label><br>
                                        {!!Form::email('email', null, ['id' => 'inputUsername', 'class'=>'form-input form-input--block', 'required' => 'true', 'placeholder' => 'Email'])!!}
                                        <br>{{$errors->first('email')}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l12">
                                        <label class="label-inline" for="inputPassword">Password</label><br>
                                        <input type="password" class="form-input form-input--block" required placeholder="Password" name="password" id="inputPassword">
                                        <br>{{$errors->first('password')}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="bzg">
                                    <div class="bzg_c" data-col="l12">
                                        <button class="btn btn--block btn--blue" type="submit">Login</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br><br><br>
                    </div>
                </div>
            </article>  
        </main>
    </div>
@stop
