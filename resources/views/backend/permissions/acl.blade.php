@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('content')
<!-- TAB MENU -->
<div class="tabbable-line">
  <ul class="nav nav-tabs" style="margin-top: 10px">
      <li class="active">
          <a href="{{ route('backend.permission.acl') }}"> Grid View </a>
      </li>
      <li>
          <a href="{{ route('backend.permission.index') }}"> Table View </a>
      </li>
  </ul>
</div>
<!-- END OF TAB MENU -->

<!-- BEGIN PAGE TITLE-->
<h3 class="page-title">{{ $baseObject->_label }} List
    <!-- <small>subtitle</small> -->
</h3>
<!-- END PAGE TITLE-->
<!-- BEGIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <div class="note note-info">
          <p>{{ $baseObject->_label }} management in grid view </p>
      </div>
      <!-- Begin: life time stats -->
      <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
              <div class="caption">
                  <i class="{{ $pageIcon or 'icon-badge'}} font-dark"></i>&nbsp;
                  <span class="caption-subject font-dark sbold uppercase">Master {{ $baseObject->_label }}</span>
              </div>
              <div class="actions">
                  @can('suitcorepermission', [$baseObject, $routeBaseName . '.acl.getcopy'])
                  {!! nav_menu(route($routeBaseName . ".acl.getcopy"), 'Copy Permission', 'fa fa-sw fa-files-o', 'btn btn-sm green-jungle btn-outline active') !!}
                  @endcan
            </div>
        </div>
        <div class="portlet-body">
          <div class="table-container">
          {!! Form::open( ['route' => 'backend.permission.acl.save', 'method'=>'post', 'id'=>'form_setting_acl']) !!}
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-11 col-md-1">
                      <input type="hidden" id="allprivileges" name="allprivileges" value="">
                      <input type="submit" class="btn blue" value="Save"/>
                    </div>
                </div>
            </div>
            <br>

            <table class="table table-striped table-bordered table-hover">
              <thead>
                  <th align="center">Action Name</th>
                  @foreach($roles as $role)
                      <th align="center">{{ $role->name }}</th>
                  @endforeach
              </thead>
              <tbody>
                  <?php
                      $currentScope = "";
                  ?>
                  @foreach($routeCollection as $action)
                      <?php
                          $routeName = $action->getName();
                          $routeNameElmt = explode('.', $routeName);
                      ?>
                      @if(count($routeNameElmt) > 1)
                          @if ($routeNameElmt[1] != $currentScope)
                              <?php
                                  $currentScope = $routeNameElmt[1];
                                  $currentScopeLabel = strtoupper(trans('acl.module_name.' . $currentScope));
                                  if ($currentScopeLabel == strtoupper(trans('acl.module_name.' . $currentScope))) {
                                    $currentScopeLabel = strtoupper($currentScope);
                                  }
                              ?>
                              <tr>
                                  <td>
                                      <b>{{ $currentScopeLabel }}</b>
                                  </td>
                                  @foreach($roles as $role)
                                      <td align="center"><b>{{ strtoupper($role->name) }}</b></td>
                                  @endforeach
                              </tr>
                          @endif
                          <tr>
                              <?php
                                  $routeBaseName = trans('acl.route_base_name.' . trim(str_replace("."," ", str_replace($routeNameElmt[0],"", str_replace($routeNameElmt[1],"",$action->getName()) ) )) );
                                  if ($routeBaseName == trans('acl.route_base_name.' . trim(str_replace("."," ", str_replace($routeNameElmt[0],"", str_replace($routeNameElmt[1],"",$action->getName()) ) )) )) {
                                    $routeBaseName = ucwords( trim(str_replace("."," ", str_replace($routeNameElmt[0],"", str_replace($routeNameElmt[1],"",$action->getName()) ) )) );
                                  }
                              ?>
                              <td>&nbsp;&nbsp;&nbsp;{{ $routeBaseName }}</td>
                              @foreach($roles as $role)
                                  <?php
                                      $aclSettingName = $role->id.'xxxxx'.str_replace(".", "xxxxx", $routeName);
                                      $permission = $permissionList && isset($permissionList[$role->id]) && isset($permissionList[$role->id][$routeName]) ? $permissionList[$role->id][$routeName] : null;
                                      $permission = $permission ? explode(':', $permission) : null;
                                  ?>
                                  <td align="center">
                                      <input data-acl type="checkbox" value="{{ $aclSettingName }}" id="{{ $aclSettingName }}" {{ $permission && is_array($permission) && count($permission) == 2 && $permission[1] == 'accept' ? 'checked' : ''}}>
                                  </td>
                              @endforeach
                          </tr>
                      @endif
                  @endforeach
              </tbody>
            </table>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-11 col-md-1">
                      <input type="submit" class="btn blue" value="Save"/>
                    </div>
                </div>
            </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>
      <!-- End: life time stats -->
  </div>
</div>
<!-- END CONTENT -->
@stop

@section('page_script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#form_setting_acl').on('submit', function() {
            var $inputTags = $('[data-acl]');

            if (!$inputTags.length ) return;

            var idx = 0;
            $.each($inputTags, function() {
                var $elem  = $(this);
                var $elemVal = $elem.val() + ':' + ($elem.is(':checked') ? 'T' : 'F');
                $('#allprivileges').val($('#allprivileges').val() + (idx == 0 ? '' : ',') + $elemVal);
                idx++;
            });

            return confirm('Are you sure want to update these privileges?');
        });
    });
</script>
@stop
