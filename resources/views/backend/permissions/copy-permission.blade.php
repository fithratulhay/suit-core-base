@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('content')
<!-- BEGIN PAGE TITLE-->
<h3 class="page-title">Copy {{ $baseObject->_label }}
    <!-- <small>subtitle</small> -->
</h3>
<!-- END PAGE TITLE-->
<!-- BEGIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- Begin: life time stats -->
      <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
              <div class="caption">
                  <i class="{{ $pageIcon or 'icon-badge'}} font-dark"></i>&nbsp;
                  <span class="caption-subject font-dark sbold uppercase">Copy {{ $baseObject->_label }}</span>
              </div>
              <div class="actions">

            </div>
        </div>
        <div class="portlet-body">
          <p>
          {!! Form::open( ['route' => 'backend.permission.acl.postcopy', 'method'=>'post', 'id'=>'form_setting_acl']) !!}
              <div class="form-group form-md-line-input">
                  <label class="col-md-2 control-label">Copy from</label>
                  <div class="col-md-4">
                      <div class="col-md-12">
                          <select name="source" class="form-control select2" required="required">
                            @foreach ($roles as $role)
                              <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                          </select>
                          @if($errors->first('source'))
                              <div class="form-control-focus">{{ $errors->first('source') }}</div>
                          @endif
                      </div>
                  </div>
                </div>
                <div class="form-group form-md-line-input">
                  <label class="col-md-2 control-label">Paste to</label>
                  <div class="col-md-4">
                      <div class="col-md-12">
                          <select name="destination" class="form-control select2" required="required">
                            @foreach ($roles as $role)
                              <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                          </select>
                          @if($errors->first('destination'))
                              <div class="form-control-focus">{{ $errors->first('destination') }}</div>
                          @endif
                      </div>
                  </div>
              </div>

              <div class="form-group form-md-line-input">
                  <label class="col-md-2 control-label"></label>
                  <div class="col-md-4">
                      <div class="col-md-12">
                          <button class="btn btn-sm green-jungle active" type="submit">
                              <i class="fa fa-files-o"></i>&nbsp;
                              Copy Permission
                              &nbsp;
                          </button>
                      </div>
                  </div>
              </div>
          {!! Form::close() !!}
          </p>
        </div>
      </div>
      <!-- End: life time stats -->
  </div>
</div>
<!-- END CONTENT -->
@stop

@section('page_script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#form_setting_acl').on('submit', function() {
            var $inputTags = $('[data-acl]');

            if (!$inputTags.length ) return;

            var idx = 0;
            $.each($inputTags, function() {
                var $elem  = $(this);
                var $elemVal = $elem.val() + ':' + ($elem.is(':checked') ? 'T' : 'F');
                $('#allprivileges').val($('#allprivileges').val() + (idx == 0 ? '' : ',') + $elemVal);
                idx++;
            });

            return confirm('Are you sure want to update these privileges?');
        });
    });
</script>
@stop
