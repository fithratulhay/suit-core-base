@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('content')
<!-- TAB MENU -->
<div class="tabbable-line">
  <ul class="nav nav-tabs" style="margin-top: 10px">
      <li>
          <a href="{{ route('backend.permission.acl') }}"> Grid View </a>
      </li>
      <li class="active">
          <a href="{{ route('backend.permission.index') }}"> Table View </a>
      </li>
  </ul>
</div>
<!-- END OF TAB MENU -->

<!-- BEGIN PAGE TITLE-->
<h3 class="page-title">{{ $baseObject->_label }} List
    <!-- <small>subtitle</small> -->
</h3>
<!-- END PAGE TITLE-->
<!-- BEGIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <div class="note note-info">
          <p>{{ $baseObject->_label }} management in table view </p>
      </div>
      <!-- Begin: life time stats -->
      <div class="portlet light portlet-fit portlet-datatable bordered">
          <div class="portlet-title">
              <div class="caption">
                  <i class="{{ $pageIcon or 'icon-badge'}} font-dark"></i>&nbsp;
                  <span class="caption-subject font-dark sbold uppercase">Master {{ $baseObject->_label }}</span>
              </div>
              <div class="actions">
                @if( Route::has($routeBaseName . '.create') )
                  @can('suitcorepermission', [$baseObject, $routeBaseName . '.create'])
                  {!! nav_menu(route($routeBaseName . ".create"), 'Create New', 'fa fa-sw fa-plus', 'btn btn-sm green btn-outline active') !!}
                  @endcan
                @endif
                @if( Route::has($routeBaseName . '.exportxls') )
                  @can('suitcorepermission', [$baseObject, $routeBaseName . '.exportxls'])
                  {!! nav_menu(route($routeBaseName . ".exportxls"), 'Export to XLS', 'fa-download', 'btn btn-sm blue btn-outline active') !!}
                  @endcan
                @endif
            </div>
        </div>
        <div class="portlet-body">
          <div class="table-container">
              <table id="{{ class_basename($baseObject) }}" class="table table-striped table-bordered table-hover" data-enhance-ajax-table="{{ route($routeBaseName . '.index.json') . "?_token=" . csrf_token() }}">
                <thead>
                    <tr>
                      @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
                        @if( $val['visible'] )
                          <th>{{ $val['label'] }}</th>
                        @endif
                      @endforeach
                      <th><b>Menu</b></th>
                  </tr>
              </thead>
          </table>
      </div>
  </div>
</div>
<!-- End: life time stats -->
</div>
</div>
<!-- END CONTENT -->
@stop

@section('page_script')
  <script>
  </script>
@stop
