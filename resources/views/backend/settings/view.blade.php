@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('page_title')
@endsection

@section('content')
<!-- BEGIN CONTENT -->
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-black">
                    <i class="{{ $pageIcon or 'icon-badge'}} font-black"></i>&nbsp;
                    <span class="caption-subject bold uppercase">Setting</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body row">
                <div class="col-md-12">
                    {!! Form::open(['route' => 'backend.settings.save', 'files'=> true, 'id'=>'form_setting', 'class' => 'form-horizontal']) !!}
                    @foreach($settings as $key => $setting)
                        <div class="portlet box green-meadow">
                            <div class="portlet-title">
                                <div class="caption"><i class="{{ $setting['icon'] }}"></i>{{ $setting['label'] }}</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse" data-original-title="{{ $setting['label'] }}" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row form-horizontal form-body">
                                    <div class="col-md-12">
                                        @foreach($setting['elements'] as $subkey => $subsetting)
                                            {!! GlobalSetting::renderFormView($subkey, $subsetting['type'], $subsetting['label'], $subsetting['default_value'], route('admin.uploadfile', ['_token' => csrf_token()]), $errors) !!}
                                        @endforeach
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-10 col-md-2 text-right">
                                                    <input type="submit" class="btn btn-primary" value="Update"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- END FORM PORTLET-->
    </div>
</div>
<!-- END CONTENT -->

<!--
<div class="form-group form-md-checkboxes">
    <label class="col-md-2 control-label">Enable Landing</label>
    <div class="col-md-10">
        <div class="md-checkbox-inline">
            <div class="md-checkbox">
                <input type="checkbox" id="enable_landing" class="md-check" name="settings[enable_landing]" @if(settings('enable_landing', false)) checked @endif>
                <label for="enable_landing">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> Yes </label>
            </div>
        </div>
    </div>
</div>
-->

@stop

@section('script-footer')
<script></script>
@stop
