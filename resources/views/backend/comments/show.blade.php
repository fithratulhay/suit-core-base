@extends('suitcore.' . $baseLayout . '.partials.view')

@section('content')
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="fa fa-black-tie font-green-sharp"></i>&nbsp;
                    <span class="caption-helper">comment detail...</span>
                </div>
                <div class="actions">
                    @if( Route::has($routeBaseName . '.destroy') )
                    {!! post_nav_menu(route($routeBaseName . '.destroy', ['id' => $baseObject->id]), '', csrf_token(), 'Are you sure?', 'icon-trash', 'btn btn-circle btn-icon-only btn-default') !!}
                    @endif
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN TABLE -->
                <table id="{{ class_basename($baseObject) }}_detail" class="table table-bordered table-striped">
                    <tbody>
                        @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
                        @if ($key == 'commentable_id')
                        <tr>
                            <td> {{ $val['label'] }} </td>
                            <td>{{ $baseObject->commentable }}</td>
                        </tr>
                        @if ($baseObject->commentable->type)
                        <tr>
                            <td> Type </td>
                            <td>{{ title_case(str_replace("-", " ", $baseObject->commentable->type->name)) }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td> Category </td>
                            <td>{{ title_case(str_replace("-", " ", $baseObject->commentable->category)) }}</td>
                        </tr>
                        @else
                            @if ($key != "parent_id" && $key != "commentable_type" && $key != 'updated_at')
                            <tr>
                                <td> {{ $val['label'] }} </td>
                                <td>{!! $baseObject->renderAttribute($key) !!}</td>
                            </tr>
                            @endif
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <!-- END TABLE -->
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
@stop
