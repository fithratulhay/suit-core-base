@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('page_styles')
    <style type="text/css">
        .chartGroup {
            position: absolute;
            z-index: 0;
            width: 100%;
            opacity: 0;
            pointer-events: none;
        }

        .tab-panel {
            position: relative;
            overflow: hidden;
        }

        .chartGroup.is-active {
            position: relative;
            z-index: 1;
            opacity: 1;
            pointer-events: auto;
        }

        .chartZoom-container {
            width: 100%;
        }

        .chartModal-container {
            opacity: 0;
        }

        .chartModal-container.chart-loaded {
            opacity: 1;
        }
    </style>
@endsection

@section('content')
<div class="row form">
    <form id="form-filter" action="" class="form-horizontal">
    <div class="form-group form-md-line-input">
        <label class="col-md-2 control-label">From</label>
        <div class="col-md-3">
            <input class="form-control" autocomplete="off" id="date-from-filter" name="date_from_monthly" type="text" dashboard-date-range-start="form-filter" value="{{ $date_from_monthly ? $date_from_monthly->format('Y-m-d') : '' }}">
        </div>
        <label class="col-md-2 control-label">To</label>
        <div class="col-md-3">
            <input class="form-control" autocomplete="off" id="date-to-filter" name="date_to" type="text" dashboard-date-range-end="form-filter" value="{{ $date_to ? $date_to->format('Y-m-d') : '' }}">
        </div>
        <div class="col-md-2 text-left">
            <button type="submit" class="btn yellow-gold" id="btn-dashboard-filter" >Apply</button>
        </div>
    </div>
    </form>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green-meadow" href="#">
            <div class="visual">
                <i class="icon-user"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ number_format($sessionAvg, 0,'.',',') }}">0</span> s</div>
                <div class="desc">Session Average</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green-meadow" href="#">
            <div class="visual">
                <i class="icon-user"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ number_format($sessionCount, 0,'.',',') }}">0</span></div>
                <div class="desc">Session Average</div>
            </div>
        </a>
    </div>
</div>
<div class="clearfix"></div>

<div class="block block-section">
    <!-- <div class="row"> -->
        <div class="block">
            <div class="selector btn-tabs">
                <button class="btn btn-default type-button btn-primary" id="btnChartZ-1" data-target="#chartZ-1">
                Monthly</button>
                <button class="btn btn-default type-button" id="btnChartZ-2" data-target="#chartZ-2">Weekly</button>
                <button class="btn btn-default type-button" id="btnChartZ-3" data-target="#chartZ-3">Daily</button>
            </div>
            <div class="tab-panel">
            <div class="chartGroup is-active" id="chartZ-1">
                <div class="chartZoom" id="chartZ-1">
                    <h3>User Chart</h3>
                    <div class="chartZoom-container" data-chart="{{ route('backend.activity.appuser-summary', ['date_from_monthly' => $date_from_monthly->format('Y-m-d'), 'date_to' => $date_to->format('Y-m-d'), 'filter-type' => 'monthly']) }}" style="width:100%"></div>
                    <a href="#charz-preview" class="chartZoom-btn" data-toggle="modal">
                        <span class="btn-icon fa fa-search"></span>
                    </a>
                </div>
                <div class="chartZoom">
                    <h3>Session Chart</h3>
                    <div class="chartZoom-container" data-chart="{{ route('backend.activity.appuser-summary', ['date_from_monthly' => $date_from_monthly->format('Y-m-d'), 'date_to' => $date_to->format('Y-m-d'), 'filter-type' => 'monthly', 'data-type'=>'monthly-session']) }}" style="width:100%"></div>
                    <a href="#charz-preview" class="chartZoom-btn" data-toggle="modal">
                        <span class="btn-icon fa fa-search"></span>
                    </a>
                </div>
            </div>
            <div class="chartGroup" id="chartZ-2">
                <div class="chartZoom">
                    <h3>User Chart</h3>
                    <div class="chartZoom-container" data-chart="{{ route('backend.activity.appuser-summary', ['date_from_weekly' => $date_from_weekly->format('Y-m-d'), 'date_to' => $date_to->format('Y-m-d'), 'filter-type' => 'weekly']) }}" style="width:100%"></div>
                    <a href="#charz-preview" class="chartZoom-btn" data-toggle="modal">
                        <span class="btn-icon fa fa-search"></span>
                    </a>
                </div>
                <div class="chartZoom">
                    <h3>Session Chart</h3>
                    <div class="chartZoom-container" data-chart="{{ route('backend.activity.appuser-summary', ['date_from_weekly' => $date_from_weekly->format('Y-m-d'), 'date_to' => $date_to->format('Y-m-d'), 'filter-type' => 'weekly', 'data-type' => 'weekly-session']) }}" style="width:100%"></div>
                    <a href="#charz-preview" class="chartZoom-btn" data-toggle="modal">
                        <span class="btn-icon fa fa-search"></span>
                    </a>
                </div>
            </div>
            <div class="chartGroup" id="chartZ-3">
                <div class="chartZoom">
                    <h3>User Chart</h3>
                    <div class="chartZoom-container" data-chart="{{ route('backend.activity.appuser-summary', ['date_from_daily' => $date_from_daily->format('Y-m-d'), 'date_to' => $date_to->format('Y-m-d'), 'filter-type' => 'daily']) }}" style="width:100%"></div>
                    <a href="#charz-preview" class="chartZoom-btn" data-toggle="modal">
                        <span class="btn-icon fa fa-search"></span>
                    </a>
                </div>
                <div class="chartZoom">
                    <h3>Session Chart</h3>
                    <div class="chartZoom-container" data-chart="{{ route('backend.activity.appuser-summary', ['date_from_daily' => $date_from_daily->format('Y-m-d'), 'date_to' => $date_to->format('Y-m-d'), 'filter-type' => 'daily', 'data-type' => 'daily-session']) }}" style="width:100%"></div>
                    <a href="#charz-preview" class="chartZoom-btn" data-toggle="modal">
                        <span class="btn-icon fa fa-search"></span>
                    </a>
                </div>
            </div>
        </div>
        </div>
    <!-- </div> -->

    <!-- <div class="cloned" style="height: 500px;"></div> -->
    <div id="charz-preview" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header text-right">
                    <button type="button" class="btn" data-dismiss="modal">
                        <span class="fa fa-close"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="chartModal-container">

                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('end_script')
<script type="text/javascript">
    (function ($, window) {
        $('#charz-preview').on('shown.bs.modal', function() {
            $(window).resize();
        });

        $('.btn-tabs').on('click', '.btn', function(e) {
            var $this = $(e.currentTarget)
            var $target = $($this.attr('data-target'))

            $this.siblings().removeClass('btn-primary')
            $this.addClass('btn-primary')

            $target.siblings('.chartGroup').removeClass('is-active')
            $target.addClass('is-active')
        })

    })(jQuery, window)
</script>
@endpush

