<div class="form-body">
    <!-- Default Entry Form -->
    @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
    @if( $val['formdisplay'] && (!isset($hiddenInputs) || !in_array($key, $hiddenInputs)) )
    {!! $baseObject->renderFormView($key, null, $errors) !!}
    @endif
    @endforeach
</div>
@section('page_script')
<script>
    var $types = $('#inputType').children();
    var $typeSelected = $types.eq(1).attr('selected', 'selected');
    setImageType();

    $('#inputType').change( function () {
        $typeSelected = $(this).val();
        hideByType()
    })

    function hideByType()
    {
        if ($typeSelected == 'image') {
            setImageType();
        } else if ($typeSelected == 'video') {
            setVideoType();
        }
    }

    function setVideoType() {
        $('#inputSource').attr('type', 'text').attr('placeholder', 'Insert video source')
    }

    function setImageType() {
        $('#inputSource').attr('type', 'file')
    }
</script>
@endsection
