@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('content')
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    {{-- <i class="fa fa-black-tie font-green-sharp"></i>&nbsp; --}}
                    <span class="caption-subject bold uppercase">{{ $baseObject->getFormattedValue() }}</span>
                    <span class="caption-helper">{{ $title or '' }}</span>
                </div>
                <div class="actions">
                    @if( Route::has($routeBaseName . '.create') )
                    {!! nav_menu(route($routeBaseName . ".create"), '', 'icon-plus', 'btn btn-circle btn-icon-only btn-default', 'Create New') !!}
                    @endif
                    @if( Route::has($routeBaseName . '.edit') )
                    {!! nav_menu(route($routeBaseName . ".edit", ['id'=>$baseObject->id]), '', 'icon-pencil', 'btn btn-circle btn-icon-only btn-default', 'Edit') !!}
                    @endif
                    @if( Route::has($routeBaseName . '.destroy') )
                    {!! post_nav_menu(route($routeBaseName . '.destroy', ['id' => $baseObject->id]), '', csrf_token(), 'Are you sure?', 'icon-trash', 'btn btn-circle btn-icon-only btn-default', 'Delete') !!}
                    @endif
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" title="Fullscreen" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-lg-3 form-horizontal pull-right">
                        @if ($baseObject->timestamps)
                        <div class="note note-warning pull-limit-bottom">
                            <p>
                                <em>
                                    Last edited at {!! $baseObject->renderAttribute('updated_at') !!}
                                </em>
                            </p>
                        </div>
                        <div class="note note-warning pull-limit-bottom">
                            <p>
                                <em>
                                    Created at {!! $baseObject->renderAttribute('created_at') !!}
                                </em>
                            </p>
                            <hr>
                        </div>
                        @endif
                    </div>

                    <div class="col-lg-9 form-horizontal form-bordered form-row-stripped">
                        @foreach($baseObject->getBufferedAttributeSettings() as $key => $attribute)
                        @if(!in_array($key, $baseObject->getHidden()))
                        <div class="form-group clearfix">
                            <label class="control-label col-md-4">{{ $attribute['label'] }}</label>
                            <div class="col-md-8">
                                <div class="form-control-static">{!! $baseObject->renderAttribute($key)  !!}</div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN SPECIFY INLINE -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-share font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Related Data</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#sample" data-toggle="tab"> Sample </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="sample">
                        <br><br>
                        Sample
                    </div>
                </div>
            </div>
        </div>
        <!-- END TAB PORTLET-->
    </div>
</div>
<!-- END SPECIFY INLINE -->
@stop
