<div class="form-body">
    <!-- Default Entry Form -->
    @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
    @if( $val['formdisplay'] && (!isset($hiddenInputs) || !in_array($key, $hiddenInputs)) )
    {!! $baseObject->renderFormView($key, null, $errors) !!}
    @endif
    @endforeach
</div>
@section('page_script')
<script>
    var $type = $('#inputType').val();
    var $typeSelected = ''
    hideByType()
    $('#inputType').change( function () {
        $typeSelected = $(this).val();
        hideByType()
    })

    function hideByType()
    {
        if ($typeSelected == 'image' || $type == 'image') {
            setImageType();
        } else if ($typeSelected == 'video' || $type == 'video') {
            setVideoType();
        } else if ($typeSelected == 'youtube' || $type == 'youtube') {
            setYoutubeType();
        }
    }

    function setYoutubeType() {
        $('#inputSource').attr('type', 'text').attr('placeholder', 'Insert video source')
    }

    function setVideoType() {
        $('#inputSource').attr('type', 'file')
    }

    function setImageType() {
        $('#inputSource').attr('type', 'file')
    }
</script>
@endsection
