@extends('suitcore.' . $baseLayout . '.layouts.base')

@section('content')
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-dark">
                    {{-- <i class="fa fa-black-tie font-dark"></i>&nbsp; --}}
                    <span class="caption-subject bold uppercase">{{ $baseObject->getFormattedValue() }}</span>
                    <span class="caption-helper">{{ $title or '' }}</span>
                    @yield('title-addition')
                </div>
                <div class="actions">
                    @if( Route::has($routeBaseName . '.create') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.create'])
                        {!! nav_menu(route($routeBaseName . ".create"), '', 'icon-plus', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.create')) !!}
                    @endcan
                    @endif
                    @if( Route::has($routeBaseName . '.edit') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.edit'])
                        {!! nav_menu(route($routeBaseName . ".edit", ['id'=>$baseObject->id]), '', 'icon-pencil', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.edit')) !!}
                    @endcan
                    @endif
                    @if( Route::has($routeBaseName . '.destroy') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.destroy'])
                        {!! post_nav_menu(route($routeBaseName . '.destroy', ['id' => $baseObject->id]), '', csrf_token(), trans('label.model.delete_confirmation_text'), 'icon-trash', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.delete')) !!}
                    @endcan
                    @endif
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" title="Fullscreen" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-lg-3 form-horizontal pull-right">
                        @if ($baseObject->timestamps)
                        <div class="note note-warning pull-limit-bottom">
                            <p>
                                <em>
                                    {{ trans('backendnav.updatedat') }} {!! $baseObject->renderAttribute('updated_at') !!}
                                </em>
                            </p>
                        </div>
                        <div class="note note-warning pull-limit-bottom">
                            <p>
                                <em>
                                    {{ trans('backendnav.createdat') }} {!! $baseObject->renderAttribute('created_at') !!}
                                </em>
                            </p>
                            <hr>
                        </div>
                        @endif
                    </div>

                    <div class="col-lg-9 form-horizontal form-bordered form-row-stripped">
                        @foreach($baseObject->getBufferedAttributeSettings() as $key => $attribute)
                        @if( !in_array($key, [ 'id', 'request' ] ) )
                        <div class="form-group clearfix">
                            <label class="control-label col-md-4">{{ $attribute['label'] }}</label>
                            <div class="col-md-8">
                                <div class="form-control-static">{!! !empty($baseObject->renderAttribute($key)) ? $baseObject->renderAttribute($key) : '-'  !!}</div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC DETIL  -->
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="fa fa-black-tie font-green-sharp"></i>&nbsp;
                            <span class="caption-subject bold uppercase">Request Detail</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row form-horizontal form-body">
                            <div class="col-md-12">
                                <?php
                                    $jsonRequest = [];
                                    try { 
                                        $jsonRequest = json_decode($baseObject->request, true);
                                    } catch (\Exception $e) { }
                                ?>
                                @if(is_array($jsonRequest) && !empty($jsonRequest))
                                    @foreach($jsonRequest as $key=>$val)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">{{ ucwords(strtolower(str_replace('_', ' ', $key))) }} :</label>
                                                    <div class="col-md-8">
                                                    @if(is_array($val) && count($val) > 0)
                                                        <ul>
                                                            @foreach($val as $key2=>$val2)
                                                            @if(is_array($val2) && count($val2) > 0)
                                                            <li>{{ $key2 }} :</li>
                                                            <ul>
                                                                @foreach($val2 as $key3=>$val3)
                                                                @if(!is_array($val3))
                                                                    <li>{{ $key3 }} : {{ $val3 }}</li>
                                                                @endif
                                                                @endforeach
                                                            </ul>
                                                            @else
                                                                <li>{{ $key2 }} : {{ $val2 }}</li>
                                                            @endif
                                                            @endforeach
                                                        </ul>
                                                    @else
                                                        <p class="form-control-static">
                                                        {!! htmlspecialchars_decode($val) !!}
                                                        </p>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
@stop
