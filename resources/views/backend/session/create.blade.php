<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>
        @if($currentRole = UserRole::getBySubPath(UserRole::getCurrentRolePath(request())))
            {{ $currentRole->name }} | Login
        @else
            Panel Admin | Login
        @endif
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="/metronic/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/metronic/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        @if($currentRole && !empty($currentRole->base_color))
            <link href="/metronic/pages/css/{{ $currentRole->base_color }}?v={{ env('CSS_VERSION', 1) }}" rel="stylesheet" type="text/css" />
            <link href="/metronic/css/{{ $currentRole->base_color }}?v={{ env('CSS_VERSION', 1) }}" rel="stylesheet" type="text/css" />
        @else
            <link href="/metronic/pages/css/custom-default.min.css?v={{ env('CSS_VERSION', 1) }}" rel="stylesheet" type="text/css" />
            <link href="/metronic/css/custom-default.min.css?v={{ env('CSS_VERSION', 1) }}" rel="stylesheet" type="text/css" />
        @endif
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link href="{{ asset('img/apple-icon.png') }}" rel="apple-touch-icon" type="image/png"/>
        <link href="{{ asset('img/favicon.png') }}" rel="shortcut icon" type="image/png"/>

        <style type="text/css">
            .no-padding {
                padding: 0 !important;
                margin: 0 !important;
            }
            .custom-form-height {
                height: 36px;
                min-height: 36px;
                max-height: 36px;
            }
        </style>
    </head>
    <!-- END HEAD -->
    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="/">
                <img src="{{ asset('img/logo-landscape-white.png') }}" width="160" alt="logo" class="logo-default" />
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            {!!Form::open(['route' => 'backend.sessions.postlogin', 
                'class' => "login-form"
            ])!!}
                <h3 class="form-title">
                    Sign In as <span class="in-block">@if($currentRole) {{ $currentRole->name }} @else SuperAdmin @endif</span>
                </h3>
                @if (session()->has('danger') || session()->has('success') ||  session()->has('info'))
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>{!! (session()->has('danger') ? session()->pull('danger') : (session()->has('success') ? session()->pull('success') : session()->pull('info') )) !!}</span>
                    </div>
                @endif
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" required /> 
                    {{$errors->first('email')}}
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required /> 
                    {{$errors->first('password')}}
                </div>
                @if(settings('enable_paneladmin_login_captcha', null))
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Validation</label>
                        <div class="col-md-5 no-padding">
                            <img src="{{ captcha_src() }}" alt="captcha" id="captcha-img" data-refresh-config="default">
                        </div>
                        <div class="col-md-5 no-padding">
                            <input class="form-control form-control-solid placeholder-no-fix custom-form-height" type="text" autocomplete="off" placeholder="type captcha ..." name="captcha" /> 
                        </div>
                        <a class="btn btn-primary col-md-2 custom-form-height" href="#" id="refresh-captcha"><span class="fa fa-refresh">&nbsp;</span></a>
                        {{$errors->first('password')}}
                    </div>
                @endif
                <div class="form-actions text-center">
                    <button type="submit" class="btn btn-primary uppercase">Login</button>
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Remember
                        <span></span>
                    </label>
                    <!--
                    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                    -->
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2018 © {{ settings('brandname', 'Application') }} </div>
        <!--[if lt IE 9]>
        <script src="/metronic/global/plugins/respond.min.js"></script>
        <script src="/metronic/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="/metronic/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/metronic/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="/metronic/pages/scripts/login.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            @if($currentRole && !empty($currentRole->base_color))
                $.backstretch(["/img/loginbg/{{ str_replace('.min.css', '', $currentRole->base_color) }}/1.jpg","/img/loginbg/{{ str_replace('.min.css', '', $currentRole->base_color) }}/2.jpg","/img/loginbg/{{ str_replace('.min.css', '', $currentRole->base_color) }}/3.jpg","/img/loginbg/{{ str_replace('.min.css', '', $currentRole->base_color) }}/4.jpg"],{fade:1e3,duration:8e3});
            @else
                $.backstretch(["/img/loginbg/custom-default/1.jpg","/img/loginbg/custom-default/2.jpg","/img/loginbg/custom-default/3.jpg","/img/loginbg/custom-default/4.jpg"],{fade:1e3,duration:8e3});
            @endif

            $('#refresh-captcha').on('click', function () {
                var captcha = $('#captcha-img');
                var config = captcha.data('refresh-config');
                $.ajax({
                    method: 'GET',
                    url: "{{ route('captcha.refresh') }}/" + config,
                }).done(function (response) {
                    captcha.prop('src', response);
                });
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>
