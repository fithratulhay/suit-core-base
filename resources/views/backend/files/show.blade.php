@extends('suitcore.' . $baseLayout . '.partials.view')

@section('content')
<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="fa fa-black-tie font-green-sharp"></i>&nbsp;
                    <span class="caption-helper">comment detail...</span>
                </div>
                <div class="actions">
                    @if( Route::has($routeBaseName . '.create') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.create'])
                        {!! nav_menu(route($routeBaseName . ".create"), '', 'icon-plus', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.create')) !!}
                    @endcan
                    @endif
                    @if( Route::has($routeBaseName . '.edit') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.edit'])
                        {!! nav_menu(route($routeBaseName . ".edit", ['id'=>$baseObject->id]), '', 'icon-pencil', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.edit')) !!}
                    @endcan
                    @endif
                    @if( Route::has($routeBaseName . '.destroy') )
                    @can('suitcorepermission', [$baseObject, $routeBaseName . '.destroy'])
                        {!! post_nav_menu(route($routeBaseName . '.destroy', ['id' => $baseObject->id]), '', csrf_token(), trans('label.model.delete_confirmation_text'), 'icon-trash', 'btn btn-circle btn-icon-only btn-default', trans('backendnav.delete')) !!}
                    @endcan
                    @endif
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" title="Fullscreen" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN TABLE -->
                <table id="{{ class_basename($baseObject) }}_detail" class="table table-bordered table-striped">
                    <tbody>
                        @foreach($baseObject->getBufferedAttributeSettings() as $key=>$val)
                        @if($key != 'updated_at')
                        @if ($key == 'source' && $baseObject->getBufferedAttributeSettings()[$key]['type'] == $baseObject::TYPE_FILE)
                        <tr>
                            <td> {{ $val['label'] }} </td>
                            <td>{{ $baseObject->renderAttribute('title') }} <span><i class="fa fa-file-pdf-o"></i></span></td>
                        </tr>
                        @else
                        <tr>
                            <td> {{ $val['label'] }} </td>
                            <td>{!! $baseObject->renderAttribute($key) !!}</td>
                        </tr>
                        @endif
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <!-- END TABLE -->
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
@stop
