<?php

return [
    'yes' => 'Yes',
    'no' => 'No',
    'or' => 'or',
    'of' => 'of',
    'from' => 'From',
    'to' => 'To',

    'dashboard' => [
        'active_app_users' => 'Active App Users',
        'closed_app_users' => 'Closed App Users',
        'active_employers_users' => 'Active Employer Users',
        'closed_employers_users' => 'Closed Employer Users',
        'inactive_app_users' => 'Inactive App Users',
        'banned_app_users' => 'Blocked App User'
    ],

    'thank_you' => 'Thank You',
    'good_bye' => 'Good Bye',
    'default_language_was_updated' => 'Default Language was Updated.',

    'model' => [
        'created_alert_title' => ':model Created',
        'created_alert_message' => 'New :model data has been created!',
        'updated_alert_title' => ':model Updated',
        'updated_alert_message' => ':model data has been updated!',
        'deleted_alert_title' => ':model Deleted',
        'deleted_alert_message' => ':model data has been deleted!',
        'closed_alert_title' => ':model Closed',
        'closed_alert_message' => ':model data has been closed!',
        'cannot_deleted_alert_title' => 'Can\'t delete :model',
        'cannot_deleted_alert_message' => ':model data not deleted! An error occured when processing with database.',
        'cannot_closed_alert_title' => 'Can\'t close :model',
        'cannot_closed_alert_message' => ':model data not closed! Data has been already closed.',
        'delete_confirmation_text' => 'Are you sure want to delete this data ?',
        'delete_confirmation_text_custom' => 'Are you sure want to delete :label ?'
    ]
];
