<?php

return [
    'yes' => 'Ya',
    'no' => 'Tidak',
    'or' => 'atau',
    'of' => 'dari',
    'from' => 'Dari',
    'to' => 'Ke',

    'dashboard' => [
        'active_app_users' => 'Pengguna Aplikasi Aktif',
        'closed_app_users' => 'Pengguna Aplikasi Ditutup',
        'active_employers_users' => 'Pengguna Penyedia Pekerjaan Aktif',
        'closed_employers_users' => 'Pengguna Penyedia Pekerjaan Ditutup',
        'inactive_app_users' => 'Pengguna Aplikasi Tidak Aktif',
        'banned_app_users' => 'Pengguna Aplikasi Diblok'
    ],

    'thank_you' => 'Terima Kasih',
    'good_bye' => 'Selamat Tinggal',
    'default_language_was_updated' => 'Bahasa Telah Diperbarui.',

    'model' => [
        'created_alert_title' => ':model Ditambahkan',
        'created_alert_message' => 'Data :model telah ditambahkan!',
        'updated_alert_title' => ':model Diubah',
        'updated_alert_message' => 'Data :model telah diubah!',
        'deleted_alert_title' => ':model Dihapus',
        'deleted_alert_message' => 'Data :model telah dihapus!',
        'closed_alert_title' => ':model Ditutup',
        'closed_alert_message' => 'Data :model telah ditutup!',
        'cannot_deleted_alert_title' => 'Tidak bisa menghapus :model',
        'cannot_deleted_alert_message' => 'Data :model tidak terhapus! Kesalahan terjadi di database.',
        'cannot_closed_alert_title' => 'Tidak bisa menutup :model',
        'cannot_closed_alert_message' => 'Data :model tidak tertutup! Mungkin sudah tertutup sebelumnya.',
        'delete_confirmation_text' => 'Yakin ingin menghapus data ini ?',
        'delete_confirmation_text_custom' => 'Yakin ingin menghapus :label ?'
    ]
];
