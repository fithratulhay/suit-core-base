<?php

namespace Suitsiteextra\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Suitsiteextra\Models\LogActivity;

class SaveLogActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 300;
    protected $param;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param = [])
    {
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $logRepo = app('Suitsiteextra\Repositories\Contract\LogActivityRepositoryContract');
        $param = $this->param;
        $logRepo->create($param);
    }
}
