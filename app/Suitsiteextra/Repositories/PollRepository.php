<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\Poll;
use Suitsiteextra\Repositories\Contract\PollRepositoryContract;

class PollRepository implements PollRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Poll $model)
    {
        $this->mainModel = $model;
    }
}
