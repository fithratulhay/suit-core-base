<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\FileCategory;
use Suitsiteextra\Repositories\Contract\FileCategoryRepositoryContract;

class FileCategoryRepository implements FileCategoryRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(FileCategory $model)
    {
        $this->mainModel = $model;
    }
}
