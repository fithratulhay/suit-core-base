<?php

namespace Suitsiteextra\Repositories;

use Suitsiteextra\Models\Comment;
use Suitsiteextra\Repositories\Contract\CommentRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class CommentRepository implements CommentRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Comment $model)
    {
        $this->mainModel = $model;
    }

    public function replyComment($id, $param)
    {
        $comment = $this->find($id);
        if(!$comment) {
            $result = [
                'status' => false,
                'message' => 'Comment replied not found'
            ];

            return $result;
        }

        $param['parent_id'] = $comment->id;
        $param['commentable_id'] = $comment->commentable_id;
        $param['commentable_type'] = $comment->commentable_type;
        $param['status'] = $this->mainModel::STATUS_PUBLISHED;

        $reply = $this->create($param);
        $result = [
            'status' => true,
            'message' => 'Comment has been replied'
        ];

        if (!$reply) {
            $result = [
                'status' => false,
                'message' => 'Error accured when reply this comment'
            ];
        }

        return $result;
    }

    public function saveComment__dispatched($object, $param)
    {
        $commentModel = $this->mainModel;
        $commentModel->fill($param);
        $comment = $object->comments()->save($commentModel);
        if ($comment) {
            $link = route('backend.comment.show', ['id' => $comment->id]);
            $message = "New comment from " . ($comment->user ? $comment->user->name : 'Unknown User');
            $userRepo = app('\Suitcore\Repositories\SuitUserRepository');
            $param['role'] = $userRepo->getMainModel()::ADMIN;
            $param['status'] = $userRepo->getMainModel()::STATUS_ACTIVE;
            $param['paginate'] = false;
            $param['perPage'] = -1;
            $admins = $userRepo->getByParameter($param, [], true);
            foreach ($admins as $admin) {
                $admin->notify('\Suitsiteextra\Notifications\AdminNotification', $link, $message);
            }
        }
    }

    public function deleted__dispatched(Comment $model)
    {
        if ($model->commentable) {
            $model->commentable->update([
                'comment_count' => $model->commentable->comments->count()
            ]);
        }
        // info("deleting... " . get_class($model));
    }

    public function saving(Comment $model)
    {
        // info("saving... " . get_class($model));
    }
}
