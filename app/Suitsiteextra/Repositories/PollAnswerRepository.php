<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitTranslation;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\PollAnswer;
use Suitsiteextra\Repositories\Contract\PollAnswerRepositoryContract;
use Suitsiteextra\Repositories\Contract\PollRepositoryContract;
use Suitsiteextra\Repositories\Contract\PollOptionRepositoryContract;

class PollAnswerRepository implements PollAnswerRepositoryContract
{
    use SuitRepositoryTrait;

    protected $optionRepo;
    protected $questionRepo;

    function __construct(
    	PollAnswer $model,
    	PollRepositoryContract $questionRepo,
    	PollOptionRepositoryContract $optionRepo
    )
    {
        $this->mainModel = $model;
        $this->optionRepo = $optionRepo;
        $this->questionRepo = $questionRepo;
    }

    public function create($param, SuitModel &$object = null, $acceptedParam = [], $forcedDefaultParam = []) {
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->getNew() : new SuitModel);
        // if (method_exists($object, 'sluggable')) {
        //     if (!empty($param['slug'])) {
        //         $param['slug'] = str_slug($param['slug']);
        //     } else {
        //         $source = $object->sluggable();
        //         $slugSource = $source['slug']['source'];
        //         $param['slug'] = str_slug($param[$slugSource]);
        //     }
        // }

        if (!empty($acceptedParam)) {
            // accepted parameter
            $param = array_only($param, $acceptedParam);
        }
        if (!empty($forcedDefaultParam)) {
            // default parameter
            $param = array_merge($param, $forcedDefaultParam);
        }
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
        }
        if (!$object->isValid('create', null, $param)) {
            return false;
        }
        $object->fill($param);

        $result = $object->save();
        if ($result && !empty($param['poll_option_id'])) {
        	$optionId = $param['poll_option_id'];
    		$option = $this->optionRepo->find($optionId);
    		if ($option) {
		    	$optionParam['participant_number'] = ++$option->participant_number;
		    	$question = $option->poll;
		    	if ($question) {
			    	$questionParam['participant_number'] = ++$question->participant_number;
			    	$this->questionRepo->update($question->id, $questionParam);
			    }
			}

		    $this->optionRepo->update($optionId, $optionParam);
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::create([
                                    'class' => $object->nodeFullClassName,
                                    'identifier' => $object->id,
                                    'attribute' => $attrName,
                                    'locale' => $normalizedLang,
                                    'value' => $param[$paramKey]
                                ]);
                            }
                        }
                    }
                }
            }
        }

        return $object;
    }

    public function update($id, $param, SuitModel &$object = null, $acceptedParam = [], $forcedDefaultParam = []) {
        /** @var SuitModel $object */
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object == null) return false;

        if (!empty($acceptedParam)) {
            // accepted parameter
            $param = array_only($param, $acceptedParam);
        }
        if (!empty($forcedDefaultParam)) {
            // default parameter
            $param = array_merge($param, $forcedDefaultParam);
        }

        $deletedFieldFiles = [];
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
            if (starts_with($key, 'delete_file__') && $val == 'on') {
                $deletedFieldFiles[] = str_replace('delete_file__', '', $key);
            }
        }
        if (!$object->isValid('update', null, $param)) {
            return false;
        }
        $object->fill($param);

        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::select('*')->instance($object->nodeFullClassName, $object->id)->locale($locale)->field($attrName)->first();
                                if ($currentTranslation) {
                                    $currentTranslation->value = $param[$paramKey];
                                    $currentTranslation->save();
                                } else {
                                    $currentTranslation = SuitTranslation::create([
                                            'class' => $object->nodeFullClassName,
                                            'identifier' => $object->id,
                                            'attribute' => $attrName,
                                            'locale' => $normalizedLang,
                                            'value' => $param[$paramKey]
                                        ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $object;
    }

    public function delete($id, SuitModel &$object = null) {
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object == null) return false;
        $option = $object->option;
        // Delete Object
        $result = $object->delete();
        if ($result) {
        	if ($option) {
		    	$optionParam['participant_number'] = --$option->participant_number;
		    	$question = $option->poll;
		    	if ($question) {
			    	$questionParam['participant_number'] = --$question->participant_number;
			    	$this->questionRepo->update($question->id, $questionParam);
			    }
			}

		    $this->optionRepo->update($option->id, $optionParam);
            // deleting translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $allTranslationDeleted = SuitTranslation::select('*')->instance($object->nodeFullClassName, $object->id)->delete();
            }
        }
        return $result;
    }

}
