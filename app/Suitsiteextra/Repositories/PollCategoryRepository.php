<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\PollCategory;
use Suitsiteextra\Repositories\Contract\PollCategoryRepositoryContract;

class PollCategoryRepository implements PollCategoryRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(PollCategory $model)
    {
        $this->mainModel = $model;
    }
}
