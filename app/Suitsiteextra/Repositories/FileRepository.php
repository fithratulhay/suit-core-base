<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\SuitFile;
use Suitsiteextra\Repositories\Contract\FileRepositoryContract;

class FileRepository implements FileRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(SuitFile $model)
    {
        $this->mainModel = $model;
    }
}
