<?php

namespace Suitsiteextra\Repositories;

use Suitsiteextra\Models\Like;
use Suitsiteextra\Repositories\Contract\LikeRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class LikeRepository implements LikeRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Like $model)
    {
        $this->mainModel = $model;
    }

    public function saveLike__dispatched($object, $param, $users = [])
    {
        $likeModel = $this->mainModel;
        $param['like_type'] = $likeModel::LIKE_TYPE;

        $link = !empty($param['like_notification_link']) ? $param['like_notification_link'] : '#';
        $message = !empty($param['like_notification_message']) ? $param['like_notification_message'] : 'like your post';
        
        unset($param['like_notification_link']);
        unset($param['like_notification_message']);

        $likeModel->fill($param);
        $object->likes()->save($likeModel);

        $param['role'] = $userRepo->getMainModel()::ADMIN;
        $param['status'] = $userRepo->getMainModel()::STATUS_ACTIVE;
        $param['paginate'] = false;
        $param['perPage'] = -1;
        $admins = $userRepo->getByParameter($param, [], true);
        
        $users = array_merge($users, $admins);
        foreach ($users as $user) {
            $user->notify('\Suitsiteextra\Notifications\AdminNotification', $link, $userName . ' ' . $message);
        }
    }

    public function disLike__dispatched($object, $param)
    {
        $likeId = $object->likes()->first() ? $object->likes()->first()->id : null;

        if ($likeId && $likeModel = $this->find($likeId)) {
            $param['like_type'] = $likeModel::DISLIKE_TYPE;
            $likeModel->fill($param);
            $object->likes()->save($likeModel);
        }
    }

    public function isLiked__cached($object, $user)
    {
        $like = $object->likes()->where('user_id', $user->id)->first();
        return $like;
    }
}
