<?php

namespace Suitsiteextra\Repositories;

use Suitsiteextra\Models\Gallery;
use Suitsiteextra\Repositories\Contract\GalleryRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Exception;
use Response;
use Upload;
use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitTranslation;


class GalleryRepository implements GalleryRepositoryContract
{
    use SuitRepositoryTrait {
    	get as originalGet;
    	jsonDatatable as originalJsonDatatable;
    	create as originalCreate;
    	update as originalUpdate;
    	delete as originalDelete;
    }

    public function __construct(Gallery $model)
    {
        $this->mainModel = $model;
    }

    public function get($objectId)
    {
        $object = ($this->mainModel ? $this->mainModel->find($objectId) : SuitModel::find($objectId));
    	$object->initType();
        return [
            'object' => $object
        ];
    }

    public function jsonDatatable($param, $columnFormatted = null, $specificFilter = null, $optionalFilter = null, $columnException = null, $datatableExtendedAndQuery = null, $selectionExist = false) {
        // Selection Column
        SuitModel::$isFormGeneratorContext = false;
        $tmpObject = ($this->mainModel ? $this->mainModel : new SuitModel);
        $tmpObject->showAllOptions = true;
        $object = $tmpObject->select($tmpObject->getTable().".*");
        // $object = $this->getSpecificUserLoggedData($tmpObject);
        $datatableSelection = [];
        $datatableExtendedSelection = [];
        $datatableKeyIndex = [];
        $datatableColumnRelationObject = [];
        $datatableColumnOptions = [];
        $datatableDateOptions = [];
        $columFilterIdx = ($selectionExist ? 1 : 0);

        foreach ($tmpObject->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
            /*
            if ($attrSettings['visible']) {
                if ($specificFilter == null ||
                    !is_array($specificFilter) ||
                    !isset($specificFilter[$attrName])) {
                    $datatableSelection[] = $tmpObject->getTable().'.'.$attrName;
                    $columFilterIdx++;
                }
            }
            */
            if (isset($attrSettings['visible']) &&
                $attrSettings['visible'] &&
                ($specificFilter == null ||
                 !is_array($specificFilter) ||
                 !isset($specificFilter[$attrName])) &&
                ($columnException == null ||
                 !is_array($columnException) ||
                 !in_array($attrName, $columnException))) {
                // selection
                $datatableSelection[$columFilterIdx] = $tmpObject->getTable().'.'.$attrName;
                // filter
                if (isset($attrSettings['filterable']) &&
                    $attrSettings['filterable']) {
                    $datatableKeyIndex[$columFilterIdx] = $attrName;
                    $datatableColumnRelationObject[$columFilterIdx] = isset($attrSettings['relation']) ? $attrSettings['relation'] : null;
                    $datatableColumnOptions[$columFilterIdx] = isset($attrSettings['options']) ? $attrSettings['options'] : null;
                    $datatableDateOptions[$columFilterIdx] = in_array($attrSettings['type'], [SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE]);
                    // for boolean filter
                    if ($attrSettings['type'] == SuitModel::TYPE_BOOLEAN) {
                        $datatableColumnOptions[$columFilterIdx] = [
                            0 => "No",
                            1 => "Yes"
                        ];
                    }
                }
                // extended selection
                if (isset($attrSettings['relation']) &&
                    $attrSettings['relation']) {
                    $datatableExtendedSelection[$attrName] = $tmpObject->getAttribute($attrSettings['relation'].'__object');
                    if (!$datatableExtendedSelection[$attrName]) unset($datatableExtendedSelection[$attrSettings['relation']]);
                }
                // next field
                $columFilterIdx++;
            }
        }

        // YADCF Column Specific Filter
        $specificDefinition = [];
        $tmpValue = "";
        $columnFilter = isset($param["columns"]) ? $param["columns"] : [];
        foreach ($columnFilter as $key => $element) {
            if (isset($element["search"]["value"]) &&
                !empty($element["search"]["value"]) &&
                isset($datatableSelection[$key]) ) {
                // Specific Column Filter
                $tmpValue = $element["search"]["value"];
                if (isset($datatableColumnRelationObject[$key]) &&
                    $datatableColumnRelationObject[$key]) {
                    $objProperty = $datatableColumnRelationObject[$key]."__object";
                    $relatedObject = $tmpObject->$objProperty;
                    if ($relatedObject) {
                        $relatedObject = $relatedObject->where($relatedObject->getUniqueValueColumn(),"=",$tmpValue)->first();
                        if ($relatedObject) {
                            $specificDefinition[$datatableSelection[$key]] = $relatedObject->id;
                        }
                    }
                } else if (isset($datatableColumnOptions[$key]) &&
                           is_array($datatableColumnOptions[$key]) &&
                           count($datatableColumnOptions[$key]) > 0) {
                    $optionKey = array_search($tmpValue, $datatableColumnOptions[$key]);
                    $specificDefinition[$datatableSelection[$key]] = $optionKey;
                } else if ( isset( $datatableDateOptions[$key] ) &&
                    $datatableDateOptions[$key] ) {
                    $specificDefinition[$datatableSelection[$key]] = $tmpValue;
                }
            }
        }

        if ($specificFilter && is_array($specificFilter)) $specificDefinition = array_merge($specificDefinition, $specificFilter);

        // Process Datatable Request
        $jsonSource = $this->preprocessDatatablesJson($object,
                             $datatableSelection,
                             $specificDefinition,
                             $optionalFilter,
                             $tmpObject->_defaultOrder,
                             $tmpObject->_defaultOrderDir,
                             $datatableExtendedSelection,
                             $datatableExtendedAndQuery);

        // Complete json, set data (view rendered) and unset rawdata (model rendered)
        $jsonSource['data'] = array();
        foreach($jsonSource['rawdata'] as $obj) {
            $tmpRow = [];
            $obj->initType();
            // Selection Tools
            $selectedIds = (is_array($columnFormatted) && isset($columnFormatted['selectedIds']) && is_array($columnFormatted['selectedIds']) ? $columnFormatted['selectedIds'] : []);
            if (is_array($columnFormatted) && isset($columnFormatted['selection'])) {
                try {
                    if (empty($columnFormatted['selection'])) {
                        $tmpRow[] = '-';
                    }
                    $selectionElmt = str_replace('#id#', $obj->getAttribute('id'), $columnFormatted['selection']);
                    if (in_array($obj->getAttribute('id'), $selectedIds)) {
                        $selectionElmt = str_replace('#checked#', 'checked', $selectionElmt);
                    } else {
                        $selectionElmt = str_replace('#checked#', '', $selectionElmt);
                    }
                    $tmpRow[] = $selectionElmt;
                } catch (Exception $e) {
                    $tmpRow[] = '-';
                }
            }
            // Data Body
            foreach ($tmpObject->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                if (isset($attrSettings['visible']) &&
                    $attrSettings['visible'] &&
                    ($specificFilter == null ||
                     !is_array($specificFilter) ||
                     !isset($specificFilter[$attrName])) &&
                    ($columnException == null ||
                     !is_array($columnException) ||
                     !in_array($attrName, $columnException))) {
                    $tmpRow[] = $obj->renderAttribute($attrName, $columnFormatted);
                }
            }
            // Action Menu
            if (is_array($columnFormatted) && isset($columnFormatted['menu'])) {
                try {
                    if (empty($columnFormatted['menu'])) {
                        $tmpRow[] = '-';
                    }
                    $tmpRow[] = str_replace("##object_label##", ($obj->getTranslationLabel() . " " . $obj->getFormattedValue()), str_replace('#id#', $obj->getAttribute('id'), $columnFormatted['menu']) );
                } catch (Exception $e) {
                    $tmpRow[] = '-';
                }
            }
            // Add Row
            $jsonSource['data'][] = $tmpRow;
        }
        unset($jsonSource['rawdata']);

        // YADCF Column Specific Filter Options
        foreach ($columnFilter as $key => $element) {
            if (isset($datatableColumnRelationObject[$key]) &&
                $datatableColumnRelationObject[$key]) {
                // for attributes with relationship
                $objProperty = $datatableColumnRelationObject[$key]."__object";
                $relatedObject = $tmpObject->$objProperty;
                $attrSettings = $tmpObject->attribute_settings;
                if ($relatedObject) {
                    if (isset($datatableKeyIndex[$key]) &&
                        isset($attrSettings[$datatableKeyIndex[$key]]) &&
                        isset($attrSettings[$datatableKeyIndex[$key]]['options']) &&
                        !empty($attrSettings[$datatableKeyIndex[$key]]['options']) ) {
                        $optionSources = $attrSettings[$datatableKeyIndex[$key]]['options'];
                        foreach ($optionSources as $value => $label) {
                            $jsonSource['yadcf_data_'.$key][] = [
                                'value' => $value,
                                'label' => $label
                            ];
                        }
                    } else {
                        // $optionSources = $relatedObject->all()->pluck('default_name', $relatedObject->getUniqueValueColumn());
                        $jsonSource['yadcf_data_'.$key]  = [];

                        $passedValue = null;
                        $passedLabel = "";
                        try {
                            $passedValue = $specificDefinition[ $datatableSelection[$key] ];
                            $passedLabel = $relatedObject->find($passedValue)->getFormattedValue();
                        } catch (Exception $e) { }

                        if ($passedValue) {
                            $jsonSource['yadcf_data_'.$key][] = [
                                'value' => $passedValue,
                                'label' => $passedLabel
                            ];
                        }
                    }
                }
            } else if (isset($datatableColumnOptions[$key]) &&
                       is_array($datatableColumnOptions[$key]) &&
                       count($datatableColumnOptions[$key]) > 0) {
                // not relationship with options input
                $jsonSource['yadcf_data_'.$key] = array_values($datatableColumnOptions[$key]);
            }
        }

        // Return JSON Response
        return Response::json($jsonSource);
    }

    /**
     * Create object with attributes in $param and return created object
     * to $object and as function result
     * @param  array $param
     * @param  SuitModel $object
     * @return SuitModel Created Object
     **/
    public function create($param, SuitModel &$object = null) {
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->getNew() : new SuitModel);

        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
        }
        if (!$object->isValid('create', null, $param)) {
            return false;
        }
        $object->fill($param);

        $object->initType();

        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::create([
                                    'class' => $object->nodeFullClassName,
                                    'identifier' => $object->id,
                                    'attribute' => $attrName,
                                    'locale' => $normalizedLang,
                                    'value' => $param[$paramKey]
                                ]);
                            }
                        }
                    }
                }
            }
        }

        if (isset($param['_tags'])) {
            $this->saveModelTag($object, $param['_tags']);
        }

        return $object;
    }

    /**
     * Update object that defined by identifier $id
     * with attributes in $param and return updated object
     * to $object and as function result
     * @param  integer $id
     * @param  array $param
     * @param  SuitModel $object
     * @return SuitModel Updated Object
     **/
    public function update($id, $param, SuitModel &$object = null) {
        /** @var SuitModel $object */
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object == null) return false;
        $deletedFieldFiles = [];
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
            if (starts_with($key, 'delete_file__') && $val == 'on') {
                $deletedFieldFiles[] = str_replace('delete_file__', '', $key);
            }
        }
        if (!$object->isValid('update', null, $param)) {
            return false;
        }
        $object->fill($param);

        $object->initType();

        Upload::cleanUploaded($object, $deletedFieldFiles, false);
        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::select('*')->instance($object->nodeFullClassName, $object->id)->locale($locale)->field($attrName)->first();
                                if ($currentTranslation) {
                                    $currentTranslation->value = $param[$paramKey];
                                    $currentTranslation->save();
                                } else {
                                    $currentTranslation = SuitTranslation::create([
                                            'class' => $object->nodeFullClassName,
                                            'identifier' => $object->id,
                                            'attribute' => $attrName,
                                            'locale' => $normalizedLang,
                                            'value' => $param[$paramKey]
                                        ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (isset($param['_tags'])) {
            $this->saveModelTag($object, $param['_tags']);
        }

        return $object;
    }

    public function delete($id, SuitModel &$object = null) {
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object == null) return false;
        // Delete Object
        $object->initType();
        $result = $object->delete();
        if ($result) {
            // deleting translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $allTranslationDeleted = SuitTranslation::select('*')->instance($object->nodeFullClassName, $object->id)->delete();
            }
        }
        return $result;
    }
}
