<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface PollRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
