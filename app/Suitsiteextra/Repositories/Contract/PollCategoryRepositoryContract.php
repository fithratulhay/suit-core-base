<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface PollCategoryRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
