<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface LogActivityRepositoryContract extends SuitRepositoryContract
{
}
