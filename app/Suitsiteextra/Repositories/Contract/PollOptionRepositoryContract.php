<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface PollOptionRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
