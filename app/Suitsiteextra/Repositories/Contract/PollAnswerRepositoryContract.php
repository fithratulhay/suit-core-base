<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface PollAnswerRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
