<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface GalleryRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
