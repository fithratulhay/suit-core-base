<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface FileRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
