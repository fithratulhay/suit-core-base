<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface FileCategoryRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
}
