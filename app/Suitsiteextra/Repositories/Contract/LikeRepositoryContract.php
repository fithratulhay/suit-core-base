<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface LikeRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
    public function saveLike__dispatched($object, $param);
    public function isLiked__cached($object, $user);
}
