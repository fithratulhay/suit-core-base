<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface CommentRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
    public function replyComment($id, $param);
    public function saveComment__dispatched($object, $param);
}
