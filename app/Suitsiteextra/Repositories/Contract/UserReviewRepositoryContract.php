<?php

namespace Suitsiteextra\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserReviewRepositoryContract extends SuitRepositoryContract
{
}