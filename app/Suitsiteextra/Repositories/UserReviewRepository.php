<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\UserReview;
use Suitsiteextra\Repositories\Contract\UserReviewRepositoryContract;

class UserReviewRepository implements UserReviewRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(UserReview $model)
    {
        $this->mainModel = $model;
    }
}
