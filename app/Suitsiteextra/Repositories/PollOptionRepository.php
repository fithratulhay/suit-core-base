<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\PollOption;
use Suitsiteextra\Repositories\Contract\PollOptionRepositoryContract;

class PollOptionRepository implements PollOptionRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(PollOption $model)
    {
        $this->mainModel = $model;
    }
}
