<?php

namespace Suitsiteextra\Repositories\Facades;

class LogActivity extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\LogActivityRepositoryContract';
    }
}
