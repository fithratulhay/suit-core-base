<?php

namespace Suitsiteextra\Repositories\Facades;

class UserReview extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\UserReviewRepositoryContract';
    }
}
