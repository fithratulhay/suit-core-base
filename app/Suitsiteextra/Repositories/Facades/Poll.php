<?php

namespace Suitsiteextra\Repositories\Facades;

class Poll extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\PollRepositoryContract';
    }
}
