<?php

namespace Suitsiteextra\Repositories\Facades;

class Like extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\LikeRepositoryContract';
    }
}
