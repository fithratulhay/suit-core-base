<?php

namespace Suitsiteextra\Repositories\Facades;

class PollCategory extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\PollCategoryRepositoryContract';
    }
}
