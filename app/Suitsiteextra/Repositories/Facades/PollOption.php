<?php

namespace Suitsiteextra\Repositories\Facades;

class PollOption extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\PollOptionRepositoryContract';
    }
}
