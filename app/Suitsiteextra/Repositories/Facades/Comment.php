<?php

namespace Suitsiteextra\Repositories\Facades;

class Comment extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\CommentRepositoryContract';
    }
}
