<?php

namespace Suitsiteextra\Repositories\Facades;

class CompanyGallery extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsiteextra\Repositories\Contract\GalleryRepositoryContract';
    }
}
