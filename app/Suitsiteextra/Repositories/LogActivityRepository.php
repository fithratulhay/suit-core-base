<?php

namespace Suitsiteextra\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsiteextra\Models\LogActivity;
use Suitsiteextra\Repositories\Contract\LogActivityRepositoryContract;

class LogActivityRepository implements LogActivityRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(LogActivity $model)
    {
        $this->mainModel = $model;
    }
}
