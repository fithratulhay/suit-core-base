<?php

namespace Suitsiteextra\Models;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitsiteextra\Models\Poll;
use Suitsiteextra\Models\PollOption;

class PollAnswer extends SuitModel
{
    public $table = 'poll_answers';

    public $fillable = [
		'poll_id',
		'user_id',
		'poll_option_id',
		'other_answer',
    ];

    public function user()
    {
    	return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function poll()
    {
        return $this->belongsTo(Poll::class, 'poll_id');
    }

    public function option()
    {
    	return $this->belongsTo(PollOption::class, 'poll_option_id');
    }

    public function getLabel()
    {
        return 'Poll Answers';
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'poll_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => 'poll',
                'label' => 'Polling Question',
                'options' => [], // static::where('id', '<>', $this->id)->get()->pluck('full_label', 'id'),
                'options_url' => (config('suitsiteextra.data_options_route_name.suitpoll') ? route(config('suitsiteextra.data_options_route_name.suitpoll')) : ''),
                'filterable' => true,
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "user",
                "label" => "User",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : ''),
                "filterable" => true
            ],
            "poll_option_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "option",
                "label" => "Answer Option",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitsiteextra.data_options_route_name.suitpolloption') ? route(config('suitsiteextra.data_options_route_name.suitpolloption')) : ''),
                "filterable" => true
            ],
            'other_answer' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'Other Answer Title',
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }
}