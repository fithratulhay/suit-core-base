<?php

namespace Suitsiteextra\Models;

use Suitcore\Models\SuitModel;
use Suitsiteextra\Models\Poll;
use Suitsiteextra\Models\PollAnswer;

class PollOption extends SuitModel
{
    public $table = 'poll_options';

    public $imageAttributes = [
    	'image' => 'polloptionimages'
    ];

    public $fillable = [
		'poll_id',
		'option_title',
		'image',
		'participant_number',
    ];

    public function poll()
    {
        return $this->belongsTo(Poll::class, 'poll_id');
    }

    public function answers()
    {
    	return $this->hasMany(PollAnswer::class, 'poll_option_id');
    }

    public function getLabel()
    {
        return 'Poll Answer Options';
    }

    public function getFormattedValue() {
        return $this->option_title;
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'poll_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => 'poll',
                'label' => 'Polling Question',
                'options' => [], // static::where('id', '<>', $this->id)->get()->pluck('full_label', 'id'),
                'options_url' => (config('suitsiteextra.data_options_route_name.suitpoll') ? route(config('suitsiteextra.data_options_route_name.suitpoll')) : ''),
                'filterable' => true,
            ],
            'option_title' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Answer Title',
            ],
            'participant_number' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Option participant Number',
            ],
            'image' => [
                'type' => self::TYPE_FILE,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Option Image',
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }
}
