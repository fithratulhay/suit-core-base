<?php

namespace Suitsiteextra\Models;

use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;

class Comment extends SuitModel
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_REVIEWED = 'reviewed';
    const STATUS_UNPUBLISHED = 'unpublished';
    const STATUS_REJECTED = 'rejected';

    const SAD_TYPE = 'sad';
    const NETRAL_TYPE = 'netral';
    const HAPPY_TYPE = 'happy';

    public $table = 'comments';

    public $fillable = [
        'user_id',
        'parent_id',
        'content',
        'commentable_id',
        'commentable_type',
        'status',
        'comment_type'
    ];

    public $rules = [
        'user_id' => 'numeric',
        'parent_id' => 'required|numeric',
        'commentable_id' => 'numeric',
        'commentable_type' => 'required',
        'content' => 'required',
        'status' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function parent()
    {
        return $this->belongsTo(Comment::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_REVIEWED => ucwords(strtolower(self::STATUS_REVIEWED)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_REJECTED => ucwords(strtolower(self::STATUS_REJECTED)),
            self::STATUS_UNPUBLISHED => ucwords(strtolower(self::STATUS_UNPUBLISHED)),
        ];
    }

    public function getTypeOptions()
    {
        return [
            self::SAD_TYPE => ucwords(strtolower(self::SAD_TYPE)),
            self::NETRAL_TYPE => ucwords(strtolower(self::NETRAL_TYPE)),
            self::HAPPY_TYPE => ucwords(strtolower(self::HAPPY_TYPE))
        ];
    }

    public function getFormattedValue() {
        return $this->content;
    }

    public function getLabel() {
        return "Comment";
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "user",
                "label" => "User",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : ''),
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => "parent",
                "label" => "Parent Comment",
                "filterable" => false,
                "options" => null,
                "options_url" => null
            ],
            "content" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Comment Content",
                "filterable" => false,
                "options" => null,
                "options_url" => null
            ],
            "commentable_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Commented Title",
                "options" => null,
                "options_url" => null
            ],
            "commentable_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Commented Type",
                "options" => null,
                "options_url" => null
            ],
            "comment_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Comment Type",
                "filterable" => true,
                "options" => $this->getTypeOptions(),
                "options_url" => null
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
