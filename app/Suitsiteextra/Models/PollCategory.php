<?php

namespace Suitsiteextra\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\SuitModel;
use Suitsiteextra\Models\Poll;

class PollCategory extends SuitModel
{
	use Sluggable;

	const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    public $table = 'poll_categories';

    public $imageAttributes = [
    	'image' => 'pollcategoryimages'
    ];

    public $fillable = [
		'name',
		'slug',
		'description',
		'image',
		'status',
    ];

    public $rules = [
		'name' => 'string|required',
		'slug' => 'unique:poll_categories',
		'description' => 'string|nullable',
		'image' => 'nullable',
		'status' => 'string|required',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function polls()
    {
        return $this->hasMany(Poll::class, 'category_id');
    }

    public function getLabel()
    {
        return 'Poll Category';
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_UNPUBLISHED => ucwords(strtolower(self::STATUS_UNPUBLISHED)),
        ];
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'name' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Category Name',
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug",
                "options" => null,
                "options_url" => null
            ],
            'description' => [
                'type' => self::TYPE_TEXTAREA,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'Category Description',
            ],
            'image' => [
                'type' => self::TYPE_FILE,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Option Image',
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }
}
