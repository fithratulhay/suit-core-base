<?php

namespace Suitsiteextra\Models;

use Suitsiteextra\Models\Comment;
use Suitsiteextra\Models\Like;
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Date\Date;
use Suitcore\Models\SuitModel;

class Gallery extends SuitModel
{
    use Sluggable;

    const FILE_TYPE_IMAGE = 'image';
    const FILE_TYPE_VIDEO = 'video';
    const FILE_TYPE_YOUTUBE = 'youtube';

    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    public $table = 'galleries';

    public $imageAttributes = [];

    public $files = [
        'source' => 'galleryfiles'
    ];

    public $fillable = [
        'parent_id',
        'title',
        'slug',
        'type',
        'date',
        'source',
        'highlight',
        'like_count',
        'comment_count',
        'position_order',
        'status'
    ];

    public $rules = [
        'parent_id' => 'numeric|nullable',
        'source' => 'required',
        'title' => 'required',
        'slug' => 'unique:company_galleries',
        'status' => 'required'
    ];

    public function initType() {
        if ($this->type == self::FILE_TYPE_IMAGE || $this->type == self::FILE_TYPE_YOUTUBE) {
            $this->imageAttributes = [
                'source' => 'galleryfiles'
            ];
        } else {
            $this->imageAttributes = [];
        }
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function parent()
    {
        return $this->belongsTo(Gallery::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(Gallery::class, 'parent_id');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function getVideoSourceAttribute($value)
    {
        $getParams = [];
        parse_str( parse_url( $value, PHP_URL_QUERY ), $getParams );
        $ytSource =  (isset($getParams['v']) ? $getParams['v'] : '');
        $source = "https://img.youtube.com/vi/" . $ytSource . "/0.jpg";
        return $source;
    }

    public function getYoutubeSource($value)
    {
        $getParams = [];
        parse_str( parse_url( $value, PHP_URL_QUERY ), $getParams );
        $ytSource =  (isset($getParams['v']) ? $getParams['v'] : '');
        $source = "https://www.youtube.com/embed/" . $ytSource ."?rel=0&amp;showinfo=0";
        return $source;
    }

    public function renderAttribute($attrName, $columnFormatted = null) {
        $attrSettings = $this->getBufferedAttributeSettings()[$attrName];
        $tmpRow = '';
        // if ($attrSettings['visible']) {
            $rendered = false;
            if (is_array($columnFormatted) &&
                isset($columnFormatted[$attrName]) &&
                !empty($columnFormatted[$attrName])) {
                // Custom Template / Format
                try {
                    $maskedValue = (property_exists(get_class($this), $attrName."_attribute_label") ? $this->getAttribute($attrName."_attribute_label") : $this->getAttribute($attrName));
                    if (isset($columnFormatted["_render_".$attrName])) {
                        // try render using provided function
                        try {
                            $maskedValue = call_user_func($columnFormatted["_render_".$attrName], $this->getAttribute($attrName));
                        } catch (Exception $e2) { }
                    }
                    if (is_array($columnFormatted[$attrName])) {
                        if (isset( $columnFormatted[$attrName][$this->getAttribute($attrName)] )) {
                            $tmpRow = str_replace('#'.$attrName.'#', $maskedValue, $columnFormatted[$attrName][$this->getAttribute($attrName)]);
                        } else {
                            $tmpRow = str_replace('#'.$attrName.'#', $maskedValue, $columnFormatted[$attrName][array_keys($columnFormatted[$attrName])[0]]);
                        }
                    } else {
                        $tmpRow = str_replace('#'.$attrName.'#', $maskedValue, $columnFormatted[$attrName]);
                    }
                    $rendered = true;
                } catch (Exception $e) {
                    // Back to default format
                    $rendered = false;
                }
            }
            if (!$rendered) {
                // Standard Template / Format
                if ($attrSettings['relation'] != null) {
                    $relatedObject = $this->getAttribute($attrSettings['relation']);
                    if ($relatedObject)
                        $tmpRow = $relatedObject->getFormattedValue();
                    else
                        $tmpRow = "-";
                } else if ($attrSettings['type'] == self::TYPE_NUMERIC) {
                    if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                        $relatedReferences = $attrSettings['options'];
                        if (isset($relatedReferences[$this->getAttribute($attrName)]))
                            $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                        else
                            $tmpRow = $this->getAttribute($attrName);
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? number_format($this->getAttribute($attrName),0,'.',',') : 0);
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_FLOAT) {
                    if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                        $relatedReferences = $attrSettings['options'];
                        if (isset($relatedReferences[$this->getAttribute($attrName)]))
                            $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                        else
                            $tmpRow = $this->getAttribute($attrName);
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? number_format($this->getAttribute($attrName),2,'.',',') : 0);
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_DATETIME) {
                    if (isEmptyDate($this->getAttribute($attrName))) {
                        $tmpRow = "-";
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? Date::createFromTimestamp(strtotime($this->getAttribute($attrName)))->format("d F Y G:i") : '-');
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_DATE) {
                    if (isEmptyDate($this->getAttribute($attrName))) {
                        $tmpRow = "-";
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? Date::createFromTimestamp(strtotime($this->getAttribute($attrName)))->format("d F Y") : '-');
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_TIME) {
                    if (isEmptyDate($this->getAttribute($attrName))) {
                        $tmpRow = "-";
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? date("G:i", strtotime($this->getAttribute($attrName))) : '-');
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_BOOLEAN) {
                    $tmpRow = ($theVal = $this->getAttribute($attrName)) === null ? '-' : ($theVal ? trans('label.yes') : trans('label.no'));
                } else if ($attrSettings['type'] == self::TYPE_FILE) {
                    if ($this->getAttribute($attrName)) {
                        if (is_array($this->imageAttributes) &&
                            count($this->imageAttributes) > 0 &&
                            in_array($attrName, array_keys($this->imageAttributes))) {
                            $tmpRow = "<img class='".self::$thumbnailClass."' src='".$this->getAttribute($attrName."_medium_cover")."' style='max-height: 300px' alt=''>";
                            if ($this->type == self::FILE_TYPE_YOUTUBE) {
                                $tmpRow = "<img class='".self::$thumbnailClass."' src='".$this->getVideoSourceAttribute($this->source)."' style='max-height: 300px' alt=''>";
                            } elseif ($this->type == self::FILE_TYPE_VIDEO) {
                                $tmpRow = "<img class='".self::$thumbnailClass."' src='".settings('default_no_image')."' style='max-height: 300px' alt=''>";
                            }
                        } else {
                            $tmpRow = "<a target='_BLANK' href='".$this->getFileAccessPath($attrName)."'>".$this->getAttribute($attrName)."</a>";
                        }
                    } else {
                        if (is_array($this->imageAttributes) &&
                            count($this->imageAttributes) > 0 &&
                            in_array($attrName, array_keys($this->imageAttributes))) {
                            $tmpRow = "<i>( ".trans('label.no_image')." )</i>";
                        } else {
                            $tmpRow = "<i>( ".trans('label.no_file')." )</i>";
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_RICHTEXTAREA) {
                    // Rich Text Based
                    $shouldTranslate = isset($attrSettings['translation']) ? $attrSettings['translation'] : false;
                    $tmpRow = htmlspecialchars_decode($this->getAttribute($attrName . ($shouldTranslate ? '__trans' : '')));
                } else {
                    // Text Based
                    if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                        $relatedReferences = $attrSettings['options'];
                        if (isset($relatedReferences[$this->getAttribute($attrName)]))
                            $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                        else
                            $tmpRow = ucwords($this->getAttribute($attrName));
                    } else {
                        $shouldTranslate = isset($attrSettings['translation']) ? $attrSettings['translation'] : false;
                        $tmpRow = $this->getAttribute($attrName . ($shouldTranslate ? '__trans' : ''));
                    }
                }
            }
        // }
        // Return
        return $tmpRow;
    }

    public function getTypeOptions()
    {
        return [
            self::FILE_TYPE_IMAGE => ucwords(strtolower(self::FILE_TYPE_IMAGE)),
            self::FILE_TYPE_VIDEO => ucwords(strtolower(self::FILE_TYPE_VIDEO)),
            self::FILE_TYPE_YOUTUBE => ucwords(strtolower(self::FILE_TYPE_YOUTUBE))
        ];
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_UNPUBLISHED => ucwords(strtolower(self::STATUS_UNPUBLISHED)),
        ];
    }

    public function getLabel() {
        return "Company Gallery";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "parent",
                "label" => "Parent Gallery",
                "filterable" => true,
                "options" => [],
                'options_url' => (config('suitapp.data_options_route_name.parentgallery') ? route(config('suitapp.data_options_route_name.parentgallery')) : ''),
                "filterable" => true
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "options" => null,
                "options_url" => null
            ],
            "date" => [
                "type" => self::TYPE_DATE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Date",
                "options" => null,
                "options_url" => null
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug",
                "options" => null,
                "options_url" => null
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Gallery Type",
                "filterable" => true,
                "options" => $this->getTypeOptions(),
                "options_url" => null
            ],
            "source" => [
                "type" => self::TYPE_FILE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Gallery Source (image max 2MB, recommended size 960 x 480)",
                "options" => null,
                "options_url" => null
            ],
            "highlight" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Highlight",
                "options" => null,
                "options_url" => null
            ],
            "comment_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Comment Count",
                "options" => null,
                "options_url" => null,
                "default_value" => 0
            ],
            "like_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Like Count",
                "options" => null,
                "options_url" => null,
                "default_value" => 0
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Position Order",
                "options" => null,
                "options_url" => null
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
