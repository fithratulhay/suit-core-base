<?php

namespace Suitsiteextra\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;
use Suitsiteextra\Models\PollAnswer;
use Suitsiteextra\Models\PollCategory;
use Suitsiteextra\Models\PollOption;

class Poll extends SuitModel
{
    use Sluggable;

	const STATUS_PUBLISHED = 'published';
    const STATUS_DRAFT = 'draft';
    const STATUS_UNPUBLISHED = 'unpublished';

    public $table = 'polls';

    public $imageAttributes = [
    	'image' => 'pollimages'
    ];

    public $fillable = [
        'user_id',
		'category_id',
		'poll_name',
        'slug',
		'poll_question',
		'image',
		'participant_number',
		'status',
    ];

    public $rules = [
        'user_id' => 'numeric|required',
        'category_id' => 'numeric|nullable',
        'poll_name' => 'string|required',
        'slug' => 'unique:polls',
        'poll_question' => 'string|required',
        'image' => 'nullable',
        'participant_number' => 'numeric|nullable',
        'status' => 'string|required',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'poll_name'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(PollCategory::class, 'category_id');
    }

    public function options()
    {
    	return $this->hasMany(PollOption::class, 'poll_id');
    }

    public function answers()
    {
    	return $this->hasMany(PollAnswer::class, 'poll_id');
    }

    public function getLabel()
    {
        return 'Polling Questions';
    }

    public function getFormattedValue() {
        return $this->poll_name;
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_UNPUBLISHED => ucwords(strtolower(self::STATUS_UNPUBLISHED)),
        ];
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'user_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => 'user',
                'label' => 'User',
                'options' => [], // static::where('id', '<>', $this->id)->get()->pluck('full_label', 'id'),
                'options_url' => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : ''),
                'filterable' => true,
            ],
            'category_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => 'category',
                'label' => 'Category',
                'options' => (new PollCategory)->all()->pluck('name', 'id'), // static::where('id', '<>', $this->id)->get()->pluck('full_label', 'id'),
                'options_url' => null,
                'filterable' => true,
            ],
            'poll_name' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Question Title',
            ],
            'slug' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'Slug',
            ],
            'poll_question' => [
                'type' => self::TYPE_TEXTAREA,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Question',
            ],
            'participant_number' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Question Participant Number',
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            'image' => [
                'type' => self::TYPE_FILE,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Question Image',
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }
}
