<?php

namespace Suitsiteextra\Models;

use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;

class Like extends SuitModel
{
    const LIKE_TYPE = 'like';
    const DISLIKE_TYPE = 'dislike';

    public $table = 'likes';

    public $fillable = [
        'user_id',
        'likeable_id',
        'likeable_type',
        'like_type'
    ];

    public $rules = [
        'user_id' => 'numeric',
        'likeable_id' => 'numeric',
        'likeable_type' => 'required',
        'like_type' => 'alpha'
    ];

    public function likeable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

     public function getLikeTypes()
    {
        return [
            self::LIKE_TYPE => ucwords(strtolower(self::LIKE_TYPE)),
            self::DISLIKE_TYPE => ucwords(strtolower(self::DISLIKE_TYPE)),
        ];
    }

    public function getLabel() {
        return "Like";
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "user",
                "label" => "User",
                "filterable" => true,
                "options" => null,
                "options_url" => null
            ],
            "likeable_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Liked",
                "options" => null,
                "options_url" => null
            ],
            "likeable_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Liked",
                "options" => null,
                "options_url" => null
            ],
            "like_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Type",
                "filterable" => true,
                "options" => $this->getLikeTypes(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
