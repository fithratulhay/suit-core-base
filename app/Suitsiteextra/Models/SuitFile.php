<?php

namespace Suitsiteextra\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\SuitModel;
use Suitsiteextra\Models\FileCategory;

class SuitFile extends SuitModel
{
    use Sluggable;

    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    public $table = 'files';

    public $imageAttributes = [
        'image' => 'companyimages'
    ];

    public $files = [
        'source' => 'companyfiles',
        'image' => 'companyimages'
    ];

    public $fillable = [
        'file_category_id',
        'title',
        'slug',
        'highlight',
        'description',
        'published_date',
        'optional_page_url',
        'image',
        'source',
        'file_size',
        'position_order',
        'status'
    ];

    public $rules = [
        'file_category_id' => 'numeric',
        'title' => 'required',
        'slug' => 'unique:files',
        'status' => 'required'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo(FileCategory::class, 'file_category_id');
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_UNPUBLISHED => ucwords(strtolower(self::STATUS_UNPUBLISHED)),
        ];
    }

    public function getPublishedDateAttribute($value)
    {
        $date = Carbon::parse($value)->format("d M Y h:i:s");
        return $date;
    }

    public function getLabel() {
        return "Company File";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "file_category_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "category",
                "label" => "Category",
                "filterable" => true,
                "options" => (new FileCategory)->all()->pluck('title','id'),
                "options_url" => null
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "options" => null,
                "options_url" => null
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug",
                "options" => null,
                "options_url" => null
            ],
            "highlight" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Highlight",
                "options" => null,
                "options_url" => null
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Description",
                "options" => null,
                "options_url" => null
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "File Thumbnail (max size 2MB, recommended size 190 x 250 pixel)",
                "options" => null,
                "options_url" => null
            ],
            "source" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "File",
                "options" => null,
                "options_url" => null
            ],
            "published_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Published Date",
                "options" => null,
                "options_url" => null
            ],
            "optional_page_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "URL Page",
                "options" => null,
                "options_url" => null
            ],
            "file_size" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "File Size",
                "options" => null,
                "options_url" => null
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Position Order",
                "options" => null,
                "options_url" => null
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
