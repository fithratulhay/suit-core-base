<?php

namespace Suitsiteextra\Models;

use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;

class LogActivity extends SuitModel
{
    public $table = 'log_activities';

    public $fillable = [
        'user_id',
        'url',
        'session_id'
    ];

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function getLabel()
    {
        return 'Log Activity';
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'user_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => 'user',
                'label' => 'User',
                'options' => [], // static::where('id', '<>', $this->id)->get()->pluck('full_label', 'id'),
                'options_url' => [],
                'filterable' => true,
            ],
            'url' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'URL',
            ],
            'session_id' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'Session Id',
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }
}
