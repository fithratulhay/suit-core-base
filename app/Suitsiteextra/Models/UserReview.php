<?php

namespace Suitsiteextra\Models;

use Suitcore\Models\SuitModel;

class UserReview extends SuitModel
{
	const STATUS_PUBLISHED = 'published';
    const STATUS_REVIEWED = 'reviewed';
    const STATUS_UNPUBLISHED = 'unpublished';
    const STATUS_REJECTED = 'rejected';

    const SAD_TYPE = 'sad';
    const NETRAL_TYPE = 'netral';
    const HAPPY_TYPE = 'happy';
    
    public $table = 'user_reviews';


    public $fillable = [
		'user_id',
		'reviewable_id',
		'reviewable_type',
		'rating',
		'review_content',
		'review_type'
    ];

    public $rules = [
        'user_id' => 'numeric|nullable',
        'reviewable_id' => 'numeric|required',
        'reviewable_type' => 'required',
        'rating' => 'numeric|nullable',
        'review_content' => 'alpha_num|nullable',
        'review_type' => 'alpha|nullable'
    ];

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function getTypeOptions()
    {
        return [
            self::SAD_TYPE => ucwords(strtolower(self::SAD_TYPE)),
            self::NETRAL_TYPE => ucwords(strtolower(self::NETRAL_TYPE)),
            self::HAPPY_TYPE => ucwords(strtolower(self::HAPPY_TYPE))
        ];
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_REVIEWED => ucwords(strtolower(self::STATUS_REVIEWED)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_REJECTED => ucwords(strtolower(self::STATUS_REJECTED)),
            self::STATUS_UNPUBLISHED => ucwords(strtolower(self::STATUS_UNPUBLISHED)),
        ];
    }

    public function getLabel() {
        return "User Review";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "user",
                "label" => "User",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : ''),
            ],
            "reviewable_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Reviewed Title",
                "options" => null,
                "options_url" => null
            ],
            "reviewable_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Reviewed Type",
                "options" => null,
                "options_url" => null
            ],
            "rating" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Rating",
                "filterable" => false,
                "options" => null,
                "options_url" => null,
                "default_value" => 0
            ],
            "review_content" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Review Content",
                "filterable" => false,
                "options" => null,
                "options_url" => null
            ],
            "review_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Review Type",
                "filterable" => true,
                "options" => $this->getTypeOptions(),
                "options_url" => null
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
