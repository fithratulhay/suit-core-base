<?php

namespace Suitsiteextra\Models;

use Suitsiteextra\Models\SuitFile;
use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\SuitModel;

class FileCategory extends SuitModel
{
    use Sluggable;

    public $table = 'file_categories';

    public $fillable = [
        'parent_id',
        'title',
        'slug',
        'description',
        'position_order',
    ];

    public $rules = [
        'parent_id' => 'numeric|nullable',
        'title' => 'required',
        'slug' => 'unique:file_categories',
        'position_order' => 'numeric'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function parent()
    {
        return $this->belongsTo(FileCategory::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(FileCategory::class, 'parent_id');
    }
    public function files()
    {
        return $this->hasMany(SuitFile::class, 'file_category_id');
    }

    public function getParentList()
    {
        return self::all()->pluck('title', 'id');
    }

    public function getLabel() {
        return "File Category";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "parent",
                "label" => "Parent Category",
                "filterable" => true,
                "options" => $this->getParentList(),
                "options_url" => null
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "options" => null,
                "options_url" => null
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug",
                "options" => null,
                "options_url" => null
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Description",
                "options" => null,
                "options_url" => null
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Position Order",
                "options" => null,
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
