<?php

namespace App\Repositories;

use Mail;
use App\Models\User;
use Suitcore\Repositories\SuitUserRepository;

class UserRepository extends SuitUserRepository
{
    // Extended Properties and Behaviour / Methods
    public function __construct(User $model) {
        parent::__construct($model);
    }

    /**
    * custom-definition
    * @param SocialAccountInterface $account
    * @param String $plainPassword Plain password
    **/
    protected function postSocialMediaIntegration($account, $plainPassword) {
     	if ($account) {
            Mail::send('emails.welcomewithpassword', [
                'name' => $account->getName(),
                'email' => $account->getEmail(),
                'username' => $account->getUsername(),
                'password' => $plainPassword,
                'accountType' => $account->getType(),
                'accountName' => $account->getName()
            ], function (Message $message) use ($account) {
                $message
                    ->to($account->getEmail(), $account->getName())
                    ->subject('Welcome to SuitApplication');
            });
        }   
    }
}
