<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitquestionanswer\Models\Question;
use Suitquestionanswer\Models\UserAnswer;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Answer extends SuitModel
{
    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'question_id',
        'answer',
        'position_order'
    ];

    public $rules = [

    ];

    public function getLabel()
    {
        return "Answer";
    }

    public function getFormattedValue()
    {
        return $this->answer;
    }
    
    public function getFormattedValueColumn()
    {
        return ['answer'];
    }

    // RELATIONSHIPS
    public function question() {
        return $this->belongsTo(Question::class);
    }

    public function userAnswer() {
        return $this->hasMany(UserAnswer::class);
    }

    public function correctQuestion(){
        return $this->hasOne(Question::class, "answer_id");
    }

    public function getAttributeSettings()
    {
        return [
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "initiated" => true,
                "relation" => false,
                "label" => "Position Order"
            ],
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "question_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "readonly" => true,
                "relation" => "question",
                "options" => Question::all()->pluck('question', 'id'),
                "label" => "Question"
            ],
            "answer" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "translation" => true,
                "relation" => false,
                "label" => "Answer"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
