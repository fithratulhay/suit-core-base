<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitquestionanswer\Models\Chapter;
use Suitquestionanswer\Models\UserMaterial;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Material extends SuitModel
{
    // CONSTANTS
    const STATUS_DRAFTED = 'drafted';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    const TYPE_VIDEO = 'video';
    const TYPE_ARTICLE = 'article';

    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'chapter_id',
        'title',
        'url',
        'thumbnail',
        'type',
        'position_order',
        'status'
    ];

    public $imageAttributes = [
        'thumbnail' => 'materialthumbnails',
    ];

    public $files = [
        'thumbnail' => 'materialthumbnails',
    ];

    public $rules = [
        'position_order' => 'numeric',
    ];

    public function getLabel()
    {
        return "Material";
    }

    public function getFormattedValue()
    {
        return $this->title;
    }
    
    public function getFormattedValueColumn()
    {
        return ['title'];
    }

    // RELATIONSHIPS
    public function chapter() {
        return $this->belongsTo(Chapter::class);
    }

    public function userMaterials() {
        return $this->hasMany(UserMaterial::class);
    }

    //

    public function getIconTypeAttribute(){
        switch($this->type){
            case self::TYPE_VIDEO :
                return 'fa-file-video';
            case self::TYPE_ARTICLE :
                return 'fa-newspaper';
            default :
                return null;
        }
    }

    // OPTIONS
    public function getTypeOptions(){
        return [
            self::TYPE_VIDEO => ucfirst(self::TYPE_VIDEO),
            self::TYPE_ARTICLE => ucfirst(self::TYPE_ARTICLE),
        ];
    }

    public function getStatusOptions(){
        return [
            self::STATUS_DRAFTED => ucfirst(self::STATUS_DRAFTED),
            self::STATUS_PUBLISHED => ucfirst(self::STATUS_PUBLISHED),
            self::STATUS_UNPUBLISHED => ucfirst(self::STATUS_UNPUBLISHED),
        ];
    }

    public function getDefaultOrderColumn() {
        return 'position_order';
    }

    public function getDefaultOrderColumnDirection() {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "relation" => false,
                "label" => "Position Order"
            ],
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "chapter_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "readonly" => true,
                "options" => Chapter::all()->pluck('positio_order', 'id'),
                "relation" => "chapter",
                "label" => "Chapter"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "translation" => true,
                "relation" => false,
                "label" => "Title"
            ],
            "url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "URL"
            ],
            "thumbnail" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Thumbnail"
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "options" => $this->getTypeOptions(),
                "relation" => false,
                "label" => "Type"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "filterable" => true,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
