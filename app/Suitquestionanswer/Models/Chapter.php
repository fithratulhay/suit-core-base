<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitquestionanswer\Models\Challenge;
use Suitquestionanswer\Models\Material;
use Suitquestionanswer\Models\Question;
use Suitquestionanswer\Models\UserChapter;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Chapter extends SuitModel
{
    // CONSTANTS
    const STATUS_DRAFTED = 'drafted';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'title',
        'challenge_id',
        'point_sum',
        'question_count',
        'material_count',
        'position_order',
        'status'
    ];

    public $rules = [
        'position_order' => 'numeric',
    ];

    public function getLabel()
    {
        return "Chapter";
    }

    public function getFormattedValue()
    {
        return $this->position_order;
    }
    
    public function getFormattedValueColumn()
    {
        return ['position_order'];
    }

    // RELATIONSHIPS
    public function challenge() {
        return $this->belongsTo(Challenge::class);
    }

    public function materials() {
        return $this->hasMany(Material::class);
    }

    public function publishedMaterials() {
        return $this->hasMany(Material::class)->where('status', self::STATUS_PUBLISHED)->orderBy('position_order', 'asc');
    }

    public function questions() {
        return $this->hasMany(Question::class);
    }

    public function publishedQuestions() {
        return $this->hasMany(Question::class)->where('status', self::STATUS_PUBLISHED)->orderBy('position_order', 'asc');
    }

    public function orderedQuestions() {
        return $this->hasMany(Question::class)->orderBy('position_order', 'asc');
    }

    public function getLastMaterialAttribute(){
        return $this->materials()->orderBy('position_order', 'desc')->first();
    }

    public function getLastQuestionAttribute(){
        return $this->questions()->orderBy('position_order', 'desc')->first();
    }

    public function userChapters() {
        return $this->hasMany(UserChapter::class);
    }

    // OPTIONS
    public function getStatusOptions(){
        return [
            self::STATUS_DRAFTED => ucfirst(self::STATUS_DRAFTED),
            self::STATUS_PUBLISHED => ucfirst(self::STATUS_PUBLISHED),
            self::STATUS_UNPUBLISHED => ucfirst(self::STATUS_UNPUBLISHED),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "challenge_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "readonly" => true,
                "relation" => "challenge",
                "label" => "Challenge"
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "relation" => false,
                "label" => "Position Order"
            ],
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "point_sum" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Total Points"
            ],
            "question_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Total Questions"
            ],
            "material_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Total Materials"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "filterable" => true,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
