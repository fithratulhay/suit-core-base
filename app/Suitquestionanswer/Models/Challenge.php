<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Cviebrock\EloquentSluggable\Sluggable;
use Suitquestionanswer\Models\Chapter;
use Suitquestionanswer\Models\UserChallenge;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Challenge extends SuitModel
{
    use Sluggable;

    // CONSTANTS
    const STATUS_DRAFTED = 'drafted';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'group_id',
        'title',
        'slug',
        'highlight',
        'description',
        'point_sum',
        'chapter_count',
        'is_double_point',
        'position_order',
        'start_datetime',
        'end_datetime',
        'status'
    ];

    public $rules = [
        'title' => 'max:50',
        'slug' => 'max:100',
        'highlight' => 'max:100',
        'position_order' => 'numeric',
    ];

    public function getLabel()
    {
        return "Challenge";
    }

    public function getFormattedValue()
    {
        return $this->title;
    }
    
    public function getFormattedValueColumn()
    {
        return ['title'];
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    // RELATIONSHIPS
    public function group() {
        return $this->belongsTo(ChallengeGroup::class, 'group_id');
    }

    public function chapters() {
        return $this->hasMany(Chapter::class);
    }

    public function orderedChapters() {
        return $this->hasMany(Chapter::class)->orderBy('position_order', 'asc');
    }

    public function publishedChapters() {
        return $this->hasMany(Chapter::class)->where('status', self::STATUS_PUBLISHED)->orderBy('position_order', 'asc');
    }

    public function userChallenges() {
        return $this->hasMany(UserChallenge::class);
    }

    public function getLastChapterAttribute(){
        return $this->chapters()->orderBy('position_order', 'desc')->first();
    }

    // OPTIONS
    public function getStatusOptions(){
        return [
            self::STATUS_DRAFTED => ucfirst(self::STATUS_DRAFTED),
            self::STATUS_PUBLISHED => ucfirst(self::STATUS_PUBLISHED),
            self::STATUS_UNPUBLISHED => ucfirst(self::STATUS_UNPUBLISHED),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "initiated" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Position Order"
            ],
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "group_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "readonly" => true,
                "relation" => 'group',
                "label" => "Challenge Group"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "translation" => true,
                "relation" => false,
                "label" => "Title"
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Optional Slug"
            ],
            "highlight" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "translation" => true,
                "relation" => false,
                "label" => "Highlight"
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "translation" => true,
                "relation" => false,
                "label" => "Description"
            ],
            "point_sum" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Total Points"
            ],
            "chapter_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Total Chapters"
            ],
            "is_double_point" => [
                "type" => self::TYPE_BOOLEAN,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Double Point"
            ],
            "start_datetime" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Start Time",
                "elmt_group" => [
                    "name" => "optional_active_datetime",
                    "type" => self::GROUPTYPE_DATETIMERANGE,
                    "role" => self::GROUPROLE_RANGE_START
                ]

            ],
            "end_datetime" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "End Time",
                "elmt_group" => [
                    "name" => "optional_active_datetime",
                    "type" => self::GROUPTYPE_DATETIMERANGE,
                    "role" => self::GROUPROLE_RANGE_END
                ]
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "filterable" => true,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
