<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitquestionanswer\Models\Challenge;
use Suitquestionanswer\Models\UserChallengeGroup;
/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class ChallengeGroup extends SuitModel
{

    // CONSTANTS
    const STATUS_DRAFTED = 'drafted';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'name',
        'status',
        'challenge_count',
        'status'
    ];

    public $rules = [
        
    ];

    public function getLabel()
    {
        return "Challenge Group";
    }

    public function getFormattedValue()
    {
        return $this->name;
    }
    
    public function getFormattedValueColumn()
    {
        return ['name'];
    }

    // RELATIONSHIPS
    public function challenges() {
        return $this->hasMany(Challenge::class, 'group_id');
    }

    public function publishedChallenges() {
        return $this->hasMany(Challenge::class, 'group_id')->where('status', self::STATUS_PUBLISHED)->orderBy('position_order', 'asc');;
    }

    public function userChallengeGroups() {
        return $this->hasMany(UserChallengeGroup::class, 'group_id');
    }

    public function getLastChallengeAttribute(){
        return $this->challenges()->orderBy('position_order', 'desc')->first();
    }

    // OPTIONS
    public function getStatusOptions(){
        return [
            self::STATUS_DRAFTED => ucfirst(self::STATUS_DRAFTED),
            self::STATUS_PUBLISHED => ucfirst(self::STATUS_PUBLISHED),
            self::STATUS_UNPUBLISHED => ucfirst(self::STATUS_UNPUBLISHED),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Name"
            ],
            "challenge_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Total Challenges"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "filterable" => true,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
