<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitquestionanswer\Models\Question;
use Suitquestionanswer\Models\Answer;
use Suitquestionanswer\Models\User;
use Suitquestionanswer\Models\UserChapter;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserAnswer extends SuitModel
{
    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'user_id' ,
        'question_id' ,
        'answer_id' ,
        'user_chapter_id',
        'is_correct',
        'answer_correct',
        'answer_wrong',
    ];

    public $rules = [

    ];

    public function getLabel(){
        return "Quiz";
    }

    // RELATIONSHIPS
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function question() {
        return $this->belongsTo(Question::class);
    }

    public function answer() {
        return $this->belongsTo(Answer::class);
    }

    public function userChapter(){
        return $this->belongsTo(UserChapter::class);
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => User::all()->pluck('name', 'id'),
                "filterable" => true,
                "relation" => 'user',
                "label" => "User"
            ],
            "question_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => Question::all()->pluck('question', 'id'),
                "filterable" => true,
                "relation" => 'question',
                "label" => "Question"
            ],
            "answer_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => Answer::all()->pluck('answer', 'id'),
                "filterable" => true,
                "relation" => 'answer',
                "label" => "Answer"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
