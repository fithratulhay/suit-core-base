<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitquestionanswer\Models\Chapter;
use Suitquestionanswer\Models\UserChallenge;
use Suitquestionanswer\Models\UserMaterial;
use Suitquestionanswer\Models\UserAnswer;
/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserChapter extends SuitModel
{
    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'user_id',
        'chapter_id',
        "user_challenge_id",
        'question_complete',
        'material_complete',
        'progress',
    ];

    public $rules = [

    ];

    // RELATIONSHIPS
    public function chapter() {
        return $this->belongsTo(Chapter::class);
    }

    public function user() {
        return $this->belongsTo(SuitUser::class);
    }

    public function userChallenge(){
        return $this->belongsTo(UserChallenge::class);
    }

    public function userMaterials(){
        return $this->hasMany(UserMaterial::class);
    }

    public function userAnswers(){
        return $this->hasMany(UserAnswer::class);
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => SuitUser::all()->pluck('name', 'id'),
                "filterable" => true,
                "relation" => 'user',
                "label" => "User"
            ],
            "chapter_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => Chapter::all()->pluck('position_order', 'id'),
                "filterable" => true,
                "relation" => 'chapter',
                "label" => "Chapter"
            ],
            "question_complete" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Question Complete"
            ],
            "material_complete" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Material Complete"
            ],
            "progress" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Progress"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
