<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitquestionanswer\Models\Material;
use Suitquestionanswer\Models\UserChapter;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserMaterial extends SuitModel
{
    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'user_id',
        'material_id',
        'user_chapter_id'
    ];

    public $rules = [

    ];

    // RELATIONSHIPS
    public function material() {
        return $this->belongsTo(Material::class);
    }

    public function user() {
        return $this->belongsTo(SuitUser::class);
    }

    public function userChapter() {
        return $this->belongsTo(UserChapter::class);
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => SuitUser::all()->pluck('name', 'id'),
                "filterable" => true,
                "relation" => 'user',
                "label" => "User"
            ],
            "material_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => Material::all()->pluck('title', 'id'),
                "filterable" => true,
                "relation" => 'material',
                "label" => "Material"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
