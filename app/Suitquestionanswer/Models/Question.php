<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitquestionanswer\Models\Chapter;
use Suitquestionanswer\Models\Answer;
use Suitquestionanswer\Models\UserAnswer;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Question extends SuitModel
{
    // CONSTANTS
    const STATUS_DRAFTED = 'drafted';
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';

    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'chapter_id',
        'answer_id',
        'question',
        'position_order',
        'point',
        'status'
    ];

    public $rules = [
        'position_order' => 'numeric',
    ];

    public function getLabel()
    {
        return "Question";
    }

    public function getFormattedValue()
    {
        return $this->question;
    }
    
    public function getFormattedValueColumn()
    {
        return ['question'];
    }

    // RELATIONSHIPS
    public function chapter() {
        return $this->belongsTo(Chapter::class);
    }

    public function answers() {
        return $this->hasMany(Answer::class);
    }

    public function orderedAnswers(){
        return $this->answers()->orderBy('position_order', 'asc');
    }

    public function getLastAnswerAttribute(){
        return $this->answers()->orderBy('position_order', 'desc')->first();
    }

    public function correctAnswer() {
        return $this->belongsTo(Answer::class, 'answer_id');
    }

    public function userAnswers() {
        return $this->hasMany(UserAnswer::class);
    }

    // OPTIONS
    public function getStatusOptions(){
        return [
            self::STATUS_DRAFTED => ucfirst(self::STATUS_DRAFTED),
            self::STATUS_PUBLISHED => ucfirst(self::STATUS_PUBLISHED),
            self::STATUS_UNPUBLISHED => ucfirst(self::STATUS_UNPUBLISHED),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Position Order"
            ],
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "chapter_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "initiated" => true,
                "readonly" => true,
                "relation" => "chapter",
                "label" => "Chapter"
            ],
            "question" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "translation" => true,
                "relation" => false,
                "label" => "Question"
            ],
            "answer_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "relation" => 'correctAnswer',
                "required" => false,
                "label" => "Correct Answer"
            ],
            "point" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Point Reward"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "filterable" => true,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
