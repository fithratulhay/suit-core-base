<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitquestionanswer\Models\Challenge;
use Suitquestionanswer\Models\UserChallengeGroup;
use Suitquestionanswer\Models\UserChapter;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserChallenge extends SuitModel
{
    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'user_id',
        'challenge_id',
        'user_group_id',
        'chapter_complete',
        'progress',
        'challenge_complete'        
    ];

    public $rules = [

    ];

    // RELATIONSHIPS
    public function challenge() {
        return $this->belongsTo(Challenge::class);
    }

    public function user() {
        return $this->belongsTo(SuitUser::class);
    }

    public function userChallengeGroup(){
        return $this->belongsTo(UserChallengeGroup::class, 'user_group_id');
    }

    public function userChapters(){
        return $this->hasMany(UserChapter::class);
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => SuitUser::all()->pluck('name', 'id'),
                "filterable" => true,
                "relation" => 'user',
                "label" => "User"
            ],
            "challenge_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "options" => Challenge::all()->pluck('title', 'id'),
                "filterable" => true,
                "relation" => 'challenge',
                "label" => "Challenge"
            ],
            "chapter_complete" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Chapter Complete"
            ],
            "progress" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Progress"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "filterable" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
