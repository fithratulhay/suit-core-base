<?php

namespace Suitquestionanswer\Models;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitquestionanswer\Models\ChallengeGroup;
use Suitquestionanswer\Models\UserChallenge;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserChallengeGroup extends SuitModel
{
    // MODEL DEFINITION
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        "user_id",
        "group_id",
    ];

    public $rules = [

    ];

    // RELATIONSHIPS
    public function challengeGroup() {
        return $this->belongsTo(ChallengeGroup::class, 'group_id');
    }

    public function user() {
        return $this->belongsTo(SuitUser::class);
    }

    public function userChallenges(){
        return $this->hasMany(UserChallenge::class, 'user_group_id');
    }

}
