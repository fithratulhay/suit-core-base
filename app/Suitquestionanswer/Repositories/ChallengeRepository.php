<?php

namespace Suitquestionanswer\Repositories;

use DB;
use Auth;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Suitquestionanswer\Models\Challenge;
use Suitquestionanswer\Models\UserChallengeGroup;
use Suitquestionanswer\Models\UserChallenge;
use Suitquestionanswer\Models\UserChapter;
use Suitquestionanswer\Models\ChallengeGroup;
use Suitquestionanswer\Repositories\Contract\ChallengeRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChallengeGroupRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;

class ChallengeRepository implements ChallengeRepositoryContract
{
    use SuitRepositoryTrait;

    protected $challengeGroupRepo, $userChallengeGroupRepo, $userRepo;

    public function __construct(Challenge $model, ChallengeGroupRepositoryContract $challengeGroupRepo, SuitUserRepositoryContract $userRepo) {
        $this->mainModel = $model;
        $this->challengeGroupRepo = $challengeGroupRepo;
        $this->userRepo = $userRepo;
    }

    public function created(Challenge $model){
        $this->challengeGroupRepo->denormalize($model->group_id);
    }

    public function updated(Challenge $model){
        $this->challengeGroupRepo->denormalize($model->group_id);

        if($model->getOriginal()['status'] === Challenge::STATUS_DRAFTED && $model->status === Challenge::STATUS_PUBLISHED){
            $users = $this->userRepo->getByParameter([
                // 'role' => $this->userRepo->getMainModel()::USER,
                'status' => $this->userRepo->getMainModel()::STATUS_ACTIVE
            ]);
    
            $link = route('frontend.challenge.index');
            $message = 'New Challenge "' . $model->title . '"';
    
            foreach($users as $user){
                $user->notify('\App\Notifications\SimpleDBNotification', $link, $message);
            }
        }
    }

    public function deleting(Challenge $model){
        return $model->group->challenge_count !== 1;
    }

    public function deleted(Challenge $model){
        $this->challengeGroupRepo->denormalize($model->group_id);
    }

    public function denormalize($id){
        $challenge = $this->find($id);

        if($challenge){
            $challenge_point_sum = 0;
            
            if($challenge->status === $challenge::STATUS_PUBLISHED){
                $chapters = $challenge->publishedChapters;
            }
            else{
                $chapters = $challenge->chapters;
            }

            foreach($chapters as $chapter){
                $challenge_point_sum += $chapter->point_sum;
            }

            $challenge->point_sum = $challenge_point_sum;
            $challenge->chapter_count = $chapters->count();
            $challenge->save();

            foreach($challenge->userChallenges as $userChallenge){
                $userChallenge->timestamps = false;
                $userChallenge->chapter_complete = $userChallenge->userChapters->where('progress', 100)->count();
                $userChallenge->progress = ($userChallenge->chapter_complete / $challenge->chapter_count) * 100; 
                $userChallenge->save();
            }

            return true;
        }

        return false;
    }

    // WIP
    public function reorder($groupId, $currentChallengeId = null){
        $challenges = $this->getByParameter([
            'group_id' => $groupId,
            'orderBy' => 'position_order',
            'orderType' => 'asc'
        ]);

        $currentPosition = 0;
        if($currentChallengeId){
            $currentChallenge = $challenges->find($currentChallengeId);
            if($currentChallenge){
                $challenges = $challenges->except($currentChallengeId);
                if($challenges->where('position_order', $currentChallenge->position_order)->first()){
                    $currentPosition = $currentChallenge->position_order;
                }
            }   
        }

        $positionOrder = 1;
        foreach($challenges as $challenge){
            if($positionOrder === $currentPosition){
                $currentChallenge->unsetEventDispatcher();
                $currentChallenge->position_order = $positionOrder++;
                $currentChallenge->save();
            }
            $challenge->unsetEventDispatcher();
            $challenge->position_order = $positionOrder++;
            $challenge->save();
        }
    }

    public function getFirstFromGroup($groupId){
        return $this->mainModel->where('group_id', $groupId)->orderBy('position_order', 'asc')->first();
    }
    
    public function getPreview($id){
        $challenge = $this->find__cached($id);

        if($challenge){
            $challenge->is_preview = true;
            $challenge->user_challenge = new Collection;
            $challenge->user_challenge->chapter_complete = 999;
            $challenge->user_challenge->progress = 100;
            $challenge->publishedChapters = $challenge->chapters;

            foreach($challenge->chapters as $chapter){
                $chapter->publishedMaterials = $chapter->materials;
                $chapter->user_chapter = new Collection;
                $chapter->user_chapter->progress = 100;
                $chapter->user_chapter->updated_at = '2019-01-01 01:01:01';
                $chapter->user_chapter->material_complete = 999;
            }

            return $challenge;
        }

        return false;
    }
}