<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserChallengeGroupRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}