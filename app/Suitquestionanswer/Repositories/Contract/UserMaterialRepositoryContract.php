<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserMaterialRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}