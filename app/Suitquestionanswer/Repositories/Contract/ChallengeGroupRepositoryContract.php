<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ChallengeGroupRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}