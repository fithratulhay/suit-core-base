<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ChallengeRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}