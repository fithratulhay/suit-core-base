<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserChallengeRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}