<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ChapterRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}