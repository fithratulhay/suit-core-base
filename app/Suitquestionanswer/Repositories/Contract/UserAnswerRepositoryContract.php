<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserAnswerRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}