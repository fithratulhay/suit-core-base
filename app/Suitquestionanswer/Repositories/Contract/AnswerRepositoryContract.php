<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface AnswerRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}