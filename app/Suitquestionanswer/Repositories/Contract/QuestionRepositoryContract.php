<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface QuestionRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}