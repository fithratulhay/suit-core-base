<?php

namespace Suitquestionanswer\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface MaterialRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}