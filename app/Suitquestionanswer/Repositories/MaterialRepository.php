<?php

namespace Suitquestionanswer\Repositories;

use Auth;
use Carbon\Carbon;
use Suitquestionanswer\Models\Material;
use Suitquestionanswer\Models\UserChapter;
use Suitquestionanswer\Models\UserMaterial;
use Suitquestionanswer\Repositories\Contract\MaterialRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChapterRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class MaterialRepository implements MaterialRepositoryContract
{
    use SuitRepositoryTrait;

    protected $chapterRepo;

    public function __construct(Material $model, ChapterRepositoryContract $chapterRepo) {
        $this->mainModel = $model;
        $this->chapterRepo = $chapterRepo;
    }
    
    public function created(Material $model){
        $this->chapterRepo->denormalize($model->chapter_id);
    }

    public function updated(Material $model){
        $this->chapterRepo->denormalize($model->chapter_id);
    }

    public function deleted(Material $model){
        $this->chapterRepo->denormalize($model->chapter_id);
    }

}