<?php

namespace Suitquestionanswer\Repositories;

use DB;
use Carbon\Carbon;
use Suitquestionanswer\Models\UserMaterial;
use Suitquestionanswer\Repositories\Contract\UserMaterialRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChapterRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserMaterialRepository implements UserMaterialRepositoryContract
{
    use SuitRepositoryTrait;

    protected $userChapterRepo;

    public function __construct(UserMaterial $model, UserChapterRepositoryContract $userChapterRepo) {
        $this->mainModel = $model;
        $this->userChapterRepo = $userChapterRepo;
    }
    
    public function created(UserMaterial $model){
        $this->userChapterRepo->denormalize($model->user_chapter_id);
    }

    public function updated(UserMaterial $model){
        $this->userChapterRepo->denormalize($model->user_chapter_id);
    }

}