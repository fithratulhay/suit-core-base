<?php

namespace Suitquestionanswer\Repositories;

use DB;
use Carbon\Carbon;
use Suitquestionanswer\Repositories\Contract\ChallengeFeatureRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChallengeGroupRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChallengeRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChapterRepositoryContract;
use Suitquestionanswer\Repositories\Contract\MaterialRepositoryContract;
use Suitquestionanswer\Repositories\Contract\QuestionRepositoryContract;
use Suitquestionanswer\Repositories\Contract\AnswerRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChallengeGroupRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChallengeRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChapterRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserMaterialRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserAnswerRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;

class ChallengeFeatureRepository implements ChallengeFeatureRepositoryContract
{
    protected $challengeGroupRepo, $challengeRepo, $chapterRepo, $materialRepo, $questionRepo, $answerRepo;
    protected $userChallengeGroupRepo, $userChallengeRepo, $userChapterRepo, $userMaterialRepo, $userQuestionRepo, $userAnswerRepo;
    protected $challengeGroupModel, $challengeModel, $chapterModel, $materialModel, $questionModel, $answerModel;
    protected $userRepo, $userModel;

    public function __construct(
        ChallengeGroupRepositoryContract $challengeGroupRepo,
        ChallengeRepositoryContract $challengeRepo,
        ChapterRepositoryContract $chapterRepo,
        MaterialRepositoryContract $materialRepo,
        QuestionRepositoryContract $questionRepo,
        AnswerRepositoryContract $answerRepo,

        UserChallengeGroupRepositoryContract $userChallengeGroupRepo,
        UserChallengeRepositoryContract $userChallengeRepo,
        UserChapterRepositoryContract $userChapterRepo,
        UserMaterialRepositoryContract $userMaterialRepo,
        UserAnswerRepositoryContract $userAnswerRepo,

        SuitUserRepositoryContract $userRepo
    ){
        $this->challengeGroupRepo = $challengeGroupRepo;
        $this->challengeRepo = $challengeRepo;
        $this->chapterRepo = $chapterRepo;
        $this->materialRepo = $materialRepo;
        $this->questionRepo = $questionRepo;
        $this->answerRepo = $answerRepo;

        $this->userChallengeGroupRepo = $userChallengeGroupRepo;
        $this->userChallengeRepo = $userChallengeRepo;
        $this->userChapterRepo = $userChapterRepo;
        $this->userMaterialRepo = $userMaterialRepo;
        $this->userAnswerRepo = $userAnswerRepo;

        $this->challengeGroupModel = $this->challengeGroupRepo->getMainModel();
        $this->challengeModel = $this->challengeRepo->getMainModel();
        $this->chapterModel = $this->chapterRepo->getMainModel();
        $this->materialModel = $this->materialRepo->getMainModel();
        $this->questionModel = $this->questionRepo->getMainModel();
        $this->answerModel = $this->answerRepo->getMainModel();

        $this->userChallengeGroupModel = $this->userChallengeGroupRepo->getMainModel();
        $this->userChallengeModel = $this->userChallengeRepo->getMainModel();
        $this->userChapterModel = $this->userChapterRepo->getMainModel();
        $this->userMaterialModel = $this->userMaterialRepo->getMainModel();
        $this->userAnswerModel = $this->userAnswerRepo->getMainModel();

        $this->userRepo = $userRepo;
        $this->userModel = $this->userRepo->getMainModel();
    }

    public function getList($userId){
        $challengeGroups = $this->challengeGroupRepo->getBy__cached('status', $this->challengeGroupModel::STATUS_PUBLISHED);

        if(!$challengeGroups->isEmpty()){
            foreach($challengeGroups as $challengeGroup){
                $userChallengeGroup = $challengeGroup->userChallengeGroups->where('user_id', $userId)->first();

                if(!$userChallengeGroup){
                    $userChallengeGroup = $this->userChallengeGroupRepo->create([
                        "user_id" => $userId,
                        "group_id" => $challengeGroup->id,
                    ]);
                }
            }

            $challenges = $this->challengeModel::leftJoin('user_challenges', function($join) use ($userId)
                            {
                                $join->on('challenges.id', '=', 'user_challenges.challenge_id');
                                $join->on('user_challenges.user_id', '=', DB::raw("'".$userId."'"));
                            })
                            ->leftJoin('user_challenge_groups', function($join) use ($userId)
                            {
                                $join->on('challenges.group_id', '=', 'user_challenge_groups.group_id');
                                $join->on('user_challenge_groups.user_id', '=', DB::raw("'".$userId."'"));
                            })
                            ->select('user_challenge_groups.challenge_complete as challenge_completed', 'user_challenges.*', 'challenges.*')
                            ->where('challenges.status', $this->challengeModel::STATUS_PUBLISHED)
                            ->whereIn('challenges.group_id', $challengeGroups->pluck('id'))
                            ->orderBy('group_id', 'asc')
                            ->orderBy('position_order', 'asc')
                            ->paginate(10);

            return $challenges;
        }

        return false;
    }

    public function getDetail($userId, $challengeSlug){
        $challenge = $this->challengeRepo->getBy__cached('slug', $challengeSlug)->first();
        
        if($challenge && $this->challengeModel::STATUS_PUBLISHED){
            $userChallenge = $challenge->userChallenges->where('user_id', $userId)->first();
            // timing check
            if($challenge->start_datetime !== null && $challenge->end_datetime !== null && ( $userChallenge == null || ($userChallenge && $userChallenge->progress !== 100)) ){
                if($challenge->start_datetime > Carbon::now()->toDateTimeString()){
                    session(['challengeError' => __('frontend.popup.challengeNotStart')]);
                    return false;
                }
                if($challenge->end_datetime <= Carbon::now()->toDateTimeString()){
                    session(['challengeError' => __('frontend.popup.challengeExpired')]);
                    return false;
                }
            }
            
            $userChallengeGroup = $this->userChallengeGroupModel::where('group_id', $challenge->group_id)->where('user_id', $userId)->first();

            if($userChallengeGroup && ($userChallengeGroup->challenge_complete+1) >= $challenge->position_order){
                
                if(!$userChallenge){
                    $userChallenge = $this->userChallengeRepo->create([
                        "user_id" => $userId,
                        "challenge_id" => $challenge->id,
                        "user_group_id" => $userChallengeGroup->id,
                        "progress" => 0,
                    ]);
                }

                $challenge->user_challenge = $userChallenge;

                foreach($challenge->publishedChapters as $chapter){
                    if(($userChallenge->chapter_complete+1) >= $chapter->position_order){
                        $userChapter = $this->userChapterModel::where('user_id', $userId)->where('chapter_id', $chapter->id)->first();
                        if(!$userChapter){
                            $userChapter = $this->userChapterRepo->create([
                                "user_id" => $userId,
                                "chapter_id" => $chapter->id,
                                "user_challenge_id" => $userChallenge->id,
                                "progress" => 0,
                            ]);
                        }
                        
                        $challenge->publishedChapters->find($chapter->id)->user_chapter = $userChapter;
                    }
                }
                
                return $challenge;
            }
            else{
                session(['challengeError' => __('frontend.popup.challengeNotComplete')]);
            }
        }

        return false;
    }

    public function getChapter($userId, $chapterId){
        $chapter = $this->chapterRepo->find__cached($chapterId);

        if($chapter && $chapter->status === $this->chapterModel::STATUS_PUBLISHED){
            $userChallenge = $this->userChallengeModel::where('challenge_id', $chapter->challenge_id)->where('user_id', $userId)->first();
            
            // Challenge progress check
            if($userChallenge){
                $challenge = $this->challengeRepo->find__cached($userChallenge->challenge_id);
                
                // timing check
                if($challenge->start_datetime !== null && $challenge->end_datetime !== null && $userChallenge && $userChallenge->progress !== 100){
                    if($challenge->start_datetime >= Carbon::now()->toDateTimeString() || $challenge->end_datetime <= Carbon::now()->toDateTimeString()){
                        return false;
                    }
                }
                
                if(($userChallenge->chapter_complete+1) >= $chapter->position_order){
                    $userChallengeGroup = $userChallenge->userChallengeGroup;

                    // Challenge Group progress check
                    if($userChallengeGroup && ($userChallengeGroup->challenge_complete+1) >= $this->challengeGroupRepo->find__cached($userChallengeGroup->group_id)->position_order){  
                        $userChapter = $userChallenge->userChapters->where('chapter_id', $chapter->id)->first();
                        if($userChapter){
                            if($userChapter->progress === 100){
                                $chapterId = $chapter->id;
                                $userAnswerClass = get_class($this->userAnswerModel);
                                $questions = $this->questionModel::leftJoin('user_answers', function($join) use ($userId, $chapterId)
                                                {
                                                    $join->on('questions.id', '=', 'user_answers.question_id');
                                                    $join->on('user_answers.user_id', '=', DB::raw("'".$userId."'"));
                                                })
                                ->select('questions.*', 'user_answers.answer_id as user_answer_id')
                                ->where('questions.chapter_id', $chapterId)
                                ->orderBy('position_order', 'asc')
                                ->get();
    
                                $chapter->questions = $questions;
    
                                $chapter->answer_correct = 0;
                                $chapter->answer_wrong = 0;
                                $chapter->user_point_sum = 0;
    
                                foreach($chapter->questions as $question){
                                    if($question->answer_id === $question->user_answer_id){
                                        $question->is_correct = 1;
                                        $chapter->answer_correct += 1;
                                        $chapter->user_point_sum += $question->point * ($challenge->is_double_point ? 2 : 1);
                                    }
                                    else{
                                        $chapter->answer_wrong += 1;
                                    }
                                }
    
                                return ['action' => 'finish', 'chapter' => $chapter];
                            }
                            else if($userChapter->material_complete === $chapter->material_count){
                                return ['action' => 'progress', 'slug' => $challenge->slug];
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function showChapter($userId, $chapterId){
        $chapter = $this->chapterRepo->find__cached($chapterId);

        if($chapter && $chapter->status === $this->chapterModel::STATUS_PUBLISHED){
            $userChallenge = $this->userChallengeModel::where('challenge_id', $chapter->challenge_id)->where('user_id', $userId)->first();
                
            // Challenge progress check
            if($userChallenge){
                $challenge = $this->challengeRepo->find__cached($userChallenge->challenge_id);
                
                // timing check
                if($challenge->start_datetime !== null && $challenge->end_datetime !== null){
                    if($challenge->start_datetime >= Carbon::now()->toDateTimeString() || $challenge->end_datetime <= Carbon::now()->toDateTimeString()){
                        return false;
                    }
                }

                if(($userChallenge->chapter_complete+1) >= $chapter->position_order){
                    $userChallengeGroup = $userChallenge->userChallengeGroup;

                    // Challenge Group progress check
                    if($userChallengeGroup && ($userChallengeGroup->challenge_complete+1) >= $this->challengeGroupRepo->find__cached($userChallengeGroup->group_id)->position_order){  
                        $userChapter = $userChallenge->userChapters->where('chapter_id', $chapter->id)->first();
                        
                        if($userChapter->progress !== 100 && $userChapter->material_complete === $chapter->material_count){
                            $question = $this->questionModel::where('chapter_id', $chapterId)->where('position_order', '>',$userChapter->question_complete)->first();
                            
                            if($question){
                                $userAnswer = $this->userAnswerModel::where('user_id', $userId)->where('question_id', $question->id)->first();

                                if(!$userAnswer){
                                    // ajax show

                                    $answers = [];

                                    foreach($question->orderedAnswers as $answer){
                                        $answers[] = [
                                            "answerValue" => $answer->id,
                                            "answerText" => $answer->answer__trans
                                        ];
                                    }

                                    return json_encode([
                                        "quizName" => "Chapter " . $chapter->position_order,
                                        "chapterId" => $chapter->id,
                                        "progressCurrent" => $userChapter->question_complete + 1,
                                        "progressFinish" => $chapter->question_count,
                                        "point" => $question->point,
                                        "question" => $question->question__trans,
                                        "answers" => $answers,
                                        "status" => ($userChapter->question_complete + 1) === $chapter->question_count ? "finished" : "unfinished",
                                    ]);
                                }
                            }
                        }
                        else{
                            // display all

                        }
                    } 
                }
            }
        }

        return false;       
    }

    public function answerQuestion($userId, $answerId){
        $answer = $this->answerRepo->find__cached($answerId);

        if($answer){

            $question = $answer->question;

            if($question && $question->status === $this->questionModel::STATUS_PUBLISHED){
                $chapter = $this->chapterRepo->find__cached($question->chapter_id);
                $userChapter = $chapter->userChapters->where('user_id', $userId)->first();

                // Chapter check
                if($userChapter && ($userChapter->question_complete+1) >= $question->position_order && $userChapter->material_complete === $chapter->material_count){
                    $userChallenge = $userChapter->userChallenge;
                    if($userChallenge){
                        $challenge = $this->challengeRepo->find__cached($userChallenge->challenge_id);

                        // Timing check
                        if($challenge->start_datetime !== null && $challenge->end_datetime !== null){
                            if($challenge->start_datetime >= Carbon::now()->toDateTimeString() || $challenge->end_datetime <= Carbon::now()->toDateTimeString()){
                                return false;
                            }
                        }               
                        
                        if(($userChallenge->chapter_complete+1) >= $chapter->position_order){
                            $userChallengeGroup = $userChallenge->userChallengeGroup;

                            // Challenge Group check
                            if($userChallengeGroup && ($userChallengeGroup->challenge_complete+1) >= $this->challengeGroupRepo->find__cached($userChallengeGroup->group_id)->position_order){
                                // eligible
                                $userAnswer = $question->userAnswers->where('user_id', $userId)->first();

                                if(!$userAnswer){
                                    // for denormalize correct and wrong answer count
                                    // $prevUserAnswer = $this->userAnswerModel->where('user_id', $userId)->orderBy('created_at', 'desc')->first();
                                    $user = $this->userRepo->find__cached($userId);

                                    $userAnswer = $this->userAnswerRepo->create([
                                        'user_id' => $userId,
                                        'question_id' => $question->id,
                                        'answer_id' => $answerId,
                                        'user_chapter_id' => $userChapter->id,
                                        'is_correct' => $question->answer_id === $answerId,
                                        // 'answer_correct' => $prevUserAnswer ? ($question->answer_id === $answerId ? $prevUserAnswer->answer_correct + 1 : $prevUserAnswer->answer_correct ) : ($question->answer_id === $answerId ? 1 : 0 ),
                                        // 'answer_wrong' => $prevUserAnswer ? ($question->answer_id === $answerId ? $prevUserAnswer->answer_wrong : $prevUserAnswer->answer_wrong + 1 ) : ($question->answer_id === $answerId ? 0 : 1 ),
                                        'answer_correct' => $question->answer_id === $answerId ? $user->answer_correct + 1 : $user->answer_correct,
                                        'answer_wrong' => $question->answer_id === $answerId ? $user->answer_wrong : $user->answer_wrong + 1,
                                    ]);

                                    $nextQuestion = $this->questionModel::where('chapter_id', $userChapter->chapter_id)->where('position_order', '>', $question->position_order)->first();
                                    
                                    // Denorm user_challenges
                                    if(!$nextQuestion){
                                        $userChallenge->chapter_complete += 1;

                                        if($userChallenge->chapter_complete === $challenge->chapter_count){
                                            // $prevUserChallenge = $this->userChallengeModel->where('user_id', $userId)->orderBy('created_at', 'desc')->first();
                                            // if($prevUserChallenge){
                                            //     $userChallenge->challenge_complete = $prevUserChallenge->challenge_complete + 1; 
                                            // }
                                            // else{
                                            //     $userChallenge->challenge_complete = 1;
                                            // }
                                            
                                            $userChallenge->challenge_complete = $user->challenge_complete + 1;
                                        }

                                        $userChallenge->save();
                                    }

                                    // Reward
                                    if($question->answer_id === $answerId){
                                        // impact, sample from suitreward point
                                        /*
                                        $this->userPointRepo->record($userId, $question->point * ($question->is_double_point ? 2 : 1), get_class($userAnswer),$userAnswer->id, $this->userPointModel::TYPE_REWARD, 'Correctly answered "' . $question->question__trans . '"!', [
                                            'note_trans_id' => 'Menjawab dengan benar "' . find_trans(get_class($question), $question->id, 'question', 'id') . '"!',
                                        ],[
                                            // denormalize
                                            'answer_correct' => $userAnswer->answer_correct,
                                            'challenge_complete' => $userChallenge->challenge_complete,
                                        ]);
                                        */
                                    }
                                    else{
                                        // denormalize
                                        $this->userRepo->update($userId, [
                                            'answer_wrong' => $userAnswer->answer_wrong,
                                            'challenge_complete' => $userChallenge->challenge_complete,
                                        ]);
                                    }

                                    if(!$nextQuestion){
                                        // if no next question
                                        return json_encode([
                                            "correct_answer_id" => $question->answer_id,
                                            "status" => "finished",
                                            "nextURL" => route('frontend.challenge.show', ['slug' => $challenge->slug])
                                        ], JSON_UNESCAPED_SLASHES);
                                    }
                                    else{
                                        foreach($nextQuestion->orderedAnswers as $answer){
                                            $answers[] = [
                                                "answerValue" => $answer->id,
                                                "answerText" => $answer->answer
                                            ];
                                        }
    
                                        return json_encode([
                                            "correct_answer_id" => $question->answer_id,
                                            "quizName" => "Chapter " . $chapter->position_order,
                                            "chapterId" => $chapter->id,
                                            "progressCurrent" => $userChapter->question_complete + 2,
                                            "point" => $nextQuestion->point,
                                            "question" => $nextQuestion->question,
                                            "answers" => $answers,
                                            "status" => ($userChapter->question_complete + 2) === $chapter->question_count ? "finished" : "unfinished", 
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }                
                // Question Check
            }
            // Answer Check
        }

        return json_encode([
            "status" => "invalid",
            "nextURL" => route('frontend.challenge.index')
        ], JSON_UNESCAPED_SLASHES);
    }

    public function readMaterial($userId, $materialId){
        $material = $this->materialRepo->find__cached($materialId);
        
        if($material && $material->status === $material::STATUS_PUBLISHED){
            
            $userChapter = $this->userChapterModel::where('chapter_id', $material->chapter_id)->where('user_id', $userId)->first();

            // Chapter progress check
            if($userChapter && ($userChapter->material_complete+1) >= $material->position_order){
                $userChallenge = $userChapter->userChallenge;
                
                // Challenge progress check
                if($userChallenge){
                    $challenge = $this->challengeRepo->find__cached($userChallenge->challenge_id);

                    // Challenge timing check
                    if($challenge->start_datetime !== null && $challenge->end_datetime !== null){
                        if($challenge->start_datetime >= Carbon::now()->toDateTimeString() || $challenge->end_datetime <= Carbon::now()->toDateTimeString()){
                            return false;
                        }
                    }

                    $chapter = $material->chapter;

                    if(($userChallenge->chapter_complete+1) >= $chapter->position_order){
                        $userChallengeGroup = $userChallenge->userChallengeGroup;

                        // Challenge Group progress check
                        if($userChallengeGroup && ($userChallengeGroup->challenge_complete+1) >= $this->challengeGroupRepo->find__cached($userChallengeGroup->group_id)->position_order){

                            $userMaterial = $material->userMaterials->where('user_id', $userId)->first();

                            if(!$userMaterial){
                                $this->userMaterialRepo->create([
                                    'user_id' => $userId,
                                    'material_id' => $materialId,
                                    'user_chapter_id' => $userChapter->id
                                ]);
                                
                                $nextMaterial = $this->materialModel::where('chapter_id', $material->chapter_id)->where('position_order', '>', $material->position_order)->first();
            
                                if($nextMaterial){
                                    return ['action' => 'progress', 'material' => $nextMaterial];
                                }
                                else{
                                    return ['action' => 'finish', 'id' => $userChapter->chapter_id, 'url' => route('frontend.chapter.show', ['id' => $userChapter->chapter_id])];
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function initiateProgress($userId, $groupId){
        $userChallengeGroup = $this->userChallengeGroupRepo->create([
            "user_id" => $userId,
            "group_id" => $groupId,
        ]);

        $firstChallenge = $this->challengeRepo->getFirstFromGroup__cached($groupId);

        $userChallenge = $this->userChallengeRepo->create([
            "user_id" => $userId,
            "challenge_id" => $firstChallenge->id,
            "user_group_id" => $userChallengeGroup->id,
            "progress" => 0,
        ]);

        $firstChapter = $this->chapterRepo->getFirstFromChallenge__cached($firstChallenge->id);

        $userChapter = $this->userChapterRepo->create([
            "user_id" => $userId,
            "chapter_id" => $firstChapter->id,
            "user_challenge_id" => $userChallenge->id,
            "progress" => 0,
        ]);
    }

    public function denormalize($id){
        $challengeGroup = $this->find($id);

        if($challengeGroup){

            if($challengeGroup->status === $challengeGroup::STATUS_PUBLISHED){
                $challengeGroup->challenge_count = $challengeGroup->publishedChallenges->count();
            }
            else{
                $challengeGroup->challenge_count = $challengeGroup->challenges->count();
            }

            $challengeGroup->save();

            foreach($challengeGroup->userChallengeGroups as $userChallengeGroup){
                $userChallengeGroup->timestamps = false;
                $userChallengeGroup->challenge_complete = $userChallengeGroup->userChallenges->where('progress', 100)->count();
                $userChallengeGroup->save();
            }

            return true;

        }

        return false;
    }
    
    /**
     * 
     */
    public function publish($id){
        $group = $this->challengeGroupRepo->find($id);

        if($group){

            // must have at least 1 Challenge
            $challenges = $group->challenges;

            if($challenges->isEmpty()){
                return ['is_success' => false, 'message' => 'Must create at least 1 challenge.', 'route' => 'backend.challengegroup.show', 'route_param' => ['id' => $id]];
            }

            // every challenges must at least have 1 chapter
            
            // Check
            foreach($challenges as $challenge){
                $chapters = $challenge->chapters;

                if($chapters->isEmpty()){
                    return ['is_success' => false, 'message' => 'Every challenge must have at least 1 chapter.', 'route' => 'backend.challenge.show', 'route_param' => ['id' => $challenge->id]];
                }
                
                // every chapters must have at least 1 question

                foreach($chapters as $chapter){
                    $questions = $chapter->questions;

                    if($questions->isEmpty()){
                        return ['is_success' => false, 'message' => 'Every chapter must have at least 1 question.', 'route' => 'backend.chapter.show', 'route_param' => ['id' => $chapter->id]];
                    }

                    // every question must have at least 4 answers and 1 correct answer
                    
                    foreach($questions as $question){
                        if($question->answer_id === null){
                            return ['is_success' => false, 'message' => 'Every question must have a correct answer.', 'route' => 'backend.question.show', 'route_param' => ['id' => $question->id]];
                        }

                        $answers = $question->answers;

                        if($answers->count() !== 4){
                            return ['is_success' => false, 'message' => 'Every question must have 4 answer choices.', 'route' => 'backend.question.show', 'route_param' => ['id' => $question->id]];
                        }
                    }
                }
            }

            // Denorm status, flag, and timing
            foreach($challenges as $challenge){
                $chapters = $challenge->chapters;

                foreach($chapters as $chapter){
                    $materials = $chapter->materials;

                    foreach($materials as $material){
                        $material->status = $this->challengeGroupModel::STATUS_PUBLISHED;
                        $material->start_datetime = $challenge->start_datetime;
                        $material->end_datetime = $challenge->end_datetime;
                        $material->save();
                    }

                    $questions = $chapter->questions;

                    foreach($questions as $question){
                        $question->status = $this->challengeGroupModel::STATUS_PUBLISHED;
                        $question->is_double_point = $challenge->is_double_point;
                        $question->start_datetime = $challenge->start_datetime;
                        $question->end_datetime = $challenge->end_datetime;
                        $question->save();
                    }

                    $chapter->status = $this->challengeGroupModel::STATUS_PUBLISHED;
                    $chapter->start_datetime = $challenge->start_datetime;
                    $chapter->end_datetime = $challenge->end_datetime;
                    $chapter->save();
                }  

                $challenge->status = $this->challengeGroupModel::STATUS_PUBLISHED;
                $challenge->save();
            }

            switch($group->status){
                case $group::STATUS_DRAFTED:
                    $group->status = $group::STATUS_PUBLISHED;
                    break;
                case $group::STATUS_UNPUBLISHED:
                    $group->status = $group::STATUS_PUBLISHED;
                    break;
            }

            $group->save();

            return ['is_success' => true, 'message' => 'Status changed to ' . ucfirst($group->status)];
        }

        return ['is_success' => false, 'message' => 'Unknown error, please contact dev team.', 'route' => 'backend.challengegroup.index', 'route_param' => []];
    }

    public function getNewestChallenge(){
        $challengeGroup = $this->challengeGroupModel->where('status', $this->challengeGroupModel::STATUS_PUBLISHED)->orderBy('created_at', 'desc')->first();

        if($challengeGroup){
            $challenge = $this->challengeModel->where('status', $this->challengeModel::STATUS_PUBLISHED)->orderBy('position_order', 'desc')->first();
            
            if($challenge){
                // Timing check
                if($challenge->start_datetime !== null && $challenge->end_datetime !== null){
                    if($challenge->start_datetime >= Carbon::now()->toDateTimeString() || $challenge->end_datetime <= Carbon::now()->toDateTimeString()){
                        return false;
                    }
                }
                
                return $challenge;
            }
        }
        
        return false;
    }

    public function getLatestUserChallenge($userId){
        return $this->userChallengeModel->where('user_id', $userId)->with('challenge')->orderBy('updated_at', 'desc')->first();
    }

    public function challengeAlmostEndNotif(){
        $users = $this->userRepo->getByParameter__cached([
            // 'role' => $this->userRepo->getMainModel()::USER,
            'status' => $this->userModel::STATUS_ACTIVE
        ], [
            'optional_dependency' => 'userChallenges'
        ]);

        $challenges =   $this->challengeModel->where('status', $this->challengeModel::STATUS_PUBLISHED)
                                             ->where(function ($query) {
                                                $query->where('end_datetime', '>=', Carbon::now()->format('Y-m-d 00:00:00'))
                                                      ->where('end_datetime', '!=', null);
                                                })
                                             ->get();
        
        foreach($challenges as $key => $challenge){
            $now = Carbon::now()->format('Y-m-d 00:00:00');

            if($now == Carbon::parse($challenge->end_datetime)->format('Y-m-d 00:00:00')){
                $challenge->notifiableMessage = 'Challenge almost end! Finish it before it gone!';
            }
            else if($now == Carbon::parse($challenge->end_datetime)->subDays(3)->format('Y-m-d 00:00:00')){
                $challenge->notifiableMessage = 'Challenge almost end! Finish it before it gone!';
            }
            else if($now == Carbon::parse($challenge->end_datetime)->subDays(7)->format('Y-m-d 00:00:00')){
                $challenge->notifiableMessage = 'Challenge almost end! Finish it before it gone!';
            }
            else{
                $challenges->forget($key);
            }

        }

        foreach($users as $user){
            foreach($challenges as $challenge){
                $userChallenge = $this->userChallengeModel->where('user_id', $user->id)->where('challenge_id', $challenge->id)->first();
                
                if($userChallenge && $userChallenge->progress == 100){           
                    continue;
                }

                $link = route('frontend.challenge.index');
                $message = $challenge->notifiableMessage;
                $user->notify('\App\Notifications\SimpleDBNotification', $link, $message);
                break;
            }
        }
    }
}