<?php

namespace Suitquestionanswer\Repositories;

use DB;
use Carbon\Carbon;
use Suitquestionanswer\Models\UserChapter;
use Suitquestionanswer\Repositories\Contract\UserChapterRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChallengeRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserChapterRepository implements UserChapterRepositoryContract
{
    use SuitRepositoryTrait;

    protected $userChallengeRepo;

    public function __construct(UserChapter $model, UserChallengeRepositoryContract $userChallengeRepo) {
        $this->mainModel = $model;
        $this->userChallengeRepo = $userChallengeRepo;
    }

    public function created(UserChapter $model){
        $this->userChallengeRepo->denormalize($model->user_challenge_id);
    }

    public function updated(UserChapter $model){
        $this->userChallengeRepo->denormalize($model->user_challenge_id);
    }

    public function denormalize($id){
        $userChapter = $this->find($id);

        if($userChapter){
            $userChapter->question_complete = $userChapter->userAnswers->count();
            $userChapter->material_complete = $userChapter->userMaterials->count();
            $userChapter->progress = ($userChapter->question_complete / $userChapter->chapter->question_count) * 100;
            $userChapter->save();

            return true;
        }

        return false;
    }

}