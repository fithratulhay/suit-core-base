<?php

namespace Suitquestionanswer\Repositories;

use DB;
use Carbon\Carbon;
use Suitquestionanswer\Models\UserChallengeGroup;
use Suitquestionanswer\Repositories\Contract\UserChallengeGroupRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserChallengeGroupRepository implements UserChallengeGroupRepositoryContract
{
    use SuitRepositoryTrait;
    
    public function __construct(UserChallengeGroup $model) {
        $this->mainModel = $model;
    }
    
    public function denormalize($id){
        $userChallengeGroup = $this->find($id);

        if($userChallengeGroup){
            $userChallengeGroup->challenge_complete = $userChallengeGroup->userChallenges->where('progress', 100)->count();
            $userChallengeGroup->save();

            return true;
        }

        return false;
    }
    
}