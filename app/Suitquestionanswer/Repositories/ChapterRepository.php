<?php

namespace Suitquestionanswer\Repositories;

use Auth;
use DB;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Suitquestionanswer\Models\ChallengeGroup;
use Suitquestionanswer\Models\Chapter;
use Suitquestionanswer\Models\UserChapter;
use Suitquestionanswer\Models\UserAnswer;
use Suitquestionanswer\Models\Question;
use Suitquestionanswer\Models\Answer;
use Suitquestionanswer\Repositories\Contract\ChapterRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChallengeRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use function GuzzleHttp\json_encode;

class ChapterRepository implements ChapterRepositoryContract
{
    use SuitRepositoryTrait;

    protected $challengeRepo;

    public function __construct(Chapter $model, ChallengeRepositoryContract $challengeRepo) {
        $this->mainModel = $model;
        $this->challengeRepo = $challengeRepo;
    }

    public function created(Chapter $model){
        $this->challengeRepo->denormalize($model->challenge_id);
        
    }

    public function updated(Chapter $model){
        $this->challengeRepo->denormalize($model->challenge_id);
    }

    public function deleting(Chapter $model){
        return $model->challenge->chapter_count !== 1;
    }

    public function deleted(Chapter $model){
        $this->challengeRepo->denormalize($model->challenge_id);
    }

    public function denormalize($id){
        $chapter = $this->find($id);

        if($chapter){
            $chapter_point_sum = 0;

            if($chapter->status === $chapter::STATUS_PUBLISHED){
                $questions = $chapter->publishedQuestions;
            }
            else{
                $questions = $chapter->questions;
            }

            foreach($questions as $question){
                $chapter_point_sum += $question->point;
            }

            $chapter->point_sum = $chapter_point_sum;
            $chapter->question_count = $questions->count();

            if($chapter->status === $chapter::STATUS_PUBLISHED){
                $chapter->material_count = $chapter->publishedMaterials->count();
            }
            else{
                $chapter->material_count = $chapter->materials->count();
            }

            $chapter->save();

            foreach($chapter->userChapters as $userChapter){
                $userChapter->timestamps = false;
                $userChapter->question_complete = $userChapter->userAnswers->count();
                $userChapter->material_complete = $userChapter->userMaterials->count();
                $userChapter->progress = ($userChapter->question_complete / $chapter->question_count) * 100;
                $userChapter->save();
            }

            return true;
        }

        return false;
    }

    public function getFirstFromChallenge($challengeId){
        return $this->mainModel->where('challenge_id', $challengeId)->orderBy('position_order', 'asc')->first();
    }

    public function getPreview($id){
        $chapter = $this->find__cached($id);

        if($chapter){
            $chapter->is_preview = true;
            $chapter->answer_correct = $chapter->question_count;
            $chapter->answer_wrong = 0;
            $chapter->user_point_sum = $chapter->point_sum;

            $chapter->questions = $chapter->orderedQuestions;

            foreach($chapter->questions as $question){
                $question->is_correct = true;
                $question->user_answer_id = $question->answer_id;
            }

            return $chapter;
        }

        return false;
    }
    
}