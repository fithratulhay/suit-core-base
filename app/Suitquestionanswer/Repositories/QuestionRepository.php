<?php

namespace Suitquestionanswer\Repositories;

use Carbon\Carbon;
use Suitquestionanswer\Models\Question;
use Suitquestionanswer\Repositories\Contract\QuestionRepositoryContract;
use Suitquestionanswer\Repositories\Contract\ChapterRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class QuestionRepository implements QuestionRepositoryContract
{
    use SuitRepositoryTrait;

    protected $chapterRepo;

    public function __construct(Question $model, ChapterRepositoryContract $chapterRepo) {
        $this->mainModel = $model;
        $this->chapterRepo = $chapterRepo;
    }
    
    public function created(Question $model){
        $this->chapterRepo->denormalize($model->chapter_id);
    }

    public function updated(Question $model){
        $this->chapterRepo->denormalize($model->chapter_id);
    }

    public function deleted(Question $model){
        $this->chapterRepo->denormalize($model->chapter_id);
    }
    
}