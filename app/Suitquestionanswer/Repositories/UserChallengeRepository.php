<?php

namespace Suitquestionanswer\Repositories;

use Suitquestionanswer\Models\UserChallenge;
use Suitquestionanswer\Repositories\Contract\UserChallengeRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChallengeGroupRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserChallengeRepository implements UserChallengeRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(UserChallenge $model, UserChallengeGroupRepositoryContract $userChallengeGroupRepo) {
        $this->mainModel = $model;
        $this->userChallengeGroupRepo = $userChallengeGroupRepo;
    }

    public function created(UserChallenge $model){
        $this->userChallengeGroupRepo->denormalize($model->user_group_id);
    }

    public function updated(UserChallenge $model){
        $this->userChallengeGroupRepo->denormalize($model->user_group_id);
    }

    public function denormalize($id){
        $userChallenge = $this->find($id);

        if($userChallenge){
            $userChallenge->chapter_complete = $userChallenge->userChapters->where('progress', 100)->count();
            $userChallenge->progress = ($userChallenge->chapter_complete / $userChallenge->challenge->chapter_count) * 100;
            $userChallenge->save();

            return true;
        }

        return false;
    }

}