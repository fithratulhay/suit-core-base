<?php

namespace Suitquestionanswer\Repositories;

use Carbon\Carbon;
use Suitquestionanswer\Models\ChallengeGroup;
use Suitquestionanswer\Repositories\Contract\ChallengeGroupRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class ChallengeGroupRepository implements ChallengeGroupRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(ChallengeGroup $model) {
        $this->mainModel = $model;
    }

    public function denormalize($id){
        $challengeGroup = $this->find($id);

        if($challengeGroup){

            if($challengeGroup->status === $challengeGroup::STATUS_PUBLISHED){
                $challengeGroup->challenge_count = $challengeGroup->publishedChallenges->count();
            }
            else{
                $challengeGroup->challenge_count = $challengeGroup->challenges->count();
            }

            $challengeGroup->save();

            foreach($challengeGroup->userChallengeGroups as $userChallengeGroup){
                $userChallengeGroup->timestamps = false;
                $userChallengeGroup->challenge_complete = $userChallengeGroup->userChallenges->where('progress', 100)->count();
                $userChallengeGroup->save();
            }

            return true;

        }

        return false;
    }

    public function unpublish($id){
        $group = $this->mainModel->find($id);

        if($group && $group->status === $group::STATUS_PUBLISHED){
            $challenges = $group->challenges;

            // Denorm status, flag, and timing
            foreach($challenges as $challenge){
                $challenge->status = ChallengeGroup::STATUS_UNPUBLISHED;
                $challenge->save();

                $chapters = $challenge->chapters;

                foreach($chapters as $chapter){
                    $chapter->status = ChallengeGroup::STATUS_UNPUBLISHED;
                    $chapter->save();

                    $materials = $chapter->materials;

                    foreach($materials as $material){
                        $material->status = ChallengeGroup::STATUS_UNPUBLISHED;
                        $material->save();
                    }

                    $questions = $chapter->questions;

                    foreach($questions as $question){
                        $question->status = ChallengeGroup::STATUS_UNPUBLISHED;
                        $question->save();
                    }
                }  
            }
            $group->status = $group::STATUS_UNPUBLISHED;
            $group->save();
            
            return ['is_success' => true, 'message' => 'Status changed to ' . ucfirst($group->status)];
        }

        return ['is_success' => false, 'message' => 'Challenge Group must be ' . ucfirst(ChallengeGroup::STATUS_PUBLISHED) . ' first.', 'route' => 'backend.challengegroup.index', 'route_param' => []];
    }

    public function getPreview($id){
        $challengeGroup = $this->find__cached($id);

        if($challengeGroup){
            $challenges = $challengeGroup->challenges;

            if(!$challenges->isEmpty()){
                foreach($challenges as $challenge){
                    $challenge->challenge_complete = 999;
                    $challenge->is_preview = true;
                }
                return $challenges;
            }
        }

        return false;
    }
}