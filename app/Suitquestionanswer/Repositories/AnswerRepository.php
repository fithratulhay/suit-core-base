<?php

namespace Suitquestionanswer\Repositories;

use Carbon\Carbon;
use Suitquestionanswer\Models\Answer;
use Suitquestionanswer\Repositories\Contract\AnswerRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class AnswerRepository implements AnswerRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Answer $model) {
        $this->mainModel = $model;
    }
    
    public function setAsCorrect($id){
        $answer = $this->mainModel->find($id);

        if($answer){
            $question = $answer->question;

            if($question){
                $question->answer_id = $id;
                $question->save();

                return $question;
            }
        }

        return false;
    }
}