<?php

namespace Suitquestionanswer\Repositories;

use DB;
use Carbon\Carbon;
use Suitquestionanswer\Models\UserAnswer;
use Suitquestionanswer\Repositories\Contract\UserAnswerRepositoryContract;
use Suitquestionanswer\Repositories\Contract\UserChapterRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserAnswerRepository implements UserAnswerRepositoryContract
{
    use SuitRepositoryTrait;

    protected $userChapterRepo;

    public function __construct(UserAnswer $model, UserChapterRepositoryContract $userChapterRepo) {
        $this->mainModel = $model;
        $this->userChapterRepo = $userChapterRepo;
    }
    
    public function created(UserAnswer $model){
        $this->userChapterRepo->denormalize($model->user_chapter_id);
    }

    public function updated(UserAnswer $model){
        $this->userChapterRepo->denormalize($model->user_chapter_id);
    }

}