<?php

namespace Suitquestionanswer\Config;

class DefaultConfig
{
    public static $data = [
        // 'menu_type' => [ ]
        // 'banner_type' => [ ]
    ];

    public static function getConfig()
    {
        $instancesConfig = [];
        // priority 1 (backward-compatibility with old-version)
        if (class_exists('\\App\\Config\\SuitquestionanswerConfig')) {
            if (is_array( \App\Config\SuitquestionanswerConfig::$data )) {
                $instancesConfig = \App\Config\SuitquestionanswerConfig::$data;
            }
        }
        // priority 2
        $suitsiteConfig = config('suitquestionanswer');
        // priority 3 (default value) : self::$data
        return array_merge( self::$data, $suitsiteConfig, $instancesConfig );
    }
}
