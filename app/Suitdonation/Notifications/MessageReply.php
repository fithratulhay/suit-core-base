<?php

namespace Suitdonation\Notifications;

use Suitcore\Notifications\BaseNotification;
use Suitcore\Notifications\Channels\DatabaseChannel;
use NotificationChannels\OneSignal\OneSignalChannel;

class MessageReply extends BaseNotification
{
    protected $view = 'emails.users.reply-message';
    protected $programMessage = null;

    public function __construct($link = null, $programMessage = null)
    {
        parent::__construct($link);
        
        if ($programMessage != null) {
            $this->programMessage = $programMessage;
            $this->message = trans(config('suitdonation.notification_messages.messagereply'));
        }
    }

    public function setDataView($notifiable)
    {
        $this->dataView = [
            'name' => $this->programMessage ? ($this->programMessage->user ? $this->programMessage->user->name : 'Anonim') : 'Anonim',
            'programMessage' => $this->programMessage
        ];
    }

    // only notif by email
    public function via($notifiable)
    {
        $channel = ['mail']; // default
        if (!empty(trim($this->message))) {
            $channel[] = DatabaseChannel::class; // database
            $channel[] = OneSignalChannel::class; // one-signal webpush / app-push
        }
        return $channel;
    }
}
