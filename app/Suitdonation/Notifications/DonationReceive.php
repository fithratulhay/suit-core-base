<?php

namespace Suitdonation\Notifications;

use Suitcore\Notifications\BaseNotification;
use Suitcore\Notifications\Channels\DatabaseChannel;
use NotificationChannels\OneSignal\OneSignalChannel;
use Suitdonation\Models\ProgramTransaction;

class DonationReceive extends BaseNotification
{
    protected $view = 'emails.users.donation-receive';
    protected $transaction = null;

    public function __construct($link = null, $transaction = null)
    {
        parent::__construct($link);
        
        if ($transaction != null) {
            $this->transaction = $transaction;
            $this->message = str_replace(':transaction_number', $this->transaction->transaction_number, trans(config('suitdonation.notification_messages.donationreceive')));
        }
        if ($this->transaction == null) {
            // prototype
            $this->transaction = ProgramTransaction::whereHas('program')->first();
        }
    }

    public function setDataView($notifiable)
    {
        $this->dataView = [
            'name' => $this->transaction ? ($this->transaction->custom_user_name ? $this->transaction->custom_user_name : $this->transaction->user->name) : 'Anonim',
            'transaction' => $this->transaction
        ];
    }

    // only notif by email
    public function via($notifiable)
    {
        $channel = ['mail']; // default
        if (!empty(trim($this->message))) {
            $channel[] = DatabaseChannel::class; // database
            $channel[] = OneSignalChannel::class; // one-signal webpush / app-push
        }
        return $channel;
    }
}
