<?php

namespace Suitdonation\Notifications;

use Suitcore\Notifications\BaseNotification;
use Suitcore\Notifications\Channels\DatabaseChannel;
use NotificationChannels\OneSignal\OneSignalChannel;

class NewProgramUpdate extends BaseNotification
{
    protected $view = 'emails.users.update';

    public function __construct($link = null)
    {
        parent::__construct($link);
        $this->message = trans(config('suitdonation.notification_messages.newprogramupdate'));
    }

    public function setDataView($notifiable)
    {
        $this->dataView = [
            'name' => 'Admin'
        ];
    }

    // only notif by email
    public function via($notifiable)
    {
        $channel = ['mail']; // default
        if (!empty(trim($this->message))) {
            $channel[] = DatabaseChannel::class; // database
            $channel[] = OneSignalChannel::class; // one-signal webpush / app-push
        }
        return $channel;
    }
}
