<?php

namespace Suitdonation\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Suitcore\Providers\LoadRepositoryConfigTrait;

class SuitdonationServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any suitcore services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any suitcore services.
     *
     * @return void
     */
    public function register()
    {
        // Register Suitdonation Service Provider
        $repositories = config('suitdonation.repositories');
        $this->loadFromConfig($repositories, false, config('suitdonation.custom_base_model'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitdonation.repositories');
        return array_keys($repositories);
    }
}
