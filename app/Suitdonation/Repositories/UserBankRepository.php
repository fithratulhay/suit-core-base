<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\UserBankRepositoryContract;
use Suitdonation\Models\UserBank;

class UserBankRepository implements UserBankRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(UserBank $model)
    {
        $this->mainModel = $model;
    }
}
