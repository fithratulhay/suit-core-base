<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\CityRepositoryContract;
use Suitdonation\Models\City;

class CityRepository implements CityRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(City $model)
    {
        $this->mainModel = $model;
    }

    public function getByProvince($provinceId, $param = [])
    {
        $object = null;
        $paginate = true;
        $perPage = 10;
        if (!empty($param['paginate'])) {
            $paginate = $param['paginate'];
        }

        if (!empty($param['perPage'])) {
            $perPage = $param['perPage'];
        }
        

        if ($provinceId) {
            $param['_province_id'] = $provinceId;
            $param['paginate'] = $paginate;
            $param['perPage'] = $perPage;
            $param['orderBy'] = 'name';
            $param['orderType'] = 'asc';
            $object = $this->getByParameter($param, [], true);
        }

        return $object;
    }

    public function orderedAll($orderType, array $with = array())
    {
        $entity = $this->make($with);

        return $entity->orderBy($orderType)->get();
    }

    public function isAvailable($cityItem)
    {
        $city = $this->find($cityItem);
        return $city;
    }
}
