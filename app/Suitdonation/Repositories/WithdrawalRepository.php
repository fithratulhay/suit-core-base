<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\Withdrawal;
use Suitdonation\Repositories\Contract\WithdrawalRepositoryContract;

class WithdrawalRepository implements WithdrawalRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Withdrawal $model)
    {
        $this->mainModel = $model;
    }

    public function getByProgram($programId, $param = [])
    {
        $object = null;
        if ($programId) {
            $param['_program_id'] = $programId;
            if (empty($param['paginate'])) {
                $param['paginate'] = true;
            }
            
            if (empty($param['perPage'])) {
                $param['perPage'] = 10;
            }

            $setting['optional_dependency'] = ['userBank', 'userBank.bank', 'program'];
            $object = $this->getByParameter($param, $setting, true);
        }

        return $object;
    }
}
