<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\Report;
use Suitdonation\Repositories\Contract\ReportRepositoryContract;

class ReportRepository implements ReportRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Report $model)
    {
        $this->mainModel = $model;
    }
}
