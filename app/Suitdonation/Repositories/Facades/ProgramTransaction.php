<?php

namespace Suitdonation\Repositories\Facades;

class ProgramTransaction extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract';
    }
}
