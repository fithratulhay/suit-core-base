<?php

namespace Suitdonation\Repositories\Facades;

class ProgramCategory extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProgramCategoryRepositoryContract';
    }
}
