<?php

namespace Suitdonation\Repositories\Facades;

class Profile extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProfileRepositoryContract';
    }
}
