<?php

namespace Suitdonation\Repositories\Facades;

class ProgramUpdate extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProgramUpdateRepositoryContract';
    }
}
