<?php

namespace Suitdonation\Repositories\Facades;

class Program extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProgramRepositoryContract';
    }
}
