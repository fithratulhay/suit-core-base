<?php

namespace Suitdonation\Repositories\Facades;

class Country extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\CountryRepositoryContract';
    }
}
