<?php

namespace Suitdonation\Repositories\Facades;

class UserBank extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\UserBankRepositoryContract';
    }
}
