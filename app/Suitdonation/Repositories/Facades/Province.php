<?php

namespace Suitdonation\Repositories\Facades;

class Province extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProvinceRepositoryContract';
    }
}
