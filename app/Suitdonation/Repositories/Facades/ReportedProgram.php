<?php

namespace Suitdonation\Repositories\Facades;

class ReportedProgram extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ReportedProgramRepositoryContract';
    }
}
