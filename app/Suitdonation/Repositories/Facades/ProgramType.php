<?php

namespace Suitdonation\Repositories\Facades;

class ProgramType extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProgramTypeRepositoryContract';
    }
}
