<?php

namespace Suitdonation\Repositories\Facades;

class Message extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\MessageRepositoryContract';
    }
}
