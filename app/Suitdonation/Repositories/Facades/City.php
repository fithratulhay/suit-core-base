<?php

namespace Suitdonation\Repositories\Facades;

class City extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\CityRepositoryContract';
    }
}
