<?php

namespace Suitdonation\Repositories\Facades;

class Withdrawal extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\WithdrawalRepositoryContract';
    }
}
