<?php

namespace Suitdonation\Repositories\Facades;

class Report extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ReportRepositoryContract';
    }
}
