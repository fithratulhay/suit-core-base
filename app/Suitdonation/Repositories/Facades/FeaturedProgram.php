<?php

namespace Suitdonation\Repositories\Facades;

class FeaturedProgram extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\FeaturedProgramRepositoryContract';
    }
}
