<?php

namespace Suitdonation\Repositories\Facades;

class ProgramGallery extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\ProgramGalleryRepositoryContract';
    }
}
