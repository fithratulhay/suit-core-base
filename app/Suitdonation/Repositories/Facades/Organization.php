<?php

namespace Suitdonation\Repositories\Facades;

class Organization extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitdonation\Repositories\Contract\OrganizationRepositoryContract';
    }
}
