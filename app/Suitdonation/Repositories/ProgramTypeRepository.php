<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\ProgramType;
use Suitdonation\Repositories\Contract\ProgramTypeRepositoryContract;

class ProgramTypeRepository implements ProgramTypeRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(ProgramType $model)
    {
        $this->mainModel = $model;
    }

    public function getByCode($typeCode)
    {
        $type = $this->mainModel->where('code', $typeCode)->first();
        return $type;
    }
}
