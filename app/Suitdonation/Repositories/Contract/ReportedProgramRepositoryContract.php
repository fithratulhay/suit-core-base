<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ReportedProgramRepositoryContract extends SuitRepositoryContract
{
}
