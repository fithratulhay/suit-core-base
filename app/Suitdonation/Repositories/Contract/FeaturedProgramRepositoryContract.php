<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface FeaturedProgramRepositoryContract extends SuitRepositoryContract
{
    public function getFeaturedProgram();
}
