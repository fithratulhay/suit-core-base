<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProvinceRepositoryContract extends SuitRepositoryContract
{
    public function getByCountry($countryId, $param = []);
    public function getOptionList();
}
