<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface OrganizationRepositoryContract extends SuitRepositoryContract
{
}
