<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProgramTypeRepositoryContract extends SuitRepositoryContract
{
    public function getByCode($typeCode);
}
