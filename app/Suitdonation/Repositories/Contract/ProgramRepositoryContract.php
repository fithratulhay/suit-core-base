<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProgramRepositoryContract extends SuitRepositoryContract
{
    public function getByType($typeCode);

    public function getByCategory($slug, $param = [], $perPage = 10, $paginate = true);

    public function getByLocation($ident, $identId = null);

    public function getRelatedCategory($slug, $perPage = 10, $paginate = true, $programId);

    public function getByEndOrder($slug, $perPage = 3, $paginate = false);

    public function getByPreference($preferences);

    public function getByCategoryAndCity($categorySlug, $cityId, $order = null);

    public function getByOrder($type = null);

    public function getFundraiser($programId, $perPage = 10, $paginate = true);

    public function updateByUser($id, $param, SuitModel &$object = null, $user, $enabledRoles = ['member', 'fundraiser']);

    public function createByUser($param, SuitModel &$object = null, $user, $enabledRoles = ['member', 'fundraiser']);
}
