<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProgramTransactionRepositoryContract extends SuitRepositoryContract
{
    public function getByStatus($statusName);

    public function getByRange($start, $end);

    public function getByUser($userId);

    public function getByProgram($slug);

    public function getDonatur($programId, $perPage = 10, $paginate = true, $param = []);

    public function finishUpdateTransaction($param, $uuid);

    public function getCount($group = null, $condition = []);
}
