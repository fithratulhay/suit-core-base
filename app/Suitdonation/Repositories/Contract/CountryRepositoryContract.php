<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface CountryRepositoryContract extends SuitRepositoryContract
{
}
