<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserRateRepositoryContract extends SuitRepositoryContract
{
	public function rateUser($fromUserId, $toUserId, $rate);

    public function checkRated($param);
}
