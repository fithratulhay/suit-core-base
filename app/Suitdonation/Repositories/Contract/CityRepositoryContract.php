<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface CityRepositoryContract extends SuitRepositoryContract
{
    public function getByProvince($provinceId, $param = []);
    public function orderedAll($orderType, array $with = array());
    public function isAvailable($cityItem);
}
