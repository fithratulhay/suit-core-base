<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProgramCategoryRepositoryContract extends SuitRepositoryContract
{
    public function getBySlug($slug);

    public function getByType($typeCode);

    public function getCategoryPreference($preferenceData);

    public function getMenuCategory();
}
