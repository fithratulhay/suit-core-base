<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface WithdrawalRepositoryContract extends SuitRepositoryContract
{
    public function getByProgram($programId, $param = []);
}
