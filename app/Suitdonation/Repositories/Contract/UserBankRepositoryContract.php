<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserBankRepositoryContract extends SuitRepositoryContract
{
}
