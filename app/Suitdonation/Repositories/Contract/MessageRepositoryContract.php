<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface MessageRepositoryContract extends SuitRepositoryContract
{
    public function sendReply($id, $param, $sender);
}
