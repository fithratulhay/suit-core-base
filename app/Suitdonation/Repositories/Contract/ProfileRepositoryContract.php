<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProfileRepositoryContract extends SuitRepositoryContract
{
}
