<?php

namespace Suitdonation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ProgramUpdateRepositoryContract extends SuitRepositoryContract
{
    public function getByProgram($programId, $page = 1);
}
