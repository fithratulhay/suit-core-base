<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\ProgramGalleryRepositoryContract;
use Suitdonation\Models\ProgramGallery;

class ProgramGalleryRepository implements ProgramGalleryRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(ProgramGallery $model)
    {
        $this->mainModel = $model;
    }
}
