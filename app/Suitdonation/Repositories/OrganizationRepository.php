<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\OrganizationRepositoryContract;
use Suitdonation\Models\Organization;

class OrganizationRepository implements OrganizationRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Organization $model)
    {
        $this->mainModel = $model;
    }
}
