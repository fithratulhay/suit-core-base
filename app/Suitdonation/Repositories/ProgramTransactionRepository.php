<?php

namespace Suitdonation\Repositories;

use Cache;
use DB;
use Exception;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\ProgramRepositoryContract;
use Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract;
use Suitdonation\Models\ProgramTransaction;
use Suitpay\Models\PaymentMethod;
use Suitpay\Repositories\Contract\PaymentMethodRepositoryContract;

class ProgramTransactionRepository implements ProgramTransactionRepositoryContract
{
    use SuitRepositoryTrait;
    protected $methodRepo;
    protected $programRepo;

    function __construct(ProgramTransaction $model, PaymentMethodRepositoryContract $methodRepo, ProgramRepositoryContract $_programRepo)
    {
        $this->mainModel = $model;
        $this->methodRepo = $methodRepo;
        $this->programRepo = $_programRepo;
    }

    public function getStatisticCountForStatus($status, $createdFrom, $createdUntil) {
        return ProgramTransaction::where('transaction_date', '>=', $createdFrom)->where('transaction_date', '<=', $createdUntil)->where('status', $status)->sum('amount');
    }

    public function getStatisticDataForStatus($status, $createdFrom, $createdUntil) {
        try {
            $transactionData = ProgramTransaction::where('transaction_date', '>=', $createdFrom)->where('transaction_date', '<=', $createdUntil)->where('status', $status)->selectRaw("DATE_FORMAT(transaction_date,'%Y-%m-%d') AS date, sum(program_transactions.amount) AS totalAmount")->orderBy('transaction_date', 'asc')->groupBy(DB::raw("DATE_FORMAT(transaction_date,'%Y-%m-%d')"))->get();
            $completeSeries = [];
            for($date = Carbon::createFromFormat('Y-m-d H:i:s', $createdFrom); $date->lte(Carbon::createFromFormat('Y-m-d H:i:s', $createdUntil)); $date->addDay()) {
                $completeSeries[$date->format('Y-m-d')] = 0;
            }
            foreach ($transactionData as $key => $object) {
                $completeSeries[$object->date] = intval($object->totalAmount);
            }
            return $completeSeries;
        } catch (Exception $e) { }
        return [];
    }

    public function changeStatusTo(ProgramTransaction $model, $newStatus) {
        if ($model &&
            $model->id &&
            $model->status != $newStatus &&
            in_array($newStatus, array_keys($this->mainModel->getStatusOptions()))) {
            $currentLock = Cache::get('transaction-lock-'.$model->transaction_number.'-'.$newStatus);
            if ($currentLock) return;
            Cache::forever('transaction-lock-'.$model->transaction_number.'-'.$newStatus, true);
            if ($model->status == $this->mainModel::STATUS_CREATED &&
                $newStatus == $this->mainModel::STATUS_CHECKOUT) {
                $model->update([
                    'status' => $this->mainModel::STATUS_CHECKOUT
                ]);
                $link = url('/donate/checkout/' . $model->uuid);
                $user = $model->user;
                if ($user) {
                    $user->notify('\Suitdonation\Notifications\DonationNotifier', $link, $model);
                }
            } elseif (in_array($model->status, [$this->mainModel::STATUS_CHECKOUT, $this->mainModel::STATUS_PENDING])) {
                $model->status = $newStatus;
                if ($newStatus == $this->mainModel::STATUS_PAID) {
                    $model->payment_date = Carbon::now();

                    if ($program = $model->program) {
                        $count = $program->transaction_count + 1;
                        $collect = $program->collected + $model->amount;
                        $avg = (int) ($collect / $count);

                        $program->rawIncrement('transaction_count'); // mutex
                        $program->rawIncrement('collected', $model->amount); // mutex

                        $this->programRepo->update($program->id, [
                            // 'collected' => $collect,
                            // 'transaction_count' => $count,
                            'transaction_avg' => $avg
                        ]);

                        $user = $program->user;
                        if ($user) {
                            $link = url('/dashboard/program/' . $program->slug);
                            $user->notify('\Suitdonation\Notifications\DonationReceive', $link, $model);
                        }
                    }

                    $user = $model->user;
                    $link = url('/dashboard/transaction');
                    if ($user) {
                        $user->notify('\Suitdonation\Notifications\DonationPaid', $link, $model);
                    }
                    // other impact of paid transaction ...
                } elseif ($newStatus == $this->mainModel::STATUS_PENDING) {
                    // other impact of pending transaction ...
                } elseif ($newStatus == $this->mainModel::STATUS_UNPAID) {
                    // other impact of unpaid transaction ...
                }
                $model->save();
            } elseif (in_array($model->status, [$this->mainModel::STATUS_PAID, $this->mainModel::STATUS_UNPAID]) &&
                $newStatus == $this->mainModel::STATUS_CHECKOUT) {
                $oldStatus = $model->status;
                $model->status = $newStatus;
                if ($oldStatus == $this->mainModel::STATUS_PAID) {
                    // revert back
                    $model->payment_date = null;

                    if ($program = $model->program) {
                        $count = $program->transaction_count - 1;
                        $collect = $program->collected - $model->amount;
                        $avg = (int) ($collect / $count);

                        $program->rawDecrement('transaction_count'); // mutex
                        $program->rawDecrement('collected', $model->amount); // mutex

                        $this->programRepo->update($program->id, [
                            'transaction_avg' => $avg
                        ]);
                    }

                    $link = url('/donate/checkout/' . $model->uuid);
                    $user = $model->user;
                    if ($user) {
                        $user->notify('\Suitdonation\Notifications\DonationNotifier', $link, $model);
                    }
                    // other impact of reinit paid transaction ...
                } elseif ($oldStatus == $this->mainModel::STATUS_UNPAID) {
                    // other impact of reinit unpaid transaction ...
                }
                $model->save();
            }
            Cache::forget('transaction-lock-'.$model->transaction_number.'-'.$newStatus);
        }
    }

    public function getByStatus($statusName)
    {
        $object = null;
        $param = [];
        if ($statusName) {
            $param['status'] = $statusName;
        }

        $param['paginate'] = true;
        $param['perPage'] = 10;
        $object = $this->getByParameter($param, [], true);
        return $object;
    }

    public function getByRange($start, $end)
    {
        $transactions = [];
        $baseModel = $this->mainModel;
        if ($start && $end) {
            $transactions = Cache::rememberForever('transaction_by_range_' . $start . $end, function() use ($baseModel, $statusName) {
                return $baseModel->beetween($start, $end)->get();
            });
        } else {
            $transactions = Cache::rememberForever('transaction_by_range_all', function() use ($baseModel) {
                return $baseModel->get();
            });
        }

        return $transactions;
    }

    public function getByUser($userId, $status = null)
    {
        $object = null;
        $param = [];
        if ($userId) {
            $param['_user_id'] = $userId;
        }

        if ($status != null) {
            $param['status'] = $status;
        }

        $param['paginate'] = true;
        $param['perPage'] = 10;
        $object = $this->getByParameter($param, [], true);
        return $object;
    }

    public function getByProgram($programId)
    {
        $object = null;
        $param = [];
        if ($programId) {
            $param['_program_id'] = $programId;
            $param['paginate'] = true;
            $param['perPage'] = 10;
            $object = $this->getByParameter($param, [], true);
        }

        return $object;
    }

    public function getDonatur($programId, $perPage = 10, $paginate = true, $param = [])
    {
        $object = null;
        $setting['optional_dependency'] = 'user';
        if ($programId) {
            $param['_program_id'] = $programId;
            $param['status'] = $this->mainModel::STATUS_PAID;
            $param['paginate'] = $paginate;
            $param['perPage'] = $perPage;
            $object = $this->getByParameter($param, $setting, true);
        }

        return $object;
    }

    public function finishUpdateTransaction($param, $uuid)
    {
        $methodId = $param['payment_method_id'];
        $method = $this->methodRepo->find($methodId);
        $transaction = null;
        $date = Carbon::now();
        $month = Carbon::now()->startOfMonth();
        $transactionExist = $this->getBy('uuid', $uuid)->first();

        $param['unique_transaction_code'] = 000;
        $amount = $transactionExist->amount;
        if ($method && $method->type == PaymentMethod::TRANSFER) {
            $programModel = $this->programRepo->getMainModel();
            if ($transactionExist->program && $transactionExist->program->round_type == $programModel::ROUND_UP) {
                $amount = ($amount + 1000) - ($amount % 1000);
            } elseif ($transactionExist->program && $transactionExist->program->round_type == $programModel::ROUND_DOWN) {
                $amount = $amount - ($amount % 1000);
            }

            $status = true;
            if (!$transactionExist->unique_transaction_code) {
                $nbTry = 0;
                do {
                    $transCode = rand(1, 999);
                    $newAmount = $amount + $transCode;
                    $trans = $this->check($month, $newAmount);
                    if (!$trans) {
                        $status = false;
                        $param['unique_transaction_code'] = $transCode;
                    }
                    $nbTry++;
                } while ($status == true && $nbTry < 1000);
                if ($nbTry >= 1000) {
                    $param['unique_transaction_code'] = ($transactionExist->id % 998);
                    if ($param['unique_transaction_code'] == 0) $param['unique_transaction_code'] = 999;
                }
            } else {
                $param['unique_transaction_code'] = $transactionExist->unique_transaction_code;
            }
        }

        $param['amount'] = $amount + $param['unique_transaction_code'];
        $param['transaction_date'] = Carbon::now();
        $param['expiry_date'] = $date->addHours(settings('max_hour'), 5*24);

        if (count($transactionExist) > 0) {
            $transaction = $this->update($transactionExist->id, $param, $transactionExist);
            $this->changeStatusTo($transaction, $this->mainModel::STATUS_CHECKOUT);
        }

        return $transaction;
    }

    protected function check($date, $newAmount)
    {
        $transDate = $this->mainModel->where('transaction_date', '>=', $date)
                ->where('amount', $newAmount)->first();

        return $transDate;
    }

    public function getCount($group = null, $condition = []) {

        $count = 0;
        if ($group == null && empty($condition)) {
            $count = $this->mainModel->count();
        } elseif (!$group == null && empty($condition)) {
            $count = $this->mainModel->groupBy($group)->count();
        }  elseif ($group == null && !empty($condition)) {
            $count = $this->mainModel->where($condition)->count();
        }  else {
            $count = $this->mainModel->where($condition)->groupBy($group)->count();
        }

        return $count;
    }

    public function getTotalDonation() {
        return (($sum = $this->mainModel->where('status', ProgramTransaction::STATUS_PAID)->sum('amount')) ? $sum : 0);
    }

    public function getNumberOfDonatur() {
        return (($count = $this->mainModel->selectRaw('COUNT(DISTINCT user_id) AS nbDonatur')->where('status', ProgramTransaction::STATUS_PAID)->pluck('nbDonatur')->first()) ? $count : 0);
    }
}
