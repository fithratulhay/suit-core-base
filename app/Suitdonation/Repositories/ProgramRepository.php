<?php

namespace Suitdonation\Repositories;

use DB;
use Exception;
use Carbon\Carbon;
use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\City;
use Suitdonation\Models\Program;
use Suitdonation\Repositories\Contract\FeaturedProgramRepositoryContract;
use Suitdonation\Repositories\Contract\CityRepositoryContract;
use Suitdonation\Repositories\Contract\ProgramCategoryRepositoryContract;
use Suitdonation\Repositories\Contract\ProgramRepositoryContract;
use Suitdonation\Repositories\Contract\ProgramTypeRepositoryContract;
use Upload;

class ProgramRepository implements ProgramRepositoryContract
{
    use SuitRepositoryTrait;

    protected $_programTypeRepo;
    protected $_programCategoryRepo;
    protected $_featuredRepo;
    protected $_cityRepo;

    public function __construct(Program $model,
        ProgramTypeRepositoryContract $_programTypeRepo,
        ProgramCategoryRepositoryContract $_programCategoryRepo,
        FeaturedProgramRepositoryContract $_featuredRepo,
        CityRepositoryContract $_cityRepo)
    {
        $this->mainModel = $model;
        $this->_programTypeRepo = $_programTypeRepo;
        $this->_programCategoryRepo = $_programCategoryRepo;
        $this->_featuredRepo = $_featuredRepo;
        $this->_cityRepo = $_cityRepo;
    }

    public function getStatisticCountForStatus($status, $createdFrom, $createdUntil) {
        return Program::where('created_at', '>=', $createdFrom)->where('created_at', '<=', $createdUntil)->where('status', $status)->count('id');
    }

    public function getStatisticDataForStatus($status, $createdFrom, $createdUntil) {
        try {
            $programData = Program::where('created_at', '>=', $createdFrom)->where('created_at', '<=', $createdUntil)->where('status', $status)->selectRaw("DATE_FORMAT(created_at,'%Y-%m-%d') AS date, count(programs.id) AS nbObj")->orderBy('created_at', 'asc')->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->get();
            $completeSeries = [];
            for($date = Carbon::createFromFormat('Y-m-d H:i:s', $createdFrom); $date->lte(Carbon::createFromFormat('Y-m-d H:i:s', $createdUntil)); $date->addDay()) {
                $completeSeries[$date->format('Y-m-d')] = 0;
            }
            foreach ($programData as $key => $object) {
                $completeSeries[$object->date] = intval($object->nbObj);
            }
            return $completeSeries;
        } catch (Exception $e) { }
        return [];
    }

    public function getByType($typeCode)
    {
        $type = $this->_programTypeRepo->getByCode($typeCode);

        $object = null;
        $param = [];
        if ($type) {
            $param['_type_id'] = $type->id;
        }

        $setting['optional_dependency'] = ['galleries', 'user', 'category'];
        $param['paginate'] = true;
        $param['perPage'] = 10;

        $param['status'] = Program::STATUS_PUBLISHED;
        $object = $this->getByParameter($param, $setting, true);
        return $object;
    }

    public function getByCategory($slug, $param = [], $perPage = 10, $paginate = true)
    {
        $object = null;
        $listId = [];
        $category = $this->_programCategoryRepo->getBySlug($slug);
        if ($category) {
            $listId = [$category->id];
            foreach ($category->childs as $child) {
                $listId[] = $child->id;
            }

            if ($category) {
                // $param = [];
                $setting['optional_dependency'] = ['galleries', 'user', 'category', 'city'];
                $param['_category_id'] = implode(',', $listId);
                if (empty($param['paginate'])) {
                    $param['paginate'] = $paginate;
                }
                if (empty($param['perPage'])) {
                    $param['perPage'] = $perPage;
                }
                // $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update
                // $param['_min_end_date'] = $now;

                $param['status'] = Program::STATUS_PUBLISHED;
                $object = $this->getByParameter($param, $setting, true);
            }
        }

        return $object;
    }

    public function getRelatedCategory($slug, $perPage = 10, $paginate = true, $programId)
    {
        $category = $this->_programCategoryRepo->getBySlug($slug);
        $object = null;
        if ($category) {
            $param = [];
            $setting['optional_dependency'] = ['galleries', 'user', 'user.profile', 'category', 'coverPicture'];
            $setting['extended_raw_condition'] = 'id != ' . $programId;
            $param['_category_id'] = $category->id;
            $param['paginate'] = $paginate;
            $param['perPage'] = $perPage;
            $param['status'] = Program::STATUS_PUBLISHED;
            $param['orderBy'] = 'collected';
            $param['orderType'] = 'desc';
            $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update
            $param['_min_end_date'] = $now;

            $object = $this->getByParameter($param, $setting, true);
        }

        return $object;
    }

    public function getByLocation($ident, $identId = null)
    {
        $object = null;
        $param = [];
        if ($identId && !empty($ident)) {
            if ($ident == 'country') {
                $param['_country_id'] = $identId;
            } elseif ($ident == 'province') {
                $param['_province_id'] = $identId;
            } elseif ($ident == 'city') {
                $param['_city_id'] = $identId;
            }
        }

        $setting['optional_dependency'] = ['galleries', 'user', 'category'];
        $param['paginate'] = true;
        $param['perPage'] = 10;
        $param['status'] = Program::STATUS_PUBLISHED;
        $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update
        $param['_min_end_date'] = $now;

        $object = $this->getByParameter($param, $setting, true);
        return $object;
    }

    public function getFundraiser($programId, $perPage = -1, $paginate = false)
    {
        $object = null;
        $setting['optional_dependency'] = ['galleries', 'user', 'category', 'city', 'transactions'];
        if ($programId) {
            $param = [];
            $param['_parent_id'] = $programId;
            $param['paginate'] = $paginate;
            $param['perPage'] = $perPage;
            $param['status'] = Program::STATUS_PUBLISHED;
            $object = $this->getByParameter($param, $setting, true);
        }

        return $object;
    }

    public function getByEndOrder($slug, $perPage = 3, $paginate = false, $param = [])
    {
        $category = $this->_programCategoryRepo->getBySlug($slug);
        $listId = [$category->id];
        foreach ($category->childs as $child) {
            $listId[] = $child->id;
        }

        $object = null;
        if ($category) {
            $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update

            $setting['optional_dependency'] = ['galleries', 'user', 'user.profile', 'category', 'city'];
            $param['_category_id'] = implode(',', $listId);
            $param['orderBy'] = 'end_date';
            $param['orderType'] = 'asc';
            if (empty($param['paginate'])) {
                $param['paginate'] = $paginate;
            }
            
            if (empty($param['perPage'])) {
                $param['perPage'] = $perPage;
            }
            $param['status'] = Program::STATUS_PUBLISHED;
            $param['_min_end_date'] = $now;

            $object = $this->getByParameter($param, $setting, true);
        }

        return $object;
    }

    public function getByPreference($preferences, $param = [])
    {
        $object = null;
        if ($preferences || !empty($preferences)) {
            $param['_category_id'] = $preferences;
        }

        $setting['optional_dependency'] = ['galleries', 'user', 'category', 'city'];
        $param['status'] = Program::STATUS_PUBLISHED;
        if (empty($param['paginate'])) {
            $param['paginate'] = true;
        }
        
        if (empty($param['perPage'])) {
            $param['perPage'] = 10;
        }
        $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update
        $param['_min_end_date'] = $now;
        $object = $this->getByParameter($param, $setting, true);

        return $object;
    }

    protected function saveModelLocation($object, $locations)
    {
        $modelLocation = $object->locations->toArray();
        $keyIds = [];
        foreach ($modelLocation as $keyId) {
          $keyIds[] = $keyId['id'];
        }

        $parentId = $object->getKey();
        $cityIds = [];
        // $this->_cityRepo = new CityRepository(new City);
        foreach ($locations as $cityItem) {
            $isAvailable = $this->_cityRepo->isAvailable($cityItem);
            if (!$isAvailable) {
                $cityParam['name'] = $cityItem;
                $city = $this->_cityRepo->create($cityParam);
                if ($city) {
                    $cityIds[] = $city->id;
                    $cityItem = $city->id;
                } else {
                    continue;
                }
            }

            $cityIds[] = $cityItem;

            $isExist = $object->checkLocationExist($cityItem);
// dd($object->locations);
            if ($isExist == 0) {
                $object->locations()->attach($cityItem);
            }
        }

        $param['city_id'] = $cityIds[0];
        $this->update($object->id, $param, $object);

        if (count($keyIds) > 0) {
            foreach ($keyIds as $keyId) {
                if (!in_array($keyId, $cityIds)) {
                    $object->detachLocation($keyId);
                }
            }
        }
    }

    public function getByCategoryAndCity($categorySlug, $cityId, $order = null, $param = [])
    {
        unset($param['category']);
        unset($param['city']);
        if ($categorySlug || $categorySlug != null || !empty($categorySlug)) {
            $category = $this->_programCategoryRepo->getBySlug($categorySlug);
            if ($category) {
                $listIds = [$category->id];

                foreach ($category->childs as $child) {
                    $listIds[] = $child->id;
                }
                $param['_category_id'] = implode(',', $listIds);
            }
        }

        if ($cityId || $cityId != null || !empty($cityId)) {
            $param['_city_id'] = $cityId;
        }

        if ($order != null) {
            if ($order == 'newest') {
                $param['orderBy'] = 'published_date';
                $param['orderType'] = 'desc';
            } elseif ($order == 'oldest') {
                $param['orderBy'] = 'published_date';
                $param['orderType'] = 'asc';
            } elseif ($order == 'popular') {
                $param['orderBy'] = 'transaction_count';
                $param['orderType'] = 'desc';
            } elseif ($order == 'relevance') {
                $param['orderBy'] = 'relevance';
                $param['orderType'] = 'desc';
            } elseif ($order == 'default') {
                $param['orderBy'] = 'position_order';
                $param['orderType'] = 'desc';
            }
        }

        $setting['optional_dependency'] = ['galleries', 'user', 'user.profile', 'category', 'city'];
        $param['status'] = Program::STATUS_PUBLISHED;
        $param['paginate'] = true;

        $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update
        $param['_min_end_date'] = $now;

        if (empty($param['perPage'])) {
            $param['perPage'] = 9;
        }
        
        $object = $this->getByParameter($param, $setting, true);

        return $object;
    }

    public function getByOrder($order = null)
    {
        if ($order != null) {
            if ($order == 'newest') {
                $param['orderBy'] = 'published_date';
                $param['orderType'] = 'desc';
            } elseif ($order == 'oldest') {
                $param['orderBy'] = 'published_date';
                $param['orderType'] = 'asc';
            }  elseif ($order == 'popular') {
                $param['orderBy'] = 'transaction_count';
                $param['orderType'] = 'desc';
            }
        }

        $setting['optional_dependency'] = ['galleries', 'user', 'category', 'city'];
        $param['status'] = Program::STATUS_PUBLISHED;
        $param['paginate'] = true;
        $param['perPage'] = 10;
        $object = $this->getByParameter($param, $setting, true);

        return $object;
    }

    public function update($id, $param, SuitModel &$object = null) {
        /** @var SuitModel $object */
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));

        if ($object == null) return false;
        $deletedFieldFiles = [];
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
            if (starts_with($key, 'delete_file__') && $val == 'on') {
                $deletedFieldFiles[] = str_replace('delete_file__', '', $key);
            }
        }
        if (!$object->isValid('update', null, $param)) {
            return false;
        }

        if (array_key_exists('status', $param) && $object->status != $param['status']) {
            $featuredProgram = $this->_featuredRepo->getBy('program_id', $object->id)->first();
            if ($featuredProgram) {
                $featuredParam['status'] = $param['status'];
                $this->_featuredRepo->update($featuredProgram->id, $featuredParam, $featuredProgram);
            }
        }

        $object->fill($param);
        Upload::cleanUploaded($object, $deletedFieldFiles, false);
        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::select('*')->instance($object->nodeFullClassName, $object->id)->locale($locale)->field($attrName)->first();
                                if ($currentTranslation) {
                                    $currentTranslation->value = $param[$paramKey];
                                    $currentTranslation->save();
                                } else {
                                    $currentTranslation = SuitTranslation::create([
                                            'class' => $object->nodeFullClassName,
                                            'identifier' => $object->id,
                                            'attribute' => $attrName,
                                            'locale' => $normalizedLang,
                                            'value' => $param[$paramKey]
                                        ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (isset($param['_tags'])) {
            $this->saveModelTag($object, $param['_tags']);
        }

        if (isset($param['_locations'])) {
            $this->saveModelLocation($object, $param['_locations']);
        }

        return $object;
    }

    public function updateByUser($id, $param, SuitModel &$object = null, $user, $enabledRoles = []) {
        // if (!in_array($user->role, $enabledRoles)) {
        //     return null;
        // }

        // if ($user->role == 'member') {
        //     $param['parent_id'] = null;
        // } elseif ($user->role == 'fundraiser' && empty($param['parent_id'])) {
        //     return null;
        // }

        /** @var SuitModel $object */
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object->user_id != $user->id) {
            return null;
        }

        if ($object == null) return false;
        $deletedFieldFiles = [];
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
            if (starts_with($key, 'delete_file__') && $val == 'on') {
                $deletedFieldFiles[] = str_replace('delete_file__', '', $key);
            }
        }
        if (!$object->isValid('update', null, $param)) {
            return false;
        }

        if (array_key_exists('status', $param) && $object->status != $param['status']) {
            $featuredProgram = $this->_featuredRepo->getBy('program_id', $object->id)->first();
            if ($featuredProgram) {
                $featuredParam['status'] = $param['status'];
                $this->_featuredRepo->update($featuredProgram->id, $featuredParam, $featuredProgram);
            }
        }

        $object->fill($param);
        Upload::cleanUploaded($object, $deletedFieldFiles, false);
        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::select('*')->instance($object->nodeFullClassName, $object->id)->locale($locale)->field($attrName)->first();
                                if ($currentTranslation) {
                                    $currentTranslation->value = $param[$paramKey];
                                    $currentTranslation->save();
                                } else {
                                    $currentTranslation = SuitTranslation::create([
                                            'class' => $object->nodeFullClassName,
                                            'identifier' => $object->id,
                                            'attribute' => $attrName,
                                            'locale' => $normalizedLang,
                                            'value' => $param[$paramKey]
                                        ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (isset($param['_tags'])) {
            $this->saveModelTag($object, $param['_tags']);
        }

        if (isset($param['_locations'])) {
            $this->saveModelLocation($object, $param['_locations']);
        }

        return $object;
    }

    public function createByUser($param, SuitModel &$object = null, $user, $enabledRoles = []) {
        // if (!in_array($user->role, $enabledRoles)) {
        //     return null;
        // }

        // if ($user->role == 'member') {
        //     $param['parent_id'] = null;
        // } elseif ($user->role == 'fundraiser' && empty($param['parent_id'])) {
        //     return null;
        // }

        $param['user_id'] = $user->id;
        $param['status'] = $this->mainModel::STATUS_DRAFT;
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->getNew() : new SuitModel);
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
        }
        if (!$object->isValid('create', null, $param)) {
            return false;
        }
        $object->fill($param);

        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $baseAttr = $object->getAttributeSettings();
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::create([
                                    'class' => $object->nodeFullClassName,
                                    'identifier' => $object->id,
                                    'attribute' => $attrName,
                                    'locale' => $normalizedLang,
                                    'value' => $param[$paramKey]
                                ]);
                            }
                        }
                    }
                }
            }
        }

        if (isset($param['_tags'])) {
            $this->saveModelTag($object, $param['_tags']);
        }

        if (isset($param['_locations'])) {
            $this->saveModelLocation($object, $param['_locations']);
        }

        return $object;
    }

    public function getDetailBySlug($slug, $additionalWith = [])
    {
        // $slug = explode('-', $slug);
        // $programId = $slug[0];
        // unset($slug[0]);
        // $slug = implode('-', $slug);
        $with = ['galleries', 'category', 'user', 'user.profile', 'city', 'tags'];
        $with = array_merge($with, $additionalWith);
        $program = $this->getBy__cached('slug', $slug, $with)->first();

        return $program;
    }

    public function getNumberOfProgram() {
        return (($count = $this->mainModel->where('status', $this->mainModel::STATUS_PUBLISHED)->count()) ? $count : 0);
    }
}
