<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\FeaturedProgramRepositoryContract;
use Suitdonation\Models\FeaturedProgram;

class FeaturedProgramRepository implements FeaturedProgramRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(FeaturedProgram $model)
    {
        $this->mainModel = $model;
    }

    public function getFeaturedProgram()
    {
        $object = null;
        $param = [];

        $setting['optional_dependency'] = ['program', 'program.user', 'program.user.profile', 'program.coverPicture', 'program.category', 'program.city'];
        $param['paginate'] = false;
        $param['orderBy'] = 'position_order';
        $param['orderType'] = 'desc';
        $param['perPage'] = static::FETCH_ALL;
        $param['status'] = FeaturedProgram::STATUS_PUBLISHED;
        $object = $this->getByParameter($param, $setting, true);

        return $object;
    }
}
