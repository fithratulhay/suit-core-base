<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\ReportedProgram;
use Suitdonation\Repositories\Contract\ReportedProgramRepositoryContract;

class ReportedProgramRepository implements ReportedProgramRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(ReportedProgram $model)
    {
        $this->mainModel = $model;
    }
}
