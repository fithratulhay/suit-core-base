<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\CountryRepositoryContract;
use Suitdonation\Models\Country;

class CountryRepository implements CountryRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Country $model)
    {
        $this->mainModel = $model;
    }
}
