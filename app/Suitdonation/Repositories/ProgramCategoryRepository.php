<?php

namespace Suitdonation\Repositories;

use Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\ProgramCategory;
use Suitdonation\Repositories\Contract\ProgramCategoryRepositoryContract;
use Suitdonation\Repositories\Contract\ProgramTypeRepositoryContract;

class ProgramCategoryRepository implements ProgramCategoryRepositoryContract
{
    use SuitRepositoryTrait;

    protected $_programTypeRepo;

    function __construct(ProgramCategory $model, ProgramTypeRepositoryContract $_programTypeRepo)
    {
        $this->mainModel = $model;
        $this->_programTypeRepo = $_programTypeRepo;
    }

    public function getBySlug($slug)
    {
        $category = $this->getBy__cached('slug', $slug, ['childs'])->first();
        return $category;
    }

    public function getByType($typeCode)
    {
        $type = $this->_programTypeRepo->getByCode($typeCode);

        $object = null;
        $param = [];
        if ($type) {
            $param['_type_id'] = $type->id;
        }

        $param['paginate'] = true;
        $param['perPage'] = 10;
        $object = $this->getByParameter($param, [], true);
        return $object;
    }

    public function getCategoryPreference($preferenceData)
    {
        $object = $this->mainModel->whereIn('id', $preferenceData)->get();

        return $object;
    }

    public function getMenuCategory($orderBy = null, $orderType = null)
    {
        if ($orderBy == null) {
            $orderBy = 'id';
        }

        if ($orderType == null) {
            $orderType = 'asc';
        }
        $object = null;
        $param = [];
        // $campaign = $this->getBy('slug', 'campaign')->first();
        // $sedekah = $this->getBy('slug', 'sedekah', ['childs'])->first();
        // $sedekahData = [];
        // if ($campaign && $sedekah) {
        //     $setting['extended_raw_condition'] = 'id != ' . $campaign->id . ' AND id != ' . $sedekah->id;
        //     $sedekahChilds = $sedekah->childs()->orderBy($orderBy, $orderType)->with('childs')->get();

        //     foreach ($sedekahChilds as $child) {
        //         $item = $child->toArray();
        //         $item['childs'] = $child->childs()->orderBy($orderBy, $orderType)->get()->toArray();
        //         $sedekahData[] = $item;
        //     }
        // }

        $campaign = $this->getBy('slug', 'campaign')->first();
        if ($campaign) {
            $setting['extended_raw_condition'] = 'id != ' . $campaign->id;
        }
        
        $setting['optional_dependency'] = ['childs', 'childs.childs', 'programs', 'programs.galleries'];
        $param['_isnull_parent_id'] = true;
        $param['orderBy'] = $orderBy;
        $param['orderType'] = $orderType;
        $param['paginate'] = false;
        $param['perPage'] = 10;
        $objects = $this->getByParameter($param, $setting, true);
        $parent = [];

        foreach ($objects as $object) {
            if ($object->slug != 'campaign' && $object->slug != 'sedekah' && $object->parent_id == null) {
                $parent[] = $object->toArray();
            }

            if ($object->slug == 'sedekah' && $object->parent_id == null) {
                foreach ($object->childs()->orderBy($orderBy, $orderType)->get() as $child) {
                    $item = $child->toArray();
                    $item['childs'] = $child->childs()->orderBy($orderBy, $orderType)->get()->toArray();
                    
                    $parent[] = $item;
                }
            }
        }
        
        return $parent;
    }

}
