<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\ProfileRepositoryContract;
use Suitdonation\Models\Profile;

class ProfileRepository implements ProfileRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Profile $model)
    {
        $this->mainModel = $model;
    }
}
