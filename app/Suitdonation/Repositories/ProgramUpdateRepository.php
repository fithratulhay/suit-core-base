<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\ProgramUpdate;
use Suitdonation\Repositories\Contract\ProgramUpdateRepositoryContract;

class ProgramUpdateRepository implements ProgramUpdateRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(ProgramUpdate $model)
    {
        $this->mainModel = $model;
    }

    public function getByProgram($programId, $page = 1)
    {
        $object = null;
        $param = [];
        if ($programId) {
            $param['_program_id'] = $programId;
        }

        $param['status'] = $this->mainModel::STATUS_PUBLISHED;
        $param['paginate'] = true;
        $param['perPage'] = 10;
        $param['page'] = $page;
        $setting['optional_dependency'] = ['user'];
        $object = $this->getByParameter($param, $setting, true);
        return $object;
    }
}
