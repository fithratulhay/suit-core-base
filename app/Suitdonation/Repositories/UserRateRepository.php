<?php

namespace Suitdonation\Repositories;

use Suitcore\Models\SuitUser;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitdonation\Repositories\Contract\ProfileRepositoryContract;
use Suitdonation\Repositories\Contract\UserRateRepositoryContract;
use Suitdonation\Models\UserRate;

class UserRateRepository implements UserRateRepositoryContract
{
    use SuitRepositoryTrait;

    protected $userRepo;
    protected $userProfileRepo;

    public function __construct(UserRate $model, SuitUserRepositoryContract $userRepo, ProfileRepositoryContract $userProfileRepo)
    {
        $this->mainModel = $model;
        $this->userRepo = $userRepo;
        $this->userProfileRepo = $userProfileRepo;
    }

    public function rateUser($fromUserId, $toUserId, $rate) {
        // use profile user
        $fromUser = ($fromUserId instanceof SuitUser && $fromUserId->id ? $fromUserId : $this->userRepo->getBy__cached('id', $fromUserId)->first());
        $toUser = ($toUserId instanceof SuitUser && $toUserId->id ? $toUserId : $this->userRepo->getBy__cached('id', $toUserId)->first());

        if ($fromUser && $toUser && $fromUser->id != $toUser->id) {
            $rateParam['rater_id']  = $fromUser->id;
            $rateParam['rated_id']  = $toUser->id;
            $rateParam['paginate']  = false;
            $rateParam['perPage']   = 1;

            $rateExist = $this->getByParameter($rateParam, [], true);

            $param = [
                'rate' => $rate
            ];
            $param = array_merge($rateParam, $param);

            $rateUser = null;
            if ($rateExist) {
                $rateUser = $this->update($rateExist->id, $param, $rateExist);
            } else {
                $rateUser = $this->create($param);
            }

            $profile = $this->userProfileRepo->getBy__cached('user_id', $toUser->id)->first();
            $updatedRatedUserProfile = null;
            if ($rateUser) {
                if ($profile) {
                    $relatedRating = $this->getBy('rated_id', $toUser->id);
                    $profileParam['rate'] = ($relatedRating->sum('rate') / $relatedRating->count());
                    $updatedRatedUserProfile = $this->userProfileRepo->update($profile->id, $profileParam, $profile);
                } else {
                    $profileParam['user_id'] = $toUser->id;
                    $profileParam['rate'] = $rate;
                    $updatedRatedUserProfile = $this->userProfileRepo->create($profileParam);
                }

                return $updatedRatedUserProfile;
            }
        }

        return false;
    }

    public function checkRated($param)
    {
        if (empty($param['rater_id']) || empty($param['rated_id'])) {
            return null;
        }

        $rate = $this->mainModel->where('rater_id', $param['rater_id'])->where('rated_id', $param['rated_id'])->first();

        return $rate;
    }
}
