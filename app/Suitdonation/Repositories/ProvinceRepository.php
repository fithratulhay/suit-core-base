<?php

namespace Suitdonation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Models\Province;
use Suitdonation\Repositories\Contract\ProvinceRepositoryContract;

class ProvinceRepository implements ProvinceRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Province $model)
    {
        $this->mainModel = $model;
    }

    public function getByCountry($countryId, $param = [])
    {
        $object = null;
        $paginate = true;
        $perPage = 10;
        $orderBy = 'name';
        $orderType = 'asc';
        if (!empty($param['paginate'])) {
            $paginate = $param['paginate'];
        }

        if (!empty($param['perPage'])) {
            $perPage = $param['perPage'];
        }

        if (!empty($param['orderBy'])) {
            $orderBy = $param['orderBy'];
        }

        if (!empty($param['orderType'])) {
            $orderType = $param['orderType'];
        }

        if ($countryId) {
            $param['_country_id'] = $countryId;
            $param['paginate'] = $paginate;
            $param['perPage'] = $perPage;
            $param['orderBy'] = $orderBy;
            $param['orderType'] = $orderType;
            $object = $this->getByParameter($param, [], true);
        }

        return $object;
    }

    public function getOptionList()
    {
        $objects = $this->mainModel->orderBy('name', 'asc')->get();
        $data = [
            'firstId' => ($objects->first() ? $objects->first()->id : 0),
            'list' => $objects->pluck('name', 'id')
        ];
        return $data;
    }
}
