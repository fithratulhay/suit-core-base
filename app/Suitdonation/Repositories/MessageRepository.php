<?php

namespace Suitdonation\Repositories;

use Exception;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitdonation\Repositories\Contract\MessageRepositoryContract;
use Suitdonation\Models\Message;

class MessageRepository implements MessageRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Message $model, SuitUserRepositoryContract $userRepo)
    {
        $this->mainModel = $model;
    }

    public function sendReply($id, $param, $sender, &$repliedMessage = null)
    {
        $message = $this->find($id);
        
        $newMessage = null;
        $param['user_id'] = $message->optional_user_id ? $message->optional_user_id : 0;
        $param['program_id'] = $message->program->id;
        $param['optional_user_id'] = $sender ? $sender->id : null;
        $param['sender_email'] = $sender ? $sender->email : '';
        $param['sender_fullname'] = $sender ? $sender->name : '';
        $param['subject'] = 'Message Reply';
        $newMessage = $this->create($param);

        $status = false;
        if ($newMessage) {
            if ($repliedMessage) {
                $repliedMessage = $newMessage;
            }
            try {
                $link = asset('/dashboard/message');
                $message->notify('\Suitdonation\Notifications\MessageReply', $link, $newMessage);
                $status = true;
            } catch (Exception $e) {}
        }

        return $status;
    }
}
