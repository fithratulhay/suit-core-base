<?php

namespace Suitdonation\Models;

use App\Models\User;
use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;

class Report extends SuitModel
{
    public $table = 'reports';

    public $files = [
        'file' => 'contentfile'
    ];

    public $fillable = [
        'program_id',
        'user_id',
        'title',
        'file',
        'report_date'
    ];

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getTitleAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getLabel() {
        return "Reports";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id'),
                "options_url" => route(config('suitdonation.data_options_route_name.program')),
                "filterable" => true
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "options" => [], // (new User)->all()->pluck('name','id'),
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Title",
                "translation" => false
            ],
            "file" => [
                "type" => self::TYPE_FILE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Report File"
            ],
            "report_date" => [
                "type" => self::TYPE_DATE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Report Date",
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
