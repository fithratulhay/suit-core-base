<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitModel;
use Suitdonation\Models\City;
use Suitdonation\Models\Country;
use Suitdonation\Models\Program;

class Province extends SuitModel
{
    public $table = 'provinces';

    public $fillable = [
        'country_id',
        'name'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'city_id');
    }

    public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function programs()
    {
        return $this->hasMany(Program::class, 'province_id');
    }

    public function getLabel() {
        return "Province";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name",
                "translation" => false
            ],
            "country_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "country",
                "label" => "Country",
                "options" => (new Country)->all()->pluck('name','id'),
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
