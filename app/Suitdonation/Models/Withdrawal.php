<?php

namespace Suitdonation\Models;

use App\Models\User;
use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;
use Suitdonation\Models\UserBank;

class Withdrawal extends SuitModel
{
    const STATUS_REQUESTED = 'requested';
    const STATUS_ONPROCESS = 'onprocess';
    const STATUS_TRANSFERRED = 'transferred';
    const STATUS_REJECTED = 'rejected';
    const STATUS_CANCEL = 'cancel';

    public $table = 'withdrawals';

    public $fillable = [
        'program_id',
        'user_id',
        'user_bank_id',
        'transaction_number',
        'amount',
        'status',
        'transfer_date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userBank()
    {
        return $this->belongsTo(UserBank::class, 'user_bank_id');
    }

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function scopeUser($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeProgram($query, $programId)
    {
        return $query->where('program_id', $programId);
    }

    public function scopeStatus($query, $statusName)
    {
        return $query->where('status', $statusName);
    }

    public function getStatusAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getTotalWidrawalOfProgram($programId)
    {
        return $this->where('program_id', $programId)->sum('amount');
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_REQUESTED => ucwords(strtolower(self::STATUS_REQUESTED)),
            self::STATUS_ONPROCESS => ucwords(strtolower(self::STATUS_ONPROCESS)),
            self::STATUS_TRANSFERRED => ucwords(strtolower(self::STATUS_TRANSFERRED)),
            self::STATUS_REJECTED => ucwords(strtolower(self::STATUS_REJECTED)),
            self::STATUS_CANCEL => ucwords(strtolower(self::STATUS_CANCEL))
        ];
    }

    public function getLabel() {
        return "Withdrawal";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id'),
                "options_url" => route(config('suitdonation.data_options_route_name.program')),
                "filterable" => true
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "user",
                "label" => "User",
                "options" => [], // (new User)->all()->pluck('name','id'),
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "user_bank_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "userBank",
                "label" => "User Bank",
                "options" => null
            ],
            "transaction_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Transaction Number"
            ],
            "amount" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Amount"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "options" => $this->getStatusOptions(),
                "label" => "Status",
                "filterable" => true
            ],
            "transfer_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Transfer Date"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
