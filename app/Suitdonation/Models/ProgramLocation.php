<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitModel;
use Suitdonation\Models\City;
use Suitdonation\Models\Program;

class ProgramLocation extends SuitModel
{
    public $table = 'program_locations';

    public $fillable = [
        'program_id',
        'city_id'
    ];

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function getOptions()
    {
        return City::pluck('name', 'id');
    }

    public function getLabel() {
        return "Location";
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.program') ? route(config('suitdonation.data_options_route_name.program')) : '')
            ],
            "city_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "city",
                "label" => "City",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.city') ? route(config('suitdonation.data_options_route_name.city')) : ''),
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
