<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;
use Suitcore\Notifications\Notifiable;
use Suitdonation\Models\Program;

class Message extends SuitModel
{
    use Notifiable;

    public $table = 'messages';

    public $fillable = [
        'user_id',
        'program_id',
        'optional_user_id',
        'sender_fullname',
        'sender_email',
        'subject',
        'is_read',
        'content'
    ];

    public function getEmailAttribute() {
        return $this->sender_email;
    }

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function optionalUser()
    {
        return $this->belongsTo(SuitUser::class, 'optional_user_id');
    }

    public function getLabel() {
        return "Program Message";
    }

    public function getFormattedValue() {
        return $this->subject;
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id'),
                "options_url" => route(config('suitdonation.data_options_route_name.program')),
                "filterable" => true
            ],
            "optional_user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "optionalUser",
                "label" => "Sender",
                "options" => [], // (new User)->all()->pluck('name','id'),
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "sender_fullname" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Sender Name"
            ],
            "sender_email" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Sender Email"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => "user",
                "label" => "Receiver",
                "options" => [], // (new User)->all()->pluck('name','id'),
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "subject" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Subject"
            ],
            "content" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Content",
                "translation" => false
            ],
            "is_read" => [
                "type" => self::TYPE_BOOLEAN,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Is Read"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
