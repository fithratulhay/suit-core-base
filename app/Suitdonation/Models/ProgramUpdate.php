<?php

namespace Suitdonation\Models;

use App\Models\User;
use Carbon\Carbon;
use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;
use Suitdonation\Models\ProgramCategory;

class ProgramUpdate extends SuitModel
{
    const STATUS_DRAFT_USER = 'draft-user';
    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';

    public $table = 'program_updates';

    public $fillable = [
        'user_id',
        'program_id',
        'program_category_id',
        'title',
        'content',
        'status'
    ];

    public $rules = [
        'title' => 'required',
        'content' => 'required',
        'status' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function category()
    {
        return $this->belongsTo(ProgramCategory::class, 'program_category_id');
    }

    public function scopeStatus($query, $statusName)
    {
        return $query->where('status', $statusName);
    }

    public function scopeProgram($query, $programId)
    {
        return $query->where('program_id', $programId);
    }

    public function getUpdateDate()
    {
        $date = $this->created_at;
        $now = Carbon::now();
        $date = Carbon::parse($date);
        $date = $date->diffInDays($now);

        return $date;
    }

    public function getTitleAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED))
        ];
    }

    public function getLabel() {
        return "Program Update";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "options" => [], // (new User)->all()->pluck('name','id'),
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id'),
                "options_url" => route(config('suitdonation.data_options_route_name.program')),
                "filterable" => true
            ],
            "program_category_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "category",
                "label" => "Program Category",
                "options" => (new ProgramCategory)->all()->pluck('title','id'),
                // "options_url" => route(config('suitdonation.data_options_route_name.programcategory')),
                "filterable" => true
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => false
            ],
            "content" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Content",
                "translation" => false
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "options" => $this->getStatusOptions(),
                "label" => "Status",
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
