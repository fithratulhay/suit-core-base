<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;

class ProgramGallery extends SuitModel
{
    const TYPE_GALLERY_IMAGE = 'image';
    const TYPE_GALLERY_VIDEO = 'video';

    public $table = 'program_galleries';

    public $imageAttributes = [
        'image' => 'programgalleries'
    ];

    public $files = [
        'image' => 'programgalleries'
    ];

    protected $extendedThumbnailStyle = [
        'grid_thumb' => '328x185'
    ];

    public $fillable = [
        'program_id',
        'type',
        'position_order',
        'optional_video_url',
        'image'
    ];

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function getLabel() {
        return "Gallery";
    }

    public function getTypeOptions()
    {
        return [
            self::TYPE_GALLERY_IMAGE => ucwords(strtolower(self::TYPE_GALLERY_IMAGE)),
            self::TYPE_GALLERY_VIDEO => ucwords(strtolower(self::TYPE_GALLERY_VIDEO))
        ];
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.program') ? route(config('suitdonation.data_options_route_name.program')) : '')
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "options" => $this->getTypeOptions(),
                "label" => "Type",
                "filterable" => true
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Order"
            ],
            "optional_video_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Video Url"
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Images"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
