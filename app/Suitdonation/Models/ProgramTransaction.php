<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitUser;
use Carbon\Carbon;
use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;
use Suitdonation\Models\ProgramCategory;
use Suitpay\Models\Contracts\PayableContract;
use Suitpay\Models\PaymentMethod;

class ProgramTransaction extends SuitModel implements PayableContract
{
    const STATUS_CREATED = 'created';
    const STATUS_CHECKOUT = 'checkout';
    const STATUS_PAID = 'paid';
    const STATUS_UNPAID = 'unpaid';
    const STATUS_CANCEL = 'cancel';
    const STATUS_PENDING = 'pending';

    public $table = 'program_transactions';

    public $fillable = [
        'uuid',
        'program_category_id',
        'program_id',
        'payment_method_id',
        'user_id',
        'custom_user_name',
        'transaction_number',
        'amount',
        'transaction_date',
        'expiry_date',
        'verified_date',
        'payment_date',
        'status',
        'message',
        'transfer_file',
        'unique_transaction_code',
        'uuid',
        'base_amount',
        'quantity',
        'is_sms_sent',
        'manual_transfer_scrap_id'
    ];

    // Datetime casting
    protected $dates = ['transaction_date', 'expiry_date', 'verified_date', 'payment_date'];

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function category()
    {
        return $this->belongsTo(ProgramCategory::class, 'program_category_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }

    public function scopeStatus($query, $statusName)
    {
        return $query->where('status', $statusName);
    }

    public function scopeBeetween($query, $start, $end)
    {
        return $query->where('transaction_date', '>=', $start)
            ->where('transaction_date', '<=', $end);
    }

    public function scopeExpired()
    {

    }

    public function scopeUser($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function getAmountAttribute($value)
    {
        $amount = $value % 1000;
        if ($amount > 0) {
            return $value;
        }

        return $value + $this->unique_transaction_code;
    }

    public static function getStatusOptions()
    {
        return [
            self::STATUS_CREATED => ucwords(strtolower(self::STATUS_CREATED)),
            self::STATUS_CHECKOUT => ucwords(strtolower(self::STATUS_CHECKOUT)),
            self::STATUS_PAID => ucwords(strtolower(self::STATUS_PAID)),
            self::STATUS_UNPAID => ucwords(strtolower(self::STATUS_UNPAID)),
            self::STATUS_CANCEL => ucwords(strtolower(self::STATUS_CANCEL))
        ];
    }

    public function getLabel() {
        return "Program Transaction";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_category_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "category",
                "label" => "Program Category",
                "options" => [], // (new ProgramCategory)->all()->pluck('title','id')
                "options_url" => route(config('suitdonation.data_options_route_name.programcategory')),
                "filterable" => true
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id')
                "options_url" => route(config('suitdonation.data_options_route_name.program')),
                "filterable" => true
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "options" => null,
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "transaction_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Transaction Number",
                "translation" => false
            ],
            "amount" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Amount"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "options" => $this->getStatusOptions(),
                "label" => "Status",
                "filterable" => true
            ],
            "transaction_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Transaction Date",
                "filterable" => true
            ],
            "manual_transfer_scrap_id" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "SCRAP ID"
            ],
            "is_sms_sent" => [
                "type" => self::TYPE_BOOLEAN,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "SMS Sent?",
                "filterable" => true
            ],
            "uuid" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "UUID",
                "translation" => false
            ],
            "quantity" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Quantity"
            ],
            "expiry_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Expired Date"
            ],
            "verified_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Verified Date",
                "filterable" => true
            ],
            "base_amount" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Base Amount"
            ],
            "payment_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Payment Date",
                "filterable" => true
            ],
            "payment_method_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "paymentMethod",
                "label" => "Payment Method",
                "options" => [],
                "options_url" => route(config('suitpay.data_options_route_name.paymentmethod')),
                "filterable" => true
            ],
            "message" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Message",
                "translation" => false
            ],
            "transfer_file" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Transfer File"
            ],
            "unique_transaction_code" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Unique Transaction Code",
                "options" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }

    // Begin payable contract implementation
    public function getOrderCode()
    {
        return $this->transaction_number;
    }

    public function getOrderDatetime()
    {
        return $this->transaction_date;
    }

    public function getOrderExpiryInMinutes()
    {
        return 6000;
    }

    public function getTotalPrice()
    {
        return $this->amount;
    }

    public function getRecipientName()
    {
        return $this->user ? $this->user->name : "Unknown";
    }

    public function getRecipientPhone()
    {
        return $this->user && $this->user->phone_number ? $this->user->phone_number : "000000000000";
    }

    public function getRecipientEmail()
    {
        return $this->user ? $this->user->email : "unknown@host.com";
    }

    public function getRecipientStreetName()
    {
        return $this->user && $this->user->profile ? $this->user->profile->address : "Unknown Street";
    }

    public function getRecipientCity()
    {
        return $this->user && $this->user->profile && $this->user->profile->city ? $this->user->profile->city->name : "Unknown City";
    }

    public function getRecipientProvince()
    {
        return  $this->user && $this->user->profile && $this->user->profile->city && $this->user->profile->city->province ? $this->user->profile->city->province->name : "Unknown Province";
    }

    public function getRecipientCountry()
    {
        return 'IDN';
    }

    public function getRecipientZipcode()
    {
        return $this->user && $this->user->profile ? $this->user->profile->postal_code : '00000';
    }

    public function getRecipientBirthdate() {
        return ($this->user && $this->user->birthdate ? $this->user->birthdate->format('Ymd') : Carbon::now()->subYears(17)->format('Ymd'));
    }

    public function getBillingRecipientName() {
        return $this->getRecipientName();
    }

    public function getBillingRecipientPhone() {
        return $this->getRecipientPhone();
    }

    public function getBillingRecipientEmail() {
        return $this->getRecipientEmail();
    }

    public function getBillingRecipientStreetName() {
        return $this->getRecipientStreetName();
    }

    public function getBillingRecipientCity() {
        return $this->getRecipientCity();
    }

    public function getBillingRecipientProvince() {
        return $this->getRecipientProvince();
    }

    public function getBillingRecipientCountry() {
        return $this->getRecipientCountry();
    }

    public function getBillingRecipientZipcode() {
        return $this->getRecipientZipcode();
    }

    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    public function getOrderItems()
    {
        $item = [];
        // product
        $items[] = [
            'id'        => $this->id,
            'price'     => intval($this->amount),
            'quantity'  => 1,
            'name'      => $this->program ?: "Unknown Program"
        ];
        // payment fee
        $paymentFee = 0;
        if ($this->paymentMethod) {
            $paymentMethodRepo = app('Suitpay\Repositories\Contract\PaymentMethodRepositoryContract');
            $paymentFee = $paymentMethodRepo->getTotalPaymentFee($this->paymentMethod->id, $this->amount);
        }
        if ($paymentFee > 0) {
            $items[] = [
                'id'        => $this->getOrderCode() . '-payment-fee',
                'price'     => intval($paymentFee),
                'quantity'  => 1,
                'name'      => 'Convinience Fee'
            ];
        }
        return  $items;
    }

    public function getOrderFullClassName()
    {
        $extendedModelNamespace = 'Suitdonation\Models';
        $extendedModelName = $extendedModelNamespace . '\\' . (new \ReflectionClass($this))->getShortName();
        return $extendedModelName;
    }

    public function getOrderCodeField() {
        return 'transaction_number';
    }

    public function getPaymentMethodObject() {
        return $this->paymentMethod;
    }

    public function getPaymentOption() {
        return "";
    }

    public function successPayment() {
        if ($this->id) {
            app('Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract')->changeStatusTo($this, self::STATUS_PAID);  
        }
    }

    public function pendingPayment() {
        if ($this->id) {
            app('Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract')->changeStatusTo($this, self::STATUS_PENDING);        
        }
    }

    public function failedPayment() {
        if ($this->id) {
            app('Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract')->changeStatusTo($this, self::STATUS_UNPAID);  
        }
    }

    public function refundPayment() {
        // no refund on donation business-process
    }

    public function cancelPayment() {
        // no cancel on donation business-process
    }

    public function reinitPayment() {
        if ($this->id) {
            app('Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract')->changeStatusTo($this, self::STATUS_CHECKOUT); // re-init transaction to checkout 
        }
    }
    // End of payable contract implemenation
}
