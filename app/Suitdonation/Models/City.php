<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;
use Suitdonation\Models\Province;

class City extends SuitModel
{
    public $table = 'cities';

    public $fillable = [
        'province_id',
        'name'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function getNameAttribute($value)
    {
        // return ucwords(strtolower($value));
        return ucwords(strtolower($value));
    }

    public function programs()
    {
        return $this->hasMany(Program::class, 'city_id');
    }

    public function getLabel() {
        return "City";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name",
                "translation" => false
            ],
            "province_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "province",
                "label" => "Province",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.province') ? route(config('suitdonation.data_options_route_name.province')) : ''),
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
