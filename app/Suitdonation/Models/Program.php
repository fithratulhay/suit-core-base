<?php

namespace Suitdonation\Models;

use Cache;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\Extensions\TaggableTrait;
use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitdonation\Models\City;
use Suitdonation\Models\Country;
use Suitdonation\Models\FeaturedProgram;
use Suitdonation\Models\Message;
use Suitdonation\Models\ProgramCategory;
use Suitdonation\Models\ProgramGallery;
use Suitdonation\Models\ProgramLocation;
use Suitdonation\Models\ProgramTransaction;
use Suitdonation\Models\ProgramType;
use Suitdonation\Models\ProgramUpdate;
use Suitdonation\Models\Province;

class Program extends SuitModel
{
    use Sluggable, TaggableTrait;

    const STATUS_DRAFT = 'draft';
    const STATUS_ONREVIEW = 'review';
    const STATUS_PUBLISHED = 'published';
    const STATUS_CLOSED = 'closed';

    const ROUND_UP = 'up';
    const ROUND_DOWN = 'down';

    public $table = 'programs';

    public $fillable = [
        'user_id',
        'program_type_id',
        'program_category_id',
        'parent_id',
        'title',
        'slug',
        'highlight',
        'description',
        'start_date',
        'end_date',
        'city_id',
        'province_id',
        'country_id',
        'optional_location_name',
        'base_amount',
        'target',
        'collected',
        'status',
        'receiver',
        'position_order',
        'is_show_target',
        'transaction_avg',
        'transaction_count',
        'discount_price',
        'published_date',
        'round_type'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function type()
    {
        return $this->belongsTo(ProgramType::class, 'program_type_id');
    }

    public function category()
    {
        return $this->belongsTo(ProgramCategory::class, 'program_category_id');
    }

    public function parent()
    {
        return $this->belongsTo(Program::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(Program::class, 'parent_id');
    }

    public function transactions()
    {
        return $this->hasMany(ProgramTransaction::class, 'program_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function updates()
    {
        return $this->hasMany(ProgramUpdate::class, 'program_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'program_id');
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'program_id');
    }

    public function galleries()
    {
        return $this->hasMany(ProgramGallery::class, 'program_id');
    }

    public function coverPicture()
    {
        if ($this->parent_id) {
            return $this->hasOne(ProgramGallery::class, 'parent_id', 'id')->orderByRaw('(position_order * -1) desc')->orderBy('created_at', 'asc');
        }
        return $this->hasOne(ProgramGallery::class, 'program_id', 'id')->orderByRaw('(position_order * -1) desc')->orderBy('created_at', 'asc');
    }

    public function featuredProgram()
    {
        return $this->hasOne(FeaturedProgram::class, 'program_id');
    }

    public function locations()
    {
        return $this->belongsToMany(City::class, 'program_locations', 'program_id', 'city_id');
    }

    public function scopeParentList($query)
    {
        return $query->where('parent_id', null)->pluck('title', 'id');
    }

    public function scopePublished($query)
    {
        return $query->where('status', static::STATUS_PUBLISHED);
    }

    public function scopeOnReview($query)
    {
        return $query->where('status', static::STATUS_ONREVIEW);
    }

    public function scopeCity($query, $cityId)
    {
        return $query->where('city_id', $cityId);
    }

    public function scopeProvince($query, $provinceId)
    {
        return $query->where('province_id', $provinceId);
    }

    public function scopeType($query, $typeId)
    {
        return $query->where('program_type_id', $typeId);
    }

    public function scopeStatus($query, $statusName)
    {
        return $query->where('status', $statusName);
    }

    public function getTitleAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    // public function getSlugAttribute($value)
    // {
    //     $access = url()->current();
    //     if (str_contains($access, 'paneladmin')) {
    //         return $value;
    //     }

    //     $slug = $this->id . '-' . $value;
    //     return $slug;
    // }

    public function getProgramDuration()
    {
        $now = Carbon::now();
        $end = Carbon::parse($this->end_date);

        $timeRange = $now->diffInDays($end);
        $time = $timeRange . ' Hari';
        if ($timeRange >= 30 && $timeRange / 30 < 12) {
            $time = $now->diffInMonths($end) . ' Bulan';
        } elseif ($timeRange / 30 >= 12) {
            $time = $now->diffInYears($end) . ' Tahun';
        }

        // $time = round($diff / 30);
        return $time;
    }

    public function getProgramDurationDetail()
    {
        $now = Carbon::now();
        $end = Carbon::parse($this->end_date);

        $diff = $now->diff($end)->format('%d D : %h H : %i M');
        return $diff;
    }

    public function getOptionalLocationNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getCollectedPrecentage()
    {
        if ($this->target == 0) {
            return 0;
        }
        $result = $this->collected / $this->target;

        $result = round($result, 2);

        return $result * 100;
    }

    public function getAmountOfWithdrawn()
    {
        return $this->withdrawals()->sum('amount');
    }

    public function getAmountOfUnWithdrawn()
    {
        $withdrawn = $this->getAmountOfWithdrawn();

        return $this->collected - $withdrawn;
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_ONREVIEW => ucwords(strtolower(self::STATUS_ONREVIEW)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_CLOSED => ucwords(strtolower(self::STATUS_CLOSED))
        ];
    }

    public function getRoundTypeOption()
    {
        return [
            self::ROUND_UP => ucwords(strtolower(self::ROUND_UP)),
            self::ROUND_DOWN => ucwords(strtolower(self::ROUND_DOWN))
        ];
    }

    public function detachLocation($key)
    {
        $this->locations()->detach($key);
    }

    public function checkLocationExist($key)
    {
        $city = $this->locations()->where('city_id', $key)->exists();
        return $city;
    }

    public function getLabel() {
        return "Program";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getFormattedValueColumn()
    {
        return ['title'];
    }

    public function getOptions() {
        return self::all();
    }

    public function getShowOptions() {
        return [
            0 => 'Not Show',
            1 => 'Show'
        ];
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "options" => [],
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : ''),
                "filterable" => true
            ],
            "program_type_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => "type",
                "label" => "Type",
                "options" => [],
                "filterable" => true,
                'form_field_relation' => [
                    'top' => '',
                    'bottom' => 'program_category_id'
                ],
                "options_url" => (config('suitdonation.data_options_route_name.programtype') ? route(config('suitdonation.data_options_route_name.programtype')) : '')
            ],
            "program_category_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "category",
                "label" => "Category",
                "options" => [],
                "filterable" => true,
                'form_field_relation' => [
                    'top' => 'program_type_id',
                    'bottom' => ''
                ],
                "options_url" => (config('suitdonation.data_options_route_name.programcategory') ? route(config('suitdonation.data_options_route_name.programcategory')) : '')
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "parent",
                "label" => "Parent Program",
                "options" => [],
                "filterable" => true,
                "options_url" => (config('suitdonation.data_options_route_name.program') ? route(config('suitdonation.data_options_route_name.program')) : '')
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => false
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug"
            ],
            "highlight" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Highlight",
                "translation" => false
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Description",
                "translation" => false
            ],
            "start_date" => [
                "type" => self::TYPE_DATE,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Start At"
            ],
            "end_date" => [
                "type" => self::TYPE_DATE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "End At"
            ],
            "published_date" => [
                "type" => self::TYPE_DATE,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Published Date",
                "filterable" => true
            ],
            "country_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => "country",
                "label" => "Country",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.country') ? route(config('suitdonation.data_options_route_name.country')) : ''),
                "filterable" => false
            ],
            "province_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => "province",
                "label" => "Province",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.province') ? route(config('suitdonation.data_options_route_name.province')) : ''),
                "filterable" => false
            ],
            "city_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => "city",
                "label" => "City",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.city') ? route(config('suitdonation.data_options_route_name.city')) : ''),
                "filterable" => false
            ],
            "optional_location_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Custom Location Name",
                "translation" => false
            ],
            "base_amount" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Base Amount"
            ],
            "target" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Target",
                "default_value" => 0
            ],
            "discount_price" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Discount",
                "default_value" => 0
            ],
            "collected" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Collected"
            ],
            "transaction_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Transaction Count"
            ],
            "transaction_avg" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Transaction Average"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "options" => $this->getStatusOptions(),
                "label" => "Status",
                "filterable" => true
            ],
            "receiver" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Receiver",
                "default_value" => 0
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Order",
                "default_value" => 0
            ],
            "is_show_target" => [
                "type" => self::TYPE_BOOLEAN,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Show Target"
            ],
            "round_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "options" => $this->getRoundTypeOption(),
                "label" => "Transaction Amount Round Type",
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
