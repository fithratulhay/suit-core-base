<?php

namespace Suitdonation\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;
use Suitdonation\Models\ProgramType;
use Suitdonation\Models\ProgramUpdate;

class ProgramCategory extends SuitModel
{
    use Sluggable;

    public $table = 'program_categories';

    public $imageAttributes = [
        'image' => 'contentimages',
        'icon' => 'contenticons'
    ];

    public $files = [
        'image' => 'contentimages',
        'icon' => 'contenticons'
    ];

    public $fillable = [
        'program_type_id',
        'parent_id',
        'title',
        'slug',
        'highlight',
        'image',
        'icon',
        'description',
        'position_order'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function type()
    {
        return $this->belongsTo(ProgramType::class, 'program_type_id');
    }

    public function parent()
    {
        return $this->belongsTo(ProgramCategory::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(ProgramCategory::class, 'parent_id');
    }

    public function programs()
    {
        return $this->hasMany(Program::class, 'program_category_id');
    }

    public function updates()
    {
        return $this->hasMany(ProgramUpdate::class, 'program_category_id');
    }

    public function scopeParentList($query)
    {
        return $query->where('parent_id', null)->pluck('title', 'id');
    }

    public function getTitleAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getLabel() {
        return "Program Category";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getFormattedValueColumn()
    {
        return ['title'];
    }

    public function getOptions() {
        return self::all()->pluck('title', 'id');
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_type_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "type",
                "label" => "Type",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.programtype') ? route(config('suitdonation.data_options_route_name.programtype')) : ''),
                "filterable" => true
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "parent",
                "label" => "Parent",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.programcategory') ? route(config('suitdonation.data_options_route_name.programcategory')) : ''),
                "filterable" => true
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => false
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug"
            ],
            "highlight" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Highlight",
                "translation" => false
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Description",
                "translation" => false
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Cover Images"
            ],
            "icon" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Icon"
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Order"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
