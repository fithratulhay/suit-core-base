<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitModel;
use Suitdonation\Models\Program;

class FeaturedProgram extends SuitModel
{
    const STATUS_DRAFT = 'draft';
    const STATUS_ONREVIEW = 'review';
    const STATUS_PUBLISHED = 'published';
    const STATUS_CLOSED = 'closed';

    public $table = 'featured_programs';

    public $fillable = [
        'program_id',
        'position_order',
        'status'
    ];

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_DRAFT => ucwords(strtolower(self::STATUS_DRAFT)),
            self::STATUS_ONREVIEW => ucwords(strtolower(self::STATUS_ONREVIEW)),
            self::STATUS_PUBLISHED => ucwords(strtolower(self::STATUS_PUBLISHED)),
            self::STATUS_CLOSED => ucwords(strtolower(self::STATUS_CLOSED))
        ];
    }

    public function getLabel() {
        return "Featured Program";
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id')
                "options_url" => route(config('suitdonation.data_options_route_name.program'))
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Position Order",
                "options" => null
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "options" => $this->getStatusOptions(),
                "label" => "Status",
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
