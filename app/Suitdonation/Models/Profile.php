<?php

namespace Suitdonation\Models;

use App\Models\User;
use Suitcore\Models\SuitModel;
use Suitdonation\Models\City;
use Suitdonation\Models\Country;
use Suitdonation\Models\Organization;
use Suitdonation\Models\Province;
use Suitdonation\Models\UserBank;

/*
|--------------------------------------------------------------------------
| contents Table Structure
|--------------------------------------------------------------------------
| * id INT AUTO INCREMENT
| * user_id INT NOT NULL
| * organization_id INT NOT NULL
| * nik
| * id_card_image
| * gender
| * address
| * city_id
| * province_id
| * country_id
| * postal_code
| * rate
| * position
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Profile extends SuitModel
{
    const MALE = 'male';
    const FEMALE = 'female';
    const VERIFY_ACCEPTED = 'accepted';
    const VERIFY_REJECTED = 'reject';
    const VERIFY_PENDING = 'pending';

    public $table = 'profiles';

    protected static $bufferAttributeSettings = null;

    public $imageAttributes = [
        'id_card_image' => 'contentidcardimage',
        'verification_photo' => 'contentverificationphoto',
        'npwp_photo' => 'contentaccountbook'
    ];

    public $files = [
        'id_card_image' => 'contentidcardimage',
        'verification_photo' => 'contentverificationphoto',
        'npwp_photo' => 'contentaccountbook'
    ];

    public $fillable = [
        'user_id',
        'user_bank_id',
        'organization_id',
        'nik',
        'id_card_image',
        'gender',
        'address',
        'city_id',
        'province_id',
        'country_id',
        'postal_code',
        'rate',
        'position',
        'facebook',
        'twitter',
        'instagram',
        'linkedin',
        'verification_photo',
        'npwp_photo',
        'npwp_number',
        'know_from',
        'is_verified',
        'verification_status'
    ];

    public $rules = [];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function bank()
    {
        return $this->belongsTo(UserBank::class, 'user_bank_id');
    }

    public function getGenderOptions()
    {
        return [
            self::MALE => ucwords(strtolower(self::MALE)),
            self::FEMALE => ucwords(strtolower(self::FEMALE))
        ];
    }

    public function getVerifyOptions()
    {
        return [
            self::VERIFY_ACCEPTED => ucwords(strtolower(self::VERIFY_ACCEPTED)),
            self::VERIFY_PENDING => ucwords(strtolower(self::VERIFY_PENDING)),
            self::VERIFY_REJECTED => ucwords(strtolower(self::VERIFY_REJECTED))
        ];
    }

    public function getLabel() {
        return "Profile";
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "options" => [],
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : '')
            ],
            "user_bank_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => "bank",
                "label" => "Bank Account",
                "options" => null
            ],
            "organization_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "organization",
                "label" => "Organization",
                "options" => (new Organization)->all()->pluck('name','id'),
                "filterable" => true
            ],
            "city_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "city",
                "label" => "City",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.city') ? route(config('suitdonation.data_options_route_name.city')) : ''),
                "filterable" => true
            ],
            "province_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "province",
                "label" => "Province",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.province') ? route(config('suitdonation.data_options_route_name.province')) : ''),
                "filterable" => true
            ],
            "country_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "country",
                "label" => "Country",
                "options" => [],
                "options_url" => (config('suitdonation.data_options_route_name.country') ? route(config('suitdonation.data_options_route_name.country')) : ''),
                "filterable" => true
            ],
            "nik" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "NIK"
            ],
            "id_card_image" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "ID CARD"
            ],
            "npwp_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "NPWP Number"
            ],
            "gender" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Gender",
                "options" => $this->getGenderOptions(),
                "filterable" => true
            ],
            "address" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Address",
                "translation" => false
            ],
            "know_from" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Know SH From"
            ],
            "postal_code" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Postal Code",
                "translation" => false
            ],
            "rate" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Rate",
                "translation" => false
            ],
            "position" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Position",
                "translation" => true
            ],
            "facebook" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Facebook",
                "translation" => true
            ],
            "twitter" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Twitter",
                "translation" => true
            ],
            "instagram" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Instagram",
                "translation" => true
            ],
            "linkedin" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Linkedin",
                "translation" => true
            ],
            "verification_photo" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "User Verification Photo"
            ],
            "npwp_photo" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "NPWP Card Photo"
            ],
            "verification_status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Verification Status",
                "options" => $this->getVerifyOptions(),
                "filterable" => true
            ],
            "is_verified" => [
                "type" => self::TYPE_BOOLEAN,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Verify User"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
