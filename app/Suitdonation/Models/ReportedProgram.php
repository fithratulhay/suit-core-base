<?php

namespace Suitdonation\Models;

use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;
use Suitdonation\Models\Program;
use Suitdonation\Models\ProgramCategory;

class ReportedProgram extends SuitModel
{
    public $table = 'reported_programs';

    public $fillable = [
        'program_id',
        'program_category_id',
        'parent_id',
        'user_id',
        'title',
        'message',
    ];

    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id');
    }

    public function category()
    {
        return $this->belongsTo(ProgramCategory::class, 'program_category_id');
    }

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function scopeParentList($query)
    {
        return $query->whereNull('parent_id')->pluck('title', 'id');
    }

    public function getLabel() {
        return "Reported Program";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "program_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => "program",
                "label" => "Program",
                "options" => [], // (new Program)->all()->pluck('title','id'),
                "options_url" => route(config('suitdonation.data_options_route_name.program')),
                "filterable" => true
            ],
            "program_category_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => "category",
                "label" => "Program Category",
                "options" => [], // (new ProgramCategory)->all()->pluck('title','id')
                "options_url" => route(config('suitdonation.data_options_route_name.programcategory')),
                "filterable" => true
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => "parent",
                "label" => "Parent",
                "options" => [], // $this->parentList()
                "options_url" => route(config('suitdonation.data_options_route_name.programcategory')),
                "filterable" => true
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => "user",
                "label" => "Reported By",
                "options" => [], // (new User)->all()->pluck('name','id'),
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => false
            ],
            "message" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Message",
                "translation" => false
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
