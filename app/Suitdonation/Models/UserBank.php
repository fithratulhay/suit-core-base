<?php

namespace Suitdonation\Models;

use App\Models\User;
use Suitpay\Models\Bank;
use Suitcore\Models\SuitModel;

class UserBank extends SuitModel
{
    public $table = 'user_banks';

    public $imageAttributes = [
        'account_book' => 'bankimages'
    ];

    public $files = [
        'account_book' => 'bankimages'
    ];

    public $fillable = [
        'user_id',
        'bank_id',
        'account_name',
        'account_number',
        'is_verified',
        'account_book'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function bank() {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function getFormattedValue() {
        return $this->bank->name;
    }

    public function getLabel() {
        return "User Bank";
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "options" => [], // (new User)->all()->pluck('name', 'id')
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "bank_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "bank",
                "label" => "Bank",
                "options" => (new Bank)->all()->pluck('name','id')
            ],
            "account_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Account Name"
            ],
            "account_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Account Number"
            ],
            "is_verified" => [
                "type" => self::TYPE_BOOLEAN,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Verify Account"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });

        static::updating(function ($model) {
            //if (empty($model->slug)) {
            //    $model->slug = str_slug($model->title);
            //}
        });
    }
}
