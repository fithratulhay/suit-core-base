<?php

namespace Suitevent\Config;

class DefaultConfig
{
    public static $data = [
    ];

    public static function getConfig()
    {
        $instancesConfig = [];
        // priority 1 (backward-compatibility with old-version)
        if (class_exists('\\App\\Config\\SuiteventConfig')) {
            if (is_array( \App\Config\SuiteventConfig::$data )) {
                $instancesConfig = \App\Config\SuiteventConfig::$data;
            }
        }
        // priority 2
        $suiteventConfig = config('suitevent');
        // priority 3 (default value) : self::$data
        return array_merge( self::$data, $suiteventConfig, $instancesConfig );
    }
}
