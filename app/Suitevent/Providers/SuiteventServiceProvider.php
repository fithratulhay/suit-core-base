<?php

namespace Suitevent\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Suitcore\Providers\LoadRepositoryConfigTrait;

class SuiteventServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any suitcore services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any suitcore services.
     *
     * @return void
     */
    public function register()
    {
        // Register Suitevent Service Provider
        $repositories = config('suitevent.repositories');
        $this->loadFromConfig($repositories, false, config('suitevent.custom_base_model'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitevent.repositories');
        return array_keys($repositories);
    }
}
