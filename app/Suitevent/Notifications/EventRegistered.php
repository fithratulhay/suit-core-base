<?php

namespace Suitevent\Notifications;

use Suitcore\Notifications\BaseNotification;

class EventRegistered extends BaseNotification
{
    protected $eventRegistration;
    protected $event;
    protected $view = 'emails.eventregistrations.registered';
    protected $url;
    protected $activationCode;

    public function __construct($id, $url, $activationCode){
        $this->eventRegistration = app(\Suitevent\Repositories\Contract\EventRegistrationRepositoryContract::class)->find($id);
        $this->event = app(\Suitevent\Repositories\Contract\EventRepositoryContract::class)->find($this->eventRegistration->event_id);
        // $this->subject = 'Thanks for your participation on ' . $this->event->name;
        $this->subject = ($this->event ? $this->event->name . ' ' : '') . 'Event Registration Confirmation';
        $this->url = $url;
        $this->activationCode = $activationCode;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function setDataView($notifiable)
    {
        $this->dataView = ['event' => $this->event, 'eventRegistration' => $this->eventRegistration, 'url' => $this->url, 'activationCode' => $this->activationCode];
    }
}
