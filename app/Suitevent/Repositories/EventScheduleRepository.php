<?php

namespace Suitevent\Repositories;

use Cache;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitevent\Models\EventSchedule;
use Suitevent\Repositories\Contract\EventScheduleRepositoryContract;

class EventScheduleRepository implements EventScheduleRepositoryContract
{
    use SuitRepositoryTrait;
    
	public function __construct(EventSchedule $model)
	{
        $this->mainModel = $model;
    }
}