<?php

namespace Suitevent\Repositories;

use Cache;
use QrCode;
use File;
use Response;
use Request;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitcore\Notifications\Notifiable;
use Suitevent\Models\EventRegistration;
use Suitevent\Repositories\Contract\EventRegistrationRepositoryContract;
use Suitcore\Models\SuitModel;

class EventRegistrationRepository implements EventRegistrationRepositoryContract
{
    use SuitRepositoryTrait, Notifiable;
    
	public function __construct(EventRegistration $model)
	{
        $this->mainModel = $model;
    }

    public function created(EventRegistration $model){
        $this->email = $model->email;
        // harusnya pakai uuid
        $activationCode = md5($model->email) . md5($model->event_id);
        $model->activation_code = $activationCode;
        $model->save();
        $url = URL('/validate?key=' . $activationCode);
        if(!File::exists(storage_path('app/public/qrcode'))) {
            File::makeDirectory(storage_path('app/public/qrcode'), 0775, true, true);
        }
        // $qr = QrCode::format('png')->size(400)->generate(URL('attend?key=' . $activationCode), storage_path('app/public/qrcode/' . $activationCode . '.png') );
        $qr = QrCode::format('png')->size(400)->generate($activationCode, storage_path('app/public/qrcode/' . $activationCode . '.png') );
        $this->notify('\Suitevent\Notifications\EventRegistered', $model->id, $url, $activationCode);

        // Notify all admin related to registration
        $userRepo = app('Suitcore\Repositories\Contract\SuitUserRepositoryContract');
        $param['role'] = $userRepo->getMainModel()::ADMIN;
        $param['status'] = $userRepo->getMainModel()::STATUS_ACTIVE;
        $param['paginate'] = false;
        $param['perPage'] = -1;
        $admins = $userRepo->getByParameter($param, [], true);
        $link = route('backend.eventregistration.show', ['id' => $model->id]);
        $message = $model->name . " had registered to event " . ($model->event ? $model->event->name : "unknown") . "!";
        foreach ($admins as $admin) {
            $admin->notify('\Suitevent\Notifications\EventRegistrationAdminNotification', $link, $message);
        }
    }
    
    // 1 email hanya dapat mendaftar 1 event
    public function getEmailUnique($email, $eventId){
        $participant = $this->getMainModel()->where('email', $email)
                                            ->where('event_id', $eventId)
                                            ->where(function ($query) {
                                                    $query->where('status','!=','rejected')
                                                    ->Where('status','!=','canceled');
                                            })
                                            ->get();

        if($participant->isEmpty()){
            return false;
            // return json_encode(array(
            //     'code' => 1,
            //     'message' => 'Email dapat digunakan.',
            // ));
        }
        else{
            return $participant->first();
            // return json_encode(array(
            //     'code' => 0,
            //     'message' => 'Email tidak dapat digunakan.',
            // ));
        }
    }

    public function validate($activationCode){
        $participant = $this->getMainModel()->where('activation_code', $activationCode)->where(function ($query) {
            $query->where('status','!=','rejected')
              ->Where('status','!=','canceled');
        })->get();
        if($participant->isEmpty()){
            return false;
            // Kode Salah
        }
        else{
            // Kode Benar
            $participant = $participant->first();
            // Belum ada expirednya
            // if(Carbon::now() >  Carbon::parse($participant->created_at)->addHour(6)){
            //     // Timeout
            // }
            // else{
            //     // Berhasil
            // }
            $participant->status = 'confirmed';
            $participant->save();
            return true;
        }
    }

    public function attend($activationCode){
        $participant = $this->getMainModel()->where('activation_code', $activationCode)->where(function ($query) {
            $query->where('status','!=','rejected')
              ->Where('status','!=','canceled');
        })->get();
        if($participant->isEmpty()){
            return false;
            // Kode Salah
        }
        else{
            // Kode Benar
            // Cek event, apakah masih berjalan atau tidak
            // if(){
            //
            // }
            $participant = $participant->first();
            $participant->status = 'attended';
            $participant->save();
            return true;
        }
    }
}