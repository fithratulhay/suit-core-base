<?php

namespace Suitevent\Repositories;

use Cache;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitevent\Models\Event;
use Suitevent\Repositories\Contract\EventRepositoryContract;

class EventRepository implements EventRepositoryContract
{
    use SuitRepositoryTrait;
    
	public function __construct(Event $model)
	{
        $this->mainModel = $model;
    }

    public function getRawPublished(){
        return $this->getMainModel()->with(['city'])->where('status', $this->mainModel::STATUS_PUBLISHED)->orderBy('datetime_start', 'asc')->get();
    }

    public function getPublished(){
        if ($possibleEvents = $this->getRawPublished__cached()) {
            $now = Carbon::now();
            return $possibleEvents->filter(function ($event) use ($now) {
                return (($event->datetime_start && $event->datetime_start->greaterThan($now)) || 
                    ($event->datetime_end && $event->datetime_end->greaterThan($now)));
            });
        }
        return collect([]);
    }

    public function getLatestEvent($limit){
        return $this->getMainModel()->where('status', $this->mainModel::STATUS_PUBLISHED)->orderBy('datetime_start', 'asc')->limit($limit)->get();
    }

    public function isEventPublished($id){
        $event = $this->find__cached($id);
        if ($event) {
            return ($event->status == $this->mainModel::STATUS_PUBLISHED);
        }
        return false;
    }
}