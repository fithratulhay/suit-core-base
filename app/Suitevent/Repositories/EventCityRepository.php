<?php

namespace Suitevent\Repositories;

use Cache;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitevent\Models\EventCity;
use Suitevent\Repositories\Contract\EventCityRepositoryContract;

class EventCityRepository implements EventCityRepositoryContract
{
    use SuitRepositoryTrait;
    
	public function __construct(EventCity $model)
	{
        $this->mainModel = $model;
    }
}