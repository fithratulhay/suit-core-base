<?php

namespace Suitevent\Repositories;

use Cache;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitevent\Models\EventSpeaker;
use Suitevent\Repositories\Contract\EventSpeakerRepositoryContract;

class EventSpeakerRepository implements EventSpeakerRepositoryContract
{
    use SuitRepositoryTrait;
    
	public function __construct(EventSpeaker $model)
	{
        $this->mainModel = $model;
    }
}