<?php

namespace Suitevent\Models;

use DB;
use File;
use Cache;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;
use Suitevent\Models\Event;
use Suitevent\Models\EventSpeaker;

class EventSchedule extends SuitModel
{
    public $table = 'event_schedules';
    protected static $bufferAttributeSettings = null;   

    public $fillable = [
        'event_id',
        'title',
        'speaker_id',
        'description',
        'actual_date',
        'time_start',
        'time_end',
    ];

    public $rules = [
        'event_id' => 'required',
        'title' => 'required',
        'speaker_id' => 'required',
    ];

    protected $dates = ['actual_date'];

    public function getLabel() {
        return "Event Schedules";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getFormattedValueColumn() {
        return ["title"];
    }

    public function event(){
        return $this->belongsTo(Event::class);
    }

    public function speaker(){
        return $this->belongsTo(EventSpeaker::class);
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
            ],
            "event_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'event',
                "label" => "Event",
                "options" => (new Event)->all()->pluck('name','id'),
            ],
            "speaker_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'speaker',
                "label" => "Speaker",
                "options" => (new EventSpeaker)->all()->pluck('name','id'),
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Description",
            ],
            "actual_date" => [
                "type" => self::TYPE_DATE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Actual Date"
            ],
            "time_start" => [
                "type" => self::TYPE_TIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Time Start",
            ],
            "time_end" => [
                "type" => self::TYPE_TIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Time End",
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ],
        ];
    }
}
