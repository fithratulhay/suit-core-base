<?php

namespace Suitevent\Models;

use DB;
use File;
use Cache;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;
use Suitevent\Models\EventSchedule;

class EventSpeaker extends SuitModel
{
    public $table = 'speakers';
    protected static $bufferAttributeSettings = null;   
 
    public $fillable = [
        'name',
        'job_position',
        'brand_name',
        'picture',
        'fb_profile_url',
        'google_profile_url',
        'linkedin_profile_url'
    ];

    public $imageAttributes = [
        'picture' => 'speakerpictures'
    ];

    public $files = [
        'picture' => 'speakerpictures'
    ];

    public $rules = [
        'name' => 'required'
    ];

    public function getLabel() {
        return "Speakers";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getFormattedValueColumn() {
        return ["name"];
    }

    public function eventSchedules(){
        return $this->hasMany(EventSchedule::class);
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name",
            ],
            "job_position" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Job Position"
            ],
            "brand_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Brand Name",
            ],
            "picture" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Profile Picture"
            ],
            "fb_profile_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "FB",
            ],
            "google_profile_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Google",
            ],
            "linkedin_profile_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "LinkedIn",
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ],
        ];
    }
}
