<?php

namespace Suitevent\Models;

use DB;
use File;
use Cache;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;
use Suitevent\Models\Event;
use Suitevent\Models\EventRegistration;

class EventCity extends SuitModel
{
    public $table = 'event_cities'; 

    protected static $bufferAttributeSettings = null;  

    public $fillable = [
        'name',
    ];

    public $rules = [
        'name' => 'required',
    ];

    public function getLabel() {
        return "Cities";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getFormattedValueColumn() {
        return ["name"];
    }

    public function events(){
        return $this->hasMany(Event::class);
    }
    
    public function eventRegistrations(){
        return $this->hasMany(EventRegistration::class);
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name",
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ],
        ];
    }
}
