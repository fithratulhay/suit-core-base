<?php

namespace Suitevent\Models;

use DB;
use File;
use Cache;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;
use Suitevent\Models\Event;
use Suitevent\Models\EventCity;

class EventRegistration extends SuitModel
{
    const STATUS_CREATED = 'created';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_REJECTED = 'rejected';
    const STATUS_CANCELED = 'canceled';

    public $table = 'event_registrations';
    protected static $bufferAttributeSettings = null;  

    public $fillable = [
        'event_id',
        'city_id',
        'name',
        'email',
        'phone_number',
        'address',
        'activation_code',
        'status',
        'registered_by_user_id',
    ];

    public $rules = [
        'event_id' => 'required',
        'city_id' => 'required',
        'name' => 'required',
        'email' => 'required',
        'phone_number' => 'required',
        'status' => 'required',
    ];

    public function getLabel() {
        return "Event Registrations";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getFormattedValueColumn() {
        return ["name"];
    }

    public function getStatusOptions(){
        return [
            self::STATUS_CREATED => str_text_beautifier(self::STATUS_CREATED),
            self::STATUS_CONFIRMED => str_text_beautifier(self::STATUS_CONFIRMED),
            self::STATUS_REJECTED => str_text_beautifier(self::STATUS_REJECTED),
            self::STATUS_CANCELED => str_text_beautifier(self::STATUS_CANCELED),
        ];
    }

    public function event(){
        return $this->belongsTo(Event::class);
    }

    public function city(){
        return $this->belongsTo(EventCity::class);
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
            ],
            "event_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'event',
                "label" => "Event",
                "options" => (new Event)->all()->pluck('name', 'id'),
                "filterable" => true
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name",
            ],
            "email" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Email",
            ],
            "phone_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Phone Number",
            ],
            "city_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'city',
                "label" => "City",
                "options" => (new EventCity)->all()->pluck('name','id'),
                "filterable" => true,
            ],
            "address" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Address",
            ],
            "activation_code" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Activation Code",
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                'options' => $this->getStatusOptions(),
                'filterable' => false,
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Date Joined",
                'filterable' => true,
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Last Edited"
            ],
        ];
    }
}
