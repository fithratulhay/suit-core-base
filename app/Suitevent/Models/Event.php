<?php

namespace Suitevent\Models;

use DB;
use File;
use Cache;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;
use Suitevent\Models\EventSchedule;
use Suitevent\Models\EventRegistration;
use Suitevent\Models\EventCity;

class Event extends SuitModel
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAFT = 'draft';

    public $table = 'events';    
    protected static $bufferAttributeSettings = null;   

    public $fillable = [
        'city_id',
        'name',
        'datetime_start',
        'datetime_end',
        'venue_detail',
        'venue_name',
        'venue_latitude',
        'venue_longitude',
        'summary',
        'picture',
        'status',
    ];

    public $imageAttributes = [
        'picture' => 'eventpictures'
    ];

    public $files = [
        'picture' => 'eventpictures'
    ];

    public $rules = [
        'name' => 'required',
        'status' => 'required',
    ];
    
    protected $dates = ['datetime_start', 'datetime_end'];

    public function getFullNameAttribute() {
        $rangesDate = [];
        if ($this->datetime_start) $rangesDate[] = $this->datetime_start->format('d F Y');
        if ($this->datetime_end) $rangesDate[] = $this->datetime_end->format('d F Y');

        return ($this->city ? $this->city : 'Unknown City') . ', ' . implode(' - ', $rangesDate);
    }

    public function getLabel() {
        return "Events";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getFormattedValueColumn() {
        return ["name"];
    }

    public function getStatusOptions(){
        return [
            self::STATUS_PUBLISHED => str_text_beautifier(self::STATUS_PUBLISHED),
            self::STATUS_DRAFT => str_text_beautifier(self::STATUS_DRAFT),
        ];
    }

    public function eventSchedules(){
        return $this->hasMany(EventSchedule::class);
    }

    public function eventRegistration(){
        return $this->hasMany(EventRegistration::class);
    }

    public function city(){
        return $this->belongsTo(EventCity::class);
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name",
            ],
            "datetime_start" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Start Date",
                // "elmt_group" => [
                //     "name" => "event_datetime",
                //     "type" => self::GROUPTYPE_DATETIMERANGE,
                //     "role" => self::GROUPROLE_RANGE_START,
                // ]
            ],
            "datetime_end" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "End Date",
                // "elmt_group" => [
                //     "name" => "event_datetime",
                //     "type" => self::GROUPTYPE_DATETIMERANGE,
                //     "role" => self::GROUPROLE_RANGE_END,
                // ]
            ],
            "city_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'city',
                "label" => "City",
                "options" => (new EventCity)->all()->pluck('name','id'),
            ],
            "venue_detail" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Venue Detail"
            ],
            "venue_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Venue Name",
                "elmt_group" => [
                    "name" => "venue_location",
                    "type" => self::GROUPTYPE_MAP,
                    "role" => self::GROUPROLE_MAP_LOCATIONNAME,
                ]
            ],
            "venue_latitude" => [
                "type" => self::TYPE_FLOAT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "readonly" => true,
                "relation" => null,
                "label" => "Venue Latitude",
                "elmt_group" => [
                    "name" => "venue_location",
                    "type" => self::GROUPTYPE_MAP,
                    "role" => self::GROUPROLE_MAP_LATITUDE,
                ]
                
            ],
            "venue_longitude" => [
                "type" => self::TYPE_FLOAT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "readonly" => true,
                "relation" => null,
                "label" => "Venue Longitude",
                "elmt_group" => [
                    "name" => "venue_location",
                    "type" => self::GROUPTYPE_MAP,
                    "role" => self::GROUPROLE_MAP_LONGITUDE,
                ]
            ],
            "summary" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Summary",
            ],
            "picture" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Event Picture"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                'options' => $this->getStatusOptions(),
                'filterable' => true,
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ],
        ];
    }
}
