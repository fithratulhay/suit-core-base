<?php

namespace Suitreward\Models;

use Suitcore\Models\SuitModel;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserPoint extends SuitModel
{
    // CONSTANTS
    const STATUS_REQUESTED = 'requested';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_REJECTED = 'rejected';

    const TYPE_REWARD = 'reward';
    const TYPE_REDEEM = 'redeem';

    // MODEL DEFINITION
    public $table = 'user_points';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'user_id',
        'rewardable_id',
        'rewardable_type',
        'point_before',
        'point_change',
        'point_after',
        'point_accumulated',
        'type',
        'note',
        'status',
    ];

    public $rules = [
        'point_change' => 'required|numeric',
    ];

    public function getLabel()
    {
        return "";
    }

    public function getFormattedValue()
    {
        return "";
    }
    
    public function getFormattedValueColumn()
    {
        return [];
    }

    // RELATIONSHIP
    public function rewardable()
    {
        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    // ACCESSOR
    public function getStatusOptions(){
        return [
            self::STATUS_REQUESTED => ucfirst(self::STATUS_REQUESTED),
            self::STATUS_CONFIRMED => ucfirst(self::STATUS_CONFIRMED),
            self::STATUS_REJECTED => ucfirst(self::STATUS_REJECTED),
        ];
    }

    public function getTypeOptions(){
        return [
            self::TYPE_REWARD => ucfirst(self::TYPE_REWARD),
            self::TYPE_REDEEM => ucfirst(self::TYPE_REDEEM),
        ];
    }


    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "options" => User::all()->pluck('name', 'id'),
                "filterable" => true,
                "relation" => 'user',
                "label" => "User"
            ],
            "rewardable_type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "filterable" => true,
                "label" => "Reward Type"
            ],
            "rewardable_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Reward ID"
            ],
            "point_before" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Point Before"
            ],
            "point_change" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Point Change"
            ],
            "point_after" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Point After"
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => self::getTypeOptions(),
                "relation" => false,
                "label" => "Type"
            ],
            "note" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "translation" => true,
                "label" => "Note"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
