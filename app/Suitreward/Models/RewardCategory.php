<?php

namespace Suitreward\Models;

use Suitcore\Models\SuitModel;
use Suitreward\Models\RewardCategory;
use Cviebrock\EloquentSluggable\Sluggable;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class RewardCategory extends SuitModel
{
    use Sluggable;

    // CONSTANTS
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    // MODEL DEFINITION
    public $table = 'reward_categories';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'parent_id',
        'name',
        'slug',
        'status',
    ];

    public $rules = [

    ];

    public function getLabel()
    {
        return "Reward Category";
    }

    public function getFormattedValue()
    {
        return $this->name;
    }
    
    public function getFormattedValueColumn()
    {
        return ['name'];
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    // RELATIONSHIPS
    public function rewards() {
        return $this->hasMany(Reward::class, 'category_id');
    }

    public function childs() {
        return $this->hasMany(RewardCategory::class, 'parent_id');
    }

    public function parent() {
        return $this->belongsTo(RewardCategory::class, 'parent_id');
    }

    // ACCESSOR
    public function getStatusOptions(){
        return [
            self::STATUS_ACTIVE => ucfirst(self::STATUS_ACTIVE),
            self::STATUS_INACTIVE => ucfirst(self::STATUS_INACTIVE),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "parent_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => 'parent',
                "options" => self::where('status', self::STATUS_ACTIVE)->pluck('id', 'name'),
                "label" => "Parent Category"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Name"
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Slug"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
