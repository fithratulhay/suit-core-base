<?php

namespace Suitreward\Models;

use Suitcore\Models\SuitModel;
use App\Models\User;
use Suitreward\Models\Reward;
use Suitreward\Models\RewardCategory;
use Suitreward\Models\UserPoint;
use Suitreward\Models\Traits\Rewardable;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class UserReward extends SuitModel
{
    const STATUS_UNQUALIFIED = 'unqualified';
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_REDEEMED = 'redeemed';

    use Rewardable;

    // MODEL DEFINITION
    public $table = 'user_rewards';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'user_id',
        'reward_id',
        'redeem_counter',
        'generated_code',
        'status',
    ];

    public $rules = [

    ];

    public function getLabel()
    {
        return "User Reward";
    }

    public function getFormattedValue()
    {
        return $this->reward_id;
    }
    
    public function getFormattedValueColumn()
    {
        return ['reward_id'];
    }

    // RELATIONSHIPS

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function reward() {
        return $this->belongsTo(Reward::class, 'reward_id');
    }

    public function morphReward()
    {
        return $this->morphOne(UserPoint::class, 'rewardable');
    }

    public function getStatusOptions(){
        return [
            self::STATUS_UNQUALIFIED => ucfirst(self::STATUS_UNQUALIFIED),
            self::STATUS_PENDING => ucfirst(self::STATUS_PENDING),
            self::STATUS_APPROVED => ucfirst(self::STATUS_APPROVED),
            self::STATUS_REDEEMED => ucfirst(self::STATUS_REDEEMED),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'user',
                "options" => User::where('status', User::STATUS_ACTIVE)->pluck('name', 'id'),
                "label" => "User"
            ],
            "reward_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'reward',
                "options" => Reward::where('status', Reward::STATUS_ACTIVE)->pluck('title', 'id'),
                "label" => "Reward"
            ],
            "redeem_counter" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "label" => "Redeem Count"
            ],
            "generated_code" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Code"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "options" => $this->getStatusOptions(),
                "relation" => false,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
