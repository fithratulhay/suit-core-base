<?php

namespace Suitreward\Models\Traits;

use Suitreward\Models\UserPoint;

trait Rewardable
{
    public function reward()
    {
        return $this->morphOne(UserPoint::class, 'rewardable');
    }
}