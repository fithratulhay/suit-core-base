<?php

namespace Suitreward\Models;

use Suitcore\Models\SuitModel;
use Suitreward\Models\RewardCategory;
use Suitreward\Models\UserReward;
use Cviebrock\EloquentSluggable\Sluggable;

/*
|--------------------------------------------------------------------------
| Table Structure
|--------------------------------------------------------------------------
| * id INT(10) UNSIGNED NOT NULL AUTO INCREMENT PRIMARY
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Reward extends SuitModel
{
    use Sluggable;

    // CONSTANTS
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    // MODEL DEFINITION
    public $table = 'rewards';
    protected static $bufferAttributeSettings = null;

    public $imageAttributes = [
        'image' => 'rewardimages',
    ];

    public $files = [
        'image' => 'rewardimages',
    ];

    public $fillable = [
        'category_id',
        'title',
        'slug',
        'highlight',
        'description',
        'image',
        'term_condition',
        'point_cost',
        'redeem_code',
        'max_redeem',
        'max_redeem_per_user',
        'redeemed_count',
        'start_datetime',
        'end_datetime',
        'status',        
    ];

    public $rules = [

    ];

    public function getLabel()
    {
        return "Reward";
    }

    public function getFormattedValue()
    {
        return $this->title;
    }
    
    public function getFormattedValueColumn()
    {
        return ['title'];
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    // RELATIONSHIP
    public function category() {
        return $this->belongsTo(RewardCategory::class, 'category_id');
    }

    public function userRewards() {
        return $this->hasMany(UserReward::class, 'category_id');
    }

    // ACCESSOR
    public function getStatusOptionsAttribute(){
        return [
            self::STATUS_ACTIVE => ucfirst(self::STATUS_ACTIVE),
            self::STATUS_INACTIVE => ucfirst(self::STATUS_INACTIVE),
        ];
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "ID"
            ],
            "category_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'category',
                "options" => RewardCategory::where('status', RewardCategory::STATUS_ACTIVE)->pluck('name', 'id'),
                "label" => "Category"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => false,
                "translation" => true,
                "label" => "Title"
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Optional Slug"
            ],
            "highlight" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "translation" => true,
                "relation" => false,
                "label" => "Highlight"
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "translation" => true,
                "relation" => false,
                "label" => "Description"
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "translation" => true,
                "relation" => false,
                "label" => "Image"
            ],
            "term_condition" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "translation" => true,
                "relation" => false,
                "label" => "Terms and Conditions"
            ],
            "point_cost" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'category',
                "label" => "Points Cost"
            ],
            "redeem_code" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Optional Redeem Code"
            ],
            "redeemed_count" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => false,
                "label" => "Redeemed Count"
            ],
            "max_redeem" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Optional Serverwide Max Redeem"
            ],
            "max_redeem_per_user" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Optional Max Redeem per User"
            ],
            "start_datetime" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "Start Datetime",
                "options" => null,
                "options_url" => null,
                "elmt_group" => [
                    "name" => "active_datetime_range",
                    "type" => self::GROUPTYPE_DATETIMERANGE,
                    "role" => self::GROUPROLE_RANGE_START
                ]
            ],
            "end_datetime" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => false,
                "label" => "End Datetime",
                "options" => null,
                "options_url" => null,
                "elmt_group" => [
                    "name" => "active_datetime_range",
                    "type" => self::GROUPTYPE_DATETIMERANGE,
                    "role" => self::GROUPROLE_RANGE_END
                ]
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "options" => $this->status_options,
                "relation" => false,
                "label" => "Status"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => false,
                "label" => "Updated At"
            ]
        ];
    }
}
