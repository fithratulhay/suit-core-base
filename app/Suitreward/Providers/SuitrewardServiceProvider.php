<?php

namespace Suitreward\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Suitcore\Providers\LoadRepositoryConfigTrait;

class SuitrewardServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any suitcore services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any suitcore services.
     *
     * @return void
     */
    public function register()
    {
        // Register Suitreward Service Provider
        $repositories = config('suitreward.repositories');
        $this->loadFromConfig($repositories, false, config('suitreward.custom_base_model'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitreward.repositories');
        return array_keys($repositories);
    }
}
