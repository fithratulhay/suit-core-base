<?php

namespace Suitreward\Repositories;

use Carbon\Carbon;
use Suitreward\Models\RewardCategory;
use Suitreward\Repositories\Contract\RewardCategoryRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class RewardCategoryRepository implements RewardCategoryRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(RewardCategory $model) {
        $this->mainModel = $model;
    }

       

}