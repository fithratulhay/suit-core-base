<?php

namespace Suitreward\Repositories;

use Carbon\Carbon;
use Suitreward\Models\UserPoint;

use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitreward\Repositories\Contract\UserPointRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserPointRepository implements UserPointRepositoryContract
{
    use SuitRepositoryTrait;

    protected $userRepo;

    public function __construct(UserPoint $model, SuitUserRepositoryContract $userRepo) {
        $this->mainModel = $model;
        $this->userRepo = $userRepo;
    }

    public function record($userId, $point, $rewardType = null, $rewardId = null, $type = null, $note = null, $extraParams = [], $extraUserParams = []){
        $user = $this->userRepo->find__cached($userId);
        if($user && ($user->point_current + $point) >= 0){
            $lastestUserPoint = $this->mainModel->where('user_id', $userId)->orderBy('created_at', 'desc')->first();
            
            if($lastestUserPoint){
                $pointBefore = $lastestUserPoint->point_after;
            }
            else{
                $pointBefore = 0;
            }

            $params = [
                'user_id' => $userId,
                'rewardable_type' => $rewardType,
                'rewardable_id' => $rewardId,
                'point_before' => $pointBefore,
                'point_change' => $point,
                'point_accumulated' => $point >= 0 ? ($pointBefore + $point) : $pointBefore,
                'point_after' => $pointBefore + $point,
                'type' => $type,
                'note' => $note,
            ];

            $params = array_merge($params, $extraParams);

            $userPoint = $this->create($params);

            $userParams = [
                'point_current' => $userPoint->point_after,
                'point_accumulated' => $userPoint->point_accumulated,
            ];

            $userParams = array_merge($userParams, $extraUserParams);

            $this->userRepo->update($user->id, $userParams);

            return $userPoint;
        }

        return false;
    }

    public function getHistory($userId, $orderType = 'desc'){
        $user = $this->userRepo->find__cached($userId);

        if($user){
            return $this->mainModel->where('user_id', $userId)->orderBy('created_at', $orderType)->get(); 
        }

        return false;
    }
}