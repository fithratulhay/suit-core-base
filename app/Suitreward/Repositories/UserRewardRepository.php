<?php

namespace Suitreward\Repositories;

use Carbon\Carbon;
use Suitreward\Models\UserReward;
use Suitreward\Repositories\Contract\RewardRepositoryContract;
use Suitreward\Repositories\Contract\RewardCategoryRepositoryContract;
use Suitreward\Repositories\Contract\UserRewardRepositoryContract;
use Suitreward\Repositories\Contract\UserPointRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use App\Repositories\Contract\CoachApiRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class UserRewardRepository implements UserRewardRepositoryContract
{
    use SuitRepositoryTrait;

    protected $rewardRepo, $rewardCategoryRepo, $userPointRepo, $userRepo, $coachApiRepo;

    public function __construct(
        UserReward $model,
        RewardRepositoryContract $rewardRepo,
        RewardCategoryRepositoryContract $rewardCategoryRepo,
        UserPointRepositoryContract $userPointRepo,
        SuitUserRepositoryContract $userRepo,
        CoachApiRepositoryContract $coachApiRepo
        ){

        $this->mainModel = $model;
        $this->rewardRepo = $rewardRepo;
        $this->rewardCategoryRepo = $rewardCategoryRepo;
        $this->userPointRepo = $userPointRepo;
        $this->userRepo = $userRepo;
        $this->coachApiRepo = $coachApiRepo;
    }

    public function record($userId, $rewardId, $inputs = null){
        $user = $this->userRepo->find__cached($userId); 
        
        if($user){
            $reward = $this->rewardRepo->find($rewardId);

            if( $reward && 
                $user->point_current >= $reward->point_cost &&
                ($reward->redeemed_count < $reward->max_redeem  || $reward->max_redeem == null) &&
                $reward->status === $this->rewardRepo->getMainModel()::STATUS_ACTIVE &&
                (($reward->start_datetime === null && $reward->end_datetime === null) || ($reward->start_datetime <= Carbon::now()->toDateTimeString() && $reward->end_datetime >= Carbon::now()->toDateTimeString()))
                ){
                
                $userRewards = $this->mainModel->where('user_id', $userId)->where('reward_id', $rewardId)->get();
                $redeemCounter = $userRewards->count();

                if($redeemCounter < $reward->max_redeem_per_user || $reward->max_redeem_per_user == null){
                    $userReward = $this->create([
                        'user_id' => $userId,
                        'reward_id' => $rewardId,
                        'redeem_counter' => $redeemCounter + 1
                    ]);

                    $reward->redeemed_count += 1;
                    $reward->save();

                    $this->userPointRepo->record($userId, -$reward->point_cost, get_class($userReward), $userReward->id, $this->userPointRepo->getMainModel()::TYPE_REDEEM, 'Successfully redeem points with "' . $reward->title . '"!', [
                        'note_trans_id' => 'Berhasil menukar poin dengan "' . $reward->title . '"!',
                    ]);

                    return $userReward;
                }
            }
        }
        
        return false;
    }

}