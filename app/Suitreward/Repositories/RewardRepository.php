<?php

namespace Suitreward\Repositories;

use Carbon\Carbon;
use Suitreward\Models\Reward;
use Suitreward\Repositories\Contract\RewardRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class RewardRepository implements RewardRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Reward $model) {
        $this->mainModel = $model;
    }

    public function getCount(){
        return $this->mainModel->where('status', Reward::STATUS_ACTIVE)->get()->count();
    }

    public function getListOf($categoryId = null, $orderType = 'desc', $pagination = false, $perPage = 5, $params = null) {
        $query = $this->mainModel->where('status', Reward::STATUS_ACTIVE)
                    ->where(function ($query) {
                        $query->where('start_datetime', '<=',Carbon::now())
                        ->orWhere('start_datetime', null);
                    })
                    ->where(function ($query) {
                        $query->where('end_datetime', '>=',Carbon::now())
                        ->orWhere('end_datetime', null);
                    });
        if($categoryId != null){
            $query->where('category_id', $categoryId);
        }
        $query->orderBy('created_at', $orderType);
        if($pagination != false){
            return $query->paginate(8);
        }
        return $query->with('rewardChallenges', 'rank')->get();
    }

}