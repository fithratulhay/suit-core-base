<?php

namespace Suitreward\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface RewardRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}