<?php

namespace Suitreward\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserRewardRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}