<?php

namespace Suitreward\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface UserPointRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}