<?php

namespace Suitreward\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface RewardCategoryRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}