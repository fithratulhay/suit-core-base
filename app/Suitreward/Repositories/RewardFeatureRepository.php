<?php

namespace Suitreward\Repositories;

use Carbon\Carbon;
use Suitreward\Repositories\Contract\RewardFeatureRepositoryContract;

use Suitreward\Repositories\Contract\RewardRepositoryContract;
use Suitreward\Repositories\Contract\RewardCategoryRepositoryContract;
use Suitreward\Repositories\Contract\UserRewardRepositoryContract;
use Suitreward\Repositories\Contract\UserPointRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;

class RewardFeatureRepository implements RewardFeatureRepositoryContract
{

    protected $rewardRepo, $rewardCategoryRepo, $userRewardRepo, $userPointRepo, $userRepo;

    public function __construct(
        RewardRepositoryContract $rewardRepo,
        RewardCategoryRepositoryContract $rewardCategoryRepo,
        UserRewardRepositoryContract $userRewardRepo,
        UserPointRepositoryContract $userPointRepo,
        SuitUserRepositoryContract $userRepo
    ){
        $this->rewardRepo = $rewardRepo;
        $this->rewardCategoryRepo = $rewardCategoryRepo;
        $this->userRewardRepo = $userRewardRepo;
        $this->userPointRepo = $userPointRepo;
        $this->userRepo = $userRepo;
    }
    
}