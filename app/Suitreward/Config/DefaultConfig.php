<?php

namespace Suitreward\Config;

class DefaultConfig
{
    public static $data = [
    ];

    public static function getConfig()
    {
        $instancesConfig = [];
        // priority 1 (backward-compatibility with old-version)
        if (class_exists('\\App\\Config\\SuitrewardConfig')) {
            if (is_array( \App\Config\SuitrewardConfig::$data )) {
                $instancesConfig = \App\Config\SuitrewardConfig::$data;
            }
        }
        // priority 2
        $suitrewardConfig = config('suitreward');
        // priority 3 (default value) : self::$data
        return array_merge( self::$data, $suitrewardConfig, $instancesConfig );
    }
}
