<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Suitcore\Console\DatabaseBackup::class,
        \Suitcore\Console\CleanExportedFiles::class,
        \Suitsite\Commands\NewsletterBroadcaster::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Newsletter Execution
        $schedule->command('newsletter:broadcast')->dailyAt(settings('newsletter_send_hour', '07').':'.settings('newsletter_send_minute', '00'));
        $schedule->command('suitcore:clean-export-file')->daily();
        $schedule->command('suitcore:backup-database')->twiceDaily(12, 23); // Sample of run backup database at 12.00 AM & 23.00 PM
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
