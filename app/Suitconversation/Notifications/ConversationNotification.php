<?php

namespace Suitconversation\Notifications;

use Suitcore\Notifications\BaseNotification;
use Suitcore\Notifications\Channels\DatabaseChannel;

class ConversationNotification extends BaseNotification
{
    public function __construct($link = null, $message = null)
    {
        parent::__construct($link);
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channel[] = DatabaseChannel::class;
        return $channel;
    }

    public function setMessage($notifiable)
    {
        return $this->message;
    }
}
