<?php

namespace Suitconversation\Models;

use Suitcore\Models\SuitModel;

class ConversationGroup extends SuitModel
{
    public $table = 'conversation_groups';

    public $fillable = [
		'name',
		'group_image',
    ];

    public $rules = [
        'name' => 'required',
		'group_image' => '',
    ];

    public function members()
    {
        return $this->hasMany(ConversationMember::class, 'group_id');
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class, 'group_id');
    }

    public function getLabel() {
        return "Conversation Group";
    }

    public function getFormattedValue() {
        return $this->name;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Group Name",
                "options" => null,
                "options_url" => null
            ],
            "group_image" => [
                "type" => self::TYPE_FILE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Group Image",
                "options" => null,
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
