<?php

namespace Suitconversation\Models;

use Suitconversation\Models\ConversationGroup;
use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;

class Conversation extends SuitModel
{
	const SEND_STATUS = 'sent';
	const UNSEND_STATUS = 'unsent';
	const RECEIVED_STATUS = 'received';
	const READ_STATUS = 'read';
	const UNREAD_STATUS = 'unread';

    public $table = 'conversations';

    public $fillable = [
        'group_id',
		'sender_id',
		'receiver_id',
		'content',
		'attachment',
		'status',
    ];

    public $rules = [
        'group_id' => 'numeric|nullable',
        'sender_id' => 'numeric|required',
		'receiver_id' => 'numeric|required',
		'content' => 'string',
		'attachment' => 'alpha_num|nullable',
		'status' => 'alpha',
    ];

    public function sender()
    {
        return $this->belongsTo(SuitUser::class, 'sender_id');
    }

    public function getContentAttribute($value)
    {
        return htmlspecialchars_decode($value);
    }

    public function receiver()
    {
        return $this->belongsTo(SuitUser::class, 'receiver_id');
    }

    public function group()
    {
        return $this->belongsTo(ConversationGroup::class, 'group_id');
    }

    public function getStatusOptions()
    {
    	return [
    		self::SEND_STATUS => ucwords(strtolower(self::SEND_STATUS)),
			self::UNSEND_STATUS => ucwords(strtolower(self::UNSEND_STATUS)),
			self::RECEIVED_STATUS => ucwords(strtolower(self::RECEIVED_STATUS)),
			self::READ_STATUS => ucwords(strtolower(self::READ_STATUS)),
			self::UNREAD_STATUS => ucwords(strtolower(self::UNREAD_STATUS)),
    	];
    }

    public function getLabel() {
        return "Conversation";
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "group_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => "group",
                "label" => "Conversation Group",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitconversation.data_options_route_name.conversationgroup') ? route(config('suitconversation.data_options_route_name.conversationgroup')) : ''),
            ],
            "sender_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "sender",
                "label" => "Sender",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : '')
            ],
            "receiver_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "receiver",
                "label" => "Receiver",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : '')
            ],
            "content" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Content",
                "options" => null,
                "options_url" => null
            ],
            "attachment" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Attachment",
                "options" => null,
                "options_url" => null
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
