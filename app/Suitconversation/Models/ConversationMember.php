<?php

namespace Suitconversation\Models;

use Suitconversation\Models\ConversationGroup;
use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitModel;

class ConversationMember extends SuitModel
{
	const INVITED_STATUS = 'invited';
	const MEMBER_STATUS = 'member';
	const BLOCKED_STATUS = 'blocked';

    public $table = 'conversation_members';

    public $fillable = [
		'user_id',
		'group_id',
		'status',
    ];

    public $rules = [
        'user_id' => 'numeric|required',
		'group_id' => 'numeric|nullable',
		'status' => 'alpha',
    ];

    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function group()
    {
        return $this->belongsTo(ConversationGroup::class, 'group_id');
    }

    public function getStatusOptions()
    {
    	return [
    		self::INVITED_STATUS => ucwords(strtolower(self::INVITED_STATUS)),
			self::MEMBER_STATUS => ucwords(strtolower(self::MEMBER_STATUS)),
			self::BLOCKED_STATUS => ucwords(strtolower(self::BLOCKED_STATUS)),
    	];
    }

    public function getLabel() {
        return "Conversation Group Member";
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "user",
                "label" => "User",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitcore.data_options_route_name.suituser') ? route(config('suitcore.data_options_route_name.suituser')) : '')
            ],
            "group_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "group",
                "label" => "Conversation Group",
                "filterable" => true,
                "options" => null,
                "options_url" => (config('suitconversation.data_options_route_name.conversationgroup') ? route(config('suitconversation.data_options_route_name.conversationgroup')) : '')
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }
}
