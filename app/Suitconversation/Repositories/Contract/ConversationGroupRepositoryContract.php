<?php

namespace Suitconversation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ConversationGroupRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
    public function addMember($object, $userIds = []);
	public function removeMember($object, $userIds = []);
}
