<?php

namespace Suitconversation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ConversationRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
    public function getIndividualConversation($user);
}
