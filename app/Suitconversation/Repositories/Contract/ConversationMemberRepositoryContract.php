<?php

namespace Suitconversation\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ConversationMemberRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour
    public function changeMemberStatus($object, $status = '');
}
