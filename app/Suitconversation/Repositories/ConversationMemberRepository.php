<?php

namespace Suitconversation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitconversation\Models\ConversationMember;
use Suitconversation\Repositories\Contract\ConversationMemberRepositoryContract;

class ConversationMemberRepository implements ConversationMemberRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(ConversationMember $model)
    {
        $this->mainModel = $model;
    }

    public function changeMemberStatus($object, $status = '')
    {
    	if ($status != '') {
    		$object->status = $status;
    		$object->save();
    	}

    	return $object;
    }
}
