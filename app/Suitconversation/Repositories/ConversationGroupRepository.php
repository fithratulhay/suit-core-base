<?php

namespace Suitconversation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitconversation\Models\ConversationGroup;
use Suitconversation\Repositories\Contract\ConversationGroupRepositoryContract;

class ConversationGroupRepository implements ConversationGroupRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(ConversationGroup $model)
    {
        $this->mainModel = $model;
    }

    public function addMember($object, $userIds = [])
    {
    	$object->members()->attach($userIds);
    }

    public function removeMember($object, $userIds = [])
    {
    	$object->members()->attach($userIds);
    }
}
