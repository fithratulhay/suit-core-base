<?php

namespace Suitconversation\Repositories\Facades;

class ConversationGroup extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'SuitconversationGroup\Repositories\Contract\ConversationGroupRepositoryContract';
    }
}
