<?php

namespace Suitconversation\Repositories\Facades;

class ConversationMember extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'SuitconversationMember\Repositories\Contract\ConversationMemberRepositoryContract';
    }
}
