<?php

namespace Suitconversation\Repositories\Facades;

class Conversation extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitconversation\Repositories\Contract\ConversationRepositoryContract';
    }
}
