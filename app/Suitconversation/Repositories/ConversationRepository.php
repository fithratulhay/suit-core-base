<?php

namespace Suitconversation\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitconversation\Models\Conversation;
use Suitconversation\Repositories\Contract\ConversationRepositoryContract;

class ConversationRepository implements ConversationRepositoryContract
{
    use SuitRepositoryTrait;

    function __construct(Conversation $model)
    {
        $this->mainModel = $model;
    }

    public function getIndividualConversation($user)
    {
        $result = [];

        if (!$user) {
            return $result;
        }

        $userId = $user->id;
        $conversations = $this->mainModel->where('sender_id', $userId)
                ->orWhere('receiver_id', $userId)
                ->groupBy('receiver_id')
                ->orderBy('id')
                ->get();

        foreach ($conversations as $conversation) {
            if ($conversation->receiver_id == $userId) {
                $result[$conversation->sender_id] = $conversation;
            } elseif (!array_key_exists($conversation->receiver_id, $result) && $conversation->receiver_id != $userId) {
                $result[$conversation->receiver_id] = $conversation;
            }
        }

    	return $result;
    }

    public function sendConversation($param)
    {
        $link = !empty($param['conversation_link']) ? $param['conversation_link'] : '#';
        $message = !empty($param['conversation_message']) ? $param['conversation_message'] : 'sent you a message';

        unset($param['conversation_link']);
        unset($param['conversation_message']);

        $conversationModel = $this->mainModel;
        $conversationModel->fill($param);
        $conversation = $conversationModel->save();

        if ($conversation->receiver) {
            $conversation->receiver->notify('\Suitconversation\Notifications\ConversationNotification', $link, $message);
        }

        if ($conversation->group) {
            $members = $conversation->group->members();
            foreach ($members as $member) {
                $member->notify('\Suitconversation\Notifications\ConversationNotification', $link, $message);
            }
        }

        return $conversation;
    }
}
