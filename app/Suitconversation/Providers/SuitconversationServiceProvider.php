<?php

namespace Suitconversation\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Suitcore\Providers\LoadRepositoryConfigTrait;

class SuitconversationServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any suitcore services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any suitcore services.
     *
     * @return void
     */
    public function register()
    {
        // Register Suitsite Service Provider
        $repositories = config('suitconversation.repositories');
        $this->loadFromConfig($repositories, false, config('suitconversation.custom_base_model'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitconversation.repositories');
        return array_keys($repositories);
    }
}
