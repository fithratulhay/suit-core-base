<?php

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Suitcore\Config\DefaultConfig;
use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Facades\SuitSetting;
use Suitcore\Repositories\Facades\SuitUserSetting;

if (! function_exists('settings')) {
    function settings($key, $default = null)
    {
        $setting = null;
        try {
            $setting = SuitSetting::getValue($key, null);
        } catch (\Exception $e) { }
        return $setting ?: array_get(DefaultConfig::getConfig(), $key, $default);
    }
}

if (! function_exists('updatesetting')) {

    function updatesetting($key, $value)
    {
        try {
            SuitSetting::updateByKey($key, $value);
        } catch (\Exception $e) { }
    }
}

if (! function_exists('isUrlAuthorized')) {
    function isUrlAuthorized($url, $isPost = false)
    {
        if ($url) {
            $baseModel = new SuitModel;
            $routeName = '';
            $normalizedUrl = preg_replace("/(#\w+#)/", "1", $url);
            if ($isPost) {
                $routeName = app('router')->getRoutes()->match(app('request')->create($normalizedUrl, 'POST'))->getName();
            } else {
                $routeName = app('router')->getRoutes()->match(app('request')->create($normalizedUrl))->getName();
            }
            if ($routeName &&
                   Gate::allows('suitcorepermission', [$baseModel, $routeName]))
                    return true;
        }
        return false;
    }
}

if (! function_exists('isNavMenuEnabled')) {
    function isNavMenuEnabled($menuCode)
    {
        $menuItems = DefaultConfig::getConfig()['backendNavigation'];
        if ($menuItems && is_array($menuItems) &&
            isset($menuItems[$menuCode]) &&
            isset($menuItems[$menuCode]['submenu'])) {
            foreach ($menuItems[$menuCode]['submenu'] as $subMenuCode => $value) {
                $baseModel = new SuitModel;
                if (isset($value['route']) &&
                   Gate::allows('suitcorepermission', [$baseModel, $value['route']]))
                    return true;
            }
        }
        if ($menuItems && is_array($menuItems) &&
            isset($menuItems[$menuCode]) &&
            (!isset($menuItems[$menuCode]['submenu']) || empty($menuItems[$menuCode]['submenu'])) &&
            isset($menuItems[$menuCode]['route'])) {
            $baseModel = new SuitModel;
            return Gate::allows('suitcorepermission', [$baseModel, $menuItems[$menuCode]['route']]);
        }
        return false;
    }
}

if (! function_exists('isEmptyDate')) {

    function isEmptyDate($date)
    {
        return ($date == null || empty($date) || $date == "0000-00-00 00:00:00");
    }
}

if (! function_exists('generateRandomString')) {

    function generateRandomString($length = 5)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($characters, $length)), 0, $length);
    }
}

if (! function_exists('startsWith')) {
    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }
}

if (! function_exists('endsWith')) {
    function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }
}

if (! function_exists('asCurrency')) {

    function asCurrency($number)
    {
        return 'Rp. '.number_format($number, 2, '.', ',');
    }
}

if (! function_exists('asPhoneNumberDestination')) {

    function asPhoneNumberDestination($text)
    {
        return str_replace(['-', ' '], '', $text);
    }
}

if (! function_exists('getUsernameByEmail')) {

    function getUsernameByEmail($email, $useDot = false) {
        $username = str_replace(substr($email, strpos($email, '@')), '', $email);

        if ($useDot) {
            return $username;
        }

        return str_replace('.', '', $username);
    }
}

if (! function_exists('getUserSettings')) {
    function getUserSettings($key, $default = null, $user_id = null)
    {
        if ($user_id && $userSetting = SuitUserSetting::getValue($user_id, $key, $default)) {
            return $userSetting->value;
        }

        if (session('user_settings')) {
            $sess = collect(session('user_settings', []))->where('key', $key)->first();
            return $sess ? $sess['value'] : $default;
        }

        return $default;
    }
}

if (! function_exists('nav_link')) {
    function nav_link($routes, $text) {
        $link = explode('/', Request::path());
        foreach($routes as $route){
            if ($link[1] == $route){
                $active = "class = 'active'";
                break;
            } else
                $active = '';
        }
        return '<li '.$active.'><a href="'.url('admin/'.$route.'').'"><i class="fa fa-folder"></i><span>'.$text.'</span></a></li>';
    }
}

if (! function_exists('nav_menu')) {
    function nav_menu($url, $text, $iconString = null, $class = "btn primary", $title = "") {
        return '<a class="'.$class.'" href="' . $url . '" title="'.$title.'">'.($iconString != null ? '<i class="' . $iconString . '"></i>' : '').' '.$text.'</a>';
    }
}

if (! function_exists('post_nav_menu')) {
    function post_nav_menu($url, $text, $token, $confirmText = null, $iconString = null, $class = "btn btn--red", $title = "") {
        return '<form style="display:inline;" method="post" action="' . $url . '"><input type="hidden" name="_token" value="' . $token . '"><button type="submit" class="'.$class.'" '.(!empty($confirmText) ? 'onClick="return confirm(\''. $confirmText .'\');"' : '').' title="'.$title.'">'.($iconString != null ? '<i class="' . $iconString . '"></i>' : '').' '.$text.'</button></form>';
    }
}

if (! function_exists('menu_items_by_type')) {
    function menu_items_by_type($type) {
        $result = null;
        $menuRepo = app('Suitsite\Repositories\Contract\MenuRepositoryContract');
        $result = $menuRepo->cachedList($type, false, null);

        return $result;
    }
}

if (! function_exists('submenu_by_type')) {
    function submenu_by_type($type, $all = false, $parentId) {
        $result = null;
        $menuRepo = app('Suitsite\Repositories\Contract\MenuRepositoryContract');
        $result = $menuRepo->cachedList($type, $all, $parentId);

        return $result;
    }
}

if (!function_exists('s3Upload')) {
    function s3Upload($fullUrl) {
        if (in_array(env('APP_ENV'), ['production'])) {
            $url = parse_url($fullUrl);
            if (SuitSetting::getValue('assets_cdn', false) && array_key_exists('path', $url)) {
                $fileName = $url['path'];
                $storageS3 = Storage::disk('s3');
                try {
                    $uploaded = $storageS3->getDriver()->put(
                        $fileName,
                        file_get_contents($fullUrl),
                        ['CacheControl' => 'max-age=315360000, public']
                    );
                    if ($uploaded) {
                        return $fileName;
                    }
                } catch (\Exception $e) {
                    info("S3-Upload [" . $fullUrl . "] : " . $e->getMessage());
                }
            }
        }
        return null;
    }
}

if (!function_exists('assetCdn')) {
    function assetCdn($assetPath)
    {
        if (in_array(env('APP_ENV'), ['production'])) {
            return SuitSetting::getValue('assets_cdn', '') . '/' . ltrim($assetPath, '/');
        }
        return asset($assetPath);
    }
}

if (!function_exists('str_text_beautifier')) {
    /**
     * [str_text_beautifier description]
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
    function str_text_beautifier($text)
    {
        if (strpos($text, '-')) {

            $texts = explode('-', $text);

            while (list($key, $val) = each($texts)) {
                $texts[$key] = ucfirst(strtolower(str_replace('_', ' ', $val)));
            }

            return implode(' & ', $texts);
        } else {
            return ucfirst(strtolower(str_replace('_', ' ', $text)));
        }
    }
}

if (! function_exists('top5Notification')) {

    function top5Notification()
    {
        if (auth()->check()) {
            $param = [
                'user_id' => auth()->user()->id,
                'orderBy' => 'created_at',
                'orderType' => 'desc',
                'paginate' => false,
                'is_read' => 0,
                'perPage' => 5
            ];
            $notificationRepo = app('Suitcore\Repositories\Contract\SuitNotificationRepositoryContract');
            return $notificationRepo->getByParameter__cached($param);
        }
        return collect([]);
    }
}

if (! function_exists('notifCounter')) {

    function notifCounter()
    {
        if (auth()->check()) {
            $notificationRepo = app('Suitcore\Repositories\Contract\SuitNotificationRepositoryContract');
            return ( $nbNotification = $notificationRepo->getNotificationCount(auth()->user()) ) < 100 ? $nbNotification : "99+";
        }
        return 0;
    }
}

/**
 * 
 */
if (! function_exists('purify_input_xss')) {

    function purify_input_xss($string, $extraFilters = ['AngularAntiXSS' => false], $options = ['AutoFormat.AutoParagraph' => false])
    {
        if(config('suitcore.purify_output_xss')){
            $output = htmlspecialchars_decode($string);

            if($output != $string){
                $output = htmlspecialchars(clean($output, $options));
            }
            else{
                $output = clean($output, $options);
            }

            if($extraFilters['AngularAntiXSS']){
                $output = preg_replace("/\{\{(.*)\}\}/", '$1', $output);
            }

            return $output;
        }

        return $string;
    }
}

/**
 * 
 */
if (! function_exists('safe_output_xss')) {

    function safe_output_xss($string, $options = ['AutoFormat.AutoParagraph' => false])
    {
        if(config('suitcore.purify_output_xss')){
            return clean(htmlspecialchars_decode($string), $options);
        }

        return htmlspecialchars_decode($string);
    }
}

if (! function_exists('exportExcelRoute')) {
    function exportExcelRoute($namePrefix, $controllerName)
    {
        Route::get('exportxls', ['as' => $namePrefix.'.exportxls', 'uses' => $controllerName.'@exportToExcel']);
        // Route::get('exportxls-file/{file}', ['as' => $namePrefix.'.exportxlsfile', 'uses' => $controllerName.'@getExcelFile']);
        Route::get('exportprogress', ['as' => $namePrefix.'.exportprogress', 'uses' => $controllerName.'@getExportProgress']);
        Route::post('exportstop', ['as' => $namePrefix.'.exportstop', 'uses' => $controllerName.'@postStopExportProcess']);
    }
}

if (! function_exists('importExcelRoute')) {
    function importExcelRoute($namePrefix, $controllerName)
    {
        Route::get('template', ['as' => $namePrefix.'.template', 'uses' => $controllerName.'@template']);
        Route::get('downloadtemplate', ['as' => $namePrefix.'.downloadtemplate', 'uses' => $controllerName.'@downloadTemplate']);
        Route::post('importfromtemplate', ['as' => $namePrefix.'.importfromtemplate', 'uses' => $controllerName.'@importFromTemplate']);
        Route::get('importprogress', ['as' => $namePrefix.'.importprogress', 'uses' => $controllerName.'@getImportProgress']);
        Route::post('importstop', ['as' => $namePrefix.'.importstop', 'uses' => $controllerName.'@postStopImportProcess']);
    }
}