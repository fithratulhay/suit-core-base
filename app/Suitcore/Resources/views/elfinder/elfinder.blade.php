@extends('suitcore.' . $baseLayout . '.layouts.base')

@inject('baseConfig', '\Suitcore\Config\DefaultConfig')

@push('style-head')
    <!-- jQuery and jQuery UI (REQUIRED) -->
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />

    <!-- elFinder CSS (REQUIRED) -->
    <link rel="stylesheet" type="text/css" href="{{ asset($dir.'/css/elfinder.min.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset($dir.'/css/theme.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset($dir.'/css/theme-material-gray.min.css') }}">
@endpush

@section('content')

<?php 
$heading = isset($pageId) && isset($baseConfig::getConfig()['backendNavigation'][$pageId[0]]['submenu'][$pageId]) ? $baseConfig::getConfig()['backendNavigation'][$pageId[0]]['submenu'][$pageId]['label'] : "File Manager";

$pageTitle = trans($heading);
?>

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="{{$pageIcon or ''}} font-dark"></i>&nbsp;
                    <span class="caption-subject font-dark sbold uppercase">{{ $pageTitle or '' }}</span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body table-manages">
                <!-- Element where elFinder will be created (REQUIRED) -->
                <div id="elfinder"></div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- END CONTENT -->
@stop

@push('end_script')
    <!-- elFinder JS (REQUIRED) -->
    <script src="{{ asset($dir.'/js/elfinder.min.js') }}"></script>

    @if($locale)
        <!-- elFinder translation (OPTIONAL) -->
        <script src="{{ asset($dir."/js/i18n/elfinder.$locale.js") }}"></script>
    @endif

    <!-- elFinder initialization (REQUIRED) -->
    <script type="text/javascript" charset="utf-8">
        // Documentation for client options:
        // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
        $().ready(function() {
            $('#elfinder').elfinder({
                // set your elFinder options here
                @if($locale)
                    lang: '{{ $locale }}', // locale
                @endif
                customData: { 
                    _token: '{{ csrf_token() }}'
                },
                url : '{{ route("elfinder.connector") }}',  // connector URL
                soundPath: '{{ asset($dir.'/sounds') }}'
            });
        });
    </script>
@endpush
