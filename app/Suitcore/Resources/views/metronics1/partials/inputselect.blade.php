<!-- (2) Dropdown List Options -->
<div class="form-group {{ $formSetting['errors'] ? 'has-error' : '' }}" id="{{ $formSetting['container_id'] }}">
    <label class="col-md-3 control-label" for="{{ $formSetting['id'] }}">{{ $formSetting['label'] }}</label>
    <div class="col-md-9">
        {{ Form::select($formSetting['name'], ($formSetting['options'] ? $formSetting['options'] : ((isset($formSetting['data_url']) && !empty($formSetting['data_url'])) ? [$formSetting['value'] => (isset($formSetting['value_text']) ?  $formSetting['value_text'] : 'Unknown ' . ucfirst(strtolower($formSetting['label'])))] : [])), $value = old($formSetting['name'], $formSetting['value']),
            array_merge([
                'class' => ('form-control select2' . ($formSetting['multiple'] ? '-multiple' : '')),
                'id' => $formSetting['id'],
                'data-value' => $value],
            $formSetting['required'] ? ['required' => ''] : [],
            $formSetting['readonly'] ? ['readonly' => ''] : [],
            $formSetting['multiple'] ? ['multiple' => ''] : [],
            ['placeholder' => 'select ' . strtolower($formSetting['label']), 'data-placeholder' => 'select ' . strtolower($formSetting['label'])],
            (!$formSetting['multiple'] && !isset($formSetting['form_field_relation']) && isset($formSetting['data_url']) && !empty($formSetting['data_url'])) ? [
                'metronics-select-autocomplete' => $formSetting['data_url'],
                'metronics-select-autocomplete-init-value' => $formSetting['value'],
                'metronics-select-autocomplete-init-text' => (isset($formSetting['value_text']) ? $formSetting['value_text'] : 'Unknown ' . ucfirst(strtolower($formSetting['label']))),
                'metronics-select-autocomplete-empty-text' => "-- select " . strtolower($formSetting['label']) . " --" 
            ] : [],
            $formSetting['attributes']))
        }}
        @if($formSetting['errors'])
        <span class="help-block help-block-error">{{ $formSetting['errors'] ? $formSetting['errors'] : "" }}</span>
        <div class="form-control-focus"> </div>
        @endif
    </div>
</div>

@push('pre_end_script_1')
@if(isset($formSetting['data_url']) && !empty($formSetting['data_url']) && isset($formSetting['form_field_relation']) && is_array($formSetting['form_field_relation']) && isset($formSetting['form_field_relation']['top']) &&  isset($formSetting['form_field_relation']['bottom']))
<script type="text/javascript">
    var initial{{ $formSetting['id'] }} = "{{ $formSetting['value'] }}";
    function init{{ $formSetting['id'] }}() {
        @if(empty($formSetting['form_field_relation']['top']))
            $.get("{!! $formSetting['data_url'] . (str_contains($formSetting['data_url'], '?') ? '&' : '?') . 'all=1' !!}", { timestamp: {{ time() }} },
        @else 
            $.get("{!! $formSetting['data_url'] . (str_contains($formSetting['data_url'], '?') ? '&' : '?') . 'all=1&' . $formSetting['form_field_relation']['top'] . '=xxxxx' !!}".replace("xxxxx", $("#{{ 'input'.ucfirst(strtolower($formSetting['form_field_relation']['top'])) }}").val()), { timestamp: {{ time() }} },
        @endif
            function(data) {
                var model = $('#{{ $formSetting['id'] }}');
                model.empty();

                model.append("<option value=''>-- select {{ $formSetting['label'] }} --</option>");
                $.each(data, function(index, element) {
                    model.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                });

                if (initial{{ $formSetting['id'] }} != "") {
                    model.val(initial{{ $formSetting['id'] }}).trigger("change");
                    initial{{ $formSetting['id'] }} = "";
                } else {
                    model.val("").trigger("change");
                }
        });
    }
</script>
@endif
@endpush

@push('pre_end_script_2')
@if(isset($formSetting['data_url']) && !empty($formSetting['data_url']) && isset($formSetting['form_field_relation']) && is_array($formSetting['form_field_relation']) && isset($formSetting['form_field_relation']['top']) &&  isset($formSetting['form_field_relation']['bottom']))
<script type="text/javascript">
    function onChange{{ $formSetting['id'] }}() {
        @if(!empty($formSetting['form_field_relation']['bottom']))
        <?php
            $childId = "input" . ucfirst(strtolower($formSetting['form_field_relation']['bottom']));
        ?>
        $('#{{ $childId }}').empty();
        $('#{{ $childId }}').append("<option value=''>-- empty, select {{ $formSetting['label'] }} first --</option>");
        $('#{{ $childId }}').val("").trigger("change");
        init{{ $childId }}();
        @endif
    }
    $('#{{ $formSetting['id'] }}').bind("change", onChange{{ $formSetting['id'] }});
</script>
@endif
@endpush

@push('end_script')
@if(isset($formSetting['data_url']) && !empty($formSetting['data_url']) && isset($formSetting['form_field_relation']) && is_array($formSetting['form_field_relation']) && isset($formSetting['form_field_relation']['top']) &&  empty($formSetting['form_field_relation']['top']))
<script type="text/javascript">
    init{{ $formSetting['id'] }}();
</script>
@endif
@endpush
