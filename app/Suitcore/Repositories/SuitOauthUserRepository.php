<?php

namespace Suitcore\Repositories;

use Suitcore\Models\SuitOauthUser;
use Suitcore\Repositories\Contract\SuitOauthUserRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;


class SuitOauthUserRepository implements SuitOauthUserRepositoryContract
{
    use SuitRepositoryTrait;

    protected $mainModel = null;

    /**
     * Default Constructor
     **/
    public function __construct(SuitOauthUser $model)
    {
        $this->mainModel = $model;
    }

    public function getOauth($oauthData)
    {
        /*
        $initialData = [
            'paginate' => 'false'
        ];

        $param = array_merge($oauthData, $initialData);
        $obj = $this->getByParameter($param);
        return $obj;
        */
        return $this->mainModel->firstOrNew($oauthData);
    }
}
