<?php

namespace Suitcore\Repositories;

use Exception;
use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitUser;
use Suitcore\Models\SuitUserToken;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitcore\Repositories\Contract\SuitUserTokenRepositoryContract;

class SuitUserTokenRepository implements SuitUserTokenRepositoryContract
{   
    use SuitRepositoryTrait;
    
    protected $mainModel = null;

    /**
     * Default Constructor
     **/
    public function __construct(SuitUserToken $model)
    {
        $this->mainModel = $model;
    }

    /**
     * Get user token from user
     * @param SuitUser $user
     * @return json
     */
    public function getUserTokenFromUser(SuitUser $user)
    {
        $uToken = $this->mainModel->create(['user_id' => $user->getKey(), 'token' => env('APP_KEY', 'suitcore')]);
        $uToken->token = $uToken->generateToken();
        $uToken->save();
        return $uToken;
    }
}
