<?php

namespace Suitcore\Repositories\Facades;

class SuitNotification extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitNotificationRepositoryContract';
    }
}
