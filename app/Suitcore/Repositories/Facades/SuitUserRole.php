<?php

namespace Suitcore\Repositories\Facades;

class SuitUserRole extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract';
    }
}
