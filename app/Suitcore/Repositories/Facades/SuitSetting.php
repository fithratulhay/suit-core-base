<?php

namespace Suitcore\Repositories\Facades;

class SuitSetting extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitSettingRepositoryContract';
    }
}
