<?php

namespace Suitcore\Repositories\Facades;

class SuitUserPermission extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract';
    }
}
