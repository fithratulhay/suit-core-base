<?php

namespace Suitcore\Repositories\Facades;

class SuitOauthUser extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitOauthUserRepositoryContract';
    }
}
