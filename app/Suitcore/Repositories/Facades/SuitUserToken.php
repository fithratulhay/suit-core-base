<?php

namespace Suitcore\Repositories\Facades;

class SuitUserToken extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUserTokenRepositoryContract';
    }
}
