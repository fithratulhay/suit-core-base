<?php

namespace Suitcore\Repositories\Facades;

class SuitUrlSeo extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUrlSeoRepositoryContract';
    }
}
