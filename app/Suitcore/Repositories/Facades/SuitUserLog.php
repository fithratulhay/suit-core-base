<?php

namespace Suitcore\Repositories\Facades;

class SuitUserLog extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUserLogRepositoryContract';
    }
}
