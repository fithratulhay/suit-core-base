<?php

namespace Suitcore\Repositories\Facades;

class SuitUser extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUserRepositoryContract';
    }
}
