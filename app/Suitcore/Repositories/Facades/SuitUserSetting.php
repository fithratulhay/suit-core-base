<?php

namespace Suitcore\Repositories\Facades;

class SuitUserSetting extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitcore\Repositories\Contract\SuitUserSettingRepositoryContract';
    }
}
