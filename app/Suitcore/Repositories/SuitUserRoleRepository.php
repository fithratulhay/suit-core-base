<?php

namespace Suitcore\Repositories;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitUserRole;
use Suitcore\Models\SuitUserPermission;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitUserRoleRepository implements SuitUserRoleRepositoryContract
{
    use SuitRepositoryTrait;

    protected $permissionRepo;

    protected $excludedRoutes = [
        'backend.sessions.login',
        'backend.sessions.postlogin',
        'backend.sessions.logout'
    ];

    public function __construct(SuitUserRole $model, SuitUserPermissionRepositoryContract $permissionRepo)
    {
        $this->mainModel = $model;
        $this->permissionRepo = $permissionRepo;
    }

    public function getByCode($code) {
        return $this->getBy__cached('code', $code)->first();
    }

    public function getBySubPath($subpath) {
        return $this->getBy__cached('subpath', $subpath)->first();
    }

    public function getAllRoles() {
        $mainObj = $this->mainModel;
        $roleList = Cache::rememberForever('role_list', function() use($mainObj) {
            return $mainObj->all()->pluck('name','id');
        });
        return $roleList;
    }

    public function getCurrentRolePath(Request $request) {
        $regex = '/'.str_replace('/', '\/', $request->root()).'\/(' . implode('|', $this->getAllSubpath()) . ')/';
        if (config('suitcore.backend_admin_subpath_access') == 'subdomain') {
            $regex = '/'.str_replace('/', '\/', 'http://' . '(' . implode('|', $this->getAllSubpath()) . ').' . config('suitcore.base_domain')) . '/';
        }
        preg_match($regex, $request->url(), $matches);
        $currentSubpath = config('suitcore.backend_admin_subpath');
        if ($matches && is_array($matches) && count($matches) > 1) {
            $currentSubpath = $matches[1];
        }
        if (empty($currentSubpath)) {
            return config('suitcore.backend_admin_subpath');
        }
        return strtolower($currentSubpath);
    }

    public function getAllSubpath() {
        $mainObj = $this->mainModel;
        $subpathList = Cache::rememberForever('role_subpath_list', function() use($mainObj) {
            try {
                return array_merge(['paneladmin'] , $mainObj->all()->pluck('subpath')->toArray());
            } catch (Exception $e) {}
            return ['paneladmin'];
        });
        return $subpathList;
    }

    public function getAllRoleCodes() {
        $mainObj = $this->mainModel;
        $roleCodeList = Cache::rememberForever('role_code_list', function() use($mainObj) {
            return $mainObj->all()->pluck('code','id');
        });
        return $roleCodeList;
    }

    public function getAllRoleObjects() {
        $mainObj = $this->mainModel;
        $roleObjectList = Cache::rememberForever('role_object_list', function() use($mainObj) {
            return $mainObj->all();
        });
        return $roleObjectList;
    }

    public function isPermitted($user, $route, $baseModel = null) {
    	if ($user) {
    		if (isset($user->role) && is_string($user->role)) {
    			// Admin allow all
    			if ($user->role == SuitUserRole::DEFAULT_ADMIN) {
    				return true;
    			}
                // Route Excludes
                if (strpos($route, 'json') !== false ||
                    strpos($route, 'importprogress') !== false ||
                    in_array($route, $this->excludedRoutes)
                ) {
                    return true;
                }
    			// Continue Check
    			$role = $this->getAllRoleObjects()->where('code','=',$user->role)->first();
                $permissionList = $this->permissionRepo->getCachedList();
    			if ($role &&
    				isset($permissionList[$role->id]) &&
                    isset($permissionList[$role->id][$route])) {
    				$permission = $permissionList[$role->id][$route];
                    $permissionElmt = explode(':', $permission);
    				$userMappingValue = null;
    				$objectMappingValue = null;
    				if ($permission &&
                        $permissionElmt &&
                        is_array($permissionElmt) &&
                        count($permissionElmt) == 2) {
                        if ($baseModel &&
                            $permissionElmt[1] == SuitUserPermission::MAPPINGACCEPT) {
                            $permissionObj = SuitUserPermission::find($permissionElmt[0]);
                            if ($permissionObj &&
                                $permissionObj->optional_user_mapping_field &&
                                $permissionObj->optional_object_mapping_field) {
                                $userMappingValue = $user->getAttribute($permissionObj->optional_user_mapping_field);
                                $objectMappingValue = $baseModel->getAttribute($permissionObj->optional_object_mapping_field);
                            }
                        }
                        return (($permissionElmt[1] == SuitUserPermission::ACCEPT) || ($permissionElmt[1] == SuitUserPermission::MAPPINGACCEPT && $userMappingValue && $objectMappingValue && $userMappingValue == $objectMappingValue) || ($permissionElmt[1] == SuitUserPermission::MAPPINGACCEPT && !$userMappingValue && !$objectMappingValue));
    				}
    			}
    		}
    	}
    	return (in_array($route, $this->excludedRoutes) ? true : settings('default_permission', false));
    	//return true; // default is permitted (for test purpose only)
    }
}
