<?php

namespace Suitcore\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Suitcore\Models\SuitNotification;
use Suitcore\Repositories\Contract\SuitNotificationRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitNotificationRepository implements SuitNotificationRepositoryContract
{
    use SuitRepositoryTrait;

    /* CONSTANTS */
    const SHOWALL = 0;
    // -- action to message
    const ACTION_READ = "read";
    const ACTION_UNREAD = "unread";

    /* CONSTRUCTOR AND SERVICES */
    public function __construct(SuitNotification $model)
    {
        $this->mainModel = $model;
        $this->dependencies = ['user'];
    }

    /**
     * Add new notification.
     * @param int $relatedUserId
     * @param string $message
     * @return void
     */
    public function add($relatedUserId, $message, $url = null)
    {
        if ($relatedUserId) {
            return $this->create([
                'user_id' => $relatedUserId,
                'message' => $message,
                'url' => $url
            ]);
        }
        return true;
    }

    /**
     * Get the number of notification for a specific user.
     * @param \App\Suitcore\Models\SuitUser $user
     * @return int
     */
    public function getNotificationCount($user)
    {
        if ($user && $user->id > 0) {
            return Cache::rememberForever('notification_counter_'.$user->id, function () use ($user) {
                $notifs = SuitNotification::where('user_id', $user->id)->where('is_read', false)->get();
                return ($notifs ? $notifs->count() : 0);
            });
        }
        return 0;
    }

    public function read(array $ids = [])
    {
        $notifications = $this->mainModel->whereIn('id', $ids)->get();
        foreach ($notifications as $notification) {
            $notification->update(['is_read' => true]);
        }
    }
}
