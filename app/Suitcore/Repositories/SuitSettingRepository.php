<?php

namespace Suitcore\Repositories;

use View;
use Exception;
use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitSetting;
use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Contract\SuitSettingRepositoryContract;

class SuitSettingRepository implements SuitSettingRepositoryContract
{   
    protected $mainModel = null;

    protected $elementGroup = [];

    /**
     * Default Constructor
     **/
    public function __construct(SuitSetting $model)
    {
        $this->mainModel = $model;
    }

    /**
     * Update setting by key.
     * @param string $key
     * @param string $value
     * @return void
     */
    public function updateByKey($key, $value)
    {
        $baseModel = ($this->mainModel ? $this->mainModel->getNew() : new SuitSetting);
        $setting = $baseModel->firstOrNew(['key' => $key]);
        $setting->value = $value;
        $result = $setting->save();
        if ($result) {
            // Begin Update Cache
            Cache::forever('settings', SuitSetting::pluck('value', 'key'));
            // Finish Update Cache
        }
    }

    /**
     * Get value of setting.
     * @param string $key
     * @param  string $default
     * @return string
     */
    public function getValue($key, $default = '')
    {
        $baseModel = ($this->mainModel ? $this->mainModel->getNew() : new SuitSetting);
        $setting = Cache::rememberForever('settings', function () use($baseModel) {
            return $baseModel->pluck('value', 'key');
        });
        return isset($setting[$key]) ? $setting[$key] : $default;
    }

    /**
     * Save settings
     * @param array $settingArray
     * @return boolean
     */
    public function save($settingArray, $fileList = []) {
        $result = false;
        if (is_array($settingArray)) {
            try {
                $settings = $settingArray;
                // checklist settings
                $definedSettings = SuitSetting::pluck('value', 'key');
                foreach ($definedSettings as $key => $value) {
                    if (!in_array($key, $fileList) &&
                        !isset($settings[$key]) &&
                        in_array($value, ['true', 'false'])) 
                        $settings[$key] = '';
                }
                // for array type settings
                $settings = collect($settings)->map(function ($item) {
                    return is_array($item) ? json_encode($item) : $item;
                })->toArray();
                // save
                foreach ($settings as $key => $value) {
                    $result = $this->updateByKey($key, ($value === null ? "" : $value));
                }
            } catch (Exception $e) { }
        }
        return $result;
    }

    private function isElmtGroupComplete($elmtGroupName) {
        if (is_array($this->elementGroup) && isset($this->elementGroup[$elmtGroupName])) {
            if (is_array($this->elementGroup[$elmtGroupName]) && isset($this->elementGroup[$elmtGroupName]['type']) && isset($this->elementGroup[$elmtGroupName]['elmt']) && is_array($this->elementGroup[$elmtGroupName]['elmt'])) {
                if ($this->elementGroup[$elmtGroupName]['type'] == SuitModel::GROUPTYPE_MAP) {
                    return (isset($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_MAP_LOCATIONNAME]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_MAP_LOCATIONNAME]) && isset($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_MAP_LATITUDE]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_MAP_LATITUDE]) && isset($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_MAP_LONGITUDE]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_MAP_LONGITUDE]));
                } elseif (in_array($this->elementGroup[$elmtGroupName]['type'], [SuitModel::GROUPTYPE_NUMERICRANGE, SuitModel::GROUPTYPE_DATETIMERANGE, SuitModel::GROUPTYPE_DATERANGE, SuitModel::GROUPTYPE_TIMERANGE])) {
                    return (isset($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_RANGE_START]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_RANGE_START]) && isset($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_RANGE_END]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][SuitModel::GROUPROLE_RANGE_END]));
                }
            }
        }
        return false;
    }

    public function resetGroupRendering() {
        $this->elementGroup = [];
    }

    public function isGroupSettingValid($elmt) {
        return (isset($elmt['elmt_group']) && is_array($elmt['elmt_group']) && isset($elmt['elmt_group']['name']) && isset($elmt['elmt_group']['type']) && isset($elmt['elmt_group']['role']));
    }

    public function addOrUpdateToGroupRendering($elmt) {
        if ($elmt && is_array($elmt) && $this->isGroupSettingValid($elmt)) {
            if (!isset($this->elementGroup[$elmt['elmt_group']['name']])) {
                $this->elementGroup[$elmt['elmt_group']['name']] = [
                    'type' => $elmt['elmt_group']['type'],
                    'elmt' => []
                ];
            }
            if (in_array($elmt['elmt_group']['role'], [SuitModel::GROUPROLE_MAP_LATITUDE, SuitModel::GROUPROLE_MAP_LONGITUDE])) {
                $elmt['readonly'] = true;
                $elmt['numberdigit'] = 10;
            }
            $this->elementGroup[$elmt['elmt_group']['name']]['elmt'][$elmt['elmt_group']['role']] = $elmt;
        }
    }

    public function removeFromGroupRendering($elmt) {
        if ($elmt && is_array($elmt) && isset($elmt['elmt_group']['name']) && isset($elmt['elmt_group']['role']) && 
            isset($this->elementGroup[$elmt['elmt_group']['name']]) &&
            isset($this->elementGroup[$elmt['elmt_group']['name']]['elmt']) &&
            isset($this->elementGroup[$elmt['elmt_group']['name']]['elmt'][$elmt['elmt_group']['role']])) {
            unset($this->elementGroup[$elmt['elmt_group']['name']]['elmt'][$elmt['elmt_group']['role']]);
        }
    }

    public function getGroupRenderingMember($elmtGroupName) {
        if ($elmtGroupName && 
            isset($this->elementGroup[$elmtGroupName])) {
            return $this->elementGroup[$elmtGroupName];
        }
        return null;
    }

    public function getAllGroupElement() {
        return $this->elementGroup;
    }

    public function renderFormView($key, $type, $label, $defaultValue = null, $uploadHandler = null, $errors = null, $attributes = null, $basic = false, $optionsAjaxHandler = null) {
        // initial setup
        $currentLocale = strtolower(config('app.fallback_locale', 'en'));
        $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $currentLocale));
        $attrSettings[$key] = [
            "type" => $type,
            "visible" => true,
            "formdisplay" => true,
            "required" => false,
            "relation" => null,
            "label" => $label,
            "readonly" => false,
            "initiated" => false,
            "filterable" => false,
            "translation" => false,
            "options" => null,
            "options_url" => null
        ];
        $formSetting = [
            'container_id' => false, //
            'id' => false, //
            'masked_id' => false, //
            'name' => false, //
            'label' => false, //
            'value' => false, //
            'masked_value' => false,
            'options' => false, //
            'required' => false, //
            'readonly' => false, //
            'image_file_url' => false,
            'file_url' => false,
            'action_handler_route' => false, //
            'errors' => false, //
            'data'       => false,
            'attributes' => false,
            'multiple'   => false,
            'wysiwyg'    => false,
            'selecttype' => false,
            'data_url' => (isset($attrSettings[$key]['options_url']) && !empty($attrSettings[$key]['options_url']) ? $attrSettings[$key]['options_url'] : false), //// options autocomplete growing ajax
            'value_text' => false //// options autocomplete growing ajax
        ]; // default setting
        if (isset($attrSettings[$key]['multiple'])) {
            $formSetting['multiple'] = $attrSettings[$key]['multiple'];
        }

        if (isset($attrSettings[$key]['wysiwyg'])) {
            $formSetting['wysiwyg'] = $attrSettings[$key]['wysiwyg'];
        }

        if (isset($attrSettings[$key]['htmldata'])) {
            $htmldata       = $attrSettings[$key]['htmldata'];
            $attributesdata = [];

            foreach ($htmldata as $key => $value) {
                $attributesdata['data-' . $key] = $value;
            }

            if ($attributes != null && sizeof($attributes) > 0) {
                if (isset($attributes['select-type'])) {
                    $formSetting['selecttype'] = $attributes['select-type'];
                    unset($attributes['select-type']);
                }
                $attributes = array_merge($attributes, $attributesdata);
            } else {
                $attributes = $attributesdata;
            }
        }

        if (isset($attrSettings[$key]['image_type'])) {
            $formSetting['image_type'] = $attrSettings[$key]['image_type'];
        }

        if (isset($attrSettings[$key]['image_zoom'])) {
            $formSetting['image_zoom'] = $attrSettings[$key]['image_zoom'];
        }

        if (isset($attrSettings[$key]['elmt_group'])) {
            $formSetting['elmt_group'] = $attrSettings[$key]['elmt_group'];
        }

        if (isset($attrSettings[$key]['form_field_relation'])) {
            $formSetting['form_field_relation'] = $attrSettings[$key]['form_field_relation'];
        }

        if ($attributes != null && sizeof($attributes) > 0) {
            if (isset($attributes['select-type'])) {
                $formSetting['selecttype'] = $attributes['select-type'];
                unset($attributes['select-type']);
            }
            $formSetting['attributes'] = $attributes;
        } else {
            $formSetting['attributes'] = [];
        }
        // construct
        $formSetting['id'] = "input".ucfirst(strtolower($key));
        $formSetting['masked_id'] = $formSetting['id']."_view";
        if (is_array($attrSettings) && $attrSettings[$key] != null) {
            $displayInForm = isset($attrSettings[$key]['formdisplay']) ? $attrSettings[$key]['formdisplay'] : false;
            if ($displayInForm) {
                $formSetting['container_id'] = $formSetting['id']."Container";
                $formSetting['name'] = "settings[" . $key . "]";
                $formSetting['label'] = (isset($attrSettings[$key]['label']) ? $attrSettings[$key]['label'] : '');
                if (settings($key, $defaultValue) == '' && isset($attrSettings[$key]['default_value'])) {
                    $formSetting['value'] = $attrSettings[$key]['default_value'];
                } else {
                    $formSetting['value'] = settings($key, $defaultValue);
                }
                $formSetting['group'] = (isset($attrSettings[$key]['group']) ? $attrSettings[$key]['group'] : false);
                $formSetting['options'] = [];
                if (isset($attrSettings[$key]['options']) &&
                    ((is_array($attrSettings[$key]['options']) &&
                      count($attrSettings[$key]['options']) > 0) ||
                     (is_a($attrSettings[$key]['options'], 'Illuminate\Support\Collection') &&
                      count($attrSettings[$key]['options']) > 0)) ) {
                    if (is_a($attrSettings[$key]['options'], 'Illuminate\Support\Collection')) {
                        $formSetting['options'] = $attrSettings[$key]['options']->toArray();
                    } else {
                        $formSetting['options'] = $attrSettings[$key]['options'];
                    }
                }
                $formSetting['required'] = (isset($attrSettings[$key]['required']) ? $attrSettings[$key]['required'] : false);
                $formSetting['readonly'] = (isset($attrSettings[$key]['readonly']) ? $attrSettings[$key]['readonly'] : false);
                $formSetting['action_handler_route'] = $uploadHandler;
                $formSetting['errors'] = ($errors ? $errors->first($key) : "");
                if ($this->isGroupSettingValid($formSetting)) {
                    // Group of Element
                    $this->addOrUpdateToGroupRendering($formSetting);
                    if ($this->isElmtGroupComplete($formSetting['elmt_group']['name'])) {
                        if ($formSetting['elmt_group']['type'] == SuitModel::GROUPTYPE_MAP) {
                            return View::make(SuitModel::$partialFormView[SuitModel::GROUPTYPE_MAP], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == SuitModel::GROUPTYPE_DATERANGE) {
                            return View::make(SuitModel::$partialFormView[SuitModel::GROUPTYPE_DATERANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == SuitModel::GROUPTYPE_DATETIMERANGE) {
                            return View::make(SuitModel::$partialFormView[SuitModel::GROUPTYPE_DATETIMERANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == SuitModel::GROUPTYPE_NUMERICRANGE) {
                            return View::make(SuitModel::$partialFormView[SuitModel::GROUPTYPE_NUMERICRANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == SuitModel::GROUPTYPE_TIMERANGE) {
                            return View::make(SuitModel::$partialFormView[SuitModel::GROUPTYPE_TIMERANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        }
                        return "";
                    }
                } else {
                    // Non-Group : process by type
                    if ($attrSettings[$key]['type'] == SuitModel::TYPE_NUMERIC) {
                        if (isset($attrSettings[$key]['options']) || isset($attrSettings[$key]['options_url'])) {
                            if ((isset($attrSettings[$key]['readonly']) &&
                                 $attrSettings[$key]['readonly']) ||
                                (isset($attrSettings[$key]['initiated']) &&
                                 $attrSettings[$key]['initiated'])){
                                // As Readonly InputText References
                                $formSetting['masked_value'] = "N/A";
                                $relatedReferences = $attrSettings[$key]['options'];
                                if (isset($relatedReferences[settings($key, $defaultValue)]))
                                    $formSetting['masked_value'] = $relatedReferences[settings($key, $defaultValue)];
                                else
                                    $formSetting['masked_value'] = ucfirst(strtolower(settings($key, $defaultValue)));
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                // As Dropdown List Options
                                if ($optionsAjaxHandler &&
                                    isset($optionsAjaxHandler[$key]) &&
                                    !empty($optionsAjaxHandler[$key])) {
                                    $formSetting['data_url'] = $optionsAjaxHandler[$key];
                                }
                                if ($formSetting['data_url']) {
                                    $formSetting['value_text'] = settings($key, $defaultValue);
                                }
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_SELECT].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        } else {
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_NUMERIC].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_FLOAT) {
                        // render
                        return View::make(SuitModel::$partialFormView[SuitModel::TYPE_FLOAT].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_BOOLEAN) {
                        // render
                        return View::make(SuitModel::$partialFormView[SuitModel::TYPE_BOOLEAN].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_DATETIME) {
                        if ((isset($attrSettings[$key]['readonly']) &&
                             $attrSettings[$key]['readonly']) ||
                            (isset($attrSettings[$key]['initiated']) &&
                             $attrSettings[$key]['initiated'])){
                            // As Readonly InputText References
                            $formSetting['masked_value'] = "N/A";
                            if (!isEmptyDate(settings($key, $defaultValue))) {
                                try {
                                    $formSetting['masked_value'] = Date::createFromTimestamp(strtotime(settings($key, $defaultValue)))->format("d F Y G:i");
                                } catch (Exception $e) { }
                            }
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        } else {
                            // As Datetime Input Options
                            if (isEmptyDate(settings($key, $defaultValue))) {
                                // $formSetting['value'] = date('Y-m-d H:i:s');
                            } else {
                                $formSetting['value'] = date("Y-m-d H:i:s", strtotime(settings($key, $defaultValue)));
                            }
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_DATETIME].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_DATE) {
                        if ((isset($attrSettings[$key]['readonly']) &&
                             $attrSettings[$key]['readonly']) ||
                            (isset($attrSettings[$key]['initiated']) &&
                             $attrSettings[$key]['initiated'])){
                            // As Readonly InputText References
                            $formSetting['masked_value'] = "N/A";
                            if (!isEmptyDate(settings($key, $defaultValue))) {
                                try {
                                    $formSetting['masked_value'] = Date::createFromTimestamp(strtotime(settings($key, $defaultValue)))->format("d F Y");
                                } catch (Exception $e) { }
                            }
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        } else {
                            // As Date Input Options
                            if (isEmptyDate(settings($key, $defaultValue))) {
                                // $formSetting['value'] = date('Y-m-d');
                            } else {
                                $formSetting['value'] = date("Y-m-d", strtotime(settings($key, $defaultValue)));
                            }
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_DATE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_TIME) {
                        if ((isset($attrSettings[$key]['readonly']) &&
                             $attrSettings[$key]['readonly']) ||
                            (isset($attrSettings[$key]['initiated']) &&
                             $attrSettings[$key]['initiated'])){
                            // As Readonly InputText References
                            $formSetting['masked_value'] = "N/A";
                            if (!isEmptyDate(settings($key, $defaultValue))) {
                                try {
                                    $formSetting['masked_value'] = date("G:i", strtotime(settings($key, $defaultValue)));
                                } catch (Exception $e) { }
                            }
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        } else {
                            // As Time Input Options
                            if (settings($key, $defaultValue) == '') {
                                // $formSetting['value'] = date('H:i');
                            } else {
                                $formSetting['value'] = date("H:i", strtotime(settings($key, $defaultValue)));
                            }
                            // render
                            return View::make(SuitModel::$partialFormView[SuitModel::TYPE_TIME].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_TEXTAREA) {
                        $formSetting['value'] = trim(settings($key, $defaultValue));
                        // render
                        $translationFormSetting = $formSetting;
                        $mainView = View::make(SuitModel::$partialFormView[SuitModel::TYPE_TEXTAREA].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                        return $mainView;
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_RICHTEXTAREA) {
                        $formSetting['value'] = trim(settings($key, $defaultValue));
                        // render
                        $translationFormSetting = $formSetting;
                        $mainView = View::make(SuitModel::$partialFormView[SuitModel::TYPE_RICHTEXTAREA].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                        return $mainView;
                    } else if ($attrSettings[$key]['type'] == SuitModel::TYPE_FILE) {
                        if (settings($key, $defaultValue)) {
                            if (is_array($this->imageAttributes) &&
                                count($this->imageAttributes) > 0 &&
                                in_array($key, array_keys($this->imageAttributes))) {
                                $formSetting['image_file_url'] = settings($key, $defaultValue);
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                $formSetting['file_url'] = $this->getFileAccessPath($key);
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        } else {
                            if (is_array($this->imageAttributes) &&
                                count($this->imageAttributes) > 0 &&
                                in_array($key, array_keys($this->imageAttributes))) {
                                $formSetting['image_file_url'] = 'nofile';
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                $formSetting['file_url'] = 'nofile';
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        }
                    } elseif ($attrSettings[$key]['type'] == SuitModel::TYPE_PASSWORD) {
                        // render
                        return View::make(SuitModel::$partialFormView[SuitModel::TYPE_PASSWORD].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                    } else {
                        // default is inputText
                        if (isset($attrSettings[$key]['options']) && is_array($attrSettings[$key]['options'])) {
                            if ((isset($attrSettings[$key]['readonly']) &&
                                 $attrSettings[$key]['readonly']) ||
                                (isset($attrSettings[$key]['initiated']) &&
                                 $attrSettings[$key]['initiated'])){
                                // As Readonly InputText References
                                $relatedReferences = $attrSettings[$key]['options'];
                                $formSetting['masked_value'] = "-";
                                if (isset($relatedReferences[settings($key, $defaultValue)]))
                                    $formSetting['masked_value'] = $relatedReferences[settings($key, $defaultValue)];
                                else
                                    $formSetting['masked_value'] = ucfirst(strtolower(settings($key, $defaultValue)));
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                // As Dropdown List Options
                                // render
                                return View::make(SuitModel::$partialFormView[SuitModel::TYPE_SELECT].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        } else {
                            // default is inputText without options
                            // render
                            $translationFormSetting = $formSetting;
                            $mainView = View::make(SuitModel::$partialFormView[SuitModel::TYPE_TEXT].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                            return $mainView;
                        }
                    }
                }
            }
        }
        // return empty form entry
        return "";
    }
}
