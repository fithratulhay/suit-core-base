<?php

namespace Suitcore\Repositories;

use Auth;
use DB;
use Hash;
use Mail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Mail\Message;
use Illuminate\Support\Str;
use Suitcore\Models\SuitUser;
use Suitcore\Accounts\SocialAccountInterface;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitUserRepository implements SuitUserRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(SuitUser $model) {
        $this->mainModel = $model;
    }

    protected function getMainColumnFromAccount(SocialAccountInterface $account) {
        $columns = [];

        if ($idKey = $account->getIdentifierColumn()) {
            $columns[$idKey] = $account->getId();
        }

        if ($tokenKey = $account->getTokenColumn()) {
            $columns[$tokenKey] = $account->getAccessToken();
        }

        if ($secretKey = $account->getSecretColumn()) {
            $columns[$secretKey] = $account->getSecret();
        }

        return $columns;
    }

    public function findByIdentifier($identifier) {
        if ($identifier) {
            return SuitUser::whereUsername($identifier)->orWhere('email', '=', $identifier)->first();
        }
        return false;
    }

    public function byIdentifier($identifier) {
        $email = $identifier;
        if (!is_email($email)) {
            $user = $this->findByIdentifier($identifier);
            if($user) {
                $email = $user->email;
            }
        }
        return SuitUser::whereEmail($email)->get();
    }

    /*
    public function createFromSocialAccount(SocialAccountInterface $account) {
        $identifierColumn = $account->getIdentifierColumn();

        //find by social account id
        $user = SuitUser::where($identifierColumn, '=', $account->getId())->first();

        // if not found, find by email
        if(!$user && $account->getEmail())
        {
            $user = SuitUser::where('email', '=', $account->getEmail())->first();
        }

        // if still not found, create new
        if(!$user)
        {
            $user = new User;
        }

        // if new user
        if (!$user->exists) {
            $user->username = ($account->getId() . "@" . ($account->getType() == "facebook" ? "facebook.com" : "twitter.com"));
            $user->name     = $account->getName();
            $user->email    = ($account->getEmail() ? $account->getEmail() : "");
            $user->type     = SuitUser::MEMBER;
            $user->status   = SuitUser::ACTIVATED;
            $user->password = Hash::make($account->getRandomPassword());
        }

        if (!$user->profile_image_url) {
            $user->profile_image_url  = $account->getPicture();
        }

        // set social account ID
        if (!$user->{$identifierColumn}) {
            $user->{$identifierColumn} = $account->getId();
        }

        if ($user->save()) {
            // Access Token
            if ($account->getTokenColumn())
            {
                $column = $account->getTokenColumn();
                $user->{$column} = $account->getAccessToken();
                $user->save();
            }
            // Secret
            if ($account->getSecretColumn())
            {
                $column = $account->getSecretColumn();
                $user->{$column} = $account->getSecret();
                $user->save();
            }
        }

        return $user;
    }
    */

    /**
     * @param SocialAccountInterface $account
     * @return SuitUser|null
     */
    public function createFromSocialAccount(SocialAccountInterface $account) {
        $columns = $this->getMainColumnFromAccount($account);

        // Find by social account or by email
        $user = $this->mainModel->where(function (Builder $query) use ($columns, $account) {
            foreach ($columns as $field => $value) {
                $query->orWhere($field, $value);
            }
            $query->orWhere('email', $account->getEmail());
        })->first();

        if (null !== $user) {
            // Just in case user login with different socmed credential but their email address is still registered.
            // Here we update their socmed credential if needed.
            $user->fill($columns);

            if ($user->isDirty() && $user->isValid('update')) {
                $user->save();
            }

            return $user;
        }

        $user = $this->mainModel->newInstance();

        // if new user
        if (!$user->exists) {
            $user->username = str_replace( ".", "_", str_replace("@", "_", $account->getEmail()) );
            $user->name     = $account->getName();
            $user->email    = $account->getEmail();
            $user->role     = SuitUser::USER;
            $user->status   = SuitUser::STATUS_ACTIVE;
            $user->password = Hash::make($password = $account->getRandomPassword());
        }

        if (! $user->picture) {
            $user->picture  = $account->getPicture();
        }

        foreach ($columns as $field => $value) {
            if (! $user->$field) {
                $user->$field = $value;
            }
        }

        if ($user->save()) {
            $this->postSocialMediaIntegration($account, $password);
            return $user;
        }

        return null;
    }

    /**
    * custom-definition
    * @param SocialAccountInterface $account
    * @param String $plainPassword Plain password
    **/
    protected function postSocialMediaIntegration($account, $plainPassword) {
        // custom-definition
    }

    public function deactivateSimilarAccount($user) {
        if (is_email($user->email)) {
            return User::whereEmail($user->email)->where('id', '<>', $user->id)->update(['status' => SuitUser::BANNED]);
        }
        return false;
    }

    public function facebookConnect($user, $accessToken, $facebookId, $facebookEmail) {
        $user->fb_access_token = $accessToken;
        $user->save();
    }

    /**
     * Somehow if needed such as email not same with username
     **/
    protected function generateUniqueUsername($email) {
        $temp = explode('@', $email);
        if (count($temp) >= 2) {
            list($username, $domain) = $temp;
        } else {
            $username = $temp[0];
        }
        $suffix = 0;
        $checkUsername = $username;
        do {
            $exist = SuitUser::where('username', '=', $checkUsername)->first();
            $suffix++;
            $checkUsername = $checkUsername . $suffix;
        } while($exist);
        $suffix--;
        if (0 == $suffix) {
            $suffix = '';
        }
        return $username . $suffix;
    }
}
