<?php

namespace Suitcore\Repositories;

use Exception;
use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitUserSetting;
use Suitcore\Repositories\Contract\SuitUserSettingRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitUserSettingRepository implements SuitUserSettingRepositoryContract
{
    use SuitRepositoryTrait;

    protected $mainModel = null;

    /**
     * Default Constructor
     **/
    public function __construct(SuitUserSetting $model)
    {
        $this->mainModel = $model;
    }

    /**
     * Update setting by key.
     * @param integer $userId
     * @param string $key
     * @param string $value
     * @return void
     */
    public function updateByKey($userId, $key, $value)
    {
        $baseModel = ($this->mainModel ? $this->mainModel->getNew() : new SuitUserSetting);
        $setting = $baseModel->firstOrNew(['user_id' => $userId, 'key' => $key]);
        $setting->value = $value;
        $result = $setting->save();
        if ($result) {
            // Begin Update Cache
            Cache::forever('user_setting_' . $setting->user_id, SuitUserSetting::forUser($setting->user_id)->pluck('value', 'key'));
            // Finish Update Cache
        }
    }

    /**
     * Get value of setting.
     * @param integer $userId
     * @param string $key
     * @param  string $default
     * @return string
     */
    public function getValue($userId, $key, $default = '')
    {
        $baseModel = ($this->mainModel ? $this->mainModel->getNew() : new SuitUserSetting);
        $setting = Cache::rememberForever('user_setting_' . $userId, function () use($baseModel, $userId) {
            return $baseModel->forUser($userId)->pluck('value', 'key');
        });
        return isset($setting[$key]) ? $setting[$key] : $default;
    }

    /**
     * Save settings
     * @param integer $userId
     * @param array $settingArray
     * @return boolean
     */
    public function save($userId, $settingArray, $fileList = [])
    {
        $result = false;
        if (is_array($settingArray)) {
            try {
                $settings = $settingArray;
                // checklist settings
                $definedSettings = SuitUserSetting::forUser($userId)->pluck('value', 'key');
                foreach ($definedSettings as $key => $value) {
                    if (!in_array($key, $fileList) &&
                        !isset($settings[$key]) &&
                        in_array($value, ['true', 'false']))
                        $settings[$key] = '';
                }
                // for array type settings
                $settings = collect($settings)->map(function ($item) {
                    return is_array($item) ? json_encode($item) : $item;
                })->toArray();
                // save
                foreach ($settings as $key => $value) {
                    $result = $this->updateByKey($userId, $key, ($value === null ? "" : $value));
                }
            } catch (Exception $e) { }
        }
        return $result;
    }
}
