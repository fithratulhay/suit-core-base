<?php

namespace Suitcore\Repositories;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Suitcore\Models\SuitUserPermission;
use Suitcore\Models\SuitUserRole;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitUserPermissionRepository implements SuitUserPermissionRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(SuitUserPermission $model)
    {
        $this->mainModel = $model;
    }

    public function refreshList() {
        Cache::forget('role_permissions');
    	$refreshList = $this->getCachedList();
    }

    public function getCachedList() {
    	return Cache::rememberForever('role_permissions', function() {
    		$dbPermission = SuitUserPermission::all();
    		$permissionList = [];
    		foreach ($dbPermission as $key => $obj) {
    			if (!isset($permissionList[$obj->role_id])) {
    				$permissionList[$obj->role_id] = [];
    			}
    			$permissionList[$obj->role_id][$obj->route_name] = $obj->id . ":" . $obj->status;
    		}
    		return $permissionList;
        });
    }

    public function getRawRoutes() {
    	$routeRawCollection = Route::getRoutes()->getRoutesByName();
        $routeCollection = [];
        foreach($routeRawCollection as $item) {
            $routeName = $item->getName();
            $routeMethod = $item->methods()[0];

            if ((substr($routeName, 0, strlen('backend')) === 'backend') &&
                (strtolower($routeMethod) == "get" ||
                 (strtolower($routeMethod) == "post" &&
                  (substr($routeName, (-1 * strlen('delete'))) === 'delete' ||
                   substr($routeName, (-1 * strlen('json'))) != 'json'))
                ) &&
                strpos($routeName, 'json') === false &&
                strpos($routeName, 'importprogress') === false &&
                strpos($routeName, 'backend.sessions') === false
            ) {
                // start with 'backend'
                // http get or (http post that end with 'delete' or not end with 'json')
                // not contain word 'json'
                $routeCollection[$routeName] = $item;
            }
        }
        //ksort($routeCollection);
        return $routeCollection;
    }

    public function getPermissionByRoleId($id, $routeName = null) {
    	$role = SuitUserRole::find($id);
    	if ($role) {
    		if ($routeName) {
    			return $role->permissions->where('route_name', $routeName)->first();
    		}
    		return $role->permissions;
    	}
    	return null;
    }

    public function getPermissionByRoleCode($code, $routeName = null) {
    	$role = SuitUserRole::where('code', '=', $code)->first();
    	if ($role) {
    		if ($routeName) {
    			return $role->permissions->where('route_name', $routeName)->first();
    		}
    		return $role->permissions;
    	}
    	return null;
    }
}
