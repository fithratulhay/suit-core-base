<?php

namespace Suitcore\Repositories;

use Suitcore\Models\SuitUserLog;
use Suitcore\Repositories\Contract\SuitUserLogRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;


class SuitUserLogRepository implements SuitUserLogRepositoryContract
{
    use SuitRepositoryTrait;

    protected $mainModel = null;

    /**
     * Default Constructor
     **/
    public function __construct(SuitUserLog $model)
    {
        $this->mainModel = $model;
    }

    public function getExcludedRouteNames() {
        return config('suitcore.user_activity_logging.excluded_route_name');
    }
}
