<?php

namespace Suitcore\Repositories\Traits;

use Exception;
use ReflectionClass;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use Suitcore\Datatable\DatatableTrait;
use Suitcore\Jobs\AsyncRepositoryExecution;
use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitTranslation;
use Suitcore\Repositories\Traits\RepositoryAsObserverTrait;
use Suitcore\Uploader\Facades\Upload;
use Suitcore\FileGrabber\Facades\FileGrab;

trait SuitRepositoryTrait
{
    use DispatchesJobs;
    use DatatableTrait;
    use RepositoryAsObserverTrait;

    protected $mainModel = null;
    protected $dependencies = [];
    protected $repoQueueName = null;

    // METHODS
    /**
     * Set Base/Custom Main Model for related repository on runtime
     * @param SuitModel Custom Model
     **/
    public function setMainModel(SuitModel $model) {
        $this->mainModel = $model;
    }

    /**
     * Get Base/Custom Main Model for related repository on runtime
     * @param SuitModel Custom Model
     **/
    public function getMainModel() {
        return $this->mainModel;
    }

    /**
    * Make a new instance of the entity to query on
    *
    * @param array $with
    */
    public function make(array $with = array())
    {
        return $this->mainModel->with($with);
    }

    /**
    * Retrieve all entities
    *
    * @param array $with
    * @return Illuminate\Database\Eloquent\Collection
    */
    public function all(array $with = array())
    {
        $entity = $this->make($with);

        return $entity->get();
    }

    /**
    * Find a single entity
    *
    * @param int $id
    * @param array $with
    * @return Illuminate\Database\Eloquent\Model
    */
    public function find($id, array $with = array())
    {
        $entity = $this->make($with);

        return $entity->find($id);
    }

    /**
    * Get Results by Page
    *
    * @param int $page
    * @param int $limit
    * @param array $with
    * @return StdClass Object with $items and $totalItems for pagination
    */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        return $this->make($with)->latest()->paginate($limit);
    }

    /**
    * Search for many results by key and value
    *
    * @param string $key
    * @param mixed $value
    * @param array $with
    * @return Illuminate\Database\Query\Builders
    */
    public function getBy($key, $value, array $with = array())
    {
        return $this->make($with)->whereIn($key, (array) $value)->get();
    }

    /**
     * Display object list as JSON text that suitable for datatable frontend needs
     * @param  array $param Datatable Parameter Request
     * @param  array $columnFormatted Array that represent custom cell formatting
     * @param  array $specificFilter Specific column-key based filter (AND Filter)
     * @param  array $optionalFilter Optional column-key based filter (OR Filter)
     * @param  array $columnException Column-key exception, force to hide these column, ignore attribute-settings
     * @param  closure $datatableExtendedAndQuery Closure function to extends SQL Query conditional statement
     * @param  boolean $selectionExist True means using selection column (multiple-checklist), otherwise hide selection column
     * @param  closure $rowBodyAdapterClosure Closure function adapter to render row body, ex. function($obj, $columnFormatted, $specificFilter, $columnException) { }
     * @param  closure $rowMenuAdapterClosure Closure function adapter to render row action menu, ex. function($obj, $columnFormatted) { }
     * @return string jsonOutput that suitable for datatable.js, can used for other representation other than datatable
     **/
    public function jsonDatatable($param, $columnFormatted = null, $specificFilter = null, $optionalFilter = null, $columnException = null, $datatableExtendedAndQuery = null, $selectionExist = false, $rowBodyAdapterClosure = null, $rowMenuAdapterClosure = null) {
        // Selection Column
        SuitModel::$isFormGeneratorContext = false;
        $tmpObject = ($this->mainModel ? $this->mainModel : new SuitModel);
        $tmpObject->showAllOptions = true;
        // $object = $tmpObject->select($tmpObject->getTable().".*");
        $object = $this->getSpecificUserLoggedData($tmpObject);
        $datatableSelection = [];
        $datatableExtendedSelection = [];
        $datatableKeyIndex = [];
        $datatableColumnRelationObject = [];
        $datatableColumnOptions = [];
        $datatableDateOptions = [];
        $columFilterIdx = ($selectionExist ? 1 : 0);

        foreach ($tmpObject->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
            /*
            if ($attrSettings['visible']) {
                if ($specificFilter == null ||
                    !is_array($specificFilter) ||
                    !isset($specificFilter[$attrName])) {
                    $datatableSelection[] = $tmpObject->getTable().'.'.$attrName;
                    $columFilterIdx++;
                }
            }
            */
            if (isset($attrSettings['options']) && is_string($attrSettings['options']) && !empty($attrSettings['options'])
                && (!isset($attrSettings['relation']) || (isset($attrSettings['filterable']) && $attrSettings['filterable']))) {
                try {
                    $tmpBuffer = $tmpObject->renderFormView($attrName);
                    $attrSettings['options'] = $tmpObject->getBufferedAttributeSettings()[$attrName]['options']; // refresh
                } catch (\Exception $e) { }
            }

            if (isset($attrSettings['visible']) &&
                $attrSettings['visible'] &&
                ($specificFilter == null ||
                 !is_array($specificFilter) ||
                 !isset($specificFilter[$attrName])) &&
                ($columnException == null ||
                 !is_array($columnException) ||
                 !in_array($attrName, $columnException))) {
                // selection
                $datatableSelection[$columFilterIdx] = $tmpObject->getTable().'.'.$attrName;
                // filter
                if (isset($attrSettings['filterable']) &&
                    $attrSettings['filterable']) {
                    $datatableKeyIndex[$columFilterIdx] = $attrName;
                    $datatableColumnRelationObject[$columFilterIdx] = isset($attrSettings['relation']) ? $attrSettings['relation'] : null;
                    $datatableColumnOptions[$columFilterIdx] = isset($attrSettings['options']) ? $attrSettings['options'] : null;
                    $datatableDateOptions[$columFilterIdx] = in_array($attrSettings['type'], [SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE]);
                    // for boolean filter
                    if ($attrSettings['type'] == SuitModel::TYPE_BOOLEAN) {
                        $datatableColumnOptions[$columFilterIdx] = [
                            0 => "No",
                            1 => "Yes"
                        ];
                    }
                }
                // extended selection
                if (isset($attrSettings['relation']) &&
                    $attrSettings['relation']) {
                    $datatableExtendedSelection[$attrName] = $tmpObject->getAttribute($attrSettings['relation'].'__object');
                    if (!$datatableExtendedSelection[$attrName]) unset($datatableExtendedSelection[$attrSettings['relation']]);
                }
                // next field
                $columFilterIdx++;
            }
        }

        // YADCF Column Specific Filter
        $specificDefinition = [];
        $tmpValue = "";
        $columnFilter = isset($param["columns"]) ? $param["columns"] : [];
        foreach ($columnFilter as $key => $element) {
            if (isset($element["search"]["value"]) &&
                !empty($element["search"]["value"]) &&
                isset($datatableSelection[$key]) ) {
                // Specific Column Filter
                $tmpValue = $element["search"]["value"];
                if (isset($datatableColumnRelationObject[$key]) &&
                    $datatableColumnRelationObject[$key]) {
                    $objProperty = $datatableColumnRelationObject[$key]."__object";
                    $relatedObject = $tmpObject->$objProperty;
                    if ($relatedObject) {
                        $relatedObject = $relatedObject->where($relatedObject->getUniqueValueColumn(),"=",$tmpValue)->first();
                        if ($relatedObject) {
                            $specificDefinition[$datatableSelection[$key]] = $relatedObject->id;
                        }
                    }
                } else if (isset($datatableColumnOptions[$key]) &&
                           is_array($datatableColumnOptions[$key]) &&
                           count($datatableColumnOptions[$key]) > 0) {
                    $optionKey = array_search($tmpValue, $datatableColumnOptions[$key]);
                    $specificDefinition[$datatableSelection[$key]] = $optionKey;
                } else if ( isset( $datatableDateOptions[$key] ) &&
                    $datatableDateOptions[$key] ) {
                    $specificDefinition[$datatableSelection[$key]] = $tmpValue;
                }
            }
        }

        if ($specificFilter && is_array($specificFilter)) $specificDefinition = array_merge($specificDefinition, $specificFilter);

        // Process Datatable Request
        $jsonSource = $this->preprocessDatatablesJson($object,
                             $datatableSelection,
                             $specificDefinition,
                             $optionalFilter,
                             $tmpObject->_defaultOrder,
                             $tmpObject->_defaultOrderDir,
                             $datatableExtendedSelection,
                             $datatableExtendedAndQuery);

        // Complete json, set data (view rendered) and unset rawdata (model rendered)
        $jsonSource['data'] = [];
        foreach($jsonSource['rawdata'] as $obj) {
            $tmpRow = [];
            // Selection Tools
            $selectedIds = (is_array($columnFormatted) && isset($columnFormatted['selectedIds']) && is_array($columnFormatted['selectedIds']) ? $columnFormatted['selectedIds'] : []);
            if (is_array($columnFormatted) && isset($columnFormatted['selection'])) {
                try {
                    if (empty($columnFormatted['selection'])) {
                        $tmpRow[] = '-';
                    }
                    $selectionElmt = str_replace('#id#', $obj->getAttribute('id'), $columnFormatted['selection']);
                    if (in_array($obj->getAttribute('id'), $selectedIds)) {
                        $selectionElmt = str_replace('#checked#', 'checked', $selectionElmt);
                    } else {
                        $selectionElmt = str_replace('#checked#', '', $selectionElmt);
                    }
                    $tmpRow[] = $selectionElmt;
                } catch (Exception $e) {
                    $tmpRow[] = '-';
                }
            }
            // Data Body
            if ($rowBodyAdapterClosure) {
                try {
                    $tmpRow = array_merge($tmpRow, $rowBodyAdapterClosure($obj, $columnFormatted, $specificFilter, $columnException));
                } catch (\Exception $e) { 
                    foreach ($tmpObject->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                        if (isset($attrSettings['visible']) &&
                            $attrSettings['visible'] &&
                            ($specificFilter == null ||
                             !is_array($specificFilter) ||
                             !isset($specificFilter[$attrName])) &&
                            ($columnException == null ||
                             !is_array($columnException) ||
                             !in_array($attrName, $columnException))) {
                            $tmpRow[] = $obj->renderAttribute($attrName, $columnFormatted);
                        }
                    }
                }
            } else {
                foreach ($tmpObject->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                    if (isset($attrSettings['visible']) &&
                        $attrSettings['visible'] &&
                        ($specificFilter == null ||
                         !is_array($specificFilter) ||
                         !isset($specificFilter[$attrName])) &&
                        ($columnException == null ||
                         !is_array($columnException) ||
                         !in_array($attrName, $columnException))) {
                        $tmpRow[] = $obj->renderAttribute($attrName, $columnFormatted);
                    }
                }
            }
            // Action Menu
            if ($rowMenuAdapterClosure) {
                try {
                    $tmpRow = array_merge($tmpRow, $rowMenuAdapterClosure($obj, $columnFormatted));
                } catch (\Exception $e) { 
                    if (is_array($columnFormatted) && isset($columnFormatted['menu'])) {
                        try {
                            if (empty($columnFormatted['menu'])) {
                                $tmpRow[] = '-';
                            }
                            $tmpRow[] = str_replace("##object_label##", ($obj->getTranslationLabel() . " " . $obj->getFormattedValue()), str_replace('#id#', $obj->getAttribute('id'), $columnFormatted['menu']) );
                        } catch (Exception $e) {
                            $tmpRow[] = '-';
                        }
                    }
                }
            } else {
                if (is_array($columnFormatted) && isset($columnFormatted['menu'])) {
                    try {
                        if (empty($columnFormatted['menu'])) {
                            $tmpRow[] = '-';
                        }
                        $tmpRow[] = str_replace("##object_label##", ($obj->getTranslationLabel() . " " . $obj->getFormattedValue()), str_replace('#id#', $obj->getAttribute('id'), $columnFormatted['menu']) );
                    } catch (Exception $e) {
                        $tmpRow[] = '-';
                    }
                }
            }
            // Add Row
            $jsonSource['data'][] = $tmpRow;
        }
        unset($jsonSource['rawdata']);

        // YADCF Column Specific Filter Options
        foreach ($columnFilter as $key => $element) {
            if (isset($datatableColumnRelationObject[$key]) &&
                $datatableColumnRelationObject[$key]) {
                // for attributes with relationship
                $objProperty = $datatableColumnRelationObject[$key]."__object";
                $relatedObject = $tmpObject->$objProperty;
                $attrSettings = $tmpObject->attribute_settings;
                if ($relatedObject) {
                    if (isset($datatableKeyIndex[$key]) &&
                        isset($attrSettings[$datatableKeyIndex[$key]]) &&
                        isset($attrSettings[$datatableKeyIndex[$key]]['options']) &&
                        !empty($attrSettings[$datatableKeyIndex[$key]]['options']) ) {
                        $optionSources = $attrSettings[$datatableKeyIndex[$key]]['options'];
                        if (is_string($attrSettings[$datatableKeyIndex[$key]]['options'])) {
                            $optionSources = [];
                            if (isset($datatableColumnOptions[$key]) &&
                                is_array($datatableColumnOptions[$key]) &&
                                count($datatableColumnOptions[$key]) > 0) {
                                $optionSources = $datatableColumnOptions[$key];
                            }
                        }
                        foreach ($optionSources as $value => $label) {
                            $jsonSource['yadcf_data_'.$key][] = [
                                'value' => $value,
                                'label' => $label
                            ];
                        }
                    } else {
                        // $optionSources = $relatedObject->all()->pluck('default_name', $relatedObject->getUniqueValueColumn());
                        $jsonSource['yadcf_data_'.$key]  = [];

                        $passedValue = null;
                        $passedLabel = "";
                        try {
                            $passedValue = $specificDefinition[ $datatableSelection[$key] ];
                            $passedLabel = $relatedObject->find($passedValue)->getFormattedValue();
                        } catch (Exception $e) { }

                        if ($passedValue) {
                            $jsonSource['yadcf_data_'.$key][] = [
                                'value' => $passedValue,
                                'label' => $passedLabel
                            ];
                        }
                    }
                }
            } else if (isset($datatableColumnOptions[$key]) &&
                       is_array($datatableColumnOptions[$key]) &&
                       count($datatableColumnOptions[$key]) > 0) {
                // not relationship with options input
                $jsonSource['yadcf_data_'.$key] = array_values($datatableColumnOptions[$key]);
            }
        }

        // Return JSON Response
        return response()->json($jsonSource);
    }

    protected function getSpecificUserLoggedData($tmpObject)
    {
        $user = auth()->user();
        $tmpObject->showAllOptions = true;
        $object = $tmpObject->select($tmpObject->getTable().".*");
        if ($user && $user->role != 'admin' && property_exists($tmpObject, 'user_id')) {
            $object = $tmpObject->select($tmpObject->getTable().".*")->where('user_id', $user->id);
        }

        return $object;
    }

    /**
     * Get object detail
     * @param  int $objectId
     * @return array Object Detail
     **/
    public function get($objectId)
    {
        $object = ($this->mainModel ? $this->mainModel->find($objectId) : SuitModel::find($objectId));
        return [
            'object' => $object
        ];
    }

    protected function doUpload(&$object)
    {
        Upload::setFilenameMaker(function ($file, $object) {
            $title = $object->getFormattedValue();
            return str_limit(str_slug(md5(Uuid::uuid4()->toString()).'-'.date('YmdHis').'-'.$title), 92) . '.' . $file->getClientOriginalExtension();
        }, $object);
        try {
            Upload::model($object, false);
        } catch (\Exception $e) {
            $object->uploadError = $e;
            // info("error doupload : " . $e->getMessage());
        }

        if ($object instanceof SuitModel) {
            foreach ($object->getImageAttributes() as $field => $value) {
                if ($object->getAttribute($field) !== null) {
                    $tmpValue = $object->getAttribute($field);
                    if (starts_with($tmpValue, 'http://') || starts_with($tmpValue, 'https://')) {
                        try {
                            $url = trim($tmpValue);
                            $someFile = FileGrab::from($url);
                            if ($someFile) {
                                $filename = str_limit(str_slug(md5(Uuid::uuid4()->toString()).'-'.date('YmdHis').'-'.$object->getFormattedValue()), 92);
                                try {
                                    $filename .= '.' . $someFile->guessExtension();
                                } catch (\Exception $e1) { }
                                $someFile->move($object->getFolderPath($field), $filename);
                                $object->{$field} = $filename;
                            }
                        } catch (\Exception $e2) { 
                            unset($object->{$field});
                        }
                    } else {
                        $folder = $object->getFolderPath($field).DIRECTORY_SEPARATOR;
                        $image = substr($object->getImagePath($field), strlen($folder));
                        try {
                            if ($file = Image::make($image)) {
                                try {
                                    $title = $object->getFormattedValue();
                                    $name = str_limit(str_slug(md5(Uuid::uuid4()->toString()) . '-' . $object->id .'-'.date('YmdHis').'-'.$title), 92) .'.png';
                                    // $name = md5($object->getFormattedValue().Carbon::now()).'.png';
                                    if (!File::exists($folder)) File::makeDirectory($folder, 0755, true);
                                    $file->save($folder.$name);
                                    $object->{$field} = $name;
                                    // info([$folder.$name, file_exists($folder.$name)]);
                                } catch (\Exception $e) {
                                    unset($object->{$field});
                                    // info("error doupload : " . $e->getMessage());
                                }
                            }
                        } catch (\Exception $e) {
                            // info("error doupload : " . $e->getMessage());
                        }
                    }
                } else {
                    unset($object->{$field});
                }
            }
        }
    }

    /**
     * Create object with attributes in $param and return created object
     * to $object and as function result
     * @param  array $param
     * @param  SuitModel $object
     * @param  array $acceptedParam Accepted parameter (filter)
     * @param  array $forcedDefaultParam Forced default parameter
     * @return SuitModel Created Object
     **/
    public function create($param, SuitModel &$object = null, $acceptedParam = [], $forcedDefaultParam = []) {
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->getNew() : new SuitModel);
        $baseAttr = $object->getAttributeSettings();
        // if (method_exists($object, 'sluggable')) {
        //     if (!empty($param['slug'])) {
        //         $param['slug'] = str_slug($param['slug']);
        //     } else {
        //         $source = $object->sluggable();
        //         $slugSource = $source['slug']['source'];
        //         $param['slug'] = str_slug($param[$slugSource]);
        //     }
        // }

        if (!empty($acceptedParam)) {
            // accepted parameter
            $param = array_only($param, $acceptedParam);
        }
        if (!empty($forcedDefaultParam)) {
            // default parameter
            $param = array_merge($param, $forcedDefaultParam);
        }
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0') $param[$key] = null;
            else{
                $param[$key] = purify_input_xss($val);
            }
        }
        if (!$object->isValid('create', null, $param)) {
            return false;
        }
        $object->fill($param);

        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                SuitTranslation::create([
                                    'class' => $object->nodeFullClassName,
                                    'identifier' => $object->id,
                                    'attribute' => $attrName,
                                    'locale' => $normalizedLang,
                                    'value' => $param[$paramKey]
                                ]);
                            }
                        }
                    }
                }
            }
        }

        return $object;
    }

    /**
     * Update object that defined by identifier $id
     * with attributes in $param and return updated object
     * to $object and as function result
     * @param  integer $id
     * @param  array $param
     * @param  SuitModel $object
     * @param  array $acceptedParam Accepted parameter (filter)
     * @param  array $forcedDefaultParam Forced default parameter
     * @return SuitModel Updated Object
     **/
    public function update($id, $param, SuitModel &$object = null, $acceptedParam = [], $forcedDefaultParam = []) {
        /** @var SuitModel $object */
        SuitModel::$isFormGeneratorContext = false;
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object == null) return false;
        $baseAttr = $object->getAttributeSettings();

        if (!empty($acceptedParam)) {
            // accepted parameter
            $param = array_only($param, $acceptedParam);
        }
        if (!empty($forcedDefaultParam)) {
            // default parameter
            $param = array_merge($param, $forcedDefaultParam);
        }

        $deletedFieldFiles = [];
        foreach($param as $key=>$val) {
            if (empty($val) && $val != '0' && $val != '0.0'){
                $param[$key] = null;
            }
            else if (starts_with($key, 'delete_file__') && $val == 'on') {
                $deletedFieldFiles[] = str_replace('delete_file__', '', $key);
            }
            else{
                $param[$key] = purify_input_xss($val);
            }
        }
        if (!$object->isValid('update', null, $param)) {
            return false;
        }
        $object->fill($param);

        Upload::cleanUploaded($object, $deletedFieldFiles, false);
        $this->doUpload($object);
        $result = $object->save();
        if ($result) {
            // saving translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $locale = strtolower(config('app.fallback_locale', 'en'));
                $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $locale));
                foreach ($baseAttr as $attrName=>$attrSettings) {
                    foreach ($localeOptions as $lang) {
                        $normalizedLang = strtolower($lang);
                        if ($normalizedLang != $locale) {
                            $paramKey = $attrName.'_trans_'.$normalizedLang;
                            if (isset($attrSettings['translation']) &&
                                $attrSettings['translation'] &&
                                isset($param[$paramKey]) &&
                                !empty($param[$paramKey])) {
                                $currentTranslation = SuitTranslation::where([
                                    'class' => $object->nodeFullClassName,
                                    'identifier' => $object->id,
                                    'attribute' => $attrName,
                                    'locale' => $normalizedLang
                                ])->first();
                                if ($currentTranslation) {
                                    $currentTranslation->update([
                                        'value' => $param[$paramKey]
                                    ]);
                                } else {
                                    $currentTranslation = SuitTranslation::create([
                                        'class' => $object->nodeFullClassName,
                                        'identifier' => $object->id,
                                        'attribute' => $attrName,
                                        'locale' => $normalizedLang,
                                        'value' => $param[$paramKey]
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $object;
    }

    public function delete($id, SuitModel &$object = null) {
        $object = ($this->mainModel ? $this->mainModel->find($id) : SuitModel::find($id));
        if ($object == null) return false;
        // Delete Object
        $result = $object->delete();
        if ($result) {
            // deleting translation if needed
            if (env('APP_MULTI_LOCALE', false)) {
                $allTranslationDeleted = SuitTranslation::where([
                    'class' => $object->nodeFullClassName,
                    'identifier' => $object->id
                ])->delete();
            }
        }
        return $result;
    }

    public function deleteAll($ids) {
        $result = false;
        if (is_array($ids) && count($ids) > 0) {
            $object = ($this->mainModel ? $this->mainModel : new SuitModel);
            // Delete Object
            $result = $object->destroy($ids);
            // $result = $object->whereIn($object->getKeyName(), $ids)->delete();
            if ($result > 0) {
                // deleting all translation if needed
                if (env('APP_MULTI_LOCALE', false)) {
                    $allTranslationDeleted = SuitTranslation::where('class', $object->nodeFullClassName)->whereIn('identifier', $ids)->delete();
                }
            }
        }
        return $result;
    }

    public function __call($name, $arguments)
    {
        if (ends_with($name, '__cached')) {
            $actualMethodName = str_replace('__cached', '', $name);
            try {
                // Return Cached Data if any
                $requestKey = $this->generateCacheKey([
                    'class' => get_class($this),
                    'method' => $actualMethodName, 
                    'signature' => $arguments
                ]);
                $cachedData = $this->getCachedData($requestKey);
                if ($cachedData !== null) return $cachedData;

                // Process (if cache missed)
                $result = $this->{$actualMethodName}(...$arguments);

                // Cache The Result and Return
                return $this->cacheResult($requestKey, $result);
            } catch (\Exception $e) { }
            return $this->{$actualMethodName}(...$arguments);
        } elseif (ends_with($name, '__dispatched')) {
            $actualMethodName = str_replace('__dispatched', '', $name);
            try {
                // send job to specified queue
                $targetQueue = $this->repoQueueName;
                $repoFullClassName = get_called_class();
                if (empty($targetQueue)) $targetQueue = config('suitcore.default_repository_queue', 'default');
                $this->dispatch((new AsyncRepositoryExecution($repoFullClassName, $actualMethodName, $arguments))->onQueue($targetQueue));
                return false; // pending result if invoker expected for result
            } catch (\Exception $e) { }
            return $this->{$actualMethodName}(...$arguments);
        }
        return $this->{$name}(...$arguments);
    }

    /**
     *
     * Get Filtered Object List/Single/Raw Query Builder
     *
     * Sample Parameter without condition and relationship
     *   attribute_name : "attribute_name"
     *   attribute_name : "attribute_name1,attribte_name2,dll"
     * Sample Parameter with condition
     *   _max_attribute_name : "max" "attribute_name"
     * Sample Parameter with relationship
     *   _relationshipName_attribute_name : "relationshipName" "attribute_name"
     * Sample Parameter with condition and relationship
     *   _max__relationshipName_attribute_name : "max" "relationshipName" "attribute_name"
     *
     * Defined Parameter
     *
     * keyword : keyword pattern separated by spaces
     * orderBy : multiple ordering column
     * orderType : multiple ordering column
     * paginate : false / true
     * perPage : number : -1 (all), 1, 2, 10, etc
     * if paginate false and perPage 1 then its single object, return first() from model collection
     *
     * @param  array  $param
     * @param  array  $setting
     * @param  boolean $cached
     * @param  boolean $translationSearch
     *
     * @return SuitModel|SuitModel[]|\Illuminate\Database\Eloquent\Collection
     **/
    public function getByParameter($param, $setting = [], $cached = false)
    {
        // Check Settings
        $defaultSetting = [
            'optional_dependency' => null,
            'optional_hidden_visibility' => null,
            'extended_raw_select' => null,
            'extended_raw_condition' => null,
            'extended_raw_order' => null,
            'extended_groupby' => null,
            'raw_result' => false
        ];
        $setting = array_merge($defaultSetting, $setting);
        if ($setting['optional_dependency'] && is_array($setting['optional_dependency'])) {
            $setting['optional_dependency'] = implode(',', $setting['optional_dependency']);
        }
        
        // Determine key of this request
        $requestKey = $this->generateCacheKey(array_merge([
            'baseClass' => get_class($this->mainModel)
        ], $param, $setting));
        // info("generated cache-key for " . get_class($this->mainModel) . " : " . $requestKey);

        // Get Cached data if intended
        if ($cached) {
            $cachedData = $this->getCachedData($requestKey);
            if ($cachedData !== null) return $cachedData;
        }

        // Base Object
        SuitModel::$isFormGeneratorContext = false;
        $baseObject = ($this->mainModel ? $this->mainModel : new SuitModel);
        $baseAttr = $baseObject->getAttributeSettings();
        $model = null;

        // Standard Select
        $model = $baseObject->select($baseObject->getTableName().".*");

        // Extended Query Select
        if ($setting['extended_raw_select']) {
            $model = $model->selectRaw($setting['extended_raw_select']);
        }

        // Populate default paramater
        $default = [
            'keyword' => false,
            'orderBy' => 'created_at',
            'orderType' => 'desc',
            'paginate' => true,
            'perPage' => static::DEFAULT_FETCH
        ];
        foreach ($baseAttr as $key=>$value) {
            $default[$key] = false;
        }

        // Merge Filter Parameter
        $param = array_merge($default, $param);

        // Process Filter
        $model = $baseObject->with(($setting['optional_dependency'] ? explode(",", $setting['optional_dependency']) : $this->dependencies)); // use default dependency if optional dependency not exist
        foreach ($param as $key=>$value) {
            $realAttrName = $key;
            $conditional = "";
            $relationName = "";
            // get conditional and real attribute name
            // IF MATCHED : _max_ , _min_ , _similar_
            if (starts_with($key, '_max_')) {
                $realAttrName = str_replace("_max_", "", $key);
                $conditional = 'max';
            } elseif (starts_with($key, '_min_')) {
                $realAttrName = str_replace("_min_", "", $key);
                $conditional = 'min';
            } elseif (starts_with($key, '_similar_')) {
                $realAttrName = str_replace("_similar_", "", $key);
                $conditional = 'similar';
            } elseif (starts_with($key, '_isnull_')) {
                $realAttrName = str_replace("_isnull_", "", $key);
                $conditional = 'isnull';
            }
            // get conditional from relationship attributes
            // IF MATCHED : _[relationshipName]_
            if (starts_with($realAttrName, '_')) {
                // if '_' exist then process relationship
                $attrNameComponents = explode('_', '#' . $realAttrName);
                if (count($attrNameComponents) > 2) {
                    // second components is relationship name
                    $relationName = $attrNameComponents[1];
                    $realAttrName = str_replace("_" . $relationName . "_", "", $realAttrName);
                }
            }
            // build condition to query builder
            if (!empty($relationName)) {
                // if relatiohship exist
                // indirect condition, within relationship
                if (method_exists($baseObject, $relationName)) {
                    $className = (get_class($baseObject->{$relationName}()->getRelated()));
                    $reflection = new $className();
                    $relationBaseAttr = $reflection->getAttributeSettings();
                    if (isset($relationBaseAttr[$realAttrName]) && ($value || (is_numeric($value) && intval($value) === 0))) {
                        if ($conditional == 'max' &&
                            isset($relationBaseAttr[$realAttrName]["type"]) &&
                            in_array($relationBaseAttr[$realAttrName]["type"], [SuitModel::TYPE_NUMERIC, SuitModel::TYPE_FLOAT, SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE, SuitModel::TYPE_TIME])) {
                            // maximum value
                            $model = $model->whereHas($relationName, function ($query) use ($realAttrName, $value) {
                                $query->where($realAttrName, '<=', $value);
                            });
                        } elseif ($conditional == 'min' &&
                            isset($relationBaseAttr[$realAttrName]["type"]) &&
                            in_array($relationBaseAttr[$realAttrName]["type"], [SuitModel::TYPE_NUMERIC, SuitModel::TYPE_FLOAT, SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE, SuitModel::TYPE_TIME])) {
                            // minimum value
                            $model = $model->whereHas($relationName, function ($query) use ($realAttrName, $value) {
                                $query->where($realAttrName, '>=', $value);
                            });
                        } elseif ($conditional == 'similar' &&
                            isset($relationBaseAttr[$realAttrName]["type"]) &&
                            in_array($relationBaseAttr[$realAttrName]["type"], [SuitModel::TYPE_TEXT, SuitModel::TYPE_TEXTAREA, SuitModel::TYPE_RICHTEXTAREA, SuitModel::TYPE_FILE])) {
                            // have similar value
                            $model = $model->whereHas($relationName, function ($query) use ($realAttrName, $value) {
                                $query->where($realAttrName, 'LIKE', '%' . $value . '%');
                            });
                        } elseif ($conditional == 'isnull') {
                            // check null
                            $model = $model->whereHas($relationName, function ($query) use ($realAttrName) {
                                $query->whereNull($realAttrName);
                            });
                        } else {
                            // exact match value
                            $model = $model->whereHas($relationName, function ($query) use ($realAttrName, $value) {
                                $values = explode(',', $value);
                                $query->whereIn($realAttrName, $values);
                            });
                        }
                    }
                }
            } else {
                // direct condition
                // add conditional to query builder
                if (isset($baseAttr[$realAttrName]) && ($value || (is_numeric($value) && intval($value) === 0))) {
                    if ($conditional == 'max' &&
                        isset($baseAttr[$realAttrName]["type"]) &&
                        in_array($baseAttr[$realAttrName]["type"], [SuitModel::TYPE_NUMERIC, SuitModel::TYPE_FLOAT, SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE, SuitModel::TYPE_TIME])) {
                        // maximum value
                        $model = $model->where($realAttrName, '<=', $value);
                    } elseif ($conditional == 'min' &&
                        isset($baseAttr[$realAttrName]["type"]) &&
                        in_array($baseAttr[$realAttrName]["type"], [SuitModel::TYPE_NUMERIC, SuitModel::TYPE_FLOAT, SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE, SuitModel::TYPE_TIME])) {
                        // minimum value
                        $model = $model->where($realAttrName, '>=', $value);
                    } elseif ($conditional == 'similar' &&
                        isset($baseAttr[$realAttrName]["type"]) &&
                        in_array($baseAttr[$realAttrName]["type"], [SuitModel::TYPE_TEXT, SuitModel::TYPE_TEXTAREA, SuitModel::TYPE_RICHTEXTAREA, SuitModel::TYPE_FILE])) {
                        // have similar value
                        $model = $model->where($realAttrName, 'LIKE', '%' . $value . '%');
                    } elseif ($conditional == 'isnull') {
                        // check null
                        $model = $model->whereNull($realAttrName);
                    } else {
                        // exact match value
                        $values = explode(',', $value);
                        $model = $model->whereIn($realAttrName, $values);
                    }
                }
            }
        }

        // Filter by keyword
        if ($keyword = $param['keyword']) {
            if (!empty($keyword)) {
                $searchTerm = preg_replace("/[ ]+/i", " ", trim( preg_replace("/[^\da-z]/i", " ", $keyword) ));
                $uniqueSearchTerm = array_unique(explode(' ', $searchTerm));
                $searchTerms = implode('|',  $uniqueSearchTerm);
                // dd($searchTerms);

                $baseMainModel = $this->mainModel;
                $model = $model->where(function ($qsearch) use($searchTerms, $baseAttr, $baseMainModel) {
                    $firstClauseInitiated = false;
                    foreach ($baseAttr as $key=>$_setting) {
                        if (in_array($_setting["type"], [SuitModel::TYPE_TEXT, SuitModel::TYPE_TEXTAREA, SuitModel::TYPE_RICHTEXTAREA, SuitModel::TYPE_FILE])) {
                            if (!$firstClauseInitiated) {
                                $qsearch->where($key, 'rlike', $searchTerms);
                                $firstClauseInitiated = true;
                            } else {
                                $qsearch->orWhere($key, 'rlike', $searchTerms);
                            }
                        }
                    }
                    // Translation Look-Up
                    $currentDefaultLocale = strtolower(config('app.fallback_locale', 'en'));
                    if (config('suitcore.multi_locale_site', false) &&
                        strtolower(app()->getLocale()) != $currentDefaultLocale) {
                        // With Translation
                        $relatedIds = SuitTranslation::where('class', get_class($baseMainModel))
                                      ->select('identifier')
                                      ->where('value', 'rlike', $searchTerms)
                                      ->where('locale', strtolower(app()->getLocale()))
                                      ->pluck('identifier');
                        if ($firstClauseInitiated) {
                            $qsearch->orWhereIn('id', $relatedIds);
                        } else {
                            $qsearch->whereIn('id', $relatedIds);
                        }
                    }
                });

                if ($param['orderBy'] == 'relevance') {
                    $weightComponents = [];
                    if (is_array($uniqueSearchTerm) && count($uniqueSearchTerm) > 0) {
                        foreach ($uniqueSearchTerm as $term) {
                            // TYPE_TEXT
                            $tmpWeight = [];
                            foreach ($baseAttr as $key=>$_setting) {
                                if (in_array($_setting["type"], [SuitModel::TYPE_TEXT])) {
                                    $tmpWeight[] = $baseObject->getTableName() . "." . $key . " LIKE '%{$term}%'";
                                }
                            }
                            $weightComponents[] = " IF( " . implode(" OR ", $tmpWeight) . ", 4, 0) ";
                            // TYPE TEXTAREA/RICHTEXTAREA
                            $tmpWeight = [];
                            foreach ($baseAttr as $key=>$_setting) {
                                if (in_array($_setting["type"], [SuitModel::TYPE_TEXTAREA, SuitModel::TYPE_RICHTEXTAREA])) {
                                    $tmpWeight[] = $baseObject->getTableName() . "." . $key . " LIKE '%{$term}%'";
                                }
                            }
                            $weightComponents[] = " IF( " . implode(" OR ", $tmpWeight) . ", 1, 0) ";
                        }
                        $weightComponents = "(" . implode(" + ", $weightComponents) . ") DESC";
                    }
                    $model = $model->orderByRaw(empty($weightComponents) ? "id DESC" : $weightComponents);
                }
            }
        }

        // Extended Query Condition
        if ($setting['extended_raw_condition']) {
            $model = $model->whereRaw($setting['extended_raw_condition']);
        }

        // Ordering Rules
        if ($setting['extended_raw_order']) {
            $model = $model->orderByRaw( $setting['extended_raw_order'] );
        } elseif ($param['orderBy']) {
            $listOrderBy = explode(',', $param['orderBy']);
            $listOrderType = explode(',', $param['orderType']);
            if ($listOrderBy && is_array($listOrderBy) && count($listOrderBy) > 0) {
                foreach($listOrderBy as $key=>$val) {
                    if (isset($baseAttr[$val])) {
                        $model = $model->orderBy( $val, ($listOrderType && is_array($listOrderType) && count($listOrderType) > 0 && isset($listOrderType[$key]) ? $listOrderType[$key] : "asc") );
                    }
                }
            }
        }

        // Group By (if any)
        if ($setting['extended_groupby']) {
            $model = $model->groupBy($setting['extended_groupby']);
        }

        // RETURN RESULT
        // [1] Raw Result
        if ($setting['raw_result']) return $model;
        // [2] Non-Raw Result
        // [2a] Paginated
        $resultData = null;
        if ($param['paginate'] && $param['perPage'] != static::FETCH_ALL) {
            $resultData = $model->paginate($param['perPage']);
        } else {
            // [2b] Non-Paginated
            if ($param['perPage'] == static::FETCH_ALL) {
                // [2b1] All Result
                $resultData = $model->get();
            } elseif ($param['perPage'] == 1) {
                // [2b2] Single Result
                $resultData = ($tmpResult = $model->first() ?: false);
            } else {
                // [2b3] Limited n-object
                $resultData = $model->take($param['perPage'] < 1 ? static::DEFAULT_FETCH : $param['perPage'])->get();
            }
        }

        // Optional Hidden Attribute Forced-Visibility 
        if ($setting['optional_hidden_visibility'] && is_array($setting['optional_hidden_visibility']) && count($setting['optional_hidden_visibility']) > 0) {
            $resultData = $resultData->makeVisible(array_intersect(array_keys($baseAttr), $setting['optional_hidden_visibility']));
        }

        // RETURN RESULT
        // for cached options
        if ($cached && $this->mainModel) {
            return $this->cacheResult($requestKey, $resultData);
        }
        // for non-cached options
        return $resultData;
    }

    /**
     * Make standard general tagging support for all type of cache-drivers (if not supported)
     **/
    /**
     * Tagging a key, mapping a key to specified tag(s)
     * @param  array $tag
     * @param  string $key
     * @return void
     **/
    private function taggingCacheAlternative($tag, $key) {
        $arrTags = is_array($tag) ? $tag : [$tag];
        foreach ($arrTags as $idx => $_tag) {
            $mappedCacheTags = Cache::get('tags-' . $_tag, []);
            $mappedCacheTags[] = $key;
            Cache::forever('tags-' . $_tag, $mappedCacheTags);
        }
    }

    /**
     * Flush tagged key
     * @param  array $tag
     * @return void
     **/
    private function flushTaggingCacheAlternative($tag) {
        $arrTags = is_array($tag) ? $tag : [$tag];
        foreach ($arrTags as $idx => $_tag) { 
            if ($mappedCacheKeys = Cache::get('tags-' . $_tag, [])) {
                foreach ($mappedCacheKeys as $idx => $key) {
                    Cache::forget($key);
                }
            }
        }
    }

    /**
     * Cache Related Helper
     **/
    /**
     * Generate Cache Key based on parameter array
     * @param  array $paramArr
     * @return string
     **/
    public function generateCacheKey(array $paramArr) {
        $clonedArr = $paramArr;
        ksort($clonedArr); 
        return md5(serialize($clonedArr));
    } 

    /**
     * Cache data wrapper
     * @param  string $key
     * @param  object $data
     * @return object
     **/
    public function cacheResult($key, $data) {
        $result = null;
        if ($data === null) $data = false;
        if ($key) {
            $tag = get_class($this->mainModel);
            // new way to return null cachedData, since 5.4 and later had not cached null and false
            $result = [
                'cached_data' => $data
            ];
            if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                Cache::tags($tag)->forever($key, $result);
            } else {
                Cache::forever($key, $result);
                $this->taggingCacheAlternative($tag, $key);
            }
        }
        return ($result && is_array($result) && isset($result['cached_data']) ? $result['cached_data'] : null);
    }

    /**
     * Return Cached data based on key array
     * @param  string $key
     * @return object
     **/
    public function getCachedData($key) {
        $cachedData = null;
        if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
            $tag = get_class($this->mainModel);
            $cachedData = Cache::tags($tag)->get($key);
        } else {
            $cachedData = Cache::get($key);
        }
        // new way to return null cachedData, since 5.4 and later had not cached null and false
        if ($cachedData &&
            is_array($cachedData) &&
            isset($cachedData['cached_data'])) {
            return $cachedData['cached_data'];
        }
        // backward compatibility for previous cached data
        if ($cachedData) return $cachedData;
        // if no cached data exist
        return null;
    }
}
