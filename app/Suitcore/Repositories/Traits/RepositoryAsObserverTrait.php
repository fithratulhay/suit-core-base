<?php

namespace Suitcore\Repositories\Traits;

use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Contract\SuitRepositoryContract;

trait RepositoryAsObserverTrait
{
    protected static $selfMainModelObserverLoaded = false;

    // METHODS
    /**
     * Load observer of current-repository to related main model
     **/
    public function loadSelfAsMainModelObserver() {
        if ($this instanceof SuitRepositoryContract &&
            !static::$selfMainModelObserverLoaded && 
            $this->mainModel &&
            $this->mainModel instanceof SuitModel) {
            $this->mainModel::observe($this);
            static::$selfMainModelObserverLoaded = true;
        }
    }

    // Additional Event Listener for main-model observer (registered-once)
    public function creating(SuitModel $model)
    {
        // info("creating... " . get_class($model));
    }

    public function updating(SuitModel $model)
    {
        // info("updating... " . get_class($model));
    }

    public function deleting(SuitModel $model)
    {
        // info("deleting... " . get_class($model));
    }

    public function saving(SuitModel $model)
    {
        // info("saving... " . get_class($model));
    }

    public function created(SuitModel $model)
    {
        // info("created... " . get_class($model));
    }

    public function updated(SuitModel $model)
    {
        // info("updated... " . get_class($model));
    }

    public function deleted(SuitModel $model)
    {
        // info("deleted... " . get_class($model));
    }

    public function saved(SuitModel $model)
    {
        // info("saved... " . get_class($model));
    }
}
