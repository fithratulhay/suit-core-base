<?php

namespace Suitcore\Repositories;

use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitRepository implements SuitRepositoryContract
{
    use SuitRepositoryTrait;

    // METHODS
    /** 
     * Default Constructor
     **/
    public function __construct(SuitModel $model)
    {
        $this->mainModel = $model;
    }
}
