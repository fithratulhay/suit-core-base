<?php

namespace Suitcore\Repositories;

use Illuminate\Notifications\Messages\MailMessage;
use Suitcore\Models\SuitEmailTemplate;
use Suitcore\Repositories\Contract\SuitEmailTemplateRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitEmailTemplateRepository implements SuitEmailTemplateRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(SuitEmailTemplate $model)
    {
        $this->mainModel = $model;
    }
}
