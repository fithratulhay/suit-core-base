<?php

namespace Suitcore\Repositories\Contract;

interface SuitUserSettingRepositoryContract extends SuitRepositoryContract
{
	public function updateByKey($user_id, $key, $value);

	public function getValue($user_id, $key, $default = '');

    public function save($user_id, $settingArray);
}
