<?php

namespace Suitcore\Repositories\Contract;

use Suitcore\Models\SuitModel;

interface SuitRepositoryContract {

    // Generic Constants
    const FETCH_ALL = -1;
    const DEFAULT_FETCH = 10;

    /**
     * Set Base/Custom Main Model for related repository on runtime
     * @param SuitModel Custom Model
     **/
    public function setMainModel(SuitModel $model);

    /**
     * Get Base/Custom Main Model for related repository on runtime
     * @param SuitModel Custom Model
     **/
    public function getMainModel();

    /**
    * Make a new instance of the entity to query on
    *
    * @param array $with
    */
    public function make(array $with = array());

    /**
    * Retrieve all entities
    *
    * @param array $with
    * @return Illuminate\Database\Eloquent\Collection
    */
    public function all(array $with = array());

    /**
    * Find a single entity
    *
    * @param int $id
    * @param array $with
    * @return Illuminate\Database\Eloquent\Model
    */
    public function find($id, array $with = array());

    /**
    * Get Results by Page
    *
    * @param int $page
    * @param int $limit
    * @param array $with
    * @return StdClass Object with $items and $totalItems for pagination
    */
    public function getByPage($page = 1, $limit = 10, $with = array());

    /**
    * Search for many results by key and value
    *
    * @param string $key
    * @param mixed $value
    * @param array $with
    * @return Illuminate\Database\Query\Builders
    */
    public function getBy($key, $value, array $with = array());

    /**
     * Display object list as JSON text that suitable for datatable frontend needs
     * @param  array $param
     * @return string jsonOutput
     **/
    public function jsonDatatable($param, $columnFormatted = null, $specificFilter = null, $optionalFilter = null, $columnException = null, $datatableExtendedAndQuery = null);

    /**
     * Get object detail
     * @param  int $objectId
     * @return array Object Detail
     **/
    public function get($objectId);

    /**
     * Create object with attributes in $param and return created object 
     * to $object and as function result
     * @param  array $param
     * @param  SuitModel $object
     * @return SuitModel Created Object
     **/
    public function create($param, SuitModel &$object = null);

    /**
     * Update object that defined by identifier $id
     * with attributes in $param and return updated object 
     * to $object and as function result
     * @param  integer $id
     * @param  array $param
     * @param  SuitModel $object
     * @return SuitModel Updated Object
     **/
    public function update($id, $param, SuitModel &$object = null);

    public function delete($id, SuitModel &$object = null);

    public function deleteAll($ids);

    /**
     *
     * Get Filtered Object List/Single/Raw Query Builder
     * 
     * Sample Parameter without condition and relationship
     *   attribute_name : "attribute_name"
     *   attribute_name : "attribute_name1,attribte_name2,dll"
     * Sample Parameter with condition
     *   _max_attribute_name : "max" "attribute_name"
     * Sample Parameter with relationship
     *   _relationshipName_attribute_name : "relationshipName" "attribute_name"
     * Sample Parameter with condition and relationship
     *   _max__relationshipName_attribute_name : "max" "relationshipName" "attribute_name"
     *
     * Defined Parameter
     *
     * keyword : keyword pattern separated by spaces
     * orderBy : multiple ordering column
     * orderType : multiple ordering column
     * paginate : false / true
     * perPage : number : -1 (all), 1, 2, 10, etc
     * if paginate false and perPage 1 then its single object, return first() from model collection
     *
     * @param  array  $param
     * @param  array  $setting
     * @param  boolean $cached
     *
     * @return SuitModel|SuitModel[]|\Illuminate\Database\Eloquent\Collection
     **/
    public function getByParameter($param, $setting = [], $cached = false);
}
