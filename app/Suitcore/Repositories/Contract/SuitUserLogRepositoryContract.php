<?php

namespace Suitcore\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface SuitUserLogRepositoryContract extends SuitRepositoryContract
{
    public function getExcludedRouteNames();
}
