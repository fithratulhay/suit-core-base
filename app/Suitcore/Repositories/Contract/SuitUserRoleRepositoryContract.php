<?php

namespace Suitcore\Repositories\Contract;

interface SuitUserRoleRepositoryContract extends SuitRepositoryContract
{   
    public function getAllRoles();

    public function getAllRoleCodes();

    public function getAllRoleObjects();

    public function isPermitted($user, $route, $baseModel = null);
}
