<?php

namespace Suitcore\Repositories\Contract;

interface SuitEmailTemplateRepositoryContract extends SuitRepositoryContract
{
    // any extended contract / behaviour signature
}
