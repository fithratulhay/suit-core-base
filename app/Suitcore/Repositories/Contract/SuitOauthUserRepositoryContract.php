<?php

namespace Suitcore\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface SuitOauthUserRepositoryContract extends SuitRepositoryContract
{
    public function getOauth($oauthData);
}
