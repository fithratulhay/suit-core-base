<?php

namespace Suitcore\Repositories\Contract;

interface SuitUserPermissionRepositoryContract extends SuitRepositoryContract
{   
    public function refreshList();

    public function getCachedList();

    public function getRawRoutes();

    public function getPermissionByRoleId($id, $routeName = null);

    public function getPermissionByRoleCode($code, $routeName = null);
}
