<?php

namespace Suitcore\Repositories\Contract;

use Suitcore\Models\SuitModel;
use Suitcore\Accounts\SocialAccountInterface;

interface SuitUserRepositoryContract extends SuitRepositoryContract
{   
	public function createFromSocialAccount(SocialAccountInterface $account);
	
	public function findByIdentifier($identifier);

    public function byIdentifier($identifier);

    public function deactivateSimilarAccount($user);
}
