<?php

namespace Suitcore\Repositories\Contract;

interface SuitNotificationRepositoryContract extends SuitRepositoryContract
{
    /**
     * Add new notification.
     * @param int $relatedUserId
     * @param string $message
     * @return void
     */
    public function add($relatedUserId, $message, $url = null);

    /**
     * Get the number of notification for a specific user.
     * @param \App\SuitCommerce\Models\User $user
     * @return int
     */
    public function getNotificationCount($user);

    public function read(array $ids = []);
}
