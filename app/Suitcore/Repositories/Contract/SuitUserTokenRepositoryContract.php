<?php

namespace Suitcore\Repositories\Contract;

use Suitcore\Models\SuitUser;

interface SuitUserTokenRepositoryContract extends SuitRepositoryContract
{   
    public function getUserTokenFromUser(SuitUser $user);
}
