<?php

namespace Suitcore\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitUrlRedirection;
use Suitcore\Repositories\Contract\SuitUrlRedirectionRepositoryContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;

class SuitUrlRedirectionRepository implements SuitUrlRedirectionRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(SuitUrlRedirection $model)
    {
        $this->mainModel = $model;
    }

    /**
    * Retrieve all entities
    *
    * @param array $with
    * @return Illuminate\Database\Eloquent\Collection
    */
    public function all(array $with = array())
    {
        $entity = $this->make($with);

        // return Cache::rememberForever('url_redirection_list', function() use($entity) {
            return $entity->get();
        // });
    }
}
