<?php

namespace Suitcore\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitUrlSeo;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitcore\Repositories\Contract\SuitUrlSeoRepositoryContract;

class SuitUrlSeoRepository implements SuitUrlSeoRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(SuitUrlSeo $model)
    {
        $this->mainModel = $model;
    }

    public function getSEOByUrl($url) {
        // Return Cached Data if any
        $requestKey = $this->generateCacheKey([
            'class' => get_class($this),
            'method' => __FUNCTION__, 
            'signature' => func_get_args()
        ]);
        $cachedData = $this->getCachedData($requestKey);
        if ($cachedData !== null) return $cachedData;

        // Process (if cache missed)
        $result = $this->make()->where('url', $url)->get()->first();

        // Cache The Result and Return
        return $this->cacheResult($requestKey, $result);
    }
}
