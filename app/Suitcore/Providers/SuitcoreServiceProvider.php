<?php

namespace Suitcore\Providers;

use Exception;
use Input;
use Schema;
use Image;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Suitcore\Uploader\Upload;
use Suitcore\Thumbnailer\Thumbnailer;
use Suitcore\FileGrabber\FileGrabber;
use Suitcore\Models\SuitEmailTemplate;
use Suitcore\Notifications\Contracts\EmailTemplateInterface as EmailTemplateContract;

class SuitcoreServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any suitcore services.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        /*
        $viewPath = __DIR__.'/../Resources/views';
        $this->loadViewsFrom($viewPath, 'suitcore');
        $this->publishes([
            $viewPath => base_path('resources/views/suitcore'),
        ], 'views');
        */

        //if (!defined('SUITCORE_ASSETS_PARENT_URL')) {
        //    define('SUITCORE_ASSETS_PARENT_URL', $this->app['url']->asset('suitcore'));
        //}

        // Upload Handler 
        // $config = $this->app['config']->get('suitcore.route', []);
        $router->group(['prefix' => 'paneladmin', 'middleware' => ['web', 'suitcore_roles']], function ($router) {
            // Default Upload Files
            $router->post('uploadfile', [
                'as' => 'admin.uploadfile',
                function(){
                    try {
                        $CKEditorFuncNum = Input::get("CKEditorFuncNum");
                        $destinationPath = public_path() . '/files/raw/';
                        $fileName = substr(str_shuffle(str_repeat('abcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, 5) . '.' . Input::file('upload')->getClientOriginalExtension();
                        $result = Input::file('upload')->move($destinationPath, $fileName);
                        if ($result) {
                            // read the image
                            try {
                                $image = Image::make($destinationPath . $fileName);
                                $width = $image->width();
                                // fix orientation       
                                $image->orientate();
                                // make interlaced image
                                $image->interlace();
                                if ($width > 1024) {
                                    // resize & save
                                    $image->widen(1024)->save($destinationPath . $fileName, 80);
                                } else {
                                    // save
                                    $image->save($destinationPath . $fileName, 80);
                                }
                            } catch (Exception $e2) { }

                            $url = url('/files/raw/' . $fileName);
                            return "<script>window.parent.CKEDITOR.tools.callFunction("
                                . $CKEditorFuncNum . ", \"" . $url . "\");</script>";
                        }
                    } catch (Exception $e) {
                        return "Upload failed!";
                    }
                }]
            );
        });
        $router->group(['middleware' => ['web']], function ($router) {
            // Default Refresh Captcha
            $router->get('/captcha/refresh/{config?}', array(
                'as' => 'captcha.refresh',
                function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
                    return $captcha->src($config);
                })
            );
        });
    }

    /**
     * Register any suitcore services.
     *
     * @return void
     */
    public function register()
    {
        // base config publish
        $configPath = __DIR__ . '/../Config/suitcore.php';
        $this->mergeConfigFrom($configPath, 'suitcore');
        $this->publishes([$configPath => config_path('suitcore.php')], 'config');
        $this->app->singleton('command.suitcore.publish', function($app)
        {
            $publicPath = $app['path.public'];
            return new \Suitcore\Console\PublishCommand($app['files'], $publicPath);
        });
        $this->commands('command.suitcore.publish');

        // Register Suitcore Service Provider
        $this->registerUploadService();
        $this->registerThumbnailerService();
        $this->registerFileGrabberService();
        $this->registerSuitCoreService();

        // Email Notification Templating Service
        $this->app->alias(SuitEmailTemplate::class, EmailTemplateContract::class);

        // Error Report Services, Rollbar only for production and staging
        if ($this->app->environment('production') || $this->app->environment('staging')) {
            $this->app->register(\Jenssegers\Rollbar\RollbarServiceProvider::class);
        }
    }

    /**
     * Upload Service
     **/
    public function registerUploadService() {
        $this->app->singleton('upload', function () {
            return new Upload;
        });
    }

    /**
     * Thumbnail Service
     **/
    public function registerThumbnailerService() {
        $this->app->singleton('thumbnailer', function () {
            return new Thumbnailer;
        });
    }

    /**
     * File Grabber Service
     **/
    public function registerFileGrabberService() {
        $this->app->singleton('filegrab', function ($app) {
            $grabber = new FileGrabber;
            $grabber->setTemp(storage_path('app/grabbers'), 0775);
            return $grabber;
        });
    }

    public function registerSuitCoreService()
    {
        $repositories = config('suitcore.repositories');
        $this->loadFromConfig($repositories, false, config('suitcore.custom_base_model'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitcore.repositories');
        return array_merge([
            'command.suitcore.publish', 
            'upload', 'thumbnailer', 'filegrab'
        ], array_keys($repositories));
    }
}
