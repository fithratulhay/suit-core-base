<?php

namespace Suitcore\Providers;

use Suitcore\Models\SuitModel;

trait LoadRepositoryConfigTrait
{
    protected function deepLoad(array $arrays = [])
    {
        foreach ($arrays as $key => $arg) {
            if (is_array($arg)) {
                $params = $this->deepLoad($arg);
                $arrays[$key] = new $key(...$params);
            } else {
                $arrays[$key] = $arg instanceof SuitModel ? new $arg([]) : new $arg;
            }
        }
        return array_values($arrays);
    }

    public function loadFromConfig(array $config = null, $singletonImplementation = false, array $customBaseModelConfig = null)
    {
        if ($config === null) {
            return;
        }

        foreach ($config as $contract => $implementation) {
            // singleton-mapping / binding
            if ($singletonImplementation) {
                $repo = key($implementation);
                $args = $implementation[$repo];

                $args = $this->deepLoad($args);

                $baseRepo = new $repo(...$args);
                $this->app->singleton($contract, function () use ($baseRepo) {
                    return $baseRepo;
                }); 
            } else {
                $this->app->bind($contract, $implementation);
            }
            // when resolved ...
            $this->app->resolving($contract, function ($impl, $app) use($contract, $customBaseModelConfig) {
                if ($customBaseModelConfig &&
                    is_array($customBaseModelConfig) &&
                    isset($customBaseModelConfig[$contract]) &&
                    method_exists($impl, 'setMainModel')) {
                    $customBaseModel = new $customBaseModelConfig[$contract];
                    $impl->setMainModel($customBaseModel);
                }
                if (method_exists($impl, 'loadSelfAsMainModelObserver')) {
                    $impl->loadSelfAsMainModelObserver();
                }
            });
        }
    }
}
