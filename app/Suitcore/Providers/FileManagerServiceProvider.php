<?php 

namespace Suitcore\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class FileManagerServiceProvider extends ServiceProvider 
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $configPath = __DIR__ . '/../Config/elfinder.php';
        $this->mergeConfigFrom($configPath, 'elfinder');
        $this->publishes([$configPath => config_path('elfinder.php')], 'config');

        $this->app->singleton('command.suitcorefilemanager.publish', function($app)
        {
			$publicPath = $app['path.public'];
            return new \Suitcore\Console\PublishCommand($app['files'], $publicPath);
        });
        $this->commands('command.suitcorefilemanager.publish');
	}

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
        $viewPath = __DIR__.'/../Resources/views/elfinder';
        $this->loadViewsFrom($viewPath, 'suitcorefilemanager');
        $this->publishes([
            $viewPath => base_path('resources/views/suitcore/elfinder'),
        ], 'views');

        if (!defined('ELFINDER_IMG_PARENT_URL')) {
			define('ELFINDER_IMG_PARENT_URL', $this->app['url']->asset('suitcore/elfinder'));
		}

        $config = $this->app['config']->get('elfinder.route', []);
        $config['namespace'] = 'Suitcore\Controllers';

        $router->group($config, function($router)
        {
            $router->get('/',  ['as' => 'elfinder.index', 'uses' =>'FileManagerController@showIndex']);
            $router->any('connector', ['as' => 'elfinder.connector', 'uses' => 'FileManagerController@showConnector']);
            $router->get('popup/{input_id}', ['as' => 'elfinder.popup', 'uses' => 'FileManagerController@showPopup']);
            $router->get('filepicker/{input_id}', ['as' => 'elfinder.filepicker', 'uses' => 'FileManagerController@showFilePicker']);
            $router->get('tinymce', ['as' => 'elfinder.tinymce', 'uses' => 'FileManagerController@showTinyMCE']);
            $router->get('tinymce4', ['as' => 'elfinder.tinymce4', 'uses' => 'FileManagerController@showTinyMCE4']);
            $router->get('ckeditor', ['as' => 'elfinder.ckeditor', 'uses' => 'FileManagerController@showCKeditor4']);
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('command.suitcorefilemanager.publish');
	}
}
