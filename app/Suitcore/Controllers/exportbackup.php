public function exportToExcel($specificFilter = null)
{
    if ($this->baseModel) {
        $baseObj = $this->baseModel;
        $dataSpecificFilter = ($specificFilter && is_array($specificFilter) ? $specificFilter : []);
        // CAPTURE DATATABLE FILTER IF ANY
        $datatableSpecificFilters = session()->get('datatable_specific_filters', null);
        if ($datatableSpecificFilters &&
            is_array($datatableSpecificFilters) &&
            count($datatableSpecificFilters) > 0) {
            $attrSettings = $baseObj->attribute_settings;
            foreach ($datatableSpecificFilters as $key => $value) {
                $estimatedMainObjectRealKey = str_replace($baseObj->getTable().".", "", $key);
                if (isset($attrSettings[$estimatedMainObjectRealKey])) {
                    $dataSpecificFilter[$key] = $value;
                }
            }
        }

        // Timezone
        $databaseTz = config('app.timezone'); 
        $defaultUserTz = config('suitcore.default_user_timezone'); // default-user-timezone
        $userTz = $defaultUserTz;
        if ($currentAuthUser = auth()->user()) {
            if (!empty($currentAuthUser->timezone)) {
                $userTz = $currentAuthUser->timezone;
            }
        }

        // BOX SPOUT WAY
        $fileName = str_replace(" ", "", $baseObj->_label)  . 'Data'.date('dmYhis').'.xlsx';
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment; filename='. $fileName,
        ];

        $callback = function () use ($baseObj, $dataSpecificFilter, $specificFilter, $userTz, $defaultUserTz, $databaseTz) {
            $writer = WriterFactory::create(Type::XLSX); // available :  XLSX / CSV / ODS

            $writer->openToFile("php://output"); // file or phpStream
            // $writer->openToBrowser($fileName); // stream data directly to the browser

            // Customizing the sheet name when writing
            $sheet = $writer->getCurrentSheet();
            $sheet->setName('Data');

            $columnHeader = [];
            foreach ($baseObj->getBufferedAttributeSettings() as $key=>$val) {
                if ( $val['visible'] ) {
                    $columnHeader[] = $val['label'];
                }
            }
            $writer->addRow($columnHeader); // Header - add a row at a time
            $objectList = $baseObj->select($baseObj->getTable().".*");
            if ($dataSpecificFilter != null && count($dataSpecificFilter) > 0) {
                $objectList->where(function($query) use ($dataSpecificFilter, $baseObj, $userTz, $defaultUserTz, $databaseTz) {
                    $attrSettings = $baseObj->attribute_settings;
                    foreach($dataSpecificFilter as $key=>$val) {
                        $estimatedMainObjectRealKey = str_replace($baseObj->getTable().".", "", $key);
                        if (isset($attrSettings[$estimatedMainObjectRealKey]) &&
                            isset($attrSettings[$estimatedMainObjectRealKey]['type']) &&
                            in_array($attrSettings[$estimatedMainObjectRealKey]['type'], [SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE]) &&
                            str_contains($val, "-yadcf_delim-") ) {
                            $ranges = explode("-yadcf_delim-", $val);
                            $currentAttrSetting = $attrSettings[$estimatedMainObjectRealKey];
                            $ranges = array_map(function($arrValue) use ($currentAttrSetting, $userTz, $defaultUserTz, $databaseTz) {
                                if ($currentAttrSetting['type'] == SuitModel::TYPE_DATETIME) {
                                    if ( ($timestamp = strtotime($arrValue)) !== false ) {
                                        $inputDate = Carbon::createFromTimestamp($timestamp);
                                        try {
                                            $inputDate = Carbon::createFromFormat('Y-m-d H:i:s', $inputDate->toDateTimeString(), $userTz);
                                        } catch (Exception $e) {
                                            try {
                                                $inputDate = Carbon::createFromFormat('Y-m-d H:i:s', $inputDate->toDateTimeString(), $defaultUserTz);
                                            } catch (Exception $e) { }
                                        }
                                        return $inputDate->tz($databaseTz)->toDateTimeString();
                                    }
                                }
                                return $arrValue;
                            }, $ranges);
                            if (count($ranges) == 2 &&
                                !empty($ranges[0]) &&
                                !empty($ranges[1])) {
                                $query->where($key,">=",$ranges[0]);
                                $query->where($key,"<=",$ranges[1]);
                            } elseif (!empty($ranges[0]) &&
                                empty($ranges[1])) {
                                $query->where($key, ">=", $ranges[0]);
                            } elseif (empty($ranges[0]) &&
                                !empty($ranges[1])) {
                                $query->where($key, "<=", $ranges[1]);
                            } 
                        } elseif (!str_contains($val, "-yadcf_delim-")) {
                            if (is_array($val) && !empty($val)) {
                                $query->whereIn($key, $val);
                            } else {
                                $query->where($key,"=",$val);
                            }
                        }
                    }
                });
            }
            // set unlimited time and fixed minimum-memory resource from current-config
            ini_set('max_execution_time', 0);
            ini_set('memory_limit','512M');
            // process
            $objectList->chunk(100, function($objects) use ($writer, $specificFilter) {
                // init data
                $data = [];
                foreach ($objects as $object) {
                    $tmpRow = [];
                    foreach ($object->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                        if ($attrSettings['visible']) {
                            if ($specificFilter == null ||
                                !is_array($specificFilter) ||
                                !isset($specificFilter[$attrName])) {
                                $tmpRow[] = $object->renderRawAttribute($attrName);
                            }
                        }
                    }
                    $data[] = $tmpRow;
                }
                // Write Rows
                $writer->addRows($data); // add multiple rows at a time
            });
            // Close Writer
            $writer->close();
        };
        // return
        return response()->stream($callback, 200, $headers);

        /*
        // MAATWEBSITE EXCEL WAY
        $currentMaxTimeLimit = ini_get('max_execution_time');
        ini_set('max_execution_time', 0);
        Excel::create(str_replace(" ", "", $baseObj->_label)  . 'Data', function ($excel) {
            // Set sheets
            $excel->sheet('Data', function ($sheet) {
                $idx = 1;
                $objectList = $baseObj->select($baseObj->getTable().".*");
                if ($specificFilter != null && count($specificFilter) > 0) {
                    $objectList->where(function($query) use ($specificFilter) {
                        foreach($specificFilter as $key=>$val) {
                            $query->where($key, "=", $val);
                        }
                    });
                }
                $objectList->chunk(100, function($objects) use ($idx, $sheet) {
                    // reinit
                    $data = [];
                    // header if needed
                    if ($idx == 1 ){
                        $data[0] = [];
                        foreach ($baseObj->getBufferedAttributeSettings() as $key=>$val) {
                            if ( $val['visible'] ) {
                                $data[0][] = $val['label'];
                            }
                        }
                    }
                    // write data
                    foreach ($objects as $object) {
                        $tmpRow = [];
                        foreach ($object->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                            if ($attrSettings['visible']) {
                                if ($specificFilter == null ||
                                    !is_array($specificFilter) ||
                                    !isset($specificFilter[$attrName])) {
                                    $tmpRow[] = $object->renderRawAttribute($attrName);
                                }
                            }
                        }
                        $data[] = $tmpRow;
                    }
                    // Sheet manipulation
                    $sheet->fromArray($data, null, 'A' . $idx, false, false);
                    // increment index
                    $idx += ($idx == 1 ? 101 : 100);
                });

                // Sheet further manipulation
                // $sheet->fromArray($data, null, 'A1', false, false);
                $sheet->cells('A1:Z1', function ($cells) {
                    $cells->setFontWeight('bold');
                });
                $sheet->freezeFirstRow();

            });
        })->export('xlsx'); //*.xls only support until 65.536 rows
        ini_set('max_execution_time', $currentMaxTimeLimit);
        */
    }
}