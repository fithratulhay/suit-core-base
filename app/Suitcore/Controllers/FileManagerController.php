<?php 

namespace Suitcore\Controllers;

use App;
use Gate;
use Theme;
use UserRole;
use Barryvdh\Elfinder\ElfinderController;
use Barryvdh\Elfinder\Session\LaravelSession;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Request;
use League\Flysystem\Cached\CachedAdapter;
use League\Flysystem\Cached\Storage\Memory;
use League\Flysystem\Filesystem;

class FileManagerController extends ElfinderController
{
    /**
     * ID of certain admin page
     **/
    public $pageId;
    public $pageTitle;
    protected $baseLayout = 'metronics1';

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->package = 'suitcorefilemanager';
        // default page ID
        $this->pageId = config('elfinder.suitcore_page_id');
        $this->pageTitle = 'File Manager';
        view()->share('pageId', $this->pageId);
        view()->share('pageTitle', $this->pageTitle);
        // set theme (based on APP_BACKEND_THEME environment variable)
        $this->middleware(function ($request, $next) {
            // Check Policy
            if (Gate::denies('suitcorepermission')) {
                return App::abort(403);
            }
            if ($user = auth()->user()) {
                if ($roleTheme = UserRole::getByCode($user->role)) {
                    $this->baseLayout = preg_replace("/[^a-z0-9]/", "", $roleTheme->theme);
                }
                view()->share('baseLayout', $this->baseLayout);
            }
            // End Check Policy
            $manualThemeSet = '';
            $availableThemes = array_keys(config('themes.themes'));
            if ($request->has('temporary_theme')) {
                $requestedThemes = $request->get('temporary_theme');
                if (in_array($requestedThemes, $availableThemes)) {
                    $request->session()->put('temporary_theme', $requestedThemes);
                }
            }
            if ($request->session()->has('temporary_theme')) {
                $requestedThemes = $request->session()->get('temporary_theme');
                if (in_array($requestedThemes, $availableThemes)) {
                    $manualThemeSet = $requestedThemes;
                }
            }
            Theme::set(!empty($manualThemeSet) ? $manualThemeSet : env('APP_BACKEND_THEME', 'default'));
            return $next($request);
        });
    }

    protected function getViewVars()
    {
        $dir = 'suitcore/' . $this->package;
        $locale = str_replace("-",  "_", $this->app->config->get('app.locale'));
        if (!file_exists($this->app['path.public'] . "/$dir/js/i18n/elfinder.$locale.js")) {
            $locale = false;
        }
        $csrf = true;
        return compact('dir', 'locale', 'csrf');
    }
}
