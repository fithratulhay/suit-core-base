<?php

namespace Suitcore\Controllers;

use DB;
use Exception;
use Excel;
use File;
use Form;
use Hash;
use MessageBag;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Cell_DataValidation;
use PHPExcel_NamedRange;
use Route;
use Theme;
use UserRole;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitcore\Jobs\SuitExportJob;
use Suitcore\Jobs\SuitImportFromTemplateJob;

/**
 * This class define standard backend controller for Suitcore Application Instances
 **/
abstract class BackendController extends BaseController
{
    /**
     * Constants for partial view
     **/
    const TABLE_SELECTION = "tableselection";
    const TABLE_MENU = "tablemenu";

    // Partial View Related
    public static $partialView = [];

    /**
     * Constants for import from excel action
     **/
    const RESETBY = "resetby";
    const REPLACE = "replace";
    const IGNORE = "ignore";

    /**
     * Constants for notifications
     **/
    const NOTICE_NOTIFICATION = "notice";
    const WARNING_NOTIFICATION = "warning";
    const ERROR_NOTIFICATION = "error";
    const OTHER_NOTIFICATION = "other";

    // PROPERTIES
    /**
     * ID of certain admin page
     **/
    public $pageId;
    public $pageTitle;
    public $pageInfo;

    // Default Services / Repository
    protected $baseRepository;
    protected $baseModel;
    protected $topLevelRelationModelString;
    protected $routeBaseName;
    protected $routeDefaultIndex;
    protected $viewBaseClosure;
    protected $viewImportExcelClosure;
    protected $viewInstanceName;
    protected $currentImportProcessFileLocation;
    protected $baseLayout = 'metronics1';
    protected $isSpecificExport = false;

    // ACTION
    /**
     * Default Constructor
     * @param  SuitRepositoryContract $_baseRepo
     * @param  string $_topLevelRelationModelString
     * @return void
     */
    public function __construct(SuitRepositoryContract $_baseRepo, $_topLevelRelationModelString = null)
    {
        parent::__construct();
        // set property
        $this->baseRepository = $_baseRepo;
        $this->baseModel = $_baseRepo->getMainModel();
        $this->topLevelRelationModelString = $_topLevelRelationModelString;
        $this->routeBaseName = "backend.generic";
        $this->routeDefaultIndex = null; // sample : "backend.generic.index"
        $this->viewBaseClosure = "backend.admin.generic";
        $this->viewImportExcelClosure = $this->viewBaseClosure . ".template"; // sample :  "backend.admin.generic.template"
        $this->viewInstanceName = 'genericObj';
        // default page ID
        $this->pageId = 1;
        $this->pageTitle = $this->baseModel->_label;
        $this->pageInfo = $this->baseModel->_desc;
        view()->share('pageId', $this->pageId);
        view()->share('pageTitle', $this->pageTitle);
        view()->share('pageInfo', $this->pageInfo);
        view()->share('baseLayout', $this->baseLayout);
        // load partial view
        $partialViewConfig = config('suitcore.admin_panel_datatable_partial_views');
        self::$partialView = [ 
            self::TABLE_SELECTION => $partialViewConfig['tableselection'],
            self::TABLE_MENU => $partialViewConfig['tablemenu'],
        ];
        // set theme (based on APP_BACKEND_THEME environment variable)
        $this->middleware(function ($request, $next) {
            // Check Policy
            if (Gate::denies('suitcorepermission')) {
                return abort(403, 'Access denied!');
                // $this->showNotification(self::ERROR_NOTIFICATION, "Access Forbidden!", "You don't have a right to access this page!");
                // return redirect()->route('backend.home.index');
            }
            // End Check Policy
            // default authenticated user (if any)
            view()->share('currentUser', $this->authUser);
            // UserRole-Based Theme Set
            if ($this->authUser) {
                if ($roleTheme = UserRole::getByCode($this->authUser->role)) {
                    $this->baseLayout = preg_replace("/[^a-z0-9]/", "", $roleTheme->theme);
                    view()->share('baseLayout', $this->baseLayout);
                }
            }
            // Manual Theme Set
            $manualThemeSet = '';
            $availableThemes = array_keys(config('themes.themes'));
            if ($request->has('temporary_theme')) {
                $requestedThemes = $request->get('temporary_theme');
                if (in_array($requestedThemes, $availableThemes)) {
                    $request->session()->put('temporary_theme', $requestedThemes);
                }
            }
            if ($request->session()->has('temporary_theme')) {
                $requestedThemes = $request->session()->get('temporary_theme');
                if (in_array($requestedThemes, $availableThemes)) {
                    $manualThemeSet = $requestedThemes;
                }
            }
            Theme::set(!empty($manualThemeSet) ? $manualThemeSet : env('APP_BACKEND_THEME', 'default'));
            return $next($request);
        });
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = view()->make($this->layout);
        }
    }

    /**
     * Display list of baseModel
     * @param
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        // Parameter
        $param = request()->all();
        $paramSerialized = [];
        if ($param) {
            $idx = 0;
            foreach ($param as $key => $value) {
                $paramSerialized[$idx] = $key."=".$value;
                $idx++;
            }
        }
        $paramSerialized = $paramSerialized ? implode("&", $paramSerialized) : null;

        $objectLabel = str_plural(strtolower($this->baseModel->_label));
        $files = $this->getProgress('export-list--' .  str_plural(strtolower($objectLabel)), [], 'session');
        $exist = [];
        foreach ($files as $file) {
            if (str_contains($file, $objectLabel) && file_exists(public_path() . $file)) {
                $exist[] = $file;
            }
        }

        $this->updateProgress('export-list--' .  str_plural(strtolower($objectLabel)), $exist, 'session');

        view()->share(['files' => $exist, 'objectLabel' => $objectLabel]);
        // Render View
        $absoluteViewPath = $this->viewBaseClosure . '.index'; // default view
        if (view()->exists($absoluteViewPath)) {
            return view($absoluteViewPath)->with($this->viewInstanceName, $this->baseModel)
                                                      ->with('paramSerialized', $paramSerialized);
        } else {
            $absoluteViewPath = config('suitcore.admin_panel_default_base_view.' . $this->baseLayout, '') . '.list'; // base view
            if (view()->exists($absoluteViewPath)) {
                return view($absoluteViewPath)->with($this->viewInstanceName, $this->baseModel)
                                                          ->with('paramSerialized', $paramSerialized);
            }
        }
        return abort(404); // view not found
    }

    /**
     * Return json list of contentType for datatable purpose
     * @param
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postIndexJson() {
        // Parameter
        $param = request()->all();
        // specific filter if any
        $baseObj = $this->baseModel;
        $allAttribute = $baseObj->getAttributeSettings();
        $specificFilterFromRequest = [];
        foreach ($allAttribute as $attr => $config) {
            if (isset($config['visible']) &&
                $config['visible'] &&
                isset($param[$attr]) &&
                !empty($param[$attr])) {
                $specificFilterFromRequest[$baseObj->getTable().'.'.$attr] = $param[$attr];
            }
        }

        // Return
        $customRender = [];
        $selectedIds = session()->get(class_basename($baseObj) . '_selected_ids', []);
        if (Route::has($this->routeBaseName . '.select')) {
            $selectionSetting = [
                'session_token' => csrf_token(),
                'url_selection' => (Route::has($this->routeBaseName . '.select') ? route($this->routeBaseName . '.select',["id"=>"#id#"]) : '')
            ];
            $renderedSelection = view()->make(self::$partialView[self::TABLE_SELECTION], ['selectionSetting' => $selectionSetting])->render();
            $customRender['selection'] = $renderedSelection;
            $customRender['selectedIds'] = $selectedIds;
        }
        $menuSetting = [
            'session_token' => csrf_token(),
            'url_detail' => (Route::has($this->routeBaseName . '.show') ? route($this->routeBaseName . '.show',["id"=>"#id#"]) : ''),
            'url_edit' => (Route::has($this->routeBaseName . '.edit') ? route($this->routeBaseName . '.edit',["id"=>"#id#"]) : ''),
            'url_delete' => (Route::has($this->routeBaseName . '.destroy') ? route($this->routeBaseName . '.destroy', ['id' => "#id#"]) : ''),
        ];
        $renderedMenu = view()->make(self::$partialView[self::TABLE_MENU], ['menuSetting' => $menuSetting])->render();
        $customRender['menu'] = $renderedMenu;
        session()->put('datatablePageId', $this->pageId);
        return $this->baseRepository->jsonDatatable(
            $param,
            $customRender,
            $specificFilterFromRequest,
            null,
            null,
            null,
            Route::has($this->routeBaseName . '.select')
        );
    }

    /**
     * Return json list of contentType for select2 purpose
     * @param
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getListJson() {
        SuitModel::$isFormGeneratorContext = false;
        // Parameter
        $q = request()->get('q', '');
        $isAll = request()->get('all', 0);
        $otherFilter = request()->all();
        // Process
        $baseObj = $this->baseModel;
        // begin prepare other-filter
        $attrSettings = $baseObj->attribute_settings;
        if ($otherFilter && is_array($otherFilter) && count($otherFilter) > 0) {
            foreach ($otherFilter as $key => $value)
                if (!isset($attrSettings[$key])) unset($otherFilter[$key]);
        }
        // end prepare other-filter
        $localColumnSearch = $baseObj->getFormattedValueColumn();

        if (method_exists($this, 'getDefaultFilter')) {
            $baseObj = $this->getDefaultFilter($baseObj);
        }

        $firstField = null;
        if ($localColumnSearch) {
            foreach ($localColumnSearch as $idx => $field) {
                if (!$firstField) {
                    $baseObj = $baseObj->where($field,"like","%".$q."%");
                    $firstField = $field;
                } else {
                    $baseObj = $baseObj->orWhere($field,"like","%".$q."%");
                }
            }
        }
        if ($otherFilter && is_array($otherFilter) && count($otherFilter) > 0) {
            $baseObj = $baseObj->where($otherFilter);
        }
        if ($firstField) {
            $baseObj = $baseObj->orderBy($firstField, 'asc');
        }
        if ($isAll) {
            $baseObj = $baseObj->get();
            if ($baseObj && count($baseObj) > 0) {
                $data = [];
                foreach ($baseObj as $obj) {
                    $data[] = [
                        'id' => $obj->id,
                        'name' => $obj->getFormattedValue()
                    ];
                }
                return response()->json($data);
            }
        } else {
            $baseObj = $baseObj->paginate(10); // ->take(10)->get();
            if ($baseObj && count($baseObj) > 0) {
                $data['items'] = [];
                foreach ($baseObj as $obj) {
                    $data['items'][] = [
                        "id" => $obj->id,
                        "value" => $obj->id,
                        "text" => $obj->getFormattedValue()
                    ];
                }
                $data['total_count'] = $baseObj->total();
                return response()->json($data);
            }
        }
        return response()->json([]);
    }

    /**
     * Display baseModel detail
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function getView($id)
    {
        SuitModel::$isFormGeneratorContext = false;
        // Fetch
        $fetchedData = $this->baseRepository->get($id);
        if ($fetchedData['object'] == null) {
            return abort(404);
        }
        // Check Policy
        if (Gate::denies('suitcorepermission', $fetchedData['object'])) {
            return abort(403, 'Access denied!');
            // $this->showNotification(self::ERROR_NOTIFICATION, "Access Forbidden!", "You don't have a right to access this page!");
            // return redirect()->route('backend.home.index');
        }
        // End Check Policy
        // Render View
        $absoluteViewPath = $this->viewBaseClosure . '.show'; // default view
        if (view()->exists($absoluteViewPath)) {
            return view($absoluteViewPath)->with($this->viewInstanceName, $fetchedData['object']);
        } else {
            $absoluteViewPath = config('suitcore.admin_panel_default_base_view.' . $this->baseLayout, '') . '.view'; // base view
            if (view()->exists($absoluteViewPath)) {
                return view($absoluteViewPath)->with($this->viewInstanceName, $fetchedData['object']);
            }
        }
        return abort(404); // view not found
    }

    /**
     * Display baseModel create form
     * @param
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $param = request()->all();
        $baseObj = $this->baseModel;
        $allAttribute = $baseObj->getAttributeSettings();
        foreach ($allAttribute as $key => $cnf) {
            if ((isset($cnf['initiated']) && !$cnf['initiated']) || old($key)) {
                $baseObj->{$key} = old($key);
            }
        }

        // Render View
        $relativeView = ".create";
        $absoluteViewPath = $this->viewBaseClosure . $relativeView; // default view
        if (view()->exists($absoluteViewPath)) {
            return view($absoluteViewPath)->with($this->viewInstanceName, $baseObj);
        } else {
            $absoluteViewPath = config('suitcore.admin_panel_default_base_view.' . $this->baseLayout, '') . $relativeView; // base view
            if (view()->exists($absoluteViewPath)) {
                return view($absoluteViewPath)->with($this->viewInstanceName, $baseObj);
            }
        }
        return abort(404); // view not found
    }

    /**
     * Save entry data from baseModel create form
     * @param
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function postCreate() {
        // Save
        $param = request()->all();
        $baseObj = $this->baseModel;
        $result = $this->baseRepository->create($param, $baseObj);
        if ($baseObj->uploadError) {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . trans('suitcore.backend.create.upload.error.title'), trans('suitcore.backend.create.upload.error.message', ['obj' => strtolower($baseObj->_label)]));
        }
        // Return
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Created', 'Successfully create new ' . strtolower($baseObj->_label) .'.');
            if (Route::has($this->routeBaseName . '.show')) {
                return redirect()->route($this->routeBaseName . '.show', ['id'=>$baseObj->id]);
            } else {
                return $this->returnToRootIndex($baseObj);
            }
        }
        $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Created', ($baseObj->errors ? $baseObj->errors->first() : "Unknown Error!"));
        return redirect()->route($this->routeBaseName . '.create')->with('errors', $baseObj->errors)->withInput($param);
    }

    /**
     * Display baseModel update form
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function getUpdate($id) {
        $baseObj = $this->baseModel->find($id);
        if ($baseObj == null) return abort(404);
        // Check Policy
        if (Gate::denies('suitcorepermission', $baseObj)) {
            return abort(403, 'Access denied!');
            // $this->showNotification(self::ERROR_NOTIFICATION, "Access Forbidden!", "You don't have a right to access this page!");
            // return redirect()->route('backend.home.index');
        }
        // End Check Policy

        // Render View
        $relativeView = ".edit";
        $absoluteViewPath = $this->viewBaseClosure . $relativeView; // default view
        if (view()->exists($absoluteViewPath)) {
            return view($absoluteViewPath)->with($this->viewInstanceName, $baseObj);
        } else {
            $absoluteViewPath = config('suitcore.admin_panel_default_base_view.' . $this->baseLayout, '') . $relativeView; // base view
            if (view()->exists($absoluteViewPath)) {
                return view($absoluteViewPath)->with($this->viewInstanceName, $baseObj);
            }
        }
        return abort(404); // view not found
    }

    /**
     * Save entry data from baseModel update form
     * @param  int $id
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function postUpdate($id) {
        // Save
        $param = request()->all();
        $baseObj = ($this->baseModel ? $this->baseModel->find($id) : null);
        if ($baseObj == null) return abort(404);
        // Check Policy
        if (Gate::denies('suitcorepermission', $baseObj)) {
            return abort(403, 'Access denied!');
            // $this->showNotification(self::ERROR_NOTIFICATION, "Access Forbidden!", "You don't have a right to access this page!");
            // return redirect()->route('backend.home.index');
        }
        // End Check Policy

        $result = $this->baseRepository->update($id, $param, $baseObj);
        if ($baseObj->uploadError) {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . trans('suitcore.backend.update.upload.error.title'), trans('suitcore.backend.update.upload.error.message', ['obj' => strtolower($baseObj->_label)]));
        }
        // Return
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Updated', 'Successfully update ' . strtolower($baseObj->_label) .'.');
            if (Route::has($this->routeBaseName . '.show'))
                return redirect()->route($this->routeBaseName . '.show', ['id'=>$id]);
            else {
                return $this->returnToRootIndex($baseObj);
            }
        }
        if ($baseObj == null) abort(404);
        $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Updated', ($baseObj->errors ? $baseObj->errors->first() : "Unknown Error!"));
        return redirect()->route($this->routeBaseName . '.update', ['id'=>$id])->with('errors', $baseObj->errors)->withInput($param);
    }

    /**
     * Delete baseModel data
     * @param int $id
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function postDelete($id) {
        // Delete
        $baseObj = $this->baseModel;
        $result = $this->baseRepository->delete($id, $baseObj);
        if ($baseObj == null) return abort(404);
        // Check Policy
        if (Gate::denies('suitcorepermission', $baseObj)) {
            return abort(403, 'Access denied!');
            // $this->showNotification(self::ERROR_NOTIFICATION, "Access Forbidden!", "You don't have a right to access this page!");
            // return redirect()->route('backend.home.index');
        }
        // End Check Policy

        // Return
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Deleted', 'Successfully delete ' . strtolower($baseObj->_label) .'.');
        } else {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Deleted', 'Fail to delete ' . strtolower($baseObj->_label) .'. An error occured when processing with database.');
        }

        return $this->returnToRootIndex($baseObj);
    }

    /**
     * Delete Many baseModel data
     * @param int $id
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function postDeleteAll() {
        // Delete
        $baseObj = $this->baseModel;
        $deletedIds = session()->get(class_basename($baseObj) . '_selected_ids', []);
        $result = false;
        if (is_array($deletedIds) && count($deletedIds) > 0) {
            $result = $this->baseRepository->deleteAll($deletedIds);
        }
        // Return
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Deleted', 'Successfully delete selected ' . strtolower($baseObj->_label) .'!');
        } else {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Deleted', 'Fail to delete selected ' . strtolower($baseObj->_label) .'. An error occured when processing with database.');
        }
        
        return $this->returnToRootIndex($baseObj);
    }

    /**
     * Select baseModel data
     * @param int $id
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function postSelect($id) {
        // Delete
        $baseObj = $this->baseModel->find($id);
        if ($baseObj) {
            $isSelected = request()->get('is_selected', 1);
            $selectedIds = session()->get(class_basename($baseObj) . '_selected_ids', []);
            if (!is_array($selectedIds)) $selectedIds = [];
            if ($isSelected) {
                array_push($selectedIds, $id);
            } else {
                // is unselected
                if (in_array($id, $selectedIds) == true) {
                    foreach ($selectedIds as $key=>$value) {
                        if ($value == $id) unset($selectedIds[$key]);
                    }
                }
            }
            session()->put(class_basename($baseObj) . '_selected_ids', $selectedIds);
            // dd($selectedIds);
            return response()->json([
                'result' => true
            ]);
        }
        return response()->json([
            'result' => false
        ]);
    }

    /**
     * Index return route
     * @param int $id
     * @return \Illuminate\Support\Facades\Redirect
     */
    protected function returnToRootIndex($baseObj) {
        if (!empty($this->routeDefaultIndex)) {
            if (endsWith('.show', $this->routeDefaultIndex) &&
                !empty($this->topLevelRelationModelString)) {
                // custom detail return
                $topLevelRelationObject = $baseObj->getAttribute($this->topLevelRelationModelString);
                if ($topLevelRelationObject) {
                    return redirect()->route($this->routeDefaultIndex, ['id'=>$topLevelRelationObject->id]);
                }
            }
            return redirect()->route($this->routeDefaultIndex);
        }
        return redirect()->route($this->routeBaseName . '.index');
    }

    public function exportToExcel($specificFilter = null)
    {
        $totalSource = $this->getProgress('sources_count', 0, 'session');
        if ($totalSource > 0) {
            return response()->json(['active' => true , 'message' => 'Existing export ' . $this->baseModel->_label . ' process has been active.']);
        }
        $this->updateProgress('sources_count', 0, 'session');
        $this->updateProgress('exported_precentage', 0, 'session');
        $this->updateProgress('exported_success_count', 0, 'session');
        $targetQueue = config('suitcore.default_export_job_queue', 'default');
        $targetQueue = $this->getExportSpecificMode() ? str_replace(" ", "-", strtolower($this->baseModel->_label)) . '-' . $targetQueue : $targetQueue;
        $fileName = str_replace(" ", "", $this->baseModel->_label)  . 'Data'.date('dmYhis').'.xlsx';
        $datatableSpecificFilters = session()->get('datatable_specific_filters', null);

        $specificFolder = '/exports/' . snake_case(str_plural(strtolower($this->baseModel->_label)));
        $folderFile = public_path() . $specificFolder;
        $fileDest = $folderFile . '/' . $fileName;

        $cacheKey = 'export-list--' . str_plural(strtolower($this->baseModel->_label));
        $existFiles = $this->getProgress($cacheKey, [], 'session');
        if (!is_array($existFiles)) $existFiles = [];
        $lastFile = $specificFolder . '/' . $fileName;
        $this->updateProgress('last-file-' . str_plural(strtolower($this->baseModel->_label)), $lastFile, 'session');
        $existFiles[] = $lastFile;
        $this->updateProgress($cacheKey, $existFiles, 'session');
        $userId = auth()->check() ? auth()->user()->id : 0;
        dispatch((new SuitExportJob(get_class($this->baseRepository), $folderFile, $fileDest, $userId, $specificFilter, $datatableSpecificFilters))->onQueue($targetQueue));

        return response()->json(['active' => true, 'message' => 'New export ' . $this->baseModel->_label . ' process successfully started.']);
    }

    /**
     * Route Implementation to import from excel page
     * @return Illuminate\View\View
     */
    public function template()
    {
        $baseObj = $this->baseModel;
        $relativeView = ".template";
        if (view()->exists($this->viewImportExcelClosure)) {
            return view($this->viewImportExcelClosure)->with($this->viewInstanceName, $baseObj);
        } else {
            $this->viewImportExcelClosure = $this->viewBaseClosure . $relativeView; // default template view
            if (view()->exists($this->viewImportExcelClosure)) {
                return view($this->viewImportExcelClosure)->with($this->viewInstanceName, $baseObj);
            } else {
                $this->viewImportExcelClosure = config('suitcore.admin_panel_default_base_view.' . $this->baseLayout, '') . $relativeView; // basic template view
                if (view()->exists($this->viewImportExcelClosure)) {
                    return view($this->viewImportExcelClosure)->with($this->viewInstanceName, $baseObj);
                }
            }
        }
        return abort(404);
    }

    /**
     * Route Implementation to download template
     * @return ExcelFile
     */
    public function downloadTemplate()
    {
        // Generate data for Excel file
        // -- data source and settings
        $sourceData = $this->getTemplateAttributes();
        $sourceDataSettings = $this->baseModel->getBufferedAttributeSettings();
        // -- references data settings for relationship attributes
        $exceptionFields = is_array($this->getImportRelationshipFieldException()) ? $this->getImportRelationshipFieldException() : [];
        $relationshipReferences = $this->getImportRelationshipReferencesData();
        // Create an Excel file
        Excel::create($this->baseModel->_label . ' Template', function ($excel) use ($sourceData, $sourceDataSettings, $exceptionFields, $relationshipReferences) {
            // 1. Create list sheet.
            $excel->sheet('Data', function ($sheet) use ($sourceData, $sourceDataSettings, $exceptionFields, $relationshipReferences) {
                $excelSourceDataFormat = [];
                $parentIdx = -1;
                $idx = 0;
                $ranges = range('A', 'Z');
                $rangePrefix = "";
                foreach ($sourceData as $key => $value) {
                    $columnIdx = $rangePrefix.$ranges[$idx]; // handle A to ZZ
                    // default is text
                    $excelSourceDataFormat[$columnIdx] = PHPExcel_Style_NumberFormat::FORMAT_TEXT;
                    // check for exception fields
                    $applicated = false;
                    if (!$applicated && in_array($key, array_keys($exceptionFields))) {
                        $excelSourceDataFormat[$columnIdx] = PHPExcel_Style_NumberFormat::FORMAT_TEXT;
                        $applicated = true;
                    }
                    // check for relationship
                    if (!$applicated && in_array($key, array_keys($relationshipReferences))) {
                        for($i=2; $i < 1000; $i++) {
                            $cellValidation = $sheet->getCell(sprintf('%s%s', $columnIdx, $i))->getDataValidation();
                            $cellValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                            $cellValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                            $cellValidation->setAllowBlank(false);
                            $cellValidation->setShowInputMessage(true);
                            $cellValidation->setShowErrorMessage(true);
                            $cellValidation->setShowDropDown(true);
                            $cellValidation->setErrorTitle('Your input is invalid');
                            $cellValidation->setError('Value is does not exist in references sheet.');
                            $cellValidation->setPromptTitle('Choose from the list');
                            $cellValidation->setFormula1($columnIdx);
                        }
                        $applicated = true;
                    }
                    // check for other fields
                    if (!$applicated &&
                        isset($sourceDataSettings[$key]) &&
                        isset($sourceDataSettings[$key]['type'])) {
                        if (($sourceDataSettings[$key]['type'] == SuitModel::TYPE_NUMERIC ||
                             $sourceDataSettings[$key]['type'] == SuitModel::TYPE_FLOAT) &&
                            ( (isset($sourceDataSettings[$key]['options']) && !is_array($sourceDataSettings[$key]['options'])) || !isset($sourceDataSettings[$key]['options'])) ) {
                            $excelSourceDataFormat[$columnIdx] = PHPExcel_Style_NumberFormat::FORMAT_NUMBER;
                        } else if ($sourceDataSettings[$key]['type'] == SuitModel::TYPE_DATETIME ||
                            $sourceDataSettings[$key]['type'] == SuitModel::TYPE_DATE ||
                            $sourceDataSettings[$key]['type'] == SuitModel::TYPE_TIME) {
                            $excelSourceDataFormat[$columnIdx] = PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME; // d/m/Y H:i
                        }
                    }
                    // continue to next index
                    $idx++;
                    if ($idx >= 26) {
                        $parentIdx++;
                        if ($parentIdx >= 26) break; // end of column index in excel
                        $idx = 0;
                        if ($parentIdx >= 0) $rangePrefix = $ranges[$parentIdx];
                    }
                }
                $sheet->fromArray($sourceData);
                $sheet->setColumnFormat($excelSourceDataFormat);
            });
            // 2. Create references sheet.
            $excel->sheet('Master Data', function ($sheet) use ($sourceData, $relationshipReferences) {
                $parentIdx = -1;
                $idx = 0;
                $ranges = range('A', 'Z');
                $rangePrefix = "";
                $sheet->fromArray($sourceData);
                foreach ($sourceData as $key => $value) {
                    $columnIdx = $rangePrefix.$ranges[$idx]; // handle A to ZZ
                    $row = 2;
                    if (isset($relationshipReferences[$key])) {
                        foreach ($relationshipReferences[$key] as $value2) {
                            $cell = sprintf('%s%s', $columnIdx, $row);
                            $sheet->setCellValue($cell, $value2);
                            $row++;
                        }
                        // Set cell range name.
                        $sheet->_parent->addNamedRange(
                            new PHPExcel_NamedRange($columnIdx, $sheet, sprintf('%s%s:%s%s', $columnIdx, 2, $columnIdx, $row - 1))
                        );
                    }
                    // continue to next index
                    $idx++;
                    if ($idx >= 26) {
                        $parentIdx++;
                        if ($parentIdx >= 26) break; // end of column index in excel
                        $idx = 0;
                        if ($parentIdx >= 0) $rangePrefix = $ranges[$parentIdx];
                    }
                }
            });
            // unset unneccessary
            unset($sourceData);
            unset($sourceDataSettings);
        })->export('xlsx');
    }

    /**
     * Helper function to return template attribuets for imported baseModel
     * @return ExcelFile
     */
    public function getTemplateAttributes()
    {
        $attributes = [];
        $sourceData = $this->baseModel->getBufferedAttributeSettings();
        foreach ($sourceData as $key => $value) {
            if ( isset($value['formdisplay']) &&
                 $value['formdisplay'] &&
                 ( (isset($value['initiated']) && !$value['initiated']) ||
                    !isset($value['initiated']))
               ) {
                $attributes[$key] = ucwords(str_replace("_", " ", strtolower($key))); // $value['label'];
            }
        }
        return $attributes;
    }

    /**
     * Get list of relationship field exception
     * @return array of [ $relationshipAttributeName => $relationshipAttributeReplacementField ]
     **/
    public function getImportRelationshipFieldException() {
        return [];
    }

    /**
     * Get list of relationship references masterdata to be added on references sheet
     * @return array of options value
     **/
    public function getImportRelationshipReferencesData() {
        $exceptionFields = is_array($this->getImportRelationshipFieldException()) ? array_keys($this->getImportRelationshipFieldException()) : [];
        $relationshipFields = [];
        $sourceData = $this->baseModel->getBufferedAttributeSettings();
        foreach ($sourceData as $key => $value) {
            if ( !in_array($key, $exceptionFields) &&
                 isset($value['formdisplay']) &&
                 $value['formdisplay'] ) {
                if (( (isset($value['relation']) && !empty($value['relation'])) ||
                    (isset($value['options']) && is_array($value['options']))
                     ) &&
                     ( (isset($value['initiated']) && !$value['initiated']) ||
                        !isset($value['initiated']))
                   ) {
                    if (isset($value['relation']) && !empty($value['relation'])) {
                        $tmpData = $this->baseModel->getRelatedObject($value['relation'])->all();
                        if ($tmpData && count($tmpData) > 0) {
                            foreach ($tmpData as $object) {
                                $relationshipFields[$key][] = $object->id . ":" . $object->getFormattedValue();
                            }
                        } else {
                            $relationshipFields[$key] = [];
                        }
                    } else {
                        $tmpData = $value['options'];
                        if ($tmpData && count($tmpData) > 0) {
                            foreach ($tmpData as $val => $label) {
                                $relationshipFields[$key][] = $val . ":" . $label;
                            }
                        } else {
                            $relationshipFields[$key] = [];
                        }
                    }
                } elseif ($value['type'] == SuitModel::TYPE_BOOLEAN) {
                    $relationshipFields[$key] = [
                        '1:Yes',
                        '0:No'
                    ];
                }
            }
        }
        return $relationshipFields;
    }

    /**
     * Route Implementation to process importing from excel
     * @return Redirect
     */
    public function importFromTemplate()
    {
        $method = request()->get('method');
        $templateAttributes = $this->getTemplateAttributes();
        $modelAttrSettings = $this->baseModel->getBufferedAttributeSettings();

        $excel = $this->getUploadedFile();
        if (!$excel) {
            $this->showNotification(self::ERROR_NOTIFICATION, $this->baseModel->_label . ' Import Failed', 'Error template! Please check if file is corrupt!');
            return redirect()->back();
        }

        // dispatch JOB category_risk
        $this->updateProgress('import_product_imported_count', 0);
        $this->updateProgress('import_product_sources_count', 1000); // initial dummy value
        $targetQueue = config('suitcore.default_import_job_queue', 'default');
        dispatch( (new SuitImportFromTemplateJob(get_class($this->baseRepository), $method, $this->currentImportProcessFileLocation, $templateAttributes))->onQueue($targetQueue) );
        // redirect back
        $this->showNotification(self::NOTICE_NOTIFICATION, $this->baseModel->_label . ' Import Process Started', 'New import ' . $this->baseModel->_label . ' process successfully started.');
        return back()->with('success', 'New import ' . $this->baseModel->_label . ' process successfully started.');

        /*
        DB::beginTransaction();
        try {
            $rowNumber = 0;
            $datas = $excel->chunk(50);
            foreach ($datas as $data) {
                foreach ($data as $row) {
                    $rowNumber++;
                    $objectInstance = $this->baseModel;
                    $keyBaseName = $this->baseModel->getImportExcelKeyBaseName();
                    if (is_array($keyBaseName)) {
                        foreach ($keyBaseName as $key => $value) {
                            if (isset($row[$value])) {
                                $objectInstance = $objectInstance->where($value, $row[$value]);
                            }
                        }
                        $objectInstance = $objectInstance->first();
                    } else {
                        if (isset($row[$keyBaseName])) {
                            $objectInstance = $objectInstance->where($keyBaseName, $row[$keyBaseName])->first();
                        }
                    }

                    // datetime, date and time format validation
                    foreach($row as $key=>$val) {
                        if (is_array($modelAttrSettings) &&
                            isset($modelAttrSettings[$key]) &&
                            isset($modelAttrSettings[$key]['type']) &&
                            !empty($val)) {
                            if ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_DATETIME ||
                                $modelAttrSettings[$key]['type'] == SuitModel::TYPE_DATE ||
                                $modelAttrSettings[$key]['type'] == SuitModel::TYPE_TIME) {
                                $dateConfirm = null;
                                try {
                                    // From Excel : m/d/Y H:i
                                    // Already a Carbon
                                    $dateConfirm = ($val ? $val->format('Y-m-d H:i:s') : null);
                                    $row[$key] = $dateConfirm;
                                    // $dateConfirm = Carbon::createFromFormat('d/m/Y H:i', $val);
                                } catch (\Exception $e) { $dateConfirm = null; }
                                if ($dateConfirm == null || $dateConfirm == false) {
                                    $messages = 'Error in row ' . $rowNumber . ': Date format does not match';
                                    throw new \Exception($messages, 1);
                                }
                            } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_PASSWORD) {
                                $row[$key] = Hash::make($row[$key]);
                            } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_NUMERIC &&
                                !empty($modelAttrSettings[$key]['relation'])) {
                                $row[$key] = explode(':', $row[$key]);
                                try {
                                    $row[$key] = intval(trim($row[$key][0]));
                                } catch (Exception $e) { $row[$key] = 0; }
                            } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_TEXT &&
                                !empty($modelAttrSettings[$key]['options'])) {
                                $row[$key] = explode(':', $row[$key]);
                                $row[$key] = trim($row[$key][0]);
                            } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_BOOLEAN) {
                                $row[$key] = explode(':', $row[$key]);
                                try {
                                    $row[$key] = intval(trim($row[$key][0]));
                                } catch (Exception $e) { $row[$key] = 0; }
                            }
                        }
                    }

                    // Process...
                    if ($objectInstance && $objectInstance->id > 0) {
                        if ($method == self::REPLACE) {
                            // if data virtual account confirmation exist, update
                            $result = $this->baseRepository->update($objectInstance->id, $row->toArray(), $objectInstance);
                        }
                        // else IGNORED
                    } else {
                        // create new record
                        $baseObj = $this->baseModel;
                        $result = $this->baseRepository->create($row->toArray(), $baseObj);
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            if ($e instanceof \PDOException) {
                $this->showNotification(self::ERROR_NOTIFICATION, $this->baseModel->_label . ' Import Failed', 'Error in row #' . $rowNumber . ' : ' . $e->errorInfo[2] . '!');
            } else {
                $this->showNotification(self::ERROR_NOTIFICATION, $this->baseModel->_label . ' Import Failed', 'Error in row #' . $rowNumber . ' : ' . $e->getMessage() . ' !');
            }
            return redirect()->back();
        }
        DB::commit();
        $this->showNotification(self::NOTICE_NOTIFICATION, $this->baseModel->_label . ' Import Success', 'New records successfully imported!');
        return redirect()->back();
        */
    }

    /**
     * Route Implementation to read uploaded template
     * @return ExcelFile
     */
    protected function getUploadedFile()
    {
        $input = request()->all();
        // $destinationPath = 'files/importexcel/'.strtolower(get_class($this->baseModel));
        $destinationPath = 'files/importexcel/'.strtolower((new \ReflectionClass($this->baseModel))->getShortName()); // upload path
        $file = array_get($input, 'file_url');
        if (is_null($file)) {
            Session::flash('error', 'Please select files that want to be imported!');
            return back();
        }
        $extension = $file->getClientOriginalExtension();
        // RENAME THE UPLOAD WITH RANDOM NUMBER
        $fileName = rand(11111, 99999) . '.' . $extension;
        // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
        $isUploadSuccess = $file->move($destinationPath, $fileName);
        $this->currentImportProcessFileLocation = $destinationPath . '/' . $fileName;
        //get by sheet
        return $isUploadSuccess; // Excel::selectSheetsByIndex(0)->load($destinationPath . '/' . $fileName, function () {})->get();
    }

    protected function updateProgress($key, $val, $type = 'cache') {

        $baseKey = md5(get_class($this->baseRepository));
        if ($type == 'cache') {
            Cache::forever($baseKey . '-' . $key, $val);
        } elseif ($type == 'session') {
            Cache::forever((auth()->check() ? auth()->user()->id : 0) . '-' . $baseKey . '-' . $key, $val);
        }
    }

    protected function getProgress($key, $defaultVal = '', $type = 'cache') {

        $baseKey = md5(get_class($this->baseRepository));

        $progress = $defaultVal;
        if ($type == 'cache') {
            $progress = Cache::get($baseKey . '-' . $key, $defaultVal);
        } elseif ($type == 'session') {
            $progress = Cache::get((auth()->check() ? auth()->user()->id : 0) . '-' . $baseKey . '-' . $key, $defaultVal);
        }

        return $progress;
    }

    /**
     * Route Implementation to return progres of import data by excel
     * @return JSON
     */
    public function getImportProgress() {
        $lastImported = $this->getProgress('import_product_imported_count', 0);
        $lastSuccessfullImported = $this->getProgress('import_product_imported_success_count', 0);
        
        $lastImportLog = $this->getProgress('import_product_imported_log', []);
        $renderedLog = "";
        if ($lastImportLog && is_array($lastImportLog) && count($lastImportLog) > 0) {
            $renderedLog .= "<ul>";
            foreach ($lastImportLog as $row => $message) {
                $renderedLog .= "<li><strong>#" . $row . "</strong> : " . $message . "</li>";
            }
            $renderedLog .= "</ul>";
        }
        if (empty($renderedLog)) $renderedLog = "<center>( <i>No Recent Log</i> )</center>";

        $totalSources = $this->getProgress('import_product_sources_count', 0);
        $lastImportMessage = $this->getProgress('import_product_message_result', "no message");
        $importProgress = intval( $totalSources <= 0 ? 0 : (($lastImported / $totalSources) * 100) );
        if ($importProgress > 100) $importProgress = 100;

        // Timing Progress/Remaining (estimated)
        $startTime = Carbon::createFromFormat('Y-m-d H:i:s', $this->getProgress('import_product_start_time', Carbon::now()->subMinutes(1)->toDateTimeString()));
        $now = Carbon::now();
        $difInSeconds = 1 + $now->diffInSeconds($startTime); // preserve 1 second
        $speed = ceil($lastImported / $difInSeconds); // (row / second)
        if ($speed == 0) $speed = 1;
        $estimatedTimeInSeconds = ceil(($totalSources-$lastImported) / $speed); // second(s)
        $progressTimeComponents = array_map(function ($comp) {
            return intval($comp);
        }, explode(':', gmdate("G:i:s", $difInSeconds)));
        $estimatedRemainingTimeComponents = array_map(function ($comp) {
            return intval($comp);
        }, explode(':', gmdate("G:i:s", $estimatedTimeInSeconds)));

        return response()->json([
            'import_product_imported_count' => $lastImported,
            'import_product_imported_success_count' => $lastSuccessfullImported,
            'import_product_imported_log' => $renderedLog,
            'import_product_sources_count' => $totalSources,
            'import_product_message_result' => $lastImportMessage,
            'import_product_progress_percentage' => $importProgress,
            'import_product_progress_time' => $progressTimeComponents,
            'import_product_estimated_remaining_time' => $estimatedRemainingTimeComponents
        ]);
    }

    public function postStopImportProcess() {
        // send signal-stop to related job
        $this->updateProgress('import_product_stop_signal', true);
        $this->showNotification(self::NOTICE_NOTIFICATION, $this->baseModel->_label . ' Import Process Stopped', 'Stop signal had been sent to related process!');
        return redirect()->route($this->routeBaseName . '.template');
    }

    // public function getExcelFile($file) {
    //     $headers = [
    //         'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    //         'Content-Disposition' => 'attachment; filename='. $fileName,
    //     ];

    //     return response()->file($file, $headers);
    // }

    /**
    * get export progress status
    * @return Json
    */
    public function getExportProgress() {
        $lastSuccessfullExported = $this->getProgress('exported_success_count', 0, 'session');
        $totalSource = $this->getProgress('sources_count', 0, 'session');
        $lastFile = $this->getProgress('last-file-' .  str_plural(strtolower($this->baseModel->_label)), null, 'session');
        
        $exportProgress = intval( $totalSource <= 0 ? 0 : (($lastSuccessfullExported / $totalSource) * 100) );
        
        if ($exportProgress >= 100) {
            $exportProgress = 100;
            $this->updateProgress('sources_count', 0, 'session');
            $this->updateProgress('exported_success_count', 0, 'session');
            $this->updateProgress('exported_precentage', 0, 'session');
        }

        return response()->json([
            'exported_success_count' => $lastSuccessfullExported,
            'sources_count' => $totalSource,
            'exported_precentage' => $exportProgress,
            'lastfile' => url($lastFile)
        ]);
    }


    /**
    * stop export process
    * @return void
    */
    public function postStopExportProcess() {
        // send signal-stop to related job
        Cache::forever('stop_export_process',true);
        $this->updateProgress('sources_count', 0, 'session');
        $this->updateProgress('exported_success_count', 0, 'session');
        $this->updateProgress('export_product_stop_signal', true, 'session');
        $this->showNotification(self::NOTICE_NOTIFICATION, $this->baseModel->_label . ' Export Process Stopped', 'Stop signal had been sent to related process!');
        return redirect()->back();
    }

    /**
     * Show notification to users
     * @param int $type
     * @param int $title
     * @param int $message
     * @return void
     */
    protected function showNotification($type, $title, $message) {
        if (!session()->has('webNotification')) {
            session()->put('webNotification', []);
        }
        $webNotification = session()->get('webNotification');
        $webNotification[] = ['type' => $type, 'title' => $title, 'message' => $message];
        session()->put('webNotification', $webNotification);
    }

    protected function setExportSpecificMode($mode = false)
    {
        $this->isSpecificExport = $mode;
    }

    protected function getExportSpecificMode()
    {
        return $this->isSpecificExport;
    }
}
