<?php

namespace Suitcore\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use SuperClosure\Serializer;
use Exception;

abstract class FrontendController extends BaseController
{
    // ATTRIBUTES
    protected $seoTitle;
    protected $seoDescription;
    protected $seoKeywords;
    protected $seoPageImage;
    protected $cachedClosures = [];

    // METHODS
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->detectGlobalParam();
        
        // default-SEO
        $this->seoRepo = app(\Suitcore\Repositories\Contract\SuitUrlSeoRepositoryContract::class);
        $requestedPath = request()->path();
        $relatedSEO = $this->seoRepo->getSEOByUrl($requestedPath == "/" ? $requestedPath : "/" . request()->path());
        if ($relatedSEO) {
            $this->seoTitle = $relatedSEO->title;
            $this->seoDescription = $relatedSEO->description;
            $this->seoKeywords = $relatedSEO->keyword;
        } else {
            $this->seoTitle = settings('site-title', env('APP_NAME', 'Suitcore Instances'));
            $this->seoDescription = str_limit(settings('site-description', env('APP_SIGNATURE', 'Suitcore Instances')), 255);
            $this->seoKeywords = settings('site-keywords', env('APP_SIGNATURE', 'Suitcore Instances'));
        }
        $this->seoPageImage = settings('site_image', asset('assets/img/apple-icon.png'));
        View::share('title', $this->seoTitle);
        View::share('description', $this->seoDescription);
        View::share('keywords', $this->seoKeywords);
        View::share('pageImage', $this->seoPageImage);
    }

    public function isPermittedToReview() {
        return ($this->authUser && $this->authUser->isAdmin());
    }

    /**
     * Setup the layout used by the controller.
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * Detect global parameter if any.
     */
    protected function detectGlobalParam()
    {
        // Global Params
    }

    /**
     * Execute an action on the controller.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        if (config('suitcore.enable_cache_level_controller', false) &&
            is_array($this->cachedClosures) &&
            !empty($this->cachedClosures) && 
            in_array($method, array_keys($this->cachedClosures))) {
            // white-list
            // info('cache-controller-enabled');
            try {
                // Return Cached Data if any
                $requestKey = $this->generateCacheKey([
                    'user_id' => (auth()->check() ? auth()->user()->id : 'guest'),
                    'class' => get_class($this),
                    'method' => $method, 
                    'signature' => $parameters
                ]);
                $cachedData = $this->getCachedData($requestKey, $this->cachedClosures[$method]);
                if ($cachedData !== null) return $cachedData;

                // Process (if cache missed)
                $result = parent::callAction($method, $parameters);
                if ($result instanceof \Illuminate\View\View) {
                    // IF VIEW, Cache The Result and Return
                    return $this->cacheResult($requestKey, ((string) $result), $this->cachedClosures[$method]);
                }
            } catch (\Exception $e) { }
        }
        // return original parent method
        return parent::callAction($method, $parameters);
    }

    /**
     * Make standard general tagging support for all type of cache-drivers (if not supported)
     **/
    /**
     * Tagging a key, mapping a key to specified tag(s)
     * @param  array $tag
     * @param  string $key
     * @return void
     **/
    private function taggingCacheAlternative($tag, $key) {
        $arrTags = is_array($tag) ? $tag : [$tag];
        foreach ($arrTags as $idx => $_tag) {
            $mappedCacheTags = Cache::get('tags-' . $_tag, []);
            $mappedCacheTags[] = $key;
            Cache::forever('tags-' . $_tag, $mappedCacheTags);
        }
    }

    /**
     * Flush tagged key
     * @param  array $tag
     * @return void
     **/
    private function flushTaggingCacheAlternative($tag) {
        $arrTags = is_array($tag) ? $tag : [$tag];
        foreach ($arrTags as $idx => $_tag) { 
            if ($mappedCacheKeys = Cache::get('tags-' . $_tag, [])) {
                foreach ($mappedCacheKeys as $idx => $key) {
                    Cache::forget($key);
                }
            }
        }
    }

    /**
     * Cache Related Helper
     **/
    /**
     * Generate Cache Key based on parameter array
     * @param  array $paramArr
     * @return string
     **/
    protected function generateCacheKey(array $paramArr) {
        $clonedArr = $paramArr;
        ksort($clonedArr); 
        return md5(serialize($clonedArr));
    } 

    /**
     * Cache data wrapper
     * @param  string $key
     * @param  object $data
     * @return object
     **/
    protected function cacheResult($key, $data, array $tag) {
        $result = null;
        if ($data === null) $data = false;
        if ($key) {
            // new way to return null cachedData, since 5.4 and later had not cached null and false
            $result = [
                'cached_data' => $data
            ];
            if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                Cache::tags($tag)->put($key, $result, config('suitcore.expiry_minutes_cache_level_controller', 60));
            } else {
                Cache::put($key, $result, config('suitcore.expiry_minutes_cache_level_controller', 60));
                $this->taggingCacheAlternative($tag, $key);
            }
        }
        return ($result && is_array($result) && isset($result['cached_data']) ? $result['cached_data'] : null);
    }

    /**
     * Return Cached data based on key array
     * @param  string $key
     * @param  string $key
     * @return object
     **/
    protected function getCachedData($key, array $tag) {
        $cachedData = null;
        if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
            $cachedData = Cache::tags($tag)->get($key);
        } else {
            $cachedData = Cache::get($key);
        }
        // new way to return null cachedData, since 5.4 and later had not cached null and false
        if ($cachedData &&
            is_array($cachedData) &&
            isset($cachedData['cached_data'])) {
            return $cachedData['cached_data'];
        }
        // backward compatibility for previous cached data
        if ($cachedData) return $cachedData;
        // if no cached data exist
        return null;
    }
}
