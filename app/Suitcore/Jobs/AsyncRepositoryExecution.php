<?php

namespace Suitcore\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AsyncRepositoryExecution implements ShouldQueue
{
    use Queueable, InteractsWithQueue, SerializesModels;

    protected $repositoryContractClassName;
    protected $methodName;
    protected $arguments;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($repositoryContractClassName, $methodName, $arguments)
    {
        $this->repositoryContractClassName = $repositoryContractClassName;
        $this->methodName = $methodName;
        $this->arguments = $arguments;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // don't catch any exception to generate failed-jobs
        // info('Async Execution : ' . $this->repositoryContractClassName);
        $repo = app($this->repositoryContractClassName);
        $repo->__call($this->methodName, $this->arguments);
    }
}
