<?php

namespace Suitcore\Jobs;

use DB;
use Exception;
use Excel;
use Hash;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Cell_DataValidation;
use PHPExcel_NamedRange;
use Illuminate\Support\Facades\Cache;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Carbon\Carbon;
use Suitcore\Models\SuitModel;

class SuitImportFromTemplateJob implements ShouldQueue
{
    use Queueable, InteractsWithQueue, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 3600; // allowed up to 1 hour for possibly long-task

    protected $repositoryContractClassName;
    protected $baseRepository;
    protected $baseModel;
    protected $method;
    protected $currentImportProcessFileLocation;
    protected $selectedAttrs;
    protected $templateAttributes;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($repositoryContractClassName, $method, $currentImportProcessFileLocation, $templateAttributes, $selectedAttrs = null)
    {
        $this->repositoryContractClassName = $repositoryContractClassName;
        $this->method = $method;
        $this->currentImportProcessFileLocation = $currentImportProcessFileLocation;
        $this->templateAttributes = $templateAttributes;
        $this->selectedAttrs = $selectedAttrs;
    }

    protected function getProgress($key, $defaultVal = '') {
        $baseKey = md5(get_class($this->baseRepository));
        return Cache::get($baseKey . '-' . $key, $defaultVal);
    }

    public function updateProgress($key, $val) {
        $baseKey = md5(get_class($this->baseRepository));
        Cache::forever($baseKey . '-' . $key, $val);
        unset($baseKey);
    }

    protected function preProcessRow(&$row) {
        // Preprocess row if any
    }

    protected function postProcessRow(&$baseObject) {
        // Postprocess row if any
    }

    protected function preProcessBeforeUpdate(&$row, &$objectInstance) {
        // Preprocess row if any
    }

    protected function preProcessBeforeCreate(&$row, &$objectInstance) {
        // Preprocess row if any
    }

    protected function preProcess() {
        // Function that called before importing
    }

    protected function postProcess() {
        // Function that called after importing
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        try {
            // as-contract
            $this->baseRepository = app($this->repositoryContractClassName);
        } catch (\Exception $e) { }
        if ($this->baseRepository == null) {
            try {
                // as-class
                $this->baseRepository = new $this->repositoryContractClassName;
            } catch (\Exception $e) { }
        }
        if ($this->baseRepository == null) {
            info(__CLASS__.'@'.__METHOD__, ["import-process-log" => 'Repository Not Found!']);
            return;
        }

        $this->baseModel = $this->baseRepository->getMainModel();

        /*
        info(__CLASS__.'@'.__METHOD__, [
            'method' => $this->method,
            'baseRepository' => $this->baseRepository,
            'baseModel' => $this->baseModel,
            'currentImportProcessFileLocation' => $this->currentImportProcessFileLocation
        ]);
        */

        $method = $this->method;
        info(__CLASS__.'@'.__METHOD__, ["import-process-log" => 'Starting import ' . $this->baseModel->_label . ' data with ' . $method . ' strategy ...']);

        $modelAttrSettings = $this->baseModel->getBufferedAttributeSettings();

        // Starting Process
        ini_set('max_execution_time', 0); // unlimited
        ini_set('memory_limit', '-1'); // unlimited
        $excel = null;
        if ($this->currentImportProcessFileLocation) {
            try {
                // Get first sheet
                $excel = Excel::selectSheetsByIndex(0)->load('public/'.$this->currentImportProcessFileLocation, function () {})->get();
            } catch (\Exception $e) {
                $excel = null;
            }
        }
        if ($excel == null || $excel->first() == null) {
            $resultMessage = $this->baseModel->_label . ' Import Failed : Error template! Please check if header column is exist and data is available!';
            info(__CLASS__.'@'.__METHOD__, ["process-error" => $resultMessage]);
            // Update Progress
            $this->updateProgress('import_product_sources_count', 0);
            $this->updateProgress('import_product_message_result', '[' . Carbon::now()->toDateTimeString() . '] ' . $resultMessage);
            // free-memory
            $method = null;
            $modelAttrSettings = null;
            $excel = null;
            gc_collect_cycles();
            // return
            return;
        }
        $tableHeader = array_keys($excel->first()->toArray());

        $checkResult = true;
        foreach ($tableHeader as $key => $value) {
            // ignore corrupt / null value
            if (!in_array($value, array_keys($this->templateAttributes)) && $value != 0) {
                $checkResult = false;
            }
        }
        if ($checkResult == false) {
            $resultMessage = $this->baseModel->_label . ' Import Failed : Your file should contain the following column: ' . implode(', ', $this->templateAttributes);
            info(__CLASS__.'@'.__METHOD__, ["process-error" => $resultMessage]);
            // Update Progress
            $this->updateProgress('import_product_sources_count', 0);
            $this->updateProgress('import_product_message_result', '[' . Carbon::now()->toDateTimeString() . '] ' . $resultMessage);
            // free-memory
            $method = null;
            $modelAttrSettings = null;
            $excel = null;
            gc_collect_cycles();
            // return
            return;
        }

        $nbRows = $excel->count();
        $startTime = Carbon::now();

        // Reset Statistic
        $this->updateProgress('import_product_imported_count', 0);
        $this->updateProgress('import_product_imported_success_count', 0);
        $this->updateProgress('import_product_imported_log', []);
        $this->updateProgress('import_product_sources_count', $nbRows);
        $this->updateProgress('import_product_message_result', "Importing product ...");
        $this->updateProgress('import_product_start_time', $startTime->toDateTimeString());

        // Preparation and Processing
        $rowNumber = 0;
        $successRowNumber = 0;
        try {
            // Separated in several data-chunk
            $datas = $excel->chunk(50);

            // import alias binding
            $importAliases = [];
            foreach($modelAttrSettings as $attrKey => $modelAttrSetting){
                if(isset($modelAttrSetting['importAlias'])){
                    $importAliases[$modelAttrSetting['importAlias']] = $attrKey;
                }
            }

            // override keybasename
            $keyBaseName = [];
            foreach($this->baseModel->getImportExcelKeyBaseName() as $keyBase){
                $keyBaseName[] = isset($importAliases[$keyBase]) ? $importAliases[$keyBase] : $keyBase;
            }

            // merge basekey with selected attrs
            if(is_array($this->selectedAttrs)){
                $this->selectedAttrs = array_merge($this->selectedAttrs, $keyBaseName);
            }
            
            // Process
            $importLog = [];
            $this->preProcess();
            foreach ($datas as $data) {
                foreach ($data as $row) {
                    $rowNumber++;
                    $objectInstance = $this->baseModel;
                    // Check For Stop Action
                    $stopSignal = $this->getProgress('import_product_stop_signal', false);
                    if ($stopSignal) {
                        $resultMessage = "Process stopped at row #" . $rowNumber . ", where " . $successRowNumber . " row(s) had successfully imported from total " . $rowNumber . " row(s) processed!"; 
                        info(__CLASS__.'@'.__METHOD__, ["import-process-log" => $resultMessage]);
                        $this->updateProgress('import_product_sources_count', 0);
                        $this->updateProgress('import_product_message_result', '[' . Carbon::now()->toDateTimeString() . '] ' . $resultMessage);
                        $this->updateProgress('import_product_stop_signal', false);
                        // free-memory
                        $method = null;
                        $modelAttrSettings = null;
                        $excel = null;
                        $tableHeader = null;
                        gc_collect_cycles();
                        // return
                        return;
                    }
                    // End Check For Stop Action
                    try {
                        // pre-process
                        $this->preProcessRow($row);

                        // datetime, date and time format validation
                        foreach($row as $key=>$val) {
                            if (is_array($modelAttrSettings) &&
                                isset($modelAttrSettings[$key]) &&
                                isset($modelAttrSettings[$key]['type']) &&
                                !empty($val)) {
                                if ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_DATETIME ||
                                    $modelAttrSettings[$key]['type'] == SuitModel::TYPE_DATE ||
                                    $modelAttrSettings[$key]['type'] == SuitModel::TYPE_TIME) {
                                    $dateConfirm = null;
                                    try {
                                        // From Excel : m/d/Y H:i
                                        // Already a Carbon
                                        $dateConfirm = ($val ? $val->format('Y-m-d H:i:s') : null);
                                        $row[$key] = $dateConfirm;
                                        // $dateConfirm = Carbon::createFromFormat('d/m/Y H:i', $val);
                                    } catch (\Exception $e) { $dateConfirm = null; }
                                    if ($dateConfirm == null || $dateConfirm == false) {
                                        throw new \Exception('Error in row ' . $rowNumber . ': Date format does not match', 1);
                                    }
                                } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_PASSWORD) {
                                    $row[$key] = Hash::make($row[$key]);
                                } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_NUMERIC &&
                                    !empty($modelAttrSettings[$key]['relation'])) {
                                    $row[$key] = explode(':', $row[$key]);
                                    try {
                                        $row[$key] = intval(trim($row[$key][0]));
                                    } catch (\Exception $e) { $row[$key] = 0; }
                                } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_TEXT &&
                                    !empty($modelAttrSettings[$key]['options'])) {
                                    $row[$key] = explode(':', $row[$key]);
                                    $row[$key] = trim($row[$key][0]);
                                } elseif ($modelAttrSettings[$key]['type'] == SuitModel::TYPE_BOOLEAN) {
                                    $row[$key] = explode(':', $row[$key]);
                                    try {
                                        $row[$key] = intval(trim($row[$key][0]));
                                    } catch (\Exception $e) { $row[$key] = 0; }
                                }
                            }

                            // alias modifier
                            if(isset($importAliases[$key])){
                                $row = $row->put($importAliases[$key], $val);
                                $row->forget($key);
                            }

                            // checkbox template blade
                            if(is_array($this->selectedAttrs) && !in_array($key, $this->selectedAttrs)){
                                $row->forget($key);
                            }
                        }

                        if (is_array($keyBaseName)) {
                            foreach ($keyBaseName as $key => $value) {
                                if (isset($row[$value])) {
                                    $objectInstance = $objectInstance->where($value, $row[$value]);
                                }
                            }
                            $objectInstance = $objectInstance->first();
                        } else {
                            if (isset($row[$keyBaseName])) {
                                $objectInstance = $objectInstance->where($keyBaseName, $row[$keyBaseName])->first();
                            }
                        }

                        // Process...
                        $result = false;
                        if ($objectInstance && $objectInstance->id > 0) {
                            if ($method == 'replace') {
                                // if data virtual account confirmation exist, update
                                $this->preProcessBeforeUpdate($row, $objectInstance);
                                $result = $this->baseRepository->update($objectInstance->id, $row->toArray(), $objectInstance);
                            } else {
                                $result = true; // successfull process but ignored (based on given parameter)
                            }
                        } else {
                            // create new record
                            $objectInstance = $this->baseModel;
                            $this->preProcessBeforeCreate($row, $objectInstance);
                            $result = $this->baseRepository->create($row->toArray(), $objectInstance);
                        }
                        if ($result) {
                            // post-process
                            $this->postProcessRow($objectInstance);
                            // continue
                            $successRowNumber++;
                            $this->updateProgress('import_product_imported_success_count', $successRowNumber);
                            $importLog[$rowNumber] = "Imported";
                            info(__CLASS__.'@'.__METHOD__, ["import-process-log" => '#' . $rowNumber . ' : ' . $importLog[$rowNumber]]);
                        } else {
                            $importErrorMessage = 'An error occured when importing!';
                            if ($objectInstance) {
                                $importErrorMessage = implode(", ", array_map(function ($item) {
                                    return ($item ? (is_array($item) ? implode(", ", $item) : $item) : "Data Invalid");
                                }, ($objectInstance->errors ? $objectInstance->errors->toArray() : ["Data Invalid"])));
                            }
                            $importLog[$rowNumber] = 'Not Imported! ' . $importErrorMessage;
                            info(__CLASS__.'@'.__METHOD__, ["import-process-log" => '#' . $rowNumber . ' : ' . $importLog[$rowNumber]]);
                        }
                        // free-up memory
                        $result = null;
                    } catch (\Exception $e) {
                        $importErrorMessage = "";
                        if ($e instanceof \PDOException) {
                            $importErrorMessage = $e->errorInfo[2];
                        } else {
                            $importErrorMessage = $e->getMessage();
                        }
                        $importLog[$rowNumber] = 'Not Imported! ' . $importErrorMessage;
                        info(__CLASS__.'@'.__METHOD__, ["import-process-log" => '#' . $rowNumber . ' : ' . $importLog[$rowNumber]]);
                    }
                    // log import progress count
                    $this->updateProgress('import_product_imported_count', $rowNumber);
                    $this->updateProgress('import_product_imported_log', $importLog);
                    // re-init log if somehow cache cleared
                    $this->updateProgress('import_product_sources_count', $nbRows);
                    $this->updateProgress('import_product_message_result', "Importing product ...");
                    $this->updateProgress('import_product_start_time', $startTime->toDateTimeString());
                    // free-up memory
                    $objectInstance = null;
                    gc_collect_cycles();
                    // sleep for a while, give some time for garbage-collector and another process / write-query for same table
                    usleep(2000); // 2ms
                } 
            }
        } catch (\Exception $e2) {
            $importErrorMessage = $e2->getMessage();
            info(__CLASS__.'@'.__METHOD__, ["import-process-log" => $importErrorMessage]);
            $this->updateProgress('import_product_sources_count', 0);
            $this->updateProgress('import_product_message_result', '[' . Carbon::now()->toDateTimeString() . '] ' . $importErrorMessage);
            // free-memory
            $method = null;
            $modelAttrSettings = null;
            $excel = null;
            $tableHeader = null;
            gc_collect_cycles();
            // return
            return;
        }
        $this->postProcess();

        $resultMessage = "All rows had processed, where " . $successRowNumber . " row(s) had successfully imported from total " . $rowNumber . " row(s) processed!"; 
        info(__CLASS__.'@'.__METHOD__, ["import-process-log" => $resultMessage]);
        $this->updateProgress('import_product_sources_count', 0);
        $this->updateProgress('import_product_message_result', '[' . Carbon::now()->toDateTimeString() . '] ' . $resultMessage);
        // free-memory
        $method = null;
        $modelAttrSettings = null;
        $excel = null;
        $tableHeader = null;
        gc_collect_cycles();
        // return
        return;
    }
}
