<?php

namespace Suitcore\Jobs;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\File;
use Cache;

class SuitExportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600; // allowed up to 1 hour for possibly long-task

    protected $repositoryContractClassName;
    protected $baseRepository;
    protected $baseModel;
    protected $specificFilter;
    protected $fileDest;
    protected $folderFile;
    protected $userId;
    protected $datatableSpecificFilters;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($repositoryContractClassName, $folderFile, $fileDest, $userId, $specificFilter = null, $datatableSpecificFilters = null)
    {
        $this->repositoryContractClassName = $repositoryContractClassName;
        $this->specificFilter = $specificFilter;
        $this->fileDest = $fileDest;
        $this->folderFile = $folderFile;
        $this->userId = $userId;
        $this->datatableSpecificFilters = $datatableSpecificFilters;
    }

    protected function getProgress($key, $defaultVal = '') {
        $baseKey = md5(get_class($this->baseRepository));
        return Cache::get($this->userId . '-' . $baseKey . '-' . $key, $defaultVal);
    }

    public function updateProgress($key, $val) {
        $baseKey = md5(get_class($this->baseRepository));
        Cache::forever($this->userId .'-'. $baseKey . '-' . $key, $val);
        unset($baseKey);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            // as-contract
            $this->baseRepository = app($this->repositoryContractClassName);
        } catch (\Exception $e) { }
        if ($this->baseRepository == null) {
            try {
                // as-class
                $this->baseRepository = new $this->repositoryContractClassName;
            } catch (\Exception $e) { }
        }
        if ($this->baseRepository == null) {
            info(__CLASS__.'@'.__METHOD__, ["export-process-log" => 'Repository Not Found!']);
            return;
        }

        $this->updateProgress('sources_count', 0);
        $this->updateProgress('exported_success_count', 0);

        $this->baseModel = $this->baseRepository->getMainModel();

        if ($this->baseModel) {
            $baseObj = $this->baseModel;
            $specificFilter = $this->specificFilter;
            $dataSpecificFilter = ($specificFilter && is_array($specificFilter) ? $specificFilter : []);
            // CAPTURE DATATABLE FILTER IF ANY
            $datatableSpecificFilters = $this->datatableSpecificFilters;
            if ($datatableSpecificFilters &&
                is_array($datatableSpecificFilters) &&
                count($datatableSpecificFilters) > 0) {
                $attrSettings = $baseObj->attribute_settings;
                foreach ($datatableSpecificFilters as $key => $value) {
                    $estimatedMainObjectRealKey = str_replace($baseObj->getTable().".", "", $key);
                    if (isset($attrSettings[$estimatedMainObjectRealKey])) {
                        $dataSpecificFilter[$key] = $value;
                    }
                }
            }

            // Timezone
            $databaseTz = config('app.timezone'); 
            $defaultUserTz = config('suitcore.default_user_timezone'); // default-user-timezone
            $userTz = $defaultUserTz;
            if ($currentAuthUser = auth()->user()) {
                if (!empty($currentAuthUser->timezone)) {
                    $userTz = $currentAuthUser->timezone;
                }
            }

            // BOX SPOUT WAY

            $writer = WriterFactory::create(Type::XLSX); // available :  XLSX / CSV / ODS

            if (!File::exists($this->fileDest)) {
                if (!File::exists($this->folderFile)) {
                    File::makeDirectory($this->folderFile, 0755, true);
                }
                File::put($this->fileDest, '');
            }

            $writer->openToFile($this->fileDest); // file or phpStream
            // $writer->openToBrowser($fileName); // stream data directly to the browser

            // Customizing the sheet name when writing
            $sheet = $writer->getCurrentSheet();
            $sheet->setName('Data');

            $columnHeader = [];
            foreach ($baseObj->getBufferedAttributeSettings() as $key=>$val) {
                if ( $val['visible'] ) {
                    $columnHeader[] = $val['label'];
                }
            }
            $writer->addRow($columnHeader); // Header - add a row at a time
            $objectList = $baseObj->select($baseObj->getTable().".*");
            if ($dataSpecificFilter != null && count($dataSpecificFilter) > 0) {
                $objectList->where(function($query) use ($dataSpecificFilter, $baseObj, $userTz, $defaultUserTz, $databaseTz) {
                    $attrSettings = $baseObj->attribute_settings;
                    foreach($dataSpecificFilter as $key=>$val) {
                        $estimatedMainObjectRealKey = str_replace($baseObj->getTable().".", "", $key);
                        if (isset($attrSettings[$estimatedMainObjectRealKey]) &&
                            isset($attrSettings[$estimatedMainObjectRealKey]['type']) &&
                            in_array($attrSettings[$estimatedMainObjectRealKey]['type'], [SuitModel::TYPE_DATETIME, SuitModel::TYPE_DATE]) &&
                            str_contains($val, "-yadcf_delim-") ) {
                            $ranges = explode("-yadcf_delim-", $val);
                            $currentAttrSetting = $attrSettings[$estimatedMainObjectRealKey];
                            $ranges = array_map(function($arrValue) use ($currentAttrSetting, $userTz, $defaultUserTz, $databaseTz) {
                                if ($currentAttrSetting['type'] == SuitModel::TYPE_DATETIME) {
                                    if ( ($timestamp = strtotime($arrValue)) !== false ) {
                                        $inputDate = Carbon::createFromTimestamp($timestamp);
                                        try {
                                            $inputDate = Carbon::createFromFormat('Y-m-d H:i:s', $inputDate->toDateTimeString(), $userTz);
                                        } catch (Exception $e) {
                                            try {
                                                $inputDate = Carbon::createFromFormat('Y-m-d H:i:s', $inputDate->toDateTimeString(), $defaultUserTz);
                                            } catch (Exception $e) { }
                                        }
                                        return $inputDate->tz($databaseTz)->toDateTimeString();
                                    }
                                }
                                return $arrValue;
                            }, $ranges);
                            if (count($ranges) == 2 &&
                                !empty($ranges[0]) &&
                                !empty($ranges[1])) {
                                $query->where($key,">=",$ranges[0]);
                                $query->where($key,"<=",$ranges[1]);
                            } elseif (!empty($ranges[0]) &&
                                empty($ranges[1])) {
                                $query->where($key, ">=", $ranges[0]);
                            } elseif (empty($ranges[0]) &&
                                !empty($ranges[1])) {
                                $query->where($key, "<=", $ranges[1]);
                            } 
                        } elseif (!str_contains($val, "-yadcf_delim-")) {
                            if (is_array($val) && !empty($val)) {
                                $query->whereIn($key, $val);
                            } else {
                                $query->where($key,"=",$val);
                            }
                        }
                    }
                });
            }

            // set unlimited time and fixed minimum-memory resource from current-config
            ini_set('max_execution_time', 0);
            ini_set('memory_limit','512M');
            // process
            $this->updateProgress('sources_count', $objectList->count());
            $totalCount = $this->getProgress('sources_count');
            $chunkCount =  100;
            $objectList->chunk($chunkCount, function($objects) use ($writer, $specificFilter, $chunkCount, $totalCount) {
                // init data
                $data = [];
                $stopSignal = Cache::get('stop_export_process');
                foreach ($objects as $object) {
                    $tmpRow = [];
                    foreach ($object->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                        if ($attrSettings['visible']) {
                            if ($specificFilter == null ||
                                !is_array($specificFilter) ||
                                !isset($specificFilter[$attrName])) {
                                $tmpRow[] = $object->renderRawAttribute($attrName);
                            }
                        }
                    }
                    $data[] = $tmpRow;
                }
                // Write Rows
                $writer->addRows($data); // add multiple rows at a time
                $prevExported = $this->getProgress('exported_success_count', 0);
                $successExported = $prevExported + $chunkCount;
                $successExported = $successExported >= $totalCount ? $totalCount : $successExported;
                $this->updateProgress('exported_success_count', $successExported);
            });

            Cache::forever('stop_export_process', false);
            // Close Writer
            $writer->close();
            // reset
            // return
            return;
            /*
            // MAATWEBSITE EXCEL WAY
            $currentMaxTimeLimit = ini_get('max_execution_time');
            ini_set('max_execution_time', 0);
            Excel::create(str_replace(" ", "", $baseObj->_label)  . 'Data', function ($excel) {
                // Set sheets
                $excel->sheet('Data', function ($sheet) {
                    $idx = 1;
                    $objectList = $baseObj->select($baseObj->getTable().".*");
                    if ($specificFilter != null && count($specificFilter) > 0) {
                        $objectList->where(function($query) use ($specificFilter) {
                            foreach($specificFilter as $key=>$val) {
                                $query->where($key, "=", $val);
                            }
                        });
                    }
                    $objectList->chunk(100, function($objects) use ($idx, $sheet) {
                        // reinit
                        $data = [];
                        // header if needed
                        if ($idx == 1 ){
                            $data[0] = [];
                            foreach ($baseObj->getBufferedAttributeSettings() as $key=>$val) {
                                if ( $val['visible'] ) {
                                    $data[0][] = $val['label'];
                                }
                            }
                        }
                        // write data
                        foreach ($objects as $object) {
                            $tmpRow = [];
                            foreach ($object->getBufferedAttributeSettings() as $attrName=>$attrSettings) {
                                if ($attrSettings['visible']) {
                                    if ($specificFilter == null ||
                                        !is_array($specificFilter) ||
                                        !isset($specificFilter[$attrName])) {
                                        $tmpRow[] = $object->renderRawAttribute($attrName);
                                    }
                                }
                            }
                            $data[] = $tmpRow;
                        }
                        // Sheet manipulation
                        $sheet->fromArray($data, null, 'A' . $idx, false, false);
                        // increment index
                        $idx += ($idx == 1 ? 101 : 100);
                    });

                    // Sheet further manipulation
                    // $sheet->fromArray($data, null, 'A1', false, false);
                    $sheet->cells('A1:Z1', function ($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->freezeFirstRow();

                });
            })->export('xlsx'); //*.xls only support until 65.536 rows
            ini_set('max_execution_time', $currentMaxTimeLimit);
            */
        }
    }
}
