<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        // User Service
        \Suitcore\Repositories\Contract\SuitUserRepositoryContract::class => \Suitcore\Repositories\SuitUserRepository::class,

        // Role Service
        \Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract::class => \Suitcore\Repositories\SuitUserRoleRepository::class,

        // Permission Service
        \Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract::class => \Suitcore\Repositories\SuitUserPermissionRepository::class,

        // Setting Service
        \Suitcore\Repositories\Contract\SuitSettingRepositoryContract::class => \Suitcore\Repositories\SuitSettingRepository::class,

        // Email Notification Templating Service
        \Suitcore\Repositories\Contract\SuitEmailTemplateRepositoryContract::class => \Suitcore\Repositories\SuitEmailTemplateRepository::class,

        // User Setting Service
        \Suitcore\Repositories\Contract\SuitUserSettingRepositoryContract::class => \Suitcore\Repositories\SuitUserSettingRepository::class,

        // Notification Service
        \Suitcore\Repositories\Contract\SuitNotificationRepositoryContract::class => \Suitcore\Repositories\SuitNotificationRepository::class,

        // User Token Service (for Rest-API Access)
        \Suitcore\Repositories\Contract\SuitUserTokenRepositoryContract::class => \Suitcore\Repositories\SuitUserTokenRepository::class,

        // Suit URL Redirection Repository Service
        \Suitcore\Repositories\Contract\SuitUrlRedirectionRepositoryContract::class => \Suitcore\Repositories\SuitUrlRedirectionRepository::class,

        // Suit URL SEO Repository Service
        \Suitcore\Repositories\Contract\SuitUrlSeoRepositoryContract::class => \Suitcore\Repositories\SuitUrlSeoRepository::class,

        // Suit Base Repository Service
        \Suitcore\Repositories\Contract\SuitRepositoryContract::class => \Suitcore\Repositories\SuitRepository::class,

        // Email Verification Repository Service
        \Suitcore\Repositories\Contract\EmailVerificationRepositoryContract::class => \Suitcore\Repositories\EmailVerificationRepository::class,

        // Oauth Repository Service
        \Suitcore\Repositories\Contract\SuitOauthUserRepositoryContract::class => \Suitcore\Repositories\SuitOauthUserRepository::class,

        // Oauth Repository Service
        \Suitcore\Repositories\Contract\SuitPasswordResetRepositoryContract::class => \Suitcore\Repositories\SuitPasswordResetRepository::class,
    ],

    'data_options_route_name' => [
        'suituser' => ''
    ],

    'admin_panel_form_input_partial_views' => [ 
        'text' => 'suitcore.metronics1.partials.inputtext',
        'password' => 'suitcore.metronics1.partials.inputpassword',
        'numeric' => 'suitcore.metronics1.partials.inputnumeric',
        'float' => 'suitcore.metronics1.partials.inputfloat',
        'boolean' => 'suitcore.metronics1.partials.inputboolean',
        'datetime' => 'suitcore.metronics1.partials.inputdatetime',
        'date' => 'suitcore.metronics1.partials.inputdate',
        'time' => 'suitcore.metronics1.partials.inputtime',
        'textarea' => 'suitcore.metronics1.partials.inputtextarea',
        'richtextarea' => 'suitcore.metronics1.partials.inputrichtextarea',
        'file' => 'suitcore.metronics1.partials.inputfile',
        'select' => 'suitcore.metronics1.partials.inputselect', 
        'referenceslabel' => 'suitcore.metronics1.partials.inputreferenceslabel', 
        'group-map' => 'suitcore.metronics1.partials.inputmap', 
    ],

    'admin_panel_datatable_partial_views' => [ 
        'tableselection' => 'suitcore.metronics1.partials.tableselection',
        'tablemenu' => 'suitcore.metronics1.partials.tablemenu',
    ],

    'default_thumbnail_dimension' => [
        'small_square' => '128x128',
        'medium_square' => '256x256',
        'large_square' => '512x512',
        'xlarge_square' => '2048x2048',
        'small_cover' => '240x_',
        'normal_cover' => '360x_',
        'medium_cover' => '480x_',
        'large_cover' => '1280x_',
        'small_banner' => '_x240',
        'normal_banner' => '_x360',
        'medium_banner' => '_x480',
        'large_banner' => '_x1280'
    ],

    "metrics" => [
        "currency" => "IDR",
        "weight"   => "gram",
        "length"   => "meter",
        "quantity" => "pcs"
    ],

    "paginations" => [
        "perPage" => 10,
    ],
    
    "backendNavigation" => [
        'A1' => [
            'label'   => 'Dashboard',
            'route'   => 'backend.home.index',
            'icon'    => 'icon-bar-chart',
            'roles'   => ['admin'],
            'submenu' => []
        ],
        'C' => [
            'label'   => 'User',
            'route'   => '',
            'icon'    => 'icon-users',
            'roles'   => ['admin'],
            'submenu' => [
                'C1'  => [
                    'label' => 'User Management',
                    'route' => 'backend.user.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'C2'  => [
                    'label' => 'Role Management',
                    'route' => 'backend.role.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'C3'  => [
                    'label' => 'Permission Management',
                    'route' => 'backend.permission.acl',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ]
            ]
        ],
        'B' => [
            'label'   => 'Marketing',
            'route'   => '',
            'icon'    => 'icon-briefcase',
            'roles'   => ['admin'],
            'submenu' => [
                'B1'  => [
                    'label' => 'Email Subscriber',
                    'route' => 'backend.newslettersubscribers.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'B2'  => [
                    'label' => 'Newsletter',
                    'route' => 'backend.newsletter.index',
                    'icon'  => 'fa fa-envelope',
                    'roles' => ['admin']
                ]
            ]
        ],
        'E' => [
            'label'   => 'Site',
            'route'   => '',
            'icon'    => 'icon-globe',
            'roles'   => ['admin'],
            'submenu' => [
                'E8' => [
                    'label' => 'Menu',
                    'route' => 'backend.menu.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E9' => [
                    'label' => 'Banner',
                    'route' => 'backend.banner.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E1' => [
                    'label' => 'Content Type & Category',
                    'route' => 'backend.contenttype.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E2' => [
                    'label' => 'Content',
                    'route' => 'backend.content.index',
                    'icon'  => 'icon-note',
                    'roles' => ['admin'],
                ],
                'E4' => [
                    'label' => 'Contact Message',
                    'route' => 'backend.contactmessage.index',
                    'icon'  => 'fa fa-comment-o',
                    'roles' => ['admin'],
                ],
                'E5' => [
                    'label' => 'FAQ Category',
                    'route' => 'backend.faqcategory.index',
                    'icon'  => 'icon-question',
                    'roles' => ['admin'],
                ],
                'E6' => [
                    'label' => 'FAQ',
                    'route' => 'backend.faq.index',
                    'icon'  => 'icon-question',
                    'roles' => ['admin'],
                ],
                'E7' => [
                    'label' => 'Setting',
                    'route' => 'backend.settings.view',
                    'icon'  => 'icon-settings',
                    'roles' => ['admin'],
                ]
            ]
        ],
        'F' => [
            'label'   => 'File Manager',
            'route'   => 'elfinder.index',
            'icon'    => 'fa fa-folder-open',
            'roles'   => ['admin'],
            'submenu' => []
        ]
    ]
];
