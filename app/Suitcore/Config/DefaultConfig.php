<?php

namespace Suitcore\Config;

class DefaultConfig
{
    public static $data = [
        "metrics" => [
            "currency" => "IDR",
            "weight"   => "gram",
            "length"   => "meter",
            "quantity" => "pcs"
        ],
        "paginations" => [
            "perPage" => 10,
        ],
        "user_email_notification" => [
            'UserActivated' => 'emails.welcome',
            'UserRegistered' => 'emails.activation'
        ],
        "backendNavigation" => []
    ];

    public static function getConfig()
    {
        $instancesConfig = [];
        // priority 1 (backward-compatibility with old-version)
        if (class_exists('\\App\\Config\\SuitcoreConfig')) {
            if (is_array( \App\Config\SuitcoreConfig::$data )) {
                $instancesConfig = \App\Config\SuitcoreConfig::$data;
            }
        }
        // priority 2
        $suitcoreConfig = config('suitcore');
        // priority 3 (default value) : self::$data
        return array_merge( self::$data, $suitcoreConfig, $instancesConfig );
    }
}
