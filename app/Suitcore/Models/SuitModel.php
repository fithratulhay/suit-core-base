<?php

namespace Suitcore\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Jenssegers\Date\Date;
use Spatie\Varnish\Varnish;
use Suitcore\Currency\ModelCurrencyTrait;
use Suitcore\Models\SuitTranslation;
use Suitcore\Thumbnailer\Contracts\Model\ImageThumbnailerInterface;
use Suitcore\Thumbnailer\Model\ThumbnailerTrait;
use Suitcore\Uploader\Contracts\ModelUploaderInterface;
use Suitcore\Uploader\ModelUploaderTrait;

class SuitModel extends Model implements ImageThumbnailerInterface, ModelUploaderInterface
{
    /* TRAIT */
    use ThumbnailerTrait {
        getAttribute as thumbnailGetAttribute;
    }
    use ModelUploaderTrait, ModelCurrencyTrait, TransformableToApi;

    /* STATIC */
    public static $thumbnailClass = 'thumbnail';
    public static $partialFormView = [];

    /* CONSTANT */
    const TYPE_TEXT = "text";
    const TYPE_PASSWORD = "password";
    const TYPE_NUMERIC = "numeric";
    const TYPE_FLOAT = "float";
    const TYPE_BOOLEAN = "boolean";
    const TYPE_DATETIME = "datetime";
    const TYPE_DATE = "date";
    const TYPE_TIME = "time";
    const TYPE_TEXTAREA = "textarea";
    const TYPE_RICHTEXTAREA = "richtextarea";
    const TYPE_FILE = "file";
    // -- for rendering
    const TYPE_SELECT = "select";
    const TYPE_REFERENCES_LABEL = "referenceslabel";
    // -- for group-type
    const GROUPTYPE_MAP = "group-map";
    const GROUPTYPE_NUMERICRANGE = "group-numeric";
    const GROUPTYPE_DATERANGE = "group-date";
    const GROUPTYPE_DATETIMERANGE = "group-datetime";
    const GROUPTYPE_TIMERANGE = "group-time";
    // -- for group-role of group-type
    const GROUPROLE_MAP_LOCATIONNAME = "map-location-name";
    const GROUPROLE_MAP_LATITUDE = "map-latitude";
    const GROUPROLE_MAP_LONGITUDE = "map-longitude";
    const GROUPROLE_RANGE_START = "range-start";
    const GROUPROLE_RANGE_END = "range-end";

    /* ATTRIBUTES */
    protected $defWidth = 480;// landscape
    protected $defHeight = 360; // portrait
    protected $thumbnailStyle = [];
    protected $extendedThumbnailStyle = [];
    protected $imageAttributes = [];
    protected $defThumbnailName = '_thumbnail';
    protected $baseFolder = 'public/files';
    protected $fillable = [];
    public $rules = [];
    public $errors;
    protected $attributeSettingsCustomState = null;
    protected static $bufferAttributeSettings = null;
    public static $isFormGeneratorContext = true;
    public $nodeClassName = '';
    public $nodeFullClassName = '';
    public $showAllOptions = false;
    public $uploadError = null;
    protected $elementGroup = [];

    public $manyRelationships = [];

    /* METHODS */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        // init base attribute-settings
        if (config('suitcore.auto_init_attribute_settings', false)) {
            $attributSettings = $this->getBufferedAttributeSettings();
            $initFillable = ($this->fillable == null || empty($this->fillable));
            $initRules = ($this->rules == null || empty($this->rules));
            $initFiles = ($this->files == null || empty($this->files)); // File Upload Folder
            $initImageAttributes = ($this->imageAttributes == null || empty($this->imageAttributes)); // Image Fields
            $initCasts = ($this->files == null || empty($this->files));
            foreach ($attributSettings as $key => $value) {
                // fillable auto-init
                if ($initFillable && $key != "id") {
                    $this->fillable[] = $key;
                }
                // rules-set init
                if ($initRules) {
                }
            }
        }
        // get node class name
        $this->nodeFullClassName = get_class($this);
        $this->nodeClassName = strtolower((new \ReflectionClass($this))->getShortName());
        // default thumbnail dimension from suitcommerce
        $this->thumbnailStyle = array_merge(config('suitcore.default_thumbnail_dimension'), $this->extendedThumbnailStyle);
        // get custom image dimension config if any
        $defaultConfigClassName = '\Suitcore\Config\DefaultConfig';
        if (class_exists($defaultConfigClassName)) {
            $defaultConfig = $defaultConfigClassName::getConfig();
            if (isset($defaultConfig['thumbnail_dimension']) &&
                isset($defaultConfig['thumbnail_dimension'][$this->nodeClassName])) {
                $this->thumbnailStyle = $defaultConfig['thumbnail_dimension'][$this->nodeClassName];
            }
        }
        // load partial form view
        $partialFormViewConfig = config('suitcore.admin_panel_form_input_partial_views');
        self::$partialFormView = [
            self::TYPE_TEXT => $partialFormViewConfig['text'],
            self::TYPE_PASSWORD => $partialFormViewConfig['password'],
            self::TYPE_NUMERIC => $partialFormViewConfig['numeric'],
            self::TYPE_FLOAT => $partialFormViewConfig['float'],
            self::TYPE_BOOLEAN => $partialFormViewConfig['boolean'],
            self::TYPE_DATETIME => $partialFormViewConfig['datetime'],
            self::TYPE_DATE => $partialFormViewConfig['date'],
            self::TYPE_TIME => $partialFormViewConfig['time'],
            self::TYPE_TEXTAREA => $partialFormViewConfig['textarea'],
            self::TYPE_RICHTEXTAREA => $partialFormViewConfig['richtextarea'],
            self::TYPE_FILE => $partialFormViewConfig['file'],
            self::TYPE_SELECT => $partialFormViewConfig['select'], // additional
            self::TYPE_REFERENCES_LABEL => $partialFormViewConfig['referenceslabel'], // additional
            self::GROUPTYPE_MAP => $partialFormViewConfig['group-map'], // additional
            self::GROUPTYPE_DATERANGE => $partialFormViewConfig['group-daterange'], // additional
            self::GROUPTYPE_DATETIMERANGE => $partialFormViewConfig['group-datetimerange'], // additional  
            self::GROUPTYPE_NUMERICRANGE => $partialFormViewConfig['group-numericrange'], // additional
            self::GROUPTYPE_TIMERANGE => $partialFormViewConfig['group-timerange'], // additional
        ];
    }

    public function getTableName() {
        return $this->table;
    }

    public function getDefaultOrderColumn() {
        return $this->getKeyName();
    }

    public function getDefaultOrderColumnDirection() {
        return 'desc';
    }

    public function getAttributeSettings() {
        // default attribute settings of generic model, override for furher use
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID",
                "readonly" => true,
                "initiated" => true,
                "filterable" => false,
                "translation" => false,
                "options" => null,
                "options_url" => null
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" =>"Created At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At",
                "readonly" => true,
                "initiated" => true,
                "translation" => false
            ]
        ];
    }

    // implements singleton pattern
    public function getBufferedAttributeSettings() {
        $className = get_called_class();
        if (!isset(static::$bufferAttributeSettings[$className]) || empty(static::$bufferAttributeSettings[$className])) {
            // BASE :: static::$bufferAttributeSettings[$className] = $this->getAttributeSettings();
            $finalAttributeSettingState = $this->getAttributeSettings();
            if ($this->attributeSettingsCustomState &&
                is_array($this->attributeSettingsCustomState) &&
                !empty($this->attributeSettingsCustomState)) {
                $currentAttributeSetting = $finalAttributeSettingState;
                $finalAttributeSettingState = [];
                foreach ($this->attributeSettingsCustomState as $idx => $attrKey) {
                    $finalAttributeSettingState[$attrKey] = $currentAttributeSetting[$attrKey];
                }
            }
            static::$bufferAttributeSettings[$className] = $finalAttributeSettingState;
            foreach (static::$bufferAttributeSettings[$className] as $attrName => $value) {
                /* Check label translation */
                $targetTranslationPath = $this->nodeClassName . "." . strtolower($attrName);
                if (trans($targetTranslationPath) != $targetTranslationPath) {
                    static::$bufferAttributeSettings[$className][$attrName]['label'] = trans($targetTranslationPath);
                }
            }
        }
        return static::$bufferAttributeSettings[$className];
    }

    public function setBufferedAttributeSettings($name, $key, $value) {
        $className = get_called_class();
        if (!isset(static::$bufferAttributeSettings[$className]) || empty(static::$bufferAttributeSettings[$className])) {
            $state = $this->getBufferedAttributeSettings();
        }
        if (isset(static::$bufferAttributeSettings[$className][$name]) &&
            isset(static::$bufferAttributeSettings[$className][$name][$key]))
            static::$bufferAttributeSettings[$className][$name][$key] = $value;
    }

    public function setAttributeSettingsCustomState($attrSettingCustomState) {
        if (is_array($attrSettingCustomState)) {
            $this->attributeSettingsCustomState = $attrSettingCustomState;
        }
    }

    public function getRelatedObject($relationshipName) {
        try {
            $className = get_class($this->{$relationshipName}()->getRelated());
            return new $className();
        } catch (Exception $e) { }
        return new SuitModel();
    }

    /**
     * Extended model for custom field
     * Rules:
     * Class name : ModelExtended
     * Foreign key : model_id
     * @return App\Suitcore\Models\SuitModel|null
     */
    public function extended()
    {
        $extendedModelName = $this->getExtendedModelClassNamespace();
        if (class_exists($extendedModelName)) {
            return $this->hasOne($extendedModelName, $this->getExtendedForeignKey());
        }
        return null;
    }

    public function getExtendedModelClassNamespace()
    {
        $extendedModelNamespace = 'App\ModelExtension';
        $extendedModelName = $extendedModelNamespace . '\\' . (new \ReflectionClass($this))->getShortName() . 'Extension';
        return $extendedModelName;
    }

    function getExtendedForeignKey()
    {
        $className = (new \ReflectionClass($this))->getShortName();
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $className, $matches);
        $words = $matches[0];
        foreach ($words as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        $result = implode('_', $words) . '_id';
        return $result;
    }

    public function getAttribute($key)
    {
        if (ends_with($key, '__trans')) {
            $actualKey = str_replace('__trans', '', $key);
            $translation = SuitTranslation::trans(strtolower(app()->getLocale()), $this->nodeFullClassName, $this->id, $actualKey);
            // return translation or fallback to default value if not exist
            return (empty($translation) ? $this->getAttribute($actualKey) : $translation);
        }
        if (ends_with($key, '_tz')) {
            $actualKey = str_replace('_tz', '', $key);
            if (in_array($actualKey, $this->dates)) {
                $defaultUserTz = config('suitcore.default_user_timezone'); // default-user-timezone
                $userTz = $defaultUserTz;
                if ($currentAuthUser = auth()->user()) {
                    if (!empty($currentAuthUser->timezone)) {
                        $userTz = $currentAuthUser->timezone;
                    }
                }
                try {
                    try {
                        return $this->{$actualKey}->tz($userTz);
                    } catch (Exception $e) { }
                    return $this->{$actualKey}->tz($defaultUserTz);
                } catch (Exception $e) { }
            }
        }
        if ($key == 'attribute_settings') {
            return $this->getBufferedAttributeSettings();
        }
        if ($key == '_label') {
            return $this->getTranslationLabel();
        }
        if ($key == '_desc') {
            return $this->getTranslationDescription();
        }
        if ($key == '_defaultOrder') {
            return $this->getDefaultOrderColumn();
        }
        if ($key == '_defaultOrderDir') {
            return $this->getDefaultOrderColumnDirection();
        }
        if (ends_with($key, '__object')) {
            return $this->getRelatedObject(str_replace('__object', '', $key));
        }
        return $this->thumbnailGetAttribute($key);
    }

    static public function getNew()
    {
        return new static;
    }

    public function getError()
    {
        return $this->errors;
    }

    public function isValid($scenario = 'create', $customRules = null, $params = null)
    {
        // Base Rules (from model definition or config)
        $rules = method_exists($this, 'rules') ? $this->rules() : $this->rules;
        $defaultConfigClassName = 'Suitcore\Config\DefaultConfig';
        if (class_exists($defaultConfigClassName)) {
            $defaultConfig = $defaultConfigClassName::getConfig();
            if (isset($defaultConfig['rules']) &&
                isset($defaultConfig['rules'][$this->nodeClassName])) {
                $rules = $defaultConfig['rules'][$this->nodeClassName];
            }
        }

        // Runtime Custom Rules
        if ($customRules != null) {
            foreach ($customRules as $k => $v) {
                $rules[$k] = $v;
            }
        }

        // Filter Rules based on scenario
        // Exclude attributes which has null value for validation purpose.
        // Add 'sometimes' rule to run validation checks against a field
        // only if that field is present in the input array.
        $newAttributes = [];
        if (is_array($params)) {
            if ($scenario == 'update') {
                // specific case for update
                foreach ($this->attributes as $key => $attribute) {
                    if (isset($params[$key])) {
                        $newAttributes[$key] = $params[$key];
                        // update specific rule
                        if (is_array($rules) && isset($rules[$key])) {
                            $split = explode('|', $rules[$key]);
                            $merged = [];
                            foreach ($split as $item) {
                                if (strpos($item, 'unique') === false) {
                                    // unique not exist
                                    $merged[] = $item;
                                } else {
                                    // unique exist
                                    if ($attribute != $params[$key]) {
                                        // and value changed (updated)
                                        $merged[] = $item;
                                    }
                                }
                            }
                            $rules[$key] = implode('|', $merged);
                        }
                    } else {
                        // remove specific rule
                        if (is_array($rules) && isset($rules[$key])) {
                            unset($rules[$key]);
                        }
                    }
                }
            } else {
                $newAttributes = $params;
            }
        } else {
            foreach ($this->attributes as $key => $attribute) {
                if ($attribute != null ||
                    $attribute === floatval(0) ||
                    $attribute === intval(0)
                ) {
                    $newAttributes[$key] = $attribute;
                }
            }
            // update rules
            if ($scenario == 'update') {
                if (is_array($rules)) {
                    foreach ($rules as $key => $value) {
                        $split = explode('|', $value);
                        $merged = [];
                        foreach ($split as $item) {
                            if (strpos($item, 'unique') === false) {
                                $merged[] = $item;
                            }
                        }
                        $rules[$key] = implode('|', $merged);
                    }
                }
            }
        }
        $v = Validator::make($newAttributes, $rules);

        if ($v->passes()) {
            return true;
        }
        $this->errors = $v->messages();
        return false;
    }

    public function freshInstance()
    {
        $fillable = $this->getFillable();
        $attributes = array_combine($fillable, array_fill(0, count($fillable), null));
        return new static($attributes);
    }

    public function getLabel() {
        return "Object";
    }

    public function getDescription() {
        return "";
    }

    final public function getTranslationLabel() {
        /* Check Translation First */
        $targetTranslationPath = $this->nodeClassName . ".classlabel";
        if (trans($targetTranslationPath) != $targetTranslationPath) {
            return trans($targetTranslationPath);
        }
        /* Return Default */
        return $this->getLabel();
    }

    final public function getTranslationDescription() {
        /* Check Translation First */
        $targetTranslationPath = $this->nodeClassName . ".classdescription";
        if (trans($targetTranslationPath) != $targetTranslationPath) {
            return trans($targetTranslationPath);
        }
        /* Return Default */
        return $this->getDescription();
    }

    public function getUniqueValue() {
        return $this->id;
    }

    public function getUniqueValueColumn() {
        return 'id';
    }

    public function getFormattedValue() {
        return "-";
    }

    public function getFormattedValueColumn() {
        return [];
    }

    public function getDefaultNameAttribute() {
        return $this->getFormattedValue();
    }

    public function __toString()
    {
        return $this->getFormattedValue();
    }

    public function getOptions() {
        return self::all();
    }

    public function getImportExcelKeyBaseName() {
        return [ $this->primaryKey ];
    }

    public function getImportExcelKeyBase() {
        try {
            $keyBaseName = $this->getImportExcelKeyBaseName();
            if (is_array($keyBaseName)) {
                $result = [];
                foreach ($keyBaseName as $key => $value) {
                    $result[$value] = $this->getAttribute($value);
                }
                return $result;
            }
            return [ $keyBaseName => $this->getAttribute($keyBaseName) ];
        } catch (Exception $e) { }
        return [];
    }

    private function isElmtGroupComplete($elmtGroupName) {
        if (is_array($this->elementGroup) && isset($this->elementGroup[$elmtGroupName])) {
            if (is_array($this->elementGroup[$elmtGroupName]) && isset($this->elementGroup[$elmtGroupName]['type']) && isset($this->elementGroup[$elmtGroupName]['elmt']) && is_array($this->elementGroup[$elmtGroupName]['elmt'])) {
                if ($this->elementGroup[$elmtGroupName]['type'] == self::GROUPTYPE_MAP) {
                    return (isset($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_MAP_LOCATIONNAME]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_MAP_LOCATIONNAME]) && isset($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_MAP_LATITUDE]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_MAP_LATITUDE]) && isset($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_MAP_LONGITUDE]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_MAP_LONGITUDE]));
                } elseif (in_array($this->elementGroup[$elmtGroupName]['type'], [self::GROUPTYPE_NUMERICRANGE, self::GROUPTYPE_DATETIMERANGE, self::GROUPTYPE_DATERANGE, self::GROUPTYPE_TIMERANGE])) {
                    return (isset($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_RANGE_START]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_RANGE_START]) && isset($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_RANGE_END]) && !empty($this->elementGroup[$elmtGroupName]['elmt'][self::GROUPROLE_RANGE_END]));
                }
            }
        }
        return false;
    }

    public function resetGroupRendering() {
        $this->elementGroup = [];
    }

    public function isGroupSettingValid($elmt) {
        return (isset($elmt['elmt_group']) && is_array($elmt['elmt_group']) && isset($elmt['elmt_group']['name']) && isset($elmt['elmt_group']['type']) && isset($elmt['elmt_group']['role']));
    }

    public function addOrUpdateToGroupRendering($elmt) {
        if ($elmt && is_array($elmt) && $this->isGroupSettingValid($elmt)) {
            if (!isset($this->elementGroup[$elmt['elmt_group']['name']])) {
                $this->elementGroup[$elmt['elmt_group']['name']] = [
                    'type' => $elmt['elmt_group']['type'],
                    'elmt' => []
                ];
            }
            if (in_array($elmt['elmt_group']['role'], [self::GROUPROLE_MAP_LATITUDE, self::GROUPROLE_MAP_LONGITUDE])) {
                $elmt['readonly'] = true;
                $elmt['numberdigit'] = 10;
            }
            $this->elementGroup[$elmt['elmt_group']['name']]['elmt'][$elmt['elmt_group']['role']] = $elmt;
        }
    }

    public function removeFromGroupRendering($elmt) {
        if ($elmt && is_array($elmt) && isset($elmt['elmt_group']['name']) && isset($elmt['elmt_group']['role']) && 
            isset($this->elementGroup[$elmt['elmt_group']['name']]) &&
            isset($this->elementGroup[$elmt['elmt_group']['name']]['elmt']) &&
            isset($this->elementGroup[$elmt['elmt_group']['name']]['elmt'][$elmt['elmt_group']['role']])) {
            unset($this->elementGroup[$elmt['elmt_group']['name']]['elmt'][$elmt['elmt_group']['role']]);
        }
    }

    public function getGroupRenderingMember($elmtGroupName) {
        if ($elmtGroupName && 
            isset($this->elementGroup[$elmtGroupName])) {
            return $this->elementGroup[$elmtGroupName];
        }
        return null;
    }

    public function getAllGroupElement() {
        return $this->elementGroup;
    }

    public function renderFormView($attrName, $uploadHandler = null, $errors = null, $attributes = null, $basic = false, $customSetting = null, $optionsAjaxHandler = null) {
        // initial setup
        $currentLocale = strtolower(config('app.fallback_locale', 'en'));
        $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $currentLocale));
        $attrSettings = $this->getBufferedAttributeSettings();
        if ($customSetting && is_array($customSetting)) {
            $attrSettings[$attrName] = $customSetting;
        }
        $enableTranslationInput = (env('APP_MULTI_LOCALE', false) &&
                                   isset($attrSettings[$attrName]['translation']) &&
                                   $attrSettings[$attrName]['translation']);
        $formSetting = [
            'container_id' => false, //
            'id' => false, //
            'masked_id' => false, //
            'name' => false, //
            'label' => false, //
            'value' => false, //
            'masked_value' => false,
            'options' => false, //
            'required' => false, //
            'readonly' => false, //
            'image_file_url' => false,
            'file_url' => false,
            'action_handler_route' => false, //
            'errors' => false, //
            'data'       => false,
            'attributes' => false,
            'multiple'   => false,
            'wysiwyg'    => false,
            'selecttype' => false,
            'data_url' => (isset($attrSettings[$attrName]['options_url']) && !empty($attrSettings[$attrName]['options_url']) ? $attrSettings[$attrName]['options_url'] : false), //// options autocomplete growing ajax
            'value_text' => false //// options autocomplete growing ajax
        ]; // default setting
        if (isset($attrSettings[$attrName]['multiple'])) {
            $formSetting['multiple'] = $attrSettings[$attrName]['multiple'];
        }

        if (isset($attrSettings[$attrName]['wysiwyg'])) {
            $formSetting['wysiwyg'] = $attrSettings[$attrName]['wysiwyg'];
        }

        if (isset($attrSettings[$attrName]['htmldata'])) {
            $htmldata       = $attrSettings[$attrName]['htmldata'];
            $attributesdata = [];

            foreach ($htmldata as $key => $value) {
                $attributesdata['data-' . $key] = $value;
            }

            if ($attributes != null && sizeof($attributes) > 0) {
                if (isset($attributes['select-type'])) {
                    $formSetting['selecttype'] = $attributes['select-type'];
                    unset($attributes['select-type']);
                }
                $attributes = array_merge($attributes, $attributesdata);
            } else {
                $attributes = $attributesdata;
            }
        }

        if (isset($attrSettings[$attrName]['image_type'])) {
            $formSetting['image_type'] = $attrSettings[$attrName]['image_type'];
        }

        if (isset($attrSettings[$attrName]['image_zoom'])) {
            $formSetting['image_zoom'] = $attrSettings[$attrName]['image_zoom'];
        }

        if (isset($attrSettings[$attrName]['elmt_group'])) {
            $formSetting['elmt_group'] = $attrSettings[$attrName]['elmt_group'];
        }

        if (isset($attrSettings[$attrName]['form_field_relation'])) {
            $formSetting['form_field_relation'] = $attrSettings[$attrName]['form_field_relation'];
        }

        if ($attributes != null && sizeof($attributes) > 0) {
            if (isset($attributes['select-type'])) {
                $formSetting['selecttype'] = $attributes['select-type'];
                unset($attributes['select-type']);
            }
            $formSetting['attributes'] = $attributes;
        } else {
            $formSetting['attributes'] = [];
        }
        // construct
        $formSetting['id'] = "input".ucfirst(strtolower($attrName));
        $formSetting['masked_id'] = $formSetting['id']."_view";
        if (is_array($attrSettings) && $attrSettings[$attrName] != null) {
            $displayInForm = isset($attrSettings[$attrName]['formdisplay']) ? $attrSettings[$attrName]['formdisplay'] : false;
            if ($displayInForm) {
                $formSetting['container_id'] = $formSetting['id']."Container";
                $formSetting['name'] = $attrName;
                $formSetting['label'] = (isset($attrSettings[$attrName]['label']) ? $attrSettings[$attrName]['label'] : '');
                if ($this->getAttribute($attrName) == '' && isset($attrSettings[$attrName]['default_value'])) {
                    $formSetting['value'] = $attrSettings[$attrName]['default_value'];
                } else {
                    $formSetting['value'] = $this->getAttribute($attrName);
                }
                $formSetting['group'] = (isset($attrSettings[$attrName]['group']) ? $attrSettings[$attrName]['group'] : false);
                $formSetting['options'] = [];
                if (isset($attrSettings[$attrName]['options']) &&
                    ((is_array($attrSettings[$attrName]['options']) &&
                      count($attrSettings[$attrName]['options']) > 0) ||
                     (is_string($attrSettings[$attrName]['options']) &&
                      !empty($attrSettings[$attrName]['options'])) ||
                     (is_a($attrSettings[$attrName]['options'], 'Illuminate\Support\Collection') &&
                      count($attrSettings[$attrName]['options']) > 0)) ) {
                    if (is_a($attrSettings[$attrName]['options'], 'Illuminate\Support\Collection')) {
                        $formSetting['options'] = $attrSettings[$attrName]['options']->toArray();
                    } elseif (is_string($attrSettings[$attrName]['options'])) {
                        try {
                            $formSetting['options'] = $this->{$attrSettings[$attrName]['options']}();
                            static::$bufferAttributeSettings[get_called_class()][$attrName]['options'] = $formSetting['options'];
                        } catch (\Exception $e) { $formSetting['options'] = []; }
                    } else {
                        $formSetting['options'] = $attrSettings[$attrName]['options'];
                    }
                }
                $formSetting['required'] = (isset($attrSettings[$attrName]['required']) ? $attrSettings[$attrName]['required'] : false);
                $formSetting['readonly'] = (isset($attrSettings[$attrName]['readonly']) ? $attrSettings[$attrName]['readonly'] : false);
                $formSetting['action_handler_route'] = $uploadHandler;
                $formSetting['errors'] = ($errors ? $errors->first($attrName) : "");
                if ($this->isGroupSettingValid($formSetting)) {
                    // Group of Element
                    $this->addOrUpdateToGroupRendering($formSetting);
                    if ($this->isElmtGroupComplete($formSetting['elmt_group']['name'])) {
                        if ($formSetting['elmt_group']['type'] == self::GROUPTYPE_MAP) {
                            return View::make(self::$partialFormView[self::GROUPTYPE_MAP], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == self::GROUPTYPE_DATERANGE) {
                            return View::make(self::$partialFormView[self::GROUPTYPE_DATERANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == self::GROUPTYPE_DATETIMERANGE) {
                            return View::make(self::$partialFormView[self::GROUPTYPE_DATETIMERANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == self::GROUPTYPE_NUMERICRANGE) {
                            return View::make(self::$partialFormView[self::GROUPTYPE_NUMERICRANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        } elseif ($formSetting['elmt_group']['type'] == self::GROUPTYPE_TIMERANGE) {
                            return View::make(self::$partialFormView[self::GROUPTYPE_TIMERANGE], ['groupFormSetting' => $this->getGroupRenderingMember($formSetting['elmt_group']['name'])])->render();
                        }
                        return "";
                    }
                } else {
                    // Non-Group : process by type
                    if ($attrSettings[$attrName]['type'] == self::TYPE_NUMERIC) {
                        if (isset($attrSettings[$attrName]['options']) || isset($attrSettings[$attrName]['options_url'])) {
                            if ((isset($attrSettings[$attrName]['readonly']) &&
                                 $attrSettings[$attrName]['readonly']) ||
                                (isset($attrSettings[$attrName]['initiated']) &&
                                 $attrSettings[$attrName]['initiated'])){
                                // As Readonly InputText References
                                $formSetting['masked_value'] = "N/A";
                                if (isset($attrSettings[$attrName]['relation']) &&
                                    !empty($attrSettings[$attrName]['relation'])) {
                                    $relatedObject = $this->getAttribute($attrSettings[$attrName]['relation']);
                                    if ($relatedObject)
                                        $formSetting['masked_value'] = $relatedObject->getFormattedValue();
                                    else
                                        $formSetting['masked_value'] = "-";
                                    // render
                                    return View::make(self::$partialFormView[self::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                                } else {
                                    $relatedReferences = $formSetting['options'];
                                    if (isset($relatedReferences[$this->getAttribute($attrName)]))
                                        $formSetting['masked_value'] = $relatedReferences[$this->getAttribute($attrName)];
                                    else
                                        $formSetting['masked_value'] = ucfirst(strtolower($this->getAttribute($attrName)));
                                    // render
                                    return View::make(self::$partialFormView[self::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                                }
                            } else {
                                // As Dropdown List Options
                                if ($optionsAjaxHandler &&
                                    isset($optionsAjaxHandler[$attrName]) &&
                                    !empty($optionsAjaxHandler[$attrName])) {
                                    $formSetting['data_url'] = $optionsAjaxHandler[$attrName];
                                }
                                if ($formSetting['data_url']) {
                                    $relatedObject = $this->getAttribute($attrSettings[$attrName]['relation']);
                                    if ($relatedObject) {
                                        $formSetting['value_text'] = $relatedObject->getFormattedValue();
                                    }
                                }
                                // render
                                return View::make(self::$partialFormView[self::TYPE_SELECT].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        } else {
                            // render
                            return View::make(self::$partialFormView[self::TYPE_NUMERIC].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_FLOAT) {
                        // render
                        return View::make(self::$partialFormView[self::TYPE_FLOAT].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_BOOLEAN) {
                        // render
                        return View::make(self::$partialFormView[self::TYPE_BOOLEAN].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_DATETIME) {
                        if ((isset($attrSettings[$attrName]['readonly']) &&
                             $attrSettings[$attrName]['readonly']) ||
                            (isset($attrSettings[$attrName]['initiated']) &&
                             $attrSettings[$attrName]['initiated'])){
                            // As Readonly InputText References
                            $formSetting['masked_value'] = "N/A";
                            if (!isEmptyDate($this->getAttribute($attrName))) {
                                try {
                                    $formSetting['masked_value'] = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName))->format("d F Y G:i");
                                } catch (Exception $e) { 
                                    $formSetting['masked_value'] = "-";
                                }
                            }
                            // render
                            return View::make(self::$partialFormView[self::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        } else {
                            // As Datetime Input Options
                            if (isEmptyDate($this->getAttribute($attrName))) {
                                // $formSetting['value'] = date('Y-m-d H:i:s');
                            } else {
                                try {
                                    $formSetting['value'] = Carbon::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName))->toDateTimeString(); 
                                } catch (Exception $e) { 
                                    $formSetting['value'] = null;
                                }
                            }
                            // render
                            return View::make(self::$partialFormView[self::TYPE_DATETIME].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_DATE) {
                        if ((isset($attrSettings[$attrName]['readonly']) &&
                             $attrSettings[$attrName]['readonly']) ||
                            (isset($attrSettings[$attrName]['initiated']) &&
                             $attrSettings[$attrName]['initiated'])){
                            // As Readonly InputText References
                            $formSetting['masked_value'] = "N/A";
                            if (!isEmptyDate($this->getAttribute($attrName))) {
                                try {
                                    $formSetting['masked_value'] = Date::createFromFormat('Y-m-d', $this->getAttribute($attrName))->format("d F Y");
                                } catch (Exception $e) { 
                                    $formSetting['masked_value'] = "-";
                                }
                            }
                            // render
                            return View::make(self::$partialFormView[self::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        } else {
                            // As Date Input Options
                            if (isEmptyDate($this->getAttribute($attrName))) {
                                // $formSetting['value'] = date('Y-m-d');
                            } else {
                                try {
                                    $formSetting['value'] = Carbon::createFromFormat('Y-m-d', $this->getAttribute($attrName))->toDateString(); 
                                } catch (Exception $e) { 
                                    $formSetting['value'] = null;
                                }
                            }
                            // render
                            return View::make(self::$partialFormView[self::TYPE_DATE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_TIME) {
                        if ((isset($attrSettings[$attrName]['readonly']) &&
                             $attrSettings[$attrName]['readonly']) ||
                            (isset($attrSettings[$attrName]['initiated']) &&
                             $attrSettings[$attrName]['initiated'])){
                            // As Readonly InputText References
                            $formSetting['masked_value'] = "N/A";
                            if (!isEmptyDate($this->getAttribute($attrName))) {
                                try {
                                    $formSetting['masked_value'] = date("G:i", strtotime($this->getAttribute($attrName)));
                                } catch (Exception $e) { }
                            }
                            // render
                            return View::make(self::$partialFormView[self::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        } else {
                            // As Time Input Options
                            if ($this->getAttribute($attrName) == '') {
                                // $formSetting['value'] = date('H:i');
                            } else {
                                $formSetting['value'] = date("H:i", strtotime($this->getAttribute($attrName)));
                            }
                            // render
                            return View::make(self::$partialFormView[self::TYPE_TIME].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                        }
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_TEXTAREA) {
                        $formSetting['value'] = trim($this->getAttribute($attrName));
                        // render
                        $translationFormSetting = $formSetting;
                        $translationFormSetting['label'] .= ($enableTranslationInput ? ' (' . strtoupper($currentLocale) . ')' : '');
                        $mainView = View::make(self::$partialFormView[self::TYPE_TEXTAREA].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                        $translationView = "";
                        if ($enableTranslationInput) {
                            // foreach targeted translation
                            foreach ($localeOptions as $lang) {
                                if (strtolower($lang) != $currentLocale) {
                                    $translationFormSetting = $formSetting;
                                    $translationFormSetting['container_id'] .= '_trans_' . $lang;
                                    $translationFormSetting['id'] .= '_trans_' . $lang;
                                    $translationFormSetting['name'] .= '_trans_' . $lang;
                                    $translationFormSetting['label'] .= ' (' . strtoupper($lang) . ')';
                                    $translationFormSetting['value'] = SuitTranslation::trans(strtolower($lang), $this->nodeFullClassName, $this->id, $attrName);
                                    $translationFormSetting['required'] = false;
                                    $translationFormSetting['errors'] = ($errors ? $errors->first($attrName) : "");
                                    // translation input form
                                    $translationView .= "\n" . View::make(self::$partialFormView[self::TYPE_TEXTAREA].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                                }
                            }
                        }
                        return $mainView . "\n" .$translationView;
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_RICHTEXTAREA) {
                        $formSetting['value'] = trim($this->getAttribute($attrName));
                        // render
                        $translationFormSetting = $formSetting;
                        $translationFormSetting['label'] .= ($enableTranslationInput ? ' (' . strtoupper($currentLocale) . ')' : '');
                        $mainView = View::make(self::$partialFormView[self::TYPE_RICHTEXTAREA].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                        $translationView = "";
                        if ($enableTranslationInput) {
                            // foreach targeted translation
                            foreach ($localeOptions as $lang) {
                                if (strtolower($lang) != $currentLocale) {
                                    $translationFormSetting = $formSetting;
                                    $translationFormSetting['container_id'] .= '_trans_' . $lang;
                                    $translationFormSetting['id'] .= '_trans_' . $lang;
                                    $translationFormSetting['name'] .= '_trans_' . $lang;
                                    $translationFormSetting['label'] .= ' (' . strtoupper($lang) . ')';
                                    $translationFormSetting['value'] = SuitTranslation::trans(strtolower($lang), $this->nodeFullClassName, $this->id, $attrName);
                                    $translationFormSetting['required'] = false;
                                    $translationFormSetting['errors'] = ($errors ? $errors->first($attrName) : "");
                                    // translation input form
                                    $translationView .= "\n" . View::make(self::$partialFormView[self::TYPE_RICHTEXTAREA].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                                }
                            }
                        }
                        return $mainView . "\n" .$translationView;
                    } else if ($attrSettings[$attrName]['type'] == self::TYPE_FILE) {
                        if ($this->getAttribute($attrName)) {
                            if (is_array($this->imageAttributes) &&
                                count($this->imageAttributes) > 0 &&
                                in_array($attrName, array_keys($this->imageAttributes))) {
                                $formSetting['image_file_url'] = $this->getAttribute($attrName."_medium_cover");
                                // render
                                return View::make(self::$partialFormView[self::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                $formSetting['file_url'] = $this->getFileAccessPath($attrName);
                                // render
                                return View::make(self::$partialFormView[self::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        } else {
                            if (is_array($this->imageAttributes) &&
                                count($this->imageAttributes) > 0 &&
                                in_array($attrName, array_keys($this->imageAttributes))) {
                                $formSetting['image_file_url'] = 'nofile';
                                // render
                                return View::make(self::$partialFormView[self::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                $formSetting['file_url'] = 'nofile';
                                // render
                                return View::make(self::$partialFormView[self::TYPE_FILE].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        }
                    } elseif ($attrSettings[$attrName]['type'] == self::TYPE_PASSWORD) {
                        // render
                        return View::make(self::$partialFormView[self::TYPE_PASSWORD].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                    } else {
                        // default is inputText
                        if (isset($attrSettings[$attrName]['options']) && 
                            (is_array($attrSettings[$attrName]['options']) ||
                             (is_string($attrSettings[$attrName]['options']) && !empty($attrSettings[$attrName]['options'])))) {
                            if ((isset($attrSettings[$attrName]['readonly']) &&
                                 $attrSettings[$attrName]['readonly']) ||
                                (isset($attrSettings[$attrName]['initiated']) &&
                                 $attrSettings[$attrName]['initiated'])){
                                // As Readonly InputText References
                                $relatedReferences = $formSetting['options'];
                                $formSetting['masked_value'] = "-";
                                if (isset($relatedReferences[$this->getAttribute($attrName)]))
                                    $formSetting['masked_value'] = $relatedReferences[$this->getAttribute($attrName)];
                                else
                                    $formSetting['masked_value'] = ucfirst(strtolower($this->getAttribute($attrName)));
                                // render
                                return View::make(self::$partialFormView[self::TYPE_REFERENCES_LABEL].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            } else {
                                // As Dropdown List Options
                                // render
                                return View::make(self::$partialFormView[self::TYPE_SELECT].($basic ? '-basic' : ''), ['formSetting' => $formSetting])->render();
                            }
                        } else {
                            // default is inputText without options
                            // render
                            $translationFormSetting = $formSetting;
                            $translationFormSetting['label'] .= ($enableTranslationInput ? ' (' . strtoupper($currentLocale) . ')' : '');
                            $mainView = View::make(self::$partialFormView[self::TYPE_TEXT].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                            $translationView = "";
                            if ($enableTranslationInput) {
                                // foreach targeted translation
                                foreach ($localeOptions as $lang) {
                                    if (strtolower($lang) != $currentLocale) {
                                        $translationFormSetting = $formSetting;
                                        $translationFormSetting['container_id'] .= '_trans_' . $lang;
                                        $translationFormSetting['id'] .= '_trans_' . $lang;
                                        $translationFormSetting['name'] .= '_trans_' . $lang;
                                        $translationFormSetting['label'] .= ' (' . strtoupper($lang) . ')';
                                        $translationFormSetting['value'] = SuitTranslation::trans(strtolower($lang), $this->nodeFullClassName, $this->id, $attrName);
                                        $translationFormSetting['required'] = false;
                                        $translationFormSetting['errors'] = ($errors ? $errors->first($attrName) : "");
                                        // translation input form
                                        $translationView .= "\n" . View::make(self::$partialFormView[self::TYPE_TEXT].($basic ? '-basic' : ''), ['formSetting' => $translationFormSetting])->render();
                                    }
                                }
                            }
                            return $mainView . "\n" .$translationView;
                        }
                    }
                }
            }
        }
        // return empty form entry
        return "";
    }

    public function renderAttribute($attrName, $columnFormatted = null) {
        $attrSettings = $this->getBufferedAttributeSettings()[$attrName];
        $tmpRow = '';
        // if ($attrSettings['visible']) {
            $rendered = false;
            if (is_array($columnFormatted) &&
                isset($columnFormatted[$attrName]) &&
                !empty($columnFormatted[$attrName])) {
                // Custom Template / Format
                try {
                    $maskedValue = (property_exists(get_class($this), $attrName."_attribute_label") ? $this->getAttribute($attrName."_attribute_label") : $this->getAttribute($attrName));
                    if (isset($columnFormatted["_render_".$attrName])) {
                        // try render using provided function
                        try {
                            $maskedValue = call_user_func($columnFormatted["_render_".$attrName], $this->getAttribute($attrName));
                        } catch (Exception $e2) { }
                    }
                    if (is_array($columnFormatted[$attrName])) {
                        if (isset( $columnFormatted[$attrName][$this->getAttribute($attrName)] )) {
                            $tmpRow = str_replace('#'.$attrName.'#', $maskedValue, $columnFormatted[$attrName][$this->getAttribute($attrName)]);
                        } else {
                            $tmpRow = str_replace('#'.$attrName.'#', $maskedValue, $columnFormatted[$attrName][array_keys($columnFormatted[$attrName])[0]]);
                        }
                    } else {
                        $tmpRow = str_replace('#'.$attrName.'#', $maskedValue, $columnFormatted[$attrName]);
                    }
                    $rendered = true;
                } catch (Exception $e) {
                    // Back to default format
                    $rendered = false;
                }
            }
            // transform options if presented as string and have no relationship
            if ((!isset($attrSettings['relation']) ||
                 $attrSettings['relation'] == null) && 
                isset($attrSettings['options']) && 
                is_string($attrSettings['options']) && 
                !empty($attrSettings['options'])) {
                try {
                    $attrSettings['options'] = $this->{$attrSettings['options']}();
                    static::$bufferAttributeSettings[get_called_class()][$attrName]['options'] = $attrSettings['options'];
                } catch (\Exception $e) { $attrSettings['options'] = []; }
            }
            // end transformation
            if (!$rendered) {
                // Standard Template / Format
                if ($attrSettings['relation'] != null) {
                    $relatedObject = $this->getAttribute($attrSettings['relation']);
                    if ($relatedObject)
                        $tmpRow = $relatedObject->getFormattedValue();
                    else
                        $tmpRow = "-";
                } else if ($attrSettings['type'] == self::TYPE_NUMERIC) {
                    if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                        $relatedReferences = $attrSettings['options'];
                        if (isset($relatedReferences[$this->getAttribute($attrName)]))
                            $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                        else
                            $tmpRow = $this->getAttribute($attrName);
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? number_format($this->getAttribute($attrName),0,'.',',') : 0);
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_FLOAT) {
                    if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                        $relatedReferences = $attrSettings['options'];
                        if (isset($relatedReferences[$this->getAttribute($attrName)]))
                            $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                        else
                            $tmpRow = $this->getAttribute($attrName);
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? number_format($this->getAttribute($attrName),2,'.',',') : 0);
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_DATETIME) {
                    if (isEmptyDate($this->getAttribute($attrName))) {
                        $tmpRow = "-";
                    } else {
                        try {
                            $defaultUserTz = config('suitcore.default_user_timezone'); // default-user-timezone
                            $userTz = $defaultUserTz;
                            if ($currentAuthUser = auth()->user()) {
                                if (!empty($currentAuthUser->timezone)) {
                                    $userTz = $currentAuthUser->timezone;
                                }
                            }
                            if ($this->getAttribute($attrName)) {
                                try {
                                    $tmpRow = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName), $userTz)->format("d F Y G:i");
                                } catch (Exception $etz) {
                                    try {
                                        $tmpRow = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName), $defaultUserTz)->format("d F Y G:i");
                                    } catch (Exception $etz) {
                                        $tmpRow = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName))->format("d F Y G:i");
                                    }
                                }
                            } else {
                                $tmpRow = "-";
                            }
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = "-"; // $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_DATE) {
                    if (isEmptyDate($this->getAttribute($attrName))) {
                        $tmpRow = "-";
                    } else {
                        try {
                            $tmpRow = Date::createFromFormat('Y-m-d', $this->getAttribute($attrName))->format("d F Y");
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = "-"; // $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_TIME) {
                    if (isEmptyDate($this->getAttribute($attrName))) {
                        $tmpRow = "-";
                    } else {
                        try {
                            $tmpRow = ($this->getAttribute($attrName) ? date("G:i", strtotime($this->getAttribute($attrName))) : '-');
                        } catch (Exception $e) {
                            // Back to default plain
                            $tmpRow = "-"; // $this->getAttribute($attrName);
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_BOOLEAN) {
                    $tmpRow = ($theVal = $this->getAttribute($attrName)) === null ? '-' : ($theVal ? trans('label.yes') : trans('label.no'));
                } else if ($attrSettings['type'] == self::TYPE_FILE) {
                    if ($this->getAttribute($attrName)) {
                        if (is_array($this->imageAttributes) &&
                            count($this->imageAttributes) > 0 &&
                            in_array($attrName, array_keys($this->imageAttributes))) {
                            $tmpRow = "<img class='".self::$thumbnailClass."' src='".$this->getAttribute($attrName."_medium_cover")."' style='max-height: 300px' alt=''>";
                        } else {
                            $tmpRow = $this->getAttribute($attrName) . " <a target='_BLANK' class='btn btn-sm green-haze' href='".$this->getFileAccessPath($attrName)."'><span class='fa fa-download'>&nbsp;</span></a>";
                        }
                    } else {
                        if (is_array($this->imageAttributes) &&
                            count($this->imageAttributes) > 0 &&
                            in_array($attrName, array_keys($this->imageAttributes))) {
                            $tmpRow = "<i>( ".trans('label.no_image')." )</i>";
                        } else {
                            $tmpRow = "<i>( ".trans('label.no_file')." )</i>";
                        }
                    }
                } else if ($attrSettings['type'] == self::TYPE_RICHTEXTAREA) {
                    // Rich Text Based
                    $shouldTranslate = isset($attrSettings['translation']) ? $attrSettings['translation'] : false;
                    $tmpRow = safe_output_xss($this->getAttribute($attrName . ($shouldTranslate ? '__trans' : '')));
                } else {
                    // Text Based
                    if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                        $relatedReferences = $attrSettings['options'];
                        if (isset($relatedReferences[$this->getAttribute($attrName)]))
                            $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                        else
                            $tmpRow = ucwords($this->getAttribute($attrName));
                    } else {
                        $shouldTranslate = isset($attrSettings['translation']) ? $attrSettings['translation'] : false;
                        $tmpRow = $this->getAttribute($attrName . ($shouldTranslate ? '__trans' : ''));
                    }
                }
            }
        // }
        // Return
        return $tmpRow;
    }

    public function renderRawAttribute($attrName) {
        $attrSettings = $this->getBufferedAttributeSettings()[$attrName];
        // transform options if presented as string and have no relationship
        if ((!isset($attrSettings['relation']) ||
             $attrSettings['relation'] == null) && 
            isset($attrSettings['options']) && 
            is_string($attrSettings['options']) && 
            !empty($attrSettings['options'])) {
            try {
                $attrSettings['options'] = $this->{$attrSettings['options']}();
                static::$bufferAttributeSettings[get_called_class()][$attrName]['options'] = $attrSettings['options'];
            } catch (\Exception $e) { $attrSettings['options'] = []; }
        }
        // end transformation
        $tmpRow = '';
        // Standard Template / Format
        if ($attrSettings['relation'] != null) {
            $relatedObject = $this->getAttribute($attrSettings['relation']);
            if ($relatedObject)
                $tmpRow = $relatedObject->getFormattedValue();
            else
                $tmpRow = "-";
        } else if ($attrSettings['type'] == self::TYPE_NUMERIC) {
            if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                $relatedReferences = $attrSettings['options'];
                if (isset($relatedReferences[$this->getAttribute($attrName)]))
                    $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                else
                    $tmpRow = $this->getAttribute($attrName);
            } else {
                try {
                    $tmpRow = ($this->getAttribute($attrName) ? number_format($this->getAttribute($attrName),0,'.',',') : 0);
                } catch (Exception $e) {
                    // Back to default plain
                    $tmpRow = $this->getAttribute($attrName);
                }
            }
        } else if ($attrSettings['type'] == self::TYPE_FLOAT) {
            if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                $relatedReferences = $attrSettings['options'];
                if (isset($relatedReferences[$this->getAttribute($attrName)]))
                    $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                else
                    $tmpRow = $this->getAttribute($attrName);
            } else {
                try {
                    $tmpRow = ($this->getAttribute($attrName) ? number_format($this->getAttribute($attrName),2,'.',',') : 0);
                } catch (Exception $e) {
                    // Back to default plain
                    $tmpRow = $this->getAttribute($attrName);
                }
            }
        } else if ($attrSettings['type'] == self::TYPE_DATETIME) {
            if (isEmptyDate($this->getAttribute($attrName))) {
                $tmpRow = "-";
            } else {
                try {
                    $defaultUserTz = config('suitcore.default_user_timezone'); // default-user-timezone
                    $userTz = $defaultUserTz;
                    if ($currentAuthUser = auth()->user()) {
                        if (!empty($currentAuthUser->timezone)) {
                            $userTz = $currentAuthUser->timezone;
                        }
                    }
                    if ($this->getAttribute($attrName)) {
                        try {
                            $tmpRow = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName), $userTz)->format("d F Y G:i");
                        } catch (Exception $etz) {
                            try {
                                $tmpRow = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName), $defaultUserTz)->format("d F Y G:i");
                            } catch (Exception $etz) {
                                $tmpRow = Date::createFromFormat('Y-m-d H:i:s', $this->getAttribute($attrName))->format("d F Y G:i");
                            }
                        }
                    } else {
                        $tmpRow = "-";
                    }
                } catch (Exception $e) {
                    // Back to default plain
                    $tmpRow = "-"; // $this->getAttribute($attrName);
                }
            }
        } else if ($attrSettings['type'] == self::TYPE_DATE) {
            if (isEmptyDate($this->getAttribute($attrName))) {
                $tmpRow = "-";
            } else {
                try {
                    $tmpRow = Date::createFromFormat('Y-m-d', $this->getAttribute($attrName))->format("d F Y");
                } catch (Exception $e) {
                    // Back to default plain
                    $tmpRow = "-"; // $this->getAttribute($attrName);
                }
            }
        } else if ($attrSettings['type'] == self::TYPE_TIME) {
            if (isEmptyDate($this->getAttribute($attrName))) {
                $tmpRow = "-";
            } else {
                try {
                    $tmpRow = ($this->getAttribute($attrName) ? date("G:i", strtotime($this->getAttribute($attrName))) : '-');
                } catch (Exception $e) {
                    // Back to default plain
                    $tmpRow = "-"; // $this->getAttribute($attrName);
                }
            }
        } else if ($attrSettings['type'] == self::TYPE_BOOLEAN) {
            $tmpRow = ($theVal = $this->getAttribute($attrName)) === null ? '-' : ($theVal ? trans('label.yes') : trans('label.no'));
        } else if ($attrSettings['type'] == self::TYPE_FILE) {
            if ($this->getAttribute($attrName)) {
                if (is_array($this->imageAttributes) &&
                    count($this->imageAttributes) > 0 &&
                    in_array($attrName, array_keys($this->imageAttributes))) {
                    $tmpRow = $this->getAttribute($attrName."_medium_cover");
                } else {
                    $tmpRow = $this->getAttribute($attrName."_full_path");
                }
            } else {
                if (is_array($this->imageAttributes) &&
                    count($this->imageAttributes) > 0 &&
                    in_array($attrName, array_keys($this->imageAttributes))) {
                    $tmpRow = trans('label.no_image');
                } else {
                    $tmpRow = trans('label.no_file');
                }
            }
        } else {
            // Text Based
            if (isset($attrSettings['options']) && is_array($attrSettings['options'])) {
                $relatedReferences = $attrSettings['options'];
                if (isset($relatedReferences[$this->getAttribute($attrName)]))
                    $tmpRow = $relatedReferences[$this->getAttribute($attrName)];
                else
                    $tmpRow = ucfirst(strtolower($this->getAttribute($attrName)));
            } else {
                $tmpRow = $this->getAttribute($attrName);
            }
        }
        // return
        return $tmpRow;
    }

    public function getFileAccessPath($field) {
        if (is_array($this->files) && isset($this->files[$field])) {
            return "/files/" . $this->files[$field] . "/" . $this->getAttribute($field);
        }
        return "";
    }

    // Overided from Query Builder
    /**
     * Increment a column's value by a given amount.
     *
     * @param  string  $column
     * @param  int     $amount
     * @param  array   $extra
     * @return int
     */
    public function increment($column, $amount = 1, array $extra = [])
    {
        $columns = array_merge([$column => $this->getAttribute($column) + $amount], $extra);

        return $this->update($columns);
    }

    public function rawIncrement($column, $amount = 1, array $extra = [])
    {
        DB::table($this->getTable())->where($this->getKeyName(), $this->getAttribute($this->getKeyName()))->increment($column, $amount);
        return true;
    }

    /**
     * Decrement a column's value by a given amount.
     *
     * @param  string  $column
     * @param  int     $amount
     * @param  array   $extra
     * @return int
     */
    public function decrement($column, $amount = 1, array $extra = [])
    {
        $columns = array_merge([$column => $this->getAttribute($column) - $amount], $extra);

        return $this->update($columns);
    }

    public function rawDecrement($column, $amount = 1, array $extra = [])
    {
        DB::table($this->getTable())->where($this->getKeyName(), $this->getAttribute($this->getKeyName()))->decrement($column, $amount);
        return true;
    }

    public function getImageAttributes()
    {
       $imageAttributes = $this->imageAttributes;

       if (!$this->isAssoc($imageAttributes)) {
           $imageAttributes = array_flip($imageAttributes);
       }

       return $imageAttributes;
    }

    /**
     * Generate Cache Key based on parameter array
     * @param  array $paramArr
     * @return string
     **/
    public function generateCacheKey(array $paramArr) {
        $clonedArr = $paramArr;
        ksort($clonedArr); 
        return md5(serialize($clonedArr));
    }

    /**
     * Flush tagged key
     * @param  array $tag
     * @return void
     **/
    public function flushTaggingCacheAlternative($tag) {
        $arrTags = is_array($tag) ? $tag : [$tag];
        foreach ($arrTags as $idx => $_tag) { 
            if ($mappedCacheKeys = Cache::get('tags-' . $_tag, [])) {
                foreach ($mappedCacheKeys as $idx => $key) {
                    Cache::forget($key);
                }
            }
        }
    }

    /**
     * Cache data wrapper
     * @param  string $key
     * @param  object $data
     * @return object
     **/
    public function cacheResult($key, $data) {
        $result = null;
        if ($data === null) $data = false;
        if ($key) {
            $tag = get_class($this);
            // new way to return null cachedData, since 5.4 and later had not cached null and false
            $result = [
                'cached_data' => $data
            ];
            if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                Cache::tags($tag)->forever($key, $result);
            } else {
                Cache::forever($key, $result);
                $this->taggingCacheAlternative($tag, $key);
            }
        }
        return ($result && is_array($result) && isset($result['cached_data']) ? $result['cached_data'] : null);
    }

    /**
     * Return Cached data based on key array
     * @param  string $key
     * @return object
     **/
    public function getCachedData($key) {
        $cachedData = null;
        if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
            $tag = get_class($this);
            $cachedData = Cache::tags($tag)->get($key);
        } else {
            $cachedData = Cache::get($key);
        }
        // new way to return null cachedData, since 5.4 and later had not cached null and false
        if ($cachedData &&
            is_array($cachedData) &&
            isset($cachedData['cached_data'])) {
            return $cachedData['cached_data'];
        }
        // backward compatibility for previous cached data
        if ($cachedData) return $cachedData;
        // if no cached data exist
        return null;
    }

    /**
     * Tagging a key, mapping a key to specified tag(s), 
     * for cache-driver that not support tagging
     * @param  array $tag
     * @param  string $key
     * @return void
     **/
    public function taggingCacheAlternative($tag, $key) {
        $arrTags = is_array($tag) ? $tag : [$tag];
        foreach ($arrTags as $idx => $_tag) {
            $mappedCacheTags = Cache::get('tags-' . $_tag, []);
            $mappedCacheTags[] = $key;
            Cache::forever('tags-' . $_tag, $mappedCacheTags);
        }
    }

    /**
     * Default Boot
     **/
    /*
    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if ($model->isDirty() && $model->nodeFullClassName) {
                if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                    Cache::tags($model->nodeFullClassName)->flush();

                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                Cache::tags( get_class($model->getRelatedObject($setting['relation'])) )->flush();
                            } catch (Exception $ex) { }
                        }
                    }
                } else {
                    $model->flushTaggingCacheAlternative($model->nodeFullClassName);
                    
                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                $relatedModel = $model->getRelatedObject($setting['relation']);
                                $relatedModel->flushTaggingCacheAlternative(get_class($relatedModel));
                            } catch (Exception $ex) { }
                        }
                    }
                }
            }
        });

        static::deleted(function ($model) {
            if ($model->nodeFullClassName) {
                if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                    Cache::tags($model->nodeFullClassName)->flush();

                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                Cache::tags( get_class($model->getRelatedObject($setting['relation'])) )->flush();
                            } catch (Exception $ex) { }
                        }
                    }
                } else {
                    $model->flushTaggingCacheAlternative($model->nodeFullClassName);

                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                $relatedModel = $model->getRelatedObject($setting['relation']);
                                $relatedModel->flushTaggingCacheAlternative(get_class($relatedModel));
                            } catch (Exception $ex) { }
                        }
                    }
                }
            }
        });
    } 
    */

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if ($model->isDirty() && $model->nodeFullClassName) {
                if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                    Cache::tags($model->nodeFullClassName)->flush();

                    if (is_array($model->manyRelationships) &&
                        count($model->manyRelationships) > 0) {
                        try {
                            foreach ($model->manyRelationships as $manyRelation) {
                                $downlineModel = $model->getRelatedObject($manyRelation);
                                Cache::tags($downlineModel->nodeFullClassName)->flush();
                            }
                        } catch (Exception $ex) { }
                    }

                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                $uplineModel = $model->getRelatedObject($setting['relation']);
                                Cache::tags( get_class($uplineModel) )->flush();

                                if (is_array($uplineModel->manyRelationships) &&
                                    count($uplineModel->manyRelationships) > 0) {
                                    try {
                                        foreach ($uplineModel->manyRelationships as $manyRelation) {
                                            $downlineModel = $uplineModel->getRelatedObject($manyRelation);
                                            Cache::tags($downlineModel->nodeFullClassName)->flush();
                                        }
                                    } catch (Exception $ex2) { }
                                }

                                foreach ($uplineModel->attribute_settings as $key2 => $setting2) {
                                    if (isset($setting2['relation']) &&
                                        !empty($setting2['relation'])) {
                                        try {
                                            $uplineModel2 = $uplineModel->getRelatedObject($setting2['relation']);
                                            Cache::tags( get_class($uplineModel2) )->flush();

                                            if (is_array($uplineModel2->manyRelationships) &&
                                                count($uplineModel2->manyRelationships) > 0) {
                                                try {
                                                    foreach ($uplineModel2->manyRelationships as $manyRelation2) {
                                                        $downlineModel2 = $uplineModel2->getRelatedObject($manyRelation2);
                                                        Cache::tags($downlineModel2->nodeFullClassName)->flush();
                                                    }
                                                } catch (Exception $ex3) { }
                                            }
                                        } catch (Exception $ex2) { }
                                    }
                                }
                            } catch (Exception $ex) { }
                        }
                    }
                } else {
                    $model->flushTaggingCacheAlternative($model->nodeFullClassName);

                    if (is_array($model->manyRelationships) &&
                        count($model->manyRelationships) > 0) {
                        try {
                            foreach ($model->manyRelationships as $manyRelation) {
                                $downlineModel = $model->getRelatedObject($manyRelation);
                                $downlineModel->flushTaggingCacheAlternative($downlineModel->nodeFullClassName);
                            }
                        } catch (Exception $ex) { }
                    }
                    
                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                $relatedModel = $model->getRelatedObject($setting['relation']);
                                $relatedModel->flushTaggingCacheAlternative(get_class($relatedModel));

                                if (is_array($relatedModel->manyRelationships) &&
                                    count($relatedModel->manyRelationships) > 0) {
                                    try {
                                        foreach ($relatedModel->manyRelationships as $manyRelation) {
                                            $downlineModel = $relatedModel->getRelatedObject($manyRelation);
                                            $downlineModel->flushTaggingCacheAlternative($downlineModel->nodeFullClassName);
                                        }
                                    } catch (Exception $ex2) { }
                                }

                                foreach ($relatedModel->attribute_settings as $key2 => $setting2) {
                                    if (isset($setting2['relation']) &&
                                        !empty($setting2['relation'])) {
                                        try {
                                            $relatedModel2 = $relatedModel->getRelatedObject($setting2['relation']);
                                            $relatedModel2->flushTaggingCacheAlternative(get_class($relatedModel2));

                                            if (is_array($relatedModel2->manyRelationships) &&
                                                count($relatedModel2->manyRelationships) > 0) {
                                                try {
                                                    foreach ($relatedModel2->manyRelationships as $manyRelation2) {
                                                        $downlineModel2 = $relatedModel2->getRelatedObject($manyRelation2);
                                                        $downlineModel2->flushTaggingCacheAlternative($downlineModel2->nodeFullClassName);
                                                    }
                                                } catch (Exception $ex3) { }
                                            }
                                        } catch (Exception $ex2) { }
                                    }
                                }
                            } catch (Exception $ex) { }
                        }
                    }
                }
                // varnish flush if used
                if (config('suitcore.varnishcacheable')) {
                    $varnishControl = new Varnish;
                    $varnishControl->flush();
                }
            }
        });

        static::deleted(function ($model) {
            if ($model->nodeFullClassName) {
                if (in_array(env('CACHE_DRIVER', 'file'), ['memcached', 'redis'])) {
                    Cache::tags($model->nodeFullClassName)->flush();

                    if (is_array($model->manyRelationships) &&
                        count($model->manyRelationships) > 0) {
                        try {
                            foreach ($model->manyRelationships as $manyRelation) {
                                $downlineModel = $model->getRelatedObject($manyRelation);
                                Cache::tags($downlineModel->nodeFullClassName)->flush();
                            }
                        } catch (Exception $ex) { }
                    }

                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                $uplineModel = $model->getRelatedObject($setting['relation']);
                                Cache::tags( get_class($uplineModel) )->flush();

                                if (is_array($uplineModel->manyRelationships) &&
                                    count($uplineModel->manyRelationships) > 0) {
                                    try {
                                        foreach ($uplineModel->manyRelationships as $manyRelation) {
                                            $downlineModel = $uplineModel->getRelatedObject($manyRelation);
                                            Cache::tags($downlineModel->nodeFullClassName)->flush();
                                        }
                                    } catch (Exception $ex2) { }
                                }

                                foreach ($uplineModel->attribute_settings as $key2 => $setting2) {
                                    if (isset($setting2['relation']) &&
                                        !empty($setting2['relation'])) {
                                        try {
                                            $uplineModel2 = $uplineModel->getRelatedObject($setting2['relation']);
                                            Cache::tags( get_class($uplineModel2) )->flush();

                                            if (is_array($uplineModel2->manyRelationships) &&
                                                count($uplineModel2->manyRelationships) > 0) {
                                                try {
                                                    foreach ($uplineModel2->manyRelationships as $manyRelation2) {
                                                        $downlineModel2 = $uplineModel2->getRelatedObject($manyRelation2);
                                                        Cache::tags($downlineModel2->nodeFullClassName)->flush();
                                                    }
                                                } catch (Exception $ex3) { }
                                            }
                                        } catch (Exception $ex2) { }
                                    }
                                }
                            } catch (Exception $ex) { }
                        }
                    }
                } else {
                    $model->flushTaggingCacheAlternative($model->nodeFullClassName);

                    if (is_array($model->manyRelationships) &&
                        count($model->manyRelationships) > 0) {
                        try {
                            foreach ($model->manyRelationships as $manyRelation) {
                                $downlineModel = $model->getRelatedObject($manyRelation);
                                $downlineModel->flushTaggingCacheAlternative($downlineModel->nodeFullClassName);
                            }
                        } catch (Exception $ex) { }
                    }

                    SuitModel::$isFormGeneratorContext = false;
                    foreach ($model->attribute_settings as $key => $setting) {
                        if (isset($setting['relation']) &&
                            !empty($setting['relation'])) {
                            try {
                                $relatedModel = $model->getRelatedObject($setting['relation']);
                                $relatedModel->flushTaggingCacheAlternative(get_class($relatedModel));

                                if (is_array($relatedModel->manyRelationships) &&
                                    count($relatedModel->manyRelationships) > 0) {
                                    try {
                                        foreach ($relatedModel->manyRelationships as $manyRelation) {
                                            $downlineModel = $relatedModel->getRelatedObject($manyRelation);
                                            $downlineModel->flushTaggingCacheAlternative($downlineModel->nodeFullClassName);
                                        }
                                    } catch (Exception $ex2) { }
                                }

                                foreach ($relatedModel->attribute_settings as $key2 => $setting2) {
                                    if (isset($setting2['relation']) &&
                                        !empty($setting2['relation'])) {
                                        try {
                                            $relatedModel2 = $relatedModel->getRelatedObject($setting2['relation']);
                                            $relatedModel2->flushTaggingCacheAlternative(get_class($relatedModel2));

                                            if (is_array($relatedModel2->manyRelationships) &&
                                                count($relatedModel2->manyRelationships) > 0) {
                                                try {
                                                    foreach ($relatedModel2->manyRelationships as $manyRelation2) {
                                                        $downlineModel2 = $relatedModel2->getRelatedObject($manyRelation2);
                                                        $downlineModel2->flushTaggingCacheAlternative($downlineModel2->nodeFullClassName);
                                                    }
                                                } catch (Exception $ex3) { }
                                            }
                                        } catch (Exception $ex2) { }
                                    }
                                }
                            } catch (Exception $ex) { }
                        }
                    }
                }
                // varnish flush if used
                if (config('suitcore.varnishcacheable')) {
                    $varnishControl = new Varnish;
                    $varnishControl->flush();
                }
            }
        });
    }
}
