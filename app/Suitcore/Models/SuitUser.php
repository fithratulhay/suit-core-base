<?php

namespace Suitcore\Models;

use DateTime;
use DateTimeZone;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Suitcore\Notifications\Notifiable;
use Suitcore\Models\SuitModel;


/*
|--------------------------------------------------------------------------
| users Table Structure
|--------------------------------------------------------------------------
- id INT(10) NOT NULL
- name VARCHAR(255) NOT NULL
- username VARCHAR(255) NOT NULL
- email VARCHAR(255) NOT NULL
- password VARCHAR(80) NOT NULL
- birthdate DATETIME NULL
- picture VARCHAR(255) NULL
- phone_number VARCHAR(15)
- fb_id VARCHAR(255)
- fb_access_token VARCHAR(255)
- tw_id VARCHAR(255)
- tw_access_token VARCHAR(255)
- gp_id VARCHAR(255)
- gp_access_token VARCHAR(255)
- referral_code VARCHAR(32) NULLABLE
- referral_user_id INT(10) NULLABLE TO users
- role VARCHAR(32) NOT NULL
- status VARCHAR(32) NOT NULL
- registration_date DATETIME NOT NULL
- last_visit qaDATETIME NOT NULL
- forget_password_token
- remember_token VARCHAR(100)
- created_at DATETIME NOT NULL
- updated_at DATETIME NOT NULL
*/
class SuitUser extends SuitModel implements AuthenticatableContract, CanResetPasswordContract
{
    // TRAIT USAGES
    use Notifiable;
    use Authenticatable;
    use CanResetPassword;

    // CONSTANTS
    // User Basic Role
    const ADMIN = 'admin';
    const USER = 'user';
    // User Basic Status
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_UNREGISTERED = 'unregistered';
    const STATUS_CLOSED = 'closed';
    const STATUS_BANNED = 'banned';

    // TABLE DEFINITION
    public $table = 'users';

    // VIEW CONFIG
    protected static $bufferAttributeSettings = null;

    // GLOBAL FILLABLE
    public $fillable = [
        'name',
        'username',
        'email',
        'password',
        'birthdate',
        'picture',
        'phone_number',
        'fb_id',
        'fb_access_token',
        'tw_id',
        'tw_access_token',
        'gp_id',
        'gp_access_token',
        'referral_code',
        'referral_user_id',
        'role',
        'status',
        'registration_date',
        'last_visit',
        'forget_password_token',
        'remember_token',
    ];

    // Validation Rules
    public $rules = [
        'username' => 'required|alpha_num|unique:users',
        'email' => 'required|email|unique:users',
        'password' => 'required',
        'name' => 'required',
        'birthdate'=> 'date',
        'phone_number' => 'numeric|nullable',
        'role'=>'required',
        'status'=>'required',
    ];

    // Not Visible in API Result
    protected $hidden = [
        'password',
        'forget_password_token',
        'remember_token',
        'fb_id',
        'fb_access_token',
        'tw_id',
        'tw_access_token',
        'gp_id',
        'gp_access_token'
    ];

    // Path / Location used by Thumbnailer Trait
    protected $imageAttributes = [
        'picture' => 'userprofilepictures'
    ];

    // Path / Location used by Uploader Trait
    protected $files = [
        'picture' => 'userprofilepictures'
    ];

    protected $extendedThumbnailStyle = [
        'grid_thumb' => '36x36',
        'grid_thumb_large' => '72x72'
    ];

    // Datetime casting
    protected $dates = ['birthdate', 'registration_date', 'last_visit'];

    // Additional Mutator
    protected $appends = ['age'];

    // MUTATOR
    public function getAgeAttribute() {
        return $this->birthdate ? $this->birthdate->diffInYears() : 0;
    }

    public function getPushRegistrationIdsAttribute() {
        return ($this->registrationIds ? $this->registrationIds->pluck('registration_id')->toArray() : []);
    }

    /**
     * Get the route key for the model if used in routes
     *
     * @return string
     */
    public function getRouteKeyName() {
        return 'username';
    }

    // RELATIONSHIP
    /**
     * @return SuitUser |\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referralUser() {
        return $this->belongsTo(SuitUser::class, 'referral_user_id');
    }

    /**
     * Get the oauth for the user.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function oauths()
    {
        return $this->hasMany(SuitOauthUser::class, 'user_id');
    }

    /**
     * Get the registration-ids for the user.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function registrationIds()
    {
        return $this->hasMany(SuitPushNotificationRegId::class, 'user_id');
    }

    // SERVICES
    // Predicate
    public function isGuest()
    {
        return $this->status == static::STATUS_UNREGISTERED;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }


    // Model Domain
    public function scopeUsers($query)
    {
        return $query->where('users.role', '=', self::USER);
    }

    public function scopeAdmins($query)
    {
        return $query->where('users.role', '=', self::ADMIN);
    }

    public function scopeActive($query)
    {
        return $query->where('users.status', '=', self::STATUS_ACTIVE);
    }

    public function scopeInactive($query)
    {
        return $query->where('users.status', '=', self::STATUS_INACTIVE);
    }

    public function scopeSimpleSearch($query, $searchTerm) {
        return $query->where(function ($qsearch) use($searchTerm) {
            // name and lastname
            $searchTerm = preg_replace("/[^a-zA-Z0-9\" ]+/", "\\ $0", $searchTerm);
            $searchTerm = str_replace("\\ ", "\\", $searchTerm);
            $searchTerms = implode('|', array_unique( explode(' ', preg_replace('!\s+!', ' ', trim($searchTerm))) ) );
            $qsearch->where('name','rlike',$searchTerms);
        });
    }

    // Simple Action
    public function activate()
    {
        return $this->update(['status' => self::STATUS_ACTIVE]);
    }

    public function deactivate()
    {
        return $this->update(['status' => self::STATUS_INACTIVE]);
    }

    public function updateLastVisit()
    {
        return $this->update(['last_visit' => Carbon::now()]);
    }

    public function generateReffCode()
    {
        $code = null;
        do {
            $code = generateRandomString(8);
        } while ($this->referral_code == $code);
        $this->update(['referral_code' => $code]);
        return $code;
    }

    public function generateActivationCode()
    {
        if ($this->status != static::STATUS_INACTIVE) {
            return false;
        }
        $code = $this->generateReffCode();
        return md5($code. $this->created_at);
    }

    public function generateResetPasswordToken()
    {
        $code = $this->generateReffCode();
        return md5($code. $this->created_at . str_random(32));
    }

    public function matchActivationCode($code)
    {
        return md5($this->referral_code. $this->created_at) == $code;
    }

    // Enumeration List / Options
    /**
     * Get options of role
     *
     */
    public function getRoleOptions()
    {
        $defaultRole = [
            self::ADMIN => ucfirst(strtolower(self::ADMIN)),
            self::USER => ucfirst(strtolower(self::USER))
        ];
        $roles = SuitUserRole::pluck('name', 'code');
        return array_merge($defaultRole, $roles->toArray());
    }

    /**
     * Get options of status
     *
     */
    public function getStatusOptions()
    {
        return [self::STATUS_ACTIVE => ucfirst(strtolower(self::STATUS_ACTIVE)),
                self::STATUS_INACTIVE => ucfirst(strtolower(self::STATUS_INACTIVE)),
                self::STATUS_UNREGISTERED => ucfirst(strtolower(self::STATUS_UNREGISTERED)),
                self::STATUS_CLOSED => ucfirst(strtolower(self::STATUS_CLOSED)),
                self::STATUS_BANNED => ucfirst(strtolower(self::STATUS_BANNED))
        ];
    }

    /**
     * Get options of timezone
     *
     */
    public function getTimezoneOptions()
    {
        $regions = [
            DateTimeZone::AFRICA,
            DateTimeZone::AMERICA,
            DateTimeZone::ANTARCTICA,
            DateTimeZone::ASIA,
            DateTimeZone::ATLANTIC,
            DateTimeZone::AUSTRALIA,
            DateTimeZone::EUROPE,
            DateTimeZone::INDIAN,
            DateTimeZone::PACIFIC,
        ];

        $timezones = [];
        foreach ($regions as $region) {
            $timezones = array_merge($timezones, DateTimeZone::listIdentifiers($region));
        }

        $timezoneOffsets = [];
        foreach ($timezones as $timezone) {
            $tz = new DateTimeZone($timezone);
            $timezoneOffsets[$timezone] = $tz->getOffset(new DateTime);
        }
        asort($timezoneOffsets);

        $timezoneList = [];
        foreach( $timezoneOffsets as $timezone => $offset )
        {
            $offsetPrefix = $offset < 0 ? '-' : '+';
            $offsetFormatted = gmdate( 'H:i', abs($offset) );
            $prettyOffset = "UTC${offsetPrefix}${offsetFormatted}";
            $timezoneList[$timezone] = "(${prettyOffset}) $timezone";
        }

        return $timezoneList;
    }

    // View Config Context
    public function getLabel()
    {
        return (trans("model.user") == "model.user" ? "User" : trans("model.user"));
    }

    public function getFormattedValue()
    {
        return $this->name . " (" . $this->email . ")";
    }

    public function getFormattedValueColumn()
    {
        return ['name', 'email'];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getImportExcelKeyBaseName() {
        return ['email'];
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Full Name"
            ],
            "username" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Username"
            ],
            "email" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Email Address"
            ],
            "password" => [
                "type" => self::TYPE_PASSWORD,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Password"
            ],
            "birthdate" => [
                "type" => self::TYPE_DATE,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Birthdate"
            ],
            "picture" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Profile Picture"
            ],
            "phone_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Phone Number"
            ],
            "referral_code" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Referral Code"
            ],
            "referral_user_id" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => "referralUser",
                "label" => "Optional User Code"
            ],
            "role" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Role",
                "options" => $this->getRoleOptions(),
                "filterable" => true
            ],
            "timezone" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Timezone",
                "options" => $this->getTimezoneOptions()
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "filterable" => true
            ],
            "registration_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Registration Date",
                "filterable" => true
            ],
            "last_visit" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "Last Visit"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }
}
