<?php

namespace Suitcore\Models;

use Route;

/*
|--------------------------------------------------------------------------
| permissions Table Structure
|--------------------------------------------------------------------------
| * id
| * role_id
| * route_name
| * status ('accept', 'deny', 'mapping_accept')
| * optional_user_mapping_field
| * optional_object_mapping_field
| * created_at
| * updated_at
*/
class SuitUserPermission extends SuitModel
{
    // CONSTANT
    const ACCEPT = 'accept';
    const DENY = 'deny';
    const MAPPINGACCEPT = 'mapping_accept';

    // MODEL DEFINITION
    public $table = 'permissions';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'role_id',
        'route_name',
        'status',
        'optional_user_mapping_field',
        'optional_object_mapping_field'
    ];

    public function rules()
    {
        $rules = [
            'role_id' => 'exists:roles,id',
            'route_name' => 'required|string',
            'status' => 'required|string',
            'optional_user_mapping_field' => 'nullable|string',
            'optional_object_mapping_field' => 'nullable|string'
        ];
        return $rules;
    }

    public function getStatusOptions()
    {
        return [
            self::ACCEPT => strtoupper(self::ACCEPT),
            self::DENY => strtoupper(self::DENY),
            self::MAPPINGACCEPT => 'ACCEPT BY MAPPING FIELD'
        ];
    }

    public function getRoutesOptions() 
    {
        $routeRawCollection = Route::getRoutes();
        $routeCollection = [];
        foreach($routeRawCollection as $item) {
            $routeName = $item->getName();
            $routeNameElmt = explode('.', $routeName);
            $endpoint = isset($routeNameElmt[0]) ? strtoupper($routeNameElmt[0]) : "";
            $section = isset($routeNameElmt[1]) ? ucfirst(strtolower($routeNameElmt[1])) : "";
            $action = isset($routeNameElmt[2]) ? ucfirst(strtolower($routeNameElmt[2])) : "";
            if ($endpoint != "BACKEND" || (isset($routeNameElmt[3]) && $routeNameElmt[3] == 'json') || (strpos($routeName, 'json') !== false) ) {
                // excluded or any action
            } else {
                $routeCollection[$routeName] = $endpoint . " - " . $action . " " . $section . " (". $routeName . ")";
            }
        }
        //ksort($routeCollection);
        return $routeCollection;
    }

    public function role()
    {
        return $this->belongsTo(SuitUserRole::class, 'role_id');
    }

    public function getFullNameAttribute()
    {
        return ($this->role ? $this->role->name : 'Unknown') . ' - ' . $this->route_name . ' : ' . $this->status;
    }

    public function getLabel()
    {
        return "Role Permissions";
    }

    public function getFormattedValue()
    {
        return $this->getFullNameAttribute();
    }

    public function getFormattedValueColumn()
    {
        return ['route_name', 'status'];
    }

    public function getDefaultOrderColumn()
    {
        return 'route_name';
    }

    public function getDefaultOrderColumnDirection()
    {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "role_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "role",
                "label" => "Role",
                "options" => (new SuitUserRole)->all()->pluck('name','id'),
                "filterable" => true
            ],
            "route_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Route Name",
                "options" => $this->getRoutesOptions()
            ],
            'status' => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "filterable" => true
            ],
            'optional_user_mapping_field' => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional User Mapping Field"
            ],
            'optional_object_mapping_field' => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Object Mapping Field"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }
}
