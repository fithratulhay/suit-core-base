<?php

namespace Suitcore\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
user_logs
========================
- id (integer - primarykey)
- user_id 
- type
- url (varchar 1024)
- request
- created_at (datetime)
- updated_at (datetime)
**/
class SuitUserLog extends SuitModel
{
    // TABLE DEFINITION
    public $table = 'user_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
    	'user_id', 'url', 'request', 'type'
    ];

    public $rules = array(
        'user_id' => 'required',
        'type' => 'required',
        'url' => 'required'
    );

    public function user() {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getLabel() {
        return "User Log";
    }

    public function getFormattedValue() {
        return ($this->user ? $this->user : 'Unknown User') . ' Activity';
    }

    public function getOptions() {
        return self::all();
    }

    /**
     * Get options of status
     *
     */
    public function getTypeOptions() {
        return ['get' => "HTTP-GET", 
                'post' => "HTTP-POST"
        ];
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Logged At",
                "filterable" => true
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => 'user',
                "label" => "By User",
                "options" => null,
                "options_url" => route(config('suitcore.data_options_route_name.suituser')),
                "filterable" => true
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Type",
                "options" => $this->getTypeOptions(),
                "filterable" => true
            ],
            "url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Access URL"
            ],
            "request" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => false,
                "required" => false,
                "relation" => null,
                "label" => "HTTP Request"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }
}
