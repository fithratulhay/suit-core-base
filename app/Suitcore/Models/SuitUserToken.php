<?php

namespace Suitcore\Models;

use Carbon\Carbon;
use Suitcore\Models\SuitModel;

/**
user_tokens :
- id
- user_id (users entity ID)
- token
- last_used
- created_at (datetime)
- updated_at (datetime)
**/
class SuitUserToken extends SuitModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['user_id', 'token', 'last_used'];

    /**
     * The attributes that are visible 
     *
     * @var array
     */
    protected $visible = ['token', 'user'];

    protected $dates = ['last_used']; 
	
	// METHODS
	/**
	 * Relationship to User
	 **/
	public function user()
    {
		return $this->belongsTo(User::class, 'user_id');
	}

    public function updateLastUsed()
    {
        $this->update(['last_used' => Carbon::now()]);
    }

    public function generateToken()
    {
        return md5($this->getKey().'#'.env('APP_KEY', 'suitcore').'#'.date('YmdHis').str_random(32));
	}
}
