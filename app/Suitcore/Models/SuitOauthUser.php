<?php

namespace Suitcore\Models;

use Suitcore\Models\SuitModel;

class SuitOauthUser extends SuitModel
{
    public $table ='oauth_users';

    public $fillable = ['provider', 'oauth_id', 'user_id', 'graph'];

    protected $casts = ['graph' => 'array'];

    /**
     * @return SuitUser|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }
}
