<?php

namespace Suitcore\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitUser;

/**
 * @property int $user_id
 * @property string $key
 * @property string $value
 * @property-read User
 */
class SuitUserSetting extends SuitModel
{
    protected $table = 'user_settings';

    public $fillable = ['user_id', 'key', 'value'];

    public $rules = [
        'user_id'  => 'required|exists:users,id',
        'key'  => 'required'
    ];

    public function scopeForUser($query, $userId) {
        return $query->where('user_id', '=', $userId);
    }

    /**
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(SuitUser::class, 'user_id');
    }

    public function getFullNameAttribute()
    {
        return ($this->user ? $this->user->getFullNameAttribute() : "Unknown") . ' - ' . $this->key;
    }

    public function getLabel()
    {
        return "User Setting";
    }

    public function getFormattedValue()
    {
        return $this->getFullNameAttribute();
    }

    public function getFormattedValueColumn()
    {
        return ['key'];
    }

    public function getDefaultOrderColumn()
    {
        return 'key';
    }

    public function getDefaultOrderColumnDirection()
    {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "user_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => "user",
                "label" => "Related User",
                "options" => (SuitModel::$isFormGeneratorContext ? (new SuitUser)->all()->pluck('name','id') : []),
                "filterable" => true
            ],
            "key" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Key"
            ],
            "value" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Value"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forget('user_setting_' . $model->user_id);
        });

        static::deleted(function ($model) {
            Cache::forget('user_setting_' . $model->user_id);
        });
    }
}
