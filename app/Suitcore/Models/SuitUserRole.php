<?php

namespace Suitcore\Models;

use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| roles Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * code VARCHAR(32) NOT NULL
| * name VARCHAR(128) NOT NULL
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class SuitUserRole extends SuitModel
{
    // CONSTANT
    const DEFAULT_ADMIN = 'admin';
    const DEFAULT_USER = 'user';

    // MODEL DEFINITION
    public $table = 'roles';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'code',
        'name',
        'subpath',
        'theme',
        'base_color'
    ];

    public function rules()
    {
        $rules = [
            'code' => 'required|string',
            'name' => 'required|string'
        ];
        return $rules;
    }

    /**
     * Get the permissions for the user.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function permissions()
    {
        return $this->hasMany(SuitUserPermission::class, 'role_id');
    }

    public function getFullNameAttribute()
    {
        return $this->name . ' (' . $this->code . ')';
    }

    public function getLabel()
    {
        return "User Role";
    }

    public function getFormattedValue()
    {
        return $this->getFullNameAttribute();
    }

    public function getFormattedValueColumn()
    {
        return ['name', 'code'];
    }

    public function getDefaultOrderColumn()
    {
        return 'code';
    }

    public function getDefaultOrderColumnDirection()
    {
        return 'asc';
    }

    /**
     * Get options of status
     *
     */
    public function getThemeOptions() {
        return config('suitcore.backend_theme');
    }

    /**
     * Get options of status
     *
     */
    public function getThemeBaseColorOptions() {
        return config('suitcore.backend_theme_base_color');
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "code" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Code"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name"
            ],
            'subpath' => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Subpath / Subdomain"
            ],
            'theme' => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Theme",
                "options" => $this->getThemeOptions()
            ],
            'base_color' => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Theme Base Color",
                "options" => $this->getThemeBaseColorOptions()
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forget('role_list');
            Cache::forget('role_subpath_list');
            Cache::forget('role_code_list');
            Cache::forget('role_object_list');
        });

        static::deleted(function ($model) {
            Cache::forget('role_list');
            Cache::forget('role_subpath_list');
            Cache::forget('role_code_list');
            Cache::forget('role_object_list');
        });
    }   
}
