<?php

namespace Suitcore\Models;

use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| url_redirections Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * source_url VARCHAR(32) NOT NULL
| * destination_url VARCHAR(128) NOT NULL
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class SuitUrlRedirection extends SuitModel
{
    // MODEL DEFINITION
    public $table = 'url_redirections';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'source_url',
        'destination_url'
    ];

    public function rules()
    {
        $rules = [
            'source_url' => 'required|string|max:1024|unique:url_redirections',
            'destination_url' => 'required|string|max:1024'
        ];
        return $rules;
    }

    public function getFullNameAttribute()
    {
        return $this->source_url;
    }

    public function getLabel()
    {
        return "URL Redirection";
    }

    public function getDescription() {
        return "Redirect non-existing URL to existing URL";
    }

    public function getFormattedValue()
    {
        return $this->getFullNameAttribute();
    }

    public function getFormattedValueColumn()
    {
        return ['source_url'];
    }

    public function getDefaultOrderColumn()
    {
        return 'source_url';
    }

    public function getDefaultOrderColumnDirection()
    {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "source_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Source (Non-Existing) URL"
            ],
            "destination_url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Target (Existing) URL"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forget('url_redirection_list');
        });

        static::deleted(function ($model) {
            Cache::forget('url_redirection_list');
        });
    }   
}
