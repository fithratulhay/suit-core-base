<?php

namespace Suitcore\Models;

use Carbon\Carbon;
use Suitcore\Models\SuitModel;

/**
push_notification_reg_ids :
- id
- user_id
- registration_id
- device_type
- last_used
- created_at (datetime)
- updated_at (datetime)
**/
class SuitPushNotificationRegId extends SuitModel
{
    // CONSTANTS
    // Basic Device Type
    const TYPE_WINDOWS = 'windows';
    const TYPE_MACOS = 'macos';
    const TYPE_ANDROID = 'android';
    const TYPE_IOS = 'ios';
    const TYPE_BROWSER = 'browser';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'push_notification_reg_ids';

    protected $fillable = ['user_id', 'registration_id', 'device_type', 'last_used'];

    protected $dates = ['last_used'];  

    // METHODS
    /**
     * Relationship to User
     **/
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope
     **/
    public function scopeDevice($query, $deviceType) {
        return $query->where('device_type', $deviceType);
    }

    public function updateLastUsed()
    {
        $this->update(['last_used' => Carbon::now()]);
    }

    public function changeGcmRegistrationId($registrationId)
    {
        if (static::where('registration_id', $registrationId)->get() != null) 
        {
            $this->delete();
            return;
        }
        $this->update(['registration_id' => $registrationId]);
    }
}
