<?php

namespace Suitcore\Models;

use Suitcore\Models\SuitModel;

class SuitUrlSeo extends SuitModel
{
    // MODEL DEFINITION
    public $table = 'url_seo';

    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'url',
        'title',
        'keyword',
        'description'
    ];

    public $rules = [
        'url' => 'required',
        'title' => 'required',
        'keyword' => 'required',
        'description' => 'required',
    ];

    public function getLabel()
    {
        return 'URL SEO';
    }

    public function getFormattedValue()
    {
        return $this->url;
    }

    public function getFormattedValueColumn()
    {
        return ['url'];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'url' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'URL Relative Path',
            ],
            'title' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Title',
            ],
            'keyword' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Keyword(s)',
            ],
            'description' => [
                'type' => self::TYPE_TEXTAREA,
                'visible' => false,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Description',
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }
}
