<?php

namespace Suitcore\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class DatabaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suitcore:backup-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will take a backup of the current database, after this please rsync target folder ([approotfolder]/storage/app/backup/database) to remote host.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Settings
        $format = "YmdHi";
        $storageDriver = "local";
        $date = Carbon::now()->format($format);
        $host = env('DB_HOST', '127.0.0.1');
        $user = env('DB_USERNAME', 'username');
        $password = env('DB_PASSWORD', 'password');
        $database = env('DB_DATABASE', 'database');
        $dataRetention = env('DB_BACKUP_RETENTION_DAYS', 7);
        $command = "mysqldump -h {$host} -u {$user} -p{$password} {$database} > {$date}.sql";

        // Run the command
        $process = new Process($command);
        // $process->start();
        try {
            $process->mustRun();
            
            // Init Storage
            $local = Storage::disk($storageDriver);

            // When the process is done
            if ($process->isSuccessful()) {
                $local->put('backup/database/'. $date . '.sql' ,  file_get_contents("{$date}.sql"));
                unlink("{$date}.sql");
            }

            // clean based on retention setting
            $minDate = Carbon::now()->subDays($dataRetention);
            $files = $local->allFiles("backup/database/");
            $subNameLength = strlen($date . '.sql');
            foreach ($files as $file) {
                $tempName = substr($file, -$subNameLength);
                $fileDate = Carbon::createFromFormat($format, str_replace(".sql", "", $tempName));
                if ($fileDate->lt($minDate) && $local->exists($file)) {
                    $local->delete($file);
                }
            }

            // if local storage try to rsync
            if ($storageDriver == "local") {
                try {
                    $basePath = base_path();
                    $rsyncHost = env('BACKUP_HOST', '');
                    $rsyncUser = env('BACKUP_USER', '');
                    $rsyncPath = env('BACKUP_PATH', '');
                    if (!empty($rsyncHost) && !empty($rsyncUser) && !empty($rsyncPath)) {
                        $command = "rsync -avz {$basePath}/storage/app/backup/database/ {$rsyncUser}@{$rsyncHost}:{$rsyncPath}";
                        $process = new Process($command);
                        $process->mustRun();
                    }
                } catch (ProcessFailedException $er) { 
                    echo $er->getMessage();
                }
            }
        } catch (ProcessFailedException $e) {
            echo $e->getMessage();
        }
    }
}
