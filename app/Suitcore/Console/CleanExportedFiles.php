<?php

namespace Suitcore\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CleanExportedFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suitcore:clean-export-file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete previous exported files before now.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'start clean exported files' . PHP_EOL;
        $now = date('dmYhis').'.xlsx';
        $subNameLength = strlen($now);
        $baseDirName = public_path('exports');
        $files = File::allFiles($baseDirName);
        foreach ($files as $file) {
            $tempName = substr($file, -$subNameLength);
            if ($tempName < $now && File::exists($file)) {
                File::delete($file);
            }
        }
        echo 'finish clean exported files' . PHP_EOL;
    }
}
