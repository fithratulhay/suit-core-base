<?php

namespace Suitcore\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

/**
 * Publish the suitcore assets to the public directory
 *
 */
class PublishCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'suitcore:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the suitcore assets';

    /** @var Filesystem $fs */
    protected $files;

    protected $publishPath;

    /**
     * Create a new Publish command
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     * @param string $publishPath
     */
    public function __construct($files, $publishPath)
    {
        parent::__construct();

        $this->files = $files;
        $this->publishPath = $publishPath;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $package = 'suitcore';
        $destination = $this->publishPath . "/{$package}";

        if ( ! is_null($path = $this->getSuitcorePath())) {
            if ($this->files->exists($destination)) {
                $this->files->deleteDirectory($destination);
                $this->info('Old published Assets have been removed');
            }
            $copyElfinder = $this->copySuitcoreFiles($destination);
        } else {
            $copyElfinder = false;
            $this->error('Could not find suitcore path');
        }

        if ( ! is_null($path = $this->getPath())) {
            $copyPublic = $this->files->copyDirectory($path, $destination);
        } else {
            $copyPublic = false;
            $this->error('Could not find public path');
        }

        if ($copyElfinder && $copyPublic) {
            $this->info('Published assets to: '.$package);
        } else {
            $this->error('Could not publish alles assets for '.$package);
        }

    }

    /**
     * Copy specific directories from elFinder to their destination
     *
     * @param $destination
     * @return bool
     */
    protected function copySuitcoreFiles($destination)
    {
        $result = true;
        $directories = array('elfinder');
        $suitcorePath = $this->getSuitcorePath();
        foreach($directories as $dir){
            $path = $suitcorePath.'/'.$dir;
            $success = $this->files->copyDirectory($path, $destination.'/'.$dir);
            $result = $success && $result;
        }
        return $result;
    }

    /**
     *  Get the path of the public folder, to merge with the elFinder folders.
     */
    protected function getPath(){
        return __DIR__ .'/../Resources/assets';
    }

    /**
     * Find the suitcore path from the vendor dir.
     *
     * @return string
     */
    protected function getSuitcorePath()
    {
        $reflector = new \ReflectionClass('\Suitcore\Models\SuitModel');
        return realpath(dirname($reflector->getFileName()) . '/..');
    }
}
