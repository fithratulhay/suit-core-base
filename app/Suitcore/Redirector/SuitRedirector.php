<?php

namespace Suitcore\Redirector;

use Symfony\Component\HttpFoundation\Request;
use Spatie\MissingPageRedirector\Redirector\Redirector;
use Suitcore\Repositories\Contract\SuitUrlRedirectionRepositoryContract;

class SuitRedirector implements Redirector
{
	protected $urlRedirectionRepo;

	public function __construct(SuitUrlRedirectionRepositoryContract $_urlRedirectionRepo)
    {
        $this->urlRedirectionRepo = $_urlRedirectionRepo;
    }

    public function getRedirectsFor(Request $request): array
    {
    	$urlRedirectionList = $this->urlRedirectionRepo->all();
    	$listUrlRedirection = [];
    	foreach ($urlRedirectionList as $key => $obj) {
    		$listUrlRedirection[$obj->source_url] = $obj->destination_url;
    	}
        return $listUrlRedirection;
    }
}
