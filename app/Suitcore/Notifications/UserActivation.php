<?php

namespace Suitcore\Notifications;

use Suitcore\Config\DefaultConfig;

class UserActivation extends BaseNotification
{
    protected $view = 'emails.users.activation';
    
    public function setMessage($notifiable)
    {
        $this->message = trans('notification.messages.users.activation');
    }
    
    // only notif by email
    public function via($notifiable)
    {
        return ['mail'];
    }
}
