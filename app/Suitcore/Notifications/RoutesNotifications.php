<?php

namespace Suitcore\Notifications;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Contracts\Notifications\Dispatcher;

trait RoutesNotifications
{
    public $emailPreview = false;
    
    /**
     * Get Notifiable Instances from given parameters
     *
     * @param  mixed  $instanceName
     * @param  mixed  $params
     * @return Object
     */
    public function getNotifiableInstance($instanceName, ...$params) {
        if (is_object($instanceName) || class_exists($instanceName)) {
            $instance = $instanceName;
            if (!is_object($instance)) {
                return (new $instance(...$params));
            } 
            return $instance;
        }
        $instanceClass = 'App\\Notifications\\'.$instanceName;
        $instanceClass = class_exists($instanceClass) ?
                    $instanceClass :
                    'Suitcore\\Notifications\\' . $instanceName;
        if (class_exists($instanceClass)) {
            return (new $instanceClass(...$params)); 
        }
        return null;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $instanceName
     * @param  mixed  $params
     * @return void|MailMessage
     */
    public function notify($instanceName, ...$params)
    {
        if ($instance = $this->getNotifiableInstance($instanceName, ...$params)) {
            try {
                if (isset($this->emailPreview) && $this->emailPreview) {
                    return $instance->toMail($this);
                }
                app(Dispatcher::class)->send($this, $instance);
            } catch (Exception $e) { 
                // possibility of wrong email address
                // info($e->getMessage());
            }
        }
    }

    /**
     * Send the given notification immediately.
     *
     * @param  mixed  $instanceName
     * @param  mixed  $params
     * @return void|MailMessage
     */
    public function notifyNow($instanceName, ...$params)
    {
        if ($instance = $this->getNotifiableInstance($instanceName, ...$params)) {
            try {
                app(Dispatcher::class)->sendNow($this, $instance, null);
            } catch (Exception $e) { 
                // possibility of wrong email address
                // info($e->getMessage());
            }
        }
    }

    /**
     * Get the notification routing information for the given driver.
     *
     * @param  string  $driver
     * @return mixed
     */
    public function routeNotificationFor($driver)
    {
        if (method_exists($this, $method = 'routeNotificationFor'.Str::studly($driver))) {
            return $this->{$method}();
        }

        switch ($driver) {
            case 'database':
                return $this->notifications();
            case 'mail':
                return isset($this->email) && is_array($this->email) ? $this->email : ((!str_contains($this->email, '@facebook.com') && !str_contains($this->email, '@twitter.com')) ? $this->email : null);
            case 'one_signal': // compatibility for old-drivers
                return (isset($this->push_registration_ids) && is_array($this->push_registration_ids) && count($this->push_registration_ids) > 0 ? $this->push_registration_ids : null);
            case 'OneSignal':
                return (isset($this->push_registration_ids) && is_array($this->push_registration_ids) && count($this->push_registration_ids) > 0 ? $this->push_registration_ids : null);
            case 'nexmo':
                return $this->phone_number;
            case 'zenziva':
                return $this->phone_number;
        }
    }
}
