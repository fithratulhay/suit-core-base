<?php

namespace Suitcore\Notifications;

use Suitcore\Config\DefaultConfig;

class UserRegistered extends BaseNotification
{
    protected $view = 'emails.users.registered';

    public function setMessage($notifiable)
    {
        $this->message = trans('notification.messages.users.registered');
    }

    // only notif by email
    public function via($notifiable)
    {
        return ['mail'];
    }
}
