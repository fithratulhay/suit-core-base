<?php

namespace Suitcore\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;
use TuxDaemon\ZenzivaNotificationChannel\ZenzivaMessage;
use TuxDaemon\ZenzivaNotificationChannel\ZenzivaChannel;
use Suitcore\Config\DefaultConfig;
use Suitcore\Notifications\Channels\DatabaseChannel;
use Suitcore\Notifications\Contracts\EmailTemplateInterface as EmailTemplateContract;

class BaseNotification extends Notification implements ShouldQueue
{
    use Queueable;

    // Base Predicate
    protected $forcePushNotificationOnly;

    // Common
    protected $dataView = [];
    protected $message;
    protected $link;

    // Specific Email Based Channel
    protected $view;
    protected $subject;
    protected $actionButtonText;
    protected $beforeActionButton;
    protected $afterActionButton;

    // Specific Push-Notification Based Channel
    protected $dataPushMessage = [];

    // Specific SMS Based Channel
    protected $shortMessage;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($link = null)
    {
        $template = $this->getClassName();
        $emailSetting = app(EmailTemplateContract::class)->getSetting($template);

        if (!$emailSetting) {
            $emailSetting = config('suitapp.emailsettings.templates.'.$template);
            if (!$emailSetting) {
                // if class basename not found, try with full class name
                $template = get_class($this);
                $emailSetting = app(EmailTemplateContract::class)->getSetting($template);
                if (!$emailSetting) {
                    $emailSetting = config('suitapp.emailsettings.templates.'.$template);
                    if (!$emailSetting) {
                        // if class basename not found, try with full class name
                        $template = "\\" . get_class($this);
                        $emailSetting = app(EmailTemplateContract::class)->getSetting($template);
                        if (!$emailSetting) {
                            $emailSetting = config('suitapp.emailsettings.templates.'.$template);
                        }
                    }
                }
            }
        } 

        if ($emailSetting) {
            $this->subject = $emailSetting['subject'];
            $this->actionButtonText = $emailSetting['action_button_text'];
            $this->beforeActionButton = $emailSetting['before_action_button'];
            $this->afterActionButton = $emailSetting['after_action_button'];
        }

        $viewConfig = isset(DefaultConfig::getConfig()['user_email_notification'][$template]) ? DefaultConfig::getConfig()['user_email_notification'][$template] : null;

        $this->view = $this->view ?: ($viewConfig ?: $this->getDefaultMailLayout());
        $this->link = $link ?: $this->link;
        $this->forcePushNotificationOnly = false;
    }

    protected function getClassName()
    {
        return class_basename(static::class);
    }

    public function getDefaultMailLayout()
    {
        return config('suitapp.emailsettings.layout');
    }

    public function setMessage($notifiable)
    {
        // For common message (database channel, sms channel, email channel, push-notification channel )
    }

    public function setShortMessage($notifiable)
    {
        // For sms channel
    }

    public function setDataView($notifiable)
    {
        // For email channel or push-notification channel (second-option)
    }

    public function setDataPushMessage($notifiable)
    {
        // For push-notification channel (first option), related to payoad-size-constraint
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $className = $this->getClassName();

        $via = [];

        if (settings($className.'_email')) {
            $via[] = 'mail';
        }

        if (settings($className.'_database')) {
            $via[] = DatabaseChannel::class;
        }

        if (settings($className.'_webpush')) {
            $via[] = OneSignalChannel::class;
        }

        if (settings($className.'_sms')) {
            $via[] = ZenzivaChannel::class;
        }

        if ($via == []) {
            $via = ['mail', DatabaseChannel::class, OneSignalChannel::class];
        }
        
        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $this->setDataView($notifiable);
        $this->setMessage($notifiable);

        return (new MailMessage)
                    ->view($this->view, $this->dataView)
                    ->subject($this->subject)
                    ->line($this->message)
                    ->line($this->beforeActionButton)
                    ->action($this->actionButtonText, $this->link ?: url('/'))
                    ->line($this->afterActionButton);
    }

    /**
     * Get the database record representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $this->setMessage($notifiable);

        return [
            $notifiable->id,
            $this->message,
            $this->link,
        ];
    }

    /**
     * Get the push-notification representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $this->setDataPushMessage($notifiable); // top-priority
        $this->setDataView($notifiable); // secondary-priority (possibility of truncated message / undelivered message)
        $this->setMessage($notifiable);

        return OneSignalMessage::create()
            ->subject(env('APP_NAME', 'Suitcore Instances')) // ->subject($this->subject ?: env('APP_NAME', 'Suitcore Instances')) // currently set to site-name because subject is too long (expected for email notification)
            ->body(($this->message ?: 'Empty Message!'))
            ->url($this->link ?: url('/'))
            ->setData('data', (empty($this->dataPushMessage) ? $this->dataView : $this->dataPushMessage)) // for apps push-notification
            ->webButton(
                OneSignalWebButton::create('action-link')
                    ->text($this->actionButtonText ?: 'View')
                    ->icon(config('suitapp.base.icon'))
                    ->url($this->link ?: url('/'))
            );
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return ZenzivaMessage
     */
    public function toZenziva($notifiable)
    {
        $this->setShortMessage($notifiable); // top-priority
        $this->setMessage($notifiable); // secondary-priority (possibility of truncated message / undelivered message)
        return ZenzivaMessage::create(empty($this->shortMessage) ? $this->message : $this->shortMessage);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
