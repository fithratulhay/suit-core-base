<?php

namespace Suitcore\Notifications\EmailTemplate;

trait EmailTemplateTrait
{
    public function getSetting($template)
    {
        $setting = static::where('template', $template)->first();
        return $setting ? $setting->toArray() : null;
    }
}
