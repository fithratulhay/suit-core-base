<?php

namespace Suitcore\Notifications;

use Suitcore\Notifications\BaseNotification;
use NotificationChannels\OneSignal\OneSignalChannel;

class UserPushNotificationMessage extends BaseNotification
{
    public function __construct($_message, $link = null)
    {
        parent::__construct($link);
        $this->message = $_message;
    }

    public function setDataPushMessage($notifiable)
    {
        $this->dataPushMessage = [
            'type' => 'standardmessage',
            'link' => ($this->link ?: url('/')),
            'message' => $this->message
        ];
    }

    // only notify by onesignal
    public function via($notifiable)
    {
        return [ OneSignalChannel::class ];
    }
}
