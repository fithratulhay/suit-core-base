<?php

namespace Suitcore\Notifications;

use Suitcore\Config\DefaultConfig;

class UserActivated extends BaseNotification
{
    protected $view = 'emails.users.activated';
    
    public function setMessage($notifiable)
    {
        $this->message = trans('notification.messages.users.activated');
    }
    
    // only notif by email
    public function via($notifiable)
    {
        return ['mail'];
    }
}
