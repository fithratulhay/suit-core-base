<?php

namespace Suitcore\Notifications\Contracts;

interface EmailTemplateInterface
{
    public function getSetting($template);
}
