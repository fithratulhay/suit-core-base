<?php

namespace Suitcore\Notifications\Channels;

use Suitcore\Repositories\Contract\SuitNotificationRepositoryContract;
use Illuminate\Notifications\Notification;

class DatabaseChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        return app(SuitNotificationRepositoryContract::class)->add(...$notification->toDatabase($notifiable));
    }
}

