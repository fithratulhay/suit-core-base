<?php

namespace Suitcore\Notifications;

use Suitcore\Models\SuitNotification;

trait HasDatabaseNotifications
{
    /**
     * Get notifications of the user.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function notifications()
    {
        return $this->hasMany(SuitNotification::class, 'user_id')
                            ->orderBy('created_at', 'desc');
    }

    /**
     * Get the entity's unread notifications.
     */
    public function unreadNotifications()
    {
        return $this->hasMany(SuitNotification::class, 'user_id')
                            ->whereNull('is_read_')
                            ->orderBy('created_at', 'desc');
    }
}
