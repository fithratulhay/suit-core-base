<?php

namespace Suitcore\Notifications;

use Suitcore\Notifications\BaseNotification;
use TuxDaemon\ZenzivaNotificationChannel\ZenzivaMessage;
use TuxDaemon\ZenzivaNotificationChannel\ZenzivaChannel;

class UserSMSNotificationMessage extends BaseNotification
{
    public function __construct($_message, $link = null)
    {
        parent::__construct($link);
        $this->message = $_message;
    }

    public function setDataView($notifiable)
    {
        $this->dataView = [
            'link' => ($this->link ?: url('/')),
            'message' => $this->message
        ];
    }

    // only notify by zenziva-sms
    public function via($notifiable)
    {
        return [ ZenzivaChannel::class ];
    }
}
