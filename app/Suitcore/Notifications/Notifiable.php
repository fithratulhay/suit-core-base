<?php

namespace Suitcore\Notifications;

trait Notifiable
{
    use HasDatabaseNotifications, RoutesNotifications;
}
