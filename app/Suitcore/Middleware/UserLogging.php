<?php

namespace Suitcore\Middleware;

use Closure;
use Request;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Suitcore\Repositories\Contract\SuitUserLogRepositoryContract;

class UserLogging
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (config('suitcore.user_activity_logging.enable', false)) {
            $loggingService = app(SuitUserLogRepositoryContract::class);
            $defaultExceptionRoutes = [
                'admin.uploadfile'
            ];
            $exceptionRoutes = array_merge($defaultExceptionRoutes, $loggingService->getExcludedRouteNames());

            $currentRoute = Request::route()->getName();
            if (auth()->check() &&
                // starts_with($currentRoute, 'backend.') &&
                !in_array($currentRoute, $exceptionRoutes) &&
                !starts_with($currentRoute, 'backend.userlog') &&
                !ends_with($currentRoute, '.importprogress') &&
                !str_contains($currentRoute, 'json')) {
                try {
                    // safe operation / non-blocking-middleware-when-error
                    $paramRequest = Request::all();
                    if (isset($paramRequest['_token'])) unset($paramRequest['_token']);
                    $loggingService->create([
                        'user_id' => auth()->user()->id,
                        'type' => (Request::isMethod('post') ? 'post' : 'get'),
                        'url' => Request::fullUrl(),
                        'request' => json_encode($paramRequest)
                    ]);
                } catch (Exception $e) { }
            }
        }
        // continue to next request
        return $next($request);
    }
}
