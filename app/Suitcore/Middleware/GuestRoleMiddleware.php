<?php

namespace Suitcore\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GuestRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (session()->has('redirectTo')) {
                return redirect()->to(session()->pull('redirectTo'));
            }
            
            // identify users
            $user = Auth::guard($guard)->user();
            if ($user->isActive()) {
                // active user
                $roleCodes = array_keys( $user->getRoleOptions() );
                if (in_array($user->role, $roleCodes)) {
                    return redirect()->intended(route('backend.home.index'));
                }
                return redirect()->intended(route('backend.sessions.login'));
            }

            return redirect()->intended(route('backend.sessions.login'));
        }

        return $next($request);
    }
}
