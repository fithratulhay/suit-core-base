<?php

namespace Suitcore\Middleware;

use Route;
use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // set languages if requested, based on request and saved session
        $defaultLocale = env('APP_DEFAULT_LANG', config('app.locale'));
        $localeOptions = explode(',', env('APP_MULTI_LOCALE_OPTIONS', $defaultLocale));
        if (request()->has('locale')) {
            $locale = request()->get('locale');
            if (in_array($locale, $localeOptions)) {
                $request->session()->put('locale', $locale);
            }
        }
        if ($request->session()->has('locale')) {
            $locale = $request->session()->get('locale');
            if (in_array($locale, $localeOptions)) {
                app()->setLocale($locale); 
            }
        } else {
            if (in_array($defaultLocale, $localeOptions)) {
                app()->setLocale($defaultLocale); 
            }
        }
        // send request to next process/middleware
        return $next($request);
    }
}
