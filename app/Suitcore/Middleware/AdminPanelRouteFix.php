<?php

namespace Suitcore\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use UserRole;

class AdminPanelRouteFix
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (config('suitcore.backend_admin_subpath_access') == 'subdomain') {
            // subdomain then all subpath redirected to related domain
            $regex = '/'.str_replace('/', '\/', $request->root()).'\/(' . implode('|', UserRole::getAllSubpath()) . ')/';
            preg_match($regex, $request->url(), $matches);
            $subpath = null;
            if ($matches && is_array($matches) && count($matches) > 1) {
                $subpath = $matches[1];
            }
            if (!empty($subpath)) {
                return redirect($request->getScheme() . '://' . $subpath . '.' . config('suitcore.base_domain'));
            }
        } else {
            // subpath then all subdomain redirected to related path
            $subdomain = str_replace('.' . config('suitcore.base_domain'), '', $request->getHost());
            if (!empty($subdomain) && in_array($subdomain, UserRole::getAllSubpath())) {
                return redirect($request->getScheme() . '://' . config('suitcore.base_domain') . '/' . $subdomain);
            }
        }
        // else follow normal flow
        return $next($request);
    }
}
