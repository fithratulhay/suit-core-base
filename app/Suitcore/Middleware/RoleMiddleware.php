<?php

namespace Suitcore\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Suitcore\Models\SuitUser;
use Suitcore\Repositories\Facades\SuitUserRole;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::guard($guard)->user();
        
        // No-User redirect to root-path
        if (!$user) {
            // return redirect(url('/'));
            return redirect()->route('backend.sessions.login');
        }

        // Default Admin continue request or redirectTo if any
        if ($user->role == SuitUser::ADMIN) {
            if ($request->session()->has('redirectTo')) {
                return redirect()->to($request->session()->pull('redirectTo'));
            }
            return $next($request);
        }

        $roleCodes = array_keys( (new SuitUser())->getRoleOptions() );
        if (in_array($user->role, $roleCodes)) {
            $currentSubpath = SuitUserRole::getCurrentRolePath($request);
            $currentRole = SuitUserRole::getBy__cached('code', $user->role)->first();
            if ($currentRole && !empty($currentRole->subpath)) {
                if ($currentSubpath != $currentRole->subpath) {
                    return redirect(str_replace($currentSubpath, $currentRole->subpath, strtolower($request->url())));
                }
            }
        } else {
            // Unknown-User-Role redirect to root-path
            // return redirect(url('/'));
            auth()->logout();
            return redirect()->route('backend.sessions.login');
        }

        // Other Backend Panel Roles continue request or redirectTo if any
        if ($request->session()->has('redirectTo')) {
            return redirect()->to($request->session()->pull('redirectTo'));
        }
        return $next($request);
    }
}
