<?php

namespace Suitcore\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Suitsite\Models\SuitUser;
use Suitsite\Models\SuitUserSetting;
use Suitsite\Models\SuitUserToken;

class ApiTokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token     = $request->get('token');
        $email     = $request->get('token_email');
        $userToken = SuitUserToken::where('token', '=', $token)->with('user')->first();

        if ($userToken &&
            $userToken->user &&
            $userToken->user->status == SuitUser::STATUS_ACTIVE &&
            strtolower($userToken->user->email) == strtolower($email)) {
            // authenticate user
            Auth::onceUsingId($userToken->user_id);
            // language setting if any
            $key = 'app_language';
            $user_id = $userToken->user_id;
            $value = app()->getLocale();
            $currentLang = Cache::remember('app_language.'.$user_id, 60, function() use ($user_id) {
                return getUserSettings('app_language', null, $user_id);
            });
            if ($currentLang != $value) {
                $userLang = SuitUserSetting::firstOrNew(compact('key', 'user_id'));
                $userLang->fill(compact('value'))->save();
                Cache::forget('app_language.'.$user_id);
            }
            // continue
            return $next($request);
        }

        return $this->toJson(40, [
            'message' => 'Unauthenticated access!',
            'result' => null
        ]);
    }

    private function toJson($status, array $data = []) {
        $data = array_merge(['status' => (int) $status], $data);
        $data['result'] = array_key_exists('result', $data) ? $data['result'] : null;
        return response()->json($data);
    }
}
