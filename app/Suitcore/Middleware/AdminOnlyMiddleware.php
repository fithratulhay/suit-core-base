<?php

namespace Suitcore\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Suitcore\Models\SuitUser;

class AdminOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::guard($guard)->user(); 

        if ($user->role != SuitUser::ADMIN) {
            return redirect(route('frontend.home'));
        }

        return $next($request);
    }
}
