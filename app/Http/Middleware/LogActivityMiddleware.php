<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Suitsiteextra\Jobs\SaveLogActivity;

class LogActivityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logParam['user_id'] = auth()->user() ? auth()->user()->id : null;
        $logParam['url'] = $request->path();
        $logParam['session_id'] = session()->getId();
        $logParam['created_at'] = Carbon::now();
        SaveLogActivity::dispatch($logParam)->onQueue(env('QUEUE_LOGACTIVITY_NAME', 'suitcore-logactivity-job'));

        if (!auth()->check()) {
            if ($request->server('AUTH_USER') && $user = User::where('username', $request->server('AUTH_USER'))->first()) {
                auth()->login($user);
            } else {
                return redirect()->route('login');
            }
        }

        return $next($request);
    }
}
