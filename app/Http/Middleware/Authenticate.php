<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                session()->put('danger', 'Anda harus terdaftar sebagai member. Silakan login atau Buat Akun Baru');
                session()->put('redirectTo', $request->url());
                return redirect()->route('sessions.login');
            }
        }

        return $next($request);
    }
}
