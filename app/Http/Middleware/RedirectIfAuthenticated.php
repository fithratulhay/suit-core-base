<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Suitcore\Repositories\Facades\SuitUserRole;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (session()->has('redirectTo')) {
                return redirect()->to(session()->pull('redirectTo'));
            }
            
            // identify users
            $user = Auth::guard($guard)->user();
            if ($user->isActive()) {
                // active user
                $roleCodes = array_keys( $user->getRoleOptions() );
                if (in_array($user->role, $roleCodes)) {
                    $currentRole = SuitUserRole::getBy__cached('code', $user->role)->first();
                    if ($currentRole && !empty($currentRole->subpath)) {
                        return redirect('/' . $currentRole->subpath);
                    }
                }
                return redirect()->intended(route('frontend.home'));
            }

            return redirect()->intended(route('frontend.home'));
        }

        return $next($request);
    }
}
