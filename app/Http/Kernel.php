<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \Suitcore\Middleware\AdminPanelRouteFix::class,
        \Spatie\MissingPageRedirector\RedirectsMissingPages::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Suitcore\Middleware\Localization::class
        ],

        'web_auth' => [
            \App\Http\Middleware\Authenticate::class,
        ],

        'suitcore_guest' => [
            \Suitcore\Middleware\GuestRoleMiddleware::class
        ],

        'suitcore_roles' => [
            \Suitcore\Middleware\AdminPanelAuthenticate::class,
            \Suitcore\Middleware\RoleMiddleware::class,
            \Suitcore\Middleware\UserLogging::class,
        ],

        'admin' => [
            \App\Http\Middleware\Authenticate::class,
            \Suitcore\Middleware\AdminOnlyMiddleware::class
        ],

        'authenticatedapi' => [
            \Suitcore\Middleware\ApiTokenAuth::class,
            // \Suitcore\Middleware\UserOnlyApiMiddleware::class,
            // \Suitcore\Middleware\Localization::class,
            'throttle:60,1',
        ],

        'api' => [
            \Suitcore\Middleware\TemporaryLocalization::class,
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'varnishcacheable' => \Spatie\Varnish\Middleware\CacheWithVarnish::class,
        'log.activity' => \App\Http\Middleware\LogActivityMiddleware::class,
    ];
}
