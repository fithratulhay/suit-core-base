<?php

namespace App\Http\Controllers\Backend;

use Suitsite\Repositories\Contract\MenuRepositoryContract;
use Suitsite\Models\Menu;
use View;

class MenuController extends BaseController
{
    /**
     * Override Default Constructor
     * @param  MenuRepositoryContract $baseRepo
     * @return void
     */
    public function __construct(MenuRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.menu";
        $this->routeDefaultIndex = "backend.menu.index";
        $this->viewBaseClosure = "backend.menus";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'E8';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
