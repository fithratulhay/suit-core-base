<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitconversation\Repositories\Contract\ConversationMemberRepositoryContract;

class ConversationMemberController extends BaseController
{
    public function __construct(ConversationMemberRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.conversation-member";
        $this->routeDefaultIndex = "backend.conversation-member.index";
        $this->viewBaseClosure = "backend.conversationmembers";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'C8';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
