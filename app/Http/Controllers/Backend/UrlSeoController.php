<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Suitcore\Models\SuitUrlSeo;
use Suitcore\Repositories\Contract\SuitUrlSeoRepositoryContract;

class UrlSeoController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param SuitUrlSeoRepositoryContract $_baseRepo
     */
    public function __construct(SuitUrlSeoRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.urlseo';
        $this->viewBaseClosure = 'backend.urlseos';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E10');
        view()->share('routeBaseName', $this->routeBaseName);
        view()->share('routeDefaultIndex', $this->routeDefaultIndex);
        view()->share('viewBaseClosure', $this->viewBaseClosure);
    }
}
