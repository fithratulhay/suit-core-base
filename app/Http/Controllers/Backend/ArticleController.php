<?php

namespace App\Http\Controllers\Backend;

use Input;
use Redirect;
use View;
use Route;
use Suitsite\Repositories\Contract\ArticleRepositoryContract;
use Suitsite\Models\Article;

class ArticleController extends BaseController
{
    /**
     * Override Default Constructor
     * @param  ArticleRepositoryContract $baseRepo
     * @return void
     */
    public function __construct(ArticleRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.article";
        $this->routeDefaultIndex = "backend.article.index";
        $this->viewBaseClosure = "backend.articles";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'E20';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    /**
     * Return json list of contentType
     * @param  
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postIndexJson() {
        // Parameter
        $baseObj = $this->baseModel;
        $param = Input::all();
        // Filter Parameter
        $specificFilter = [];
        if (isset($param['user_id'])) $specificFilter['user_id'] = $param['user_id'];
        // Return
        $customRender = [
            
        ];

        $selectedIds = session()->get(class_basename($baseObj) . '_selected_ids', []);
        if (Route::has($this->routeBaseName . '.select')) {
            $selectionSetting = [
                'session_token' => csrf_token(),
                'url_selection' => (Route::has($this->routeBaseName . '.select') ? route($this->routeBaseName . '.select',["id"=>"#id#"]) : '')
            ];
            $renderedSelection = View::make(self::$partialView[self::TABLE_SELECTION], ['selectionSetting' => $selectionSetting])->render();
            $customRender['selection'] = $renderedSelection;
            $customRender['selectedIds'] = $selectedIds;
        }

        $menuSetting = [
            'session_token' => csrf_token(),
            'url_detail' => (Route::has($this->routeBaseName . '.show') ? route($this->routeBaseName . '.show',["id"=>"#id#"]) : ''),
            'url_edit' => (Route::has($this->routeBaseName . '.edit') ? route($this->routeBaseName . '.edit',["id"=>"#id#"]) : ''),
            'url_delete' => (Route::has($this->routeBaseName . '.destroy') ? route($this->routeBaseName . '.destroy', ['id' => "#id#"]) : ''),
        ];

        $renderedMenu = View::make(self::$partialView[self::TABLE_MENU], ['menuSetting' => $menuSetting])->render();
        $customRender['menu'] = $renderedMenu;
        return $this->baseRepository->jsonDatatable(
            $param, 
            $customRender, 
            $specificFilter,
            null,
            null,
            null,
            Route::has($this->routeBaseName . '.select'));
    }
}
