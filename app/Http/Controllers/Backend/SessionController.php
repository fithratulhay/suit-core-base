<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Exception;
use FileGrab;
use Redirect;
use Session;
use View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Controller;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitcore\Repositories\Facades\SuitUserRole;
use Suitcore\Models\SuitUser;

class SessionController extends Controller
{
    use ValidatesRequests;

    public function __construct(SuitUserRepositoryContract $userRepo)
    {
        // any defined if needed   
    }

    public function getLogin(Request $request)
    {
        return view('backend.session.create');
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'email' => 'required_without:username|email',
            'username' => 'required_without:email',
            'password' => 'required'
        ];
        if (settings('enable_paneladmin_login_captcha', null)) {
            $rules['captcha'] = 'required|captcha';
        }

        try {
            $this->validate($request, $rules);
        } catch (ValidationException $e) {
            session()->put('danger', 'You should provide valid authentication data and captcha!');
            return redirect()->back()->withInput($request->except('password'));
        }

        $credentials = [($request->has('email') ? 'email' : 'username'), 'password'];

        if (!auth()->attempt($request->only($credentials))) {
            session()->put('danger', 'Email / Password Anda salah.');
            return redirect()->back()->withInput($request->except('password'));
        }

        $user = auth()->user();
        if ($user && $user->status != SuitUser::STATUS_ACTIVE) {
            auth()->logout();
            session()->put('danger', 'Your account not activated yet!');
            return redirect()->back()->withInput($request->except('password'));
        }
        $user->updateLastvisit();
        $currentRole = SuitUserRole::getBy__cached('code', $user->role)->first();
        if ($request->has('_redirect')) {
            $red = $request->get('_redirect') != str_contains($request->get('_redirect'), 'login') ? $request->get('_redirect') : '/' . $currentRole->subpath;
            return redirect($red)->with('success', 'Anda telah login');
        }

        return redirect()->route('backend.home.index');
    }

    public function getLogout(Request $request)
    {
        $successUpdate = Auth::user()->updateLastvisit();
        if ($successUpdate) {
            Auth::logout();
        }
        // dd('dd');
        return redirect()->route('frontend.home');
    }
}
