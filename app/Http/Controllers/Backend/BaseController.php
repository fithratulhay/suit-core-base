<?php

namespace App\Http\Controllers\Backend;

use Suitcore\Config\DefaultConfig;
use Suitcore\Repositories\Contract\SuitRepositoryContract;

/**
 * Extend from SuitCore Backend Controller.
 **/
class BaseController extends \Suitcore\Controllers\BackendController
{
    protected $pageIcon;

    // ACTION
    /**
     * Default Constructor
     * @param  SuitRepositoryContract $_baseRepo
     * @return void
     */
    public function __construct(SuitRepositoryContract $baseRepo, $_topLevelRelationModelString = null)
    {
        parent::__construct($baseRepo, $_topLevelRelationModelString);
    }

    /**
     * Set ID Page
     * Set Icon Page
     *
     * @param string $pageId
     */
    protected function setID($pageId)
    {
        $this->pageId = $pageId;
        $this->pageIcon = $this->getPageIcon();
        view()->share('pageId', $this->pageId);
        view()->share('pageIcon', $this->pageIcon);
    }

    /**
     * Get Icon Page from Base Config
     *
     * @return string
     */
    private function getPageIcon()
    {
        if (strlen($this->pageId) > 0)
        {
            if (strlen($this->pageId) > 1)
                if (isset(DefaultConfig::getConfig()['backendNavigation'][$this->pageId])) {
                    return DefaultConfig::getConfig()['backendNavigation'][$this->pageId]['icon'];
                }
                return array_key_exists($this->pageId, DefaultConfig::getConfig()['backendNavigation'][(string)$this->pageId[0]]['submenu'])
                    ? DefaultConfig::getConfig()['backendNavigation'][$this->pageId[0]]['submenu'][$this->pageId]['icon'] :'';

            return array_key_exists($this->pageId, DefaultConfig::getConfig()['backendNavigation'])
                ? DefaultConfig::getConfig()['backendNavigation'][$this->pageId]['icon'] : '';
        }
    }
}
