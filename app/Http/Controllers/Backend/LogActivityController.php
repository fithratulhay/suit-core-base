<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Cache;
use Carbon\Carbon;
use DB;
use Request;
use Response;
use Suitcore\Models\SuitModel;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitsiteextra\Models\LogActivity;
use View;

class LogActivityController extends BaseController
{
    // METHODS
    /**
     * Override Default Constructor.
     */
    public function __construct(SuitRepositoryContract $baseRepo)
    {
        parent::__construct($baseRepo);
        // page ID
        $this->setID('A3');
        View::share('title', 'Dashboard');
        View::share('pageTitle', 'Dashboard');
    }

    private function getDateRangeParameters($request) {
        $date_from_monthly = Carbon::now()->subYear(1)->startOfDay();
        $date_from_weekly = Carbon::now()->subYear(1)->startOfDay();
        $date_from_daily = Carbon::now()->subMonth(1)->startOfDay();
        try {
            if ($request->has('date_from_monthly')) {
                $date_from_monthly = Carbon::createFromFormat('Y-m-d', $request->get('date_from_monthly'))->startOfDay();
            }
            if ($request->has('date_from_weekly')) {
                $date_from_weekly = Carbon::createFromFormat('Y-m-d', $request->get('date_from_weekly'))->startOfDay();
            }
            if ($request->has('date_from_daily')) {
                $date_from_daily = Carbon::createFromFormat('Y-m-d', $request->get('date_from_daily'))->startOfDay();
            }
        } catch (Exception $e) { }
        $date_to = Carbon::now()->endOfDay();
        try {
            if ($request->has('date_to')) {
                $date_to = Carbon::createFromFormat('Y-m-d', $request->get('date_to'))->endOfDay();
            }
        } catch (Exception $e) { }
        $databaseFormatFrom = $date_from_monthly->toDateTimeString();
        $databaseFormatTo = $date_to->toDateTimeString();
        return [$date_from_monthly, $date_from_weekly, $date_from_daily, $date_to, $databaseFormatFrom, $databaseFormatTo];
    }

    public function getIndex()
    {
        list($date_from_monthly, $date_from_weekly, $date_from_daily, $date_to, $databaseFormatFrom, $databaseFormatTo) = $this->getDateRangeParameters(request());

        $sessions = Cache::remember('sessions-activity' . $date_from_monthly->format('d_m_Y') . '_' . $date_to->format('d_m_Y'), 1, function() use($databaseFormatFrom, $databaseFormatTo) {
            return LogActivity::select(['session_id', 'created_at'])->where('created_at', '>=', $databaseFormatFrom)->where('created_at', '<=', $databaseFormatTo)->get();
        });

        $sessionGroups = $sessions->groupBy('session_id');
        $sessionCount = $sessionGroups->count();
        
        $diffTime = 0;
        foreach ($sessionGroups as $obj) {
            $max = Carbon::parse($obj->max('created_at'));
            $min = Carbon::parse($obj->min('created_at'));
            $diffTime += $max->diffInSeconds($min);
        }

        $sessionAvg = $sessionCount > 0 && count($sessionGroups) > 0 ? (int)round($diffTime / $sessionCount) : 0;
        // RENDER
        return View::make('backend.activity.index', compact('date_from_monthly', 'date_from_weekly', 'date_from_daily', 'date_to', 'sessionAvg', 'sessionCount'));
    }

    public function pattern()
    {
        return view('backend.pattern');
    }

    // public function getAppUserSummaryJson(Request $request)
    // {
    //     list($date_from, $date_to, $databaseFormatFrom, $databaseFormatTo) = $this->getDateRangeParameters(request());
    //     $completeSeries = $this->initDateRangeSet($date_from, $date_to, 4);

    //     $appActiveUsers = User::where('created_at', '>=', $databaseFormatFrom)->where('created_at', '<=', $databaseFormatTo)->where('status', User::STATUS_ACTIVE)->selectRaw("DATE_FORMAT(created_at,'%Y-%m-%d') AS date, count(users.id) AS nbObj")->orderBy('created_at', 'asc')->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->get();
    //     foreach ($appActiveUsers as $key => $object) {
    //         $completeSeries[0][$object->date] = intval($object->nbObj);
    //     }

    //     $appInactiveUsers = User::where('created_at', '>=', $databaseFormatFrom)->where('created_at', '<=', $databaseFormatTo)->where('status', User::STATUS_INACTIVE)->selectRaw("DATE_FORMAT(created_at,'%Y-%m-%d') AS date, count(users.id) AS nbObj")->orderBy('created_at', 'asc')->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->get();
    //     foreach ($appInactiveUsers as $key => $object) {
    //         $completeSeries[1][$object->date] = intval($object->nbObj);
    //     }

    //     $closedUsers = User::where('created_at', '>=', $databaseFormatFrom)->where('created_at', '<=', $databaseFormatTo)->where('status', User::STATUS_CLOSED)->selectRaw("DATE_FORMAT(created_at,'%Y-%m-%d') AS date, count(users.id) AS nbObj")->orderBy('created_at', 'asc')->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->get();
    //     foreach ($closedUsers as $key => $object) {
    //         $completeSeries[2][$object->date] = intval($object->nbObj);
    //     }

    //     $appBannedUsers = User::where('created_at', '>=', $databaseFormatFrom)->where('created_at', '<=', $databaseFormatTo)->where('status', User::STATUS_BANNED)->selectRaw("DATE_FORMAT(created_at,'%Y-%m-%d') AS date, count(users.id) AS nbObj")->orderBy('created_at', 'asc')->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->get();
    //     foreach ($appBannedUsers as $key => $object) {
    //         $completeSeries[3][$object->date] = intval($object->nbObj);
    //     }

    //     $categories = array_keys($completeSeries[0]);
    //     $series1 = array_values($completeSeries[0]);
    //     $series2 = array_values($completeSeries[1]);
    //     $series3 = array_values($completeSeries[2]);
    //     $series4 = array_values($completeSeries[3]);
    //     $title = "User Data " . $date_from->format('d F Y') . " - " . $date_to->format('d F Y');
    //     $data = [
    //         'title' => $title,
    //         'xAxis' => [
    //             'categories' => $categories,
    //             'title' => [
    //                 'text' => 'Date'
    //             ],
    //         ],
    //         'yAxisTitle' => 'New User',
    //         'series' => [
    //             [
    //                 'name' => 'Active User',
    //                 'data' => $series1,
    //             ],
    //             [
    //                 'name' => 'Inactive User',
    //                 'data' => $series2,
    //             ],
    //             [
    //                 'name' => 'Closed User',
    //                 'data' => $series3,
    //             ],
    //             [
    //                 'name' => 'Banner User',
    //                 'data' => $series4,
    //             ]
    //         ],
    //     ];
    //     return $data;
    // }

    public function getAppUserSummaryJson(Request $request)
    {
        list($date_from_monthly, $date_from_weekly, $date_from_daily, $date_to, $databaseFormatFrom, $databaseFormatTo) = $this->getDateRangeParameters(request());

        $type = 'monthly';
        $dataType = null;
        $textTitle = "Month";
        $yAxisTitle = 'Number of Average User';
        $title = "User Data " . $date_from_monthly->format('d F Y') . " - " . $date_to->format('d F Y');
        if (request()->has('filter-type')) {
            $type = request()->get('filter-type');
        }

        if (request()->has('data-type')) {
            $dataType = request()->get('data-type');
            $yAxisTitle = 'Number of Average Session (s)';
        }

        $completeSeries = $this->initDateRangeSet($date_from_monthly, $date_to, 2, $type);
        $logActivities = [];
        $logActivitySessions = [];
        $activities = Cache::remember('data_monthly' . $databaseFormatFrom . $databaseFormatTo, 1 , function() use($databaseFormatFrom, $databaseFormatTo) {
            return LogActivity::where('created_at', '>=', $databaseFormatFrom)
            ->where('created_at', '<=', $databaseFormatTo)
            ->select(['session_id', 'created_at'])
            ->orderBy('created_at', 'asc')
            ->get();
        });

        if ($type == 'monthly') {
            $logActivities = $activities->groupBy( function($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
            if ($dataType == 'monthly-session') {
                $diffTime = 0;
                foreach ($logActivities as $key => $object) {
                    $y = Carbon::parse($object->first()->date)->format('y');
                    $sessions = $object->groupBy('session_id');
                    $max = 0;
                    $min = 0;
                    $sessionCount = 0;
                    foreach ($sessions as $session) {
                        $max = Carbon::parse($session->max('created_at'));
                        $min = Carbon::parse($session->min('created_at'));
                        $diffTime += $max->diffInSeconds($min);
                        $sessionCount++;
                    }

                    $count = $sessionCount > 0 && $diffTime > 0 ? (int)round($diffTime / $sessionCount) : 0;
                    $arrKey = $key . "/" . $y;
                    $completeSeries[0][$arrKey] = $count;
                }
            } else {
                foreach ($logActivities as $key => $object) {
                    $y = Carbon::parse($object->first()->date)->format('y');
                    $count = $object->groupBy('session_id')->count();
                    $arrKey = $key . "/" . $y;
                    $completeSeries[0][$arrKey] = $count;
                }
            }
        } elseif ($type == 'weekly') {
            $textTitle = "Week";
            $title = "User Data " . $date_from_weekly->format('d F Y') . " - " . $date_to->format('d F Y');
            $logActivities = $activities->groupBy( function($date) {
                    return Carbon::parse($date->created_at)->format('W');
                });

            if ($dataType == 'weekly-session') {
                $diffTime = 0;
                foreach ($logActivities as $key => $object) {
                    $y = Carbon::parse($object->first()->date)->format('y');
                    $sessions = $object->groupBy('session_id');
                    $max = 0;
                    $min = 0;
                    $sessionCount = 0;

                    foreach ($sessions as $session) {
                        $max = Carbon::parse($session->max('created_at'));
                        $min = Carbon::parse($session->min('created_at'));
                        $diffTime += $max->diffInSeconds($min);
                        $sessionCount++;
                    }

                    $count = $sessionCount > 0 && $diffTime > 0 ? (int)round($diffTime / $sessionCount) : 0;
                    $arrKey = $key . "/" . $y;
                    $completeSeries[0][$arrKey] = $count;
                }
            } else {
                foreach ($logActivities as $key => $object) {
                    $y = Carbon::parse($object->first()->date)->format('y');
                    $arrKey = $key . '/' . $y;
                    $count = $object->groupBy('session_id')->count();
                    $completeSeries[0][$arrKey] = $count;
                }
            }
        } elseif ($type == 'daily') {
            $textTitle = "Date";
            $title = "User Data " . $date_from_daily->format('d F Y') . " - " . $date_to->format('d F Y');
            $logActivities = $activities->groupBy( function($date) {
                    return Carbon::parse($date->created_at)->format('Y-m-d');
                });

            if ($dataType == 'daily-session') {
                $diffTime = 0;
                foreach ($logActivities as $key => $object) {
                    $sessions = $object->groupBy('session_id');
                    $max = 0;
                    $min = 0;
                    $sessionCount = 0;

                    foreach ($sessions as $session) {
                        $max = Carbon::parse($session->max('created_at'));
                        $min = Carbon::parse($session->min('created_at'));
                        $diffTime += $max->diffInSeconds($min);
                        $sessionCount++;
                    }

                    $count = $sessionCount > 0 && $diffTime > 0 ? (int)round($diffTime / $sessionCount) : 0;
                    $completeSeries[0][$key] = $count;
                }
            } else {
                foreach ($logActivities as $key => $object) {
                    $count = $object->groupBy('session_id')->count();
                    $completeSeries[0][$key] = $count;
                }
            }
        }
        $categories = array_keys($completeSeries[0]);
        $series1 = array_values($completeSeries[0]);

        $data = [
            'title' => $title,
            'xAxis' => [
                'categories' => $categories,
                'title' => [
                    'text' => $textTitle
                ],
            ],
            'yAxisTitle' => $yAxisTitle,
            'series' => [
                [
                    'name' => 'User Activity',
                    'data' => $series1,
                ]
            ],
        ];
        return $data;
    }

    private function initDateRangeSet(Carbon $start_date, Carbon $end_date, $nbSub, $type)
    {
        $series = [];
        for($i = 0; $i < $nbSub; $i++) {
            $series[$i] = [];
        }
        for($date = clone $start_date; $date->lte($end_date); $date->addDay()) {
            for($i = 0; $i < $nbSub; $i++) {
                $arrKey = $date->format('Y-m-d');
                $y = Carbon::parse($date)->format('y');
                if ($type == 'monthly') {
                    $m = Carbon::parse($date)->format('m');
                    $arrKey = $m . "/" . $y;
                } elseif ($type == 'weekly') {
                    $week = Carbon::parse($date)->format('W');
                    $arrKey = $week . '/' . $y;
                }
                $series[$i][$arrKey] = 0;
            }
        }

        return $series;
    }
}