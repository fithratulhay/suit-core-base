<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\FaqRepositoryContract;
use Suitsite\Models\Faq;

class FaqController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param FaqRepositoryContract $_baseRepo
     */
    public function __construct(FaqRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.faq';
        $this->viewBaseClosure = 'backend.faqs';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E6');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
