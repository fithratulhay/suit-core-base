<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsiteextra\Repositories\Contract\PollOptionRepositoryContract;

class PollOptionController extends BaseController
{
    public function __construct(PollOptionRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.poll-option";
        $this->routeDefaultIndex = "backend.poll-option.index";
        $this->viewBaseClosure = "backend.polloptions";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'G2';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
