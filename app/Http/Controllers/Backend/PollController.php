<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsiteextra\Repositories\Contract\PollRepositoryContract;

class PollController extends BaseController
{
    public function __construct(PollRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.poll";
        $this->routeDefaultIndex = "backend.poll.index";
        $this->viewBaseClosure = "backend.polls";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'G1';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
