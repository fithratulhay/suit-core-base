<?php

namespace App\Http\Controllers\Backend;

use App;
use datetime as datetime;
use Hash;
use Input;
use Route;
use Redirect;
use View;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use App\Models\User;

class UserController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param SuitUserRepositoryContract $_baseRepo
     */
    public function __construct(SuitUserRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.user';
        $this->routeDefaultIndex = 'backend.user.index';
        $this->viewBaseClosure = 'backend.users';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('C1');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    /**
     * Display baseModel detail
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function getView($id)
    {
        // Fetch
        view()->share('title', 'Detail');
        $fetchedData = $this->baseRepository->get($id);
        // Related
        $userProfile = null;
        // Return
        $baseObj = $fetchedData['object'];
        return view($this->viewBaseClosure . '.show')->with($this->viewInstanceName, $baseObj)
                ->with('userProfile',$userProfile);
    }

    public function getShowprofile($id)
    {
        $user = User::find($id);
        $orders = Order::where('user_id', $id)->get();
        $reviews = Review::where('user_id', $id)->get();

        return View::make('backend.users.profile', array('user' => $user, 'orders' => $orders, 'reviews' => $reviews));
    }

    public function postCreate()
    {
        // Save
        $param = Input::all();
        $baseObj = $this->baseModel;
        if (isset($param['password']) &&
            !empty($param['password']) &&
            isset($param['password_confirmation']) &&
            !empty($param['password_confirmation']) &&
            ($param['password'] == $param['password_confirmation'])) {
            $param['password'] = Hash::make($param['password']);
        } else {
            if (isset($param['password'])) unset($param['password']);
        }
        $date = new datetime();
        $param['registration_date'] = $date;
        $param['last_visit'] = $date;
        // $param['status'] = User::STATUS_INACTIVE;
        $result = $this->baseRepository->create($param, $baseObj);
        // Return
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->getLabel().' Created', 'New '.$baseObj->getLabel().' data had been created!');
            if (Route::has($this->routeBaseName.'.show')) {
                return Redirect::route($this->routeBaseName.'.show', ['id' => $baseObj->id]);
            } else {
                if (!empty($this->routeDefaultIndex)) {
                    return Redirect::route($this->routeDefaultIndex);
                }

                return Redirect::route($this->routeBaseName.'.index');
            }
        }

        return Redirect::route($this->routeBaseName.'.create')->with('errors', $baseObj->errors)->withInput($param);
    }

    public function postUpdate($id)
    {
        // Save
        $param = Input::all();
        $baseObj = $this->baseModel->find($id);
        if ($baseObj) {
            if (isset($param['password']) &&
                !empty($param['password']) &&
                isset($param['password_confirmation']) &&
                !empty($param['password_confirmation']) &&
                ($param['password'] == $param['password_confirmation'])) {
                $param['password'] = Hash::make($param['password']);
            } else {
                $param['password'] = $baseObj->password;
            }
        }
        $result = $this->baseRepository->update($id, $param, $baseObj);
        // Return
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->getLabel().' Updated', $baseObj->getLabel().' data had been updated!');
            if (Route::has($this->routeBaseName.'.show')) {
                return Redirect::route($this->routeBaseName.'.show', ['id' => $id]);
            } else {
                if (!empty($this->routeDefaultIndex)) {
                    return Redirect::route($this->routeDefaultIndex);
                }

                return Redirect::route($this->routeBaseName.'.index');
            }
        }
        if ($baseObj == null) {
            App::abort(404);
        }

        return Redirect::route($this->routeBaseName.'.update', ['id' => $id])->with('errors', $baseObj->errors)->withInput($param);
    }
}
