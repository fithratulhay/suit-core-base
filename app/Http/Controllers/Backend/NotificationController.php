<?php

namespace App\Http\Controllers\Backend;

use View, Redirect, Auth, App;
use Illuminate\Http\Request;
use Suitcore\Models\SuitNotification;
use Suitcore\Repositories\Contract\SuitNotificationRepositoryContract;

class NotificationController extends BaseController
{
    /**
     * Override Default Constructor
     * @param  SuitNotificationRepositoryContract $_baseRepo
     * @return void
     */
    public function __construct(SuitNotificationRepositoryContract $_baseRepo){
        parent::__construct($_baseRepo);
        $this->routeBaseName = "backend.notification";
        $this->routeDefaultIndex = "backend.notification.index";
        $this->viewBaseClosure = "backend.notifications";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'A2';
        View::share('pageId', $this->pageId);
        View::share('pageTitle', 'Notification');
        View::share('pageIcon', 'fa fa-bell-o');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    /**
     * Show list of notification from certain users
     *
     * @return View of table that contains notification item list
     **/
    public function getList()
    {
        $user = auth()->user();
        if (!$user) abort(404);
        $input = request()->all();
        $param = [];
        $param['_user_id'] = $user->id;
        $param['orderBy'] = (isset($input['orderBy']) ? $input['orderBy'] : 'created_at');
        $param['orderType'] = (isset($input['orderType']) ? $input['orderType'] : 'desc');
        $param['paginate'] = true;
        $param['perPage'] = (isset($input['perPage']) ? $input['perPage'] : 15);
        $currentSetting = 'allnewest';
        if ($param['orderType'] == 'asc') $currentSetting = 'alloldest';
        if (isset($input['is_read'])) {
            $param['is_read'] = intval($input['is_read']);
            if ($param['is_read'] == 0) {
                if ($param['orderType'] == 'desc') $currentSetting = 'unreadnewest';
                else $currentSetting = 'unreadoldest';
            }
        }
        // render
        $notifications = $this->baseRepository->getByParameter($param);

        return view('backend.notifications.index')->with(['notifications'=>$notifications, 'user'=>$user, 'currentSetting'=>$currentSetting]);
    }

    /**
     * Redirect to notification URL and set state as read
     *
     * @param $id ID of notification item
     * @return Redirect
     **/
    public function getClick($id)
    {
        $notification = $this->baseRepository->get($id)['object'];
        $notification->update(['is_read' => true]);
        return redirect()->to($notification->url ?: route('backend.notification.index'));
    }

    /**
     * Remove an item from notification list
     *
     * @param $id ID of notification item
     * @return Redirect
     **/
    public function deleteRemove($id){
        $user = auth()->user();
        if ($user == null) abort(404);
        $notification = $this->baseRepository->find($id);
        if ($notification && $notification->user_id == $user->id) {
            // only authorized user can delete a notification item
            $notification->delete();
        }
        return redirect(route('backend.notification.index'));
    }

    /**
     * Apply an action to multiple selection notification
     *
     * @param $request HTTP Request
     * @return Redirect
     **/
    public function postAction(Request $request)
    {
        if (isset($request->id)) {
            $user = auth()->user();
            $param['_user_id'] = $user->id;
            $param['id'] = implode(',', $request->get('id'));
            $notifications = $this->baseRepository->getByParameter($param);
            $ids = $notifications->pluck('id')->toArray();
            if ($request->has('delete') && $request->get('delete', false) == true) {
                $this->baseRepository->deleteAll($ids);
                $this->showNotification(self::NOTICE_NOTIFICATION, 'Notification Action', 'Selected notifications had been removed!');
            } else {
                $this->baseRepository->read($ids);
                $this->showNotification(self::NOTICE_NOTIFICATION, 'Notification Action', 'Selected notifications had been set as read!');
            }
        } else {
            if ($request->has('delete') && $request->get('delete', false) == true) {
                $this->showNotification(self::WARNING_NOTIFICATION, 'Notification Action', 'Check a notification which will be removed!');
            } else {
                $this->showNotification(self::WARNING_NOTIFICATION, 'Notification Action', 'Check a notification which will be set as read');
            }
        }

        return redirect()->back();
    }
}
