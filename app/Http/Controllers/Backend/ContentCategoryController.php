<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\ContentCategoryRepositoryContract;
use Suitsite\Models\ContentCategory;

class ContentCategoryController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param ContentCategoryRepositoryContract $_baseRepo
     */
    public function __construct(ContentCategoryRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.contentcategory';
        $this->viewBaseClosure = 'backend.contentcategories';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E1');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    protected function processRequest($request)
    {
        $slug = $request['slug'] ?: ($request['name'] ?: '');
        $request['slug'] = str_slug($slug);
        return parent::processRequest($request);
    }
}
