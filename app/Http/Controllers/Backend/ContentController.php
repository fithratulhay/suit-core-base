<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\ContentRepositoryContract;
use Suitsite\Models\Content;

class ContentController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param ContentRepositoryContract $_baseRepo
     */
    public function __construct(ContentRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.content';
        $this->viewBaseClosure = 'backend.contents';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E2');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    protected function processRequest($request)
    {
        // $slug = isset( $request['slug'] ) && $request['slug'] ?: ($request['title'] ?: '');
        // $request['slug'] = str_slug($slug);
        return parent::processRequest($request);
    }

    public function getCreate()
    {
        view()->share('hiddenInputs', ['slug']);
        return parent::getCreate();
    }
}
