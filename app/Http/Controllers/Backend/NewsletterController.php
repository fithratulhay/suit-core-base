<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\NewsletterRepositoryContract;
use Suitsite\Models\Newsletter;

class NewsletterController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param NewsletterRepositoryContract $_baseRepo
     */
    public function __construct(NewsletterRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.newsletter';
        $this->viewBaseClosure = 'backend.newsletters';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('B2');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    public function getPreview($id) {
        $newsletter = Newsletter::find($id);
        if (!$newsletter) return app()->abort(404);
        $previewOnly = true;
        return view('emails.newsletter', compact('newsletter', 'previewOnly'));
    }
}
