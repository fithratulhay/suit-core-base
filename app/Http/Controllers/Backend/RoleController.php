<?php

namespace App\Http\Controllers\Backend;

use File;
use Form;
use Input;
use MessageBag;
use Redirect;
use View;
use Suitcore\Models\SuitUserRole;
use Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract;

class RoleController extends BaseController
{
    /**
     * Override Default Constructor
     * @param  SuitUserRoleRepositoryContract $_baseRepo
     * @return void
     */
    public function __construct(SuitUserRoleRepositoryContract $_baseRepo){
        parent::__construct($_baseRepo);
        $this->routeBaseName = "backend.role";
        $this->routeDefaultIndex = "backend.role.index";
        $this->viewBaseClosure = "backend.roles";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'C2';
        View::share('pageId', $this->pageId);
        View::share('pageIcon', 'fa fa-home');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
