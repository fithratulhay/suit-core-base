<?php

namespace App\Http\Controllers\Backend;

use Suitcore\Repositories\Contract\SuitSettingRepositoryContract;
use Exception;
use File;
use Input;
use Redirect;
use Suitcore\Models\SuitModel;
use Suitcore\Models\SuitSetting;
use Suitcore\Repositories\Contract\SuitRepositoryContract;
use View;

class SettingsController extends BaseController
{
    // PROPERTIES
    // Default Services / Repository
    protected $settingsRepo;

    protected $settingItems;

    // ACTION
    /**
     * Default Constructor.
     *
     * @param SuitSettingRepositoryContract $_adsRepo
     */
    public function __construct(SuitSettingRepositoryContract $_settingsRepo, SuitRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->setID('E7');
        $this->settingsRepo = $_settingsRepo;
        $this->settingItems = config("suitapp.setting_items", []);
    }

    public function getList()
    {
        $settings = new SuitSetting();
        $settingItems = $this->settingItems;
        view()->share('title', 'Settings');
        view()->share('pageTitle', 'Settings');
        return View::make('backend.settings.view')->with('settings', $settingItems);
    }

    public function postSaveSettings()
    {
        $settings = Input::get('settings', []);

        // Custom Input File
        $partialPath = '/files/sitesetting/';
        $destinationPath = public_path() . $partialPath;
        if (!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0775, true);
        }
        $fileList = [];
        if ($this->settingItems && 
            is_array($this->settingItems) &&
            count($this->settingItems) > 0) {
            foreach ($this->settingItems as $key => $setting) {
                foreach ($setting['elements'] as $subkey => $subsetting) {
                    if ($subsetting['type'] == SuitModel::TYPE_FILE) {
                        $fileList[] = $subkey;
                    }
                }
            }
        }
        $fileList = collect($fileList);
        foreach ($fileList as $fileObj) {
            try {
                if ($file = Input::file($fileObj)) {
                    $fileName =  $fileObj . '.' . $file->getClientOriginalExtension();
                    $result = $file->move($destinationPath, $fileName);
                    if ($result) {
                        $settings[$fileObj] = url($partialPath . $fileName);
                    }
                }
            } catch (Exception $e) { }
        }

        // Saving
        $this->settingsRepo->save($settings, $fileList->toArray());

        // Redirect
        return Redirect::route('backend.settings.view');
    }
}
