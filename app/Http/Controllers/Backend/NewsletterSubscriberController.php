<?php

namespace App\Http\Controllers\Backend;

use Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract;
use Suitsite\Models\NewsletterSubscriber;
use View;

class NewsletterSubscriberController extends BaseController
{
    /**
     * Override Default Constructor
     * @param  NewsletterSubscriberRepositoryContract $baseRepo
     * @return void
     */
    public function __construct(NewsletterSubscriberRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.newslettersubscribers";
        $this->routeDefaultIndex = "backend.newslettersubscribers.index";
        $this->viewBaseClosure = "backend.newslettersubscribers";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'B1';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
