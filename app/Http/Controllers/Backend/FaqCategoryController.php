<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\FaqCategoryRepositoryContract;
use Suitsite\Models\FaqCategory;

class FaqCategoryController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param FaqCategoryRepositoryContract $_baseRepo
     */
    public function __construct(FaqCategoryRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.faqcategory';
        $this->viewBaseClosure = 'backend.faqcategories';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E5');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    protected function processRequest($request)
    {
        $slug = $request['slug'] ?: ($request['name'] ?: '');
        $request['slug'] = str_slug($slug);
        return parent::processRequest($request);
    }
}
