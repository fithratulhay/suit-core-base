<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsiteextra\Repositories\Contract\PollCategoryRepositoryContract;

class PollCategoryController extends BaseController
{
    public function __construct(PollCategoryRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.poll-category";
        $this->routeDefaultIndex = "backend.poll-category.index";
        $this->viewBaseClosure = "backend.pollcategories";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'G0';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
