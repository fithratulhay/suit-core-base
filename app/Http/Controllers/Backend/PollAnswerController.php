<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsiteextra\Repositories\Contract\PollAnswerRepositoryContract;

class PollAnswerController extends BaseController
{
    public function __construct(PollAnswerRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.poll-answer";
        $this->routeDefaultIndex = "backend.poll-answer.index";
        $this->viewBaseClosure = "backend.pollanswers";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'G3';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
