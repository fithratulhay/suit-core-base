<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\ContentTypeRepositoryContract;
use Suitsite\Models\ContentType;

class ContentTypeController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param ContentTypeRepositoryContract $_baseRepo
     */
    public function __construct(ContentTypeRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.contenttype';
        $this->viewBaseClosure = 'backend.contenttypes';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E1');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
