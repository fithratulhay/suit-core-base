<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Suitcore\Models\SuitUserLog;
use Suitcore\Repositories\Contract\SuitUserLogRepositoryContract;

class UserLogController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param SuitUserLogRepositoryContract $_baseRepo
     */
    public function __construct(SuitUserLogRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.userlog';
        $this->viewBaseClosure = 'backend.userlogs';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('C4');
        view()->share('routeBaseName', $this->routeBaseName);
        view()->share('routeDefaultIndex', $this->routeDefaultIndex);
        view()->share('viewBaseClosure', $this->viewBaseClosure);
    }
}
