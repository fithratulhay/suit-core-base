<?php

namespace App\Http\Controllers\Backend;

use Input;
use Redirect;
use Route;
use View;
use Suitsiteextra\Repositories\Contract\CommentRepositoryContract;

class CommentController extends BaseController
{
    public function __construct(CommentRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.comment";
        $this->routeDefaultIndex = "backend.comment.index";
        $this->viewBaseClosure = "backend.comments";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'E15';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    /**
     * Return json list of Comment
     * @param
     * @return \Symfony\Component\HttpFoundation\Response
     */

     public function postIndexJson() {
        // Parameter
        $baseObj = $this->baseModel;
        $param = Input::all();
        // Filter Parameter
        $specificFilter = [];
        if (isset($param['user_id'])) $specificFilter['user_id'] = $param['user_id'];
        // Return
        $customRender = [
            'commentable_id' => "#commentable_id#",
            '_render_commentable_id' => function ($val) {
                $obj = $this->baseRepository->getBy('commentable_id', $val)->first();
                $model = $obj->commentable_type;
                $model = $model::find($val);
                return $model ? $model->title : '';
            },
            'commentable_type' => '#commentable_type#',
            '_render_commentable_type' => function($val) {
                $type = explode("\\", $val);
                return $type[count($type) - 1];
            }
        ];

        $menuSetting = [
            'session_token' => csrf_token(),
            'url_detail' => (Route::has($this->routeBaseName . '.show') ? route($this->routeBaseName . '.show',["id"=>"#id#"]) : ''),
            'url_delete' => (Route::has($this->routeBaseName . '.destroy') ? route($this->routeBaseName . '.destroy', ['id' => "#id#"]) : ''),
        ];
        $renderedMenu = View::make(self::$partialView[self::TABLE_MENU], ['menuSetting' => $menuSetting])->render();
        $customRender['menu'] = $renderedMenu;
        return $this->baseRepository->jsonDatatable(
            $param,
            $customRender,
            $specificFilter,
            null,
            null,
            null,
            Route::has($this->routeBaseName . '.select'));
    }

    /**
     * Post Reply a Comment
     *
     * @param  integer $id
     *
     * @return [type]     [description]
     */
    public function postReply($id)
    {
        // Reply
        $param   = Input::all();
        if (auth()->user()) {
            $param['user_id'] = auth()->user()->id;
        } else {
            $this->showNotification(self::ERROR_NOTIFICATION, 'Can\'t Reply ' . $this->baseModel->getLabel(), $this->baseModel->getLabel() . ' had not replied! An error occured when processing with database or email protocol.');
            if (Route::has($this->routeBaseName . '.show')) {
                return Redirect::route($this->routeBaseName . '.show', ['id' => $id]);
            } else {
                if (!empty($this->routeDefaultIndex)) {
                    return Redirect::route($this->routeDefaultIndex);
                }
                return Redirect::route($this->routeBaseName . '.index');
            }
        }

        $result  = $this->baseRepository->replyComment($id, $param);
        // Result
        if ($result['status']) {
            $this->showNotification(self::NOTICE_NOTIFICATION, $this->baseModel->getLabel() . ' Replied', $result['message']);
        } else {
            $this->showNotification(self::ERROR_NOTIFICATION, 'Can\'t Reply ' . $this->baseModel->getLabel(), $result['message']);
        }
        // Return
        if (Route::has($this->routeBaseName . '.show')) {
            return Redirect::route($this->routeBaseName . '.show', ['id' => $id]);
        } else {
            if (!empty($this->routeDefaultIndex)) {
                return Redirect::route($this->routeDefaultIndex);
            }
            return Redirect::route($this->routeBaseName . '.index');
        }
    }
}
