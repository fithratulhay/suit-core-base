<?php

namespace App\Http\Controllers\Backend;

use App;
use Suitsiteextra\Repositories\Contract\GalleryRepositoryContract;
use Illuminate\Support\Facades\Gate;
use Input;
use Redirect;
use Response;
use Route;
use Suitcore\Models\SuitModel;
use View;

class GalleryController extends BaseController
{
    protected $fileRepo;
    public function __construct(GalleryRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.gallery";
        $this->routeDefaultIndex = "backend.gallery.index";
        $this->viewBaseClosure = "backend.galleries";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'E14';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    public function getParentListJson() {
        SuitModel::$isFormGeneratorContext = false;
        // Parameter
        $q = Input::get('q', '');
        $isAll = Input::get('all', 0);
        $otherFilter = Input::all();
        $otherFilter['parent_id'] = null;
        // Process
        $baseObj = $this->baseModel;
        // begin prepare other-filter
        $attrSettings = $baseObj->attribute_settings;
        if ($otherFilter && is_array($otherFilter) && count($otherFilter) > 0) {
            foreach ($otherFilter as $key => $value)
                if (!isset($attrSettings[$key])) unset($otherFilter[$key]);
        }
        // end prepare other-filter
        $localColumnSearch = $baseObj->getFormattedValueColumn();

        if (method_exists($this, 'getDefaultFilter')) {
            $baseObj = $this->getDefaultFilter($baseObj);
        }

        $firstField = null;
        if ($localColumnSearch) {
            foreach ($localColumnSearch as $idx => $field) {
                if (!$firstField) {
                    $baseObj = $baseObj->where($field,"like","%".$q."%");
                    $firstField = $field;
                } else {
                    $baseObj = $baseObj->orWhere($field,"like","%".$q."%");
                }
            }
        }
        if ($otherFilter && is_array($otherFilter) && count($otherFilter) > 0) {
            $baseObj = $baseObj->where($otherFilter);
        }
        if ($firstField) {
            $baseObj = $baseObj->orderBy($firstField, 'asc');
        }
        if ($isAll) {
            $baseObj = $baseObj->get();
            if ($baseObj && count($baseObj) > 0) {
                $data = [];
                foreach ($baseObj as $obj) {
                    $data[] = [
                        'id' => $obj->id,
                        'name' => $obj->getFormattedValue()
                    ];
                }
                return Response::json($data);
            }
        } else {
            $baseObj = $baseObj->paginate(10); // ->take(10)->get();
            if ($baseObj && count($baseObj) > 0) {
                $data['items'] = [];
                foreach ($baseObj as $obj) {
                    $data['items'][] = [
                        "id" => $obj->id,
                        "value" => $obj->id,
                        "text" => $obj->getFormattedValue()
                    ];
                }
                $data['total_count'] = $baseObj->total();
                return Response::json($data);
            }
        }
        return Response::json([]);
    }

    public function postCreate() {
        // Save
        $param = Input::all();
        // if (empty($param['source']) && !empty($param['galleries'])) {
        //     $param['source'] = $param['galleries'][0];
        // }
        $baseObj = $this->baseModel;
        $result = $this->baseRepository->create($param, $baseObj);
        if ($baseObj->uploadError) {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . trans('suitcore.backend.create.upload.error.title'), trans('suitcore.backend.create.upload.error.message', ['obj' => strtolower($baseObj->_label)]));
        }
        // Return
        if ($result) {
            // if (!empty($param['type']) && $param['type'] == $baseObj::FILE_TYPE_IMAGE && !empty($param['galleries'])) {
            //     $galleries = $this->fileRepo->saveFile($result, $param);
            // }
            $message = 'Gallery baru telah ditambahkan';
            $this->sendNotification('gallery', $message, $result);
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Created', 'Successfully create new ' . strtolower($baseObj->_label) .'.');
            if (Route::has($this->routeBaseName . '.show')) {
                return Redirect::route($this->routeBaseName . '.show', ['id'=>$baseObj->id]);
            } else {
                return $this->returnToRootIndex($baseObj);
            }
        }
        $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Created', ($baseObj->errors ? $baseObj->errors->first() : "Unknown Error!"));
        return Redirect::route($this->routeBaseName . '.create')->with('errors', $baseObj->errors)->withInput($param);
    }

    public function postUpdate($id) {
        // Save
        $param = Input::all();
        // if (empty($param['source']) && !empty($param['galleries'])) {
        //     $param['source'] = $param['galleries'][0];
        // }
        $baseObj = $this->baseModel;
        if ($baseObj == null) return App::abort(404);
        // Check Policy
        if (Gate::denies('suitcorepermission', $baseObj)) {
            return App::abort(403);
        }
        // End Check Policy

        $result = $this->baseRepository->update($id, $param, $baseObj);
        if ($baseObj->uploadError) {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . trans('suitcore.backend.update.upload.error.title'), trans('suitcore.backend.update.upload.error.message', ['obj' => strtolower($baseObj->_label)]));
        }
        // Return
        if ($result) {
            // if (!empty($param['deleteGalleries'])) {
            //     foreach ($param['deleteGalleries'] as $galleryId) {
            //         $this->fileRepo->delete($galleryId);
            //     }
            // }
            // if (!empty($param['type']) && $param['type'] == $baseObj::FILE_TYPE_IMAGE && !empty($param['galleries'])) {
            //     $galleries = $this->fileRepo->saveFile($result, $param);
            // }
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Updated', 'Successfully update ' . strtolower($baseObj->_label) .'.');
            if (Route::has($this->routeBaseName . '.show'))
                return Redirect::route($this->routeBaseName . '.show', ['id'=>$id]);
            else {
                return $this->returnToRootIndex($baseObj);
            }
        }
        if ($baseObj == null) App::abort(404);
        $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Updated', ($baseObj->errors ? $baseObj->errors->first() : "Unknown Error!"));
        return Redirect::route($this->routeBaseName . '.update', ['id'=>$id])->with('errors', $baseObj->errors)->withInput($param);
    }
}
