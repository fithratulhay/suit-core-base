<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitconversation\Repositories\Contract\ConversationGroupRepositoryContract;

class ConversationGroupController extends BaseController
{
    public function __construct(ConversationGroupRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.conversation-group";
        $this->routeDefaultIndex = "backend.conversation-group.index";
        $this->viewBaseClosure = "backend.conversationgroups";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'C7';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
