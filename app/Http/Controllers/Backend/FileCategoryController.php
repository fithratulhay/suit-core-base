<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsiteextra\Repositories\Contract\FileCategoryRepositoryContract;

class FileCategoryController extends BaseController
{
    public function __construct(FileCategoryRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.file-category";
        $this->routeDefaultIndex = "backend.file-category.index";
        $this->viewBaseClosure = "backend.filecategories";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'F2';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
