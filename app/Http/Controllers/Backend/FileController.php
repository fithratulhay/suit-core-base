<?php

namespace App\Http\Controllers\Backend;

use Input;
use Route;
use Redirect;
use View;
use Suitsiteextra\Repositories\Contract\FileRepositoryContract;

class FileController extends BaseController
{
    public function __construct(FileRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.file";
        $this->routeDefaultIndex = "backend.file.index";
        $this->viewBaseClosure = "backend.files";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'F3';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    public function postCreate() {
        // Save
        $param = Input::all();
        $baseObj = $this->baseModel;
        $result = $this->baseRepository->create($param, $baseObj);
        if ($baseObj->uploadError) {
            $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . trans('suitcore.backend.create.upload.error.title'), trans('suitcore.backend.create.upload.error.message', ['obj' => strtolower($baseObj->_label)]));
        }
        // Return
        if ($result) {
            $message = $result->category ? $result->category->title . ' baru telah ditambahkan' : 'File baru telah ditambahkan';
            $fileCategory = 'file';
            if ($result->category->parent && $result->category->parent->slug == 'policy-procedure'
                || $result->category && $result->category->slug == 'procedure-policy') {
                $fileCategory = 'procedure-policy';
            }
            $this->showNotification(self::NOTICE_NOTIFICATION, $baseObj->_label . ' Created', 'Successfully create new ' . strtolower($baseObj->_label) .'.');
            if (Route::has($this->routeBaseName . '.show')) {
                return Redirect::route($this->routeBaseName . '.show', ['id'=>$baseObj->id]);
            } else {
                return $this->returnToRootIndex($baseObj);
            }
        }
        $this->showNotification(self::ERROR_NOTIFICATION, $baseObj->_label . ' Not Created', ($baseObj->errors ? $baseObj->errors->first() : "Unknown Error!"));
        return Redirect::route($this->routeBaseName . '.create')->with('errors', $baseObj->errors)->withInput($param);
    }
}
