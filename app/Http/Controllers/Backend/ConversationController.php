<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitconversation\Repositories\Contract\ConversationRepositoryContract;

class ConversationController extends BaseController
{
    public function __construct(ConversationRepositoryContract $baseRepo){
        parent::__construct($baseRepo);
        $this->routeBaseName = "backend.conversation";
        $this->routeDefaultIndex = "backend.conversation.index";
        $this->viewBaseClosure = "backend.conversations";
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->pageId = 'C6';
        View::share('pageId', $this->pageId);
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
