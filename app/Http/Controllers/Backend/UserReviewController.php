<?php

namespace App\Http\Controllers\Backend;

use Suitsiteextra\Repositories\Contract\UserReviewRepositoryContract;

class UserReviewController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param SuitUserLogRepositoryContract $_baseRepo
     */
    public function __construct(UserReviewRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.review';
        $this->viewBaseClosure = 'backend.reviews';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('C5');
        view()->share('routeBaseName', $this->routeBaseName);
        view()->share('routeDefaultIndex', $this->routeDefaultIndex);
        view()->share('viewBaseClosure', $this->viewBaseClosure);
    }
}