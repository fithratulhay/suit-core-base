<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Suitcore\Models\SuitUrlRedirection;
use Suitcore\Repositories\Contract\SuitUrlRedirectionRepositoryContract;

class UrlRedirectionController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param SuitUrlRedirectionRepositoryContract $_baseRepo
     */
    public function __construct(SuitUrlRedirectionRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.urlredirection';
        $this->viewBaseClosure = 'backend.urlredirections';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E11');
        view()->share('routeBaseName', $this->routeBaseName);
        view()->share('routeDefaultIndex', $this->routeDefaultIndex);
        view()->share('viewBaseClosure', $this->viewBaseClosure);
    }
}
