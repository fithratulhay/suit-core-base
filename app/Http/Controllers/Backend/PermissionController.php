<?php

namespace App\Http\Controllers\Backend;

use Cache;
use File;
use Form;
use Input;
use MessageBag;
use Redirect;
use View;
use Illuminate\Http\Request;
use Suitcore\Models\SuitUserRole;
use Suitcore\Models\SuitUserPermission;
use Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract;

class PermissionController extends BaseController
{
    protected $roleRepo;

    /**
     * Override Default Constructor
     * @param  SuitUserPermissionRepositoryContract $_baseRepo
     * @param  SuitUserRoleRepositoryContract $_roleRepo
     * @return void
     */
    public function __construct(SuitUserPermissionRepositoryContract $_baseRepo, SuitUserRoleRepositoryContract $_roleRepo){
        parent::__construct($_baseRepo);
        $this->routeBaseName = "backend.permission";
        $this->routeDefaultIndex = "backend.permission.index";
        $this->viewBaseClosure = "backend.permissions";
        $this->viewInstanceName = 'baseObject';
        $this->roleRepo = $_roleRepo;
        // page ID
        $this->pageId = 'C3';
        View::share('pageId', $this->pageId);
        View::share('pageIcon', 'fa fa-home');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }

    public function getAcl()
    {
        $baseObject = $this->baseModel;
        $roles = $this->roleRepo->getAllRoleObjects();
        $routeCollection = $this->baseRepository->getRawRoutes();
        $permissionList = $this->baseRepository->getCachedList();
        return view('backend.permissions.acl', compact('baseObject','roles','routeCollection', 'permissionList'));
    }

    public function postUpdateAcl()
    {
        $param = Input::all();
        $result = false;
        
        if (!isset($param['allprivileges'])) {
            // acl-list-checkbox mandatory
            $this->showNotification(self::ERROR_NOTIFICATION, 'ACL Not Updated', 'Access control list not updated! Permission data entry not exist!');
            return redirect()->back();
        }
        
        $param = explode(',',$param['allprivileges']);

        $permissionList = $this->baseRepository->getCachedList();

        $listUpdateAllow = [];
        $listUpdateDeny = [];
        $listCreate = [];

        foreach ($param as $key => $value) {
            $elmt = explode(':',$value);
            if (count($elmt) == 2) {
                $values = explode('xxxxx', $elmt[0]);
                $roleId = $values[0];
                unset($values[0]);
                $routeName = implode('.', $values);
                // Save Operations
                $permission = $permissionList && isset($permissionList[$roleId]) && isset($permissionList[$roleId][$routeName]) ? $permissionList[$roleId][$routeName] : null;
                if ($permission) {
                    $permission = explode(':', $permission);
                    if ($elmt[1] == 'T') {
                        $listUpdateAllow[] = $permission[0];
                    } else {
                        // $elmt[1] == 'F'
                        $listUpdateDeny[] = $permission[0];
                    }
                } else {
                    $listCreate[] = [
                        'role_id' => $roleId,
                        'route_name' => $routeName,
                        'status' => ($elmt[1] == 'T' ? SuitUserPermission::ACCEPT : SuitUserPermission::DENY),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s')
                    ];
                }
            }
        }
        if ($listCreate && count($listCreate) > 0) {
            $result = SuitUserPermission::insert($listCreate);
        }
        if ($listUpdateAllow && count($listUpdateAllow) > 0) {
            $result = SuitUserPermission::whereIn('id', $listUpdateAllow)->update(['status' => SuitUserPermission::ACCEPT]);
        }
        if ($listUpdateDeny && count($listUpdateDeny) > 0) {
            $result = SuitUserPermission::whereIn('id', $listUpdateDeny)->update(['status' => SuitUserPermission::DENY]);
        }
        // Begin Update Cache
        $permissionList = $this->baseRepository->refreshList();
        // Finish Update Cache
        if ($result) {
            $this->showNotification(self::NOTICE_NOTIFICATION, 'ACL Updated', 'Access control list has been updated!');
        } else {
            $this->showNotification(self::ERROR_NOTIFICATION, 'ACL Not Updated', 'Access control list not updated! An error occured when processing with database.');
        }
        return Redirect::route('backend.permission.acl');
    }

    public function getCopy()
    {
        $baseObject = $this->baseModel;
        $roles = SuitUserRole::all();
        return view('backend.permissions.copy-permission', compact('baseObject', 'roles'));
    }

    public function postCopy(Request $request)
    {
        $acceptedSources = SuitUserPermission::where('role_id', $request->source)->get();
        if ($acceptedSources->count() > 0 && $request->destination) {
            foreach ($acceptedSources as $source) {
                $target = SuitUserPermission::firstOrNew([
                                        'role_id' => $request->destination,
                                        'route_name' => $source->route_name
                                    ]);
                $target->status = $source->status;
                $target->save();
            }
            Cache::forget('role_permissions');
            $this->showNotification(self::NOTICE_NOTIFICATION, 'ACL Updated', 'Success copy permission');
            return redirect()->back();
        }
        $this->showNotification(self::ERROR_NOTIFICATION, 'ACL Not Updated', 'No permission to copied');
        return redirect()->back();
    }
}
