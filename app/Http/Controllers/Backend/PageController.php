<?php

namespace App\Http\Controllers\Backend;

use View;
use Suitsite\Repositories\Contract\PageRepositoryContract;
use Suitsite\Models\Page;

class PageController extends BaseController
{
    /**
     * Override Default Constructor.
     *
     * @param PageRepositoryContract $_baseRepo
     */
    public function __construct(PageRepositoryContract $_baseRepo)
    {
        parent::__construct($_baseRepo);
        $this->routeBaseName = 'backend.page';
        $this->viewBaseClosure = 'backend.pages';
        $this->viewInstanceName = 'baseObject';
        // page ID
        $this->setID('E3');
        View::share('routeBaseName', $this->routeBaseName);
        View::share('routeDefaultIndex', $this->routeDefaultIndex);
        View::share('viewBaseClosure', $this->viewBaseClosure);
    }
}
