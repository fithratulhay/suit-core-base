<?php

namespace App\Http\Controllers\Api;

use Hash;
use Input;
use Mail;
use Response;
use App\Models\User;
use App\Models\UserLanguageCapability;
use App\Models\UserProfile;
use App\Models\UserExperience;
use App\Models\UserEducation;
use App\Repositories\MatcherRepository;
use App\Repositories\UserLanguageCapabilityRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserExperienceRepository;
use App\Repositories\UserEducationRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Suitcore\Notification\GcmNotifier;
use Suitcore\Controllers\ApiController;

// USERCONTROLLER
class UserController extends ApiController
{
    use ResetsPasswords;
    
    /**
     * @var UserRepository
     * @deprecated
     */
    protected $authUser;
    protected $userLangRepo;
    protected $userEducationRepo;
    protected $userExperienceRepo;

    /**
     * Initialize new CountryController
     *
     * @param UserRepository $repository
     * @param User           $_user
     */
    public function __construct(UserRepository $repository, 
        UserLanguageCapabilityRepository $_userLangRepo,
        UserEducationRepository $_userEducationRepo,
        UserExperienceRepository $_userExperienceRepo,
        User $_user)
    {
        parent::__construct($repository, $_user);

        $this->userLangRepo = $_userLangRepo;
        $this->userEducationRepo = $_userEducationRepo;
        $this->userExperienceRepo = $_userExperienceRepo;
    }

    /**
     * @api GET /v1/user/list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = $this->repository->getByParameter($request->all());

        if (! $data->isEmpty()) {
            return $this->toJson(20, [
                'message' => 'User list retrieved',
                'results' => $data->toApi()
            ]);
        }

        return $this->toJson(20, [
            'message' => 'No data found'
        ]);
    }

    /**
     * Retrieve user data by ID.
     *
     * @param  int|User  $id
     * @return User
     */
    protected function findUser($id)
    {
        return $id instanceof User ? $id : $this->baseModel->find($id);
    }

    /**
     * @api GET /v1/user/{id}
     * @param int|User $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $message = $id === $this->authUser ? 'Your' : 'User id: '.$id;

        $userId = ($id && $id instanceof User ? $id->id : $id); 
        $user = $this->repository->getCachedUserProfile($userId);
        // if ($user = $this->findUser($id)) {
        if ($user) {
            // $user->load('profile', 'educations', 'languageCapabilities', 'skills', 'organizations', 'experiences', 'companies');
            
            return $this->toJson(20, [
                'message' => $message." account retrieved",
                'result' => $user->toApi()
            ]);
        }

        return $this->toJson(40, ['message' => $message.'account not found']);
    }

    public function update($id, Request $request, $updateWithProfile = false)
    {
        $message = $id === $this->authUser ? 'Your' : 'User id: '.$id;

        $user = $this->findUser($id);

        if (null === $user) {
            return $this->toJson(20, [
                'message' => 'User not found'
            ]);
        }

        $updateable = ['password', 'password_confirm', 'current_password', 'picture', 'name', 'last_name', 'birthdate', 'birth_year', 'birth_month', 'phone_number', 'profile_completion_step', 'fb_id', 'fb_access_token', 'li_access_token', 'gp_id', 'gp_access_token', 'current_experience'];
        $input = array_filter($request->only($updateable));
        // $input = array_filter($request->only($user->getFillable()));
        //if($request->has('password_confirm')) {
        //    $input['password_confirm'] = $request->get('password_confirm');
        //}

        if($request->has('birth_year')) {
            if($request->has('birth_month')) {
                $input['birthdate'] = Carbon::create($request->get('birth_year'), $request->get('birth_month'), 1)->format('Y-m-d');
            } else {
                $input['birthdate'] = Carbon::create($request->get('birth_year'), 1, 1)->format('Y-m-d');
            }
        }

        if (array_key_exists('password', $input)) {
            try {
                $input = $this->updatePassword($input);
            } catch (\InvalidArgumentException $e) {
                return $this->toJson(34, ['message' => $e->getMessage()]);
            }
        }

        /*
        if (array_key_exists('email', $input)) {
            // Remove email when exists.
            unset($input['email']);
        }
        if (array_key_exists('username', $input)) {
            // Remove username when exists.
            unset($input['username']);
        }
        if (array_key_exists('age', $input)) {
            // Remove age when exists.
            unset($input['age']);
        }
        */
        if (isset($input['current_experience'])) {
            $this->updateCurrentExperience($input['current_experience']);
        }

        // if ($input && $user->fill($input)->save()) {
        if ($input && $this->repository->update($user->id, $input, $user)) {
            if ($updateWithProfile) {
                $this->updateProfile($request);
            }
            $user->load('profile', 'educations', 'languageCapabilities', 'skills', 'organizations', 'experiences', 'companies');
            return $this->toJson(20, [
                'message' => $message.' profile updated',
                'result' => $user->toApi()
            ]);
        } elseif ($updateWithProfile) {
            // just profile
            return $this->updateProfile($request);
        }

        return $this->toJson(30, [
            'message' => sprintf(
                'Something went wrong, %s profile is not updated.',
                $message
            )
        ]);
    }

    /**
     * @api GET /v1/user/{id}
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAccount()
    {
        return $this->show($this->authUser);
    }

    /**
     * @api POST /v1/user/{id}
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAccount(Request $request)
    {
        return $this->update($this->authUser, $request, true);
    }

    /**
     * @api POST /v1/user/{id}
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearIssue(Request $request)
    {
        $id = $this->authUser->getKey();
        $profile = $this->findUser($id)->profile;
        if (null === $profile) {
            return $this->toJson(20, [
                'message' => 'Profile not found'
            ]);
        }

        if ($profile->fill(['is_issue' => false, 'issue_message' => null])->save()) {
            return $this->showAccount();
        }

        return $this->toJson(30, [
            'message' => 'Something went wrong, can not clear issue message. Please try again later'
        ]);
    }

    /**
     * @api GET /v1/user/{id}
     * @param int|User $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProfile($id = null)
    {
        $id = $id ?: $this->authUser->getKey();

        if ($profile = $this->findUser($id)->profile) {
            return $this->toJson(20, [
                'message' => "User id: {$id} profiles retrieved",
                'result' => $profile->toArray()
            ]);
        }

        return $this->toJson(40, ['message' => 'User not found']);
    }

    /**
     * @api POST /v1/user/{id}
     * @param Request $request
     * @param int|User $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request, $id = null)
    {
        // Init User Model (MUST be current authenticated user)
        // $id = $id ?: $this->authUser->getKey();
        // $user = $this->findUser($id);
        $user = $this->authUser;
        if (!$user) {
            return $this->toJson(30, [
                'message' => 'User not found!'
            ]);
        }

        if ($request->get('user_experience_from') === 0 || $request->get('user_experience_from') == '0') {
            $request->merge(['user_experience_from' => 'nol']);
        }
        if ($request->get('user_experience_until') === 0 || $request->get('user_experience_until') == '0') {
            $request->merge(['user_experience_until' => 'nol']);
        }
        if ($request->get('current_industry_id') === '' || $request->get('current_industry_id') == '0') {
            $request->merge(['current_industry_id' => 'nol']);
        }
        if ($request->get('current_profession_id') === '' || $request->get('current_profession_id') == '0') {
            $request->merge(['current_profession_id' => 'nol']);
        }
        // Update User Profile
        $profile = $user->profile;
        $input = array_filter($request->all()); 
        if ($request->has('personality')) {
            $input['about'] = $request->get('personality');
        }
        if(isset($input['user_experience_from']) && $input['user_experience_from'] == 'nol') {
            $input['user_experience_from'] = '0';
        }
        if(isset($input['user_experience_until']) && $input['user_experience_until'] == 'nol') {
            $input['user_experience_until'] = '0';
        }
        if(isset($input['current_industry_id']) && $input['current_industry_id'] == 'nol') {
            $input['current_industry_id'] = null;
        }
        if(isset($input['current_profession_id']) && $input['current_profession_id'] == 'nol') {
            $input['current_profession_id'] = null;
        }
        if ($user && (null === $profile)) {
            $profile = new UserProfile;
            $user->profile()->save($profile);
        }
        // enforce completion step if any
        if ($request->has('profile_completion_step')) {
            $user->update([ 'profile_completion_step' => $request->get('profile_completion_step') ]);
            $user = $this->findUser($user->id);
        }
        // continue update user profile
        if ($profile->fill($input)->save()) {
            if ($user->profile_completion_step < 6) {
                $profileCompletionResult = true;
                // update education if any
                if($request->has('degree_id')) {
                    $profileCompletionResult = false;
                    // Custom Way for OnBoarding
                    $params = array_merge($input, ['user_id' => $user->getKey()]);
                    $lastObject = $this->userEducationRepo->getByParameter(['user_id' => $user->getKey(), 'paginate' => false, 'perPage' => 1]);
                    if ($lastObject && $lastObject->id > 0) {
                        $profileCompletionResult = $this->userEducationRepo->update($lastObject->id, $params);
                    } else {
                        $profileCompletionResult = $this->userEducationRepo->create($params);
                    }
                }
                if (!$profileCompletionResult) {
                    return $this->toJson(30, [
                        'message' => 'Your profile is updated but education data was incorrect and not saved!',
                        'result' => $user->toApi()
                    ]);
                }
                // update experiences if any
                if($request->has('company') || $request->has('industry_id') || $request->has('profession_id') || $request->has('environment_id')) {
                    $profileCompletionResult = false;
                    // Custom Way for OnBoarding
                    $params = array_merge($input, ['user_id' => $user->getKey()]);
                    $lastObject = $this->userExperienceRepo->getByParameter(['user_id' => $user->getKey(), 'paginate' => false, 'perPage' => 1]);
                    if ($lastObject && $lastObject->id > 0) {
                        $profileCompletionResult = $this->userExperienceRepo->update($lastObject->id, $params);
                    } else {
                        $profileCompletionResult = $this->userExperienceRepo->create($params);
                    }
                    if ($profileCompletionResult) {
                        if (!$user->profile_completion_step || $user->profile_completion_step < 6) {
                            // Custom Way for OnBoarding
                            // update profile
                            $profile = $user->profile;
                            if ($user && (null === $profile)) {
                                $profile = new UserProfile;
                                $user->profile()->save($profile);
                            }
                            $profile->fill([
                                'current_company' => $profileCompletionResult->company,
                                'current_environment_id' => $profileCompletionResult->environment_id,
                                'current_type_of_job' => $profileCompletionResult->type_of_job,
                                'current_job_level_id' => $profileCompletionResult->job_level_id,
                                'current_profession_id' => $profileCompletionResult->profession_id,
                                // 'current_join_date' => $profileCompletionResult->profession_id,
                                'current_industry_id' => $profileCompletionResult->industry_id,
                                'current_job_title' => $profileCompletionResult->job_title,
                                'current_salary' => $profileCompletionResult->monthly_salary,
                            ])->save();
                            // end update profile
                        }
                    }
                }
                if (!$profileCompletionResult) {
                    return $this->toJson(30, [
                        'message' => 'Your profile is updated but experience data was incorrect and not saved!',
                        'result' => $user->toApi()
                    ]);
                }
                // update languages experiences if any
                if($request->has('language_id') && $request->has('rate')) {
                    $profileCompletionResult = false;
                    // Custom Way for OnBoarding
                    $rate = $request->get('rate');
                    $langParams = [
                        'user_id' => $user->id,
                        'language_id' => $request->get('language_id'),
                        'listen_capability' => $rate,
                        'speak_capability' => $rate,
                        'read_capability' => $rate,
                        'write_capability' => $rate
                    ];
                    $lastObject = $this->userLangRepo->getByParameter(['user_id' => $user->id, 'paginate' => false, 'perPage' => 1]);
                    if ($lastObject && $lastObject->id > 0) {
                        $profileCompletionResult = $this->userLangRepo->update($lastObject->id, $langParams);
                    } else {
                        $profileCompletionResult = $this->userLangRepo->create($langParams);
                    }
                }
                if (!$profileCompletionResult) {
                    return $this->toJson(30, [
                        'message' => 'Your profile is updated but languages capabilities data was incorrect and not saved!',
                        'result' => $user->toApi()
                    ]);
                }
            }
            // render result with updated user
            $user = $this->findUser($user->id);
            $user->load('profile', 'languageCapabilities', 'skills', 'organizations', 'experiences', 'companies');
            return $this->toJson(20, [
                'message' => 'Successfully update your profile',
                'result' => $user->toApi()
                // 'result' => $profile->toApi()
            ]);
        }
        // error when updating user profile
        return $this->toJson(30, [
            'message' => 'Something went wrong, your profile is not updated. Please try again later'
        ]);
    }

    public function updateCurrentExperience($experienceId)
    {
        $this->userExperienceRepo->updateCurrentExperience($this->authUser ? $this->authUser->id : null, $experienceId); 
    }

    public function deleteAccount(Request $request)
    {
        // Find User
        $user = $this->authUser;
        if (null === $user) {
            return $this->toJson(30, [
                'message' => 'User not found'
            ]);
        }

        if (!$request->has('password') || empty($request->get('password'))) {
            return $this->toJson(30, [
                'message' => 'Password required to fullfill this action!'
            ]);
        }

        if (!Hash::check($request->get('password'), $user->password)) {
            return $this->toJson(30, [
                'message' => 'Wrong Password! Password required to fullfill this action!'
            ]);
        }

        // Mark User Deleted
        try {
            $this->repository->closeAppAccount($user);
        } catch (\Exception $e) {
            // error result
            return $this->toJson(30, [
                'message' => 'Something went wrong, your account not deleted. Please try again later!'
            ]);
        }

        // success result
        return $this->toJson(20, [
            'message' => 'Account deleted!',
            'result' => $user->toApi()
        ]);
    }

    protected function updatePassword(array $input)
    {
        if (! array_key_exists('current_password', $input)) {
            throw new \InvalidArgumentException('Current Password is required to update your password');
        }

        if (!Hash::check($input['current_password'], $this->authUser->password)) {
            throw new \InvalidArgumentException('Wrong Current Password! Current Password required to fullfill this action!');
        }

        if (! array_key_exists('password_confirm', $input)) {
            throw new \InvalidArgumentException('Password confirmation is required to update your password');
        }

        if ($input['password'] !== $input['password_confirm']) {
            throw new \InvalidArgumentException('Password confirm field should equals with password field');
        }

        if (Hash::check($input['password'], $this->authUser->password)) {
            throw new \InvalidArgumentException('Your password is remain the same');
        }

        $input['password'] = \Hash::make($input['password']);
        unset($input['current_password']);
        unset($input['password_confirm']);

        return $input;
    }

    // User Social Media / Detail Update
    // Update data detail of current user
    // POST /api/user/me/updatesocmed
    // PARAM MUST : token
    // PARAM OPTIONAL : fb_id fb_email fb_name fb_access_token twitter_id twitter_username twitter_name twitter_access_token twitter_secret
    public function updateSocmed()
    {
        if ($this->authUser) {
            $user = $this->authUser;
            if (Input::has('fb_id')) {
                $user->fb_id = Input::get('fb_id');
            }
            if (Input::has('fb_email')) {
                $user->fb_email = Input::get('fb_email');
            }
            if (Input::has('fb_name')) {
                $user->fb_name = Input::get('fb_name');
            }
            if (Input::has('fb_access_token')) {
                $user->fb_access_token = Input::get('fb_access_token');
            }
            if (Input::has('twitter_id')) {
                $user->twitter_id = Input::get('twitter_id');
            }
            if (Input::has('twitter_username')) {
                $user->twitter_username = Input::get('twitter_username');
            }
            if (Input::has('twitter_name')) {
                $user->twitter_name = Input::get('twitter_name');
            }
            if (Input::has('twitter_access_token')) {
                $user->twitter_access_token = Input::get('twitter_access_token');
            }
            if (Input::has('twitter_secret')) {
                $user->twitter_secret = Input::get('twitter_secret');
            }

            $result = $user->save();
            if ($result) {
                return response()->json(['status' => 20, 'message' => 'Your Social Media Updated', 'result' => $user]);
            }

            return response()->json(['status' => 30, 'message' => 'Your Profile Not Updated! Something goes wrong with database!', 'result' => $user]);
        }

        return response()->json(['status' => 40, 'message' => 'API access failed! There is no logged in user in current session!']);
    }

    public function postForgetPassword(Request $request)
    {
        $email = $request->get('email');
        if (!User::where('email', $email)->first()) {
            return response()->json(['status' => 30, 'message' => 'User not found', 'result' => null]);
        }
        $this->sendResetLinkEmail($request);
        return response()->json(['status' => 20, 'message' => 'Link To Reset Password Has Been Sent. Please Check Your Email', 'result' => null]);
    }

    public function sendFCM(Request $request)
    {
        $user = $this->authUser;
        $message = $request->except('token');
        $badge = $request->get('badge', 1);
        $meta = ['notification' => ['badge' => $badge]];
        app(GcmNotifier::class)->send($user, $message, $meta);
        return response()->json(['status' => 20, 'message' => 'FCM has Sent to '.$user->name, 'result' => $message]);
    }
}
