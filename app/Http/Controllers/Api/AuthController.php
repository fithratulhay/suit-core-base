<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Suitcore\Controllers\ApiController;
use Suitcore\Accounts\FacebookAccount;
use Suitcore\Accounts\GoogleAccount;
use Suitcore\Accounts\LinkedInAccount;
use Suitcore\Accounts\SocialAccountInterface;
use Suitcore\Models\SuitUserToken;
use Suitcore\Repositories\Contract\SuitUserTokenRepositoryContract;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use App\Models\EmailVerification;
use App\Models\User;

/**
 * @property-read UserRepository
 */
class AuthController extends ApiController
{
    protected $userTokenRepo;

    /**
     * New AuthController instance
     *
     * @param SuitUserRepositoryContract $repository
     * @param SuitUserTokenRepositoryContract $userTokenRepo
     */
    public function __construct(SuitUserRepositoryContract $repository, SuitUserTokenRepositoryContract $userTokenRepo)
    {
        parent::__construct($repository);
        $this->userTokenRepo = $userTokenRepo;
    }

    /**
     * @api POST /v1/user/register
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email|unique:users,email',
                'password' => 'required'
            ]);
        } catch (ValidationException $e) {
            $failedFields = $e->validator->failed();
            if (isset($failedFields['email']['Unique'])) {
                // check user
                $user = $this->repository->getBy('email', $request->get('email'))->first();
                if ($user) {
                    if ($user->status == User::STATUS_ACTIVE) {
                        // and user already have profile / candidate (priority 1)
                        return $this->toJson(39, [
                            'subcode' => 2,
                            'message' => "Already Registered!",
                            'errors' => $e->validator->errors(),
                            'user' => $user
                        ]);
                    } elseif ($user->status == User::STATUS_INACTIVE) {
                        // NOT ACTIVATED YET
                        return $this->toJson(39, [
                            'subcode' => 5,
                            'message' => "Not Activated Yet!",
                            'errors' => $e->validator->errors(),
                            'user' => $user
                        ]);
                    } elseif ($user->status == User::STATUS_UNREGISTERED || $user->status == User::STATUS_CLOSED) {
                        // PREVIOUSLY DELETED ACCOUNT
                        return $this->toJson(39, [
                            'subcode' => 6,
                            'message' => "Account Deleted!",
                            'errors' => $e->validator->errors(),
                            'user' => $user
                        ]);
                    } else {
                        // BANNED
                        return $this->toJson(39, [
                            'subcode' => 7,
                            'message' => "Account Banned",
                            'errors' => $e->validator->errors(),
                            'user' => $user
                        ]);
                    }
                }
            }
            return $this->toJson(39, [
                'subcode' => 1,
                'message' => $e->getMessage(),
                'errors' => $e->validator->errors()
            ]);
        }

        $data = array_merge($request->only('email', 'password'), [
            'role' => User::USER,
            'status' => User::STATUS_INACTIVE,
            'birthdate' => date('Y-m-d'),
            'last_visit' => date('Y-m-d H:i:s')
        ]);

        if (!isset($data['name']) || empty($data['name'])) {
            $data['name'] = $data['email'];
        }

        if (!isset($data['phone_number']) || empty($data['phone_number'])) {
            $data['phone_number'] = '00000000';
        }

        $data['password'] = \Hash::make($data['password']);

        if (! isset($data['username'])) {
            $data['username'] = preg_replace('/[^A-Za-z0-9\-]/', '', $data['email']);
        }

        if ($user = $this->repository->create($data)) {
            $this->sendVerification($data['email']);

            return $this->toJson(20, [
                'message' => 'User is registered',
                'result' => [
                    'user' => $user
                ]
            ]);
        }

        return $this->toJson(30, [
            'message' => 'Failed registering new user'
        ]);
    }

    /**
     * @api POST /v1/user/login
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credential = $request->only('email', 'password');

        /** @var User $user */
        $user =  $this->repository->getBy('email', $request->get('email'))->first();

        if (!$user || $user->status == User::STATUS_CLOSED) {
            return $this->toJson(40, [
                'message' => 'Login failed! User not found'
            ]);
        }

        if ($user->status == User::STATUS_INACTIVE) {
            return $this->toJson(313, [
                'message' => 'Login failed! User is not activated yet. Check your activation email or go to '.route('frontend.reactivation.link').' to re-send activation link',
                'result' => [
                    'resend_activation_url' => route('frontend.reactivation.link', ['email' => $user->email]),
                ]
            ]);
        }

        if ($user->status == User::STATUS_BANNED) {
            return $this->toJson(314, [
                'message' => 'Login failed! User is banned, please contact our administrator'
            ]);
        }

        if ($user->status == User::STATUS_UNREGISTERED) {
            $user->update([
                'status' => User::STATUS_ACTIVE
            ]);
            /*
            return $this->toJson(315, [
                'message' => 'Login failed! User is not registered, please contact our administrator'
            ]);
            */
        }

        if ($user->status != User::STATUS_ACTIVE) {
            return $this->toJson(313, [
                'message' => 'Login failed! User is not activated yet. Check your activation email or go to '.route('frontend.reactivation.link').' to re-send activation link',
                'result' => [
                    'resend_activation_url' => route('frontend.reactivation.link', ['email' => $user->email]),
                ]
            ]);
        }

        if (\Hash::check($credential['password'], $user->password)) {
            $userToken = $this->loginOnce($user);

            return $this->toJson(20, [
                'message' => 'Login success',
                'result' => [
                    'token' => $userToken->token,
                    'user' => $user->toApi()
                ]
            ]);
        }

        return $this->toJson(40, ['message' => 'Login failed! Wrong email or password']);
    }

    /**
     * @api POST /v1/user/login/facebook
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginFacebook(Request $request)
    {
        try {
            $account = new FacebookAccount($request);
        } catch (\InvalidArgumentException $e) {
            return $this->toJson(325, ['message' => $e->getMessage()]);
        }

        $user = $this->findOrCreateAccount($request, $account);

        if (null === $user) {
            return $this->toJson(40, [
                'message' => 'Failed to create new user from facebook credential'
            ]);
        }

        $userToken = $this->loginOnce($user);

        return $this->toJson(20, [
            'message' => 'Successfully logged in using facebook credential',
            'result' => [
                'token' => $userToken->token,
                'user' => $user->toApi()
            ]
        ]);
    }

    /**
     * @api POST /v1/user/login/google
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginGoogle(Request $request)
    {
        try {
            $account = new GoogleAccount($request);
        } catch (\InvalidArgumentException $e) {
            return $this->toJson(325, ['message' => $e->getMessage()]);
        }

        $user = $this->findOrCreateAccount($request, $account);

        if (null === $user) {
            return $this->toJson(40, [
                'message' => 'Failed to create new user from google credential'
            ]);
        }

        $userToken = $this->loginOnce($user);

        return $this->toJson(20, [
            'message' => 'Successfully logged in using google credential',
            'result' => [
                'token' => $userToken->token,
                'user' => $user->toApi()
            ]
        ]);
    }

    /**
     * @api POST /v1/user/login/linkedin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginLinkedin(Request $request)
    {
        try {
            $account = new LinkedInAccount($request);
        } catch (\InvalidArgumentException $e) {
            return $this->toJson(325, ['message' => $e->getMessage()]);
        }

        $user = $this->findOrCreateAccount($request, $account);

        if (null === $user) {
            return $this->toJson(40, [
                'message' => 'Failed to create new user from linkedin credential'
            ]);
        }

        $userToken = $this->loginOnce($user);

        return $this->toJson(20, [
            'message' => 'Successfully logged in using linkedin credential',
            'result' => [
                'token' => $userToken->token,
                'user' => $user->toApi()
            ]
        ]);
    }

    /**
     * Create new user account from social account credential
     *
     * @param SocialAccountInterface $account
     * @return User|null
     */
    protected function findOrCreateAccount(Request $request, SocialAccountInterface $account)
    {
        if ($request->has('token') &&
            $token = $this->userTokenRepo->getBy('token', $request->get('token'))->first())
        {
            return $token->user;
        }

        if ($user = $this->repository->createFromSocialAccount($account)) {
            return $user;
        }

        return null;
    }

    /**
     * @api GET /v1/user/check/{email}
     * @param string $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmail($email)
    {
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->toJson(340, ['message' => 'Email address not valid!']);
        }

        $user = $this->repository->getBy('email', $email)->first();

        if (null !== $user) {
            return $this->toJson(20, ['message' => 'User profile retrieved', 'result' => $user->toApi()]);
        }

        return $this->toJson(31, ['message' => 'User not found']);
    }

    /**
     * @api POST /v1/user/forgot-password/{email}
     * @param string $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->toJson(340, ['message' => 'Email address not valid!']);
        }

        $user = $this->repository->getBy('email', $email)->first();

        if (null === $user) {
            return $this->toJson(31, ['message' => 'User not found']);
        }

        $user->forget_password_token = \Hash::make($password = str_random(6));
        $user->save();

        \Mail::queue('emails.'.$this->getLocale().'.forgotpasswordresult', [
            'newpassword' => $password
        ], function (Message $message) use ($user) {
            $message->to($user->email, $user->name)->subject('YesJob Reset Password Request');
        });

        return $this->toJson(20, ['message' => 'Reset password email send']);
    }

    /**
     * @api POST /v1/user/send-verification/{email}
     * @param string $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendVerification($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->toJson(340, ['message' => 'Email address not valid!']);
        }

        /** @var User $user */
        $user = $this->repository->getBy('email', $email)->first();

        if ($user && $user->isActive()) {
            return $this->toJson(318, ['message' => 'User has been activated']);
        }

        /** @var EmailVerification $verify */
        $verify = EmailVerification::firstOrNew(['email' => $email]);

        $verify->status = EmailVerification::REGISTERED;
        $verify->activation_code = strtoupper(substr($user->generateActivationCode(), 0, 8));
        $verify->save();

        list($name) = explode('@', $email);

        $userEmail = $verify->email;
        $code = $verify->activation_code;
        \Mail::queue('emails.'.$this->getLocale().'.verification', [
            'name' => $name,
            'code' => $code,
            'activationLink' => route('frontend.mobile.emailactivation', ['code' => $code, 'email' => $userEmail, 'locale' => $this->getLocale()])
        ], function ($m) use ($userEmail, $name) {
            $m->to($userEmail, $name)->subject('YesJob email verification');
        });

        return $this->toJson(20, []);
    }

    /**
     * @api GET /v1/user/verify/{code}/{$email}
     * @param string $code
     * @param string $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request, $code, $email)
    {
        $model = EmailVerification::where('email', $email)
            ->where('activation_code', $code)
            ->first();

        if ($model) {
            $model->status = EmailVerification::VERIFIED;
            $model->save();

            if ($user = $model->user) {
                $user->activate();
            }

            Mail::queue('emails.'.$this->getLocale().'.welcome', [
                'name' => $user->name, 'siteurl' => route('frontend.home')
            ], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('YesJob | Welcome');
            });

            if (!$request->isJson() && !$request->wantsJson()) {
                return view('frontend.user.mobileVerificationSuccess');
            }

            return $this->toJson(20, ['message' => 'Email verified!']);
        }

        return $this->toJson(31, ['message' => 'User not found!']);
    }

    /**
     * @api POST /v1/user/logout
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function logout(Request $request)
    {
        if ($this->authUser) {
            if ($request->has('registration_id')) {
                $this->authUser->removeGcmRegistrationId(
                    $request->get('registration_id')
                );
            }

            $userToken = $request->get('token');
            $userToken = $this->userTokenRepo->getBy('token', $userToken)->first();
            if ($userToken->delete()) {
                return $this->toJson(20, ['message' => 'Logout Success, session ended']);
            } else {
                return $this->toJson(30, ['message' => 'Invalid token!']);
            }
        }

        return $this->toJson(41, ['message' => 'Logout Failed, there is no logged in user in current session!']);
    }

    /**
     * @api POST /v1/user/add-gcmid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addGcmId(Request $request)
    {
        if (! $request->has('registration_id')) {
            return $this->toJson(314, ['message' => 'Cannot register user, mandatory field not set']);
        }

        $user = $this->authUser;

        $user->addGcmRegistrationId($request->get('registration_id'), $request->get('device_type'));

        return $this->toJson(20, ['message' => 'GCM Registration ID Updated', 'result' => $user->toArray()]);
    }

    /**
     * @api POST /v1/user/remove-gcmid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeGcmId(Request $request)
    {
        if (! $request->has('registration_id')) {
            return $this->toJson(314, ['message' => 'Cannot register user, mandatory field not set']);
        }

        $user = $this->authUser;

        $user->removeGcmRegistrationId($request->get('registration_id'));

        return $this->toJson(20, ['message' => 'GCM Registration ID Updated', 'result' => $user->toArray()]);
    }

    /**
     * Generate random string for user activation.
     *
     * @return string
     */
    protected function generateActivationCode()
    {
        $random = '';
        srand((float) microtime() * 1000000);
        $data = '0123456789';

        for ($i = 0; $i < 6; ++$i) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }

        return $random;
    }
}
