<?php

namespace App\Http\Controllers\Api;

use Suitcore\Controllers\ApiController;
use Suitsite\Repositories\Contract\FaqRepositoryContract;
use Suitsite\Models\Faq;

class FaqController extends ApiController 
{
	// Constructor
	public function __construct(FaqRepositoryContract $repository)
    {
        parent::__construct($repository);
    }
}
