<?php

namespace App\Http\Controllers\Frontend;

use Suitsite\Repositories\Contract\ContentRepositoryContract;
use Suitsite\Repositories\Contract\ContentTypeRepositoryContract;
use Suitsite\Repositories\Contract\ContentCategoryRepositoryContract;

class ContentController extends BaseController
{
    /** CONSTANTS FOR STATIC CONTENT **/
    const TYPE_STATIC = 'static';
    const STATIC_ABOUT = 'about';
    const STATIC_TERM = 'term';
    const STATIC_POLICY = 'policy';

    /** ATTRIBUTES **/
    protected $contentRepo;
    protected $contentTypeRepo;
    protected $contentCategoryRepo;
    public $staticSlug = [
                            self::STATIC_POLICY,
                            self::STATIC_TERM,
                            self::STATIC_ABOUT, 
                         ];

    /** METHODS / ACTIONS **/
    /**
     * Default constructor.
     **/
    public function __construct(ContentRepositoryContract $_contentRepo, ContentTypeRepositoryContract $_contentTypeRepo, ContentCategoryRepositoryContract $_contentCategoryRepo)
    {
        parent::__construct();
        $this->contentRepo = $_contentRepo;
        $this->contentTypeRepo = $_contentTypeRepo;
        $this->contentCategoryRepo = $_contentCategoryRepo;
    }

    /**
     * Show Static Content Detail.
     *
     * @return View|Redirect
     **/
    public function getStaticContent($slug)
    {
        if (in_array($slug, $this->staticSlug)) {
            $content = $this->contentRepo->getOrInit__cached($slug, self::TYPE_STATIC);
            view()->share(['pageTitle' => $content->title]);

            return view('frontend.content.static')->with('content', $content);
        }

        return redirect()->route('frontend.home');
    }

    /**
     * Show Dynamic Content List/Detail Pages
     *
     * @return View|Redirect
     **/
    public function getDynamicContent($type, $slug = null)
    {
        $contentType = $this->contentTypeRepo->getBy__cached('code', $type)->first();
        if ($contentType && $contentType->code != self::TYPE_STATIC) {
            view()->share(['pageTitle' => $contentType->name]);
            if($slug){
                $object = $this->contentRepo->getContentBy__cached($slug, $contentType->code);
                if ($object) {
                    view()->share(['pageTitle' => $object->title]);
                    $recentList = $this->contentRepo->getListOf__cached($contentType->code, ($object->category ? $object->category->slug : null), false, 5);
                    return view('frontend.content.detail')->with('content', $object)
                                ->with('title', $contentType->name)
                                ->with('routeNode', $contentType->code)
                                ->with('recentList', $recentList);
                } 
                return redirect()->route('frontend.home');
            } else {
                $contentCategories = $this->contentCategoryRepo->getBy__cached('type_id', $contentType->id);
                $contentCategory = null;
                if (request()->has('cat')) $contentCategory = $this->contentCategoryRepo->getCategoryBy__cached(request()->get('cat'), $contentType->code);
                $objectList = $this->contentRepo->getListOf__cached($contentType->code, ($contentCategory ? $contentCategory->slug : null), true, 9, request()->all());
                view()->share(['pageTitle' => $contentType->name]);
                return view('frontend.content.list')
                        ->with('title', $contentType->name)
                        ->with('routeNode', $contentType->code)
                        ->with('contents', $objectList)
                        ->with('contentCategories', $contentCategories)
                        ->with('contentCategory', $contentCategory);
            }
        }
        return redirect()->route('frontend.home');
    }

    /**
     * Show Dynamic Content Search List Pages
     *
     * @return View|Redirect
     **/
    public function getSearchContent()
    {
        $keyword = request()->get('keyword', '');
        $page = request()->get('page', 1);
        if (!empty($keyword)) {
            // default
            view()->share(['pageTitle' => 'Search Result']);
            $dynamicTypeList = array_values(array_except($this->contentTypeRepo->all__cached()->pluck('id', 'code')->toArray(), [self::TYPE_STATIC]));
            $objectList = $this->contentRepo->getByParameter([
                'type_id' => implode(',', $dynamicTypeList),
                'keyword' => $keyword,
                'orderBy' => 'relevance',
                'pagination' => true,
                'perPage' => 9,
                'page' => $page
            ], [], true);
            return view('frontend.content.search')
                    ->with('contents', $objectList)
                    ->with('keyword', $keyword);
        }
        return redirect()->route('frontend.home');
    }
}
