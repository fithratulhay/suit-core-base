<?php

namespace App\Http\Controllers\Frontend;

use Suitsite\Models\ContactMessage;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitsite\Repositories\Contract\ContentTypeRepositoryContract;
use Suitsite\Repositories\Contract\ContentRepositoryContract;
use Suitsite\Repositories\Contract\FaqCategoryRepositoryContract;
use Suitsite\Repositories\Contract\FaqRepositoryContract;
use Suitsite\Repositories\Contract\ContactMessageRepositoryContract;
use Suitsite\Repositories\Contract\BannerRepositoryContract;

class HomeController extends BaseController
{
    protected $bannerRepo;
    protected $contentRepo;
    protected $userRepo;
    protected $contactMessageRepo;
    protected $faqRepo;
    protected $faqCategoryRepo;
    protected $userStoryRepo;

    public function __construct(ContentTypeRepositoryContract $_contentTypeRepo,
        ContentRepositoryContract $_contentRepo, 
        SuitUserRepositoryContract $_userRepo, 
        ContactMessageRepositoryContract $_contactMessageRepo,
        FaqRepositoryContract $_faqRepo,
        FaqCategoryRepositoryContract $_faqCategoryRepo,
        BannerRepositoryContract $_bannerRepo)
    {
        parent::__construct();
        $this->bannerRepo = $_bannerRepo;
        $this->contentTypeRepo = $_contentTypeRepo;
        $this->contentRepo = $_contentRepo;
        $this->userRepo = $_userRepo;
        $this->contactMessageRepo = $_contactMessageRepo;
        $this->faqRepo = $_faqRepo;
        $this->faqCategoryRepo = $_faqCategoryRepo;
    }

    /**
     * Show Frontend / Desktop Site Homepage.
     *
     * @return View
     **/
    public function getIndex()
    {
        $showLanding = settings('enable_landing', '');
        if (!empty($showLanding)) {
            view()->share(['pageTitle' => '']);
            if (session()->has('errors')) {
                $errors = session()->get('errors');  
                if ($errors->first('email')) {
                    session()->put('error', $errors->first('email'));
                }
            }
            return view('frontend.home.landing');
        }

        $mainBanners = $this->bannerRepo->getBanners__cached('main-banner');
        $allType = $this->contentTypeRepo->all__cached();
        $contentShowcase = [];
        if ($allType && count($allType) > 0) {
            foreach ($allType as $contentType) {
                $contentShowcase[] = [
                    'type' => $contentType,
                    'contents' => $this->contentRepo->getListOf__cached($contentType->code, null, false, 6)
                ];
            }
        }

        view()->share(['pageTitle' => 'Home', 'layoutType' => 'home']);
        return view('frontend.home.index', compact('mainBanners', 'contentShowcase'));
    }

    public function getPattern()
    {
        return view('frontend.home.pattern');
    }

     /**
     * Show Frequently Ask Questions (F.A.Q) Pages
     *
     * @return View
     **/
    public function getFaq($slug = null)
    {
        $faqCategories = $this->faqCategoryRepo->getCachedList();
        $faqs = $this->faqRepo->getCachedList(null);
        view()->share(['pageTitle' => 'FAQ' ]);
        return view('frontend.home.faq', compact('faqs'));
    }

    /**
     * Show Contact Us Pages
     *
     * @return View
     **/
    public function getContactform()
    {
        $contactmessage = new ContactMessage;
        $contactcategory = $contactmessage->getCategoryOptions();
        view()->share(['pageTitle' => 'Kontak Kami']);
        return view('frontend.home.contact')->with('contactmessage', $contactmessage)
                    ->with('contactcategory', $contactcategory);
    }

    /**
     * Process messages entry from Contact Us Pages
     *
     * @return View
     **/
    public function postContactform()
    {
        $param = request()->all();
        $param['status'] = ContactMessage::MESSAGE_CREATED;
        $result = $this->contactMessageRepo->create($param);
        // Return
        if ($result) {
            // notify success
            session()->put('success', 'Your contact message sent! Please wait for administrator responds.');
            return redirect()->route('frontend.home.contactus');
        }
        // notify error
        session()->put('errors', 'Can\'t send your contact message!');
        return redirect()->route('frontend.home.contactus')->withInput($param);
    }
}
