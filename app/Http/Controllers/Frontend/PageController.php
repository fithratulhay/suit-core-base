<?php

namespace App\Http\Controllers\Frontend;

use Suitsite\Repositories\Contract\PageRepositoryContract;
use Suitsite\Repositories\Contract\PageTypeRepositoryContract;
use Suitsite\Repositories\Contract\PageCategoryRepositoryContract;

class PageController extends BaseController
{
    /** ATTRIBUTES **/
    protected $pageRepo;

    /** METHODS / ACTIONS **/
    /**
     * Default constructor.
     **/
    public function __construct(PageRepositoryContract $_pageRepo)
    {
        parent::__construct();
        $this->pageRepo = $_pageRepo;
    }

    /**
     * Show Page Detail.
     *
     * @return View|Redirect
     **/
    public function getPage($slug)
    {
        $page = $this->pageRepo->getPageBy($slug);
        if (!$page) return app()->abort(404);
        view()->share(['pageTitle' => $page->title]);

        return view('frontend.page.static')->with('content', $page);
    }
}
