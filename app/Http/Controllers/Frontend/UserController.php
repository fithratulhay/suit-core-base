<?php

namespace App\Http\Controllers\Frontend;

use Input;
use Redirect;
use View;
use Response;
use Auth;
use App;
use Session;
use Hash;
use Mail;
use Validator;
use Event;
use Email;
use FileGrab;
use Upload;
use Exception;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\OauthUser;
use App\Models\User;
use App\Repositories\UserRepository;
use Suitcore\Models\SuitPushNotificationRegId;

class UserController extends BaseController
{
    use ResetsPasswords;

    protected $userRepo;

    public function __construct(UserRepository $_userRepo)
    {
        parent::__construct();
        $this->userRepo = $_userRepo;
    }

    public function getRegistration()
    {
        return view('frontend.user.registration');
    }

    public function postRegistration(Request $request)
    {
        $rules = [
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirm' => 'required',
        ];

        $this->validate($request, $rules);

        $param = $request->all();
        if ($param['password'] != $param['password_confirm']) return redirect()->back(); // second layer validation

        $date = new Carbon();
        $userData = array_merge($param, [
                        'role' => User::USER,
                        'name' => $param['username'],
                        'password' => bcrypt($param['password']),
                        'registration_date' => $date,
                        'last_visit' => $date,
                        'birthdate' => '1990-01-01', // default birthday
                        'phone_number' => '0000000000', // default phone number
                        'status' => User::STATUS_INACTIVE, // User::STATUS_ACTIVE,
                    ]);
        // Set Up User
        $user = new User();
        $user->fill($userData);
        $prevUser = User::where('email',$user->email)->orWhere('username',$user->username)->first();
        if ($prevUser) {
            Session::put('message', 'Registrasi tidak berhasil! Akun dengan username atau email terkait sudah ada !');
            return redirect()->back()->withInput($request->except('password'));
        }
        $result = $user->save();
        if (!$result) return redirect()->route('frontend.user.registration');
        // Send Email
        if ($user->isActive()) {
            Mail::queue('emails.welcome', [
                'name' => $user->name, 'siteurl' => route('frontend.home')
            ], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Talentsaga | Welcome to Talentsaga');
            });
        } else {
            Mail::queue('emails.activation', [
                'user' => $user,
                'name' => $user->name,
                'activationLink' => $this->generateActivationLink($user),
            ], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Talentsaga | Welcome to Talentsaga');
            });
        }
        // Next Stage
        Session::put('success', 'Registrasi berhasil, silakan login menggunakan akun terkait!');
        // Redirect
        return redirect()->route('frontend.home');
    }

    // PROFILE
    public function getMyProfile() {
        $user = auth()->user();
        return $this->getProfile($user->id);
    }

    public function getProfile($userId) {
        $user = User::find($userId);
        $userProfile = $user->profile;
        if (!$userProfile) {
            if (auth()->user() && $userId == auth()->user()->id) {
                Session::put('error', trans('label.myprofile_notfound'));
            } else {
                Session::put('error', trans('label.profile_notfound'));
            }
            return redirect()->route('frontend.home');
        }
        $userGalleries = $this->userGalleryRepo->getByParameter([
                            'user_id' => $user->id,
                            'perPage' => UserGalleryRepository::FETCH_ALL,
                            'paginate' => false
                        ]);
        return view('frontend.user.profile', compact('user', 'userProfile', 'userGalleries'));
    }

    public function getAccountInformation()
    {
        $user = auth()->user();
        return view('frontend.user.dashboard.account', compact('user', 'userProfile'));
    }

    public function postAccountInformation(Request $request)
    {
        // Init User
        $user = auth()->user();
        // Request Parameter
        $pictureUpdated = false;
        $backgroundUpdated = false;
        $passwordChanged = false;
        if ($request->has('picture')) {
            $param = $request->only(['picture']);
            $pictureUpdated = $this->userRepo->update($user->id, $param, $user);
        }
        if ($request->hasFile('background')) {
            $param = $request->only(['background']);
            $backgroundUpdated = $this->userRepo->update($user->id, $param, $user);
        }
        if ($request->has('current_password') &&
            Hash::check($request->get('current_password'), $user->password) &&
            $request->has('new_password') &&
            $request->has('new_password_confirm') &&
            !empty($request->get('current_password')) &&
            !empty($request->get('new_password')) &&
            ($request->get('new_password') == $request->get('new_password_confirm')) ) {
            $passwordChanged = $this->userRepo->update($user->id, [
                'password' => bcrypt($request->get('new_password'))
            ], $user);
        }
        if ($pictureUpdated || $backgroundUpdated || $passwordChanged) {
            $updatedFields = [];
            if ($pictureUpdated) $updatedFields[] = 'profile picture';
            if ($backgroundUpdated) $updatedFields[] = 'profile background';
            if ($passwordChanged) $updatedFields[] = 'password';

            Session::put('success', 'You have successfully changed your ' . implode(', ', $updatedFields) . '!');

            if ($passwordChanged) {
                Mail::queue('emails.passwordchanged', [
                    'user' => $user,
                    'name' => $user->name
                ], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Your Password has been changed');
                });
            }

            return redirect()->back();
        }
        Session::put('error', 'Nothing changed!');
        return redirect()->back()->withInput(request()->all());
    }

    public function getDashboardPersonal()
    {
        $user = auth()->user();
        $userProfile = $user->profile;
        if (!$userProfile) {
            $userProfile = new UserProfile;
            $userProfile->user_id = $user->id;
        }
        $cities = City::orderBy('country_id', 'asc')->orderBy('name', 'asc')->get();
        return view('frontend.user.dashboard.personal', compact('user', 'userProfile', 'cities'));
    }

    public function postDashboardPersonal(Request $request)
    {
        // Init User
        $user = auth()->user();
        $userProfile = $user->profile;
        if (!$userProfile) {
            Session::put('error', 'Please provide personal data first!');
            return redirect()->route('user.dashboard.personal');
        }
        // Request Parameter
        $userProfileData = $request->all();
        $isDraft = isset($userProfileData['saveasdraft']) ? $userProfileData['saveasdraft'] : false;

        // Talent Category Initiation if Needed
        $firstCat = TalentCategory::first();
        if (!$firstCat) 
            $firstCat = TalentCategory::create([
                'name' => 'Other Category'
            ]);

        // Talent Expertise Initiation if Needed
        $fistExpertise = TalentExpertise::first();
        if (!$fistExpertise) 
            $fistExpertise = TalentExpertise::create([
                'talent_category_id' => $firstCat->id,
                'name' => 'Other Expertise',
                'is_curated' => false
            ]);

        // Talent Profession
        if (!isset($userProfileData['talent_profession']) || empty($userProfileData['talent_profession'])) {
            $userProfileData['talent_profession'] = ($userProfile->talent_profession ? $userProfile->talent_profession : 'Any Profession');
        }

        // Rest of Profile Data
        $userProfileData = array_merge($userProfileData, [
            'user_id' => $user->id,
            'talent_category_id' => ( $userProfile->talent_category_id ? $userProfile->talent_category_id : $firstCat->id ),
            'talent_expertise_id' => ( $userProfile->talent_expertise_id ? $userProfile->talent_expertise_id : $fistExpertise->id ),
            'price_notes' => ($userProfile->price_notes ? $userProfile->price_notes : 'Contact For Price'),
            'contact_for_price' => ($userProfile->contact_for_price ? $userProfile->contact_for_price : true),
            'status' => ($isDraft ? UserProfile::STATUS_DRAFT : UserProfile::STATUS_PUBLISHED)
        ]);

        if (isset($userProfileData['date']) &&
            isset($userProfileData['month']) &&
            isset($userProfileData['year']))
            $userProfileData['birthdate'] = $userProfileData['year'].'-'.str_pad($userProfileData['month'], 2, "0", STR_PAD_LEFT).'-'.str_pad($userProfileData['date'], 2, "0", STR_PAD_LEFT);
        
        /*
        if (isset($userProfileData['country_id']) && $userProfileData['country_id'] && is_numeric($userProfileData['country_id']))
            $userProfileData['country_id'] = str_replace('number:', '', $userProfileData['country_id']);
        else {
            Session::put('error', 'You don\'t pick your country yet!');
            return redirect()->back()->withInput(request()->all());
        }
        */

        if (isset($userProfileData['city_id']) && $userProfileData['city_id'] && is_numeric($userProfileData['city_id']))
            $userProfileData['city_id'] = str_replace('number:', '', $userProfileData['city_id']);
        else {
            Session::put('error', 'You don\'t pick your city yet!');
            return redirect()->back()->withInput(request()->all());
        }
        // Set Up User Profile
        $userProfile = $user->profile;
        if (!$userProfile) {
            $userProfile = new UserProfile;
        }
        // dd($userProfileData);
        $userProfile->fill($userProfileData);
        $result = $userProfile->save();
        if (!$result) {
            Session::put('error', 'Error when save personal data!');
            return redirect()->back()->withInput(request()->all());
        }
        // Update User
        $userProfileData['status'] = User::STATUS_ACTIVE;
        $user->fill($userProfileData);
        $result = $user->save();
        if (!$result) {
            Session::put('error', 'Error when save personal data!');
            return redirect()->back()->withInput(request()->all());
        }
        // Next Stage - Redirect
        Session::put('success', 'Personal data saved!');
        return redirect()->back();
    }

    // OTHERS
    protected function generateActivationLink($user)
    {
        $code = $user->generateActivationCode();
        $id = $user->id;

        return route('frontend.user.activation', compact('id', 'code'));
    }

    public function produceHashCode($id)
    {
        $user = User::find($id);

        $created_at = strtotime($user->created_at);
        $formatted_time = date('dmYhis', $created_at);

        $hashed = $user->username.'-created'.$formatted_time.'-'.$user->email;
        $hashcode = md5($hashed);

        return $hashcode;
    }

    public function getForgetPassword()
    {
        view()->share('pageTitle', 'Forgot Password');
        return View::make('frontend.user.forgetpassword');
    }

    public function postForgetPassword(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    public function getConfirmResetPassword(Request $request, $token = null)
    {
        $this->resetView = 'frontend.user.confirmforgetpassword';
        return $this->showResetForm($request, $token);
    }

    public function postConfirmResetPassword(Request $request)
    {
        $this->redirectPath = route('sessions.login');
        return $this->reset($request);
    }

    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        // Email Notify
        try {
            Mail::queue('emails.passwordchanged', [
                'name' => $user->name
            ], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Your Password has been changed');
            });
        } catch (Exception $e) { }

        Auth::guard($this->getGuard())->login($user);
        return redirect()->route('sessions.login');
    }

    /**
     * Get the response for after a successful password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        session(['message' => 'Successfully Reset Your Password']);
        return redirect($this->redirectPath())->with('status', trans($response));
    }

    public function getUserActivation(Request $request)
    {
        $email = $request->get('email');
        
        if (!$email) {
            return view('frontend.user.reactivationForm', compact('email'));
        }

        $user = User::where('email', $email)
                ->where('status', User::STATUS_INACTIVE)
                ->first();

        if ($user && $code = $user->generateActivationCode()) {
            $email = $user->email;
            Mail::queue('emails.reactivation', [
                'user' => $user,
                'name' => $user->name,
                'activationLink' => $this->generateActivationLink($user),
            ], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Talentsaga | Reactivate Your Account');
            });
            return view('frontend.user.reactivationSuccess', compact('email'));
        }
        return view('frontend.user.reactivationFailed', compact('email'));
    }

    public function activateAccount($id, $hashcode)
    {
        $user = User::find($id);
        if ($user == null) {
            Session::put('salahaktivasi', true);
            return Redirect::route('frontend.user.activation.failed');
        } else {
            if ($user->matchActivationCode($hashcode)) {
                // Change user status
                $user->status = User::STATUS_ACTIVE;
                $user->save();
                Session::put('aktivasi', true);

                // Send Welcome Email
                Mail::queue('emails.welcome', [
                    'name' => $user->name
                ], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Talentsaga | Welcome to Talentsaga');
                });

                // Redirect to Activation Success
                return Redirect::route('frontend.user.activation.success');
            } else {
                Session::put('salahaktivasi', true);
                return Redirect::route('frontend.user.activation.failed');
            }
        }
    }

    public function activateAccountSuccess()
    {
        view()->share('pageTitle', 'Activation Success');
        return view('frontend.user.activationSuccess');
    }

    public function activateAccountFailed()
    {
        view()->share('pageTitle', 'Activation Failed');
        return view('frontend.user.activationFailed');
    }

    public function postPushNotificationRegistration() {
        $user = auth()->user();
        $postRegistrationId = request()->get('registration_id', '');
        $deviceType = request()->get('device_type', '');
        $data = [
            'result' => 'failed'
        ];
        if ($user && $postRegistrationId) {
            $savedRegId = SuitPushNotificationRegId::where('registration_id', $postRegistrationId)->first();
            if ($savedRegId) {
                if ($savedRegId->user_id == $user->id) {
                    $data['result'] = 'success';
                } 
            } else {
                $savedRegId = SuitPushNotificationRegId::fill([
                    'user_id' => $user->id, 
                    'registration_id' => $postRegistrationId, 
                    'device_type' => $deviceType, 
                    'last_used' => Carbon::now()->toDateString()
                ]);
                if ($savedRegId->save()) {
                    $data['result'] = 'success';
                }
            }
        }
        return response()->json($data);
    }
}
