<?php

namespace App\Http\Controllers\Frontend;

use View;
use Redirect;
use App;
use Input;
use Suitsite\Models\NewsletterSubscriber;
use Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract;

class NewsletterController extends BaseController {
    /** ATTRIBUTES **/
    protected $newsletterSubscriberRepo;

    /** METHODS / ACTIONS **/
    public function __construct(NewsletterSubscriberRepositoryContract $_newsletterSubscriberRepo)
    {
        parent::__construct();
        $this->newsletterSubscriberRepo = $_newsletterSubscriberRepo;
    }

    /**
     * Save subscribing or save enable news letter settings
     *
     * @return Redirect
     **/
    public function updateSubscriber(){
        $user = auth()->user();

        $rules = [
            'email' => 'required|email'
        ];
        $this->validate(request(), $rules);
        $param = request()->all();

        if ($user) {
            if (!isset($param['email']) || empty($param['email'])) {
                $param['email'] = $user->email;
                $param['name'] = $user->name;   
            }
        } 

        if (isset($param['email']) && !empty($param['email'])) {
            $newsletterSubscriber = $this->newsletterSubscriberRepo->getByEmail($param['email']);
            if (!$newsletterSubscriber) {
                $newsletterSubscriber = new NewsletterSubscriber;
                if (!isset($param['name']) || empty($param['name'])) {
                    $param['name'] = explode('@', $param['email'])[0];
                }
                $result = $this->newsletterSubscriberRepo->create($param, $newsletterSubscriber);
                if ($result) {
                    // notify
                    $this->notify('success', "Alamat Email anda telah ditambahkan sebagai langganan newsletter kami!");
                } else {
                    // notify
                    $this->notify('danger', "Terjadi sesuatu pada sistem sehingga gagal menyimpan alamat email anda, silahkan coba lagi!");
                }
            } else {
                // notify already subscriber
                $this->notify('info', 'Alamat email anda sebelumnya telah berlangganan!');
            }
        } else {
            $this->notify('danger', 'Anda belum memasukkan alamat email anda!');
        }

        return Redirect::route('frontend.home');   
    }
}
