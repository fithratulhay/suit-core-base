<?php

namespace App\Http\Controllers\Frontend;

use Suitcore\Controllers\FrontendController;

class BaseController extends FrontendController
{
    // ATTRIBUTES

    // METHODS
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function notify($type, $msg = null)
    {
        $msg && session()->put($type, $msg);
    }

    public function notifySuccess($msg = null)
    {
        $this->notify('success', $msg);
    }

    public function notifyInfo($msg = null)
    {
        $this->notify('info', $msg);
    }

    public function notifyDanger($msg = null)
    {
        $this->notify('danger', $msg);
    }
}
