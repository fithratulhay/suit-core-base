<?php

namespace Suitpay\Jobs;

use DB, App, Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use Suitpay\Models\Contracts\PayableContract;
use Suitpay\Repositories\ThirdpartyPaymentProcessRepository;
use Suitpay\Controllers\PaymentController;
use Suitpay\ThirdpartyAPI\Doku\Doku;

class DokuCheck implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $invoiceNumber;
    protected $sessionId;
    protected $paymentProcessRepo;
    protected $paymentController;

    /**
     * Create a new job instance.
     *
     * @param  Podcast  $podcast
     * @return void
     */
    public function __construct($_invoiceNumber, $_sessionId)
    {
        $this->invoiceNumber = $_invoiceNumber;
        $this->sessionId = $_sessionId;
        $this->paymentProcessRepo = new ThirdpartyPaymentProcessRepository;
        $this->paymentController = new PaymentController($this->paymentProcessRepo, [
                [
                    'code' => PaymentController::DOKU,
                    'isProduction' => env('DOKU_IS_PRODUCTION', false),
                    'serverKey' => env('DOKU_SHARED_KEY', '1lk8kCqRZ40M'),
                    'mallId' => env('DOKU_MALL_ID', '4222'),
                    'ccInstallmentMallId' => env('DOKU_MALL_ID_CCINTALLMENT', '2473'),
                    'successRedirectUrl' => '/'
                ]
            ], 'frontend.home');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Doku::$sharedKey = env('DOKU_SHARED_KEY', '1lk8kCqRZ40M');
        Doku::$isProduction = env('DOKU_IS_PRODUCTION', false);
        Doku::$mallId = env('DOKU_MALL_ID', '4222');
        Doku::$ccInstallmentMallId = env('DOKU_MALL_ID_CCINTALLMENT', '2473');

        $postParam = [
            'TRANSIDMERCHANT' => $this->invoiceNumber,
            'SESSIONID' => $this->sessionId
        ];
        $resultMessage = 'INVALID';
        $verifyStatus = 'INVALID';
        $responseCode = '5514';
        $relatedOrderCode = str_replace('_', '/', $this->invoiceNumber);
        $relatedOrderClassObject = new PayableContract();
        $relatedOrder = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$relatedOrderCode)->first();
        try {
            // BEGIN CHECK STATUS AND MUTATOR PREVIOUS PARAM REQUEST
            if ($relatedOrder &&
                $paymentMethod = $relatedOrder->getPaymentMethod() &&
                $paymentMethod->thirdparty_module_enabled_payment == Doku::CREDITCARDINSTALLMENT) {
                Doku::$mallId = Doku::$ccInstallmentMallId;
            }
            $checkStatusWords = Doku::doCreateWordsCustom([
                'MALLID' => Doku::$mallId,
                'SHAREDKEY' => Doku::$sharedKey,
                'TRANSIDMERCHANT' => $this->invoiceNumber
            ]);
            $checkStatusData = [
                'MALLID' => Doku::$mallId,
                'CHAINMERCHANT' => 'NA',
                'TRANSIDMERCHANT' => $this->invoiceNumber,
                'SESSIONID' => $this->sessionId,
                'WORDS' => $checkStatusWords
            ];
            info(__CLASS__.'@'.__METHOD__, compact('checkStatusData'));
            $checkStatusResponse = Doku::doCheckStatus($checkStatusData);
            info(__CLASS__.'@'.__METHOD__, compact('checkStatusResponse'));
            $xmlObj = simplexml_load_string($checkStatusResponse);
            $xmlObj = json_decode(json_encode($xmlObj),1);
            // dd($xmlObj['RESPONSECODE']);
            $postParam = $xmlObj;
            $resultMessage = $xmlObj['RESULTMSG'];
            $verifyStatus = $xmlObj['VERIFYSTATUS'];
            $responseCode = $xmlObj['RESPONSECODE'];
            /*
            <PAYMENT_STATUS> 
                <AMOUNT></AMOUNT>
                <TRANSIDMERCHANT></TRANSIDMERCHANT>
                <WORDS></WORDS>               
                <RESPONSECODE></RESPONSECODE>
                <APPROVALCODE></APPROVALCODE>
                <RESULTMSG></RESULTMSG>
                <PAYMENTCHANNEL></PAYMENTCHANNEL>
                <PAYMENTCODE></PAYMENTCODE>
                <SESSIONID></SESSIONID>
                <BANK></BANK>
                <MCN></MCN>
                <PAYMENTDATETIME></PAYMENTDATETIME>
                <VERIFYID></VERIFYID>
                <VERIFYSCORE></VERIFYSCORE>
                <VERIFYSTATUS></VERIFYSTATUS>
            </PAYMENT_STATUS>
            */
            // END CHECK STATUS
        } catch (Exception $e) {
            $resultMessage = 'INVALID';
            $verifyStatus = 'INVALID';
            $responseCode = '5514';
            info(__CLASS__.'@'.__METHOD__, compact('e'));
        }
        // Merchant process ...
        if ($resultMessage != 'SUCCESS' && $verifyStatus != 'APPROVE') {
            $responseCode = '5514'; 
        }
        $this->paymentController->dokuMerchantProcess($relatedOrder, $this->sessionId, $responseCode, json_encode($postParam));
    }
}
