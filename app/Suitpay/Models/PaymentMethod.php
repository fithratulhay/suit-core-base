<?php

namespace Suitpay\Models;

use File;
use Suitcore\Models\SuitModel;
use Suitpay\Config\DefaultConfig;
use Suitpay\ThirdpartyAPI\Doku\Doku;
use Suitpay\ThirdpartyAPI\Veritrans\Veritrans;
use Suitpay\ThirdpartyAPI\Ipay88\Ipay88;

/*
|--------------------------------------------------------------------------
| payment_method Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * bank_id INT(10) NULL
| * bank_name VARCHAR(45) NULL
| * bank_branch VARCHAR(45) NOT NULL
| * account_name VARCHAR(45) NOT NULL
| * account_number VARCHAR(100) NOT NULL
| * title VARCHAR(45) NOT NULL
| * type VARCHAR(45)
| * admin_fee DOUBLE(15,2)
| * optional_fixed_fee DOUBLE(15,2) NULL
| * optional_percentage_fee DOUBLE(15,2) NULL
| * tax DOUBLE(15,2) NULL
| * status VARCHAR(45) NOT NULL
| * image VARCHAR(45)
| * description TEXT
| * thirdparty_module_code TEXT
| * thirdparty_module_enabled_payment TEXT
| * thirdparty_module_installment_setting TEXT
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class PaymentMethod extends SuitModel
{
    /*
     * List of payment method type.
     */
    const ESCROW = "escrow";
    const TRANSFER = "manualtransfer";
    const INHOUSELEASING = "inhouseleasing";
    const THIRDPARTY = "thirdparty";
    const OFFLINE = "offline";
    const ACTIVE = "active";
    const INACTIVE = "inactive";

    // MODEL DEFINITION
    public $table = 'payment_methods';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'bank_name',
        'bank_id',
        'bank_branch',
        'account_number',
        'account_name',
        'title',
        'type',
        'admin_fee',
        'optional_fixed_fee',
        'optional_percentage_fee',
        'tax',
        'status',
        'image',
        'description',
        'thirdparty_module_code',
        'thirdparty_module_enabled_payment',
        'thirdparty_module_installment_setting'
    ];

    public $rules = [
        'title' => 'required',
        'status' => 'required',
    ];

    protected $imageAttributes = [
        'image' => 'paymentmethod_pictures',
    ];

    protected $files = [
        'image' => 'paymentmethod_pictures',
    ];

    protected $extendedThumbnailStyle = [
        'logo_small' => '_x50'
    ];

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function getBankNameAttribute() {
        return $this->bank ? $this->bank->name : $this->getAttributeFromArray('bank_name');
    }

    public function getFullBankNameAttribute() {
        return ($this->bank ? $this->bank->name : $this->bank_name) . ' ' . $this->account_number;
    }

    public function getFullTitleAttribute() {
        return $this->title . ($this->type == self::TRANSFER ? ' : '.$this->full_bank_name : '');
    }

    // LOCAL SCOPE
    /**
     * Scope a query to include only active payment method.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * Scope a query to include only the active bank transfer method.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeTransferBank($query)
    {
        return $query->where('status', static::ACTIVE)
                     ->where('type', static::TRANSFER);
    }

    public function scopeThirdParty($query)
    {
        return $query->where('status', static::ACTIVE)
                     ->where('type', static::THIRDPARTY);
    }

    public function scopeAllThirdParty($query)
    {
        return $query->where('type', static::THIRDPARTY);
    }

    // SERVICES
    /**
     * Get the payment method name.
     * @param int $methodId
     * @return string
     */
    public static function getName($methodId)
    {
        $paymentMethod = PaymentMethod::find($methodId);
        if ($paymentMethod) {
            return $paymentMethod->title;
        } else {
            return "Nama payment tidak ditemukan";
        }
    }

    /**
     * Get valid path for payment method image.
     * @return string|bool
     */
    public function getValidImagePath()
    {
        if ($this->image != null && !empty($this->image)) {
            $targetPath = AppConfig::upload_path() . '/paymentmethod_pictures/' . $this->image;
            if (File::exists($targetPath)) {
                return asset('uploads/paymentmethod_pictures/' . $this->image);
            }
        }
        return false;
    }

    /**
     * Get options of payment method type.
     */
    public function getTypeOptions()
    {
        $config = DefaultConfig::getConfig();
        $options = [];
        if (isset($config['payment'])) {
            if (isset($config['payment'][self::ESCROW]) && $config['payment'][self::ESCROW])
                $options[self::ESCROW] = "ESCROW (Deposit)";
            if (isset($config['payment'][self::TRANSFER]) && $config['payment'][self::TRANSFER])
                $options[self::TRANSFER] = "Manual Bank Transfer";
            if (isset($config['payment'][self::INHOUSELEASING]) && $config['payment'][self::INHOUSELEASING])
                $options[self::INHOUSELEASING] = "In House Leasing";
            if (isset($config['payment'][self::THIRDPARTY]) && $config['payment'][self::THIRDPARTY])
                $options[self::THIRDPARTY] = "Third Party";
        }
        return $options;
    }

    /**
     * Get options of payment method status.
     */
    public function getStatusOptions()
    {
        return [
            self::ACTIVE => ucfirst(strtolower(self::ACTIVE)),
            self::INACTIVE => ucfirst(strtolower(self::INACTIVE))
        ];
    }

    public function getThirdpartyModule()
    {
        $module = ThirdPartyModule::where('code','=',$this->thirdparty_module_code)->first();
        return $module;
    }

    public function getLabel()
    {
        return "Payment Method";
    }

    public function getFormattedValue()
    {
        return $this->full_title;
    }

    public function getFormattedValueColumn() {
        return ['title', 'account_number'];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getDefaultOrderColumn() {
        return 'title';
    }

    public function getDefaultOrderColumnDirection() {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title"
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Type",
                "options" => $this->getTypeOptions()
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "filterable" => true
            ],
            "bank_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => 'bank',
                "label" => "Destination Bank Name",
                "options" => (new Bank)->all()->pluck('name','id')
            ],
            "bank_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Destination Custom Bank Name"
            ],
            "bank_branch" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Destination Bank Branch"
            ],
            "account_name" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Destination Account Name"
            ],
            "account_number" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Destination Account Number"
            ],
            "admin_fee" => [
                "type" => self::TYPE_FLOAT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Admin Fee (". DefaultConfig::getConfig()['metrics']['currency'] .")"
            ],
            "optional_fixed_fee" => [
                "type" => self::TYPE_FLOAT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Fixed Payment Fee (". DefaultConfig::getConfig()['metrics']['currency'] .")"
            ],
            "optional_percentage_fee" => [
                "type" => self::TYPE_FLOAT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Percentage Payment Fee (%)"
            ],
            "tax" => [
                "type" => self::TYPE_FLOAT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Tax (". DefaultConfig::getConfig()['metrics']['currency'] .")"
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Image"
            ],
            "description" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Description"
            ],
            "thirdparty_module_code" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Thirdparty Module Code",
                "options" => ['veritrans' => 'VERITRANS', 'doku' => 'DOKU']
            ],
            "thirdparty_module_enabled_payment" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Thirdparty Module Enabled Payment",
                "options" => array_merge(Doku::getPaymentTypeOptions(), Veritrans::getPaymentTypeOptions())
            ],
            "thirdparty_module_installment_setting" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Thirdparty Module Installment Setting"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    /**
     * Get total payment fee of related payment method
     * @param  int $orderTotalPrice
     * @return int|NULL
     */
    public function getTotalPaymentFee($orderTotalPrice = 0)
    {
        $optionalPercentagetoFix = 0;
        $feeAccumulation = ($this->admin_fee + $this->optional_fixed_fee + $this->tax);
        if ($this->optional_percentage_fee && $this->optional_percentage_fee < 100) {
            /*
            // $optionalPercentagetoFix = ($orderTotalPrice * 100 / (100 - $this->optional_percentage_fee) + $this->admin_fee) - ($orderTotalPrice + $this->admin_fee);
            $optionalPercentagetoFix = ($orderTotalPrice * 100 / (100 - $this->optional_percentage_fee) + $feeAccumulation) - ($orderTotalPrice + $this->admin_fee);
            */
            $optionalPercentagetoFix = ((($orderTotalPrice + $feeAccumulation) * 100) / (100 - $this->optional_percentage_fee)) - ($orderTotalPrice + $feeAccumulation);
        }
        $total = $feeAccumulation + ceil($optionalPercentagetoFix);
        //if ($total < $this->minimum_admin_fee) {
        //    return $this->minimum_admin_fee;
        //}
        return round($total);
    }
}
