<?php

namespace Suitpay\Models\Contracts;

interface OptionalOrderPurchasedItemContract
{
    public function getItemName();

    public function getList($orderId);

    public function getAmount();
}