<?php

namespace Suitpay\Models\Contracts;

interface PayableContract
{
    public function getOrderCode();

    public function getOrderDatetime();

    public function getOrderExpiryInMinutes();

    public function getTotalPrice();

    public function getRecipientBirthdate();

    public function getRecipientName();

    public function getRecipientPhone();

    public function getRecipientEmail();

    public function getRecipientStreetName();

    public function getRecipientCity();

    public function getRecipientProvince();

    public function getRecipientCountry();

    public function getRecipientZipcode();

    public function getBillingRecipientName();

    public function getBillingRecipientPhone();

    public function getBillingRecipientEmail();

    public function getBillingRecipientStreetName();

    public function getBillingRecipientCity();

    public function getBillingRecipientProvince();

    public function getBillingRecipientCountry();

    public function getBillingRecipientZipcode();

    public function getPaymentMethod();

    public function getOrderItems();

    public function getOrderFullClassName();

    public function getOrderCodeField();

    public function getPaymentMethodObject();

    public function getPaymentOption();

    public function successPayment();

    public function pendingPayment();

    public function failedPayment();

    public function refundPayment();

    public function cancelPayment();

    public function reinitPayment();
}