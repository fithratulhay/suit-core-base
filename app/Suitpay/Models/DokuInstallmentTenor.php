<?php

namespace Suitpay\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;

/*
|--------------------------------------------------------------------------
| doku_installment_tenors Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * doku_bank_id INT(10) NOT NULL
| * term INT(11) NOT NULL
| * interest DOUBLE(15,2) NOT NULL
| * description VARCHAR(255)
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class DokuInstallmentTenor extends SuitModel
{
    // MODEL DEFINITION
    public $table = 'doku_installment_tenors';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'doku_bank_id',
        'term',
        'interest',
        'description'
    ];

    public $rules = [
        'doku_bank_id' => 'required',
        'term' => 'required',
        'interest' => 'required'
    ];
    
    // VIRTUAL ATTRIBUTES
    public function getFullNameAttribute() {
        return $this->getFormattedValue();
    }

    // RELATIONSHIP
    public function dokuBank()
    {
        return $this->belongsTo(DokuBank::class, 'doku_bank_id');
    }

    // SERVICES
    public function getMonthlyPayment($price) {
        $interestNominal = $this->interest ? (($this->interest / 100) * $price) : 0;
        return intval(ceil(($price + $interestNominal) / $this->term));
    }

    public function getLabel()
    {
        return 'Doku Installment Tenor';
    }

    public function getFormattedValue()
    {
        return $this->term . " Month (" . $this->interest . "%)";
    }

    public function getFormattedValueColumn()
    {
        return ['doku_bank_id', 'term', 'interest'];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getDefaultOrderColumn() {
        return 'id';
    }

    public function getDefaultOrderColumnDirection() {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'doku_bank_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => 'dokuBank',
                'label' => 'Doku Bank',
                'options' => (new DokuBank)->get()->pluck('fullName', 'id')
            ],
            'term' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Term (Month)'
            ],
            'interest' => [
                'type' => self::TYPE_FLOAT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Interest (%)'
            ],
            'description' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'Plan ID'
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();
    }
}

