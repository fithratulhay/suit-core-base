<?php

namespace Suitpay\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;
use Suitpay\Models\Bank;

/*
|--------------------------------------------------------------------------
| Standard Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * code VARCHAR(255) NOT NULL
| * name VARCHAR(255) NOT NULL
| * bank_id INT(10)
| * image
| * status
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class PaymentGatewayBank extends SuitModel
{
    const ACTIVE = "active";
    const INACTIVEVISIBLE = "visible";
    const INACTIVE = "inactive";

    // MODEL DEFINITION
    public $table = 'doku_banks';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'code',
        'name',
        'bank_id',
        'image',
        'status'
    ];

    public $rules = [
        'code' => 'required',
        'name' => 'required',
        'status' => 'required'
    ];

    protected $imageAttributes = [
        'image' => 'dokubank_pictures',
    ];

    protected $files = [
        'image' => 'dokubank_pictures',
    ];

    protected $extendedThumbnailStyle = [
        'logo_small' => '_x50',
        'logo_footer_small' => '_x32'
    ];

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function installmentTenor()
    {
        return $this->hasMany(InstallmentTenor::class, 'doku_bank_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', static::ACTIVE);
    }

    public function scopeVisible($query)
    {
        return $query->whereIn('status', [static::ACTIVE, static::INACTIVEVISIBLE]);
    }

    // SERVICES
    public function getFullNameAttribute() {
        return $this->getFormattedValue();
    }

    public function getLabel()
    {
        return 'Doku Credit Card';
    }

    public function getFormattedValue()
    {
        return $this->name . " (" . $this->code . ")";
    }

    public function getFormattedValueColumn()
    {
        return ['name',' code'];
    }

    public function getStatusOptions()
    {
        return [
            self::ACTIVE => ucfirst(strtolower(self::ACTIVE)),
            self::INACTIVEVISIBLE => "Inactive but Visible For Customer",
            self::INACTIVE => ucfirst(strtolower(self::INACTIVE))
        ];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getDefaultOrderColumn() {
        return 'name';
    }

    public function getDefaultOrderColumnDirection() {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'code' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Acquirer Code'
            ],
            'name' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Bank Name'
            ],
            'bank_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => false,
                'formdisplay' => true,
                'required' => true,
                'relation' => 'bank',
                'label' => 'Masterdata Bank References',
                'options' => (new Bank)->get()->pluck('name', 'id')
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Image"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions()
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();
    }
}
