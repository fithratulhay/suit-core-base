<?php

namespace Suitpay\Models;

use Suitcore\Models\SuitModel;
use Illuminate\Database\Eloquent\Model;

class ThirdPartyModule extends SuitModel
{
    public $table = 'thirdparty_modules';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'name',
        'code',
        'description',
        'api_id', // merchantID
        'api_token', // clientKey
        'api_secret' // serverKey
    ];

    public $rules = [
        'name' => 'required',
        'code' => 'required|unique:thirdparty_modules|alpha_num'
    ];

    public function getLabel()
    {
        return "Third Party Payment";
    }

    public function getFormattedValue()
    {
        return $this->id;
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getAttributeSettings()
    {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "name" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Name"
            ],
            "code" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Code",
                "options" => ['veritrans' => 'Midtrans / Veritrans']
            ],
            "description" => [
                "type" => self::TYPE_TEXTAREA,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Description"
            ],
            "api_id" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "API Merchant ID"
            ],
            "api_token" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "API Client Key"
            ],
            "api_secret" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "API Server Key"
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ],
        ];
    }
}
