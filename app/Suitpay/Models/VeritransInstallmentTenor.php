<?php

namespace Suitpay\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;

/*
|--------------------------------------------------------------------------
| installment_tenors Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * veritrans_bank_id INT(10) NOT NULL
| * term INT(11) NOT NULL
| * interest DOUBLE(15,2) NOT NULL
| * description VARCHAR(255)
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class VeritransInstallmentTenor extends SuitModel
{
    // MODEL DEFINITION
    public $table = 'veritrans_installment_tenors';
    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'veritrans_bank_id',
        'term',
        'interest',
        'description'
    ];

    public $rules = [
        'veritrans_bank_id' => 'required',
        'term' => 'required',
        'interest' => 'required'
    ];

    public function veritransBank()
    {
        return $this->belongsTo(VeritransBank::class, 'veritrans_bank_id');
    }

    // SERVICES
    public function getFullNameAttribute() {
        return $this->getFormattedValue();
    }

    public function getLabel()
    {
        return 'Installment Tenor';
    }

    public function getFormattedValue()
    {
        return $this->term . " Month (" . $this->interest . "%)";
    }

    public function getFormattedValueColumn()
    {
        return ['veritrans_bank_id', 'term', 'interest'];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getDefaultOrderColumn() {
        return 'id';
    }

    public function getDefaultOrderColumnDirection() {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'veritrans_bank_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => 'veritransBank',
                'label' => 'Veritrans Bank',
                'options' => (new VeritransBank)->get()->pluck('fullName', 'id')
            ],
            'term' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Term (Month)'
            ],
            'interest' => [
                'type' => self::TYPE_FLOAT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Interest (%)'
            ],
            'description' => [
                'type' => self::TYPE_TEXTAREA,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => null,
                'label' => 'Description'
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();
    }
}

