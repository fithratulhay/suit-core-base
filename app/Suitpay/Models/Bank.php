<?php

namespace Suitpay\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;
use Suitpay\Config\DefaultConfig;

/*
|--------------------------------------------------------------------------
| couriers Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * code VARCHAR(255) NOT NULL
| * name VARCHAR(255) NOT NULL
| * accounting_code VARCHAR(255) NULL
| * thirdparty_code VARCHAR(24) NULL
| * type VARCHAR(45) NULL
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Bank extends SuitModel
{
    const ACTUAL = 'actual';
    const VIRTUAL = 'virtual';

    // MODEL DEFINITION
    public $table = 'banks';
    protected static $bufferAttributeSettings = null;
    protected $attributeSettingsCustomState = null;

    public $fillable = [
        'code',
        'name'
    ];

    public $rules = [
        'code' => 'required|unique:banks,code',
        'name' => 'required'
    ];

    // SERVICES
    public function getFullNameAttribute() {
        return $this->getFormattedValue();
    }

    public function getLabel()
    {
        return 'Bank';
    }

    public function getFormattedValue()
    {
        return $this->name . " (" . $this->code . ")";
    }

    public function getFormattedValueColumn()
    {
        return ['name',' code'];
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getThirdPartyCodeOptions()
    {
        $default = isset(DefaultConfig::getConfig()['thirdparty_bank_code']) ? DefaultConfig::getConfig()['thirdparty_bank_code'] : null;

        if ($default) {
            return $default;
        }

        return [];
    }

    public function getDefaultOrderColumn() {
        return 'name';
    }

    public function getDefaultOrderColumnDirection() {
        return 'asc';
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'code' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Code (ATM Bersama)'
            ],
            'name' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Bank Name'
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forever('bank_masterdata_list', $model->getOptions());
        });

        static::deleted(function ($model) {
            Cache::forever('bank_masterdata_list', $model->getOptions());
        });
    }
}
