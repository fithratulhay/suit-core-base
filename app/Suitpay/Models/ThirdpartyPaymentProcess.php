<?php

namespace Suitpay\Models;

use Mail;
use Carbon\Carbon;
use Suitcore\Models\SuitModel;

/*
|--------------------------------------------------------------------------
| thirdparty_payment_processes Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * transaction_id TINYTEXT NOT NULL
| * thirdparty_module_id INT NULL FOREIGN KEY
| * order_code TEXT NULL
| * status TEXT NOT NULL ['success', 'pending', 'failed', 'refund', 'cancel', 'reinit']
| * json_result TEXT
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class ThirdpartyPaymentProcess extends SuitModel
{
    const STATUS_SUCCESS = 'success';
    const STATUS_PENDING = 'pending'; // For Long Time Settlement
    const STATUS_FAILED = 'failed';
    const STATUS_REFUND = 'refund'; // From Success become Refund if invalid stock
    const STATUS_CANCEL = 'cancel';
    const STATUS_REINIT = 'reinit';

    public $table = 'thirdparty_payment_processes';

    public $fillable = [
        'transaction_id',
        'thirdparty_module_id',
        'order_code',
        'status',
        'json_result'
    ];

    public $rules = [
        'transaction_id' => 'required',
        'status' => 'required',
    ];

    // RELATIONSHIP
    /**
     * Get thirdparty module
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function thirdpartyModule()
    {
        return $this->belongsTo(ThirdPartyModule::class, 'thirdparty_module_id');
    }

    /**
     * Get options of order status.
     */
    public function getStatusOptions()
    {
        return [
            self::STATUS_SUCCESS => ucfirst(strtolower(self::STATUS_SUCCESS)),
            self::STATUS_PENDING => ucfirst(strtolower(self::STATUS_PENDING)),
            self::STATUS_FAILED => ucfirst(strtolower(self::STATUS_FAILED)),
            self::STATUS_REFUND => ucfirst(strtolower(self::STATUS_REFUND)),
            self::STATUS_CANCEL => ucfirst(strtolower(self::STATUS_CANCEL)),
            self::STATUS_REINIT => ucfirst(strtolower(self::STATUS_REINIT))
        ];
    }

    public function getLabel()
    {
        return 'Thirdparty Payment Process';
    }

    public function getFormattedValue()
    {
        return ($this->thirdpartyModule ? $this->thirdpartyModule->code . " - " : "") . $this->transaction_id;
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'readonly' => true,
                'label' => 'ID'
            ],
            "transaction_id" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                'readonly' => true,
                "relation" => null,
                "label" => "Transaction ID"
            ],
            "thirdparty_module_id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                'readonly' => true,
                "relation" => "thirdpartyModule",
                "label" => "Thirdparty Module",
                "options" => [] // Readonly Mode, options not needed
            ],
            "order_code" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                'readonly' => true,
                "label" => "Transaction Number"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "filterable" => true,
                "options" => $this->getStatusOptions()
            ],
            "json_result" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                'readonly' => true,
                "relation" => null,
                "label" => "JSON Result"
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => true,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At',
                'filterable' => true
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }

    // Boot
    protected static function boot()
    {
        // Run Parent First
        parent::boot();
    }
}
