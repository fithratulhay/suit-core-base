<?php

namespace Suitpay\Models;

use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| veritrans_banks Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * code VARCHAR(255) NOT NULL
| * name VARCHAR(255) NOT NULL
| * bank_id INT(10)
| * image
| * status
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class VeritransBank extends PaymentGatewayBank
{
    // MODEL DEFINITION
    public $table = 'veritrans_banks';
    protected static $bufferAttributeSettings = null;

    public function installmentTenor()
    {
        return $this->hasMany(InstallmentTenor::class, 'veritrans_bank_id');
    }

    public function getLabel()
    {
        return 'Midtrans Credit Card';
    }

    protected static function boot()
    {
        parent::boot();
    }
}
