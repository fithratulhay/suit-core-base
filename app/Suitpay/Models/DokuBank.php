<?php

namespace Suitpay\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;

/*
|--------------------------------------------------------------------------
| doku_banks Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * code VARCHAR(255) NOT NULL
| * name VARCHAR(255) NOT NULL
| * bank_id INT(10)
| * image
| * status
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class DokuBank extends PaymentGatewayBank
{
    public $table = 'doku_banks';
    protected static $bufferAttributeSettings = null;

    protected $imageAttributes = [
        'image' => 'dokubank_pictures',
    ];

    protected $files = [
        'image' => 'dokubank_pictures',
    ];

    public function installmentTenor()
    {
        return $this->hasMany(DokuInstallmentTenor::class, 'doku_bank_id');
    }

    public function getLabel()
    {
        return 'Doku Credit Card';
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forever('doku_creditcard_footer_list', DokuBank::visible()->orderBy('name', 'asc')->get());
            Cache::forever('doku_creditcard_active_list', DokuBank::active()->orderBy('name', 'asc')->pluck('name', 'id'));
            Cache::forever('raw_doku_creditcard_active_list', DokuBank::active()->orderBy('name', 'asc')->get());
        });

        static::deleted(function ($model) {
            Cache::forever('doku_creditcard_footer_list', DokuBank::visible()->orderBy('name', 'asc')->get());
            Cache::forever('doku_creditcard_active_list', DokuBank::active()->orderBy('name', 'asc')->pluck('name', 'id'));
            Cache::forever('raw_doku_creditcard_active_list', DokuBank::active()->orderBy('name', 'asc')->get());
        });
    }
}
