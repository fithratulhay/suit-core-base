<?php

namespace Suitpay\Config;

use Suitpay\Models\PaymentMethod;

class DefaultConfig
{
    public static $data = [
        "metrics" => [
            "currency" => "Rp",
            "quantity" => "pcs"
        ],
        "payment" => [
            PaymentMethod::THIRDPARTY => true
        ]
    ];

    public static function getConfig()
    {
        $instancesConfig = [];
        // priority 1 (backward-compatibility with old-version)
        if (class_exists('\\App\\Config\\SuitpayConfig')) {
            if (is_array( \App\Config\SuitpayConfig::$data )) {
                $instancesConfig = \App\Config\SuitpayConfig::$data;
            }
        }
        // priority 2
        $suitpayConfig = config('suitpay');
        // priority 3 (default value) : self::$data
        return array_merge( self::$data, $suitpayConfig, $instancesConfig );
    }
}
