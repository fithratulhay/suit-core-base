<?php

namespace Suitpay\Controllers;

use App, Response, Redirect, View, Session, Exception;
use Illuminate\Http\Request;
use App\Http\Requests;
use Suitcore\Controllers\BaseController;
use Suitpay\ThirdpartyAPI\Veritrans\Veritrans;
use Suitpay\ThirdpartyAPI\Doku\Doku;
use Suitpay\ThirdpartyAPI\Ipay88\Ipay88;
use Suitpay\ThirdpartyAPI\OtherPayment\OtherPayment;
use Suitpay\Jobs\DokuCheck;
use Suitpay\Models\ThirdpartyPaymentProcess;
use Suitpay\Models\DokuInstallmentTenor;
use Suitpay\Models\Contracts\PayableContract;
use Suitpay\Models\Contracts\OptionalOrderPurchasedItemContract;
use Suitpay\Repositories\Contract\ThirdpartyPaymentProcessRepositoryContract;

// Currently Handling :
// (1) VERITRANS
// (2) DOKU
// (3) IPay88
// (4) Others 
/**
 * GENERAL PROCESS PATTERN
 * 1. Initiation [then make POST request with required parameter]
 * 2. Redirect to authentication phase 1 view of payment gateway
 * 3. If on page (2) click 'Cancel', then gateway will redirected to getUnfinish()
 * 4. If on page (2) click 'Ok', then gateway will process request
 * 5. If point (4) success, gateway will redirect to getFinishSuccess() and also
 *    call postHandler() so the merchant can process the result
 * 6. If point (4) success, gateway will redirect to getFinishFailed() and also
 *    call postHandler() so the merchant can process the result
 * 7. DONE. In view of getFinishSuccess and getFinishFailed we can put redirecting
 *    action to any page, such as home index or last successful/failed order detail page
 * PS. URL of getUnfinish(), getFinishSuccess(), getFinishFailed() and postHandler()
 *     must registered in thirdparty payment gateway configuration/settings
 **/
class PaymentController extends BaseController
{
    // CONSTANTS
    // ThirdParty Module
    const VERITRANS = 'veritrans'; // midtrans
    const DOKU = 'doku';
    const IPAY88 = 'ipay88';
    const OTHERS = 'others';

    // ATTRIBUTES
    protected $pageId;
    protected $paymentRepo;
    protected $basePayableModel;
    protected $routeDefaultIndex;
    protected $enabledPayment;
    protected $successRedirectUrl;
    protected $responseRedirectUrl;
    protected $backendUrl;
    protected $callbackUrl;

    // METHODS
    /**
     * Override Default Constructor
     * @param  ThirdpartyPaymentProcessRepositoryContract $_paymentRepo
     * @param  PayableContract $_basePayableModel
     * @param  string $_paymentConfiguration
     * @param  string $_routeDefaultIndex
     * @return void
     */
    public function __construct(ThirdpartyPaymentProcessRepositoryContract $_paymentRepo, PayableContract $_basePayableModel, $_paymentConfiguration, $_routeDefaultIndex)
    {
        $this->paymentRepo = $_paymentRepo;
        $this->basePayableModel = $_basePayableModel;
        $this->routeDefaultIndex = $_routeDefaultIndex;
        if (is_array($_paymentConfiguration)) {
            foreach($_paymentConfiguration as $paymentConfig) {
                if (is_array($paymentConfig)) {
                    if (isset($paymentConfig['code']) &&
                        isset($paymentConfig['serverKey'])) {
                        $isProduction = (isset($paymentConfig['isProduction']) && is_bool($paymentConfig['isProduction']) ? $paymentConfig['isProduction'] : false); // default is development
                        if ($paymentConfig['code'] == self::VERITRANS) {
                            Veritrans::$serverKey = $paymentConfig['serverKey'];
                            // True for production mode
                            Veritrans::$isProduction = $isProduction;
                        } elseif ($paymentConfig['code'] == self::DOKU) {
                            $mallId = (isset($paymentConfig['mallId']) && !empty($paymentConfig['mallId']) ? $paymentConfig['mallId'] : '00000000');
                            $ccInstallmentMallId = (isset($paymentConfig['ccInstallmentMallId']) && !empty($paymentConfig['ccInstallmentMallId']) ? $paymentConfig['ccInstallmentMallId'] : $mallId);
                            Doku::$isProduction = $isProduction; // True for production mode
                            Doku::$sharedKey = $paymentConfig['serverKey'];
                            Doku::$mallId = $mallId;
                            Doku::$ccInstallmentMallId = $ccInstallmentMallId;
                            $this->successRedirectUrl = $paymentConfig['successRedirectUrl'];
                        } elseif ($paymentConfig['code'] == self::IPAY88) {
                            $merchantCode = (isset($paymentConfig['merchantCode']) && !empty($paymentConfig['merchantCode']) ? $paymentConfig['merchantCode'] : '00000000');
                            Ipay88::$isProduction = $isProduction; // True for production mode
                            Ipay88::$sharedKey = $paymentConfig['serverKey'];
                            Ipay88::$merchantCode = $merchantCode;
                            Ipay88::$klikPayCode = $paymentConfig['klikPayCode'];
                            Ipay88::$clearKey = $paymentConfig['clearKey'];
                            $this->responseRedirectUrl = $paymentConfig['responseRedirectUrl'];
                            $this->backendUrl = $paymentConfig['backendUrl'];
                            $this->callbackUrl = $paymentConfig['callbackUrl'];
                        } elseif ($paymentConfig['code'] == self::OTHERS) {
                            $merchantCode = (isset($paymentConfig['merchantCode']) && !empty($paymentConfig['merchantCode']) ? $paymentConfig['merchantCode'] : '00000000');
                            OtherPayment::$sharedKey = $paymentConfig['serverKey'];
                            OtherPayment::$merchantCode = $merchantCode;
                            $this->backendUrl = $paymentConfig['backendUrl'];
                        }
                    }
                }
            }
        }
        $this->pageId = 'payment';

        view()->share('title', 'Payment');
        view()->share('keywords', 'Payment');
        view()->share('description', 'Payment');
        view()->share('pageImage', '');
    }

    /**
     * Set Base/Custom Main Model for related repository on runtime
     * @param SuitModel Custom Model
     **/
    public function setMainModel(PayableContract $model) {
        $this->basePayableModel = $model;
    }

    /**
     * Get Base/Custom Main Model for related repository on runtime
     * @param SuitModel Custom Model
     **/
    public function getMainModel() {
        return $this->basePayableModel;
    }

    /**
     * [GET] Init veritrans payment process
     * (another payment gateway should adapted to payment gateway expected initiation)
     * (much more like this :D)
     * @param  object $payableItem implement \App\Suitpay\Models\Contracts\PayableContract
     * @param  boolean $ignorePaymentMethod
     * @param  boolean $snap
     * @return redirect
     **/
    public function getVeritransInitialWeb(PayableContract $payableItem, $ignorePaymentMethod = false, $snap = false)
    {
        $vt = new Veritrans;
        if ($payableItem) {
            $matchedModule = $ignorePaymentMethod;
            if (!$ignorePaymentMethod && $payableItem->getPaymentMethod()) {
                $module = $payableItem->getPaymentMethod()->thirdparty_module_code;
                $matchedModule = ($module == self::VERITRANS);
            }
            // Existed Unpaid (Had Checked Out) Order with Veritrans Payment Method
            if ($matchedModule) {
                // [1] populate payableItem detail
                $safeOrderCode = str_replace('/', '_', $payableItem->getOrderCode());
                $transaction_details = [
                    'order_id'      => $safeOrderCode,
                    'gross_amount'  => $payableItem->getTotalPrice()
                ];
                // [2] populate customer shipping address
                $namePart = explode(" ", (!empty($payableItem->getRecipientName()) ? $payableItem->getRecipientName() : "Raw User"));
                $firstName = $namePart[0];
                $lastName = trim(str_replace($firstName, "", (!empty($payableItem->getRecipientName()) ? $payableItem->getRecipientName() : "Raw User")));
                $addressStreet = ($payableItem->getRecipientStreetName() ? $payableItem->getRecipientStreetName() : 'N/A');
                $veritransValidatedStreet = strlen($addressStreet) > 199 ? substr($addressStreet,0,194)."..." : $addressStreet;
                $shipping_address = [
                    'first_name'    => $firstName,
                    'last_name'     => $lastName,
                    'address'       => $veritransValidatedStreet,
                    'city'          => ($payableItem->getRecipientCity() ? $payableItem->getRecipientCity()  : "N/A"),
                    'postal_code'   => ($payableItem->getRecipientZipcode() ? $payableItem->getRecipientZipcode() : "00000"),
                    'phone'         => $payableItem->getRecipientPhone(),
                    'country_code'  => 'IDN' // TODO : country masterdata
                ];
                // [3] populate customer billing address
                $billing_address = $shipping_address;
                // [4] Populate customer Info
                $customer_details = [
                    'first_name'        => $firstName,
                    'last_name'         => $lastName,
                    'email'             => $payableItem->getRecipientEmail(),
                    'phone'             => $payableItem->getRecipientPhone(),
                    'billing_address'   => $billing_address,
                    'shipping_address'  => $shipping_address
                ];
                // [5] Prepare data to send on request to redirect_url
                // Uncomment 'credit_card_3d_secure' => true if want to disable 3DSecure
                if ($snap) {
                    $transaction_data = [
                    'credit_card'           => [
                        'secure' => true  // default payment process types
                    ],
                    'transaction_details'   => $transaction_details,
                    'item_details'          => $payableItem->getOrderItems(),
                    'customer_details'      => $customer_details,
                    //Set Custom Expiry
                    'expiry'            => [
                        "start_time"        => $payableItem->getOrderDatetime() . " +0700", // "2015-09-08 11:06:53 +0700"
                        "duration"      => $payableItem->getOrderExpiryInMinutes(),
                        "unit"              => "minutes"
                    ]
                    ];
                } else {
                    $transaction_data = [
                        'payment_type'          => 'vtweb', // default payment process type
                    'vtweb'                 => [
                        'credit_card_3d_secure' => true  // default payment process types
                    ],
                    'transaction_details'   => $transaction_details,
                    'item_details'          => $payableItem->getOrderItems(),
                    'customer_details'      => $customer_details,
                    //Set Custom Expiry
                    'custom_expiry'         => [
                        "order_time"        => $payableItem->getOrderDatetime() . " +0700", // "2015-09-08 11:06:53 +0700"
                        "expiry_duration"   => $payableItem->getOrderExpiryInMinutes(),
                        "unit"              => "minute"
                    ]
                    ];
                }
                // --- set payment process type
                if ($payableItem->getPaymentMethod()->thirdparty_module_enabled_payment) {
                    Veritrans::$enabledPayment = explode(',', $payableItem->getPaymentMethod()->thirdparty_module_enabled_payment);
                    if ($snap) {
                        $transaction_data['enabled_payments'] = Veritrans::$enabledPayment;
                    } else {
                        $transaction_data['vtweb'] = [
                            'enabled_payments'    => Veritrans::$enabledPayment,
                            'credit_card_3d_secure' => true
                        ];
                    }
                } 
                try
                {
                    if ($snap) {
                        Veritrans::$isSnap = true;
                        $snap_token = $vt->getSnapToken($transaction_data);
                        return $snap_token;
                    } else {
                        $vtweb_url = $vt->vtweb_charge($transaction_data);
                        return redirect($vtweb_url);
                    }
                }
                catch (Exception $e)
                {
                    Session::flash('error', 'Permintaan pemrosesan pembayaran ditolak atau sudah melewati batas waktu pembayaran!');
                    // return $e->getMessage;
                    return redirect()->back();
                }
            }
        }
        return Redirect::route($this->routeDefaultIndex);
    }

    // SPECIFIC FOR SNAP MODE
    public function getSnap()
    {
        return View::make('frontend.payment.snap');
    }

    // SPECIFIC FOR SNAP MODE
    public function getSnapToken($orderId, $paymentMethodCode)
    {
        return '';
    }

    // SPECIFIC FOR SNAP MODE
    public function getSnapTokenInitiation(PayableContract $payableItem, $ignorePaymentMethod = false) 
    {
        return $this->getVeritransInitialWeb($payableItem, $ignorePaymentMethod, true);
    }

    // SPECIFIC FOR SNAP MODE
    public function postFinish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        return $result->status_message;
    }

    /**
    * [POST] Handler Veritrans : Handling result from veritrans payment gateway
    * Return json response as acknowledge that payment received
    * (should adapted to payment gateway expected result)
    *
    * @return Response|JSON
    **/
    // Route::post('/vt_notif', 'InstancesNameOfPaymentController@postVeritransNotification');
    // Sample PARAMS :
    /*
    {
        "status_code": "200",
        "status_message": "Veritrans payment notification",
        "transaction_id": "13ad1f8c-f1f0-473b-8bc8-9975592ceab3",
        "order_id": "Sample Order-2",
        "payment_type": "credit_card",
        "transaction_time": "2016-06-16 18:08:38 +0700",
        "transaction_status": "capture",
        "fraud_status": "CHALLENGE",
        "masked_card": "529721-1111",
        "gross_amount": "2800000.00",
        "signature_key": "cd9f11189cf9243bdd98a5fafcc744732b2b44af95616fecd6b0a32f9c9853b0b1249000deb4469f2e876880d1ebcf7277563105e2239a64192985dc30e86698"
    }
    */
    public function postVeritransNotification() // notification
    {
        $vt = new Veritrans;
        // dd('Test notification handling!!!');
        $json_result = file_get_contents('php://input');
        $result = json_decode(str_replace('\"', '"', $json_result));
        if ($result) {
            $notif = $vt->status($result->order_id);
            $transactionId = $result->transaction_id;
            $relatedOrderCode = str_replace('_', '/', $result->order_id);
            $relatedOrderClassObject = $this->basePayableModel;
            $relatedOrder = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$relatedOrderCode)->first();
            if ($notif && $relatedOrder) {
                $thirdpartyPaymentProcess = new ThirdpartyPaymentProcess;
                $newparam = [];
                $newparam['transaction_id'] = $transactionId;
                $newparam['order_code'] = $relatedOrder->getOrderCode();
                if ($relatedOrder->paymentMethod) {
                    $module = $relatedOrder->paymentMethod->getThirdpartyModule();
                    if ($module) {
                        $newparam['thirdparty_module_id'] = $module->id;
                    }
                }
                // check fraudDetectionSystem status first then transactionStatus
                if (isset($notif->fraud_status)) {
                    // With FDS
                    if ($notif->fraud_status == Veritrans::VERITRANS_FDS_ACCEPT) {
                        if ($notif->transaction_status == Veritrans::VERITRANS_SUCCESS ||
                            $notif->transaction_status == Veritrans::VERITRANS_SETTLEMENT) {
                            $newparam['status'] = ThirdpartyPaymentProcess::STATUS_SUCCESS; // 'success' is successfull / ok status from thirdparty payment gateway
                        } elseif ($notif->transaction_status == Veritrans::VERITRANS_PENDING ||
                            $notif->transaction_status == Veritrans::VERITRANS_AUTHORIZE) {
                            $newparam['status'] = ThirdpartyPaymentProcess::STATUS_PENDING; 
                        } else {
                            // VERITRANS_DENY or VERITRANS_EXPIRE or any undefined
                            $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
                        }
                    } elseif ($notif->fraud_status == Veritrans::VERITRANS_FDS_CHALLENGE) {
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_PENDING; 
                    } else {
                        // VERITRANS_FDS_DENY or any
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
                    }
                } else {
                    // Without FDS
                    if ($notif->transaction_status == Veritrans::VERITRANS_SUCCESS ||
                        $notif->transaction_status == Veritrans::VERITRANS_SETTLEMENT) {
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_SUCCESS; // 'success' is successfull / ok status from thirdparty payment gateway
                    } elseif ($notif->transaction_status == Veritrans::VERITRANS_PENDING ||
                        $notif->transaction_status == Veritrans::VERITRANS_AUTHORIZE) {
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_PENDING; 
                    } else {
                        // VERITRANS_DENY or VERITRANS_EXPIRE or any undefined
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
                    }
                }
                $newparam['json_result'] = $json_result;
                $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
                if ($result) {
                    // success
                    $this->paymentRepo->processImpact($result, $relatedOrder);
                    return Response::json(['status' => 'confirmed']);
                }
            }
        }
        return Response::json(['status' => 'failed']);
    }

    protected function identifyCurrentTransaction(Request $request)
    {
        // old response from midtrans
        $orderId = $request->get('order_id', '');
        $transactionId = $request->get('id', '');
        $statusCode = $request->get('status_code', '');
        $transactionStatus = $request->get('transaction_status', '');

        // new response from midtrans (ex: CIMB Clicks)
        if (isset($request->response)) {
            $response = json_decode($request->response);
            if (isset($response->order_id)) { 
                // order ID
                $orderId = str_replace('_', '/', $response->order_id);
            }
            if (isset($response->transaction_id)) { 
                // transaction ID
                $transactionId = $response->transaction_id;
            }
            if (isset($response->status_code)) { 
                // status code
                $statusCode = $response->status_code;
            }
            if (isset($response->transaction_status)) { 
                // transaction status
                $transactionStatus = $response->transaction_status;
            }
        }

        if (empty($orderId)) {
            $orderId = $request->session()->get('CURRENT_ORDER_ID', null);
        }

        return compact('orderId', 'transactionId', 'statusCode', 'transactionStatus');
    }

    // SPECIFIC FOR DOKU ====================================================
    public function getDokuInitialWeb(PayableContract $payableItem, $ignorePaymentMethod = false, $dokuHosted = true, $isMobile = false)
    {
        try {
            \Debugbar::disable();
        } catch (\Exception $e) { }
        if ($payableItem) {
            $matchedModule = $ignorePaymentMethod;
            if (!$ignorePaymentMethod && $payableItem->getPaymentMethod()) {
                $module = $payableItem->getPaymentMethod()->thirdparty_module_code;
                $matchedModule = ($module == self::DOKU);
            }
            // Existed Unpaid (Had Checked Out) Order with Veritrans Payment Method
            if ($matchedModule) {
                // [1] populate payableItem detail
                $safeOrderCode = str_replace('/', '_', $payableItem->getOrderCode());
                $transaction_details = [
                    'invoice'       => $safeOrderCode,
                    'amount'        => $payableItem->getTotalPrice() . ".00",
                    'currency'      => '360'
                ];

                $paymentMethod = $payableItem->getPaymentMethodObject();
                $paymentchannel = $paymentMethod ? $paymentMethod->thirdparty_module_enabled_payment : Doku::CREDITCARD;

                //Doku::$sharedKey = env('DOKU_SHARED_KEY', '1lk8kCqRZ40M');
                //Doku::$isProduction = env('DOKU_IS_PRODUCTION', false);

                if ($paymentchannel == Doku::CREDITCARDINSTALLMENT) {
                    Doku::$mallId = Doku::$ccInstallmentMallId;
                    $paymentchannel = Doku::CREDITCARD;   
                } 
                //else {
                //    Doku::$mallId = env('DOKU_MALL_ID', '4222');
                //}

                $words = Doku::doCreateWords($transaction_details);

                // specific case
                if (Doku::$isProduction) {
                    if ($paymentchannel == Doku::BANKTRANSFER) {
                        $paymentchannel = Doku::BANKTRANSFER_PRODUCTION;
                    } elseif ($paymentchannel == Doku::CSTORE) {
                        $paymentchannel = Doku::CSTORE_PRODUCTION;
                    }
                }

                // [2] populate customer info
                $addressStreet = ($payableItem->getRecipientStreetName() ? $payableItem->getRecipientStreetName() : 'N/A');
                $validatedStreet = str_limit($addressStreet, 195);
                $customer = [
                    'name'    => (!empty($payableItem->getRecipientName()) ? $payableItem->getRecipientName() : "Raw User"),
                    'data_phone' => $payableItem->getRecipientPhone(),
                    'data_email' => $payableItem->getRecipientEmail(),
                    'data_address' => $validatedStreet
                ];

                // [3] Populate Transaction Item
                $transactionItems = $payableItem->getOrderItems();
                /*
                // Why Like This? Because there is limitation of parameter length on Doku Server
                // Subject to be monitored if there is changes in future
                $basket = [];
                foreach ($transactionItems as $key => $item) {
                    $basket[] = [
                        'name' => $item['id'] . " - " . $item['name'],
                        'amount' => $item['price'].".00",
                        'quantity' => $item['quantity'],
                        'subtotal' => ($item['price'] * $item['quantity']) . ".00"
                    ];
                }
                */
                $basketSummary = [
                    'amount' => 0,
                    'quantity' => 0,
                    'subtotal' => 0
                ];
                foreach ($transactionItems as $key => $item) {
                    $basketSummary['amount'] += $item['price'];
                    $basketSummary['quantity'] += $item['quantity'];
                    $basketSummary['subtotal'] += ($item['price'] * $item['quantity']);
                }
                $basket[] = [
                    'name' => 'Total Summary',
                    'amount' => $basketSummary['amount'] . ".00",
                    'quantity' => $basketSummary['quantity'],
                    'subtotal' => $basketSummary['subtotal'] . ".00"
                ];

                // specific for transaction that need generatedPaymentCode
                $generatedPaymentCode = null;
                if (!$dokuHosted &&
                    in_array($paymentchannel, [Doku::BANKTRANSFER, Doku::BANKTRANSFER_PRODUCTION, Doku::CSTORE, Doku::CSTORE_PRODUCTION])) {
                    // [4] Transaction Data
                    $dataPayment = [
                        'req_mall_id' => Doku::$mallId,
                        'req_chain_merchant' => 'NA',
                        'req_amount' => $transaction_details['amount'],
                        'req_words' => $words,
                        'req_currency' => '360',
                        'req_purchase_currency' => '360',
                        'req_purchase_amount' => $transaction_details['amount'],
                        'req_trans_id_merchant' => $transaction_details['invoice'],
                        'req_request_date_time' => date('YmdHis'),
                        'req_session_id' => sha1(date('YmdHis')),
                        'req_name' => $customer['name'],
                        'req_email' => $customer['data_email'],
                        'req_mobile_phone' => $customer['data_phone'],
                        'req_basket' => $basket,
                        'req_address' => $customer['data_address'],
                        'req_expiry_time' => $payableItem->getOrderExpiryInMinutes()
                    ];
                    if ($paymentchannel == Doku::BANKTRANSFER || $paymentchannel == Doku::BANKTRANSFER_PRODUCTION) {
                        $response = Doku::doGeneratePaycodeVA($dataPayment);
                    } else {
                        $response = Doku::doGeneratePaycode($dataPayment);
                    }
                    // dd($dataPayment, $response);
                    if ($response->res_response_code == Doku::DOKU_SUCCESS) {
                        $generatedPaymentCode = $response;
                    }
                }
                
                if ($dokuHosted) {
                    // [4] Transaction Data
                    unset($transaction_details['currency']);
                    $words = Doku::doCreateWords($transaction_details);
                    $dataPayment = [
                        'MALLID' => Doku::$mallId,
                        'CHAINMERCHANT' => 'NA',
                        'AMOUNT' => $transaction_details['amount'],
                        'WORDS' => $words,
                        'PURCHASEAMOUNT' => $transaction_details['amount'],
                        'TRANSIDMERCHANT' => $transaction_details['invoice'],
                        'REQUESTDATETIME' => date('YmdHis'),
                        'CURRENCY' => '360',
                        'PURCHASECURRENCY' => '360',
                        'SESSIONID' => sha1(date('YmdHis')),
                        'NAME' => $customer['name'],
                        'EMAIL' => $customer['data_email'],
                        'BASKET' => $basket,
                        'ADDRESS' => $customer['data_address'],
                        'COUNTRY' => '360',
                        'STATE' => $payableItem->getRecipientProvince(),
                        'CITY' => $payableItem->getRecipientCity(),
                        'PROVINCE' => $payableItem->getRecipientProvince(),
                        'ZIPCODE' => $payableItem->getRecipientZipcode(),
                        'HOMEPHONE' => $customer['data_phone'],
                        'MOBILEPHONE' => $customer['data_phone'],
                        'WORKPHONE' => $customer['data_phone'],
                        'BIRTHDATE' => $payableItem->getRecipientBirthdate(),
                        'PAYMENTCHANNEL' => str_replace("D", "", $paymentchannel)
                    ];
                    $dokuInstallment = null;
                    if ($payableItem->getPaymentOption()) {
                        $paymentOption = $payableItem->getPaymentOption();
                        if ($paymentOption) {
                            try {
                                $paymentOption = json_decode($paymentOption);
                            } catch (\Exception $e) { }
                        }
                        if ($paymentchannel == Doku::CREDITCARD && $paymentOption && $paymentOption->installment_config_installment) {
                            $dokuInstallment = DokuInstallmentTenor::find($paymentOption->installment_config_installment);
                            if($dokuInstallment && $dokuInstallment->dokuBank) {
                                $dataPayment['INSTALLMENT_ACQUIRER'] = $dokuInstallment->dokuBank->code;
                                $dataPayment['TENOR'] = str_pad($dokuInstallment->term, 2, "0", STR_PAD_LEFT);
                                $isOnUs = true;
                                if(!empty($dokuInstallment->description)) {
                                    $dataPayment['PROMOID'] = trim($dokuInstallment->description); 
                                    $isOnUs = (starts_with($dataPayment['PROMOID'], '0'));
                                }
                                if(!$isOnUs) {
                                    $dataPayment['PAYMENTTYPE'] = 'OFFUSINSTALLMENT';
                                }
                            }
                        }
                    }
                    // render
                    $dataPayment['BASKET'] = Doku::formatBasket($dataPayment['BASKET']);
                    // dd($dataPayment, Doku::getDokuHostedUrl());
                    return View::make(($isMobile ? 'frontend.payment.dokuhostedfullmobilepage' : 'frontend.payment.dokuhostedfullpage'), [
                        'order' => $payableItem,
                        'paymentMethod' => $paymentMethod,
                        'dataPayment' => $dataPayment,
                        'url' => Doku::getDokuHostedUrl($paymentchannel == Doku::BCA_CLICKPAY),
                        'dokuInstallment' => $dokuInstallment
                    ]);
                    // $response = Doku::initDokuHostedInitialPage($dataPayment);
                    // return $response;
                } else {
                    // Merchant Hosted
                    // render
                    return View::make('frontend.payment.dokufullpage', [
                        'words' => $words,
                        'order' => $payableItem,
                        'mallId' => Doku::$mallId,
                        'params' => $transaction_details,
                        'paymentchannel' => $paymentchannel,
                        'generatedPaymentCode' => $generatedPaymentCode
                    ]);
                }
            }
        }
        return App::abort(404);
    }

    public function getDokuMerchantHosted(PayableContract $payableItem, $ignorePaymentMethod = false)
    {
        return $this->getDokuInitialWeb($payableItem, $ignorePaymentMethod, false);
    }

    /**
    * Related API Process for Mobile Apps DOKU Payment
    **/
    public function getDokuAPIPaymentProcess(PayableContract $payableItem, $extraParameter = []) {
        // [0] Base Timezone and Validation
        date_default_timezone_set('Asia/Jakarta');
        $paymentChannelCode = '';
        $paymentFee = 0;
        if ($payableItem->getPaymentMethod()) {
            $paymentMethodRepo = app('Suitpay\Repositories\Contract\PaymentMethodRepositoryContract');
            $paymentFee = $paymentMethodRepo->getTotalPaymentFee($payableItem->getPaymentMethod()->id, $payableItem->getTotalPrice());
            $paymentChannelCode = $payableItem->getPaymentMethod()->thirdparty_module_enabled_payment;
        }
        if (in_array($paymentChannelCode, [Doku::CREDITCARD, Doku::CREDITCARDINSTALLMENT, Doku::DOKUWALLET])) {
            if (!isset($extraParameter['doku_pairing_code'])) return [
                'status' => 30,
                'data' => 'doku_pairing_code is required!'
            ];
            if (!isset($extraParameter['doku_token'])) return [
                'status' => 30,
                'data' => 'doku_token is required!'
            ];
            if (!isset($extraParameter['device_id'])) return [
                'status' => 30,
                'data' => 'device_id is required!'
            ];
        } elseif (in_array($paymentChannelCode, [Doku::BCA_CLICKPAY, Doku::MANDIRI_CLICKPAY, Doku::BRI_EPAY, Doku::CIMB_CLICKS, Doku::PERMATA_NET, Doku::DANAMON, Doku::MUAMALAT])) {
            if (!isset($extraParameter['card_number'])) return [
                'status' => 30,
                'data' => 'card_number is required!'
            ];
            if (!isset($extraParameter['CHALLENGE_CODE_1'])) return [
                'status' => 30,
                'data' => 'CHALLENGE_CODE_1 is required!'
            ];
            if (!isset($extraParameter['CHALLENGE_CODE_2'])) return [
                'status' => 30,
                'data' => 'CHALLENGE_CODE_2 is required!'
            ];
            if (!isset($extraParameter['CHALLENGE_CODE_3'])) return [
                'status' => 30,
                'data' => 'CHALLENGE_CODE_3 is required!'
            ];
            if (!isset($extraParameter['response_token'])) return [
                'status' => 30,
                'data' => 'response_token is required!'
            ];
        }
        // [1] Transaction Summary Detail
        $safeOrderCode = str_replace('/', '_', $payableItem->getOrderCode());
        $transaction_details = [
            'invoice'       => $safeOrderCode,
            'amount'        => $payableItem->getTotalPrice() . ".00",
        ];
        $cardNumber = "";
        if ($paymentFee > 0) {
            $transaction_details['amount'] = ($payableItem->getTotalPrice() + $paymentFee).".00";
        }
        // inject production payment channel code
        if (Doku::$isProduction) {
            if ($paymentChannelCode == Doku::BANKTRANSFER) $paymentChannelCode = Doku::BANKTRANSFER_PRODUCTION;
            elseif ($paymentChannelCode == Doku::CSTORE) $paymentChannelCode = Doku::CSTORE_PRODUCTION;
        }
        if (in_array($paymentChannelCode, [Doku::CREDITCARD, Doku::CREDITCARDINSTALLMENT, Doku::DOKUWALLET])) {
            $transaction_details['currency']        = '360';
            $transaction_details['pairing_code']    = $extraParameter['doku_pairing_code'];
            $transaction_details['token']           = $extraParameter['doku_token'];
            $transaction_details['deviceid']        = $extraParameter['device_id'];
        } elseif (in_array($paymentChannelCode, [Doku::BANKTRANSFER, Doku::BANKTRANSFER_PRODUCTION, Doku::CSTORE, Doku::CSTORE_PRODUCTION])) {
            // No extra details
        } elseif (in_array($paymentChannelCode, [Doku::BCA_CLICKPAY, Doku::MANDIRI_CLICKPAY, Doku::BRI_EPAY, Doku::CIMB_CLICKS, Doku::PERMATA_NET, Doku::DANAMON, Doku::MUAMALAT])) {
            $transaction_details['currency']        = '360';
            $cardNumber = str_replace(" - ", "", $extraParameter['card_number']);
        }
        // [2] Create Signature
        $words = Doku::doCreateWords($transaction_details);
        // [3] Customer Data
        $customer = [
            'name' => $payableItem->getRecipientName(),
            'data_phone' => $payableItem->getRecipientPhone(),
            'data_email' => $payableItem->getRecipientEmail(),
            'data_address' => $payableItem->getRecipientStreetName()
        ];
        // [4] Populate Transaction Item
        $transactionItems = $payableItem->getOrderItems();
        $basketSummary = [
            'amount' => 0,
            'quantity' => 0,
            'subtotal' => 0
        ];
        foreach ($transactionItems as $key => $item) {
            $basketSummary['amount'] += $item['price'];
            $basketSummary['quantity'] += $item['quantity'];
            $basketSummary['subtotal'] += ($item['price'] * $item['quantity']);
        }
        $basket[] = [
            'name' => 'Total Summary',
            'amount' => $basketSummary['amount'] . ".00",
            'quantity' => $basketSummary['quantity'],
            'subtotal' => $basketSummary['subtotal'] . ".00"
        ];
        // [5] Compile / Prepare Payment Request
        $dataPayment = [
            'req_mall_id' => Doku::$mallId,
            'req_chain_merchant' => 'NA',
            'req_amount' => $transaction_details['amount'],
            'req_words' => $words,
            'req_trans_id_merchant' => $transaction_details['invoice'],
            'req_purchase_amount' => $transaction_details['amount'],
            'req_request_date_time' => date('YmdHis'),
            'req_session_id' => sha1(date('YmdHis')),
            'req_name' => $customer['name'],
            'req_email' => $customer['data_email'],
            'req_address' => $customer['data_address'],
            'req_basket' => $basket,
        ];
        if (in_array($paymentChannelCode, [Doku::CREDITCARD, Doku::CREDITCARDINSTALLMENT, Doku::DOKUWALLET])) {
            $dataPayment = array_merge($dataPayment, [
                'req_mobile_phone' => $customer['data_phone'],
                'req_currency' => '360',
                'req_purchase_currency' => '360',
                'req_payment_channel' => str_replace("D", "", $paymentChannelCode),
                'req_token_id' => (isset($transaction_details['token']) ? $transaction_details['token'] : '')
            ]);
            // for installment
            if (isset($extraParameter['installment_acquirer']) && 
                isset($extraParameter['installment_tenor']) && 
                isset($extraParameter['installment_plan_id'])) {
                $dataPayment = array_merge($dataPayment, [
                    'req_installment_acquirer' => $extraParameter['installment_acquirer'],
                    'req_tenor' => $extraParameter['installment_tenor'],
                    'req_plan_id' => $extraParameter['installment_plan_id']
                ]);
            }
            // for 1-click / 2-click
            if (isset($extraParameter['credit_card_token']) && 
                isset($extraParameter['credit_card_customer_id'])) {
                $dataPayment = array_merge($dataPayment, [
                    'req_token_payment' => $extraParameter['credit_card_token'],
                    'req_customer_id' => $extraParameter['credit_card_customer_id']
                ]);
            }
        } elseif (in_array($paymentChannelCode, [Doku::BANKTRANSFER, Doku::BANKTRANSFER_PRODUCTION, Doku::CSTORE, Doku::CSTORE_PRODUCTION])) {
            $dataPayment = array_merge($dataPayment, [
                'req_mobile_phone' => $customer['data_phone'],
                'req_expiry_time' => $payableItem->getOrderExpiryInMinutes()
            ]);
        } elseif (in_array($paymentChannelCode, [Doku::BCA_CLICKPAY, Doku::MANDIRI_CLICKPAY, Doku::BRI_EPAY, Doku::CIMB_CLICKS, Doku::PERMATA_NET, Doku::DANAMON, Doku::MUAMALAT])) {
            $dataPayment = array_merge($dataPayment, [
                'req_mobile_phone' => $customer['data_phone'],
                'req_currency' => '360',
                'req_purchase_currency' => '360',
                'req_payment_channel' => str_replace("D", "", $paymentChannelCode),
                'req_card_number' => $cardNumber,
                'req_challenge_code_1' => $extraParameter['CHALLENGE_CODE_1'],
                'req_challenge_code_2' => $extraParameter['CHALLENGE_CODE_2'],
                'req_challenge_code_3' => $extraParameter['CHALLENGE_CODE_3'],
                'req_response_token' => $extraParameter['response_token']
            ]);
        }
        // [6] Do Payment Request
        $response = Doku::DOKU_DENY;
        if (in_array($paymentChannelCode, [Doku::CREDITCARD, Doku::CREDITCARDINSTALLMENT, Doku::DOKUWALLET])) {
            $response = Doku::doPayment($dataPayment);
        } elseif (in_array($paymentChannelCode, [Doku::BANKTRANSFER, Doku::BANKTRANSFER_PRODUCTION, Doku::CSTORE, Doku::CSTORE_PRODUCTION])) {
            $response = Doku::doGeneratePaycode($dataPayment);
        } elseif (in_array($paymentChannelCode, [Doku::BCA_CLICKPAY, Doku::MANDIRI_CLICKPAY, Doku::BRI_EPAY, Doku::CIMB_CLICKS, Doku::PERMATA_NET, Doku::DANAMON, Doku::MUAMALAT])) {
            $response = Doku::doDirectPayment($dataPayment);
        }
        // [7] Return Result
        try {
            if($response && $response->res_response_code == Doku::DOKU_SUCCESS) {
                // Merchant process for CreditCard, Wallet, DirectDebit
                if (in_array($paymentChannelCode, [Doku::CREDITCARD, Doku::CREDITCARDINSTALLMENT, Doku::DOKUWALLET, Doku::BCA_CLICKPAY, Doku::MANDIRI_CLICKPAY, Doku::BRI_EPAY, Doku::CIMB_CLICKS, Doku::PERMATA_NET, Doku::DANAMON, Doku::MUAMALAT])) {
                    $this->dokuMerchantProcess($payableItem, ( isset($response->res_transaction_code) ? $response->res_transaction_code : (isset($response->res_tracking_id) ? $response->res_tracking_id : '00000000') ), $response->res_response_code, json_encode($response));
                }
                // Return Response
                if (in_array($paymentChannelCode, [Doku::BANKTRANSFER, Doku::BANKTRANSFER_PRODUCTION])) {
                    $response->va_bank = 'Permata';
                    $response->va_bank_code = '013';
                    $response->va_number = Doku::getPermataVAPrefix() . $response->res_pay_code;
                }
                return [
                    'status' => 20,
                    'data' => $response
                ];
            } else {
                return [
                    'status' => 30,
                    'data' => $response
                ];
            }
        } catch (Exception $e) {
            return [
                'status' => 50,
                'data' => $e->getMessage()
            ];
        }
    }

    /**
     *  POST DOKU FORM that initiated from getDokuInitialWeb()
     **/
    public function postDokuFormNotification()
    {
        try {
            \Debugbar::disable();
        } catch (\Exception $e) { }

        $postParam = request()->all();

        //Doku::$sharedKey = env('DOKU_SHARED_KEY', '1lk8kCqRZ40M');
        //Doku::$isProduction = env('DOKU_IS_PRODUCTION', false);
        //Doku::$mallId = env('DOKU_MALL_ID', '4222');

        // [1] initial order
        $token = isset($postParam['doku_token']) ? $postParam['doku_token'] : (isset($postParam['doku-token']) ? $postParam['doku-token'] : '');
        $chainMerchant = isset($postParam['doku_chain_merchant']) ? $postParam['doku_chain_merchant'] : (isset($postParam['chain_merchant']) ? $postParam['chain_merchant'] : (isset($postParam['doku-chain-merchant']) ? $postParam['doku-chain-merchant'] : (isset($postParam['chain-merchant']) ? $postParam['chain-merchant'] : 'NA') ) );
        $pairingCode = isset($postParam['doku_pairing_code']) ? $postParam['doku_pairing_code'] : (isset($postParam['doku-pairing-code']) ? $postParam['doku-pairing-code'] : '');
        $invoiceNumber = isset($postParam['doku_invoice_no']) ? $postParam['doku_invoice_no'] : (isset($postParam['trans_id']) ? $postParam['trans_id'] : (isset($postParam['invoice']) ? $postParam['invoice'] : (isset($postParam['doku-invoice-no']) ? $postParam['doku-invoice-no'] : '') ) );
        $amount = isset($postParam['doku_amount']) ? $postParam['doku_amount'] : (isset($postParam['amount']) ? $postParam['amount'] : (isset($postParam['doku-amount']) ? $postParam['doku-amount'] : '0.00') );
        $currency = isset($postParam['doku_currency']) ? $postParam['doku_currency'] : (isset($postParam['doku-currency']) ? $postParam['doku-currency'] : '360');
        $installmentAcquirer = isset($postParam['req_installment_acquirer']) ? $postParam['req_installment_acquirer'] : null;
        $installmentTenor = isset($postParam['req_tenor']) ? $postParam['req_tenor'] : null;
        $planId = isset($postParam['req_plan_id']) ? $postParam['req_plan_id'] : null;

        // identify order
        $relatedOrderCode = str_replace('_', '/', $invoiceNumber);
        $relatedOrderClassObject = $this->basePayableModel;
        $relatedOrder = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$relatedOrderCode)->first();

        if ($relatedOrder) {
            $paymentMethod = $relatedOrder->getPaymentMethodObject();
            $paymentchannel = $paymentMethod ? $paymentMethod->thirdparty_module_enabled_payment : request()->get('paymentchannel', 'D15');

            // [2] populate customer info
            $addressStreet = ($relatedOrder->getRecipientStreetName() ? $relatedOrder->getRecipientStreetName() : 'N/A');
            $validatedStreet = str_limit($addressStreet, 195);
            $customer = [
                'name'    => (!empty($relatedOrder->getRecipientName()) ? $relatedOrder->getRecipientName() : "Raw User"),
                'data_phone' => $relatedOrder->getRecipientPhone(),
                'data_email' => $relatedOrder->getRecipientEmail(),
                'data_address' => $validatedStreet
            ];

            // [3] Populate Transaction Item
            $transactionItems = $relatedOrder->getOrderItems();
            /*
            $basket = [];
            foreach ($transactionItems as $key => $item) {
                $basket[] = [
                    'name' => $item['id'] . " - " . $item['name'],
                    'amount' => $item['price'].".00",
                    'quantity' => $item['quantity'],
                    'subtotal' => ($item['price'] * $item['quantity']) . ".00"
                ];
            }
            */
            $basketSummary = [
                'amount' => 0,
                'quantity' => 0,
                'subtotal' => 0
            ];
            foreach ($transactionItems as $key => $item) {
                $basketSummary['amount'] += $item['price'];
                $basketSummary['quantity'] += $item['quantity'];
                $basketSummary['subtotal'] += ($item['price'] * $item['quantity']);
            }
            $basket[] = [
                'name' => 'Total Summary',
                'amount' => $basketSummary['amount'] . ".00",
                'quantity' => $basketSummary['quantity'],
                'subtotal' => $basketSummary['subtotal'] . ".00"
            ];

            $params = [
                'amount' => $amount,
                'invoice' => $invoiceNumber,
                'currency' => $currency,
                'pairing_code' => $pairingCode,
                'token' => $token
            ];
            $words = Doku::doCreateWords($params);

            if ($paymentchannel == Doku::CREDITCARD) {
                // For Credit Card
                $dataPayment = [
                    'req_mall_id' => Doku::$mallId,
                    'req_chain_merchant' => $chainMerchant,
                    'req_amount' => $amount,
                    'req_words' => $words,
                    'req_words_raw' => Doku::doCreateWordsRaw($params),
                    'req_purchase_amount' => $amount,
                    'req_trans_id_merchant' => $invoiceNumber,
                    'req_request_date_time' => date('YmdHis'),
                    'req_currency' => $currency,
                    'req_purchase_currency' => $currency,
                    'req_session_id' => sha1(date('YmdHis')),
                    'req_name' => $customer['name'],
                    'req_payment_channel' => str_replace("D", "", $paymentchannel),
                    'req_basket' => $basket,
                    'req_email' => $customer['data_email'],
                    'req_token_id' => $token,
                    'req_mobile_phone' => $customer['data_phone'],
                    'req_address' => $customer['data_address']
                ];
                if ($installmentAcquirer && $installmentTenor) {
                    $dataPayment['req_installment_acquirer'] = $installmentAcquirer;
                    $dataPayment['req_tenor'] = $installmentTenor;
                    if ($planId) {
                        $dataPayment['req_plan_id'] = $planId;
                    }
                }

                $responsePayment = Doku::doPayment($dataPayment);

                if($responsePayment->res_response_code == Doku::DOKU_SUCCESS){
                    // Merchant process ...
                    $this->dokuMerchantProcess($relatedOrder, $responsePayment->res_session_id, $responsePayment->res_response_code, json_encode($responsePayment));

                    //process tokenization
                    if(isset($responsePayment->res_bundle_token)) {
                        $tokenPayment = json_decode($responsePayment->res_bundle_token);

                        if ($tokenPayment->res_token_code == Doku::DOKU_SUCCESS) {
                            //save token
                            $getTokenPayment = $tokenPayment->res_token_payment;
                        }
                    }

                    //redirect process to doku
                    if (!empty($this->successRedirectUrl)) {
                        $responsePayment->res_redirect_url = $this->successRedirectUrl.'?order_id='.$relatedOrderCode.'&status_code='.$responsePayment->res_response_code.'&transaction_status='.$responsePayment->res_response_code.'&TRANSIDMERCHANT='.$responsePayment->res_response_msg;
                        $responsePayment->res_show_doku_page = false; 
                    } else {
                        // true if you want to show doku page first before redirecting to redirect url
                        $responsePayment->res_show_doku_page = true; 
                    }
                    
                    //example : Response doku to merchant
                    //MIPPayment.processRequest ACKNOWLEDGE : {"res_approval_code":"245391","res_trans_id_merchant":"invoice_1461728094","res_amount":"50000.00","res_payment_date_time":"20160427003515","res_verify_score":"-1","res_verify_id":"","res_verify_status":"NA","res_words":"00a22b8d81a731d948605b682578d6a9074de5c47498312cd13abd0ef2f80e7a","res_response_msg":"SUCCESS","res_mcn":"5***********8754","res_mid":"094345145394964","res_bank":"Bank BNI","res_response_code":"0000","res_session_id":"b249a07ff9c5251dddc87997d482836ea3b8affd","res_payment_channel":"D15"}        
                    
                    return json_encode($responsePayment);
                } else {
                    return json_encode($responsePayment);
                }
            } elseif ($paymentchannel == Doku::DOKUWALLET) {
                // For Doku Wallet
                // PRE-PAYMENT
                $dataSimpleTransaction = [
                    'invoice'       => $relatedOrderCode,
                    'amount'        => $amount,
                    'currency'      => '360'
                ];
                $simpleWords = Doku::doCreateWords($dataSimpleTransaction);
                $dataPrePayment = [
                    'req_token_id' => $token,
                    'req_pairing_code' => $pairingCode,
                    'req_customer' => $customer,
                    'req_basket' => $basket,
                    'req_words' => $simpleWords
                //    , 'req_bin_filter' => array("411111", "548117", "433???6", "41*3"),
                ];
                //$responsePrePayment = Doku::doPrePayment($dataPrePayment);
                //if($responsePrePayment->res_response_code == Doku::DOKU_SUCCESS) { 
                    // prepayment success
                    $dataPayment = [
                        'req_mall_id' => Doku::$mallId,
                        'req_chain_merchant' => $chainMerchant,
                        'req_amount' => $amount,
                        'req_words' => $words,
                        'req_words_raw' => Doku::doCreateWordsRaw($params),
                        'req_purchase_amount' => $amount,
                        'req_trans_id_merchant' => $invoiceNumber,
                        'req_request_date_time' => date('YmdHis'),
                        'req_currency' => $currency,
                        'req_purchase_currency' => $currency,
                        'req_session_id' => sha1(date('YmdHis')),
                        'req_name' => $customer['name'],
                        'req_payment_channel' => str_replace("D", "", $paymentchannel),
                        'req_basket' => $basket,
                        'req_email' => $customer['data_email'],
                        'req_token_id' => $token,
                        'req_mobile_phone' => $customer['data_phone'],
                        'req_address' => $customer['data_address']
                    ];

                    // PAYMENT
                    $responsePayment = Doku::doPayment($dataPayment);

                    if($responsePayment->res_response_code == Doku::DOKU_SUCCESS){
                        // Merchant process ...
                        $this->dokuMerchantProcess($relatedOrder, $responsePayment->res_tracking_id, $responsePayment->res_response_code, json_encode($responsePayment));

                        //process tokenization
                        if(isset($responsePayment->res_bundle_token)) {
                            $tokenPayment = json_decode($responsePayment->res_bundle_token);

                            if ($tokenPayment->res_token_code == Doku::DOKU_SUCCESS) {
                                //save token
                                $getTokenPayment = $tokenPayment->res_token_payment;
                            }
                        }

                        //redirect process to doku
                        if (!empty($this->successRedirectUrl)) {
                            $responsePayment->res_redirect_url = $this->successRedirectUrl.'?order_id='.$relatedOrderCode.'&status_code='.$responsePayment->res_response_code.'&transaction_status='.$responsePayment->res_response_code.'&TRANSIDMERCHANT='.$responsePayment->res_response_msg;
                            $responsePayment->res_show_doku_page = false; 
                        } else {
                            // true if you want to show doku page first before redirecting to redirect url
                            $responsePayment->res_show_doku_page = true; 
                        }
                    }
                    // Return payment result, example :
                    // MIPPayment.processRequest ACKNOWLEDGE : {"res_tracking_id":29204,"res_dp_mall_id":1336,"res_response_msg":"Berhasil","res_approval_code":"253961","res_trans_id_merchant":"invoice_1461748645","res_payment_channel_code":"01","res_status":"Success","res_bank":"CASH","res_response_code":"0000","res_payment_channel":"04"}
                    return json_encode($responsePayment);
                //} else {
                //    // Return pre-payment result
                //    return json_encode($responsePrePayment);
                //}
            } elseif ($paymentchannel == Doku::MANDIRI_CLICKPAY) {
                // For Mandiri Clickpay
                $params = [
                    'amount' => $amount,
                    'invoice' => $invoiceNumber,
                    'currency' => $currency
                ];
                $words = Doku::doCreateWords($params);

                $cc = isset($postParam['cc_number']) ? $postParam['cc_number'] : '';
                $cc = str_replace(" - ", "", $cc);
                $challengeCode1 = isset($postParam['CHALLENGE_CODE_1']) ? $postParam['CHALLENGE_CODE_1'] : '';
                $challengeCode2 = isset($postParam['CHALLENGE_CODE_2']) ? $postParam['CHALLENGE_CODE_2'] : '';
                $challengeCode3 = isset($postParam['CHALLENGE_CODE_3']) ? $postParam['CHALLENGE_CODE_3'] : '';
                $responseToken = isset($postParam['response_token']) ? $postParam['response_token'] : '';

                $dataPayment = [
                    'req_mall_id' => Doku::$mallId,
                    'req_chain_merchant' => $chainMerchant,
                    'req_amount' => $amount,
                    'req_words' => $words,
                    'req_purchase_amount' => $amount,
                    'req_trans_id_merchant' => $invoiceNumber,
                    'req_request_date_time' => date('YmdHis'),
                    'req_currency' => $currency,
                    'req_purchase_currency' => $currency,
                    'req_session_id' => sha1(date('YmdHis')),
                    'req_payment_channel' => '02',
                    'req_name' => $customer['name'],
                    'req_email' => $customer['data_email'],
                    'req_card_number' => $cc,
                    'req_mobile_phone' => $customer['data_phone'],
                    'req_basket' => $basket,
                    'req_challenge_code_1' => $challengeCode1,
                    'req_challenge_code_2' => $challengeCode2,
                    'req_challenge_code_3' => $challengeCode3,
                    'req_response_token' => $responseToken,
                    'req_address' => $customer['data_address'],
                    'req_expiry_time' => $relatedOrder->getOrderExpiryInMinutes()
                ];

                $response = Doku::doDirectPayment($dataPayment);

                /*
                "res_response_msg":"SUCCESS",
                "res_transaction_code":"d4efbb8c4ebb9a3597c05aa32f2b341e77f98e63",
                "res_mcn":"4***********1111",
                "res_approval_code":"1234",
                "res_trans_id_merchant":"1706332101",
                "res_payment_date":"20160319184828",
                "res_bank":"MANDIRI CLICK PAY",
                "res_amount":"30000.00",
                "res_message":"PAYMENT APPROVED",
                "res_response_code":"0000",
                "res_session_id":"50d240541f4d8d7565b18cb5ca93a660"
                */
                if ($response->res_response_code == Doku::DOKU_SUCCESS){
                    // Merchant process ...
                    $this->dokuMerchantProcess($relatedOrder, $response->res_transaction_code, $response->res_response_code, json_encode($response));
                    // Render Result
                    return View::make('frontend.payment.dokuiframesuccess', [
                        'words' => $words,
                        'order' => $relatedOrder,
                        'mallId' => Doku::$mallId,
                        'params' => $params,
                        'paymentchannel' => $paymentchannel,
                        'generatedPaymentCode' => $response
                    ]);
                } else {
                    // Merchant process ...
                    $this->dokuMerchantProcess($relatedOrder, (isset($response->res_transaction_code) ? $response->res_transaction_code : '00000000'), $response->res_response_code, json_encode($response));
                    // Render Result
                    // dd($response);
                    return View::make('frontend.payment.dokuiframefailed', [
                        'words' => $words,
                        'order' => $relatedOrder,
                        'mallId' => Doku::$mallId,
                        'params' => $params,
                        'paymentchannel' => $paymentchannel,
                        'generatedPaymentCode' => null
                    ]);
                }
            } else {
                // For VA Bank Transfer, Convenience Store
                $params = [
                    'amount' => $amount,
                    'invoice' => $invoiceNumber,
                    'currency' => $currency
                ];
                $words = Doku::doCreateWords($params);

                $dataPayment = [
                    'req_mall_id' => Doku::$mallId,
                    'req_chain_merchant' => $chainMerchant,
                    'req_amount' => $amount,
                    'req_words' => $words,
                    'req_purchase_amount' => $amount,
                    'req_trans_id_merchant' => $invoiceNumber,
                    'req_request_date_time' => date('YmdHis'),
                    'req_session_id' => sha1(date('YmdHis')),
                    'req_name' => $customer['name'],
                    'req_email' => $customer['data_email'],
                    'req_mobile_phone' => $customer['data_phone'],
                    'req_basket' => $basket,
                    'req_address' => $customer['data_address'],
                    'req_expiry_time' => $relatedOrder->getOrderExpiryInMinutes()
                ];

                $response = Doku::doGeneratePaycode($dataPayment);

                if ($response->res_response_code == Doku::DOKU_SUCCESS){
                    return View::make('frontend.payment.dokuiframesuccess', [
                        'words' => $words,
                        'order' => $relatedOrder,
                        'mallId' => Doku::$mallId,
                        'params' => $params,
                        'paymentchannel' => $paymentchannel,
                        'generatedPaymentCode' => $response
                    ]);
                } else {
                    return View::make('frontend.payment.dokuiframefailed', [
                        'words' => $words,
                        'order' => $relatedOrder,
                        'mallId' => Doku::$mallId,
                        'params' => $params,
                        'paymentchannel' => $paymentchannel,
                        'generatedPaymentCode' => null
                    ]);
                }
            }
        }
        return json_encode([
            'status' => "FAILED",
            'request' => $postParam
        ]);
    }

    public function dokuMerchantProcess(PayableContract $relatedOrder, $resTrackingId, $resResponseCode, $rawJson) {
        if ($relatedOrder) {
            $thirdpartyPaymentProcess = new ThirdpartyPaymentProcess;
            $newparam = [];
            $newparam['transaction_id'] = $resTrackingId;
            $newparam['order_id'] = $relatedOrder->id;
            $newparam['order_full_classname'] = $relatedOrder->getOrderFullClassName();
            $newparam['order_code'] = $relatedOrder->getOrderCode();
            if ($relatedOrder->getPaymentMethodObject()) {
                $module = $relatedOrder->getPaymentMethodObject()->getThirdpartyModule();
                if ($module) {
                    $newparam['thirdparty_module_id'] = $module->id;
                }
            }
            // check status
            if ($resResponseCode == Doku::DOKU_SUCCESS) {
                $newparam['status'] = ThirdpartyPaymentProcess::STATUS_SUCCESS; // 'success' is successfull / ok status from thirdparty payment gateway
            } elseif ($resResponseCode == Doku::DOKU_PENDING ||
                $resResponseCode == Doku::DOKU_AUTHORIZE) {
                $newparam['status'] = ThirdpartyPaymentProcess::STATUS_PENDING; 
            } else {
                // DENY or EXPIRE or any undefined
                $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
            }
            $newparam['json_result'] = $rawJson;
            $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
            if ($result) {
                $this->paymentRepo->processImpact($result, $relatedOrder); 
                // more if needed
            }
        }
    }

    /*
        DOKU Post Identify that accessed by Doku Server
    */
    public function postDokuIdentify(Request $request)
    {
        try {
            $result = $request->all();
            // render
            $orderCode = $result['TRANSIDMERCHANT'];
            $orderCode = str_replace('_', '/', $orderCode);

            $relatedOrderClassObject = $this->basePayableModel;
            $order = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$orderCode)->first();
            if ($order) {
                $amount = $result['AMOUNT'];
                $paymentChannel = "D" . $result['PAYMENTCHANNEL'];
                $sessionId = $result['SESSIONID'];
                // SPECIFIC CASE SECTION
                if (Doku::$isProduction) {
                    if ($paymentChannel == Doku::BANKTRANSFER_PRODUCTION) {
                        $paymentChannel = Doku::BANKTRANSFER;
                    }
                    if ($paymentChannel == Doku::CSTORE_PRODUCTION) {
                        $paymentChannel = Doku::CSTORE;
                    }
                }
                // END OF SPECIFIC CASE SECTION
                if ($order->getTotalPrice() . ".00" == $amount &&
                    $order->paymentMethod &&
                    trim($order->paymentMethod->thirdparty_module_enabled_payment) == $paymentChannel) {
                    // success
                    return "OK";
                }
            }
        } catch(Exception $e) { }
        // failed
        return "UNKNOWN";
    }

    /*
        DOKU Post Notification that accessed by Doku Server
    */
    public function postDokuNotification(Request $request) 
    {
        try {
            \Debugbar::disable();
        } catch (\Exception $e) { }

        $ipAddress = $request->ip();
        info(__CLASS__.'@'.__METHOD__, compact('ipAddress'));

        if (!starts_with($ipAddress, '103.10.129.')) {
            // return "CONTINUE";
        }

        /*
        $whiteListIP = [
            '127.0.0.1', '::1', '103.10.129.0','103.10.129.1', '103.10.129.2', '103.10.129.3', '103.10.129.4', '103.10.129.5', '103.10.129.6', '103.10.129.7', '103.10.129.8', '103.10.129.9', '103.10.129.10', '103.10.129.11', '103.10.129.12', '103.10.129.13', '103.10.129.14', '103.10.129.15', '103.10.129.16', '103.10.129.17', '103.10.129.18', '103.10.129.19', '103.10.129.20', '103.10.129.21', '103.10.129.22', '103.10.129.23', '103.10.129.24', '103.10.129.130', '103.10.129.206'
        ];
        if (!in_array($ipAddress, $whiteListIP)) {
            return "CONTINUE";
        }
        */

        $postParam = $request->all();

        info(__CLASS__.'@'.__METHOD__, compact('postParam'));

        //Doku::$sharedKey = env('DOKU_SHARED_KEY', '1lk8kCqRZ40M');
        //Doku::$isProduction = env('DOKU_IS_PRODUCTION', false);
        //Doku::$mallId = env('DOKU_MALL_ID', '4222');

        // [1] initial order
        $notifiedWords = isset($postParam['WORDS']) ? $postParam['WORDS'] : '';
        $invoiceNumber = isset($postParam['TRANSIDMERCHANT']) ? $postParam['TRANSIDMERCHANT'] : '';
        $amount = isset($postParam['AMOUNT']) ? $postParam['AMOUNT'] : 0;
        $responseCode = isset($postParam['RESPONSECODE']) ? $postParam['RESPONSECODE'] : '9999';
        $verifyStatus = isset($postParam['VERIFYSTATUS']) ? $postParam['VERIFYSTATUS'] : 'NA';
        $resultMessage = isset($postParam['RESULTMSG']) ? $postParam['RESULTMSG'] : 'FAILED';
        $sessionId = isset($postParam['SESSIONID']) ? $postParam['SESSIONID'] : '';
        $notifiedPaymentChannel = "D" . (isset($postParam['PAYMENTCHANNEL']) ? $postParam['PAYMENTCHANNEL'] : 'NA');
        // PAYMENTDATETIME

        // Identify Order
        $relatedOrderCode = str_replace('_', '/', $invoiceNumber);
        $relatedOrderClassObject = $this->basePayableModel;
        $relatedOrder = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$relatedOrderCode)->first();

        if (!empty($sessionId) &&
            $responseCode == Doku::DOKU_SUCCESS &&
            $relatedOrder) {
            $paymentMethod = $relatedOrder->getPaymentMethodObject();

            if ($paymentMethod &&
                $paymentMethod->thirdparty_module_enabled_payment == Doku::CREDITCARDINSTALLMENT) {
                Doku::$mallId = Doku::$ccInstallmentMallId;
            }

            $words = Doku::doCreateWords([
                'amount' => $amount,
                'invoice' => $invoiceNumber,
                'result_message' => $resultMessage,
                'verify_status' => $verifyStatus
            ]);
            // SPECIFIC CASE SECTION
            if (Doku::$isProduction) {
                if ($notifiedPaymentChannel == Doku::BANKTRANSFER_PRODUCTION) {
                    $notifiedPaymentChannel = Doku::BANKTRANSFER;
                }
                if ($notifiedPaymentChannel == Doku::CSTORE_PRODUCTION) {
                    $notifiedPaymentChannel = Doku::CSTORE;
                }
            }
            // END OF SPECIFIC CASE SECTION
            if ($words == $notifiedWords &&
                ( ($paymentMethod->thirdparty_module_enabled_payment == $notifiedPaymentChannel) ||
                  ($paymentMethod->thirdparty_module_enabled_payment == Doku::CREDITCARDINSTALLMENT && $notifiedPaymentChannel == Doku::CREDITCARD)
                )) {
                if (env('DOKU_SECURE_CHECK', false)) {
                    $job = (new DokuCheck($invoiceNumber, $sessionId))->delay(Carbon::now()->addSeconds(15));
                    dispatch($job);
                } else {
                    // Merchant process ...
                    if ($resultMessage != 'SUCCESS' && $verifyStatus != 'APPROVE') {
                        $responseCode = '9999'; // Local Generated Code for identifier
                    }
                    $this->dokuMerchantProcess($relatedOrder, $sessionId, $responseCode, json_encode($postParam));
                }
                return "CONTINUE";
            }
        }
        return "FAILED";
    }

    /*
        DOKU Post Result that accessed by Doku Server
    */
    public function postDokuResult(Request $request)
    {
        try {
            $result = $request->all();
            // render
            $orderCode = $result['TRANSIDMERCHANT'];
            $orderCode = str_replace('_', '/', $orderCode);
            $relatedOrderClassObject = $this->basePayableModel;
            $order = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$orderCode)->first();
            if ($order) {
                $amount = $result['AMOUNT'];
                $paymentChannel = "D" . $result['PAYMENTCHANNEL'];
                $sessionId = $result['SESSIONID'];
                // SPECIFIC CASE SECTION
                if (Doku::$isProduction) {
                    if ($paymentChannel == Doku::BANKTRANSFER_PRODUCTION) {
                        $paymentChannel = Doku::BANKTRANSFER;
                    }
                    if ($paymentChannel == Doku::CSTORE_PRODUCTION) {
                        $paymentChannel = Doku::CSTORE;
                    }
                }
                // END OF SPECIFIC CASE SECTION
                if ($order->getTotalPrice() . ".00" == $amount &&
                    $order->paymentMethod &&
                    trim($order->paymentMethod->thirdparty_module_enabled_payment) == $paymentChannel) {
                    // success
                    return "OK";
                }
            }
        } catch(Exception $e) { }
        // failed
        return "UNKNOWN";
    }

    public function postDokuFinish(Request $request)
    {
        return "NOT IMPLEMENTED YET"; // should be implemented on child class
    }

    // SPECIFIC FOR IPAY88 ====================================================
    public function getIpay88InitialWeb(PayableContract $payableItem, $ignorePaymentMethod = false, $mobileAdaptiveView = false)
    {
        try {
            \Debugbar::disable();
        } catch (\Exception $e) { }
        if ($payableItem) {
            $matchedModule = $ignorePaymentMethod;
            if (!$ignorePaymentMethod && $payableItem->getPaymentMethod()) {
                $module = $payableItem->getPaymentMethod()->thirdparty_module_code;
                $matchedModule = ($module == self::IPAY88);
            }
            // Existed Unpaid (Had Checked Out) Order with Veritrans Payment Method
            if ($matchedModule) {
                // [1] populate payableItem detail
                $safeOrderCode = str_replace('/', '_', $payableItem->getOrderCode());
                $paymentMethod = $payableItem->getPaymentMethodObject();
                $paymentchannel = $paymentMethod ? $paymentMethod->thirdparty_module_enabled_payment : Ipay88::CREDITCARD;

                // [2] populate customer info
                $addressStreet = ($payableItem->getRecipientStreetName() ? $payableItem->getRecipientStreetName() : 'N/A');
                $validatedStreet = str_limit($addressStreet, 195);
                $customer = [
                    'name'    => (!empty($payableItem->getRecipientName()) ? $payableItem->getRecipientName() : "Raw User"),
                    'data_phone' => $payableItem->getRecipientPhone(),
                    'data_email' => $payableItem->getRecipientEmail(),
                    'data_address' => $validatedStreet
                ];

                // [3] Generate Transaction Data
                $magicAmount = ($payableItem->getTotalPrice() * 100);
                $dataPayment = [
                    'MerchantCode' => Ipay88::$merchantCode,
                    'PaymentId' => str_replace("IP", "", $paymentchannel),
                    'RefNo' => $safeOrderCode,
                    'Amount' => $magicAmount,
                    'Currency' => Ipay88::CURRENCY_IDR,
                    'ProdDesc' => 'Transaction #' . $payableItem->getOrderCode(),
                    'UserName' => $customer['name'],
                    'UserEmail' => $customer['data_email'],
                    'UserContact' => $customer['data_phone'],
                    'Remark' => '',
                    'Lang' => 'ISO-8859-1',
                    'Signature' => Ipay88::generateRequestPageSignature($safeOrderCode, $magicAmount),
                    'ResponseURL' => $this->responseRedirectUrl,
                    'BackendURL' => $this->backendUrl
                ];
                // render
                return View::make(($mobileAdaptiveView ? 'frontend.payment.ipay88hostedfullpagemobile' : 'frontend.payment.ipay88hostedfullpage'), [
                    'order' => $payableItem,
                    'paymentMethod' => $paymentMethod,
                    'dataPayment' => $dataPayment,
                    'url' => Ipay88::getIpay88HostedInitUrl()
                ]);
            }
        }
        return App::abort(404);
    }

    public function getIpay88ResponsePage($params, $mobileAdaptiveView = false)
    {
        $paymentResult = array_only($params, ['MerchantCode', 'PaymentId', 'RefNo', 'Amount', 'Currency', 'Remark', 'TransId', 'AuthCode', 'Status', 'ErrDesc', 'Signature', 'VirtualAccountAssigned']);
        // render
        $orderCode = $paymentResult['RefNo'];
        $orderCode = str_replace('_', '/', $orderCode);
        $relatedOrderClassObject = $this->basePayableModel;
        $order = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$orderCode)->first();
        $responseSignature = Ipay88::generateResponsePageSignature($orderCode, $paymentResult['Amount'], $paymentResult['PaymentId'], $paymentResult['Status']);
        //dd($paymentResult);
        if ($order) {
            if ($paymentResult['Signature'] == $responseSignature) {
                $newparam = [];
                $thirdpartyPaymentProcess = new ThirdpartyPaymentProcess;
                if (env('ENABLE_IPAY88_CONFIRMATION_ON_DIRECT_RESPONSE', false)) {
                    $newparam['transaction_id'] = $paymentResult['TransId'];
                    $newparam['order_id'] = $order->id;
                    $newparam['order_full_classname'] = $order->getOrderFullClassName();
                    $newparam['order_code'] = $order->getOrderCode();
                    if ($order->getPaymentMethodObject()) {
                        $module = $order->getPaymentMethodObject()->getThirdpartyModule();
                        if ($module) {
                            $newparam['thirdparty_module_id'] = $module->id;
                        }
                    }
                    $newparam['json_result'] = json_encode($paymentResult);
                }
                if ($paymentResult['Status'] == 1) {
                    // SUCCESS
                    if (env('ENABLE_IPAY88_CONFIRMATION_ON_DIRECT_RESPONSE', false)) {
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_SUCCESS;
                        $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
                        if ($result) {
                            $this->paymentRepo->processImpact($result, $order); 
                            // more if needed
                        }
                    }
                    if ($mobileAdaptiveView) {
                        return view('frontend.payment.ipay88successmobile', compact('order'));
                    }
                    return view('frontend.payment.ipay88success', compact('order'));
                } elseif ($paymentResult['Status'] == 6) {
                    // SUCCESS
                    if (env('ENABLE_IPAY88_CONFIRMATION_ON_DIRECT_RESPONSE', false)) {
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_PENDING;
                        $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
                        if ($result) {
                            $this->paymentRepo->processImpact($result, $order); 
                            // more if needed
                        }
                    }
                    $errorDesc = $paymentResult['ErrDesc'];
                    $VA = (isset($paymentResult['VirtualAccountAssigned']) ? $paymentResult['VirtualAccountAssigned'] : 'unknown');
                    if ($mobileAdaptiveView) {
                        return view('frontend.payment.ipay88pendingmobile', compact('order', 'errorDesc', 'VA'));
                    }
                    return view('frontend.payment.ipay88pending', compact('order', 'errorDesc', 'VA'));
                } else {
                    // FAILED
                    if (env('ENABLE_IPAY88_CONFIRMATION_ON_DIRECT_RESPONSE', false)) {
                        $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
                        $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
                        if ($result) {
                            $this->paymentRepo->processImpact($result, $order); 
                            // more if needed
                        }
                    }
                }
            }
            // failed
            $errorDesc = $paymentResult['ErrDesc'];
            if ($mobileAdaptiveView) {
                return view('frontend.payment.ipay88failedmobile', compact('order', 'errorDesc'));
            }
            return view('frontend.payment.ipay88failed', compact('order', 'errorDesc'));
        }
        return App::abort(404);
    }

    public function postIpay88Notification(Request $request)
    {
        $paymentResult = array_only($request->all(), ['MerchantCode', 'PaymentId', 'RefNo', 'Amount', 'Currency', 'Remark', 'TransId', 'AuthCode', 'Status', 'ErrDesc', 'Signature']);
        // render
        $orderCode = $paymentResult['RefNo'];
        $orderCode = str_replace('_', '/', $orderCode);
        $relatedOrderClassObject = $this->basePayableModel;
        $order = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$orderCode)->first();
        $responseSignature = Ipay88::generateResponsePageSignature($orderCode, $paymentResult['Amount'], $paymentResult['PaymentId'], $paymentResult['Status']);
        if ($order && $paymentResult['Signature'] == $responseSignature) {
            $thirdpartyPaymentProcess = new ThirdpartyPaymentProcess;
            $newparam = [];
            $newparam['transaction_id'] = $paymentResult['TransId'];
            $newparam['order_id'] = $order->id;
            $newparam['order_full_classname'] = $order->getOrderFullClassName();
            $newparam['order_code'] = $order->getOrderCode();
            if ($order->getPaymentMethodObject()) {
                $module = $order->getPaymentMethodObject()->getThirdpartyModule();
                if ($module) {
                    $newparam['thirdparty_module_id'] = $module->id;
                }
            }
            // check status
            $resResponseCode = $paymentResult['Status'];
            if ($resResponseCode == Ipay88::IPAY88_SUCCESS) {
                $newparam['status'] = ThirdpartyPaymentProcess::STATUS_SUCCESS; // 'success' is successfull / ok status from thirdparty payment gateway
            } elseif ($resResponseCode == Ipay88::IPAY88_OTHER) {
                $newparam['status'] = ThirdpartyPaymentProcess::STATUS_PENDING; // 'pending' is pending / pending status from thirdparty payment gateway
            } else {
                // FAILED
                $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
            }
            $newparam['json_result'] = json_encode($paymentResult);
            $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
            if ($result) {
                $this->paymentRepo->processImpact($result, $order); 
                // more if needed
            }
            return "RECEIVEOK";
        }
        // failed
        return "RECEIVEFAILED";
    }

    // FOR NON PAYMENT GATEWAY NOTIFICATION
    // Ex. : "manualtransfer", "offline", etc
    public function postNonPaymentGatewayNotification(Request $request)
    {
        try {
            $paymentResult = array_only($request->all(), ['RefNo', 'Amount', 'TransId', 'Status', 'Signature']);
            // render
            $orderCode = $paymentResult['RefNo'];
            $orderCode = str_replace('_', '/', $orderCode);
            $relatedOrderClassObject = $this->basePayableModel;
            $order = $relatedOrderClassObject->where($relatedOrderClassObject->getOrderCodeField(),'=',$orderCode)->first();
            $responseSignature = OtherPayment::generateResponsePageSignature($orderCode, $paymentResult['Amount'], $paymentResult['Status']);
            if ($order && 
                $order->getTotalPrice() <= intval($paymentResult['Amount']) &&
                $paymentResult['Signature'] == $responseSignature) {
                $thirdpartyPaymentProcess = new ThirdpartyPaymentProcess;
                $newparam = [];
                $newparam['transaction_id'] = $paymentResult['TransId'];
                $newparam['order_id'] = $order->id;
                $newparam['order_full_classname'] = $order->getOrderFullClassName();
                $newparam['order_code'] = $order->getOrderCode();
                if ($order->getPaymentMethodObject()) {
                    $module = $order->getPaymentMethodObject()->getThirdpartyModule();
                    if ($module) {
                        $newparam['thirdparty_module_id'] = $module->id;
                    }
                }
                // check status
                $resResponseCode = $paymentResult['Status'];
                if ($resResponseCode == OtherPayment::OTHERPAYMENT_SUCCESS) {
                    $newparam['status'] = ThirdpartyPaymentProcess::STATUS_SUCCESS; // 'success' is successfull / ok status from thirdparty payment gateway
                } else {
                    // FAILED
                    $newparam['status'] = ThirdpartyPaymentProcess::STATUS_FAILED; 
                }
                $newparam['json_result'] = json_encode($paymentResult);
                $result = $this->paymentRepo->create($newparam, $thirdpartyPaymentProcess);
                if ($result) {
                    $this->paymentRepo->processImpact($result, $order); 
                    // more if needed
                }
                return "NOTIFICATION-RECEIVED-OK";
            }  
        } catch (Exception $e) { }
        // failed
        return "NOTIFICATION-RECEIVED-FAILED";
    }
}
