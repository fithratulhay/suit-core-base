<?php

namespace Suitpay\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Models\Bank;
use Suitpay\Repositories\Contract\BankRepositoryContract;

class BankRepository implements BankRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Bank $model)
    {
        $this->mainModel = $model;
    }

    public function getOptions() {
    	return $this->mainModel->getOptions();
    }
}
