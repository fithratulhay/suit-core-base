<?php

namespace Suitpay\Repositories\Facades;

class VeritransInstallmentTenor extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\VeritransInstallmentTenorRepositoryContract';
    }
}
