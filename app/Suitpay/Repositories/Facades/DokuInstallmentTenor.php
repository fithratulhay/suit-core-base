<?php

namespace Suitpay\Repositories\Facades;

class DokuInstallmentTenor extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\DokuInstallmentTenorRepositoryContract';
    }
}
