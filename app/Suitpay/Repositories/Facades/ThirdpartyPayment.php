<?php

namespace Suitpay\Repositories\Facades;

class ThirdpartyPayment extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\ThirdpartyPaymentRepositoryContract';
    }
}
