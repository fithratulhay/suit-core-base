<?php

namespace Suitpay\Repositories\Facades;

class DokuBank extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\DokuBankRepositoryContract';
    }
}
