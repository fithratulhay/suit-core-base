<?php

namespace Suitpay\Repositories\Facades;

class Bank extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\BankRepositoryContract';
    }
}
