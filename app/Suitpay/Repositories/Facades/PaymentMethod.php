<?php

namespace Suitpay\Repositories\Facades;

class PaymentMethod extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\PaymentMethodRepositoryContract';
    }
}
