<?php

namespace Suitpay\Repositories\Facades;

class VeritransBank extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\VeritransBankRepositoryContract';
    }
}
