<?php

namespace Suitpay\Repositories\Facades;

class ThirdPartyModule extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitpay\Repositories\Contract\ThirdPartyModuleRepositoryContract';
    }
}
