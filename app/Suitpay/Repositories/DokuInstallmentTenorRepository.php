<?php

namespace Suitpay\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Models\DokuInstallmentTenor;
use Suitpay\Repositories\Contract\DokuInstallmentTenorRepositoryContract;

class DokuInstallmentTenorRepository implements DokuInstallmentTenorRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(DokuInstallmentTenor $model)
    {
        $this->mainModel = $model;
    }
}
