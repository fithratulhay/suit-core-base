<?php

namespace Suitpay\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Models\VeritransInstallmentTenor;
use Suitpay\Repositories\Contract\VeritransInstallmentTenorRepositoryContract;

class VeritransInstallmentTenorRepository implements VeritransInstallmentTenorRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(VeritransInstallmentTenor $model)
    {
        $this->mainModel = $model;
    }
}
