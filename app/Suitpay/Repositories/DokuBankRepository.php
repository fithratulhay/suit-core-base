<?php

namespace Suitpay\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Repositories\Contract\DokuBankRepositoryContract;
use Suitpay\Models\DokuBank;

class DokuBankRepository implements DokuBankRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(DokuBank $model)
    {
        $this->mainModel = $model;
    }

    public function getActiveCreditCards()
    {
        return $this->mainModel->active()->orderBy('name', 'asc')->get();
    }

    public function getCachedList()
    {
        $creditCardList = [];
        $baseModel = $this->mainModel;
        $creditCardList = Cache::rememberForever('doku_creditcard_active_list', function () use($baseModel) {
            return $baseModel->active()->orderBy('name', 'asc')->pluck('name', 'id');
        });
        return $creditCardList;
    }

    public function getRawCachedList()
    {
        $creditCardList = [];
        $baseModel = $this->mainModel;
        $creditCardList = Cache::rememberForever('raw_doku_creditcard_active_list', function () use($baseModel) {
            return $baseModel->active()->orderBy('name', 'asc')->get();
        });
        return $creditCardList;
    }

    public function getVisibleList()
    {
        $creditCardList = [];
        $baseModel = $this->mainModel;
        $creditCardList = Cache::rememberForever('doku_creditcard_footer_list', function () use($baseModel) {
            return $baseModel->visible()->orderBy('name', 'asc')->get();
        });
        return $creditCardList;
    }
}
