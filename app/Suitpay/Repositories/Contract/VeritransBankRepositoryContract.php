<?php

namespace Suitpay\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface VeritransBankRepositoryContract extends SuitRepositoryContract
{
    public function getActiveCreditCards();
    public function getCachedList();
    public function getRawCachedList();
    public function getVisibleList();
}
