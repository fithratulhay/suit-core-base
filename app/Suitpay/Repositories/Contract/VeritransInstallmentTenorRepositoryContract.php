<?php

namespace Suitpay\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface VeritransInstallmentTenorRepositoryContract extends SuitRepositoryContract
{
}
