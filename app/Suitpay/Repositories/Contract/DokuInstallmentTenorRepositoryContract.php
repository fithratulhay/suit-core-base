<?php

namespace Suitpay\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface DokuInstallmentTenorRepositoryContract extends SuitRepositoryContract
{
}
