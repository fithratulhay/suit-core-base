<?php

namespace Suitpay\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;
use Suitpay\Models\Contracts\PayableContract;
use Suitpay\Models\ThirdpartyPaymentProcess;

interface ThirdpartyPaymentProcessRepositoryContract extends SuitRepositoryContract
{
	public function processImpact(ThirdpartyPaymentProcess $paymentProcess, PayableContract $model);
}
