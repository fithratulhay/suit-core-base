<?php

namespace Suitpay\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface PaymentMethodRepositoryContract extends SuitRepositoryContract
{
    public function getTotalPaymentFee($paymentMethodId, $orderTotalPrice = 0);
}
