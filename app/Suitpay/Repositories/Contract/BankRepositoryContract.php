<?php

namespace Suitpay\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface BankRepositoryContract extends SuitRepositoryContract
{
    public function getOptions();
}
