<?php

namespace Suitpay\Repositories;

use Suitpay\Models\Contracts\PayableContract;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Models\ThirdpartyPaymentProcess;
use Suitpay\Repositories\Contract\ThirdpartyPaymentProcessRepositoryContract;

class ThirdpartyPaymentProcessRepository implements ThirdpartyPaymentProcessRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(ThirdpartyPaymentProcess $model)
    {
        $this->mainModel = $model;
    }

    public function processImpact(ThirdpartyPaymentProcess $paymentProcess, PayableContract $payableModel) {
    	if ($paymentProcess && 
    		$paymentProcess->id && 
    		$payableModel &&
    		$payableModel->id) {
    		if ($paymentProcess->status == $this->mainModel::STATUS_SUCCESS) {
    			$payableModel->successPayment();
    		} elseif ($paymentProcess->status == $this->mainModel::STATUS_PENDING) {
    			$payableModel->pendingPayment();
    		} elseif ($paymentProcess->status == $this->mainModel::STATUS_FAILED) {
    			$payableModel->failedPayment();
    		} elseif ($paymentProcess->status == $this->mainModel::STATUS_REFUND) {
    			$payableModel->refundPayment();
    		} elseif ($paymentProcess->status == $this->mainModel::STATUS_CANCEL) {
                $payableModel->cancelPayment();
            } elseif ($paymentProcess->status == $this->mainModel::STATUS_REINIT) {
                $payableModel->reinitPayment();
            }
    	}
    }
}
