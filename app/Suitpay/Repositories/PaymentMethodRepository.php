<?php

namespace Suitpay\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Models\PaymentMethod;
use Suitpay\Repositories\Contract\PaymentMethodRepositoryContract;

class PaymentMethodRepository implements PaymentMethodRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(PaymentMethod $model)
    {
        $this->mainModel = $model;
    }

    // BEGIN OBSERVER
    public function saved(PaymentMethod $model) {
        Cache::forget('payment-method-active');
    }

    public function deleted(PaymentMethod $model) {
        Cache::forget('payment-method-active');
    }
    // END OBSERVER

    public function getActivePaymentMethod() {
        $baseModel = $this->mainModel;
        return Cache::rememberForever('payment-method-active', function () use($baseModel) {
            return $baseModel->active()->orderBy('type', 'asc')->orderBy('title', 'asc')->get();
        });
    }

    /**
     * Get total payment fee of a payment method
     * @param  int  $paymentMethodId
     * @param  int $orderTotalPrice
     * @return int|NULL
     */
    public function getTotalPaymentFee($paymentMethodId, $orderTotalPrice = 0)
    {
        $paymentMethod = PaymentMethod::find($paymentMethodId);
        if ($paymentMethod) {
            $optionalPercentagetoFix = 0;
            $feeAccumulation = ($paymentMethod->admin_fee + $paymentMethod->optional_fixed_fee + $paymentMethod->tax);
            if ($paymentMethod->optional_percentage_fee && $paymentMethod->optional_percentage_fee < 100) {
                /*
                // $optionalPercentagetoFix = ($orderTotalPrice * 100 / (100 - $paymentMethod->optional_percentage_fee) + $feeAccumulation) - ($orderTotalPrice + $feeAccumulation);
                $optionalPercentagetoFix = ($orderTotalPrice * 100 / (100 - $paymentMethod->optional_percentage_fee) + $paymentMethod->admin_fee) - ($orderTotalPrice + $paymentMethod->admin_fee);
                */
                $optionalPercentagetoFix = ((($orderTotalPrice + $feeAccumulation) * 100) / (100 - $paymentMethod->optional_percentage_fee)) - ($orderTotalPrice + $feeAccumulation);
            }
            $total = $feeAccumulation + ceil($optionalPercentagetoFix);
            //if ($total < $paymentMethod->minimum_admin_fee) {
            //    return $paymentMethod->minimum_admin_fee;
            //}
            return round($total);
        }
        return null;
    }
}
