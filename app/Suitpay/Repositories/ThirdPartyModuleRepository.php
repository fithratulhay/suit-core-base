<?php

namespace Suitpay\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitpay\Models\ThirdPartyModule;
use Suitpay\Repositories\Contract\ThirdPartyModuleRepositoryContract;

class ThirdPartyModuleRepository implements ThirdPartyModuleRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(ThirdPartyModule $model)
    {
        $this->mainModel = $model;
    }
}
