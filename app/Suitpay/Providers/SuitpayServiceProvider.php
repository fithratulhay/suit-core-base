<?php

namespace Suitpay\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Suitcore\Providers\LoadRepositoryConfigTrait;


class SuitpayServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any suitcore services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any suitcore services.
     *
     * @return void
     */
    public function register()
    {
        $repositories = config('suitpay.repositories');
        $this->loadFromConfig($repositories, false, config('suitpay.custom_base_model'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitpay.repositories');
        return array_keys($repositories);
    }

}
