<?php

namespace Suitpay\ThirdpartyAPI\Ipay88;

use Suitpay\Exceptions\Ipay88Exception;

/*
List of Error Messages
======================
Duplicate reference number : Reference number must be unique for each transaction.
Invalid merchant : The merchant code does not exist or wrong merchant code.
Invalid parameters : Some parameter posted to iPay88 is invalid or empty
Overlimit per transaction : You exceed the amount value per transaction.
Payment not allowed : The Payment method you requested is not allowed for this merchant code, please contact iPay88 to enable your payment option.
Permission not allow : Request URL registered in iPay88 merchant account does not match. Please register your website Request URL with iPay88.
Signature not match : The Signature generated is incorrect.
Status not approved : Account was suspended or not active.
*/

Class Ipay88
{
  // PAYMENT TYPE
  // -- Credit Card
  const CREDITCARD = "IP1"; // IDR
  // -- Bank Transfer
  const BANKTRANSFER_BII = "IP9"; // IDR
  const BANKTRANSFER_MANDIRI = "IP17"; // IDR
  const VA_BCA = "IP30"; // IDR
  // -- Internet Banking (Direct Debit)
  const BCA_KLIKPAY = "IP8"; // IDR, Old = IP123
  const MANDIRI_CLICKPAY = "IP4"; // IDR
  const CIMB_CLICKS = "IP11"; // IDR
  const MUAMALAT_IB = "IP14"; // IDR
  // -- E-Wallet
  const XL_TUNAI = "IP7"; // IDR
  const KARTUKU = "IP10"; // IDR
  const MANDIRI_ECASH = "IP13"; // IDR
  const TELKOMSEL_CASH = "IP15"; // IDR
  const INDOSAT_DOMPETKU = "IP16"; // IDR
  const PAY4ME = "IP22"; // IDR
  const PAYPAL = "IP6"; // USD

  // CURRENCY
  const CURRENCY_IDR = "IDR";
  const CURRENCY_USD = "USD";

  // TRANSACTION STATUS
  const IPAY88_SUCCESS = '1';
  const IPAY88_DENY = '0';
  const IPAY88_OTHER = '6'; //PENDING for VA BCA, APPROVED for KlikPay BCA

  // PAYMENT TYPE
  const FULL_PAYMENT = '01';
  const INSTALLMENT_PAYMENT = '02';
  const MIXED_PAYMENT = '03'; // 01 & 02

  // ENDPOINT URL
  const SANDBOX_IPAY88HOSTED_URL = 'https://sandbox.ipay88.co.id/epayment';
  const PRODUCTION_IPAY88HOSTED_URL = 'https://payment.ipay88.co.id/epayment';

  // REDIRECT URL FOR BCA KLIKPAY
  const SANDBOX_IPAY88REDIRECTED_URL_BCA_KLIKPAY = 'https://sandbox.ipay88.co.id/klikpaybca/purchasing/purchase.asp?action=loginRequest';
  const PRODUCTION_IPAY88REDIRECTED_URL_BCA_KLIKPAY = 'https://klikpay.klikbca.com/purchasing/purchase.do?action=loginRequest';

  const IPAY88_HOSTED_CHECK_STATUS_URL = '/enquiry.asp';
  const IPAY88_HOSTED_INIT_URL = '/entry.asp';
  const IPAY88_HOSTED_INIT_URL_BCA_KLIKPAY = '/WebService/SeamlessPayment/Entry.asmx';

  public static $isProduction = false; // production state
  public static $sharedKey = ''; // merchant unique key
  public static $merchantCode = ''; // merchant code
  public static $klikPayCode = ''; // merchant klikPay Code for BCA Klikpay
  public static $clearKey = ''; // merchant clearKey for BCA Klikpay

  public static function getIpay88HostedInitUrl() {
    return (self::$isProduction ? self::PRODUCTION_IPAY88HOSTED_URL : self::SANDBOX_IPAY88HOSTED_URL) . self::IPAY88_HOSTED_INIT_URL;
  }

  public static function getIpay88HostedInitUrlBCAKlikpay() {
    return (self::$isProduction ? self::PRODUCTION_IPAY88HOSTED_URL : self::SANDBOX_IPAY88HOSTED_URL) . self::IPAY88_HOSTED_INIT_URL_BCA_KLIKPAY;
  }

  public static function getIpay88RedirectUrl() {
    return (self::$isProduction ? self::PRODUCTION_IPAY88REDIRECTED_URL_BCA_KLIKPAY : self::SANDBOX_IPAY88REDIRECTED_URL_BCA_KLIKPAY);
  }

  public static function doCheckStatus($orderCode, $amount) {
    $query = (self::$isProduction ? self::PRODUCTION_IPAY88HOSTED_URL : self::SANDBOX_IPAY88HOSTED_URL) . self::IPAY88_HOSTED_CHECK_STATUS_URL . "?MerchantCode=" . $merchantCode . "&RefNo=" . $orderCode . "&Amount=" . $amount;
    $url = parse_url($query);
    $host = $url["host"];
    $path = $url["path"] . "?" . $url["query"];
    $timeout = 1;
    $fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
    $result = false;
    if ($fp) {
      fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
      while (!feof($fp)) {
        $buf .= fgets($fp, 128);
      }
      $lines = split("\n", $buf);
      $result = $lines[count($lines)-1];
      fclose($fp);
    } else {
      // handling error
    }
    return $result;
  }

  public static function hex2bin($hexSource) {
    $bin = null;
    for ($i=0;$i<strlen($hexSource);$i=$i+2) {
      $bin .= chr(hexdec(substr($hexSource,$i,2)));
    }
    return $bin;
  }

  /**
  * For BCA KlikPay
  */

  public static function toHexString($string){
    $hex = '';
    for ($i=0; $i<strlen($string); $i++){
      $ord = ord($string[$i]);
      $hexCode = dechex($ord);
      $hex .= substr('0'.$hexCode, -2);
    }
    return strToUpper($hex);
  }

  public static function fromHexString($hex){
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2){
      $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
  }

  public static function getHash($value){
    $hash = 0;
    for ($i = 0; $i < strlen($value); $i++){
      $hash = (intval($hash) * 31) + ord($value[$i]); $hash = self::intval32bits($hash);
    }
    return (int)$hash;
  }

  public static function intval32bits($value){
    $value = ($value & 0xFFFFFFFF);
    if ($value & 0x80000000) $value = -((~$value & 0xFFFFFFFF) + 1);
    return $value;
  }

  public static function generateKeyId($string){
    return self::toHexString($string);
  }

  ####### END #######

  public static function generateRequestPageSignature($orderCode, $amount, $currency = 'IDR') {
    return base64_encode(self::hex2bin(sha1( self::$sharedKey . self::$merchantCode . $orderCode . $amount . $currency)));
  }

  public static function generateResponsePageSignature($orderCode, $amount, $paymentChannelId, $status, $currency = 'IDR') {
    return base64_encode(self::hex2bin(sha1( self::$sharedKey . self::$merchantCode . str_replace("IP", "", $paymentChannelId) . $orderCode . $amount . $currency . $status )));
  }

  /**
  * For BCA KlikPay
  */

  public static function generateSignature($transactionDate, $transactionNo, $amount, $currency = 'IDR') {
    date_default_timezone_set ("Asia/Jakarta");
    $keyId = self::generateKeyId(self::$clearKey);
    $tempKey1 = self::$klikPayCode . $transactionNo . $currency . $keyId;
    $hashKey1 = self::getHash($tempKey1);
    $tdate = date_create_from_format("d/m/Y H:i:s",$transactionDate);
    $idate = (int)date_format($tdate, 'dmY');
    $iamount = (int)$amount;
    $tempKey2 = ($idate + $iamount);
    $tempKey2 = $tempKey2 . "";
    $hashKey2 = self::getHash($tempKey2);
    $hash = abs($hashKey1+$hashKey2);
    return $hash."";
  }

  public static function generateAuthKey($transactionDate, $transactionNo, $currency = 'IDR'){
    $keyId = self::generateKeyId(self::$clearKey);
    $str1 = str_pad(self::$klikPayCode,10, '0', STR_PAD_RIGHT) . str_pad($transactionNo,18, 'A', STR_PAD_RIGHT) . str_pad($currency,5, '1',STR_PAD_RIGHT) . str_pad($transactionDate,19, 'C', STR_PAD_LEFT) . $keyId;
    $mdstr1 = md5($str1);
    $my_key = self::fromHexString($keyId);
    $data = self::fromHexString($mdstr1);
    $my_key .= substr($my_key,0,8);
    $secret = mcrypt_encrypt(MCRYPT_3DES, $my_key, $data, MCRYPT_MODE_ECB);
    return self::toHexString($secret);
  }

  /**
  * Return list of payment type that supported by Doku
  * @return string[] List of payment type
  */
  public static function getPaymentTypeOptions() {
    return [
        self::CREDITCARD => "IPay88 Credit Card", // IDR
        self::BANKTRANSFER_BII => "IPay88 BII Bank Transfer", // IDR
        self::BANKTRANSFER_MANDIRI => "IPay88 Mandiri Bank Transfer", // IDR
        self::BCA_KLIKPAY => "IPay88 BCA KlikPay", // IDR
        self::MANDIRI_CLICKPAY => "IPay88 Mandiri ClickPay", // IDR
        self::CIMB_CLICKS => "IPay88 CIMB Clicks", // IDR
        self::MUAMALAT_IB => "IPay88 Muamalat Internet Banking", // IDR
        self::XL_TUNAI => "IPay88 XL Tunai", // IDR
        self::KARTUKU => "IPay88 Kartuku", // IDR
        self::MANDIRI_ECASH => "IPay88 Mandiri E-Cash", // IDR
        self::TELKOMSEL_CASH => "IPay88 Telkomsel Cash", // IDR
        self::INDOSAT_DOMPETKU => "IPay88 Indosat Dompetku", // IDR
        self::VA_BCA => "IPay88 BCA Virtual Account", // IDR
        self::PAY4ME => "IPay88 Pay4ME", // IDR
        self::PAYPAL => "IPay88 Paypal" // USD
    ];
  }

  /**
   * Return detail of payment fee related to payment type parameter
   * @param string $paymentType Code of payment type
   * @return mixed[] Detail of payment fee
   **/
  public static function getPaymentFee($paymentType) {
    return [
      'minimum_amount' => 0, // new rule and only applicated to mandiriEcash
      'base_amount' => 0,
      'percentage_amount' => 0
    ];
  }
}