<?php

namespace Suitpay\ThirdpartyAPI\OtherPayment;

use Suitpay\Exceptions\OtherPaymentException;

Class OtherPayment
{
  // PAYMENT TYPE
  const MANUALTRANSFER = "OP1"; // IDR
  const OFFLINE = "OP2"; // COD, etc

  // CURRENCY
  const CURRENCY_IDR = "IDR";
  const CURRENCY_USD = "USD";

  // TRANSACTION STATUS
  const OTHERPAYMENT_REINIT = '2';
  const OTHERPAYMENT_SUCCESS = '1';
  const OTHERPAYMENT_DENY = '0';

  // ENDPOINT URL
  public static $sharedKey = ''; // merchant unique key
  public static $merchantCode = ''; // merchant code

  public static function hex2bin($hexSource) {
    $bin = null;
    for ($i=0;$i<strlen($hexSource);$i=$i+2) {
      $bin .= chr(hexdec(substr($hexSource,$i,2)));
    }
    return $bin;
  } 

  public static function generateResponsePageSignature($orderCode, $amount, $status, $currency = 'IDR') {
    return base64_encode(self::hex2bin(sha1( self::$sharedKey . self::$merchantCode . $orderCode . $amount . $currency . $status )));
  }

  /**
  * Return list of payment type that supported by Doku
  * @return string[] List of payment type
  */
  public static function getPaymentTypeOptions() {
    return [
        self::MANUALTRANSFER => "Manual Bank Transfer", // IDR
        self::OFFLINE => "Offline Payment", // IDR
    ];
  }

  /**
   * Return detail of payment fee related to payment type parameter
   * @param string $paymentType Code of payment type
   * @return mixed[] Detail of payment fee
   **/
  public static function getPaymentFee($paymentType) {
    return [
      'minimum_amount' => 0, // new rule and only applicated to mandiriEcash
      'base_amount' => 0,
      'percentage_amount' => 0
    ];
  }
}
