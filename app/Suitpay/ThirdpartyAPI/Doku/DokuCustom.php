<?php

namespace Suitpay\ThirdpartyAPI\Doku;

use Suitpay\Exceptions\DokuException;

Class DokuCustom
{
  // PAYMENT TYPE
  // -- Credit Card
  const CREDITCARD = "15";
  // -- Bank Transfer
  const BANKTRANSFER = "07"; // BCA, Permata, and more to come
  // -- Internet Banking (Direct Debit)
  const BCA_CLICKPAY = "18";
  const MANDIRI_CLICKPAY = "02";
  const BRI_EPAY = "06";
  const CIMB_CLICKS = "19";
  // -- Convenient Store (Indomaret, Kioson, and more to come)
  const CSTORE = "31";

  // TRANSACTION STATUS : [capture | pending | deny] => [settlement | deny | authorize | expire]
  const DOKU_SUCCESS = '0000';
  const DOKU_PENDING = 'pending'; // For Payment Type outside of Veritrans::CREDITCARD means transaction created
  const DOKU_SETTLEMENT = 'settlement';
  const DOKU_DENY = '5514';
  const DOKU_AUTHORIZE = 'authorize';
  const DOKU_EXPIRE = '5509';

  // FRAUD STATUS (from FDS / Fraud Detection System)
  const DOKU_FDS_ACCEPT = 'accept';
  const DOKU_FDS_CHALLENGE = 'challenge';
  const DOKU_FDS_DENY = 'deny';

  /**
    * Your merchant's server shared key
    * @static
    */
  public static $sharedKey;

  /**
    * Your merchant's mall id
    * @static
    */
  public static $mallId;
  
  /**
    * Your enabled payment type
    * @static
    */
  public static $enabledPayment;

  /**
    * true for production
    * false for sandbox mode
    * @static
    */
  public static $isProduction = false;

    /**
    * Default options for every request
    * @static
    */
    public static $curlOptions = []; 

    const SANDBOX_BASE_URL = 'http://staging.doku.com/api/payment';
    const PRODUCTION_BASE_URL = 'https://pay.doku.com/api/payment';

    public function config($params)
    {
        DokuCustom::$isProduction = $params['production'];
        DokuCustom::$sharedKey = $params['server_key'];
        DokuCustom::$mallId = $params['mall_id'];
    }

    /**
    * @return string Doku API URL, depends on $isProduction
    */
    public static function getBaseUrl()
    {
      return DokuCustom::$isProduction ?
          DokuCustom::PRODUCTION_BASE_URL : DokuCustom::SANDBOX_BASE_URL;
    }

  /**
   * Send GET request
   * @param string  $url
   * @param string  $server_key
   * @param mixed[] $data_hash
   */
  public static function get($url, $server_key, $data_hash)
  {
    return self::remoteCall($url, $server_key, $data_hash, false);
  }

  /**
   * Send POST request
   * @param string  $url
   * @param string  $server_key
   * @param mixed[] $data_hash
   */
  public static function post($url, $server_key, $data_hash)
  {
      return self::remoteCall($url, $server_key, $data_hash, true);
  }

    /**
   * Actually send request to API server
   * @param string  $url
   * @param string  $server_key
   * @param mixed[] $data_hash
   * @param bool    $post
   */
    public static function remoteCall($url, $server_key, $data_hash, $post = true)
    { 
      $ch = curl_init();
      $curl_options = array(
        CURLOPT_URL => $url,
        CURLOPT_HEADER => 0,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json',
          'Accept: application/json',
          // 'Authorization: Basic ' . base64_encode($server_key . ':')
        ),
        CURLOPT_RETURNTRANSFER => 1,
        // CURLOPT_CAINFO => dirname(__FILE__) . "/data/cacert.pem"
      );

      // merging with Doku_Config::$curlOptions
      if (count(DokuCustom::$curlOptions)) {
        // We need to combine headers manually, because it's array and it will no be merged
        if (DokuCustom::$curlOptions[CURLOPT_HTTPHEADER]) {
          $mergedHeders = array_merge($curl_options[CURLOPT_HTTPHEADER], DokuCustom::$curlOptions[CURLOPT_HTTPHEADER]);
          $headerOptions = array( CURLOPT_HTTPHEADER => $mergedHeders );
        } else {
          $mergedHeders = array();
        }

        $curl_options = array_replace_recursive($curl_options, DokuCustom::$curlOptions, $headerOptions);
      }

      if ($post) {
        $curl_options[CURLOPT_POST] = 1;

        if ($data_hash) {
          $body = 'data='. json_encode($data_hash);
          // $body = json_encode($data_hash);
          $curl_options[CURLOPT_POSTFIELDS] = $body;
        } else {
          $curl_options[CURLOPT_POSTFIELDS] = '';
        }
      }

      curl_setopt_array($ch, $curl_options);

      $result = curl_exec($ch);
      // curl_close($ch);
     
      if ($result === FALSE) {
        throw new DokuException('CURL Error: ' . curl_error($ch), curl_errno($ch));
      }
      else {
        if(is_string($result)){
          $result_array = json_decode($result);
          //if (!in_array($result_array->status_code, array(200, 201, 202, 407))) {
          //  $message = 'Doku Error (' . $result_array->status_code . '): '
          //      . $result_array->status_message;
          //  throw new DokuException($message, $result_array->status_code);
          //}
          //else {
            return $result_array;
          //}
        }else{
          return $result;
        }
      }
    }

    public function formatBasket($data){
      $parseBasket = '';
      if(is_array($data)){
        foreach($data as $basket){
          $parseBasket = $parseBasket . $basket['name'] .','. $basket['amount'] .','. $basket['quantity'] .','. $basket['subtotal'] .';';
        }
      }else if(is_object($data)){
        foreach($data as $basket){
          $parseBasket = $parseBasket . $basket->name .','. $basket->amount .','. $basket->quantity .','. $basket->subtotal .';';
        }
      }else{
        $parseBasket = $data;
      }
      return $parseBasket;
    }

    public function doCreateWords($data){
      if(!empty($data['device_id'])){ 
        if(!empty($data['pairing_code'])){
          return sha1($data['amount'] . DokuCustom::$mallId . DokuCustom::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);
        }else{
          return sha1($data['amount'] . DokuCustom::$mallId . DokuCustom::$sharedKey . $data['invoice'] . $data['currency'] . $data['device_id']);
        }
      }else if(!empty($data['pairing_code'])){
        return sha1($data['amount'] . DokuCustom::$mallId . DokuCustom::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);
      }else if(!empty($data['currency'])){
        return sha1($data['amount'] . DokuCustom::$mallId . DokuCustom::$sharedKey . $data['invoice'] . $data['currency']);
      }else{
        return sha1($data['amount'] . DokuCustom::$mallId . DokuCustom::$sharedKey . $data['invoice']);
      }
    }
    
    public function doCreateWordsRaw($data){
      if(!empty($data['device_id'])){
        if(!empty($data['pairing_code'])){
          return $data['amount'].DokuCustom::$mallId.DokuCustom::$sharedKey.$data['invoice'].$data['currency'].$data['token'].$data['pairing_code'].$data['device_id'];
        }else{
          return $data['amount'].DokuCustom::$mallId.DokuCustom::$sharedKey.$data['invoice'].$data['currency'].$data['device_id'];
        }
      }else if(!empty($data['pairing_code'])){
        return $data['amount'].DokuCustom::$mallId.DokuCustom::$sharedKey.$data['invoice'].$data['currency'].$data['token'].$data['pairing_code'];
      }else if(!empty($data['currency'])){
        return $data['amount'].DokuCustom::$mallId.DokuCustom::$sharedKey.$data['invoice'].$data['currency'];
      }else{
        return $data['amount'].DokuCustom::$mallId.DokuCustom::$sharedKey.$data['invoice'];
      }
    }

    public function doPrePayment($data) {
      $data['req_basket'] = $this->formatBasket($data['req_basket']);
      $result = DokuCustom::post(
        DokuCustom::getBaseUrl() . '/PrePayment',
        DokuCustom::$sharedKey,
        $data
      );

      return $result;
    }

    public function doPayment($data) {
      $data['req_basket'] = $this->formatBasket($data['req_basket']);
      $result = DokuCustom::post(
        DokuCustom::getBaseUrl() . '/paymentMip',
        DokuCustom::$sharedKey,
        $data
      );

      return $result;
    }

    public function doDirectPayment($data) {
      $result = DokuCustom::post(
        DokuCustom::getBaseUrl() . '/PaymentMIPDirect',
        DokuCustom::$sharedKey,
        $data
      );

      return $result;
    }

    public function doGeneratePaycode($data) {
      $result = DokuCustom::post(
        DokuCustom::getBaseUrl() . '/doGeneratePaymentCode',
        DokuCustom::$sharedKey,
        $data
      );

      return $result;
    }

    public function doRedirectPayment($data) {
      $result = DokuCustom::post(
        DokuCustom::getBaseUrl() . '/doInitiatePayment',
        DokuCustom::$sharedKey,
        $data
      );

      return $result;
    }

    public function doCapture($data) {
      $result = DokuCustom::post(
        DokuCustom::getBaseUrl() . '/DoCapture',
        DokuCustom::$sharedKey,
        $data
      );

      return $result;
    }

    public function doCheckPaymentStatus($trans_id) {
      $data                    = [
        'MALLID'          => DokuCustom::$mallId,
        'CHAINMERCHANT'   => 'NA',
        'TRANSIDMERCHANT' => $trans_id,
        'SESSIONID'       => date('YmdHis'),  
        'WORDS'           => sha1( DokuCustom::$mallId . DokuCustom::$sharedKey . $data['TRANSIDMERCHANT'] )   
      ];

      $responseXML = DokuCustom::post(
        str_replace('/api/payment', '', DokuCustom::getBaseUrl()) . '/Suite/CheckStatus',
        DokuCustom::$sharedKey,
        $data
      );

      return $responseXML;
    }

    /**
    * Return list of payment type that supported by Doku
    * @return string[] List of payment type
    */
    public static function getPaymentTypeOptions() {
      return [
          self::CREDITCARD => "Credit Card",
          self::BANKTRANSFER => "Bank Transfer",
          self::BCA_CLICKPAY => "BCA ClickPay",
          self::MANDIRI_CLICKPAY => "Mandiri ClickPay",
          self::BRI_EPAY => "BRI E-Pay",
          self::CIMB_CLICKS => "CIMB Clicks",
          self::CSTORE => "Convenient Store (Indomaret)"
      ];
    }

    /**
     * Return detail of payment fee related to payment type parameter
     * @param string $paymentType Code of payment type
     * @return mixed[] Detail of payment fee
     **/
    public static function getPaymentFee($paymentType) {
      return [
        'minimum_amount' => ($paymentType == self::MANDIRI_ECASH ? 2500 : 0), // new rule and only applicated to mandiriEcash
        'base_amount' => (
            $paymentType == self::CREDITCARD || $paymentType == self::BCA_CLICKPAY || $paymentType == self::CSTORE ? 2500 : (
                $paymentType == self::BANKTRANSFER ? 4900 : (
                    $paymentType == self::MANDIRI_CLICKPAY || $paymentType == self::BRI_EPAY || $paymentType == self::CIMB_CLICKS  ? 5000 : 0
                )
            )
          ),
        'percentage_amount' => ($paymentType == self::CREDITCARD ? 3.2 :0)
      ];
    }
}
