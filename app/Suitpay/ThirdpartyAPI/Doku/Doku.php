<?php

namespace Suitpay\ThirdpartyAPI\Doku;

use Suitpay\Exceptions\DokuException;

Class Doku
{
  // PAYMENT TYPE
  // -- Credit Card
  const CREDITCARD = "D15"; // Merchant Hosted
  const CREDITCARDINSTALLMENT = "D15B"; // DOKU Hosted
  // -- Doku Wallet
  const DOKUWALLET = "D04"; // Merchant Hosted
  // -- Bank Transfer
  const BANKTRANSFER = "D05"; // Merchant Hosted : Bank Permate VA Lite
  const BANKTRANSFER_PRODUCTION = "D36"; // Merchant Hosted : Bank Permate VA Lite
  // -- Internet Banking (Direct Debit)
  const BCA_CLICKPAY = "D18"; // DOKU Hosted
  const MANDIRI_CLICKPAY = "D02"; // Merchant Hosted
  const BRI_EPAY = "D06"; // DOKU Hosted
  const CIMB_CLICKS = "D19"; // DOKU Hosted
  const PERMATA_NET = "D28"; // DOKU Hosted
  const DANAMON = "D26"; // DOKU Hosted
  const MUAMALAT = "D25"; // DOKU Hosted
  // -- Convenient Store (Alfagroup)
  const CSTORE = "D14"; // Merchant Hosted
  const CSTORE_PRODUCTION = "D35"; // Merchant Hosted

  const DOKU_PERMATA_VA_PREFIX_SANDBOX = "89650"; // DOKU Company Code for PermataVA 
  const DOKU_PERMATA_VA_PREFIX_PRODUCTION = "88560018"; // DOKU Company Code for PermataVA

  // TRANSACTION STATUS : [capture | pending | deny] => [settlement | deny | authorize | expire]
  const DOKU_SUCCESS = '0000';
  const DOKU_PENDING = '9999'; // Local Code
  const DOKU_SETTLEMENT = '0000'; // Local Code, same with success
  const DOKU_DENY = '5514';
  const DOKU_AUTHORIZE = '9990'; // Local Code
  const DOKU_EXPIRE = '5509';

  // FRAUD STATUS (from FDS / Fraud Detection System)
  const DOKU_FDS_ACCEPT = 'accept';
  const DOKU_FDS_CHALLENGE = 'challenge';
  const DOKU_FDS_DENY = 'deny';

  // API ENDPOINT URL
  const SANDBOX_DOKUHOSTED_URL = 'https://staging.doku.com';
  const PRODUCTION_DOKUHOSTED_URL = 'https://pay.doku.com';
  const SANDBOX_BASE_URL = 'https://staging.doku.com/api/payment';
  const PRODUCTION_BASE_URL = 'https://pay.doku.com/api/payment';

  const prePaymentUrl = '/PrePayment';
  const paymentUrl = '/paymentMip';
  const directPaymentUrl = '/PaymentMIPDirect';
  const generateCodeUrl = '/doGeneratePaymentCode';
  const generateCodeVAUrl = '/DoGeneratePaycodeVA';

  const DOKU_HOSTED_CHECK_STATUS_URL = '/Suite/CheckStatus';
  const DOKU_HOSTED_INIT_URL = '/Suite/Receive';
  const DOKU_HOSTED_INIT_BCAKLIK_URL = '/Suite/ReceiveMIP';

  public static $isProduction = false; // production state
  public static $sharedKey = ''; // doku's merchant unique key
  public static $mallId = ''; // doku's merchant id
  public static $ccInstallmentMallId = ''; // doku's merchant id for cc installment

  public static function getDokuHostedUrl($isBcaKlik = false) {
    if ($isBcaKlik) {
      return (self::$isProduction ? self::PRODUCTION_DOKUHOSTED_URL : self::SANDBOX_DOKUHOSTED_URL) . self::DOKU_HOSTED_INIT_BCAKLIK_URL;
    }
    return (self::$isProduction ? self::PRODUCTION_DOKUHOSTED_URL : self::SANDBOX_DOKUHOSTED_URL) . self::DOKU_HOSTED_INIT_URL;
  }

  public static function getPermataVAPrefix() {
    return (self::$isProduction ? self::DOKU_PERMATA_VA_PREFIX_PRODUCTION : self::DOKU_PERMATA_VA_PREFIX_SANDBOX);
  }

  public static function initDokuHostedInitialPage($data, $isBcaKlik = false) {
    $data['BASKET'] = self::formatBasket($data['BASKET']);
    // dd(self::getDokuHostedUrl($isBcaKlik), $data);
    $ch = curl_init( self::getDokuHostedUrl($isBcaKlik) );
    return self::doRawConnection($ch, $data);
  }

  public static function doCreateWords($data){

    $extendedSignature = '';
    if (!empty($data['result_message']) && !empty($data['verify_status'])) {
      $extendedSignature = $data['result_message'] . $data['verify_status'];
    }

    if (!empty($data['device_id'])){
      if (!empty($data['pairing_code'])) {
        return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);
      } else {
        return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['device_id']);
      }
    } elseif (!empty($data['deviceid'])){
      // compatibility between web/sdk version
      if (!empty($data['pairing_code'])) {
        return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['deviceid']);
      } else {
        return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['deviceid']);
      }
    } else if(!empty($data['pairing_code'])){

      return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);

    }else if(!empty($data['currency'])){

      return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $extendedSignature . $data['currency']);

    }else{

      return sha1($data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $extendedSignature);

    }
  }

  public static function doCreateWordsCustom($data){
    if (is_array($data)) {
      return sha1( implode('', array_values($data)) );
    }
    return '';
  }

  public static function doCreateWordsRaw($data){

    if(!empty($data['device_id'])){

      if(!empty($data['pairing_code'])){

        return $data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id'];

      }else{

        return $data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['device_id'];

      }

    }else if(!empty($data['pairing_code'])){

      return $data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'];

    }else if(!empty($data['currency'])){

      return $data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'] . $data['currency'];

    }else{

      return $data['amount'] . self::$mallId . self::$sharedKey . $data['invoice'];

    }
  }

  public static function formatBasket($data){
    $parseBasket = null;
    if(is_array($data)){
      foreach($data as $basket){
        $parseBasket = $parseBasket . $basket['name'] .','. $basket['amount'] .','. $basket['quantity'] .','. $basket['subtotal'] .';';
      }
    }else if(is_object($data)){
      foreach($data as $basket){
        $parseBasket = $parseBasket . $basket->name .','. $basket->amount .','. $basket->quantity .','. $basket->subtotal .';';
      }
    }

    return $parseBasket;
  }

  protected static function doRawConnection(&$ch, $data) {
    curl_setopt( $ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0); 

    $response = curl_exec( $ch );

    curl_close($ch);

    return $response;
  }

  protected static function doConnection(&$ch, $data) {
    curl_setopt( $ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, 'data='. json_encode($data));
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0); 

    $responseJson = curl_exec( $ch );

    curl_close($ch);

    // dd((self::$isProduction ? self::PRODUCTION_BASE_URL : self::SANDBOX_BASE_URL) . self::generateCodeVAUrl, 'data='. json_encode($data), $responseJson );

    if(is_string($responseJson)){
      return json_decode($responseJson);
    } else {
      return $responseJson;
    }
  }

  public static function doPrePayment($data){
    $data['req_basket'] = self::formatBasket($data['req_basket']);

    $ch = curl_init( (self::$isProduction ? self::PRODUCTION_BASE_URL : self::SANDBOX_BASE_URL) . self::prePaymentUrl );
    return self::doConnection($ch, $data);
  }

  public static function doPayment($data){
    $data['req_basket'] = self::formatBasket($data['req_basket']);
    
    $ch = curl_init( (self::$isProduction ? self::PRODUCTION_BASE_URL : self::SANDBOX_BASE_URL) . self::paymentUrl );
    return self::doConnection($ch, $data);
  }

  public static function doDirectPayment($data){
    $data['req_basket'] = self::formatBasket($data['req_basket']);
    
    $ch = curl_init( (self::$isProduction ? self::PRODUCTION_BASE_URL : self::SANDBOX_BASE_URL) . self::directPaymentUrl );
    return self::doConnection($ch, $data);
  }

  public static function doGeneratePaycodeVA($data){
    $data['req_basket'] = self::formatBasket($data['req_basket']);
    
    $ch = curl_init( (self::$isProduction ? self::PRODUCTION_BASE_URL : self::SANDBOX_BASE_URL) . (self::$isProduction ? self::generateCodeVAUrl : self::generateCodeUrl) );
    return self::doConnection($ch, $data);
  }

  public static function doGeneratePaycode($data){
    $data['req_basket'] = self::formatBasket($data['req_basket']);
    
    $ch = curl_init( (self::$isProduction ? self::PRODUCTION_BASE_URL : self::SANDBOX_BASE_URL) . self::generateCodeUrl );
    return self::doConnection($ch, $data);
  }

  public static function doCheckStatus($data) {
    $ch = curl_init( (self::$isProduction ? self::PRODUCTION_DOKUHOSTED_URL : self::SANDBOX_DOKUHOSTED_URL) . self::DOKU_HOSTED_CHECK_STATUS_URL );
    return self::doRawConnection($ch, $data);
  }

  /**
  * Return list of payment type that supported by Doku
  * @return string[] List of payment type
  */
  public static function getPaymentTypeOptions() {
    return [
        self::CREDITCARD => "Doku Credit Card",
        self::CREDITCARDINSTALLMENT => "Doku Credit Card with Installment",
        self::BANKTRANSFER => "Doku Bank Transfer",
        self::DOKUWALLET => "Doku Wallet",
        self::BCA_CLICKPAY => "Doku BCA ClickPay",
        self::MANDIRI_CLICKPAY => "Doku Mandiri ClickPay",
        self::BRI_EPAY => "Doku BRI E-Pay",
        self::CIMB_CLICKS => "Doku CIMB Clicks",
        self::PERMATA_NET => "Doku Permata Net",
        self::DANAMON => "Doku Danamon",
        self::MUAMALAT => "Doku Muamalat",
        self::CSTORE => "Doku ALFA"
    ];
  }

  /**
   * Return detail of payment fee related to payment type parameter
   * @param string $paymentType Code of payment type
   * @return mixed[] Detail of payment fee
   **/
  public static function getPaymentFee($paymentType) {
    return [
      'minimum_amount' => ($paymentType == self::MANDIRI_ECASH ? 2500 : 0), // new rule and only applicated to mandiriEcash
      'base_amount' => (
          $paymentType == self::CREDITCARD || $paymentType == self::CREDITCARDINSTALLMENT || $paymentType == self::BCA_CLICKPAY || $paymentType == self::CSTORE ? 2500 : (
              $paymentType == self::BANKTRANSFER ? 5000 : (
                  $paymentType == self::MANDIRI_CLICKPAY || $paymentType == self::BRI_EPAY || $paymentType == self::CIMB_CLICKS  ? 5000 : 0
              )
          )
        ),
      'percentage_amount' => ($paymentType == self::CREDITCARD || $paymentType == self::CREDITCARDINSTALLMENT ? 3.0 :0)
    ];
  }
}
