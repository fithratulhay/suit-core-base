<?php

namespace Suitpay\ThirdpartyAPI\Veritrans;

use Suitpay\Exceptions\VeritransException;

Class Veritrans
{
  // PAYMENT TYPE
  // -- Credit Card
  const CREDITCARD = "credit_card";
  // -- Bank Transfer
  const BANKTRANSFER = "bank_transfer"; // BCA, Permata, and more to come
  const ECHANNEL = "echannel"; // For Mandiri BillPayment
  // -- Internet Banking (Direct Debit)
  const BCA_CLICKPAY = "bca_klikpay";
  const BCA_KLIKBCA = "bca_klikbca";
  const MANDIRI_CLICKPAY = "mandiri_clickpay";
  const BRI_EPAY = "bri_epay";
  const CIMB_CLICKS = "cimb_clicks";
  // -- E-Wallet
  const TELKOMSEL_CASH = "telkomsel_cash";
  const XL_TUNAI = "xl_tunai";
  const INDOSAT_DOMPETKU = "indosat_dompetku";
  const MANDIRI_ECASH = "mandiri_ecash";
  // -- Convenient Store (Indomaret, Kioson, and more to come)
  const CSTORE = "cstore";

  // TRANSACTION STATUS : [capture | pending | deny] => [settlement | deny | authorize | expire]
  const VERITRANS_SUCCESS = 'capture';
  const VERITRANS_PENDING = 'pending'; // For Payment Type outside of Veritrans::CREDITCARD means transaction created
  const VERITRANS_SETTLEMENT = 'settlement';
  const VERITRANS_DENY = 'deny';
  const VERITRANS_AUTHORIZE = 'authorize';
  const VERITRANS_EXPIRE = 'expire';

  // FRAUD STATUS (from FDS / Fraud Detection System)
  const VERITRANS_FDS_ACCEPT = 'accept';
  const VERITRANS_FDS_CHALLENGE = 'challenge';
  const VERITRANS_FDS_DENY = 'deny';

  /**
    * Your merchant's server key
    * @static
    */
  public static $serverKey;
  
  /**
    * Your enabled payment type
    * @static
    */
  public static $enabledPayment;

  /**
    * true for production
    * false for sandbox mode
    * @static
    */
  public static $isProduction = false;

  /**
    * true for snap
    * false for default mode
    * @static
    */
  public static $isSnap = false;

    /**
    * Default options for every request
    * @static
    */
    public static $curlOptions = []; 

    const SANDBOX_BASE_URL = 'https://api.sandbox.midtrans.com/v2';
    const PRODUCTION_BASE_URL = 'https://api.midtrans.com/v2';

    const SNAP_SANDBOX_BASE_URL = 'https://app.sandbox.midtrans.com/snap/v1';
    const SNAP_PRODUCTION_BASE_URL = 'https://app.midtrans.com/snap/v1';

    public function config($params)
    {
        Veritrans::$serverKey = $params['server_key'];
        Veritrans::$isProduction = $params['production'];
    }

    /**
    * @return string Veritrans API URL, depends on $isProduction
    */
    public static function getBaseUrl()
    {
      if (Veritrans::$isSnap) {
        return Veritrans::$isProduction ?
          Veritrans::SNAP_PRODUCTION_BASE_URL : Veritrans::SNAP_SANDBOX_BASE_URL;
      }
      return Veritrans::$isProduction ?
          Veritrans::PRODUCTION_BASE_URL : Veritrans::SANDBOX_BASE_URL;
    }

  /**
   * Send GET request
   * @param string  $url
   * @param string  $server_key
   * @param mixed[] $data_hash
   */
  public static function get($url, $server_key, $data_hash)
  {
    return self::remoteCall($url, $server_key, $data_hash, false);
  }

  /**
   * Send POST request
   * @param string  $url
   * @param string  $server_key
   * @param mixed[] $data_hash
   */
  public static function post($url, $server_key, $data_hash)
  {
      return self::remoteCall($url, $server_key, $data_hash, true);
  }

    /**
   * Actually send request to API server
   * @param string  $url
   * @param string  $server_key
   * @param mixed[] $data_hash
   * @param bool    $post
   */
    public static function remoteCall($url, $server_key, $data_hash, $post = true)
    { 
    
      $ch = curl_init();

      $curl_options = array(
        CURLOPT_URL => $url,
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json',
          'Accept: application/json',
          'Authorization: Basic ' . base64_encode($server_key . ':')
        ),
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_CAINFO => dirname(__FILE__) . "/data/cacert.pem"
      );

      // merging with Veritrans_Config::$curlOptions
      if (count(Veritrans::$curlOptions)) {
        // We need to combine headers manually, because it's array and it will no be merged
        if (Veritrans::$curlOptions[CURLOPT_HTTPHEADER]) {
          $mergedHeders = array_merge($curl_options[CURLOPT_HTTPHEADER], Veritrans::$curlOptions[CURLOPT_HTTPHEADER]);
          $headerOptions = array( CURLOPT_HTTPHEADER => $mergedHeders );
        } else {
          $mergedHeders = array();
        }

        $curl_options = array_replace_recursive($curl_options, Veritrans::$curlOptions, $headerOptions);
      }

      if ($post) {
        $curl_options[CURLOPT_POST] = 1;

        if ($data_hash) {
          $body = json_encode($data_hash);
          $curl_options[CURLOPT_POSTFIELDS] = $body;
        } else {
          $curl_options[CURLOPT_POSTFIELDS] = '';
        }
      }

      curl_setopt_array($ch, $curl_options);

      $result = curl_exec($ch);
      $info = null;
      if (Veritrans::$isSnap) {
        $info = curl_getinfo($ch);
      }
      // curl_close($ch);
     
      if ($result === FALSE) {
        throw new VeritransException('CURL Error: ' . curl_error($ch), curl_errno($ch));
      }
      else {
        $result_array = json_decode($result);
        if (Veritrans::$isSnap && $info && $info['http_code'] != 201) {
          $message = 'Midtrans Error (' . $info['http_code'] . '): '
            . implode(',', $result_array->error_messages);
          throw new VeritransException($message, $info['http_code']);
        }
        else if (!Veritrans::$isSnap && !in_array($result_array->status_code, array(200, 201, 202, 407))) {
          $message = 'Midtrans Error (' . $result_array->status_code . '): '
              . $result_array->status_message;
          //throw new Exception($message, $result_array->status_code);
          throw new VeritransException($message, $result_array->status_code);
        }
        else {
          return $result_array;
        }
      }
    }

    /**
    * Return list of payment type that supported by Veritrans
    * @return string[] List of payment type
    */
    public static function getPaymentTypeOptions() {
      return [
          self::CREDITCARD => "Midtrans Credit Card",
          self::BANKTRANSFER => "Midtrans Bank Transfer",
          self::ECHANNEL => "Midtrans Mandiri Bill Payment",
          self::BCA_CLICKPAY => "Midtrans BCA ClickPay",
          self::BCA_KLIKBCA => "Midtrans KlikBCA",
          self::MANDIRI_CLICKPAY => "Midtrans Mandiri ClickPay",
          self::BRI_EPAY => "Midtrans BRI E-Pay",
          self::CIMB_CLICKS => "Midtrans CIMB Clicks",
          self::TELKOMSEL_CASH => "Midtrans Telkomsel Cash",
          self::XL_TUNAI => "Midtrans XL Tunai",
          self::INDOSAT_DOMPETKU => "Midtrans Indosat Dompetku",
          self::MANDIRI_ECASH => "Midtrans Mandiri E-Cash",
          self::CSTORE => "Midtrans Convenient Store Counter"
      ];
    }

    /**
     * return snap token
     **/
    public static function getSnapToken($payloads)
    {
      $result = Veritrans::post(
        Veritrans::getBaseUrl() . '/transactions',
        Veritrans::$serverKey,
        $payloads);

      return $result->token;
    }

    /**
    * Charge / Send Transaction for VT-Web
    * @param mixed[] $payloads Transaction data
    * @return string Transaction redirect URL
    */
    public function vtweb_charge($payloads)
    { 

      $result = Veritrans::post(
        Veritrans::getBaseUrl() . '/charge',
        Veritrans::$serverKey,
        $payloads);

        return $result->redirect_url;
    }

    /**
    * Charge / Send Transaction for VT-Direct
    * @param mixed[] $payloads Transaction data
    * @return mixed[] Transaction result
    */
    public function vtdirect_charge($payloads)
    { 

      $result = Veritrans::post(
        Veritrans::getBaseUrl() . '/charge',
        Veritrans::$serverKey,
        $payloads);

        return $result;
    }

    /**
    * Retrieve transaction tokenize credit card
    * @return mixed[]
    */
    public static function token()
    {
      return Veritrans::get(
          Veritrans::getBaseUrl() . '/token',
          Veritrans::$serverKey,
          false);
    }

    /**
    * Capture Transaction that need authorization (if transaction status == authorize)
    * @param string $id Order ID or transaction ID
    * @param integer $grossAmount Gross amount of transaction, same amount with pre-authorization
    * @return mixed[] Transaction result
    */
    public function capture($id, $grossAmount)
    { 

      $result = Veritrans::post(
        Veritrans::getBaseUrl() . '/capture',
        Veritrans::$serverKey,
        [
            'transaction_id'      => $id,
            'gross_amount'        => $grossAmount
        ]);

        return $result;
    }

    /**
    * Retrieve transaction status
    * @param string $id Order ID or transaction ID
    * @return mixed[]
    */
    public static function status($id)
    {
      return Veritrans::get(
          Veritrans::getBaseUrl() . '/' . $id . '/status',
          Veritrans::$serverKey,
          false);
    }

    /**
    * Appove challenge transaction (if fraud status == challenge)
    * @param string $id Order ID or transaction ID
    * @return string
    */
    public static function approve($id)
    {
      return Veritrans::post(
          Veritrans::getBaseUrl() . '/' . $id . '/approve',
          Veritrans::$serverKey,
          false)->status_code;
    }

    /**
    * Cancel transaction before it's setteled
    * @param string $id Order ID or transaction ID
    * @return string
    */
    public static function cancel($id)
    {
      return Veritrans::post(
          Veritrans::getBaseUrl() . '/' . $id . '/cancel',
          Veritrans::$serverKey,
          false)->status_code;
    }

   /**
    * Expire transaction before it's setteled
    * @param string $id Order ID or transaction ID
    * @return mixed[]
    */
    public static function expire($id)
    {
      return Veritrans::post(
          Veritrans::getBaseUrl() . '/' . $id . '/expire',
          Veritrans::$serverKey,
          false);
    }

    /**
     * Return detail of payment fee related to payment type parameter
     * @param string $paymentType Code of payment type
     * @return mixed[] Detail of payment fee
     **/
    public static function getPaymentFee($paymentType) {
      return [
        'minimum_amount' => ($paymentType == self::MANDIRI_ECASH ? 2500 : 0), // new rule and only applicated to mandiriEcash
        'base_amount' => (
            $paymentType == self::CREDITCARD || $paymentType == self::BCA_CLICKPAY || $paymentType == self::BCA_KLIKBCA  || $paymentType == self::CSTORE ? 2500 : (
                $paymentType == self::BANKTRANSFER || $paymentType == self::ECHANNEL ? 4900 : (
                    $paymentType == self::MANDIRI_CLICKPAY || $paymentType == self::BRI_EPAY || $paymentType == self::CIMB_CLICKS  ? 5000 : (
                        $paymentType == self::TELKOMSEL_CASH || $paymentType == self::XL_TUNAI || $paymentType == self::INDOSAT_DOMPETKU ? 3000 : 
                            0
                    )
                )
            )
          ),
        'percentage_amount' => ($paymentType == self::CREDITCARD ? 3.2 : ($paymentType == self::MANDIRI_ECASH ? 1.0 : 0))
      ];
    }
}
