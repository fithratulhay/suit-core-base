<?php

namespace Suitsite\Commands;

use DB;
use Mail;
use Suitsite\Repositories\Contract\NewsletterRepositoryContract;
use Illuminate\Console\Command;

class NewsletterBroadcaster extends Command
{
    protected $newsletter;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletter:broadcast';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Newsletter Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NewsletterRepositoryContract $newsletter)
    {
        parent::__construct();
        $this->newsletter = $newsletter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Broadcast newsletter ...');
        $nbNewsletterExecuted = $this->newsletter->execute();
        if ($nbNewsletterExecuted) {
            $this->info($nbNewsletterExecuted . ' newsletter sent!');
        } else {
            $this->info('No newsletter had sent!');
        }
    }
}
