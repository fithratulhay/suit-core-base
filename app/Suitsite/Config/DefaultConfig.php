<?php

namespace Suitsite\Config;

class DefaultConfig
{
    public static $data = [
        // 'menu_type' => [ ]
        // 'banner_type' => [ ]
    ];

    public static function getConfig()
    {
        $instancesConfig = [];
        // priority 1 (backward-compatibility with old-version)
        if (class_exists('\\App\\Config\\SuitsiteConfig')) {
            if (is_array( \App\Config\SuitsiteConfig::$data )) {
                $instancesConfig = \App\Config\SuitsiteConfig::$data;
            }
        }
        // priority 2
        $suitsiteConfig = config('suitsite');
        // priority 3 (default value) : self::$data
        return array_merge( self::$data, $suitsiteConfig, $instancesConfig );
    }
}
