<?php

namespace Suitsite\Repositories\Contract;

use Suitsite\Models\ContactMessage;
use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ContactMessageRepositoryContract extends SuitRepositoryContract
{
    public function sendReply($id, $param, ContactMessage &$contactus);
}
