<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface NewsletterSubscriberRepositoryContract extends SuitRepositoryContract
{
    /**
     * Get object detail by email
     * @param  string $name
     * @param  string $email
     * @return array Object Detail
     **/
    public function getByEmail($email);
}
