<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface MenuRepositoryContract extends SuitRepositoryContract
{
    public function getActiveMenu($type);
}
