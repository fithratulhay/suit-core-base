<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ContentCategoryRepositoryContract extends SuitRepositoryContract
{
    public function getOrInit($slug, $typeCode);

    public function getCategoryBy($slug, $typeCode);
}
