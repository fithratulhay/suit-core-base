<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ContentTypeRepositoryContract extends SuitRepositoryContract
{
    public function getOrInit($code);

    public function getTypeBy($code);
}
