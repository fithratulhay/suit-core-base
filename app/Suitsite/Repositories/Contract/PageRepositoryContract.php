<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface PageRepositoryContract extends SuitRepositoryContract
{
    public function getOrInit($slug);

    public function getPageBy($slug);

    public function getListOf($pagination = true, $perPage = 9, $params = null);
}
