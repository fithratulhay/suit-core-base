<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface ContentRepositoryContract extends SuitRepositoryContract
{
    public function getOrInit($slug, $typeCode = null, $categorySlug = null);

    public function getContentBy($slug, $typeCode);

    public function getListOf($typeCode, $categorySlug = null, $pagination = true, $perPage = 9, $params = null);
}
