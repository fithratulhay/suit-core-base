<?php

namespace Suitsite\Repositories\Contract;

use Suitsite\Models\Newsletter;
use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface NewsletterRepositoryContract extends SuitRepositoryContract
{
    public function getRecipients(Newsletter $newsletter);

    public function validateEmail($email);

    public function getGuestSubscriberEmails($maxNbRecepient);

    public function getUserEmails($maxNbRecepient);

    public function getAdminEmails($maxNbRecepient);

    public function manualSingleExecution($emailAddressList, $recepientType);

    public function execute();
}
