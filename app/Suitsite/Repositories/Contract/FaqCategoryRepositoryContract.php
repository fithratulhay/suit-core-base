<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface FaqCategoryRepositoryContract extends SuitRepositoryContract
{
    public function getOrInit($slug);

    public function getCategoryBy($slug);

    public function getFirst();

    public function getCachedList();
}
