<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface BannerRepositoryContract extends SuitRepositoryContract
{
    public function getCachedList($positionCode);

    public function getBanners($type = '');
}
