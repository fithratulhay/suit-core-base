<?php

namespace Suitsite\Repositories\Contract;

use Suitcore\Repositories\Contract\SuitRepositoryContract;

interface FaqRepositoryContract extends SuitRepositoryContract
{
    public function getCachedList($categorySlug);
}
