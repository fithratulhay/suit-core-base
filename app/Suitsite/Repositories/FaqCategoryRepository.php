<?php

namespace Suitsite\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\FaqCategoryRepositoryContract;
use Suitsite\Models\FaqCategory;

class FaqCategoryRepository implements FaqCategoryRepositoryContract
{
    use SuitRepositoryTrait;
    
    public function __construct(FaqCategory $model)
    {
        $this->mainModel = $model;
    }

    public function getOrInit($slug) {
    	$currentState = $this->getBy($slug);
    	if (!$currentState) {
    		$currentState = new FaqCategory;
    		$currentState->position_order = 0;
    		$currentState->name = ucfirst(strtolower($slug));
    		$currentState->slug = $slug;
    		$currentState->save();
    	}
    	return $currentState;
    }

    public function getCategoryBy($slug) {
        $currentState = $this->getCachedList();
        return (isset($currentState[$slug]) ? $currentState[$slug] : null);
    }

    public function getFirst() {
        $currentState = $this->getCachedList();
        return $currentState->first();
    }

    public function getCachedList() {
        $faqCategoryList = [];
        $baseModel = $this->mainModel;
        $faqCategoryList = Cache::rememberForever('faq_categories', function () use($baseModel) {
            return $baseModel->orderBy('position_order','asc')->orderBy('name','asc')->get()->keyBy('slug');
        });
        return $faqCategoryList;
    }
}
