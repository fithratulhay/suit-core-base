<?php

namespace Suitsite\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\ContentCategoryRepositoryContract;
use Suitsite\Models\ContentCategory;

class ContentCategoryRepository implements ContentCategoryRepositoryContract
{
    use SuitRepositoryTrait;

    protected $contentTypeRepo;

    public function __construct(ContentCategory $model, ContentTypeRepository $contentTypeRepo)
    {
        $this->mainModel = $model;
        $this->contentTypeRepo = $contentTypeRepo;
    }

    public function getOrInit($slug, $typeCode) {
        $relatedType = $this->contentTypeRepo->getOrInit($typeCode);
        $currentState = $this->mainModel->where('slug','=',$slug)
                             ->where('type_id','=',$relatedType->id)->first();
        if (!$currentState) {
            $currentState = new ContentCategory;
            $currentState->parent_id = null;
            $currentState->type_id = $relatedType->id;
            $currentState->name = ucfirst(strtolower($slug));
            $currentState->slug = $slug;
            $currentState->save();
        }
        return $currentState;
    }

    public function getCategoryBy($slug, $typeCode) {
        $relatedType = $this->contentTypeRepo->getTypeBy($typeCode);
        $currentState = null;
        if ($relatedType) {
            $currentState = $this->mainModel->where('slug','=',$slug)
                             ->where('type_id','=',$relatedType->id)->first();
        }
        return $currentState;
    }
}
