<?php

namespace Suitsite\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract;
use Suitsite\Models\NewsletterSubscriber;

class NewsletterSubscriberRepository implements NewsletterSubscriberRepositoryContract
{
    use SuitRepositoryTrait;
    
    public function __construct(NewsletterSubscriber $model)
    {
        $this->mainModel = $model;
    }

    /**
     * Get object detail by email
     * @param  string $name
     * @param  string $email
     * @return array Object Detail
     **/
    public function getByEmail($email)
    {
    	$object = ($this->mainModel ? $this->mainModel : NewsletterSubscriber::class);
    	$object = $object->where('email', $email)
    	           ->first();
        return $object;
    }
}
