<?php

namespace Suitsite\Repositories;

use Suitsite\Models\Content;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\ContentRepositoryContract;
use Suitsite\Repositories\Contract\ContentTypeRepositoryContract;
use Suitsite\Repositories\Contract\ContentCategoryRepositoryContract;

class ContentRepository implements ContentRepositoryContract
{
    use SuitRepositoryTrait;

    protected $contentTypeRepo;
    protected $contentCategoryRepo;

    public function __construct(Content $model, ContentTypeRepositoryContract $_contentTypeRepo, ContentCategoryRepositoryContract $_contentCategoryRepo)
    {
        $this->mainModel = $model;
        $this->contentTypeRepo = $_contentTypeRepo;
        $this->contentCategoryRepo = $_contentCategoryRepo;
    }

    public function getOrInit($slug, $typeCode = null, $categorySlug = null) {
        $relatedType = (!empty($typeCode) ? $this->contentTypeRepo->getOrInit($typeCode) : null);
        $relatedCategory = null;
        if ($relatedType) {
            $relatedCategory = (!empty($categorySlug) ? $this->contentCategoryRepo->getOrInit($categorySlug, $relatedType->code) : null);
        }
        $currentState = $this->mainModel->where('slug','=',$slug);
        if ($relatedType) {
            $currentState = $currentState->where('type_id','=',$relatedType->id);
        }
        if ($relatedCategory) {
            $currentState = $currentState->where('category_id','=',$relatedCategory->id);
        }
        $currentState = $currentState->first();
        if (!$currentState) {
            $currentState = new Content;
            $currentState->type_id = ($relatedType ? $relatedType->id : null);
            $currentState->category_id = ($relatedCategory ? $relatedCategory->id : null);
            $currentState->title = ucfirst(strtolower($slug));
            $currentState->slug = $slug;
            $currentState->highlight = "No Content Yet";
            $currentState->content = "No Content Yet";
            $currentState->image = null;
            $currentState->status = Content::PUBLISHED_STATUS;
            $currentState->save();
        }
        return $currentState;
    }

    public function getContentBy($slug, $typeCode) {
        $object = null;
        $param = [];
        $param['_type_code'] = $typeCode;
        $param['slug'] = $slug;
        $param['status'] = Content::PUBLISHED_STATUS;
        $param['paginate'] = false; // single object
        $param['perPage'] = 1; // single object
        $object = $this->getByParameter($param, [
            'optional_dependency' => ['type', 'category']
        ]);
        return $object;
    }

    public function getListOf($typeCode, $categorySlug = null, $pagination = true, $perPage = 9, $params = null) {
        $listObj = null;
        $param = [];
        $param['_type_code'] = $typeCode;
        $param['_category_slug'] = ($categorySlug ? $categorySlug : false);
        $param['status'] = Content::PUBLISHED_STATUS;
        $param['paginate'] = $pagination;
        $param['perPage'] = $perPage;
        $listObj = $this->getByParameter($param);
        return $listObj;
    }
}
