<?php

namespace Suitsite\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\MenuRepositoryContract;
use Suitsite\Models\Menu;

class MenuRepository implements MenuRepositoryContract
{
    use SuitRepositoryTrait;
    
    public function __construct(Menu $model)
    {
        $this->mainModel = $model;
    }

    public function getActiveMenu($type)
    {
        $is_active = Menu::STATUS_ACTIVE;
        return Cache::rememberForever('suitcore_menu_'.$type, function () use ($type, $is_active) {
            $menus = $this->mainModel->where(compact('type', 'is_active'))->orderByRaw('position_order * -1 DESC')->orderBy('label')->with(['children', 'children.children'])->get()->keyBy('id');
            $sorted = collect($menus)->map(function ($item, $key) use (&$menus) {
                if ($parent = $item['parent_id']) {
                    $menus[$parent]->menus = $item;
                    unset($menus[$key]);
                }
                return $item;
            });
            return $menus;
        });
    }

    public function allDescendantId($menu) {
        // cached
        if ($menu) {
            $repoHandler = $this;
            return Cache::rememberForever('descendant_menu_of_'.$menu->id, function () use($repoHandler, $menu) {
                return $repoHandler->directQueryAllDescendantId($menu);
            });
        } else {
            return [];
        }
    }

    public function directQueryAllDescendantId($menu) {
        // non cache
        if ($menu) {
            $childMenu = $menu->children;
            if ($childMenu && count($childMenu) > 0) {
                $result = [ $menu->id ];
                foreach ($childMenu as $child) {
                    $result = array_merge($result, $this->directQueryAllDescendantId($child));
                }
                return $result;
            } else {
                return [ $menu->id ];
            }
        } else {
            return [];
        }
    }

    public function cachedList($type, $fetchAll = false, $parentMenuId = 0)
    {
        if ($type) {
            $is_active = Menu::STATUS_ACTIVE;
            $menuObj = $this->mainModel;
            return Cache::rememberForever(($fetchAll ? 'all_menu_' . $type : $type . '_menu_by_'.($parentMenuId ? $parentMenuId : 'main')), function () use($fetchAll, $parentMenuId, $menuObj, $type, $is_active) {
                $menus = $menuObj->where(compact('type', 'is_active'))->orderByRaw('position_order * -1 DESC')->orderBy('label','asc');
                if ($fetchAll) $menus = $menus->get();
                elseif ($parentMenuId) $menus = $menus->with('children')->where('parent_id', '=', $parentMenuId)->get();
                else $menus = $menus->with('children')->whereNull('parent_id')->get();

                if ($menus && count($menus) > 0) return $menus;
                return collect([]);
            });
        }
        return collect([]);
    }

    // Event Listener
    public function saved(Menu $menu) {
        if ($menu) {
            Cache::forget('suitcore_menu_'.$menu->type);
            Cache::forget('descendant_menu_of_'.$menu->id);
            Cache::forget('all_menu_' . $menu->type);
            Cache::forget($menu->type . '_menu_by_main');
            Cache::forget($menu->type . '_menu_by_' . $menu->id);
            if ($menu->parent) {
                Cache::forget($menu->type . '_menu_by_' . $menu->parent->id);
            }
        }
    }

    public function deleted(Menu $menu) {
        if ($menu) {
            Cache::forget('suitcore_menu_'.$menu->type);
            Cache::forget('descendant_menu_of_'.$menu->id);
            Cache::forget('all_menu_' . $menu->type);
            Cache::forget($menu->type . '_menu_by_main');
            Cache::forget($menu->type . '_menu_by_' . $menu->id);
            if ($menu->parent) {
                Cache::forget($menu->type . '_menu_by_' . $menu->parent->id);
            }
        }
    }
}
