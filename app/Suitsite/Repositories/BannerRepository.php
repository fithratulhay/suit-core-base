<?php

namespace Suitsite\Repositories;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\BannerRepositoryContract;
use Suitsite\Models\Banner;

class BannerRepository implements BannerRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Banner $model)
    {
        $this->mainModel = $model;
    }

    public function getCachedList($type) {
        $now = Carbon::now()->format("Y-m-d H:") . "00:00"; // hourly-update
        return $this->getByParameter([
            'perPage' => self::FETCH_ALL,
            'type' => $type
        ], [
            'extended_raw_condition' => sprintf('(status = "%s" OR (status = "%s" AND active_start_date <= "%s" AND active_end_date >= "%s"))', Banner::STATUS_ACTIVE, Banner::STATUS_TIMED, $now, $now),
            'extended_raw_order' => '(position_order * -1) desc'
        ], true); // set true for cached result
    }

    public function getBanners($type = '') {
        $bannerList = $this->mainModel
                           ->where(function ($q) {
                                $now = Carbon::now()->toDateTimeString();
                                $q->where('status', $this->mainModel::STATUS_ACTIVE);
                                $q->orWhere(function ($q) use($now) {
                                    $q->where('status', $this->mainModel::STATUS_TIMED);
                                    $q->where('active_start_date', '<=', $now);
                                    $q->where('active_end_date', '>=', $now);
                                });
                           });
        if ($type) return $bannerList->where('type', $type)->get();
        return $bannerList->get();
    }
}
