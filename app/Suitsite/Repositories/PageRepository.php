<?php

namespace Suitsite\Repositories;

use Suitsite\Models\Page;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\PageRepositoryContract;

class PageRepository implements PageRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Page $model)
    {
        $this->mainModel = $model;
    }

    public function getOrInit($slug) {
        $currentState = $this->mainModel->where('slug','=',$slug)->first();
        if (!$currentState) {
            $currentState = new Page;
            $currentState->title = ucfirst(strtolower($slug));
            $currentState->slug = $slug;
            $currentState->content = "No Page Yet";
            $currentState->status = Page::PUBLISHED_STATUS;
            $currentState->save();
        }
        return $currentState;
    }

    public function getPageBy($slug) {
        $object = null;
        $param = [];
        $param['slug'] = $slug;
        $param['status'] = Page::PUBLISHED_STATUS;
        $param['paginate'] = false; // single object
        $param['perPage'] = 1; // single object
        $object = $this->getByParameter($param);
        return $object;
    }

    public function getListOf($pagination = true, $perPage = 9, $params = null) {
        $listObj = null;
        $param = [];
        $param['status'] = Page::PUBLISHED_STATUS;
        $param['paginate'] = $pagination;
        $param['perPage'] = $perPage;
        $listObj = $this->getByParameter($param);
        return $listObj;
    }

    public function getAllPageSlugInRegex() {
        try {
            $allType = $this->getBy__cached('status', Page::PUBLISHED_STATUS)->pluck('slug')->toArray();
            return '(' . implode('|', $allType) . ')';
        } catch (\Exception $e) {
            // possibility of first init
        }
        return '(defaultpage)';
    }
}
