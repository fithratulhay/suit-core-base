<?php

namespace Suitsite\Repositories;

use Illuminate\Support\Facades\Cache;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\FaqRepositoryContract;
use Suitsite\Models\Faq;

class FaqRepository implements FaqRepositoryContract
{
    use SuitRepositoryTrait;

    protected $faqCategoryRepo;

    public function __construct(Faq $model, FaqCategoryRepository $_faqCategoryRepo)
    {
        $this->mainModel = $model;
        $this->faqCategoryRepo = $_faqCategoryRepo;
    }

    public function getCachedList($categorySlug) {
        $baseModel = $this->mainModel;
        $faqCategory = ($categorySlug ? $this->faqCategoryRepo->getBy__cached('slug', $categorySlug) : null);
        if ($faqCategory) $faqCategory = $faqCategory->first();
        $faqList = [];
        if ($faqCategory) {
            $faqList = Cache::rememberForever('faq_by_category_'.$faqCategory->id, function () use($baseModel, $faqCategory) {
                return $baseModel->where('faq_category_id','=',$faqCategory->id)->orderByRaw('(position_order * -1) desc')->get()->keyBy('id');
            });
        } else {
            $faqList = Cache::rememberForever('faq_by_category_all', function () use($baseModel) {
                return $baseModel->orderByRaw('(position_order * -1) desc')->get()->keyBy('id');
            });
        }
        return $faqList;
    }
}
