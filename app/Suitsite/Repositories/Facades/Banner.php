<?php

namespace Suitsite\Repositories\Facades;

class Banner extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\BannerRepositoryContract';
    }
}
