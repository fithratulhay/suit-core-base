<?php

namespace Suitsite\Repositories\Facades;

class FaqCategory extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\FaqCategoryRepositoryContract';
    }
}
