<?php

namespace Suitsite\Repositories\Facades;

class Menu extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\MenuRepositoryContract';
    }
}
