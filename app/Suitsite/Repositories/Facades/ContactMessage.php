<?php

namespace Suitsite\Repositories\Facades;

class ContactMessage extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\ContactMessageRepositoryContract';
    }
}
