<?php

namespace Suitsite\Repositories\Facades;

class Newsletter extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\NewsletterRepositoryContract';
    }
}
