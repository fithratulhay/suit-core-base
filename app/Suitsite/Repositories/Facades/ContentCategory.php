<?php

namespace Suitsite\Repositories\Facades;

class ContentCategory extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\ContentCategoryRepositoryContract';
    }
}
