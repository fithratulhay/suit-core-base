<?php

namespace Suitsite\Repositories\Facades;

class ContentType extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\ContentTypeRepositoryContract';
    }
}
