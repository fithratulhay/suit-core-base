<?php

namespace Suitsite\Repositories\Facades;

class NewsletterSubscriber extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract';
    }
}
