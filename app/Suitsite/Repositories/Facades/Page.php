<?php

namespace Suitsite\Repositories\Facades;

class Page extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'Suitsite\Repositories\Contract\PageRepositoryContract';
    }
}
