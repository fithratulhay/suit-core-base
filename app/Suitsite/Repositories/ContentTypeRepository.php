<?php

namespace Suitsite\Repositories;

use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\ContentTypeRepositoryContract;
use Suitsite\Models\ContentType;

class ContentTypeRepository implements ContentTypeRepositoryContract
{
    use SuitRepositoryTrait;
    
    public function __construct(ContentType $model)
    {
        $this->mainModel = $model;
    }

    public function getOrInit($code) {
        $currentState = $this->mainModel->where('code','=',$code)->first();
        if (!$currentState) {
            $currentState = new ContentType;
            $currentState->name = ucfirst(strtolower($code));
            $currentState->code = $code;
            $currentState->save();
        }
        return $currentState;
    }

    public function getTypeBy($code) {
        $currentState = $this->mainModel->where('code','=',$code)->first();
        return $currentState;
    }

    public function getAllTypeInRegex() {
        try {
            $allType = $this->all__cached()->pluck('code')->toArray();
            return '(' . implode('|', $allType) . ')';
        } catch (\Exception $e) {
            // possibility of first init
        }
        return '(default)';
    }
}
