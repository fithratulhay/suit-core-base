<?php

namespace Suitsite\Repositories;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitsite\Repositories\Contract\ArticleRepositoryContract;
use Suitsite\Models\Article;

class ArticleRepository implements ArticleRepositoryContract
{
    use SuitRepositoryTrait;

    public function __construct(Article $model)
    {
        $this->mainModel = $model;
    }
}
