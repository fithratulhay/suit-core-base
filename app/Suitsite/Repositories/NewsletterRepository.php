<?php

namespace Suitsite\Repositories;

use DB;
use Mail;
use Carbon\Carbon;
use Suitcore\Notifications\Notifiable;
use Suitcore\Models\SuitUser;
use Suitcore\Repositories\Traits\SuitRepositoryTrait;
use Suitcore\Repositories\Contract\SuitUserRepositoryContract;
use Suitsite\Repositories\Contract\NewsletterRepositoryContract;
use Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract;
use Suitsite\Models\Newsletter;
use Suitsite\Models\NewsletterSubscriber;

class NewsletterRepository implements NewsletterRepositoryContract
{
    use Notifiable, SuitRepositoryTrait;

    public $email;
    public $emailList;

    protected $userRepo;
    protected $subscriberRepo;

    public function __construct(Newsletter $model, SuitUserRepositoryContract $_userRepo, NewsletterSubscriberRepositoryContract $_subscriberRepo)
    {
        $this->mainModel = $model;
        $this->userRepo = $_userRepo;
        $this->subscriberRepo = $_subscriberRepo;
    }

    public function getRecipients(Newsletter $newsletter)
    {
        $recipientType = $newsletter->recipient;
        $prevNbRecepient = ($newsletter->actual_nb_recepient ? $newsletter->actual_nb_recepient : 0);
        $maxNbRecepient = ($newsletter->limited_nb_sent && $newsletter->limited_nb_sent >= 0 ? ($newsletter->limited_nb_sent - $prevNbRecepient) : $this->mainModel::UNLIMITED_SENT);
        $_emailList = [];
        if ($recipientType == $this->mainModel::GUEST_SUBSCRIBER) {
            $_emailList = $this->getGuestSubscriberEmails($maxNbRecepient);
        }
        if ($recipientType == $this->mainModel::USER) {
            $_emailList = $this->getUserEmails($maxNbRecepient);
        }
        if ($recipientType == $this->mainModel::ADMIN) {
            $_emailList = $this->getAdminEmails($maxNbRecepient);
        }
        if ($recipientType == $this->mainModel::ALL) {
            $_emailList = array_merge(
                $this->getGuestSubscriberEmails($this->mainModel::UNLIMITED_SENT),
                $this->getUserEmails($this->mainModel::UNLIMITED_SENT)
            );
            $_emailList = array_slice($_emailList, 0, $maxNbRecepient);
        }
        foreach ($_emailList as $email => $name) {
            if (!$this->validateEmail($email)) {
                unset($_emailList[$email]);
            }
        }
        return $_emailList;
    }

    public function validateEmail($email) {
        $pattern = '/^("[\w-\.\s]+")|(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/i';
        if(preg_match($pattern, $email))
            return true;
        else
            return false;
    }

    public function getGuestSubscriberEmails($maxNbRecepient)
    {
        $emails = $this->subscriberRepo->getMainModel()->whereRaw("email REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'");
        if ($maxNbRecepient > $this->mainModel::UNLIMITED_SENT) {
            $emails = $emails->take($maxNbRecepient);
        }
        $emails = $this->filterEmailAddrList( $emails->get()->pluck('name', 'email')->toArray() );
        return $emails;
    }

    public function getUserEmails($maxNbRecepient)
    {
        $emails = $this->userRepo->getMainModel()->whereRaw("email REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'")->where('status', $this->userRepo->getMainModel()::STATUS_ACTIVE);
        if ($maxNbRecepient > $this->mainModel::UNLIMITED_SENT) {
            $emails = $emails->take($maxNbRecepient);
        }
        $emails = $this->filterEmailAddrList( $emails->get()->pluck('name', 'email')->toArray() );
        return $emails;
    }

    public function getAdminEmails($maxNbRecepient)
    {
        $emails = $this->userRepo->getMainModel()->where('role', $this->userRepo->getMainModel()::ADMIN)->whereRaw("email REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'")->where('status', $this->userRepo->getMainModel()::STATUS_ACTIVE);
        if ($maxNbRecepient > $this->mainModel::UNLIMITED_SENT) {
            $emails = $emails->take($maxNbRecepient);
        }
        $emails = $this->filterEmailAddrList( $emails->get()->pluck('name', 'email')->toArray() );
        return $emails;
    }

    public function manualSingleExecution($emailAddressList, $recepientType)
    {
        $nbNewsletterExecuted = 0;
        if (is_array($emailAddressList)) {
            $newsletters = $this->mainModel->where('status', $this->mainModel::READYTOEXECUTE)->where('recipient', '=', $recepientType)->get();
            foreach ($newsletters as $key => $newsletter) {
                $newsletter->actual_nb_recepient = ($newsletter->actual_nb_recepient ? $newsletter->actual_nb_recepient : 0);
                if (($newsletter->limited_nb_sent == $this->mainModel::UNLIMITED_SENT) || 
                    ($newsletter->limited_nb_sent > $this->mainModel::UNLIMITED_SENT && $newsletter->actual_nb_recepient < $newsletter->limited_nb_sent)) {
                    if ($newsletter->limited_nb_sent == $this->mainModel::UNLIMITED_SENT) {
                        $this->emailList = $emailAddressList;
                    } else {
                        $nbFreeSlot = $newsletter->limited_nb_sent - $newsletter->actual_nb_recepient;
                        $this->emailList = array_slice($emailAddressList, 0, $nbFreeSlot);
                    }
                    $nbEmailAddrExecuted = 0;
                    if ($this->emailList && is_array($this->emailList) && count($this->emailList) > 0) {
                        foreach ($this->emailList as $_email => $_name) {
                            $this->email = $_email;
                            $this->notify('Suitsite\\Notifications\\NewsletterNotifier', null, $newsletter, $_name);
                            $nbEmailAddrExecuted++;
                        }
                    }
                    $nextActualNbRecepient = ( ($newsletter->actual_nb_recepient ? $newsletter->actual_nb_recepient : 0) + $nbEmailAddrExecuted );
                    $newsletter->update([
                        'actual_nb_recepient' => $nextActualNbRecepient,
                        'status' => ( ($newsletter->limited_nb_sent == $this->mainModel::UNLIMITED_SENT) || ($newsletter->limited_nb_sent != $this->mainModel::UNLIMITED_SENT && $nextActualNbRecepient >= $newsletter->limited_nb_sent) ? $this->mainModel::EXECUTED : $this->mainModel::READYTOEXECUTE )
                    ]);
                    $nbNewsletterExecuted++;
                }
            }
        }
        return $nbNewsletterExecuted;
    }

    public function execute()
    {
        $newsletters = $this->mainModel->where('status', $this->mainModel::READYTOEXECUTE)->where('recipient', '<>', $this->mainModel::NEW_GUEST_SUBSCRIBER)->get();
        $nbNewsletterExecuted = 0;
        foreach ($newsletters as $key => $newsletter) {
            $this->emailList = $this->getRecipients($newsletter);
            $nbEmailAddrExecuted = 0;
            if ($this->emailList && is_array($this->emailList) && count($this->emailList) > 0) {
                foreach ($this->emailList as $_email => $_name) {
                    $this->email = $_email;
                    $this->notify('Suitsite\\Notifications\\NewsletterNotifier', null, $newsletter, $_name);
                    $nbEmailAddrExecuted++;
                }
            }
            $newsletter->update([
                'actual_nb_recepient' => (($newsletter->actual_nb_recepient ? $newsletter->actual_nb_recepient : 0) + $nbEmailAddrExecuted),
                'status' => $this->mainModel::EXECUTED
            ]);
            $nbNewsletterExecuted++;
        }
        return $nbNewsletterExecuted;
    }

    private function filterEmailAddrList(array $arrList) {
        $arrResult = $arrList;
        $a = array_map('trim', array_keys($arrResult));
        $b = array_map('trim', $arrResult);
        $arrResult = array_combine($a, $b);
        $arrResult = array_change_key_case( $arrResult );
        return $arrResult;
    }
}
