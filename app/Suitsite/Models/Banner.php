<?php

namespace Suitsite\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;

/*
|--------------------------------------------------------------------------
| banners Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * type VARCHAR(45)
| * position_order INT(10)
| * title VARCHAR
| * subtitle VARCHAR
| * highlight VARCHAR
| * url VARCHAR(255)
| * image VARCHAR(45)
| * status VARCHAR (40)
| * active_start_date datetime NOT NULL
| * active_end_date datetime NOT NULL
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Banner extends SuitModel
{
    // Standard / Default Type
    const TYPE_MAIN = "main-banner";
    const TYPE_SIDE = "side-banner";

    // Status
    const STATUS_ACTIVE = "active";
    const STATUS_TIMED = "timed";
    const STATUS_INACTIVE = "inactive";

    // MODEL DEFINITION
    public $table = 'banners';

    protected static $bufferAttributeSettings = null;
 
    public $imageAttributes = [
        'image' => 'bannerimages'
    ];

    public $files = [
        'image' => 'bannerimages'
    ];

    public $fillable = [
        'type',
        'position_order',
        'title',
        'subtitle',
        'highlight',
        'url',
        'image',
        'status',
        'active_start_date',
        'active_end_date'
    ];

    public $rules = [
        //'image'=>'required|mimes:jpg,jpeg,png,gif,bmp',
        'title' => 'required',
        'active_start_date' => 'date|required_if:status,timed|nullable',
        'active_end_date' => 'date|required_if:status,timed|nullable'
    ];

    protected $extendedThumbnailStyle = [
        'extra_small_square' => '64x64',
    ];

    protected $appends = ['type_attribute_label', 'status_attribute_label'];

    // EXTENDED ATTRIBUTES / MUTATOR
    public function getTypeAttributeLabelAttribute() {
        return $this->getTypeOptions()[$this->type] ?? $this->type;
    }

    public function getStatusAttributeLabelAttribute() {
        return $this->getStatusOptions()[$this->status];
    }

    // MODEL SCOPE
    /**
     * Filter banner with given type.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisibleForType($query, $type)
    {
        return $query->whereNotNull('image')
                     ->where(function ($q) {
                        $q->where('status', '=', self::STATUS_ACTIVE)
                          ->orWhere(function ($sq) {
                            $sq->where('status', '=', self::STATUS_TIMED)
                               ->where('active_start_date', '<=', DB::raw('date_format(NOW(),"%Y-%m-%d %H:%i:%S")'))
                               ->where('active_end_date', '>=', DB::raw('date_format(NOW(),"%Y-%m-%d %H:%i:%S")'));
                          });
                     })
                     ->where('type', $type);
    }

    /**
     * Get options of type
     *
     */
    public function getTypeOptions() {
        $default = isset(DefaultConfig::getConfig()['banner_type']) ? DefaultConfig::getConfig()['banner_type'] : null;
        if ($default) {
            return $default;
        }
        return [
            self::TYPE_MAIN => "Main Banner", 
            self::TYPE_SIDE => "Side Banner"
        ];
    }

    /**
     * Get options of status
     *
     */
    public function getStatusOptions() {
        return [self::STATUS_ACTIVE => "Active", 
                self::STATUS_TIMED => "Timed", 
                self::STATUS_INACTIVE => "Non Active"
        ];
    }

    public function getLabel() {
        return "Banner";
    }

    public function getFormattedValue() {
        return $this->text;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "type" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Banner Type",
                "options" => $this->getTypeOptions(),
                "filterable" => true
            ],
            "position_order" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Position Order"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => true
            ],
            "subtitle" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Sub-Title",
                "translation" => true
            ],
            "highlight" => [
                "type" => self::TYPE_TEXT,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Highlight",
                "translation" => true
            ],
            "url" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "URL"
            ],
            "image" => [
                "type" => self::TYPE_FILE,
                "visible" => false,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Image File (minimum width 1280 pixels)"
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "filterable" => true
            ],
            "active_start_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Timed Start Datetime",
                "elmt_group" => [
                    "name" => "active_date_range",
                    "label" => "Optional Timed Datetime",
                    "type" => self::GROUPTYPE_DATETIMERANGE,
                    "role" => self::GROUPROLE_RANGE_START
                ]
            ],
            "active_end_date" => [
                "type" => self::TYPE_DATETIME,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Optional Timed End Datetime",
                "elmt_group" => [
                    "name" => "active_date_range",
                    "label" => "Optional Timed Datetime",
                    "type" => self::GROUPTYPE_DATETIMERANGE,
                    "role" => self::GROUPROLE_RANGE_END
                ]
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }
}
