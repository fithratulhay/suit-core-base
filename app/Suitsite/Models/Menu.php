<?php

namespace Suitsite\Models;

use Illuminate\Support\Facades\Cache;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;

/*
|--------------------------------------------------------------------------
| menus Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * type VARCHAR(16) NOT NULL
| * position_order INTEGER NULL
| * parent_id INTEGER NULL
| * label VARCHAR(64) NOT NULL
| * url VARCHAR(1024) NOT NULL
| * is_active TINYINT(1) NOT NULL
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Menu extends SuitModel
{
    const HEADER = 'header';
    const LEFTSIDE = 'leftside';
    const RIGHTSIDE = 'rightside';
    const FOOTER = 'footer';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    // MODEL DEFINITION
    protected $primaryKey = 'id';

    public $table = 'menus';

    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'type',
        'position_order',
        'parent_id',
        'label',
        'url',
        'is_active',
    ];

    public $rules = [
        'type' => 'required',
        'label' => 'required',
        'url' => 'required',
        'is_active' => 'required'
    ];

    // ACCESSOR
    public function getFullLabelAttribute() {
        $typeOptions = $this->getTypeOptions();
        $menuType = (isset($typeOptions[$this->type]) ? $typeOptions[$this->type] : str_text_beautifier($this->type));
        return $menuType . " / " . $this->getFormattedValue();
    }

    // RELATIONSHIP
    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    // SUITCORE-RELATED
    public function getLabel()
    {
        return 'Menu';
    }

    public function getFormattedValue()
    {   
        return ($this->parent ? $this->parent . " / " : "") . $this->label;
    }

    public function getFormattedValueColumn()
    {
        return ['label'];
    }

    public function getDefaultOrderColumn() {
        return 'position_order';
    }

    public function getDefaultOrderColumnDirection() {
        return 'desc';
    }

    public function getOptions()
    {
        return self::all();
    }

    public function getStatusOptions()
    {
        return [
            static::STATUS_ACTIVE => 'Active',
            static::STATUS_INACTIVE => 'Inactive',
        ];
    }

    public function getTypeOptions()
    {
        $default = isset(DefaultConfig::getConfig()['menu_type']) ? DefaultConfig::getConfig()['menu_type'] : null;

        if ($default) {
            return $default;
        }

        return [
            static::HEADER => title_case(static::HEADER),
            static::LEFTSIDE => title_case(static::LEFTSIDE),
            static::RIGHTSIDE => title_case(static::RIGHTSIDE),
            static::FOOTER => title_case(static::FOOTER),
        ];
    }

    public function getAttributeSettings()
    {
        // default attribute settings of generic model, override for furher use
        return [
            'id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => false,
                'required' => true,
                'relation' => null,
                'label' => 'ID'
            ],
            'type' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Type',
                'options' => $this->getTypeOptions(),
                'filterable' => true,
                'form_field_relation' => [
                    'top' => '',
                    'bottom' => 'parent_id'   
                ],
                'options_url' => 'static',
            ],
            'position_order' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Position Order'
            ],
            'parent_id' => [
                'type' => self::TYPE_NUMERIC,
                'visible' => true,
                'formdisplay' => true,
                'required' => false,
                'relation' => 'parent',
                'label' => 'Parent Menu',
                'options' => [], // static::where('id', '<>', $this->id)->get()->pluck('full_label', 'id'),
                'form_field_relation' => [
                    'top' => 'type',
                    'bottom' => ''   
                ],
                'options_url' => (config('suitsite.data_options_route_name.menu') ? route(config('suitsite.data_options_route_name.menu')) : ''),
                'filterable' => true,
            ],
            'label' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Label',
                'translation' => true
            ],
            'url' => [
                'type' => self::TYPE_TEXT,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'URL',
            ],
            'is_active' => [
                'type' => self::TYPE_BOOLEAN,
                'visible' => true,
                'formdisplay' => true,
                'required' => true,
                'relation' => null,
                'label' => 'Is Active?',
            ],
            'created_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' =>'Created At'
            ],
            'updated_at' => [
                'type' => self::TYPE_DATETIME,
                'visible' => false,
                'formdisplay' => false,
                'required' => false,
                'relation' => null,
                'label' => 'Updated At'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forget('suitcore_menu_' . $model->type);
        });

        static::deleted(function ($model) {
            Cache::forget('suitcore_menu_' . $model->type);
        });
    }
}
