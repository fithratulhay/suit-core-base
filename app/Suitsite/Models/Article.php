<?php

namespace Suitsite\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Suitcore\Models\SuitModel;
use Suitsite\Config\DefaultConfig;

/*
|--------------------------------------------------------------------------
| banners Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * type VARCHAR(45)
| * position_order INT(10)
| * title VARCHAR
| * subtitle VARCHAR
| * highlight VARCHAR
| * url VARCHAR(255)
| * image VARCHAR(45)
| * status VARCHAR (40)
| * active_start_date datetime NOT NULL
| * active_end_date datetime NOT NULL
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Article extends SuitModel
{
    // Standard / Default Type

    // Status

    // MODEL DEFINITION
    public $table = 'articles';

    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'title',
        'content'
    ];

    public $rules = [
        'title' => 'required',
        'content' => 'required'
    ];

    protected $appends = ['type_attribute_label', 'status_attribute_label'];

    // EXTENDED ATTRIBUTES / MUTATOR

    // MODEL SCOPE
    /**
     * Filter banner with given type.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */

     /**
     * Get options of type
     *
     */

    /**
     * Get options of status
     *
     */

    public function getLabel() {
        return "Article";
    }

    public function getFormattedValue() {
        return $this->text;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => true
            ],
            "content" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Content",
                "translation" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }
}
