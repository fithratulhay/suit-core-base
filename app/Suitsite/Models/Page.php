<?php

namespace Suitsite\Models;

use DB;
use File;
use Cviebrock\EloquentSluggable\Sluggable;
use Suitcore\Models\SuitModel;

/*
|--------------------------------------------------------------------------
| pages Table Structure
|--------------------------------------------------------------------------
| * id INT(10) NOT NULL
| * title varchar(128) NOT NULL
| * slug varchar(128)
| * content MEDIUMTEXT NOT NULL
| * status varchar(16)
| * created_at TIMESTAMP
| * updated_at TIMESTAMP
*/
class Page extends SuitModel
{
    use Sluggable;

    const PUBLISHED_STATUS = 'published';
    const DRAFT_STATUS = 'draft';

    // MODEL DEFINITION
    public $table = 'pages';

    protected static $bufferAttributeSettings = null;

    public $fillable = [
        'title',
        'slug',
        'content',
        'status'
    ];

    public $rules = [
        'title' => 'required',
        // 'slug' => 'required',
        'content' => 'required',
        // 'status' => 'required'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function baseNewQuery()
    {
        return parent::newQuery();
    }

    /**
     * Scope a query to only include published page
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('status', static::PUBLISHED_STATUS);
    }

    /**
     * Scope a query to only include draft page
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDraft($query)
    {
        return $query->where('status', static::DRAFT_STATUS);
    }

    /**
     * Get options of status
     *
     */
    public function getStatusOptions() {
        return [self::DRAFT_STATUS => ucfirst(strtolower(self::DRAFT_STATUS)),
                self::PUBLISHED_STATUS => ucfirst(strtolower(self::PUBLISHED_STATUS))
        ];
    }

    public function getLabel() {
        return "Static Page";
    }

    public function getFormattedValue() {
        return $this->title;
    }

    public function getOptions() {
        return self::all();
    }

    public function getAttributeSettings() {
        return [
            "id" => [
                "type" => self::TYPE_NUMERIC,
                "visible" => true,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "ID"
            ],
            "title" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Title",
                "translation" => true
            ],
            "slug" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => false,
                "relation" => null,
                "label" => "Slug (generated if empty)"
            ],
            "content" => [
                "type" => self::TYPE_RICHTEXTAREA,
                "visible" => false,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Contents",
                "translation" => true
            ],
            "status" => [
                "type" => self::TYPE_TEXT,
                "visible" => true,
                "formdisplay" => true,
                "required" => true,
                "relation" => null,
                "label" => "Status",
                "options" => $this->getStatusOptions(),
                "filterable" => true
            ],
            "created_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Created At"
            ],
            "updated_at" => [
                "type" => self::TYPE_DATETIME,
                "visible" => false,
                "formdisplay" => false,
                "required" => true,
                "relation" => null,
                "label" => "Updated At"
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

        });

        static::updating(function ($model) {

        });
    }
}
