<?php

namespace Suitsite\Notifications;

use Suitcore\Notifications\BaseNotification;
use Suitsite\Models\Newsletter;

class NewsletterNotifier extends BaseNotification
{
    protected $view = 'emails.newsletter';
    protected $newsletter;

    public function __construct($link = null, $newsletter = null)
    {
        parent::__construct($link);
        $this->newsletter = $newsletter ?: (static::$preview ? Newsletter::latest()->first() : new Newsletter);
        $this->subject = $this->newsletter->email_subject;
    }

    public function setDataView($notifiable)
    {
        $this->dataView = ['newsletter' => $this->newsletter];
    }

    // only notif by email
    public function via($notifiable)
    {
        return ['mail'];
    }
}
