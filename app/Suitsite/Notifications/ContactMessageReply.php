<?php

namespace Suitsite\Notifications;

use Carbon\Carbon;
use App\Models\BranchContactMessage;
use Suitcore\Notifications\BaseNotification;
use Suitsite\Models\ContactMessage;

class ContactMessageReply extends BaseNotification
{
    protected $view = 'emails.contactmessage.reply';
    protected $contactmessage;
    protected $replyEmail;
    protected $replyName;

    public function __construct($link = null, $contactmessage = null)
    {
        parent::__construct($link);
        $contactMessagePrototype = new ContactMessage;
        $contactMessagePrototype->fill([
            'sender_name' => 'Customer',
            'sender_email' => 'customer@email.net',
            'category' => 'Any Issue',
            'content' => 'I have any issue',
            'reply' => 'Ok we will handle it',
            'status' => ContactMessage::MESSAGE_REPLIED
        ]);
        $contactMessagePrototype->created_at = $contactMessagePrototype->updated_at = Carbon::now()->toDateTimeString();
        $this->contactmessage = $contactmessage ?: $contactMessagePrototype;
        $this->subject = "Reply For " . ($this->contactmessage->getCategoryOptions()[$this->contactmessage->category] ?? '');

        $this->replyEmail = settings('email');
        $this->replyName = 'Administrator';
    }

    public function setDataView($notifiable)
    {
        $this->dataView = [
            'name' => $this->contactmessage->sender_name,
            'contactmessage' => $this->contactmessage
        ];
    }

    // only notif by email
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return parent::toMail($notifiable)->replyTo($this->replyEmail, $this->replyName);
    }
}
