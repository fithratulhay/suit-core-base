<?php

namespace Suitsite\Notifications;

class ContactMessageForward extends ContactMessageReply
{
    protected $view = 'emails.contactmessage.forward';

    public function __construct($link = null, $contactmessage = null)
    {
        parent::__construct($link, $contactmessage);
        $this->replyEmail = $this->contactmessage->sender_email;
        $this->replyName = $this->contactmessage->sender_name;

        $this->subject = 'Customer Message '
                         . ($this->contactmessage->getCategoryOptions()[$this->contactmessage->category] ?? '');
    }

    public function setDataView($notifiable)
    {
        $this->dataView = [
            'name' => $this->contactmessage->sender_name,
            'contactmessage' => $this->contactmessage,
            'replyName' => $this->replyName,
            'replyEmail' => $this->replyEmail,
            'replyLink' => route('backend.contactmessage.show', ['id' => $this->contactmessage->id])
        ];
    }
}
