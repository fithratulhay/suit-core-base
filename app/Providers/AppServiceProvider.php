<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Suitcore\Providers\LoadRepositoryConfigTrait;

class AppServiceProvider extends ServiceProvider
{
    use LoadRepositoryConfigTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Any Application Specific Service Providers
        $this->registerAppService();
    }

    /**
     * App Service
     **/
    public function registerAppService() {
        
        $repositories = config('suitapp.repositories');
        $this->loadFromConfig($repositories);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $repositories = config('suitapp.repositories');
        return array_keys($repositories);
    }
}
