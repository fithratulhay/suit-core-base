<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate, SuitUserRoleRepositoryContract $roleRepo)
    {
        $this->registerPolicies($gate);

        // Gate
        $gate->define('suitcorepermission', function ($user, $baseModel = null, $routeName = null) use($roleRepo) {
            $checkedRoute = (!empty($routeName) ? $routeName : \Request::route()->getName());
            return $roleRepo->isPermitted($user, $checkedRoute, $baseModel);
        });
    }
}
