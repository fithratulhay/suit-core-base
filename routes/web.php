<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
  |--------------------------------------------------------------------------
  | Pattern Shortcut
  |--------------------------------------------------------------------------
  |
  | Define frequently used pattern
  |
 */

Route::pattern('id', '\d+');
Route::pattern('slug', '[a-z0-9-]+');

/*
 * Routes List
 */

if (config('suitcore.backend_admin_subpath_access') == 'subpath') {
  // Route Authentication
  Route::group(['middleware' => 'web'], function () {
      // User Authentication
      Route::resource('sessions', 'Auth\SessionController')->except(['index']);
      Route::get('sessions', function () {
        return redirect('/');
      });
      Route::get('logout', ['as'=>'sessions.logout', 'middleware' => 'web_auth', 'uses' =>'Auth\SessionController@destroy']);
      Route::group(['middleware' => 'guest'], function () {
      	// standard / traditional login
          Route::get('login', ['as'=>'sessions.login', 'uses' =>'Auth\SessionController@create']);
          Route::post('guest', ['as'=>'sessions.guest', 'uses' =>'Auth\SessionController@guest']);
          // social media integration
          Route::get('auth/{app}', ['as'=>'sessions.auth', 'uses' =>'Auth\SessionController@auth']);
          Route::get('redirect/auth/{app}', ['as'=>'sessions.redirect.auth', 'uses' =>'Auth\SessionController@redirectAuth']);
      });
  });
}

// Route Backend
if (config('suitcore.backend_admin_subpath_access') == 'subdomain') {
  Route::group(['domain' => UserRole::getCurrentRolePath(request()) . '.' . config('suitcore.base_domain'), 'middleware' => ['web']], function() {
    include("web_backend.php");
  });
} else {
  Route::group(['prefix' => UserRole::getCurrentRolePath(request()), 'middleware' => ['web']], function() {
    include("web_backend.php");
  });
}

// Route Frontend
include("web_frontend.php");

// Route Mobile
include("web_mobile.php");

// Other Web Group-Routes as needed
// include ("web_others.php");
