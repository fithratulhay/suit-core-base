<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
  |--------------------------------------------------------------------------
  | Pattern Shortcut
  |--------------------------------------------------------------------------
  |
  | Define frequently used pattern
  |
 */
Route::pattern('id', '\d+');

// Route API
// Base
Route::get('/', function (Request $request) {
    return "-";
});

// API FAQ
Route::group(['prefix' => 'faq'], function () {
  Route::get('/', ['as'=>'api.v1.faq.list', 'uses'=>'Api\FaqController@getIndex']);
  Route::get('{id}', ['as' => 'api.v1.faq.detail', 'uses' => 'Api\FaqController@getDetail']);
});

// API User
Route::group(['prefix' => 'user'], function () {
    Route::post('register', ['as' => 'api.v1.user.registration', 'uses' => 'Api\AuthController@register']);
    // Route::get('forgot-password/{email}', ['as' => 'api.v1.user.forgotpassword', 'uses' => 'Api\AuthController@forgot']);
    Route::get('send-verification/{email}', ['as' => 'api.v1.user.sendverification', 'uses' => 'Api\AuthController@sendVerification']);
    Route::get('verify-email/{code}/{email}', ['as' => 'api.v1.user.verify.process', 'uses' => 'Api\AuthController@verify']);
    Route::post('forget_password', ['as' => 'api.v1.user.forget.password', 'uses' => 'Api\UserController@postForgetPassword']);

    Route::group(['prefix' => 'login'], function () {
        Route::post('', ['as' => 'api.v1.user.login', 'uses' => 'Api\AuthController@login']);
        Route::post('facebook', ['as' => 'api.v1.user.login.facebook', 'uses' => 'Api\AuthController@loginFacebook']);
        Route::post('google', ['as' => 'api.v1.user.login.google', 'uses' => 'Api\AuthController@loginGoogle']);
        Route::post('linkedin', ['as' => 'api.v1.user.login.linkedin', 'uses' => 'Api\AuthController@loginLinkedin']);
    });

    Route::group(['middleware' => ['authenticatedapi']], function () {
        Route::post('send_fcm', ['as' => 'api.v1.user.send_fcm', 'uses' => 'Api\UserController@sendFCM']);
        Route::get('list', ['as' => 'api.v1.user.list', 'uses' => 'Api\UserController@getIndex']);
        Route::post('logout', ['as' => 'api.v1.user.logout', 'uses' => 'Api\AuthController@logout']);
        Route::get('{id}', ['as' => 'api.v1.user.detail', 'uses' => 'Api\UserController@show']);

        Route::post('me/clear_issue', ['as' => 'api.v1.user.profile.update', 'uses' => 'Api\UserController@clearIssue']);
        Route::get('me', ['as' => 'api.v1.user.profile.show', 'uses' => 'Api\UserController@showAccount']);
        Route::post('me', ['as' => 'api.v1.user.profile.update', 'uses' => 'Api\UserController@updateAccount']);

        Route::get('profile', ['as' => 'api.v1.user.profile.show', 'uses' => 'Api\UserController@showProfile']);
        Route::post('profile/add', ['as' => 'api.v1.user.profile.create', 'uses' => 'Api\UserController@create']);
        Route::post('profile', ['as' => 'api.v1.user.profile.update', 'uses' => 'Api\UserController@updateProfile']);
        Route::post('deleteaccount', ['as' => 'api.v1.user.deleteaccount', 'uses' => 'Api\UserController@deleteAccount']);

        Route::post('socmed', ['as' => 'api.v1.user.socmed', 'uses' => 'Api\UserController@updateSocmed']);

        Route::get('check/{email}', ['as' => 'api.v1.user.check', 'uses' => 'Api\AuthController@checkEmail']);

        Route::post('add-gcmid', ['as' => 'api.v1.user.register.gcmid', 'uses' => 'Api\AuthController@addGcmId']);
        Route::post('remove-gcmid', ['as' => 'api.v1.user.unregister.gcmid', 'uses' => 'Api\AuthController@removeGcmId']);

        /*
         * User Setting
         */
        Route::get('settings', ['as' => 'api.v1.user.settings.show', 'uses' => 'Api\User\SettingController@show']);
        Route::post('settings', ['as' => 'api.v1.user.settings.update', 'uses' => 'Api\User\SettingController@update']);
    });
});
