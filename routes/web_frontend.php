<?php
/*
|--------------------------------------------------------------------------
| Frontend Web Routes
|--------------------------------------------------------------------------
|
| Here is a route group for frontend context, such as public site, member site or other
| site that accessed by user role from external business context / process flow 
| (outside company)
| From these point of view, you can add another group if neccessary
|
*/

// VARNISH CACHED ROUTE IF NEEDED
// Route::group(['middleware' => 'varnishcacheable:15'], function() { // using time-init
Route::group(['middleware' => 'varnishcacheable'], function() {

});

// HOME INDEX
Route::get('/', ['as' => 'frontend.home', 'uses' => 'Frontend\HomeController@getIndex']);
Route::get('/pattern', ['as' => 'frontend.pattern', 'uses' => 'Frontend\HomeController@getPattern']);

// CONTENT & STATIC PAGE
Route::get('/search', ['as'=>'frontend.home.content.search', 'uses'=>'Frontend\ContentController@getSearchContent']);
Route::get('/{type}/{slug?}', ['as'=>'frontend.home.content.dynamic', 'uses'=>'Frontend\ContentController@getDynamicContent'])->where(['type' => app('Suitsite\Repositories\Contract\ContentTypeRepositoryContract')->getAllTypeInRegex()]);
Route::get('/{pageslug}', ['as'=>'frontend.home.page', 'uses'=>'Frontend\PageController@getPage'])->where(['pageslug' => app('Suitsite\Repositories\Contract\PageRepositoryContract')->getAllPageSlugInRegex()]);

// CONTACT US
Route::get('/contactus', ['as' => 'frontend.home.contactus', 'uses' => 'Frontend\HomeController@getContactform']);
Route::post('/contactus', ['as' => 'frontend.home.contactus.save', 'uses' => 'Frontend\HomeController@postContactform']);

// FAQ
Route::get('/faq/{slug?}', ['as' => 'frontend.home.faq', 'uses' => 'Frontend\HomeController@getFaq']);

// GUEST USER CONTEXT
Route::group(['middleware' => 'guest'], function () {
    Route::get('register', ['as' => 'frontend.user.registration', 'uses' => 'Frontend\UserController@getRegistration']);
    Route::post('register', ['as' => 'frontend.user.registration.save', 'uses' => 'Frontend\UserController@postRegistration']);
    Route::get('/forgetpassword', ['as' => 'frontend.user.forgetpassword', 'uses' => 'Frontend\UserController@getForgetPassword']);
    Route::post('/forgetpassword', ['as' => 'frontend.user.forgetpassword.send', 'uses' => 'Frontend\UserController@postForgetPassword']);
    Route::get('confirmresetpassword/{token?}', ['as' => 'frontend.user.forgetpassword.confirm', 'uses' => 'Frontend\UserController@getConfirmResetPassword']);
    Route::post('confirmresetpassword/{token?}', ['as' => 'frontend.user.forgetpassword.confirm.submit', 'uses' => 'Frontend\UserController@postConfirmResetPassword']);
    Route::get('activateuser/{id}/{hashcode}', ['as' => 'frontend.user.activation', 'uses' => 'Frontend\UserController@activateAccount']);
    Route::get('activationsuccess', ['as' => 'frontend.user.activation.success', 'uses' => 'Frontend\UserController@activateAccountSuccess']);
    Route::get('activationfailed', ['as' => 'frontend.user.activation.failed', 'uses' => 'Frontend\UserController@activateAccountFailed']);
    Route::get('producehashcode/{id}', ['as' => 'frontend.user.hashcode', 'uses' => 'Frontend\UserController@produceHashCode']);
    Route::get('reactivation',['as'=>'frontend.reactivation.link','uses'=>'Frontend\UserController@getUserActivation']);
});

// AUTHENTICATED USER CONTEXT
Route::group(['middleware' => ['web','auth']], function () {
    // User Pofile
    Route::get('/myprofile', ['as'=>'user.myprofile',  'uses' =>'Frontend\UserController@getMyProfile']);
    Route::get('/profile/{userId}', ['as'=>'user.profile',  'uses' =>'Frontend\UserController@getProfile']);
    Route::get('/dashboard/account', ['as'=>'user.dashboard.account',  'uses' =>'Frontend\UserController@getAccountInformation']);
    Route::post('/dashboard/account', ['as'=>'user.dashboard.account.save',  'uses' =>'Frontend\UserController@postAccountInformation']);
    Route::get('/dashboard/personal', ['as'=>'user.dashboard.personal',  'uses' =>'Frontend\UserController@getDashboardPersonal']);
    Route::post('/dashboard/personal', ['as'=>'user.dashboard.personal.save',  'uses' =>'Frontend\UserController@postDashboardPersonal']);

    // Registration OneSignal
    Route::post('pushregistration', ['as' => 'frontend.user.pushregistration', 'uses' => 'Frontend\UserController@postPushNotificationRegistration']);
});

// NEWSLETTER
Route::post('/newslettersubscribe', ['as' => 'frontend.newsletter.subscribe', 'uses' => 'Frontend\NewsletterController@updateSubscriber']);
