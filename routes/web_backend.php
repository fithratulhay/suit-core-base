<?php
/*
|--------------------------------------------------------------------------
| Backend Web Routes
|--------------------------------------------------------------------------
|
| Here is a route group for backend context, such as admin portal or other
| portal that accessed by user role from internal business context / process flow 
| (inside company)
| From these point of view, you can add another group if neccessary
|
*/

// Scope after Admin Logged in

// User Authentication & Session
Route::group(['middleware' => ['suitcore_guest']], function () {
    Route::get('login', ['as'=>'backend.sessions.login', 'uses' =>'Backend\SessionController@getLogin']);
    Route::post('postlogin', ['as'=>'backend.sessions.postlogin', 'uses' =>'Backend\SessionController@postLogin']);
});
Route::group(['middleware' => ['suitcore_roles']], function () {
    Route::get('logout', ['as'=>'backend.sessions.logout', 'uses' =>'Backend\SessionController@getLogout']);
});

// ----- HOME DASHBOARD -----
// Index
Route::group(['middleware' => ['suitcore_roles']], function () {
    Route::get('/', ['as' => 'backend.home.index', 'uses' => 'Backend\HomeController@getIndex']);
    Route::get('appuser-summary.json', ['as' => 'backend.home.appuser-summary', 'uses' => 'Backend\HomeController@getAppUserSummaryJson']);
    Route::get('notification', ['as' => 'backend.notification.index', 'uses' => 'Backend\NotificationController@getList']);
    Route::get('notification/{id}/read', ['as' => 'backend.notification.click', 'uses' => 'Backend\NotificationController@getClick']);
    Route::post('notification/action', ['as' => 'backend.notification.action', 'uses' => 'Backend\NotificationController@postAction']);
    Route::get('activity', ['as' => 'backend.activity.index', 'uses' => 'Backend\LogActivityController@getIndex']);
    Route::get('activity-appuser-summary.json', ['as' => 'backend.activity.appuser-summary', 'uses' => 'Backend\LogActivityController@getAppUserSummaryJson']);
});

// ----- SETTING -----
// Site Base Settings
Route::group(['prefix' => 'settings', 'middleware' => ['suitcore_roles']], function() {
    Route::get('view', ['as' => 'backend.settings.view', 'uses' => 'Backend\SettingsController@getList']);
    Route::post('save', ['as' => 'backend.settings.save', 'uses' => 'Backend\SettingsController@postSaveSettings']);
});


// ----- SITE USERS -----
// User Management
Route::group(['prefix' => 'user', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.user.index', 'uses' => 'Backend\UserController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.user.index.json', 'uses' => 'Backend\UserController@postIndexJson']);
    Route::get('optionsjson', ['as' => 'backend.user.options.json', 'uses' => 'Backend\UserController@getListJson']);
    Route::get('create', ['as' => 'backend.user.create', 'uses' => 'Backend\UserController@getCreate']);
    Route::post('create', ['as' => 'backend.user.store', 'uses' => 'Backend\UserController@postCreate']);
    Route::get('{id}', ['as' => 'backend.user.show', 'uses' => 'Backend\UserController@getView']);
    Route::get('{id}/update', ['as' => 'backend.user.edit', 'uses' => 'Backend\UserController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.user.update', 'uses' => 'Backend\UserController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.user.destroy', 'uses' => 'Backend\UserController@postDelete']);

    exportExcelRoute('backend.user', 'Backend\UserController');

    Route::get('updateprofile', ['as' => 'admin.user.updateprofile', 'uses' => 'Backend\UserController@updateProfile']);
    Route::post('updateprofile', ['as' => 'admin.user.updateprofile.save', 'uses' => 'Backend\UserController@postUpdateProfile']);

    importExcelRoute('backend.user', 'Backend\UserController');
});

// User Role Management
Route::group(['prefix' => 'role', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.role.index', 'uses' => 'Backend\RoleController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.role.index.json', 'uses' => 'Backend\RoleController@postIndexJson']);
    Route::get('create', ['as' => 'backend.role.create', 'uses' => 'Backend\RoleController@getCreate']);
    Route::post('create', ['as' => 'backend.role.store', 'uses' => 'Backend\RoleController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.role.edit', 'uses' => 'Backend\RoleController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.role.update', 'uses' => 'Backend\RoleController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.role.destroy', 'uses' => 'Backend\RoleController@postDelete']);
});

// Role Permission Management
Route::group(['prefix' => 'permission', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.permission.index', 'uses' => 'Backend\PermissionController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.permission.index.json', 'uses' => 'Backend\PermissionController@postIndexJson']);
    Route::get('acl', ['as' => 'backend.permission.acl', 'uses' => 'Backend\PermissionController@getAcl']);
    Route::post('acl', ['as' => 'backend.permission.acl.save', 'uses' => 'Backend\PermissionController@postUpdateAcl']);
    Route::get('copy-permission', ['as' => 'backend.permission.acl.getcopy', 'uses' => 'Backend\PermissionController@getCopy']);
    Route::post('copy-permission', ['as' => 'backend.permission.acl.postcopy', 'uses' => 'Backend\PermissionController@postCopy']);
    Route::get('create', ['as' => 'backend.permission.create', 'uses' => 'Backend\PermissionController@getCreate']);
    Route::post('create', ['as' => 'backend.permission.store', 'uses' => 'Backend\PermissionController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.permission.edit', 'uses' => 'Backend\PermissionController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.permission.update', 'uses' => 'Backend\PermissionController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.permission.destroy', 'uses' => 'Backend\PermissionController@postDelete']);
});

// User Log Management
Route::group(['prefix' => 'userlog', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.userlog.index', 'uses' => 'Backend\UserLogController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.userlog.index.json', 'uses' => 'Backend\UserLogController@postIndexJson']);
    Route::get('{id}', ['as' => 'backend.userlog.show', 'uses' => 'Backend\UserLogController@getView']);
    Route::get('exportxls', ['as' => 'backend.userlog.exportxls', 'uses' => 'Backend\UserLogController@exportToExcel']);
});

// User Review Management
Route::group(['prefix' => 'review', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.review.index', 'uses' => 'Backend\UserReviewController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.review.index.json', 'uses' => 'Backend\UserReviewController@postIndexJson']);
    // Route::get('create', ['as' => 'backend.review.create', 'uses' => 'Backend\UserReviewController@getCreate']);
    // Route::post('create', ['as' => 'backend.review.store', 'uses' => 'Backend\UserReviewController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.review.edit', 'uses' => 'Backend\UserReviewController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.review.update', 'uses' => 'Backend\UserReviewController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.review.destroy', 'uses' => 'Backend\UserReviewController@postDelete']);
});

// User Conversation Management
Route::group(['prefix' => 'conversation', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.conversation.index', 'uses' => 'Backend\ConversationController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.conversation.index.json', 'uses' => 'Backend\ConversationController@postIndexJson']);
    // Route::get('create', ['as' => 'backend.conversation.create', 'uses' => 'Backend\ConversationController@getCreate']);
    // Route::post('create', ['as' => 'backend.conversation.store', 'uses' => 'Backend\ConversationController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.conversation.edit', 'uses' => 'Backend\ConversationController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.conversation.update', 'uses' => 'Backend\ConversationController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.conversation.destroy', 'uses' => 'Backend\ConversationController@postDelete']);
});

// User Conversation Group Management
Route::group(['prefix' => 'conversation-group', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.conversation-group.index', 'uses' => 'Backend\ConversationGroupController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.conversation-group.index.json', 'uses' => 'Backend\ConversationGroupController@postIndexJson']);
    Route::get('create', ['as' => 'backend.conversation-group.create', 'uses' => 'Backend\ConversationGroupController@getCreate']);
    Route::post('create', ['as' => 'backend.conversation-group.store', 'uses' => 'Backend\ConversationGroupController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.conversation-group.edit', 'uses' => 'Backend\ConversationGroupController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.conversation-group.update', 'uses' => 'Backend\ConversationGroupController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.conversation-group.destroy', 'uses' => 'Backend\ConversationGroupController@postDelete']);
    Route::get('optionsjson', ['as' => 'backend.conversation-group.options.json', 'uses' => 'Backend\ConversationGroupController@getListJson']);
});

// User Conversation Member Management
Route::group(['prefix' => 'conversation-member', 'middleware' => ['suitcore_roles']], function () {
    Route::get('index', ['as' => 'backend.conversation-member.index', 'uses' => 'Backend\ConversationMemberController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.conversation-member.index.json', 'uses' => 'Backend\ConversationMemberController@postIndexJson']);
    Route::get('create', ['as' => 'backend.conversation-member.create', 'uses' => 'Backend\ConversationMemberController@getCreate']);
    Route::post('create', ['as' => 'backend.conversation-member.store', 'uses' => 'Backend\ConversationMemberController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.conversation-member.edit', 'uses' => 'Backend\ConversationMemberController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.conversation-member.update', 'uses' => 'Backend\ConversationMemberController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.conversation-member.destroy', 'uses' => 'Backend\ConversationMemberController@postDelete']);
});
// ----- TRANSACTION / BUSINESS PROCESS -----

// ----- MARKETING -----
// Newsletter Subscribers
Route::group(['prefix' => 'newslettersubscribers'], function() {
    Route::get('index', ['as' => 'backend.newslettersubscribers.index', 'uses' => 'Backend\NewsletterSubscriberController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.newslettersubscribers.index.json', 'uses' => 'Backend\NewsletterSubscriberController@postIndexJson']);
    Route::get('{id}/update', ['as' => 'backend.newslettersubscribers.edit', 'uses' => 'Backend\NewsletterSubscriberController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.newslettersubscribers.update', 'uses' => 'Backend\NewsletterSubscriberController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.newslettersubscribers.destroy', 'uses' => 'Backend\NewsletterSubscriberController@postDelete']);
    Route::get('exportxls', ['as' => 'backend.newslettersubscribers.exportxls', 'uses' => 'Backend\NewsletterSubscriberController@exportToExcel']);
});

// Newsletter 
Route::group(['prefix' => 'newsletter'], function() {
    Route::get('index', ['as' => 'backend.newsletter.index', 'uses' => 'Backend\NewsletterController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.newsletter.index.json', 'uses' => 'Backend\NewsletterController@postIndexJson']);
    Route::get('create', ['as' => 'backend.newsletter.create', 'uses' => 'Backend\NewsletterController@getCreate']);
    Route::post('create', ['as' => 'backend.newsletter.store', 'uses' => 'Backend\NewsletterController@postCreate']);
    Route::get('{id}', ['as' => 'backend.newsletter.show', 'uses' => 'Backend\NewsletterController@getView']);
    Route::get('{id}/preview', ['as' => 'backend.newsletter.preview', 'uses' => 'Backend\NewsletterController@getPreview']);
    Route::get('{id}/update', ['as' => 'backend.newsletter.edit', 'uses' => 'Backend\NewsletterController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.newsletter.update', 'uses' => 'Backend\NewsletterController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.newsletter.destroy', 'uses' => 'Backend\NewsletterController@postDelete']);
});

// ----- MASTERDATA -----

// ----- WEB CONTENT -----
// Banner Management
Route::group(['prefix' => 'banner', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.banner.index', 'uses' => 'Backend\BannerController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.banner.index.json', 'uses' => 'Backend\BannerController@postIndexJson']);
    Route::get('create', ['as' => 'backend.banner.create', 'uses' => 'Backend\BannerController@getCreate']);
    Route::post('create', ['as' => 'backend.banner.store', 'uses' => 'Backend\BannerController@postCreate']);
    Route::get('{id}', ['as' => 'backend.banner.show', 'uses' => 'Backend\BannerController@getView']);
    Route::get('{id}/update', ['as' => 'backend.banner.edit', 'uses' => 'Backend\BannerController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.banner.update', 'uses' => 'Backend\BannerController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.banner.destroy', 'uses' => 'Backend\BannerController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.banner.select', 'uses' => 'Backend\BannerController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.banner.destroyall', 'uses' => 'Backend\BannerController@postDeleteAll']);
});

// Article Management
Route::group(['prefix' => 'article', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.article.index', 'uses' => 'Backend\ArticleController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.article.index.json', 'uses' => 'Backend\ArticleController@postIndexJson']);
    Route::get('create', ['as' => 'backend.article.create', 'uses' => 'Backend\ArticleController@getCreate']);
    Route::post('create', ['as' => 'backend.article.store', 'uses' => 'Backend\ArticleController@postCreate']);
    Route::get('{id}', ['as' => 'backend.article.show', 'uses' => 'Backend\ArticleController@getView']);
    Route::get('{id}/update', ['as' => 'backend.article.edit', 'uses' => 'Backend\ArticleController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.article.update', 'uses' => 'Backend\ArticleController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.article.destroy', 'uses' => 'Backend\ArticleController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.article.select', 'uses' => 'Backend\ArticleController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.article.destroyall', 'uses' => 'Backend\ArticleController@postDeleteAll']);
});

// Content Type Management
Route::group(['prefix' => 'contenttype', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.contenttype.index', 'uses' => 'Backend\ContentTypeController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.contenttype.index.json', 'uses' => 'Backend\ContentTypeController@postIndexJson']);
    Route::get('create', ['as' => 'backend.contenttype.create', 'uses' => 'Backend\ContentTypeController@getCreate']);
    Route::post('create', ['as' => 'backend.contenttype.store', 'uses' => 'Backend\ContentTypeController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.contenttype.edit', 'uses' => 'Backend\ContentTypeController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.contenttype.update', 'uses' => 'Backend\ContentTypeController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.contenttype.destroy', 'uses' => 'Backend\ContentTypeController@postDelete']);
});

// Content Category Management
Route::group(['prefix' => 'contentcategory', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.contentcategory.index', 'uses' => 'Backend\ContentCategoryController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.contentcategory.index.json', 'uses' => 'Backend\ContentCategoryController@postIndexJson']);
    Route::get('create', ['as' => 'backend.contentcategory.create', 'uses' => 'Backend\ContentCategoryController@getCreate']);
    Route::post('create', ['as' => 'backend.contentcategory.store', 'uses' => 'Backend\ContentCategoryController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.contentcategory.edit', 'uses' => 'Backend\ContentCategoryController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.contentcategory.update', 'uses' => 'Backend\ContentCategoryController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.contentcategory.destroy', 'uses' => 'Backend\ContentCategoryController@postDelete']);
});

// Content Management
Route::group(['prefix' => 'content', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.content.index', 'uses' => 'Backend\ContentController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.content.index.json', 'uses' => 'Backend\ContentController@postIndexJson']);
    Route::get('create', ['as' => 'backend.content.create', 'uses' => 'Backend\ContentController@getCreate']);
    Route::post('create', ['as' => 'backend.content.store', 'uses' => 'Backend\ContentController@postCreate']);
    Route::get('{id}', ['as' => 'backend.content.show', 'uses' => 'Backend\ContentController@getView']);
    Route::get('{id}/update', ['as' => 'backend.content.edit', 'uses' => 'Backend\ContentController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.content.update', 'uses' => 'Backend\ContentController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.content.destroy', 'uses' => 'Backend\ContentController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.content.select', 'uses' => 'Backend\ContentController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.content.destroyall', 'uses' => 'Backend\ContentController@postDeleteAll']);
});

// Page Management
Route::group(['prefix' => 'page', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.page.index', 'uses' => 'Backend\PageController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.page.index.json', 'uses' => 'Backend\PageController@postIndexJson']);
    Route::get('create', ['as' => 'backend.page.create', 'uses' => 'Backend\PageController@getCreate']);
    Route::post('create', ['as' => 'backend.page.store', 'uses' => 'Backend\PageController@postCreate']);
    Route::get('{id}', ['as' => 'backend.page.show', 'uses' => 'Backend\PageController@getView']);
    Route::get('{id}/update', ['as' => 'backend.page.edit', 'uses' => 'Backend\PageController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.page.update', 'uses' => 'Backend\PageController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.page.destroy', 'uses' => 'Backend\PageController@postDelete']);
});

// Menu Management
Route::group(['prefix' => 'menu', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.menu.index', 'uses' => 'Backend\MenuController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.menu.index.json', 'uses' => 'Backend\MenuController@postIndexJson']);
    Route::get('optionsjson', ['as' => 'backend.menu.options.json', 'uses' => 'Backend\MenuController@getListJson']);
    Route::get('create', ['as' => 'backend.menu.create', 'uses' => 'Backend\MenuController@getCreate']);
    Route::post('create', ['as' => 'backend.menu.store', 'uses' => 'Backend\MenuController@postCreate']);
    Route::get('{id}', ['as' => 'backend.menu.show', 'uses' => 'Backend\MenuController@getView']);
    Route::get('{id}/update', ['as' => 'backend.menu.edit', 'uses' => 'Backend\MenuController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.menu.update', 'uses' => 'Backend\MenuController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.menu.destroy', 'uses' => 'Backend\MenuController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.menu.select', 'uses' => 'Backend\MenuController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.menu.destroyall', 'uses' => 'Backend\MenuController@postDeleteAll']);
});

// Faq Category Management
Route::group(['prefix' => 'faqcategory', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.faqcategory.index', 'uses' => 'Backend\FaqCategoryController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.faqcategory.index.json', 'uses' => 'Backend\FaqCategoryController@postIndexJson']);
    Route::get('optionsjson', ['as' => 'backend.faqcategory.options.json', 'uses' => 'Backend\FaqCategoryController@getListJson']);
    Route::get('create', ['as' => 'backend.faqcategory.create', 'uses' => 'Backend\FaqCategoryController@getCreate']);
    Route::post('create', ['as' => 'backend.faqcategory.store', 'uses' => 'Backend\FaqCategoryController@postCreate']);
    Route::get('{id}/update', ['as' => 'backend.faqcategory.edit', 'uses' => 'Backend\FaqCategoryController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.faqcategory.update', 'uses' => 'Backend\FaqCategoryController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.faqcategory.destroy', 'uses' => 'Backend\FaqCategoryController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.faqcategory.select', 'uses' => 'Backend\FaqCategoryController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.faqcategory.destroyall', 'uses' => 'Backend\FaqCategoryController@postDeleteAll']);
});

// Faq Management
Route::group(['prefix' => 'faq', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.faq.index', 'uses' => 'Backend\FaqController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.faq.index.json', 'uses' => 'Backend\FaqController@postIndexJson']);
    Route::get('create', ['as' => 'backend.faq.create', 'uses' => 'Backend\FaqController@getCreate']);
    Route::post('create', ['as' => 'backend.faq.store', 'uses' => 'Backend\FaqController@postCreate']);
    Route::get('{id}', ['as' => 'backend.faq.show', 'uses' => 'Backend\FaqController@getView']);
    Route::get('{id}/update', ['as' => 'backend.faq.edit', 'uses' => 'Backend\FaqController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.faq.update', 'uses' => 'Backend\FaqController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.faq.destroy', 'uses' => 'Backend\FaqController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.faq.select', 'uses' => 'Backend\FaqController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.faq.destroyall', 'uses' => 'Backend\FaqController@postDeleteAll']);
});

// Contact Message Management
Route::group(['prefix' => 'contactmessage', 'middleware' => ['suitcore_roles']], function () {
    Route::get('/', ['as' => 'backend.contactmessage', function() {
        return Redirect::route('backend.contactmessage.index');
    }]);
    Route::get('index', ['as' => 'backend.contactmessage.index', 'uses' => 'Backend\ContactMessageController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.contactmessage.index.json', 'uses' => 'Backend\ContactMessageController@postIndexJson']);
    Route::get('{id}', ['as' => 'backend.contactmessage.show', 'uses' => 'Backend\ContactMessageController@getView']);
    Route::post('{id}/destroy', ['as' => 'backend.contactmessage.destroy', 'uses' => 'Backend\ContactMessageController@postDelete']);
    // Extended from standard action
    Route::post('{id}', ['as' => 'backend.contactmessage.show.reply', 'uses' => 'Backend\ContactMessageController@postReply']);
});

// URL SEO Management
Route::group(['prefix' => 'urlseo', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.urlseo.index', 'uses' => 'Backend\UrlSeoController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.urlseo.index.json', 'uses' => 'Backend\UrlSeoController@postIndexJson']);
    Route::get('create', ['as' => 'backend.urlseo.create', 'uses' => 'Backend\UrlSeoController@getCreate']);
    Route::post('create', ['as' => 'backend.urlseo.store', 'uses' => 'Backend\UrlSeoController@postCreate']);
    Route::get('{id}', ['as' => 'backend.urlseo.show', 'uses' => 'Backend\UrlSeoController@getView']);
    Route::get('{id}/update', ['as' => 'backend.urlseo.edit', 'uses' => 'Backend\UrlSeoController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.urlseo.update', 'uses' => 'Backend\UrlSeoController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.urlseo.destroy', 'uses' => 'Backend\UrlSeoController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.urlseo.select', 'uses' => 'Backend\UrlSeoController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.urlseo.destroyall', 'uses' => 'Backend\UrlSeoController@postDeleteAll']);
});

// URL Redirection Management
Route::group(['prefix' => 'urlredirection', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.urlredirection.index', 'uses' => 'Backend\UrlRedirectionController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.urlredirection.index.json', 'uses' => 'Backend\UrlRedirectionController@postIndexJson']);
    Route::get('create', ['as' => 'backend.urlredirection.create', 'uses' => 'Backend\UrlRedirectionController@getCreate']);
    Route::post('create', ['as' => 'backend.urlredirection.store', 'uses' => 'Backend\UrlRedirectionController@postCreate']);
    Route::get('{id}', ['as' => 'backend.urlredirection.show', 'uses' => 'Backend\UrlRedirectionController@getView']);
    Route::get('{id}/update', ['as' => 'backend.urlredirection.edit', 'uses' => 'Backend\UrlRedirectionController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.urlredirection.update', 'uses' => 'Backend\UrlRedirectionController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.urlredirection.destroy', 'uses' => 'Backend\UrlRedirectionController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.urlredirection.select', 'uses' => 'Backend\UrlRedirectionController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.urlredirection.destroyall', 'uses' => 'Backend\UrlRedirectionController@postDeleteAll']);
});

// Company Gallery Management
Route::group(['prefix' => 'gallery', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.gallery.index', 'uses' => 'Backend\GalleryController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.gallery.index.json', 'uses' => 'Backend\GalleryController@postIndexJson']);
    Route::get('create', ['as' => 'backend.gallery.create', 'uses' => 'Backend\GalleryController@getCreate']);
    Route::post('create', ['as' => 'backend.gallery.store', 'uses' => 'Backend\GalleryController@postCreate']);
    Route::get('{id}', ['as' => 'backend.gallery.show', 'uses' => 'Backend\GalleryController@getView']);
    Route::get('{id}/update', ['as' => 'backend.gallery.edit', 'uses' => 'Backend\GalleryController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.gallery.update', 'uses' => 'Backend\GalleryController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.gallery.destroy', 'uses' => 'Backend\GalleryController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.gallery.select', 'uses' => 'Backend\GalleryController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.gallery.destroyall', 'uses' => 'Backend\GalleryController@postDeleteAll']);
});

// Comment Management
Route::group(['prefix' => 'comment', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.comment.index', 'uses' => 'Backend\CommentController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.comment.index.json', 'uses' => 'Backend\CommentController@postIndexJson']);
    //Route::get('create', ['as' => 'backend.comment.create', 'uses' => 'Backend\CommentController@getCreate']);
    //Route::post('create', ['as' => 'backend.comment.store', 'uses' => 'Backend\CommentController@postCreate']);
    Route::get('{id}', ['as' => 'backend.comment.show', 'uses' => 'Backend\CommentController@getView']);
    Route::get('{id}/update', ['as' => 'backend.comment.edit', 'uses' => 'Backend\CommentController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.comment.update', 'uses' => 'Backend\CommentController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.comment.destroy', 'uses' => 'Backend\CommentController@postDelete']);

    //Route::post('{id}/select', ['as' => 'backend.comment.select', 'uses' => 'Backend\CommentController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.comment.destroyall', 'uses' => 'Backend\CommentController@postDeleteAll']);
    Route::post('{id}', ['as' => 'backend.contactmessage.show.reply', 'uses' => 'Backend\CommentController@postReply']);
});

// File Category Management
Route::group(['prefix' => 'file-category', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.file-category.index', 'uses' => 'Backend\FileCategoryController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.file-category.index.json', 'uses' => 'Backend\FileCategoryController@postIndexJson']);
    Route::get('create', ['as' => 'backend.file-category.create', 'uses' => 'Backend\FileCategoryController@getCreate']);
    Route::post('create', ['as' => 'backend.file-category.store', 'uses' => 'Backend\FileCategoryController@postCreate']);
    Route::get('{id}', ['as' => 'backend.file-category.show', 'uses' => 'Backend\FileCategoryController@getView']);
    Route::get('{id}/update', ['as' => 'backend.file-category.edit', 'uses' => 'Backend\FileCategoryController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.file-category.update', 'uses' => 'Backend\FileCategoryController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.file-category.destroy', 'uses' => 'Backend\FileCategoryController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.file-category.select', 'uses' => 'Backend\FileCategoryController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.file-category.destroyall', 'uses' => 'Backend\FileCategoryController@postDeleteAll']);
});

// File Category Management
Route::group(['prefix' => 'file', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.file.index', 'uses' => 'Backend\FileController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.file.index.json', 'uses' => 'Backend\FileController@postIndexJson']);
    Route::get('create', ['as' => 'backend.file.create', 'uses' => 'Backend\FileController@getCreate']);
    Route::post('create', ['as' => 'backend.file.store', 'uses' => 'Backend\FileController@postCreate']);
    Route::get('{id}', ['as' => 'backend.file.show', 'uses' => 'Backend\FileController@getView']);
    Route::get('{id}/update', ['as' => 'backend.file.edit', 'uses' => 'Backend\FileController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.file.update', 'uses' => 'Backend\FileController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.file.destroy', 'uses' => 'Backend\FileController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.file.select', 'uses' => 'Backend\FileController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.file.destroyall', 'uses' => 'Backend\FileController@postDeleteAll']);
});

// Poll Category Management
Route::group(['prefix' => 'poll-category', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.poll-category.index', 'uses' => 'Backend\PollCategoryController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.poll-category.index.json', 'uses' => 'Backend\PollCategoryController@postIndexJson']);
    Route::get('create', ['as' => 'backend.poll-category.create', 'uses' => 'Backend\PollCategoryController@getCreate']);
    Route::post('create', ['as' => 'backend.poll-category.store', 'uses' => 'Backend\PollCategoryController@postCreate']);
    Route::get('{id}', ['as' => 'backend.poll-category.show', 'uses' => 'Backend\PollCategoryController@getView']);
    Route::get('{id}/update', ['as' => 'backend.poll-category.edit', 'uses' => 'Backend\PollCategoryController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.poll-category.update', 'uses' => 'Backend\PollCategoryController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.poll-category.destroy', 'uses' => 'Backend\PollCategoryController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.poll-category.select', 'uses' => 'Backend\PollCategoryController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.poll-category.destroyall', 'uses' => 'Backend\PollCategoryController@postDeleteAll']);

    Route::get('optionsjson', ['as' => 'backend.poll-category.options.json', 'uses' => 'Backend\PollCategoryController@getListJson']);
});


// Poll Question Management
Route::group(['prefix' => 'poll', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.poll.index', 'uses' => 'Backend\PollController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.poll.index.json', 'uses' => 'Backend\PollController@postIndexJson']);
    Route::get('create', ['as' => 'backend.poll.create', 'uses' => 'Backend\PollController@getCreate']);
    Route::post('create', ['as' => 'backend.poll.store', 'uses' => 'Backend\PollController@postCreate']);
    Route::get('{id}', ['as' => 'backend.poll.show', 'uses' => 'Backend\PollController@getView']);
    Route::get('{id}/update', ['as' => 'backend.poll.edit', 'uses' => 'Backend\PollController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.poll.update', 'uses' => 'Backend\PollController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.poll.destroy', 'uses' => 'Backend\PollController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.poll.select', 'uses' => 'Backend\PollController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.poll.destroyall', 'uses' => 'Backend\PollController@postDeleteAll']);

    Route::get('optionsjson', ['as' => 'backend.poll.options.json', 'uses' => 'Backend\PollController@getListJson']);
});

// Poll Answer Option Management
Route::group(['prefix' => 'poll-option', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.poll-option.index', 'uses' => 'Backend\PollOptionController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.poll-option.index.json', 'uses' => 'Backend\PollOptionController@postIndexJson']);
    Route::get('create', ['as' => 'backend.poll-option.create', 'uses' => 'Backend\PollOptionController@getCreate']);
    Route::post('create', ['as' => 'backend.poll-option.store', 'uses' => 'Backend\PollOptionController@postCreate']);
    Route::get('{id}', ['as' => 'backend.poll-option.show', 'uses' => 'Backend\PollOptionController@getView']);
    Route::get('{id}/update', ['as' => 'backend.poll-option.edit', 'uses' => 'Backend\PollOptionController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.poll-option.update', 'uses' => 'Backend\PollOptionController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.poll-option.destroy', 'uses' => 'Backend\PollOptionController@postDelete']);

    Route::post('{id}/select', ['as' => 'backend.poll-option.select', 'uses' => 'Backend\PollOptionController@postSelect']);
    Route::post('destroyall', ['as' => 'backend.poll-option.destroyall', 'uses' => 'Backend\PollOptionController@postDeleteAll']);
    Route::get('optionsjson', ['as' => 'backend.poll-option.options.json', 'uses' => 'Backend\PollOptionController@getListJson']);
});

// Poll Answer Management
Route::group(['prefix' => 'poll-answer', 'middleware' => ['suitcore_roles']], function() {
    Route::get('index', ['as' => 'backend.poll-answer.index', 'uses' => 'Backend\PollAnswerController@getIndex']);
    Route::post('indexjson', ['as' => 'backend.poll-answer.index.json', 'uses' => 'Backend\PollAnswerController@postIndexJson']);
    Route::get('create', ['as' => 'backend.poll-answer.create', 'uses' => 'Backend\PollAnswerController@getCreate']);
    Route::post('create', ['as' => 'backend.poll-answer.store', 'uses' => 'Backend\PollAnswerController@postCreate']);
    Route::get('{id}', ['as' => 'backend.poll-answer.show', 'uses' => 'Backend\PollAnswerController@getView']);
    Route::get('{id}/update', ['as' => 'backend.poll-answer.edit', 'uses' => 'Backend\PollAnswerController@getUpdate']);
    Route::post('{id}/update', ['as' => 'backend.poll-answer.update', 'uses' => 'Backend\PollAnswerController@postUpdate']);
    Route::post('{id}/destroy', ['as' => 'backend.poll-answer.destroy', 'uses' => 'Backend\PollAnswerController@postDelete']);

    // Route::post('{id}/select', ['as' => 'backend.poll-answer.select', 'uses' => 'Backend\PollAnswerController@postSelect']);
    // Route::post('destroyall', ['as' => 'backend.poll-answer.destroyall', 'uses' => 'Backend\PollAnswerController@postDeleteAll']);
});