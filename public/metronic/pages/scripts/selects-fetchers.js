var SelectsFetchers = function() {

    var handleSelect2ToMultiselect = function() {
        var selectors = $('.select2');

        if (!selectors.length) return;

        var elmts = $('[data-child]');

        if (!elmts.length) return;

        $.each(elmts, function() {
            var elmt = $(this);
            var child = elmt.data('child') || false;
            var url = elmt.data('url') || false;

            if(!child) return;

            if(!url) return;

            if (child.indexOf('#')) {
                elmtChild = $('#' + child);
            } else {
                elmtChild = $(child);
            }

            elmt.on('change', function(e) {
                e.preventDefault();
                SelectsFetchers.initChildJsonMultiselect($(this), elmtChild, url);
            });

            SelectsFetchers.initChildJsonMultiselect(elmt, elmtChild, url);
        });
    }

    var handleSelect2ToMultiselects = function() {
        var selectors = $('.select2');

        if (!selectors.length) return;

        var elmts = $('[data-childs]');

        if (!elmts.length) return;

        $.each(elmts, function() {
            var elmt = $(this);
            var child = elmt.data('childs') || false;
            var url = elmt.data('url') || false;

            if(!child) return;

            if(!url) return;

            var childs = child.split(',');
            var elmtChilds = [];

            if (childs.length > 0) {
                for (i = 0; i < childs.length; i++) {
                    if (childs.indexOf('#')) {
                        elmtChilds.push($('#' + childs[i]));
                    } else {
                        elmtChilds.push($(childs[i]));
                    }
                }
            }

            elmt.on('change', function(e) {
                e.preventDefault();
                SelectsFetchers.initChildsJsonMultiselects($(this), elmtChilds, url);
            });

            SelectsFetchers.initChildsJsonMultiselects(elmt, elmtChilds, url);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleSelect2ToMultiselect();
            handleSelect2ToMultiselects();
        },

        initChildJsonMultiselect: function(multiselector, multiselect, url) {
            var ref = multiselect.attr('id');
            var blokUI = '#ms-' + ref;
            var parents = [];
            multiselector.children('option:selected').each( function() {
                parents.push($(this).val());
            });
            var result = false;
            $.ajax({
                type: "GET",
                async: true,
                url: url + '?nocache=' + Math.random(),
                data: { id: parents },
                cache: false,
                beforeSend: function() {
                    App.blockUI({
                        target: blokUI,
                        animate: true
                    });
                },
                complete: function () {
                    App.unblockUI(blokUI);
                },
                success: function (data) {
                    var result = data;
                    var groups = [];
                    var selecteds = [];
                    $.each(result, function(index, element) {
                        var elmtValue = $(multiselect).val() || false;
                        var dataValue = $(multiselect).data('value') || [];
                        var selectedElmt = elmtValue || dataValue;
                        if (selectedElmt.length > 0) {
                            $.each(selectedElmt, function(indexSelected) {
                                selecteds.push(selectedElmt[indexSelected]);
                            });
                        }
                        $(multiselect).empty();

                        var objects = element['data'];
                        if (objects.length > 0) {
                            var group = $('<optgroup />');
                            group.attr('id', 'group-' + $(multiselect).attr('id') + '-' + element.id);
                            group.attr('label', element.name);
                            $.each(objects, function(indexObject, object) {
                                if (selecteds.length > 0
                                    && (selecteds.indexOf(object.id.toString()) > -1
                                        || selecteds.indexOf(object.id) > -1)) {
                                    var option = $("<option selected='selected'></option>");
                                    option.val(object.id);
                                    option.text(object.name);

                                    group.append(option);
                                } else {
                                    var option = $("<option></option>");
                                    option.val(object.id);
                                    option.text(object.name);

                                    group.append(option);
                                }
                            });
                            var objGroup = {id: $(multiselect).attr('id'), group: group};
                            groups.push(objGroup);
                        }
                    });
                    $.each(groups, function( index ) {
                        $(multiselect).append(groups[index]['group']);
                    });
                    $(multiselect).multiSelect('refresh');
                },
            });

            return result;
        },

        initChildsJsonMultiselects: function(multiselector, multiselects, url) {
            var parents = [];
            multiselector.children('option:selected').each( function() {
                parents.push($(this).val());
            });
            var result = false;
            $.ajax({
                type: "GET",
                async: true,
                url: url + '?nocache=' + Math.random(),
                data: { id: parents },
                cache: false,
                beforeSend: function() {
                    $.each(multiselects, function( index, value ) {
                        var blokUI = '#ms-' + value.attr('id');
                        App.blockUI({
                            target: blokUI,
                            animate: true
                        });
                    });
                },
                complete: function () {
                    $.each(multiselects, function( index, value ) {
                        var blokUI = '#ms-' + value.attr('id');
                        App.unblockUI(blokUI);
                    });
                },
                success: function (data) {
                    result = data;
                    var groups = [];
                    var selecteds = [];
                    $.each(data, function(index, element) {
                        $.each(multiselects, function( indexMultiselect, multiselect ) {
                            var elmtValue = $(multiselect).val() || false;
                            var dataValue = $(multiselect).data('value') || [];
                            var selectedElmt = elmtValue || dataValue;
                            if (selectedElmt.length > 0) {
                                $.each(selectedElmt, function(indexSelected) {
                                    selecteds.push({id: $(multiselect).attr('id'), selected: selectedElmt[indexSelected]});
                                });
                            }
                            $(multiselect).empty();

                            var dataObjects = $(multiselect).data('objects');
                            if (!dataObjects) return;

                            var objects = element[dataObjects];
                            if (objects.length > 0) {
                                var group = $('<optgroup />');
                                group.attr('id', 'group-' + $(multiselect).attr('id') + '-' + element.id);
                                group.attr('label', element.name);
                                $.each(objects, function(indexObject, object) {
                                    if (selecteds.length > 0) {
                                        var filteredSelecteds = selecteds.filter(function (element) {
                                            return element.id === $(multiselect).attr('id');
                                        });
                                        var elementPosInt = filteredSelecteds.map(function(s) {return s.selected; }).indexOf(object.id);
                                        var elementPosString = filteredSelecteds.map(function(s) {return s.selected; }).indexOf(object.id.toString());
                                        if (elementPosInt > -1 || elementPosString > -1) {
                                            var option = $("<option selected='selected'></option>");
                                            option.val(object.id);
                                            option.text(object.name);

                                            group.append(option);
                                        } else {
                                            var option = $("<option></option>");
                                            option.val(object.id);
                                            option.text(object.name);

                                            group.append(option);
                                        }
                                    } else {
                                        var option = $("<option></option>");
                                        option.val(object.id);
                                        option.text(object.name);

                                        group.append(option);
                                    }
                                });
                                var objGroup = {id: $(multiselect).attr('id'), group: group};
                                groups.push(objGroup);
                            }
                        });
                    });

                    $.each(multiselects, function( indexMultiselect, multiselect ) {
                        $.each(groups, function( index ) {
                            if (groups[index]['id'] == $(multiselect).attr('id')) {
                                $(multiselect).append(groups[index]['group']);
                            }
                        });
                        $(multiselect).multiSelect('refresh');
                    });
                },
            });

            return result;
        }
    };
}();

jQuery(document).ready(function() {
    SelectsFetchers.init();
});
