var ContentJobs = function() {

    var handleJobs = function() {
        var content = $('.list-myjob');

        content.on('click', '.pagination a', function(e) {
            e.preventDefault();

            var url = $(this).attr('href');

            handlePage(content, url);

            window.history.pushState("", "", url);
        });
    }

    var handlePage = function(content, url) {
        $.ajax({
            url : url
        }).done(function (data) {
            content.html(data);
        }).fail(function () {
            alert('Page could not be loaded.');
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleJobs();
        }
    };
}();

jQuery(document).ready(function() {
    ContentJobs.init();
});
