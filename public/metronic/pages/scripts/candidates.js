var ContentCandidates = function() {

    var handleCandidates = function() {
        var content = $('.list-candidate');

        content.on('click', '.pagination a', function(e) {
            e.preventDefault();

            var url = $(this).attr('href');

            handlePage(content, url);

            window.history.pushState("", "", url);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleCandidates();
        },

        handlePage: function(content, url) {
            $.ajax({
                url : url
            }).done(function (data) {
                content.html(data);
            }).fail(function () {
                alert('Page could not be loaded.');
            });
        }
    };
}();

jQuery(document).ready(function() {
    ContentCandidates.init();
});
