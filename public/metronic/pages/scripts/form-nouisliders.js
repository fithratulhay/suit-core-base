var FormNoUISliders = function() {

    var rangeAgeSlider = function() {
        var rangeSlider = $('.rangeSlider');

        if(!rangeSlider.length) return;

        var elmt = document.getElementById('age-range-slider');

        var step = $(elmt).data('step') || 1.0;
        var start = $(elmt).data('start') || [20, 30];
        var minFor = $(elmt).data('for-min');

        if (!minFor) return;

        var maxFor = $(elmt).data('for-max');
        if (!maxFor) return;

        var minInput = $('#' + minFor);
        var maxInput = $('#' + maxFor);

        var min = minInput.data('min') || 1;
        var max = maxInput.data('max') || 100;

        noUiSlider.create(elmt, {
            start: start,
            connect: true,
            step: step,
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 0
            })
        });

        elmt.noUiSlider.on('update', function( values, handle ) {
            var value = values[handle];

            if ( handle ) {
                maxInput.val(value);
            } else {
                minInput.val(value);
            }
        });

        minInput.on('change', function(){
            elmt.noUiSlider.set([this.value, null]);
        });

        maxInput.on('change', function(){
            elmt.noUiSlider.set([null, this.value]);
        });
    }

    var rangeCurrencySlider = function() {
        var rangeSlider = $('.rangeSlider');

        if(!rangeSlider.length) return;

        var elmt = document.getElementById('currency-range-slider');

        var step = $(elmt).data('step') || 100.0;
        var start = $(elmt).data('start') || [1000, 1000000];
        var minFor = $(elmt).data('for-min');

        if (!minFor) return;

        var maxFor = $(elmt).data('for-max');
        if (!maxFor) return;

        var minInput = $('#' + minFor);
        var maxInput = $('#' + maxFor);

        var min = minInput.data('min') || 100;
        var max = maxInput.data('max') || 10000000;

        noUiSlider.create(elmt, {
            start: start,
            connect: true,
            step: step,
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                decimals: 2,
                thousand: ','
            })
        });

        elmt.noUiSlider.on('update', function( values, handle ) {
            var value = values[handle];

            if ( handle ) {
                maxInput.val(value);
            } else {
                minInput.val(value);
            }
        });

        minInput.on('change', function(){
            elmt.noUiSlider.set([this.value, null]);
        });

        maxInput.on('change', function(){
            elmt.noUiSlider.set([null, this.value]);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            rangeAgeSlider();
            rangeCurrencySlider();
        }
    };
}();

jQuery(document).ready(function() {
    FormNoUISliders.init();
});
