var DropdownSelects = function () {

    var handleMultiSelect = function () {
        var elmts = $('[data-multi-select]');

        if (!elmts.length) return;

        $.each(elmts, function() {
            var elmt    = $(this);

            if (!elmt.is('[data-select-group]')) {
                $(elmt).multiSelect();
            } else {
                $(elmt).multiSelect({
                    selectableOptgroup: true
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
        }
    };

}();

jQuery(document).ready(function() {
   DropdownSelects.init();
});
