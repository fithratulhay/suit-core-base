var VacancyNoUiSliders = function() {

    var salaries = function() {
        var rangeSlider = document.getElementById('salaries');

        var dataStart   = $('#salaries').data('start');

        if(!dataStart) return;

        var dataPostfix   = $('#salaries').data('postfix');

        if(!dataPostfix) dataPostfix = '';

        noUiSlider.create(rangeSlider, {
            start: dataStart,
            connect: true,
            step: 100000.0,
            range: {
                'min': 10000,
                'max': 10000000
            }
        });

        var minInput = $('#salaries').data('salary-min-input');
        if(!minInput) return;
        var maxInput = $('#salaries').data('salary-max-input');
        if(!maxInput) return;

        var minInputNumber = document.getElementById(minInput);
        var maxInputNumber = document.getElementById(maxInput);

        rangeSlider.noUiSlider.on('update', function( values, handle ) {

            var value = values[handle];

            if ( handle ) {
                maxInputNumber.value = value;
            } else {
                minInputNumber.value = value;
            }
        });

        minInputNumber.addEventListener('change', function(){
            rangeSlider.noUiSlider.set([this.value, null]);
        });

        maxInputNumber.addEventListener('change', function(){
            rangeSlider.noUiSlider.set([null, this.value]);
        });
    }

    var ages = function() {
        var rangeSlider = document.getElementById('ages');

        var dataStart   = $('#ages').data('start');

        if(!dataStart) return;

        var dataPostfix   = $('#ages').data('postfix');

        if(!dataPostfix) dataPostfix = '';

        noUiSlider.create(rangeSlider, {
            start: dataStart,
            connect: true,
            step: 1.0,
            range: {
                'min': 1,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });

        var minInput = $('#ages').data('age-min-input');
        if(!minInput) return;
        var maxInput = $('#ages').data('age-max-input');
        if(!maxInput) return;

        var minInputNumber = document.getElementById(minInput);
        var maxInputNumber = document.getElementById(maxInput);

        rangeSlider.noUiSlider.on('update', function( values, handle ) {

            var value = values[handle];

            if ( handle ) {
                maxInputNumber.value = value;
            } else {
                minInputNumber.value = value;
            }
        });

        minInputNumber.addEventListener('change', function(){
            rangeSlider.noUiSlider.set([this.value, null]);
        });

        maxInputNumber.addEventListener('change', function(){
            rangeSlider.noUiSlider.set([null, this.value]);
        });
    }

    var gpa4 = function() {
        var softSlider = document.getElementById('gpa4');

        var dataStart   = $('#gpa4').data('start');

        if(!dataStart) return;

        noUiSlider.create(softSlider, {
            start: dataStart,
            range: {
                min: 1.00,
                max: 4.00
            },
            format: wNumb({
                decimals: 2
            }),
            format: wNumb({
                decimals: 2
            }),
            pips: {
                mode: 'values',
                values: [1.00, 2.00, 3.00, 4.00],
                density: 3.50
            }
        });

        var input = $('#gpa4').data('gpa4-input');

        if(!input) return;

        var inputNumber = document.getElementById(input);

        softSlider.noUiSlider.on('update', function(values, handle) {
            var value = values[handle];
            inputNumber.value = value;
        });

        inputNumber.addEventListener('change', function() {
            softSlider.noUiSlider.set([this.value, null]);
        });
    }

    var gpa5 = function() {
        var softSlider = document.getElementById('gpa5');

        var dataStart   = $('#gpa5').data('start');

        if(!dataStart) return;

        noUiSlider.create(softSlider, {
            start: dataStart,
            range: {
                min: 1.00,
                max: 5.00
            },
            format: wNumb({
                decimals: 2
            }),
            pips: {
                mode: 'values',
                values: [1.00, 2.00, 3.00, 4.00, 5.00],
                density: 2.50
            }
        });

        var input = $('#gpa5').data('gpa5-input');

        if(!input) return;

        var inputNumber = document.getElementById(input);

        softSlider.noUiSlider.on('update', function(values, handle) {
            var value = values[handle];
            inputNumber.value = value;
        });

        inputNumber.addEventListener('change', function() {
            softSlider.noUiSlider.set([this.value, null]);
        });
    }

    var experiences = function() {
        var rangeSlider = document.getElementById('experiences');

        var dataStart   = $('#experiences').data('start');

        if(!dataStart) return;

        var dataPostfix   = $('#experiences').data('postfix');

        if(!dataPostfix) dataPostfix = '';

        noUiSlider.create(rangeSlider, {
            start: dataStart,
            connect: true,
            step: 1.0,
            range: {
                'min': 1,
                'max': 10
            },
            format: wNumb({
                decimals: 0
            })
        });

        var minInput = $('#experiences').data('experience-min-input');
        if(!minInput) return;
        var maxInput = $('#experiences').data('experience-max-input');
        if(!maxInput) return;

        var minInputNumber = document.getElementById(minInput);
        var maxInputNumber = document.getElementById(maxInput);

        rangeSlider.noUiSlider.on('update', function( values, handle ) {

            var value = values[handle];

            if ( handle ) {
                maxInputNumber.value = value;
            } else {
                minInputNumber.value = value;
            }
        });

        minInputNumber.addEventListener('change', function(){
            rangeSlider.noUiSlider.set([this.value, null]);
        });

        maxInputNumber.addEventListener('change', function(){
            rangeSlider.noUiSlider.set([null, this.value]);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            salaries();
            ages();
            gpa4();
            gpa5();
            experiences();
        }
    };
}();

jQuery(document).ready(function() {
    VacancyNoUiSliders.init();
});
