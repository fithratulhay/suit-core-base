var LocationFetchers = function() {

    var countries = function() {
        var countries = $('[data-country]');

        if (!countries.length) return;

        $.each(countries, function() {
            var elem        = $(this);

            var region      = elem.data('for-region');
            if(!region) return;
            var regionElmt  = $('#' + region);

            var city        = elem.data('for-city');
            var cityElmt    = null;

            if(!city) {
                cityElmt = null;
            } else {
                cityElmt = $('#' + city);
            }

            var area        = elem.data('for-area');
            var areaElmt    = null;

            if(!area) {
                areaElmt = null;
            } else {
                areaElmt = $('#' + area);
            }

            elem.on('change', function() {
                changeLocation(elem, regionElmt, cityElmt, areaElmt);
            });

            changeLocation(elem, regionElmt, cityElmt, areaElmt);
        });

        function changeLocation(selector, region, city, area) {
            city = city || null;
            area = area || null;

            var json  = selector.data('country');
            var value = region.data('value') || '';

            if ($(selector).val() != '') {
                $.get(json, { id: $(selector).val() }, function(data) {
                    region.empty();

                    if (city != null) {
                        city.empty();
                        city.append('<option value="">Select City</option>');
                        area.empty();
                        area.append('<option value="">Select Area</option>');
                    }

                    region.append('<option value="">Select Region</option>');

                    $.each(data, function(index, element) {
                        if (element.id == value) {
                            region.append("<option value='"+ element.id +"' selected='selected'>" + element.name + "</option>");
                        } else {
                            region.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                        }
                    });

                    region.val(value).trigger('change');

                    if (city != null) {
                        city.val('').trigger('change');
                        area.val('').trigger('change');
                    }
                });
            } else {
                region.val(value).trigger('change');

                if (city != null) {
                    city.val('').trigger('change');
                    area.val('').trigger('change');
                }
            }
        }
    }

    var regions = function() {
        var regions = $('[data-region]');

        if (!regions.length) return;

        $.each(regions, function() {
            var elem        = $(this);

            var city        = elem.data('for-city');
            if(!city) return;
            var cityElmt    = $('#' + city);

            var area        = elem.data('for-area');
            var areaElmt    = null;

            if(!area) {
                areaElmt = null;
            } else {
                areaElmt = $('#' + area);
            }

            elem.on('change', function() {
                changeLocation(elem, cityElmt, areaElmt);
            });

            changeLocation(elem, cityElmt, areaElmt);
        });

        function changeLocation(selector, city, area) {
            area = area || null;

            var json  = selector.data('region');
            var value = city.data('value') || '';

            if ($(selector).val() != '') {
                $.get(json, { id: $(selector).val() }, function(data) {
                    city.empty();

                    if (area != null) {
                        area.empty();
                        area.append('<option value="">Select Area</option>');
                    }

                    city.append('<option value="">Select City</option>');

                    $.each(data, function(index, element) {
                        if (element.id == value) {
                            city.append("<option value='"+ element.id +"' selected='selected'>" + element.name + "</option>");
                        } else {
                            city.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                        }
                    });

                    city.val(value).trigger('change');

                    if (area != null) {
                        area.val('').trigger('change');
                    }
                });
            } else {
                city.val(value).trigger('change');

                if (area != null) {
                    area.val('').trigger('change');
                }
            }
        }
    }

    var cities = function() {
        var cities = $('[data-city]');

        if (!cities.length) return;

        $.each(cities, function() {
            var elem        = $(this);

            var area        = elem.data('for-area');
            if(!area) return;
            var areaElmt    = $('#' + area);

            elem.on('change', function() {
                changeLocation(elem, areaElmt);
            });

            changeLocation(elem, areaElmt);
        });

        function changeLocation(selector, area) {
            var json  = selector.data('city');
            var value = area.data('value') || '';

            if ($(selector).val() != '') {
                $.get(json, { id: $(selector).val() }, function(data) {
                    area.empty();
                    area.append('<option value="">Select Area</option>');

                    $.each(data, function(index, element) {
                        if (element.id == value) {
                            area.append("<option value='"+ element.id +"' selected='selected'>" + element.name + "</option>");
                        } else {
                            area.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                        }
                    });

                    area.val(value).trigger('change');
                });
            } else {
                area.val(value).trigger('change');
            }
        }
    }

    return {
        //main function to initiate the module
        init: function() {
            countries();
            regions();
            cities();
        }
    };

}();

jQuery(document).ready(function() {
    LocationFetchers.init();
});
