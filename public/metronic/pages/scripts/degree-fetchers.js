var DegreeFetchers = function() {

    var degrees = function() {
        var degrees = $('[data-degree]');

        if (!degrees.length) return;

        $.each(degrees, function() {
            var elem        = $(this);

            var majors      = elem.data('for-major');
            if(!majors) return;
            var $major  = $('#' + majors);

            elem.on('change', function() {
                changeDegree(elem, $major);
            });

            changeDegree(elem, $major);
        });

        function changeDegree(selector, major) {
            var json = selector.data('degree');

            if ($(selector).val() != '') {
                $.get(json, { id: $(selector).val() }, function(data) {
                    major.empty();

                    major.append('<option value="">Select Major</option>');

                    $.each(data, function(index, element) {
                        major.append("<option value='"+ element.id +"'>" + element.name + "</option>");
                    });

                    major.val('').trigger('change');
                });
            } else {
                major.val('').trigger('change');
            }
        }
    }

    return {
        //main function to initiate the module
        init: function() {
            degrees();
        }
    };

}();

jQuery(document).ready(function() {
    DegreeFetchers.init();
});
