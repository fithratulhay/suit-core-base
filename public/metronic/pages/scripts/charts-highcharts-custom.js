jQuery(document).ready(function() {
  // HIGHCHARTS DEMOS
    function chartZoom() {
    // LINE CHART 2
        var $chartZoom   = $('.chartZoom')
        if(!$chartZoom.length) return;

        var chartContainer = '.chartZoom-container'

        $(chartContainer).each(function() {
            var $thisChart = $(this);
            var dataChart   = $thisChart.attr('data-chart');
            var title       = $thisChart.attr('title');
            var xtitle       = $thisChart.attr('xtitle');
            var ytitle       = $thisChart.attr('ytitle');

            $.getJSON(dataChart, function (data) {
                chartOption($thisChart, data, title, xtitle, ytitle);

            });
        })

        $chartZoom.on('click', '.chartZoom-btn', function(e) {
            e.preventDefault();
            var $this   = $(e.currentTarget);
            var chart   = $this.prev('.chartZoom-container').attr('data-chart');
            var target  = $this.attr('href');
            var $target = $(target);
            var $chartContainer = $target.find('.chartModal-container');
            var modalTitle = $this.prev('.chartZoom-container').attr('title');
            var _xTitle = $this.prev('.chartZoom-container').attr('xtitle');
            var _yTitle = $this.prev('.chartZoom-container').attr('ytitle');
            
            $chartContainer.empty();

            $.getJSON(chart, function (data) {

                chartOption($chartContainer, data, modalTitle, _xTitle, _yTitle);
            });
        })

        function chartOption(element, data, title, xTitle, yTitle) {
            var options = {
                chart: { 
                    type: 'line',
                    zoomType: 'x'
                },
                title: {
                    text: title,
                    style: {
                        'fontSize': '14px',
                        'fontFamily': 'Open Sans'
                    }
                },
                yAxis: { title: { text: '' } },
                plotOptions: {
                    line: {
                        dataLabels: { enabled: true },
                        enableMouseTracking: true
                    }
                },
                credits: { enabled: false },
                legend: {
                    enabled: true
                }
            };

            element.addClass('chart-loaded');

            var currentOpts = $.extend(true, options, {});
            currentOpts.title.text = data.title;
            currentOpts.series = data.series;

            currentOpts.xAxis = data.xAxis;
            currentOpts.yAxis.title.text = data.yAxisTitle;
            currentOpts.legend.enabled = true;

            element.highcharts( currentOpts );

            /*
            element.highcharts({
                title: {
                    text: title
                },
                xAxis: {
                    type: 'datetime',
                    title: {
                        text: xTitle
                    }
                },
                yAxis: {
                    title: {
                        text: yTitle
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },
                credits: { enabled: false },

                series: [{
                    type: 'area',
                    name: xTitle,
                    data: data
                }]
            });
            */
        }

        
    }

    chartZoom();

});
