<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        \Suitconversation\Repositories\Contract\ConversationRepositoryContract::class => \Suitconversation\Repositories\ConversationRepository::class,
        \Suitconversation\Repositories\Contract\ConversationGroupRepositoryContract::class => \Suitconversation\Repositories\ConversationGroupRepository::class,
        \Suitconversation\Repositories\Contract\ConversationMemberRepositoryContract::class => \Suitconversation\Repositories\ConversationMemberRepository::class,
    ],

    'data_options_route_name' => [
    	'conversationgroup' => 'backend.conversation-group.options.json'
    ]
];