<?php

return [
    'base_domain' => env('APP_BASE_DOMAIN', 'suitcore.local'),

    'enable_cache_level_repository' => env('ENABLE_CACHE_LEVEL_REPOSITORY', true),

    'enable_cache_level_controller' => env('ENABLE_CACHE_LEVEL_CONTROLLER', false),

    'expiry_minutes_cache_level_controller' => env('EXPIRY_MINUTES_CACHE_LEVEL_CONTROLLER', 60), // pessimistic-cache

    'image_resizer_quality' => env('IMAGE_RESIZER_QUALITY', 75),

    'multi_locale_site' => env('APP_MULTI_LOCALE', false),

    'default_user_timezone' => 'Asia/Jakarta',

    'default_user_role' => 'member',

    'images' => [
        'imageWithThumbnail' => true,
        'imageDestinationPath' => 'public/files', // base_path based
        'imageUseAbsolutePath' => false,
        'imageFileNameOnly' => true,
        'imageBasePath' => 'public', // based_path based
        'imageDirectory' => '',
        'imageMaxHeight' => 1800, // based on retina display
        'imageMaxWidth' => 2880, // based on retina display
    ],

    'thumbnailer' => [
        // 'thumb' => '_thumb_',
        // 'size' => '300x300',
    ],

    'uploader' => [
        // 'override' => false,
        // 'modelOverride' => true,
        // 'baseFolder' => 'public/uploads',
        // 'folder' => '',
    ],

    'varnishcacheable' => env('APP_VARNISH_CACHEABLE', false),

    'backend_admin_subpath' => env('APP_ADMIN_DEFAULT_SUBPATH', 'paneladmin'),

    'backend_admin_subpath_access' => env('APP_ADMIN_DEFAULT_SUBPATH_ACCESS', 'subpath'), // 'subpath' or 'subdomain'

    'default_user_timezone' => 'Asia/Jakarta',

    'default_user_role' => 'member',

    'default_repository_queue' => env('QUEUE_NAME', 'suitcore-default'),

    'default_import_job_queue' => env('QUEUE_IMPORT_NAME', 'suitcore-import'),

    'default_export_job_queue' => env('QUEUE_EXPORT_NAME', 'suitcore-export'),

    'user_activity_logging' => [
        'enable' => env('APP_ENABLE_USER_ACTIVITY_LOGGING', false),
        'excluded_route_name' => [
            'backend.home.appuser-summary'
        ]
    ],

    'thumbnailer_method' => env('APP_THUMBNAILER_METHOD', 'thumbnailer'), // 'thumbnailer' or 'medialibrary'

    'auto_init_attribute_settings' => env('APP_AUTO_INIT_ATTRIBUTE_SETTINGS', false),
    
    'custom_base_model' => [],

    // enable purify_input_xss() helper, clean malicious input when creating and updating record
    'purify_input_xss' => env('PURIFY_INPUT_XSS', false),

    // enable purify_output_xss() helper, prevent xss by purifying after being decoded
    'purify_output_xss' => env('PURIFY_OUTPUT_XSS', false),

    // if true, hide web with css when embedded to an iframe
    'bust_iframe' => env('BUST_IFRAME', false),

    'repositories' => [
        // User Service
        \Suitcore\Repositories\Contract\SuitUserRepositoryContract::class => \App\Repositories\UserRepository::class,

        // Role Service
        \Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract::class => \Suitcore\Repositories\SuitUserRoleRepository::class,

        // Permission Service
        \Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract::class => \Suitcore\Repositories\SuitUserPermissionRepository::class,

        // User Log Service
        \Suitcore\Repositories\Contract\SuitUserLogRepositoryContract::class => \Suitcore\Repositories\SuitUserLogRepository::class,

        // Setting Service
        \Suitcore\Repositories\Contract\SuitSettingRepositoryContract::class => \App\Repositories\SettingRepository::class,

        // Email Notification Templating Service
        \Suitcore\Repositories\Contract\SuitEmailTemplateRepositoryContract::class => \Suitcore\Repositories\SuitEmailTemplateRepository::class,

        // User Setting Service
        \Suitcore\Repositories\Contract\SuitUserSettingRepositoryContract::class => \Suitcore\Repositories\SuitUserSettingRepository::class,

        // Notification Service
        \Suitcore\Repositories\Contract\SuitNotificationRepositoryContract::class => \Suitcore\Repositories\SuitNotificationRepository::class,

        // User Token Service (for Rest-API Access)
        \Suitcore\Repositories\Contract\SuitUserTokenRepositoryContract::class => \Suitcore\Repositories\SuitUserTokenRepository::class,

        // Suit URL Redirection Repository Service
        \Suitcore\Repositories\Contract\SuitUrlRedirectionRepositoryContract::class => \Suitcore\Repositories\SuitUrlRedirectionRepository::class,

        // Suit URL SEO Repository Service
        \Suitcore\Repositories\Contract\SuitUrlSeoRepositoryContract::class => \Suitcore\Repositories\SuitUrlSeoRepository::class,

        // Suit Base Repository Service
        \Suitcore\Repositories\Contract\SuitRepositoryContract::class => \Suitcore\Repositories\SuitRepository::class,

        // Oauth Repository Service
        \Suitcore\Repositories\Contract\SuitOauthUserRepositoryContract::class => \Suitcore\Repositories\SuitOauthUserRepository::class,
    ],

    /*
    // SINGLETON IMPLEMENTATION
    'repositories' => [
        // User Service
        \Suitcore\Repositories\Contract\SuitUserRepositoryContract::class => [
            \Suitcore\Repositories\SuitUserRepository::class => [
                \Suitcore\Models\SuitUser::class,
            ],
        ],

        // Role Service
        \Suitcore\Repositories\Contract\SuitUserRoleRepositoryContract::class => [
            \Suitcore\Repositories\SuitUserRoleRepository::class => [
                \Suitcore\Models\SuitUserRole::class,
                \Suitcore\Repositories\SuitUserPermissionRepository::class => [
                    \Suitcore\Models\SuitUserPermission::class,
                ],
            ],
        ],

        // Permission Service
        \Suitcore\Repositories\Contract\SuitUserPermissionRepositoryContract::class => [
            \Suitcore\Repositories\SuitUserPermissionRepository::class => [
                \Suitcore\Models\SuitUserPermission::class,
            ],
        ],

        // Setting Service
        \Suitcore\Repositories\Contract\SuitSettingRepositoryContract::class => [
            \Suitcore\Repositories\SuitSettingRepository::class => [
                \Suitcore\Models\SuitSetting::class,
            ],
        ],

        // Email Notification Templating Service
        \Suitcore\Repositories\Contract\SuitEmailTemplateRepositoryContract::class => [
            \Suitcore\Repositories\SuitEmailTemplateRepository::class => [
                \Suitcore\Models\SuitEmailTemplate::class,
            ],
        ],

        // User Setting Service
        \Suitcore\Repositories\Contract\SuitUserSettingRepositoryContract::class => [
            \Suitcore\Repositories\SuitUserSettingRepository::class => [
                \Suitcore\Models\SuitUserSetting::class,
            ],
        ],

        // Notification Service
        \Suitcore\Repositories\Contract\SuitNotificationRepositoryContract::class => [
            \Suitcore\Repositories\SuitEmailTemplateRepository::class => [
                \Suitcore\Models\SuitEmailTemplate::class,
            ],
        ],

        // User Token Service (for Rest-API Access)
        \Suitcore\Repositories\Contract\SuitUserTokenRepositoryContract::class => [
            \Suitcore\Repositories\SuitUserTokenRepository::class => [
                \Suitcore\Models\SuitUserToken::class,
            ],
        ],

        // Suit URL Redirection Repository Service
        \Suitcore\Repositories\Contract\SuitUrlRedirectionRepositoryContract::class => [
            \Suitcore\Repositories\SuitUrlRedirectionRepository::class => [
                \Suitcore\Models\SuitUrlRedirection::class,
            ],
        ],

        // Suit Base Repository Service
        \Suitcore\Repositories\Contract\SuitRepositoryContract::class => [
            \Suitcore\Repositories\SuitRepository::class => [
                \Suitcore\Models\SuitModel::class,
            ],
        ],
    ],
    */

    'data_options_route_name' => [
        'suituser' => 'backend.user.options.json'
    ],
    
    'admin_panel_default_base_view' => [
        'metronics1' => 'suitcore.metronics1.partials',
        'metronics2' => 'suitcore.metronics2.partials',
        'metronics3' => 'suitcore.metronics3.partials',
        'metronics4' => 'suitcore.metronics4.partials',
        'metronics5' => 'suitcore.metronics5.partials',
        'metronics6' => 'suitcore.metronics6.partials',
    ],

    'admin_panel_form_input_partial_views' => [ 
        'text' => 'suitcore.metronics1.partials.inputtext',
        'password' => 'suitcore.metronics1.partials.inputpassword',
        'numeric' => 'suitcore.metronics1.partials.inputnumeric',
        'float' => 'suitcore.metronics1.partials.inputfloat',
        'boolean' => 'suitcore.metronics1.partials.inputboolean',
        'datetime' => 'suitcore.metronics1.partials.inputdatetime',
        'date' => 'suitcore.metronics1.partials.inputdate',
        'time' => 'suitcore.metronics1.partials.inputtime',
        'textarea' => 'suitcore.metronics1.partials.inputtextarea',
        'richtextarea' => 'suitcore.metronics1.partials.inputrichtextarea',
        'file' => 'suitcore.metronics1.partials.inputfile',
        'select' => 'suitcore.metronics1.partials.inputselect', 
        'referenceslabel' => 'suitcore.metronics1.partials.inputreferenceslabel', 
        'group-map' => 'suitcore.metronics1.partials.inputmap', 
        'group-daterange' => 'suitcore.metronics1.partials.inputdaterange',
        'group-datetimerange' => 'suitcore.metronics1.partials.inputdatetimerange',
        'group-numericrange' => 'suitcore.metronics1.partials.inputnumericrange',
        'group-timerange' => 'suitcore.metronics1.partials.inputtimerange',
    ],

    'admin_panel_datatable_partial_views' => [ 
        'tableselection' => 'suitcore.metronics1.partials.tableselection',
        'tablemenu' => 'suitcore.metronics1.partials.tablemenu',
    ],

    'default_thumbnail_dimension' => [
        'small_square' => '128x128',
        'medium_square' => '256x256',
        'large_square' => '512x512',
        'xlarge_square' => '2048x2048',
        'small_cover' => '240x_',
        'normal_cover' => '360x_',
        'medium_cover' => '480x_',
        'large_cover' => '1280x_',
        'small_banner' => '_x240',
        'normal_banner' => '_x360',
        'medium_banner' => '_x480',
        'large_banner' => '_x1280'
    ],

    "metrics" => [
        "currency" => "IDR",
        "weight"   => "gram",
        "length"   => "meter",
        "quantity" => "pcs"
    ],

    "paginations" => [
        "perPage" => 10,
    ],
    
    "backendNavigation" => [
        'A' => [
            'label' => 'Home',
            'route' => 'backend.home.index',
            'icon' => 'fa fa-home',
            'roles' => ['admin'],
            'submenu' => [
                'A1' => [
                    'label' => 'Dashboard',
                    'route' => 'backend.home.index',
                    'icon' => 'fa fa-dashboard',
                    'roles' => ['admin'],
                ],
                'A2' => [
                    'label' => 'Notification',
                    'route' => 'backend.notification.index',
                    'icon' => 'fa fa-bell',
                    'roles' => ['admin'],
                ],
                'A3' => [
                    'label' => 'Activity Log',
                    'route' => 'backend.activity.index',
                    'icon' => 'fa fa-area-chart',
                    'roles' => ['admin'],
                ]
            ]
        ],
        'C' => [
            'label'   => 'User',
            'route'   => '',
            'icon'    => 'icon-users',
            'roles'   => ['admin'],
            'submenu' => [
                'C1'  => [
                    'label' => 'User Management',
                    'route' => 'backend.user.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'C2'  => [
                    'label' => 'Role Management',
                    'route' => 'backend.role.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'C3'  => [
                    'label' => 'Permission Management',
                    'route' => 'backend.permission.acl',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'C4'  => [
                    'label' => 'User Log',
                    'route' => 'backend.userlog.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'C5' => [
                    'label' => 'User Review',
                    'route' => 'backend.review.index',
                    'icon' => 'icon-star',
                    'roles' => ['admin'],
                ],
                'C6' => [
                    'label' => 'User Conversation',
                    'route' => 'backend.conversation.index',
                    'icon' => 'fa fa-comments-o',
                    'roles' => ['admin'],
                ],
                'C7' => [
                    'label' => 'User Conversation Group',
                    'route' => 'backend.conversation-group.index',
                    'icon' => 'fa fa-comments-o',
                    'roles' => ['admin'],
                ],
                'C8' => [
                    'label' => 'User Conversation Member',
                    'route' => 'backend.conversation-member.index',
                    'icon' => 'fa fa-comments-o',
                    'roles' => ['admin'],
                ]
            ]
        ],
        'B' => [
            'label'   => 'Marketing',
            'route'   => '',
            'icon'    => 'icon-briefcase',
            'roles'   => ['admin'],
            'submenu' => [
                'B1'  => [
                    'label' => 'Email Subscriber',
                    'route' => 'backend.newslettersubscribers.index',
                    'icon'  => 'fa fa-user',
                    'roles' => ['admin']
                ],
                'B2'  => [
                    'label' => 'Newsletter',
                    'route' => 'backend.newsletter.index',
                    'icon'  => 'fa fa-envelope',
                    'roles' => ['admin']
                ]
            ]
        ],
        'E' => [
            'label'   => 'Site',
            'route'   => '',
            'icon'    => 'icon-globe',
            'roles'   => ['admin'],
            'submenu' => [
                'E8' => [
                    'label' => 'Menu',
                    'route' => 'backend.menu.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E9' => [
                    'label' => 'Banner',
                    'route' => 'backend.banner.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E1' => [
                    'label' => 'Content Type & Category',
                    'route' => 'backend.contenttype.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E2' => [
                    'label' => 'Content',
                    'route' => 'backend.content.index',
                    'icon'  => 'icon-note',
                    'roles' => ['admin'],
                ],
                'E3' => [
                    'label' => 'Static Page',
                    'route' => 'backend.page.index',
                    'icon'  => 'icon-note',
                    'roles' => ['admin'],
                ],
                'E14' => [
                    'label'   => 'Gallery',
                    'route'   => 'backend.gallery.index',
                    'icon'    => 'fa fa-folder-open',
                    'roles'   => ['admin'],
                ],
                'E15' => [
                    'label'   => 'Comments',
                    'route'   => 'backend.comment.index',
                    'icon'    => 'fa fa-comments',
                    'roles'   => ['admin'],
                ],
                'E4' => [
                    'label' => 'Contact Message',
                    'route' => 'backend.contactmessage.index',
                    'icon'  => 'fa fa-comment-o',
                    'roles' => ['admin'],
                ],
                'E5' => [
                    'label' => 'FAQ Category',
                    'route' => 'backend.faqcategory.index',
                    'icon'  => 'icon-question',
                    'roles' => ['admin'],
                ],
                'E6' => [
                    'label' => 'FAQ',
                    'route' => 'backend.faq.index',
                    'icon'  => 'icon-question',
                    'roles' => ['admin'],
                ],
                'E10' => [
                    'label' => 'URL SEO',
                    'route' => 'backend.urlseo.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E11' => [
                    'label' => 'URL Redirection',
                    'route' => 'backend.urlredirection.index',
                    'icon'  => 'icon-tag',
                    'roles' => ['admin'],
                ],
                'E7' => [
                    'label' => 'Setting',
                    'route' => 'backend.settings.view',
                    'icon'  => 'icon-settings',
                    'roles' => ['admin'],
                ],
            ]
        ],
        'F' => [
            'label'   => 'File Manager',
            'route'   => 'elfinder.index',
            'icon'    => 'fa fa-folder-open',
            'roles'   => ['admin'],
            'submenu' => [
                'F1' => [
                    'label'   => 'File Collection Manager',
                    'route'   => 'elfinder.index',
                    'icon'    => 'fa fa-folder-open',
                    'roles'   => ['admin'],
                ],
                'F2' => [
                    'label'   => 'File Category',
                    'route'   => 'backend.file-category.index',
                    'icon'    => 'fa fa-file-archive-o',
                    'roles'   => ['admin'],
                ],
                'F3' => [
                    'label'   => 'File',
                    'route'   => 'backend.file.index',
                    'icon'    => 'fa fa-file-pdf-o',
                    'roles'   => ['admin'],
                ]
            ]
        ],
        'G' => [
            'label'   => 'Polling',
            'route'   => '',
            'icon'    => 'fa fa-clone',
            'roles'   => ['admin'],
            'submenu' => [
                'G0' => [
                    'label'   => 'Polling Category',
                    'route'   => 'backend.poll-category.index',
                    'icon'    => 'fa fa-folder',
                    'roles'   => ['admin'],
                ],
                'G1' => [
                    'label'   => 'Polling Question',
                    'route'   => 'backend.poll.index',
                    'icon'    => 'fa fa-question',
                    'roles'   => ['admin'],
                ],
                'G2' => [
                    'label'   => 'Polling Answer Options',
                    'route'   => 'backend.poll-option.index',
                    'icon'    => 'fa fa-ellipsis-v',
                    'roles'   => ['admin'],
                ],
                'G3' => [
                    'label'   => 'Polling Answer',
                    'route'   => 'backend.poll-answer.index',
                    'icon'    => 'fa fa-check-square-o',
                    'roles'   => ['admin'],
                ]
            ]
        ]
    ],

    "backend_theme" => [
        "metronics-1" => "Metronics 1",
        "metronics-2" => "Metronics 2",
        "metronics-3" => "Metronics 3",
        "metronics-4" => "Metronics 4",
        "metronics-5" => "Metronics 5",
        "metronics-6" => "Metronics 6",
    ],

    "backend_theme_base_color" => [
        "custom-default.min.css" => "Default Blue",
        "custom-green.min.css"   => "Green",
        "custom-orange.min.css"   => "Orange",
        "custom-pink.min.css" => "Pink",
        "custom-purple.min.css" => "Purple",
        "custom-red.min.css" => "Red",
        "custom-tosca.min.css" => "Tosca"
    ],
];
