<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        // Challenge Group : Can be course category, etc
        \Suitquestionanswer\Repositories\Contract\ChallengeGroupRepositoryContract::class => \Suitquestionanswer\Repositories\ChallengeGroupRepository::class,

        // Challenge : Can be course, etc
        \Suitquestionanswer\Repositories\Contract\ChallengeRepositoryContract::class => \Suitquestionanswer\Repositories\ChallengeRepository::class,

        // Challenge Chapter : Can be course session, etc
        \Suitquestionanswer\Repositories\Contract\ChapterRepositoryContract::class => \Suitquestionanswer\Repositories\ChapterRepository::class,

        // Challenge Chapter Material : Can be course session material
        \Suitquestionanswer\Repositories\Contract\MaterialRepositoryContract::class => \Suitquestionanswer\Repositories\MaterialRepository::class,

        // Challenge Chapter Question
        \Suitquestionanswer\Repositories\Contract\QuestionRepositoryContract::class => \Suitquestionanswer\Repositories\QuestionRepository::class,

        // Challenge Chapter Question Answers
        \Suitquestionanswer\Repositories\Contract\AnswerRepositoryContract::class => \Suitquestionanswer\Repositories\AnswerRepository::class,

        // Challenge Group User Participation
        \Suitquestionanswer\Repositories\Contract\UserChallengeGroupRepositoryContract::class => \Suitquestionanswer\Repositories\UserChallengeGroupRepository::class,

        // Challenge User Participation
        \Suitquestionanswer\Repositories\Contract\UserChallengeRepositoryContract::class => \Suitquestionanswer\Repositories\UserChallengeRepository::class,

        // Challenge Chapter User Participation
        \Suitquestionanswer\Repositories\Contract\UserChapterRepositoryContract::class => \Suitquestionanswer\Repositories\UserChapterRepository::class,

        // Challenge Chapter Material User Participation (download status, etc)
        \Suitquestionanswer\Repositories\Contract\UserMaterialRepositoryContract::class => \Suitquestionanswer\Repositories\UserMaterialRepository::class,

        // Challenge Chapter Questen User Answer
        \Suitquestionanswer\Repositories\Contract\UserAnswerRepositoryContract::class => \Suitquestionanswer\Repositories\UserAnswerRepository::class,

        // Overall Transaction between Repositories
        \Suitquestionanswer\Repositories\Contract\ChallengeFeatureRepositoryContract::class => \Suitquestionanswer\Repositories\ChallengeFeatureRepository::class,
    ],

    'data_options_route_name' => [],
];
