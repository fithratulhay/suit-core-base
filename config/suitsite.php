<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        \Suitsite\Repositories\Contract\ArticleRepositoryContract::class => \Suitsite\Repositories\ArticleRepository::class,

        \Suitsite\Repositories\Contract\BannerRepositoryContract::class => \Suitsite\Repositories\BannerRepository::class,

        \Suitsite\Repositories\Contract\ContactMessageRepositoryContract::class => \Suitsite\Repositories\ContactMessageRepository::class,

        \Suitsite\Repositories\Contract\ContentTypeRepositoryContract::class => \Suitsite\Repositories\ContentTypeRepository::class,

        \Suitsite\Repositories\Contract\ContentCategoryRepositoryContract::class => \Suitsite\Repositories\ContentCategoryRepository::class,

        \Suitsite\Repositories\Contract\ContentRepositoryContract::class => \Suitsite\Repositories\ContentRepository::class,

        \Suitsite\Repositories\Contract\PageRepositoryContract::class => \Suitsite\Repositories\PageRepository::class,

        \Suitsite\Repositories\Contract\FaqRepositoryContract::class => \Suitsite\Repositories\FaqRepository::class,

        \Suitsite\Repositories\Contract\FaqCategoryRepositoryContract::class => \Suitsite\Repositories\FaqCategoryRepository::class,

        \Suitsite\Repositories\Contract\MenuRepositoryContract::class => \Suitsite\Repositories\MenuRepository::class,

        \Suitsite\Repositories\Contract\NewsletterRepositoryContract::class => \Suitsite\Repositories\NewsletterRepository::class,

        \Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract::class => \Suitsite\Repositories\NewsletterSubscriberRepository::class,
    ],

    'banner_type' => [ 
        'main-banner' => 'Main Banner',
        'side-banner' => 'Side Banner'
    ],

    'menu_type' => [ 
        'header' => 'Header Menu Navigation',
        'side_nav' => 'Side Menu Navigation',
        'footer_nav' => 'Footer Menu Navigation'
    ],

    /*
    // SINGLETON IMPLEMENTATION
    'repositories' => [
        \Suitsite\Repositories\Contract\BannerRepositoryContract::class => [
            \Suitsite\Repositories\BannerRepository::class => [
                \Suitsite\Models\Banner::class,
            ],
        ],

        \Suitsite\Repositories\Contract\ContactMessageRepositoryContract::class => [
            \Suitsite\Repositories\ContactMessageRepository::class => [
                \Suitsite\Models\ContactMessage::class,
            ],
        ],

        \Suitsite\Repositories\Contract\ContentTypeRepositoryContract::class => [
            \Suitsite\Repositories\ContentTypeRepository::class => [
                \Suitsite\Models\ContentType::class,
            ],
        ],

        \Suitsite\Repositories\Contract\ContentCategoryRepositoryContract::class => [
            \Suitsite\Repositories\ContentCategoryRepository::class => [
                \Suitsite\Models\ContentCategory::class,
                \Suitsite\Repositories\ContentTypeRepository::class => [
                    \Suitsite\Models\ContentType::class,
                ],
            ],
        ],

        \Suitsite\Repositories\Contract\ContentRepositoryContract::class => [
            \Suitsite\Repositories\ContentRepository::class => [
                \Suitsite\Models\Content::class,
                \Suitsite\Repositories\ContentTypeRepository::class => [
                    \Suitsite\Models\ContentType::class,
                ],
                \Suitsite\Repositories\ContentCategoryRepository::class => [
                    \Suitsite\Models\ContentCategory::class,
                    \Suitsite\Repositories\ContentTypeRepository::class => [
                        \Suitsite\Models\ContentType::class,
                    ],
                ],
            ],
        ],

        \Suitsite\Repositories\Contract\FaqRepositoryContract::class => [
            \Suitsite\Repositories\FaqRepository::class => [
                \Suitsite\Models\Faq::class,
                \Suitsite\Repositories\FaqCategoryRepository::class => [
                    \Suitsite\Models\FaqCategory::class,
                ],
            ],
        ],

        \Suitsite\Repositories\Contract\FaqCategoryRepositoryContract::class => [
            \Suitsite\Repositories\FaqCategoryRepository::class => [
                \Suitsite\Models\FaqCategory::class,
            ],
        ],

        \Suitsite\Repositories\Contract\MenuRepositoryContract::class => [
            \Suitsite\Repositories\MenuRepository::class => [
                \Suitsite\Models\Menu::class,
            ],
        ],

        \Suitsite\Repositories\Contract\NewsletterRepositoryContract::class => [
            \Suitsite\Repositories\NewsletterRepository::class => [
                \Suitsite\Models\Newsletter::class,
            ],
        ],

        \Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract::class => [
            \Suitsite\Repositories\NewsletterSubscriberRepository::class => [
                \Suitsite\Models\NewsletterSubscriber::class,
            ],
        ],
    ],
    */

    'data_options_route_name' => [
        'menu' => 'backend.menu.options.json',
    ]
];
