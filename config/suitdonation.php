<?php

return [
    'notification_messages' => [
        'donationnotifier' => 'notification.donationnotifier',
        'donationpaid' => 'notification.donationpaid',
        'newprogramupdate' => 'notification.newprogramupdate',
        'programupdatepublished' => 'notification.programupdatepublished',
        'messagereply' => 'notification.messagereply',
        'donationreceive' => 'notification.donationreceive',
    ],

    'custom_base_model' => [],

    'repositories' => [
        \Suitdonation\Repositories\Contract\CityRepositoryContract::class => \Suitdonation\Repositories\CityRepository::class,

        \Suitdonation\Repositories\Contract\CountryRepositoryContract::class => \Suitdonation\Repositories\CountryRepository::class,

        \Suitdonation\Repositories\Contract\MessageRepositoryContract::class => \Suitdonation\Repositories\MessageRepository::class,

        \Suitdonation\Repositories\Contract\OrganizationRepositoryContract::class => \Suitdonation\Repositories\OrganizationRepository::class,

        \Suitdonation\Repositories\Contract\ProfileRepositoryContract::class => \Suitdonation\Repositories\ProfileRepository::class,
        \Suitdonation\Repositories\Contract\ProgramRepositoryContract::class => \Suitdonation\Repositories\ProgramRepository::class,

        \Suitdonation\Repositories\Contract\ProgramCategoryRepositoryContract::class => \Suitdonation\Repositories\ProgramCategoryRepository::class,

        \Suitdonation\Repositories\Contract\ProgramGalleryRepositoryContract::class => \Suitdonation\Repositories\ProgramGalleryRepository::class,

        \Suitdonation\Repositories\Contract\ProgramTransactionRepositoryContract::class => \Suitdonation\Repositories\ProgramTransactionRepository::class,

        \Suitdonation\Repositories\Contract\ProgramTypeRepositoryContract::class => \Suitdonation\Repositories\ProgramTypeRepository::class,

        \Suitdonation\Repositories\Contract\ProgramUpdateRepositoryContract::class => \Suitdonation\Repositories\ProgramUpdateRepository::class,

        \Suitdonation\Repositories\Contract\ProvinceRepositoryContract::class => \Suitdonation\Repositories\ProvinceRepository::class,

        \Suitdonation\Repositories\Contract\ReportRepositoryContract::class => \Suitdonation\Repositories\ReportRepository::class,

        \Suitdonation\Repositories\Contract\ReportedProgramRepositoryContract::class => \Suitdonation\Repositories\ReportedProgramRepository::class,

        \Suitdonation\Repositories\Contract\WithdrawalRepositoryContract::class => \Suitdonation\Repositories\WithdrawalRepository::class,

        \Suitdonation\Repositories\Contract\FeaturedProgramRepositoryContract::class => \Suitdonation\Repositories\FeaturedProgramRepository::class,

        \Suitdonation\Repositories\Contract\LikeRepositoryContract::class => \Suitdonation\Repositories\LikeRepository::class,

        \Suitdonation\Repositories\Contract\UserBankRepositoryContract::class => \Suitdonation\Repositories\UserBankRepository::class,

        \Suitdonation\Repositories\Contract\UserRateRepositoryContract::class => \Suitdonation\Repositories\UserRateRepository::class,
    ],

    'data_options_route_name' => [
        'program' => 'backend.program.options.json',
        'programtype' => 'backend.program-type.options.json',
        'programcategory' => 'backend.program-category.options.json',
        'reportedprogram' => 'backend.reported-program.options.json',
        'country' => 'backend.country.options.json',
        'province' => 'backend.province.options.json',
        'city' => 'backend.city.options.json'
    ]
];
