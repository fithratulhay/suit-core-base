<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'onesignal' => [
        'app_id' => env('ONESIGNAL_APP_ID'),
        'rest_api_key' => env('ONESIGNAL_REST_API_KEY')
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'rollbar' => [
        'access_token' => env('ROLLBAR_ACCESS_TOKEN', ''),
        'level' => env('ROLLBAR_LEVEL', 'error'),
    ],

    // Socialite
    
    'facebook' => [
        'client_id' => env('FB_CLIENT_ID'),
        'client_secret' => env('FB_CLIENT_SECRET'),
        'redirect' => env('FB_REDIRECT'),
    ],

    'twitter' => [
        'client_id' => env('TW_CLIENT_ID'),
        'client_secret' => env('TW_CLIENT_SECRET'),
        'redirect' => env('TW_REDIRECT'),
    ],

    'google' => [
        'client_id' => env('GP_CLIENT_ID'),
        'client_secret' => env('GP_CLIENT_SECRET'),
        'redirect' => env('GP_REDIRECT'),
    ],

    'zenziva' => [
        'userkey' => env('ZENZIVA_SMS_CLIENT_USERKEY', ''),
        'passkey' => env('ZENZIVA_SMS_CLIENT_PASSKEY', ''),
        'masking' => env('ZENZIVA_SMS_CLIENT_MASKING', false)
    ],

];
