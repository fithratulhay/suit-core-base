<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        // Bank Service
        \Suitpay\Repositories\Contract\BankRepositoryContract::class => \Suitpay\Repositories\BankRepository::class,

        // Contact Message Service
        \Suitpay\Repositories\Contract\DokuBankRepositoryContract::class => \Suitpay\Repositories\DokuBankRepository::class,

        // ThirdPartyModule Type Service
        \Suitpay\Repositories\Contract\DokuInstallmentTenorRepositoryContract::class => \Suitpay\Repositories\DokuInstallmentTenorRepository::class,

        // ThirdPartyModule Category Service
        \Suitpay\Repositories\Contract\PaymentMethodRepositoryContract::class => \Suitpay\Repositories\PaymentMethodRepository::class,

        // ThirdPartyModule Service
        \Suitpay\Repositories\Contract\ThirdPartyModuleRepositoryContract::class => \Suitpay\Repositories\ThirdPartyModuleRepository::class,

        // ThirdpartyPaymentProcess Service
        \Suitpay\Repositories\Contract\ThirdpartyPaymentProcessRepositoryContract::class => \Suitpay\Repositories\ThirdpartyPaymentProcessRepository::class,

        // ThirdpartyPaymentProcess Category Service
        \Suitpay\Repositories\Contract\VeritransBankRepositoryContract::class => \Suitpay\Repositories\VeritransBankRepository::class,

        // VeritransInstallmentTenor Service
        \Suitpay\Repositories\Contract\VeritransInstallmentTenorRepositoryContract::class => \Suitpay\Repositories\VeritransInstallmentTenorRepository::class,
    ],

    /*
    // SINGLETON IMPLEMENTATION
    'repositories' => [

        // Bank Service
        \Suitpay\Repositories\Contract\BankRepositoryContract::class => [
            \Suitpay\Repositories\BankRepository::class => [
                \Suitpay\Models\Bank::class,
            ],
        ],

        // Contact Message Service
        \Suitpay\Repositories\Contract\DokuBankRepositoryContract::class => [
            \Suitpay\Repositories\DokuBankRepository::class => [
                \Suitpay\Models\DokuBank::class,
            ],
        ],

        // ThirdPartyModule Type Service
        \Suitpay\Repositories\Contract\DokuInstallmentTenorRepositoryContract::class => [
            \Suitpay\Repositories\DokuInstallmentTenorRepository::class => [
                \Suitpay\Models\DokuInstallmentTenor::class,
            ],
        ],

        // ThirdPartyModule Category Service
        \Suitpay\Repositories\Contract\PaymentMethodRepositoryContract::class => [
            \Suitpay\Repositories\PaymentMethodRepository::class => [
                \Suitpay\Models\PaymentMethod::class,
            ],
        ],

        // ThirdPartyModule Service
        \Suitpay\Repositories\Contract\ThirdPartyModuleRepositoryContract::class => [
            \Suitpay\Repositories\ThirdPartyModuleRepository::class => [
                \Suitpay\Models\ThirdPartyModule::class,
            ],
        ],

        // ThirdpartyPaymentProcess Service
        \Suitpay\Repositories\Contract\ThirdpartyPaymentProcessRepositoryContract::class => [
            \Suitpay\Repositories\ThirdpartyPaymentProcessRepository::class => [
                \Suitpay\Models\ThirdpartyPaymentProcess::class,
            ],
        ],

        // ThirdpartyPaymentProcess Category Service
        \Suitpay\Repositories\Contract\VeritransBankRepositoryContract::class => [
            \Suitpay\Repositories\VeritransBankRepository::class => [
                \Suitpay\Models\VeritransBank::class,
            ],
        ],

        // VeritransInstallmentTenor Service
        \Suitpay\Repositories\Contract\VeritransInstallmentTenorRepositoryContract::class => [
            \Suitpay\Repositories\VeritransInstallmentTenorRepository::class => [
                \Suitpay\Models\VeritransInstallmentTenor::class,
            ],
        ],
    ],
    */

    "metrics" => [
        "currency" => "Rp",
        "quantity" => "pcs"
    ],

    "payment" => [
        "thirdparty" => true
    ],
];

