<?php 

return [
    'base' => [
        'icon' => env('APP_ICON', 'https://upload.wikimedia.org/wikipedia/commons/4/4f/Laravel_logo.png')
    ],

	'api' => [
		'google_map' => env('GOOGLE_MAP_API'),
		'facebook_app' => env('FACEBOOK_APP_ID')
	],

    'emailer' => [
        'from' => [
            'address' => env('MAIL_FROM_ADDRESS', 'suitcore@suitmedia.com'),
            'name' => env('MAIL_FROM_NAME', 'Suitcore'),
        ],
        'subject' => 'Welcome !',
        'to' => 'test@suitmedia.com',
        'siteurl' => env('APP_URL', 'http://suitcore.local'),
        'sitename' => env('APP_NAME', 'Suitcore Instances'),
        'activation' => ['subject' => 'Aktifasi Akun'],
        'welcome' => [],
        'alert' => ['parent' => 'welcome', 'subject' => 'alert'],
        'invoice' => [],
    ],

    // Application Repositories
    'repositories' => [],

    // Option Autocomplete Routes
    'data_options_route_name' => [],

    // Setting Items
    'setting_items' => [
        "company_identity" => [
            "icon" => "fa fa-building-o",
            "label" => "Company Identity",
            "elements" => [
                "legalname" => [
                    "label" => "Company Name",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => env('APP_NAME', 'Suitcore')
                ],
                "brandname" => [
                    "label" => "Brand Name",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => env('APP_NAME', 'Suitcore')
                ],
                "enable_paneladmin_login_captcha" => [
                    "label" => "Enable Chaptcha For Backend Login",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_BOOLEAN,
                    "default_value" => false
                ],
                "enable_landing" => [
                    "label" => "Enable Landing",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_BOOLEAN,
                    "default_value" => false
                ]
            ]
        ],
        "social_media" => [
            "icon" => "icon-link",
            "label" => "Social Media",
            "elements" => [
                "facebook" => [
                    "label" => "Facebook Page",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ],
                "twitter" => [
                    "label" => "Twitter Page",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => env('APP_NAME', 'Suitcore')
                ],
                "youtube" => [
                    "label" => "Youtube Page",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ],
                "instagram" => [
                    "label" => "Instagram Page",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ]
            ]
        ],
        "contact_info" => [
            "icon" => "icon-call-end",
            "label" => "Contact Info",
            "elements" => [
                "email" => [
                    "label" => "Email Address",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => env("MAIL_FROM_ADDRESS", "support@suitcore.dev")
                ],
                "phone" => [
                    "label" => "Phone Number (Call Center)",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ],
                "fax" => [
                    "label" => "Fax Number",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ],
                "address" => [
                    "label" => "Address Line 1",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ],
                "address2" => [
                    "label" => "Address Line 2",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => ""
                ]
            ]
        ],
        "notification" => [
            "icon" => "fa fa-star-o",
            "label" => "Notification",
            "elements" => [
                "newsletter_send_hour" => [
                    "label" => "Newsletter Send Daily at Hour",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => "07"
                ],
                "newsletter_send_minute" => [
                    "label" => "Newsletter Send Daily at Minute",
                    "type"  => \Suitcore\Models\SuitModel::TYPE_TEXT,
                    "default_value" => "00"
                ]
            ]
        ]
    ]
];
