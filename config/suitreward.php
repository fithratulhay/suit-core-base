<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        // User Point Transaction
        \Suitreward\Repositories\Contract\UserPointRepositoryContract::class => \Suitreward\Repositories\UserPointRepository::class,

        // Reward
        \Suitreward\Repositories\Contract\RewardRepositoryContract::class => \Suitreward\Repositories\RewardRepository::class,

        // Reward Category
        \Suitreward\Repositories\Contract\RewardCategoryRepositoryContract::class => \Suitreward\Repositories\RewardCategoryRepository::class,

        // User Rewarded
        \Suitreward\Repositories\Contract\UserRewardRepositoryContract::class => \App\Repositories\UserRewardRepository::class,

        // Reward Transaction between repositories / services
        \Suitreward\Repositories\Contract\RewardFeatureRepositoryContract::class => \Suitreward\Repositories\RewardFeatureRepository::class,

    ],

];
