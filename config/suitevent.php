<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        \Suitevent\Repositories\Contract\SpeakerRepositoryContract::class => \Suitevent\Repositories\SpeakerRepository::class,
        \Suitevent\Repositories\Contract\EventRepositoryContract::class => \Suitevent\Repositories\EventRepository::class,
        \Suitevent\Repositories\Contract\EventScheduleRepositoryContract::class => \Suitevent\Repositories\EventScheduleRepository::class,
        \Suitevent\Repositories\Contract\EventRegistrationRepositoryContract::class => \Suitevent\Repositories\EventRegistrationRepository::class,
        \Suitevent\Repositories\Contract\EventCityRepositoryContract::class => \Suitevent\Repositories\EventCityRepository::class
    ],

    'data_options_route_name' => [
    	'speaker' => 'backend.speaker.options.json',
    	'event' => 'backend.event.options.json'
    ]
];