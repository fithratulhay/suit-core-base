<?php

return [
    'custom_base_model' => [],

    'repositories' => [
        \Suitsiteextra\Repositories\Contract\LogActivityRepositoryContract::class => \Suitsiteextra\Repositories\LogActivityRepository::class,
        \Suitsiteextra\Repositories\Contract\GalleryRepositoryContract::class => \Suitsiteextra\Repositories\GalleryRepository::class,
        \Suitsiteextra\Repositories\Contract\CommentRepositoryContract::class => \Suitsiteextra\Repositories\CommentRepository::class,
        \Suitsiteextra\Repositories\Contract\LikeRepositoryContract::class => \Suitsiteextra\Repositories\LikeRepository::class,
        \Suitsiteextra\Repositories\Contract\FileRepositoryContract::class => \Suitsiteextra\Repositories\FileRepository::class,
        \Suitsiteextra\Repositories\Contract\FileCategoryRepositoryContract::class => \Suitsiteextra\Repositories\FileCategoryRepository::class,
        \Suitsiteextra\Repositories\Contract\UserReviewRepositoryContract::class => \Suitsiteextra\Repositories\UserReviewRepository::class,
        \Suitsiteextra\Repositories\Contract\PollRepositoryContract::class => \Suitsiteextra\Repositories\PollRepository::class,
        \Suitsiteextra\Repositories\Contract\PollAnswerRepositoryContract::class => \Suitsiteextra\Repositories\PollAnswerRepository::class,
        \Suitsiteextra\Repositories\Contract\PollOptionRepositoryContract::class => \Suitsiteextra\Repositories\PollOptionRepository::class,
        \Suitsiteextra\Repositories\Contract\PollCategoryRepositoryContract::class => \Suitsiteextra\Repositories\PollCategoryRepository::class,
    ],

    'data_options_route_name' => [
        'suitpoll' => 'backend.poll.options.json',
        'suitpolloption' => 'backend.poll-option.options.json',
    ]

    /*
    // SINGLETON IMPLEMENTATION
    'repositories' => [
        \Suitsite\Repositories\Contract\BannerRepositoryContract::class => [
            \Suitsite\Repositories\BannerRepository::class => [
                \Suitsite\Models\Banner::class,
            ],
        ],

        \Suitsite\Repositories\Contract\ContactMessageRepositoryContract::class => [
            \Suitsite\Repositories\ContactMessageRepository::class => [
                \Suitsite\Models\ContactMessage::class,
            ],
        ],

        \Suitsite\Repositories\Contract\ContentTypeRepositoryContract::class => [
            \Suitsite\Repositories\ContentTypeRepository::class => [
                \Suitsite\Models\ContentType::class,
            ],
        ],

        \Suitsite\Repositories\Contract\ContentCategoryRepositoryContract::class => [
            \Suitsite\Repositories\ContentCategoryRepository::class => [
                \Suitsite\Models\ContentCategory::class,
                \Suitsite\Repositories\ContentTypeRepository::class => [
                    \Suitsite\Models\ContentType::class,
                ],
            ],
        ],

        \Suitsite\Repositories\Contract\ContentRepositoryContract::class => [
            \Suitsite\Repositories\ContentRepository::class => [
                \Suitsite\Models\Content::class,
                \Suitsite\Repositories\ContentTypeRepository::class => [
                    \Suitsite\Models\ContentType::class,
                ],
                \Suitsite\Repositories\ContentCategoryRepository::class => [
                    \Suitsite\Models\ContentCategory::class,
                    \Suitsite\Repositories\ContentTypeRepository::class => [
                        \Suitsite\Models\ContentType::class,
                    ],
                ],
            ],
        ],

        \Suitsite\Repositories\Contract\FaqRepositoryContract::class => [
            \Suitsite\Repositories\FaqRepository::class => [
                \Suitsite\Models\Faq::class,
                \Suitsite\Repositories\FaqCategoryRepository::class => [
                    \Suitsite\Models\FaqCategory::class,
                ],
            ],
        ],

        \Suitsite\Repositories\Contract\FaqCategoryRepositoryContract::class => [
            \Suitsite\Repositories\FaqCategoryRepository::class => [
                \Suitsite\Models\FaqCategory::class,
            ],
        ],

        \Suitsite\Repositories\Contract\MenuRepositoryContract::class => [
            \Suitsite\Repositories\MenuRepository::class => [
                \Suitsite\Models\Menu::class,
            ],
        ],

        \Suitsite\Repositories\Contract\NewsletterRepositoryContract::class => [
            \Suitsite\Repositories\NewsletterRepository::class => [
                \Suitsite\Models\Newsletter::class,
            ],
        ],

        \Suitsite\Repositories\Contract\NewsletterSubscriberRepositoryContract::class => [
            \Suitsite\Repositories\NewsletterSubscriberRepository::class => [
                \Suitsite\Models\NewsletterSubscriber::class,
            ],
        ],
    ],
    */
];
